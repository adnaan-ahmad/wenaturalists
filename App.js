import React, { Component } from 'react';
import { 
  View, 
  Text,
  PermissionsAndroid,
  TouchableOpacity,
  Platform, 
  BackHandler, 
  Linking,
  StyleSheet
} from 'react-native';
import 'react-native-gesture-handler'
import messaging from '@react-native-firebase/messaging';
import VersionCheck from 'react-native-version-check'
import Modal from 'react-native-modal';
import {Provider} from 'react-redux';
import * as encoding from 'text-encoding';

import store from './src/services/Redux/Store/store'
import MainStackNavigator from './src/navigation/MainStackNavigator'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from './assets/Icons/selection.json'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  cameraPermission = async () => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
     
      } else {
        // Permission Denied
        alert('CAMERA Permission Denied');
      }
    } 
  }



  getFcmToken = async () => {
    try {
      const fcmToken = await messaging().getToken();
    } catch (error) { 
    }
  }

  requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      this.getFcmToken()
    }
  }

  checkVersion = async () => {
    try {
      let updateNeeded = await VersionCheck.needUpdate()

      if (updateNeeded && updateNeeded.isNeeded) {
        this.setState({
          visible: true
        })
      }
    } catch (error) {
      console.log("version error : ",error);
    }
  }

  redirectToStore = async () =>{
    let updateNeeded = await VersionCheck.needUpdate()
    if (updateNeeded && updateNeeded.storeUrl) {
      BackHandler.exitApp();
      Linking.openURL(updateNeeded.storeUrl);
    }
  }

  componentDidMount() {
    //this.cameraPermission()
    this.requestUserPermission()
    this.checkVersion()
    Icon.loadFont()
  }

  render() {
    return (
      <Provider store={store}>
        <Modal 
          isVisible={this.state.visible}
          style={{alignItems:'center',alignSelf:'center',width:100,height:100}}>

            <View style={styles.confirmpopModalDialog}>
              <Text style={styles.confirmPopupModalBody}>Please update the latest version of the app</Text>
              <View style={styles.modalFooter}>
                <TouchableOpacity style={styles.button} onPress={this.redirectToStore}>
                  <Text style={{color:"#fff",fontSize:12, textTransform:'uppercase',fontWeight:'600'}}>Update</Text>
                </TouchableOpacity>
              </View>
            </View>

        </Modal>
        <MainStackNavigator/>
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  confirmpopModalDialog : {
    width: 280,
    backgroundColor: "#ffffff",
    margin: 40,
    borderRadius: 4,
    overflow: 'hidden',
},
  confirmPopupModalBody : {
    paddingVertical: 20,
    paddingHorizontal:30,
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 22,
    color: '#43454a',
    textAlign: 'center',
    flexWrap: 'wrap',
    alignSelf:"center",
  },
  modalFooter : {
    backgroundColor: "#E7F3E3",
    display: 'flex',
    paddingVertical: 10,
    paddingHorizontal:15,
},
button: {
  backgroundColor:"#00394D",
  paddingVertical: 10,
  paddingHorizontal:20,
  alignSelf:'center',
  borderRadius: 4,
}
})
