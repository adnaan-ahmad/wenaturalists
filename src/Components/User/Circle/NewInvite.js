import React, { Component } from 'react'
import { ActivityIndicator, ImageBackground, ScrollView, TextInput, Dimensions, FlatList, View, Text, TouchableOpacity, Image, StyleSheet, Platform, SafeAreaView } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'
import axios from 'axios'
import Snackbar from 'react-native-snackbar'
import AsyncStorage from '@react-native-community/async-storage'

import defaultProfile from '../../../../assets/defaultProfile.png'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { REACT_APP_userServiceURL } from '../../../../env.json'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

const screenHeight = Dimensions.get('window').height

export default class NewInvite extends Component {

    constructor(props) {
        super(props)

        this.state = {
            users: [],
            selectedPage: '',
            name: '',
            searchText: '',
            selectedContactsState: [],
            searchIcon: true,
            userId: ''
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value })


        }).catch((e) => {
            console.log(e)
        })

    }

    searchNames = (query) => {
        if (query === '') {
            return this.props.connectsData
        }

        const regex = new RegExp(`${query.trim()}`, 'i')

        return this.props.connectsData.filter(item => (item.username).search(regex) >= 0)
    }

    sendParticipantRequest = () => {
        
        let userIds = []
        for (let data of this.state.selectedContactsState) {
            userIds.push(data.id)
        }

        let postBody = {
            "circleId": this.props.circleId,
            "createdBy": this.state.userId,
            "memberIds": userIds
        }
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/circle/create/member/request',
            headers: {'Content-Type': 'application/json'},
            data: postBody,
            withCredentials: true
        }).then((response, dispatch) => {
            let res = response.data
            if (res && res.status === '201 CREATED') {
                // console.log(res.status)
                
                this.textInput.clear()
                Snackbar.show({
                    backgroundColor: '#97A600',
                    text: "Invitation Sent successfully",
                    textColor: "#00394D",
                    duration: Snackbar.LENGTH_LONG,
                })
                setTimeout(() => {
                    this.setState({ selectedContactsState: [], searchText: '' })
                    this.props.changeState({ newInviteModalOpen: false })
                    this.props.getSentMemberList(this.state.userId)
                    this.props.getUsersSuggestion()
                }, 2000)
            }
        }).catch((err) => {
            // console.log(err)
            if (this.state.selectedContactsState.length === 0) {
                Snackbar.show({
                    backgroundColor: '#B22222',
                    text: "Please add minimum 1 participant",
                    // textColor: "#00394D",
                    duration: Snackbar.LENGTH_LONG,
                })
            }
            else {
                Snackbar.show({
                    backgroundColor: '#B22222',
                    text: "Please check your network or try again later",
                    // textColor: "#00394D",
                    duration: Snackbar.LENGTH_LONG,
                })
                setTimeout(() => {
                    this.setState({ selectedContactsState: [], searchText: '', newInviteModalOpen: false })
                }, 2000)
            }
        })

    }

    render() {
        // console.log('------------------------------ this.state.selectedContactsState -------------------------------', this.state.selectedContactsState)
        return (
            <SafeAreaView style={{ backgroundColor: '#F7F7F5', height: 612, position: 'absolute', bottom: -50, width: '100%' }}>

                <View style={styles.linearGradientView2}>
                    <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={styles.linearGradient2}>
                    </LinearGradient>
                </View>

                <TouchableOpacity activeOpacity={0.7} style={styles.crossButtonContainer}
                    onPress={() => this.props.changeState({ newInviteModalOpen: false })} >
                    <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon2} />
                </TouchableOpacity>


                <View style={{ backgroundColor: '#E7F3E3', paddingTop: 10, borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
                    <Text style={styles.requestEndorsementText}>ADD PARTICIPANTS</Text>
                    {this.state.selectedContactsState.length === 1 ? <Text style={styles.connectsSelectedText}>1 participant added</Text> :
                        <Text style={styles.connectsSelectedText}>{this.state.selectedContactsState.length} participants added</Text>}



                    {this.state.selectedContactsState.length < 6 ? <View style={{
                        flexDirection: 'row'
                    }}
                        keyboardShouldPersistTaps='handled' horizontal={true}

                    >
                        {this.state.selectedContactsState.map((item) => (
                            <TouchableOpacity onPress={() => this.setState({ selectedContactsState: this.state.selectedContactsState.filter((person) => person !== item) })}
                                key={item.id} activeOpacity={0.6} style={{ height: 70, width: 70, paddingTop: 8, paddingBottom: 6, paddingHorizontal: 6 }} >

                                <View>
                                    <ImageBackground source={item.originalProfileImage ? { uri: item.originalProfileImage } : defaultProfile}
                                        imageStyle={{ borderRadius: 15 }}
                                        style={[styles.image, { alignSelf: 'center' }]} >
                                        <Icon name='Cross_Rounded' size={16} color="#F2FFF5" style={styles.crossIcon} />
                                    </ImageBackground>
                                </View>
                                <Text style={styles.selectedName}>{item.username}</Text>

                            </TouchableOpacity>))}
                    </View>

                        :

                        <FlatList
                            keyboardShouldPersistTaps='handled'
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{ paddingHorizontal: 6 }}
                            keyExtractor={(item) => item.id}
                            data={this.state.selectedContactsState}
                            ref={ref => this.scrollView = ref}
                            onContentSizeChange={() => {
                                this.scrollView.scrollToEnd({ animated: true })
                            }}
                            renderItem={({ item }) => (

                                <TouchableOpacity onPress={() => this.setState({ selectedContactsState: this.state.selectedContactsState.filter((person) => person !== item) })}
                                    activeOpacity={0.6} style={{ height: 70, width: 70, paddingTop: 8, paddingBottom: 6 }} >

                                    <View>
                                        <ImageBackground source={item.originalProfileImage ? { uri: item.originalProfileImage } : defaultProfile}
                                            imageStyle={{ borderRadius: 15 }}
                                            style={[styles.image, { alignSelf: 'center' }]} >
                                            <Icon name='Cross_Rounded' size={16} color="#F2FFF5" style={styles.crossIcon} />
                                        </ImageBackground>
                                    </View>
                                    <Text style={styles.selectedName}>{item.username}</Text>

                                </TouchableOpacity>


                            )}
                        />}


                </View>



                <View style={{
                    height: 60, backgroundColor: '#F7F7F5', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: -1
                }}>

                    <TextInput
                        style={styles.textInput}
                        placeholderTextColor="#D9E1E4"
                        onChangeText={(value) => { this.setState({ searchText: value }) }}
                        color='#154A59'
                        placeholder='Search'
                        onFocus={() => this.setState({ searchIcon: false })}
                        onBlur={() => this.setState({ searchIcon: true })}
                        underlineColorAndroid="transparent"
                        ref={input => { this.textInput = input }}
                    />


                </View>


                <FlatList
                    keyboardShouldPersistTaps='handled'
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 80 }}
                    style={{ height: '50%' }}
                    keyExtractor={(item) => item.id}
                    data={this.searchNames(this.state.searchText)}
                    initialNumToRender={10}
                    renderItem={({ item }) => (

                        <TouchableOpacity activeOpacity={0.96} onPress={() => {
                            this.state.selectedContactsState.includes(item) ?

                                this.setState({ selectedContactsState: this.state.selectedContactsState.filter((person) => person !== item) })

                                : this.setState({ selectedContactsState: [...this.state.selectedContactsState, item] })

                        }} >
                            <View style={styles.item}>

                                {this.state.selectedContactsState.includes(item) ?

                                    <ImageBackground source={item.originalProfileImage ? { uri: item.originalProfileImage } : defaultProfile}
                                        imageStyle={{ borderRadius: 15 }}
                                        style={[styles.image, { marginLeft: '7%', opacity: 0.5, paddingBottom: 20 }]} >
                                        <Icon name='FollowTick' size={30} color="#00394D" style={Platform.OS === 'android' ? { marginTop: -12 } : null} />
                                    </ImageBackground>

                                    : <Image source={item.originalProfileImage ? { uri: item.originalProfileImage } : defaultProfile} style={[styles.image, { marginLeft: '7%' }]} />
                                }
                                <View style={styles.nameMsg}>
                                    <Text style={styles.name}>
                                        {item.username}
                                    </Text>

                                </View>
                                <Text style={styles.time}></Text>

                            </View>

                        </TouchableOpacity>


                    )}
                />


                <TouchableOpacity onPress={() => this.sendParticipantRequest()}
                    activeOpacity={0.8} style={styles.floatingIcon} >
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='Feedback' size={15} color='#E7F3E3' style={[styles.editIcon, Platform.OS === 'android' ? { marginTop: 12 } : null]} />
                        <Text style={styles.sendRequest}>Send Invite</Text>
                    </View>
                </TouchableOpacity>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    // --- Hubroot ---

    container: {
        flexDirection: 'column',
        textAlign: 'left',
        fontSize: 15,
        position: "absolute",
        top: 30,
        height: screenHeight,
        backgroundColor: '#fff',
        //width: '100%'
    },
    requestEndorsementText: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 14,
        color: '#154A59',
        textAlign: 'center'
    },
    connectsSelectedText: {
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        color: '#698F8A',
        textAlign: 'center',
        marginVertical: 10
    },
    crossIcon2: {
        marginTop: Platform.OS === 'android' ? 8 : 0
    },
    crossButtonContainer: {
        alignSelf: 'center',
        width: 42,
        height: 42,
        borderRadius: 21,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E7F3E3',
        marginBottom: 10
    },
    linearGradientView2: {
        width: '100%',
        height: 200,
        position: 'absolute',
        top: -50,
        alignSelf: 'center'
    },
    linearGradient2: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6
    },
    sendRequest: {
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 15,
        color: '#E7F3E3',
        marginLeft: 6
    },
    floatingIcon: {
        //flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: 175,
        height: 39,
        borderRadius: 28,
        backgroundColor: '#367681',
        position: 'absolute',
        //top: 416,
        bottom: 66,
        right: 12,
    },
    editIcon: {
        alignSelf: 'center'
    },
    crossIcon: {
        position: 'absolute',
        right: -10,
        bottom: -16,
        alignSelf: 'flex-end'
    },
    selectedName: {
        fontSize: 11,
        color: '#154A59',
        textAlign: 'center',
        // marginBottom: 6,
        fontFamily: 'Montserrat-Medium'
    },
    searchIcon: {
        position: 'absolute',
        left: 136,
        top: 15,
        // backgroundColor: 'red',
        zIndex: 2
    },
    border: {
        borderWidth: 1,
        marginRight: '6%',
        width: '95%',
        marginTop: '-1%',
        borderColor: '#91B3A2',
        alignSelf: 'center',
        borderRadius: 1,
    },
    textInput: {
        fontSize: 17,
        fontFamily: 'Montserrat-Medium',
        padding: 10,
        backgroundColor: '#FFF',
        width: '90%',
        zIndex: 1,
        textAlign: 'center',
        borderRadius: 8,
        borderColor: '#D9E1E4',
        borderWidth: 1,
        shadowColor: '#36768140',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    tabNavigator: {
        marginTop: '-8%',
    },


    // --- Assignment ---

    item: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'space-around',
        backgroundColor: '#F7F7F500',
        width: '100%',
        height: 48,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',

        // borderBottomWidth: 1,
        //borderBottomRadius: 1,
        // borderBottomColor: '#E2E7E9',
        // borderRadius: 10
    },
    image: {
        height: 30,
        width: 30,
        borderRadius: 15,
        //marginLeft: '7%',
    },
    name: {
        fontWeight: '700',
        color: '#4B4F56',
        fontSize: 14,
        textAlign: 'left',
        // marginLeft: '-6%',
        textTransform: 'capitalize'
    },
    message: {
        color: '#698F8A',
        fontSize: 10.5,
    },

    // --- New ---

    time: {
        color: '#AABCC3',
        fontSize: 10,
        marginRight: '10%'
    },
    nameMsg: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: '6%',
        //textAlign: 'left'
    },
    imageGroup: {
        height: 26,
        width: 26,
        borderRadius: 13,
    },
})
