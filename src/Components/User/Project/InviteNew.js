import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  TextInput,
  Platform,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';

import {COLORS} from '../../Shared/Colors';
import typography from '../../Shared/Typography';
import defaultShape from '../../Shared/Shape';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class InviteNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: this.props.type ? this.props.type : '',
      projectId: this.props.projectId ? this.props.projectId : '',
      projectCreatorId: this.props.projectCreatorId
        ? this.props.projectCreatorId
        : '',
      projectType: this.props.projectType ? this.props.projectType : '',
      projectTitle: this.props.projectTitle ? this.props.projectTitle : '',
      partyType: this.props.partyType ? this.props.partyType : '',
      userId: this.props.userId ? this.props.userId : '',
      nameInput: '',
      modalHeight: 300,
      addedNameList: [],
      suggestedNameList: [],
    };
  }

  componentDidMount() {
    this.state.type === 'Admin' || this.state.type === 'transferAdmin'
      ? this.getAdminConfig()
      : this.userSuggestion();
  }

  getAdminConfig = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/admin/get/config/' +
        this.state.userId +
        '?entityId=' +
        this.state.projectId,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          suggestedNameList:
            this.state.type === 'transferAdmin'
              ? response.data.body.connectedUserList
              : response.data.body.inviteeDetails,
        });
      })
      .catch((err) => console.log(err));
  };

  userSuggestion = () => {
    let url = '';
    if (
      this.state.type === 'Leader' ||
      this.state.type === 'Faculty' ||
      this.state.type === 'Speaker'
    ) {
      url = `${REACT_APP_userServiceURL}/participants/faculty-suggestions/${this.state.userId}?creatorUserId=${this.state.projectCreatorId}&projectId=${this.state.projectId}&entityType=${this.state.partyType}`;
    } else {
      url = `${REACT_APP_userServiceURL}/participants/find-project-participants-suggestions/${this.state.projectId}/${this.state.userId}/${this.state.userId}/${this.state.projectType}/${this.state.partyType}`;
    }
    axios({
      method: 'get',
      url: url,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({suggestedNameList: response.data.body});
      })
      .catch((err) => console.log('user suggesstion error : ', err));
  };

  transferAdminRight = () => {
    let adminPostBody = {
      entityId: this.state.projectId,
      adminId: this.state.addedNameList[0].id,
      superAdminId: this.props.superAdminId,
      remainAdmin: false,
    };
    console.log(adminPostBody, this.state.addedNameList[0]);
    axios({
      method: 'post',
      url: `${REACT_APP_userServiceURL}/backend/admin/transfer/right`,
      data: adminPostBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({addedNameList: [], nameInput: ''});
        this.props.changeFun();
        Snackbar.show({
          backgroundColor: COLORS.primarydark,
          text: `Transferred admin rights`,
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        });
      })
      .catch((err) => console.log('transfer admin error : ', err));
  };

  sendInvite = () => {
    let addedNameListIds = [];
    for (let i of this.state.addedNameList) {
      addedNameListIds.push(i.id);
    }
    let adminPostBody = {
      entityId: this.state.projectId,
      entityType: 'PROJECT',
      superAdminId: this.props.superAdminId,
      adminIds: addedNameListIds,
    };
    let postBody = {
      projectId: this.state.projectId,
      projectCreatorId: this.state.projectCreatorId,
      title: this.state.projectTitle,
      userIds: addedNameListIds,
      projectType: this.state.projectType,
    };

    let url = '';

    if (this.state.type === 'Admin') {
      url = `${REACT_APP_userServiceURL}/backend/admin/create`;
    } else if (this.state.type === 'Leader') {
      url = `${REACT_APP_userServiceURL}/backend/participation/faculty/send/request`;
    } else {
      url = `${REACT_APP_userServiceURL}/backend/participation/offline/send/request`;
    }

    addedNameListIds.length > 0
      ? axios({
          method: 'post',
          url: url,
          data: this.state.type === 'Admin' ? adminPostBody : postBody,
          withCredentials: true,
        })
          .then((response) => {
            this.setState({addedNameList: [], nameInput: ''});
            this.props.changeFun();
            Snackbar.show({
              backgroundColor: COLORS.primarydark,
              text: `Invite sent successfully`,
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          })
          .catch((err) => console.log('user send invite error : ', err))
      : Snackbar.show({
          backgroundColor: '#B22222',
          text: `Please add user to invite`,
          duration: Snackbar.LENGTH_LONG,
        });
  };

  searchNames = (query) => {
    if (query === '') {
      return [];
    }

    const regex = new RegExp(`${query.trim()}`, 'i');

    return this.state.suggestedNameList.filter(
      (item) => item.username.search(regex) >= 0,
    );
  };

  addUserForInvite = (item) => {
    let tempindex = this.state.suggestedNameList.findIndex(
      (x) => x.id === item.item.id,
    );
    this.state.type === 'transferAdmin' &&
      this.state.addedNameList.splice(0, 1);
    this.state.type === 'transferAdmin'
      ? this.setState({
          addedNameList: this.state.addedNameList.concat(item.item),
          nameInput: '',
        })
      : this.setState(
          {addedNameList: this.state.addedNameList.concat(item.item)},
          () => {
            this.state.suggestedNameList.splice(tempindex, 1);
            this.setState({
              suggestedNameList: this.state.suggestedNameList,
              nameInput: '',
            });
          },
        );
  };

  renderSuggestedName = (item) => {
    return (
      <View
        style={{
          marginTop: 5,
          borderBottomWidth: 0.5,
          borderBottomColor: COLORS.grey_350,
          paddingVertical: 5,
          paddingHorizontal: 15,
          alignItems: 'flex-start',
        }}>
        <TouchableOpacity
          onPress={() => this.addUserForInvite(item)}
          style={{flexDirection: 'row', alignItems: 'center'}}>
          {item.item.imageUrl || item.item.personalInfo?.profileImage ? (
            <Image
              style={{height: 30, width: 30, borderRadius: 15}}
              source={{
                uri: item.item.imageUrl
                  ? item.item.imageUrl
                  : item.item.personalInfo?.profileImage,
              }}
            />
          ) : item.item.type === 'INDIVIDUAL' && !item.item.imageUrl ? (
            <Image
              style={{height: 30, width: 30, borderRadius: 15}}
              source={defaultProfile}
            />
          ) : item.item.type === 'COMPANY' && !item.item.imageUrl ? (
            <Image
              style={{height: 30, width: 30, borderRadius: 15}}
              source={defaultBusiness}
            />
          ) : (
            <></>
          )}
          <Text
            style={[
              typography.Note2,
              {
                color: COLORS.dark_600,
                marginLeft: 6,
              },
            ]}>
            {item.item.username}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderAddUserItem = (item) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: 10,
          marginLeft: 10,
          alignSelf: 'flex-start',
          backgroundColor: COLORS.altgreen_100,
          borderRadius: 20,
          paddingHorizontal: 15,
        }}>
        {item.item.imageUrl || item.item.personalInfo?.profileImage ? (
          <Image
            style={{height: 30, width: 30, borderRadius: 15}}
            source={{
              uri: item.item.imageUrl
                ? item.item.imageUrl
                : item.item.personalInfo?.profileImage,
            }}
          />
        ) : item.item.type === 'INDIVIDUAL' && !item.item.imageUrl ? (
          <Image
            style={{height: 30, width: 30, borderRadius: 15}}
            source={defaultProfile}
          />
        ) : item.item.type === 'COMPANY' && !item.item.imageUrl ? (
          <Image
            style={{height: 30, width: 30, borderRadius: 15}}
            source={defaultBusiness}
          />
        ) : (
          <></>
        )}
        {/* <Image
          source={
            item.item.imageUrl
              ? {uri: item.item.imageUrl}
              : item.item.personalInfo?.profileImage
              ? {uri: item.item.personalInfo.profileImage}
              : defaultProfile
          }
          style={{height: 30, width: 30, borderRadius: 15}}
        /> */}
        <Text
          style={[
            typography.Note2,
            {
              color: COLORS.dark_600,
              marginLeft: 6,
            },
          ]}>
          {item.item.username}
        </Text>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: COLORS.white,
          height: Platform.OS === 'ios' ? this.state.modalHeight : 300,
          borderTopLeftRadius: 15,
          borderTopRightRadius: 15,
        }}>
        <View
          style={{
            marginTop: 20,
            alignSelf: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          {this.state.type !== 'transferAdmin' && (
            <Icon name="Add_Group" size={16} color={COLORS.dark_800} />
          )}
          <Text
            style={[
              typography.Button_Lead,
              {
                color: COLORS.dark_800,
                marginLeft: 5,
              },
            ]}>
            {this.state.type === 'transferAdmin'
              ? 'Transfer Admin Rights'
              : 'Add ' + this.state.type}
          </Text>
        </View>

        <TextInput
          placeholder="Enter Name"
          style={{
            borderBottomColor: COLORS.dark_600,
            borderBottomWidth: 2,
            width: '60%',
            fontSize: 16,
            alignSelf: 'center',
            marginTop: 15,
            paddingLeft: 5,
          }}
          onFocus={() => this.setState({modalHeight: 450})}
          onBlur={() => this.setState({modalHeight: 300})}
          onChangeText={(value) => this.setState({nameInput: value})}
          value={this.state.nameInput}
        />

        {this.state.suggestedNameList.length > 0 &&
          this.state.nameInput !== '' && (
            <FlatList
              data={this.searchNames(this.state.nameInput)}
              keyExtractor={(item) => item.id}
              renderItem={(item) => this.renderSuggestedName(item)}
            />
          )}

        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={this.state.addedNameList}
          keyExtractor={(item) => item.id}
          renderItem={(item) => this.renderAddUserItem(item)}
        />

        <TouchableOpacity
          onPress={
            this.state.type === 'transferAdmin'
              ? this.transferAdminRight
              : this.sendInvite
          }
          style={{
            backgroundColor: COLORS.dark_800,
            borderRadius: 4,
            paddingHorizontal: 10,
            paddingVertical: 6,
            alignSelf: 'center',
            bottom: 25,
            position: 'absolute',
          }}>
          <Text style={[typography.Caption, {color: COLORS.white}]}>
            {this.state.type === 'transferAdmin' ? 'SUBMIT' : 'SEND INVITE'}
          </Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}
