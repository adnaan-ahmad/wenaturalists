import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  TextInput,
  Platform,
  FlatList,
  TouchableOpacity,
  Modal,
  StyleSheet,
  Alert,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import LinearGradient from 'react-native-linear-gradient';

import {COLORS} from '../../Shared/Colors';
import typography from '../../Shared/Typography';
import defaultShape from '../../Shared/Shape';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import InviteNew from './InviteNew';
import {unixTime2, validateEmail} from '../../Shared/commonFunction';
import defaultProfile from '../../../../assets/defaultProfile.png';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');

export default class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectId: this.props.projectId ? this.props.projectId : '',
      projectCreatorId: this.props.projectCreatorId
        ? this.props.projectCreatorId
        : '',
      projectType: this.props.projectType ? this.props.projectType : '',
      projectTitle: this.props.projectTitle ? this.props.projectTitle : '',
      partyType: this.props.partyType ? this.props.partyType : '',
      userId: this.props.userId ? this.props.userId : '',

      pageAdminHistory: 0,
      pagePendingInvitation: 0,
      adminConfig: {},
      adminList: [],
      adminHistory: [],
      pendingInvitation: [],
      inviteNewAdminModalOpen: false,
      transferAdminModalOpen: false,
    };
  }

  componentDidMount() {
    this.getAdminConfig();
    this.getCurrentAdminList();
    this.getAdminHistory();
    this.getPendingInvitation();
  }

  getAdminConfig = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/admin/get/config/' +
        this.state.userId +
        '?entityId=' +
        this.state.projectId,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          adminConfig: response.data.body,
        });
      })
      .catch((err) => console.log(err));
  };

  getCurrentAdminList = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/admin/getCurrentAdmininstrations/' +
        this.state.userId +
        '/' +
        this.state.projectId,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          adminList: this.state.adminList.concat(response.data.body),
        });
      })
      .catch((err) => console.log(err));
  };

  getAdminHistory = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/admin/getAdminHistory/' +
        this.state.userId +
        '/' +
        this.state.projectId +
        '?page=' +
        this.state.pageAdminHistory +
        '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          adminHistory: this.state.adminHistory.concat(
            response.data.body.content,
          ),
        });
      })
      .catch((err) => console.log(err));
  };

  handleLoadMore = () => {
    this.setState({pageAdminHistory: this.state.pageAdminHistory + 1}, () =>
      this.getAdminHistory(),
    );
  };

  getPendingInvitation = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/admin/getPendingInvitations/' +
        this.state.userId +
        '/' +
        this.state.projectId +
        '?page=' +
        this.state.pagePendingInvitation +
        '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          pendingInvitation: this.state.pendingInvitation.concat(
            response.data.body.content,
          ),
        });
      })
      .catch((err) => console.log(err));
  };

  alertWithdraw = (id, type, username) => {
    Alert.alert(
      '',
      type && type === 'removeAdmin'
        ? `Are you sure you want to remove ${username} as an admin for the ${this.state.projectType} ${this.state.projectTitle}?`
        : type && type === 'withdrawTransfer'
        ? `Are you sure you want to withdraw this transfer invite?`
        : 'Are you sure you want to withdraw this invite?',
      [
        {
          text: 'YES',
          onPress: () =>
            type && type === 'removeAdmin'
              ? this.removeAdmin(id)
              : type && type === 'withdrawTransfer'
              ? this.handleWithdrawTransfer()
              : this.handleWithdrawSubmit(id),
          style: 'cancel',
        },
        {
          text: 'NO',
          // onPress: () => console.log('user cancelled deletion'),
        },
      ],
    );
  };

  handleWithdrawSubmit = (id) => {
    let postBody = {
      entityId: this.state.projectId,
      adminId: id,
      superAdminId: this.state.adminConfig.superAdminId,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/admin/withdraw/invite',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState(
          {
            pagePendingInvitation: 0,
            pendingInvitation: [],
            pageAdminHistory: 0,
            adminHistory: [],
          },
          () => {
            this.getPendingInvitation();
            this.getAdminConfig();
            this.getAdminHistory();
          },
        );
      })
      .catch((err) => console.log(err));
  };

  handleWithdrawTransfer = () => {
    let postBody = {
      entityId: this.state.projectId,
      superAdminId: this.state.adminConfig.superAdminId,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/admin/withdraw/transfer/invite',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState(
          {
            pagePendingInvitation: 0,
            pendingInvitation: [],
            pageAdminHistory: 0,
            adminHistory: [],
            adminList: [],
          },
          () => {
            this.getPendingInvitation();
            this.getAdminConfig();
            this.getAdminHistory();
            this.getCurrentAdminList();
          },
        );
      })
      .catch((err) => console.log(err));
  };

  changeFun = () => {
    this.getAdminConfig();
    this.setState(
      {
        pagePendingInvitation: 0,
        pendingInvitation: [],
        adminHistory: [],
        pageAdminHistory: 0,
        adminList: [],
      },
      () => {
        this.getPendingInvitation();
        this.getAdminHistory();
        this.getCurrentAdminList();
      },
    );
  };

  removeAdmin = (id) => {
    let postBody = {
      entityId: this.state.projectId,
      adminId: id,
      superAdminId: this.state.adminConfig.superAdminId,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/admin/remove',
      headers: {'Content-Type': 'application/json'},
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '201 CREATED') {
          this.getAdminConfig();
          this.setState(
            {
              pageAdminHistory: 0,
              pagePendingInvitation: 0,
              adminHistory: [],
              pendingInvitation: [],
              adminList: [],
            },
            () => {
              this.getCurrentAdminList();
              this.getAdminHistory();
              this.getPendingInvitation();
            },
          );
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  inviteNewAdminModal = () => {
    return (
      <Modal
        visible={this.state.inviteNewAdminModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({inviteNewAdminModalOpen: false})}>
            <Icon name="Cross" size={13} color={COLORS.dark_600} />
          </TouchableOpacity>

          <InviteNew
            type="Admin"
            userId={this.state.userId}
            projectId={this.state.projectId}
            projectCreatorId={this.state.projectCreatorId}
            projectType={this.state.projectType}
            partyType={this.state.partyType}
            projectTitle={this.state.projectTitle}
            superAdminId={this.state.adminConfig.superAdminId}
            changeFun={this.changeFun}
          />
        </View>
      </Modal>
    );
  };

  transferAdminModal = () => {
    return (
      <Modal
        visible={this.state.transferAdminModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({transferAdminModalOpen: false})}>
            <Icon name="Cross" size={13} color={COLORS.dark_600} />
          </TouchableOpacity>

          <InviteNew
            type="transferAdmin"
            userId={this.state.userId}
            projectId={this.state.projectId}
            projectCreatorId={this.state.projectCreatorId}
            projectType={this.state.projectType}
            partyType={this.state.partyType}
            projectTitle={this.state.projectTitle}
            superAdminId={this.state.adminConfig.superAdminId}
            changeFun={this.changeFun}
          />
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <View style={{paddingHorizontal: 15, paddingTop: 10}}>
        {this.inviteNewAdminModal()}
        {this.transferAdminModal()}

        {/*************  Current Administration starts *****************/}

        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={[typography.Button_Lead, {color: COLORS.dark_800}]}>
            Current Administration
          </Text>
          {this.state.adminConfig.noOfAdminCanBeAdded > 0 && (
            <TouchableOpacity
              onPress={() => this.setState({inviteNewAdminModalOpen: true})}
              style={{marginLeft: 15}}>
              <Icon name="AddUser" color={COLORS.dark_800} size={14} />
            </TouchableOpacity>
          )}
        </View>
        <FlatList
          horizontal
          style={{marginTop: 10}}
          showsHorizontalScrollIndicator={false}
          data={this.state.adminList}
          keyExtractor={(item) => item.id}
          renderItem={(item) => (
            <TouchableOpacity
              onPress={() => {
                item.item.userType === 'INDIVIDUAL' &&
                item.item.userId === this.state.userId
                  ? this.props.navigation('ProfileStack')
                  : item.item.userType === 'INDIVIDUAL' &&
                    item.item.userId !== this.state.userId
                  ? this.props.navigation('ProfileStack', {
                      screen: 'OtherProfileScreen',
                      params: {userId: item.item.userId},
                    })
                  : item.item.userType === 'COMPANY'
                  ? this.props.navigation('ProfileStack', {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.item.userId},
                    })
                  : this.props.navigation('CircleProfileStack', {
                      screen: 'CircleProfile',
                      params: {slug: item.item.customUrl},
                    });
              }}
              style={{
                width: 180,
                height: 180,
                backgroundColor: COLORS.white,
                borderRadius: 4,
                alignItems: 'center',
                marginRight: 10,
              }}>
              <Image
                source={
                  item.item.originalProfileImage
                    ? {uri: item.item.originalProfileImage}
                    : defaultProfile
                }
                style={{height: 40, width: 40, borderRadius: 20, marginTop: 12}}
              />
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginTop: 6},
                ]}>
                {item.item.username}
              </Text>
              <Text style={[typography.Caption, {color: COLORS.grey_400}]}>
                {item.item.country}
              </Text>

              <View
                style={{
                  borderWidth: 1,
                  borderColor: COLORS.grey_350,
                  borderRadius: 4,
                  paddingVertical: 2,
                  paddingHorizontal: 5,
                  marginTop: 6,
                }}>
                <Text style={[typography.Note, {color: COLORS.grey_350}]}>
                  {item.item.adminType}
                </Text>
              </View>

              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.grey_400, marginTop: 2},
                ]}>
                Since{' '}
                {moment.unix(item.item.acceptedDate / 1000).format('MMM-YYYY')}
              </Text>

              {(item.item.userId === this.state.adminConfig?.superAdminId ||
                item.item.canRemoveAdmin) && (
                <TouchableOpacity
                  onPress={() =>
                    this.state.adminConfig?.superAdminId &&
                    item.item.canTransferSuperAdminRight === 'PENDING'
                      ? this.alertWithdraw(
                          item.item.userId,
                          'withdrawTransfer',
                          item.item.username,
                        )
                      : item.item.userId ===
                        this.state.adminConfig?.superAdminId
                      ? this.setState({transferAdminModalOpen: true})
                      : this.alertWithdraw(
                          item.item.userId,
                          'removeAdmin',
                          item.item.username,
                        )
                  }
                  style={{
                    marginTop: 8,
                    backgroundColor: COLORS.altgreen_100,
                    paddingVertical: 6,
                    paddingHorizontal: 20,
                    borderRadius: 4,
                  }}>
                  <Text
                    style={[typography.Note2, {color: COLORS.altgreen_400}]}>
                    {item.item.userId ===
                      this.state.adminConfig?.superAdminId &&
                    item.item.canTransferSuperAdminRight === 'PENDING'
                      ? 'Transfer Pending'
                      : item.item.userId ===
                        this.state.adminConfig?.superAdminId
                      ? 'Transfer Rights'
                      : item.item.canRemoveAdmin
                      ? 'Remove Admin'
                      : ''}
                  </Text>
                </TouchableOpacity>
              )}
            </TouchableOpacity>
          )}
        />

        {/**********  Current Administration ends ************/}

        {/**********  Admin history starts ************/}

        {this.state.adminHistory.length > 0 && (
          <Text
            style={[
              typography.Button_Lead,
              {color: COLORS.dark_800, marginTop: 15},
            ]}>
            Admin History
          </Text>
        )}
        <FlatList
          horizontal
          style={{marginTop: 10}}
          showsHorizontalScrollIndicator={false}
          data={this.state.adminHistory}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={1}
          keyExtractor={(item) => item.id}
          renderItem={(item) => (
            <View
              style={{
                paddingVertical: 6,
                paddingHorizontal: 10,
                backgroundColor: COLORS.white,
                borderRadius: 4,
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 10,
              }}>
              <TouchableOpacity
                onPress={() => {
                  item.item.userType === 'INDIVIDUAL' &&
                  item.item.userId === this.state.userId
                    ? this.props.navigation('ProfileStack')
                    : item.item.userType === 'INDIVIDUAL' &&
                      item.item.userId !== this.state.userId
                    ? this.props.navigation('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.item.userId},
                      })
                    : item.item.userType === 'COMPANY'
                    ? this.props.navigation('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                        params: {userId: item.item.userId},
                      })
                    : this.props.navigation('CircleProfileStack', {
                        screen: 'CircleProfile',
                        params: {slug: item.item.customUrl},
                      });
                }}
                style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  source={
                    item.item.originalProfileImage
                      ? {uri: item.item.originalProfileImage}
                      : defaultProfile
                  }
                  style={{height: 40, width: 40, borderRadius: 20}}
                />
                <View style={{marginLeft: 6}}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    {item.item.username}
                  </Text>
                  <Text style={[typography.Caption, {color: COLORS.grey_400}]}>
                    {item.item.country}
                  </Text>
                  {item.item.acceptedDate > 0 && (
                    <Text style={[typography.Note2, {color: COLORS.dark_600}]}>
                      {moment
                        .unix(item.item.acceptedDate / 1000)
                        .format('MMM-YYYY')}{' '}
                      to{' '}
                      {moment
                        .unix(item.item.revokedDate / 1000)
                        .format('MMM-YYYY')}
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
            </View>
          )}
        />

        {/**********  Admin history ends ************/}

        {/**********  Invitation starts ************/}

        {this.state.pendingInvitation.length > 0 && (
          <Text
            style={[
              typography.Button_Lead,
              {color: COLORS.dark_800, marginTop: 15},
            ]}>
            Inviations
          </Text>
        )}
        <FlatList
          style={{marginBottom: 30}}
          showsVerticalScrollIndicator={false}
          data={this.state.pendingInvitation}
          keyExtractor={(item) => item.id}
          renderItem={(item) => (
            <View
              style={{
                paddingVertical: 6,
                paddingHorizontal: 10,
                backgroundColor: COLORS.white,
                borderRadius: 4,
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 10,
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                onPress={() => {
                  item.item.userType === 'INDIVIDUAL' &&
                  item.item.userId === this.state.userId
                    ? this.props.navigation('ProfileStack')
                    : item.item.userType === 'INDIVIDUAL' &&
                      item.item.userId !== this.state.userId
                    ? this.props.navigation('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.item.userId},
                      })
                    : item.item.userType === 'COMPANY'
                    ? this.props.navigation('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                        params: {userId: item.item.userId},
                      })
                    : this.props.navigation('CircleProfileStack', {
                        screen: 'CircleProfile',
                        params: {slug: item.item.customUrl},
                      });
                }}
                style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  source={
                    item.item.originalProfileImage
                      ? {uri: item.item.originalProfileImage}
                      : defaultProfile
                  }
                  style={{height: 40, width: 40, borderRadius: 20}}
                />
                <View style={{marginLeft: 6}}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    {item.item.username}
                  </Text>
                  <Text style={[typography.Caption, {color: COLORS.grey_400}]}>
                    {item.item.country}
                  </Text>
                </View>
              </TouchableOpacity>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  alignSelf: 'flex-start',
                  marginTop: 10,
                }}>
                {item.item.status === 'Revoked' && (
                  <Icon name="FollowTick" color={COLORS.dark_600} size={12} />
                )}
                {item.item.status !== 'Revoked' ? (
                  <TouchableOpacity
                    onPress={() => this.alertWithdraw(item.item.userId)}
                    style={{
                      marginLeft: 8,
                      backgroundColor: COLORS.altgreen_100,
                      paddingVertical: 6,
                      paddingHorizontal: 10,
                      borderRadius: 4,
                    }}>
                    <Text
                      style={[typography.Note2, {color: COLORS.altgreen_400}]}>
                      WITHDRAW
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <Text
                    style={[
                      typography.Note2,
                      {color: COLORS.dark_600, marginLeft: 4},
                    ]}>
                    Revoked
                  </Text>
                )}
              </View>
            </View>
          )}
        />

        {/**********  Invitation ends ************/}
      </View>
    );
  }
}
