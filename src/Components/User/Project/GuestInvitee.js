import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  TextInput,
  Platform,
  FlatList,
  TouchableOpacity,
  Modal,
  StyleSheet,
  Alert,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import LinearGradient from 'react-native-linear-gradient';

import {COLORS} from '../../Shared/Colors';
import typography from '../../Shared/Typography';
import defaultShape from '../../Shared/Shape';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import InviteNew from './InviteNew';
import {validateEmail} from '../../Shared/commonFunction';
import defaultProfile from '../../../../assets/defaultProfile.png';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class GuestInvitee extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectId: this.props.projectId ? this.props.projectId : '',
      projectCreatorId: this.props.projectCreatorId
        ? this.props.projectCreatorId
        : '',
      projectType: this.props.projectType ? this.props.projectType : '',
      projectTitle: this.props.projectTitle ? this.props.projectTitle : '',
      partyType: this.props.partyType ? this.props.partyType : '',
      userId: this.props.userId ? this.props.userId : '',

      inviteNewParticipantModalOpen: false,
      inviteOutsideWenatExpand: false,

      mailList: [],
      mailInput: '',
      page: 0,
      facultyList: [],
    };
  }

  componentDidMount() {
    this.facultyList();
  }

  facultyList = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/faculty-list/' +
        this.state.projectId +
        '/' +
        this.state.userId +
        '?page=' +
        this.state.page +
        '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          facultyList: this.state.facultyList.concat(
            response.data.body.content,
          ),
        });
      })
      .catch((err) => console.log(err));
  };

  handleLoadMore = () => {
    this.setState({page: this.state.page + 1}, () => this.facultyList());
  };

  verifyOutsideWenat = (value) => {
    validateEmail(value)
      ? axios({
          method: 'get',
          url:
            REACT_APP_userServiceURL +
            '/participants/verify/wenatmember/?emails=' +
            this.state.mailInput,
          withCredentials: true,
        })
          .then((response) => {
            if (response.data.body.length > 0) {
              this.setState({mailInput: ''});
              Snackbar.show({
                backgroundColor: '#B22222',
                text: `${value.trim()} is already a member at WeNaturalists`,
                duration: Snackbar.LENGTH_LONG,
              });
            } else {
              this.setState({
                mailList: [...this.state.mailList, value.trim()],
                mailInput: '',
              });
            }
          })
          .catch((err) => console.log(err))
      : Snackbar.show({
          backgroundColor: '#B22222',
          text: `Please enter a valid email`,
          duration: Snackbar.LENGTH_LONG,
        });
  };

  sendInviteOutsideWenat = () => {
    let postBody = {
      emails: this.state.mailList,
      invitationType: 'PARTICIPATION',
      approverId: this.state.userId,
      projectId: this.state.projectId,
    };
    this.state.mailList.length > 0
      ? axios({
          method: 'post',
          url: REACT_APP_userServiceURL + '/backend/participation/outside/save',
          data: postBody,
          withCredentials: true,
        })
          .then((response) => {
            this.setState({
              mailList: [],
              mailInput: '',
            });
            Snackbar.show({
              backgroundColor: COLORS.primarydark,
              text: `Invite sent successfully`,
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          })
          .catch((err) =>
            Snackbar.show({
              backgroundColor: '#B22222',
              text: err.response.data.message,
              duration: Snackbar.LENGTH_LONG,
            }),
          )
      : Snackbar.show({
          backgroundColor: '#B22222',
          text: `Please enter email addresses to send invite`,
          duration: Snackbar.LENGTH_LONG,
        });
  };

  alertWithdraw = (id) => {
    Alert.alert('', 'Are you sure you want to withdraw this invite?', [
      {
        text: 'YES',
        onPress: () => this.handleWithdrawSubmit(id),
        style: 'cancel',
      },
      {
        text: 'NO',
        // onPress: () => console.log('user cancelled deletion'),
      },
    ]);
  };

  handleWithdrawSubmit = (id) => {
    let postBody = {
      projectId: this.state.projectId,
      userId: id,
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/backend/participation/withdraw-invitee-request',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({page: 0, facultyList: []}, () => this.facultyList());
      })
      .catch((err) => console.log(err));
  };

  changeFun = () => {
    this.setState({page: 0, facultyList: []}, () => this.facultyList());
  };

  inviteNewParticipantModal = () => {
    return (
      <Modal
        visible={this.state.inviteNewParticipantModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({inviteNewParticipantModalOpen: false})
            }>
            <Icon name="Cross" size={13} color={COLORS.dark_600} />
          </TouchableOpacity>

          <InviteNew
            type={
              this.state.projectType === 'TRAINING'
                ? 'Faculty'
                : this.state.projectType === 'EVENT' ||
                  this.state.projectType === 'STORYBOOK'
                ? 'Speaker'
                : 'Leader'
            }
            userId={this.state.userId}
            projectId={this.state.projectId}
            projectCreatorId={this.state.projectCreatorId}
            projectType={this.state.projectType}
            partyType={this.state.partyType}
            projectTitle={this.state.projectTitle}
            changeFun={this.changeFun}
          />
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <View>
        {this.inviteNewParticipantModal()}
        <TouchableOpacity
          onPress={() => this.setState({inviteNewParticipantModalOpen: true})}
          style={{
            backgroundColor: COLORS.altgreen_300,
            borderRadius: 8,
            paddingHorizontal: 10,
            paddingVertical: 6,
            alignSelf: 'flex-end',
            marginTop: 15,
            marginRight: 15,
          }}>
          <Text style={[typography.Caption, {color: COLORS.white}]}>
            Invite New{' '}
            {this.state.projectType === 'TRAINING'
              ? 'Faculty'
              : this.state.projectType === 'EVENT' ||
                this.state.projectType === 'STORYBOOK'
              ? 'Speaker'
              : 'Leader'}
          </Text>
        </TouchableOpacity>

        {/* ******************** Invite friends outside WeNaturalists Starts ******************* */}

        <TouchableOpacity
          onPress={() =>
            this.setState({
              inviteOutsideWenatExpand: !this.state.inviteOutsideWenatExpand,
              mailList: [],
              mailInput: '',
            })
          }
          style={{
            marginTop: 12,
            paddingHorizontal: 15,
            paddingVertical: 8,
            backgroundColor: COLORS.altgreen_250,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon
              name="Add_Group"
              size={14}
              color={COLORS.dark_800}
              style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
            />
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_800,
                  marginLeft: 6,
                  marginRight: 17,
                  marginVertical: 8,
                },
              ]}>
              Invite friends outside WeNaturalists
            </Text>
          </View>
          <Icon
            name={
              this.state.inviteOutsideWenatExpand ? 'Arrow_Up' : 'Arrow_Down'
            }
            size={12}
            color={COLORS.dark_800}
            style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
          />
        </TouchableOpacity>
        {this.state.inviteOutsideWenatExpand && (
          <View
            style={{
              paddingHorizontal: 15,
              paddingBottom: 8,
              backgroundColor: COLORS.altgreen_250,
              alignItems: 'center',
            }}>
            <View
              style={{
                minHeight: 100,
                width: '90%',
                alignSelf: 'flex-end',
                backgroundColor: COLORS.white,
                borderRadius: 8,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                paddingLeft: 10,
              }}>
              <FlatList
                keyboardShouldPersistTaps="handled"
                scrollEventThrottle={0}
                ref={(ref) => (this.scrollView = ref)}
                onContentSizeChange={() => {
                  this.scrollView.scrollToEnd({animated: false});
                }}
                columnWrapperStyle={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                numColumns={10}
                showsVerticalScrollIndicator={false}
                data={this.state.mailList}
                keyExtractor={(item, index) => item + index}
                renderItem={({item, index}) => (
                  <View
                    style={{
                      marginLeft: 5,
                      marginTop: 5,
                      backgroundColor: COLORS.green_500,
                      borderRadius: 8,
                      paddingVertical: 2,
                      paddingHorizontal: 5,
                    }}>
                    <Text style={[typography.Note2, {color: COLORS.white}]}>
                      {item}
                    </Text>
                  </View>
                )}
              />
              <TextInput
                multiline
                placeholder="You can add upto 10 email addresses"
                style={{
                  height: 100,
                  width: '100%',
                  paddingLeft: 10,
                  paddingTop: 10,
                }}
                onChangeText={(value) => {
                  this.setState({mailInput: value.trim()});

                  value[value.length - 1] === ' ' && value.trim()
                    ? this.verifyOutsideWenat(value)
                    : null;
                }}
                value={this.state.mailInput}
              />
            </View>
            <TouchableOpacity
              onPress={this.sendInviteOutsideWenat}
              style={{
                backgroundColor: COLORS.dark_600,
                borderRadius: 4,
                paddingHorizontal: 10,
                paddingVertical: 6,
                alignSelf: 'flex-end',
                marginTop: 10,
              }}>
              <Text style={[typography.Caption, {color: COLORS.white}]}>
                SEND INVITE
              </Text>
            </TouchableOpacity>
          </View>
        )}

        {/* ******************** Invite friends outside WeNaturalists Ends ******************* */}

        {/* ******************** Faculty list starts ******************* */}

        <FlatList
          style={{marginTop: 10}}
          showsVerticalScrollIndicator={false}
          data={this.state.facultyList}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={2}
          keyExtractor={(item) => item.id}
          renderItem={({item, index}) => (
            <View
              style={{
                marginTop: 10,
                width: '90%',
                alignSelf: 'center',
                paddingHorizontal: 15,
                paddingVertical: 8,
                backgroundColor: COLORS.white,
                borderRadius: 4,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={
                    item.originalProfileImage
                      ? {uri: item.originalProfileImage}
                      : defaultProfile
                  }
                  style={{height: 30, width: 30, borderRadius: 15}}
                />
                <View style={{marginLeft: 8}}>
                  <Text
                    numberOfLines={1}
                    style={[
                      typography.Caption,
                      {
                        color: COLORS.dark_800,
                        textTransform: 'capitalize',
                        maxWidth: 120,
                      },
                    ]}>
                    {item.username}
                  </Text>
                  <Text style={[typography.Note2, {color: COLORS.grey_400}]}>
                    {item.country}
                  </Text>
                </View>
              </View>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name={item.status === 'REQUESTED' ? 'Time' : 'FollowTick'}
                  color={
                    item.status === 'REQUESTED'
                      ? COLORS.altgreen_300
                      : COLORS.dark_600
                  }
                  size={12}
                />
                {item.withdrawInvite ? (
                  <TouchableOpacity
                    onPress={() => this.alertWithdraw(item.userId)}
                    style={{
                      marginLeft: 8,
                      backgroundColor: COLORS.altgreen_100,
                      paddingVertical: 6,
                      paddingHorizontal: 10,
                      borderRadius: 4,
                    }}>
                    <Text
                      style={[typography.Note2, {color: COLORS.altgreen_400}]}>
                      WITHDRAW
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <Text
                    style={[
                      typography.Note2,
                      {color: COLORS.dark_600, marginLeft: 4},
                    ]}>
                    {item.status === 'ACCEPTED'
                      ? 'Accepted'
                      : item.status === 'DELETED'
                      ? 'Declined'
                      : 'Withdrawn'}
                  </Text>
                )}
              </View>
            </View>
          )}
        />

        {/* ******************** Faculty list Ends ******************* */}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
