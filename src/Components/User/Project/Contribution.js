import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  TextInput,
  Platform,
  FlatList,
  TouchableOpacity,
  Modal,
  StyleSheet,
  Alert,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import LinearGradient from 'react-native-linear-gradient';

import {COLORS} from '../../Shared/Colors';
import typography from '../../Shared/Typography';
import defaultShape from '../../Shared/Shape';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import InviteNew from './InviteNew';
import {unixTime2, validateEmail} from '../../Shared/commonFunction';
import defaultProfile from '../../../../assets/defaultProfile.png';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');

export default class Contribution extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.userId ? this.props.userId : '',
      projectId: this.props.projectId ? this.props.projectId : '',
      page: 0,
      contributorList: [],
    };
  }

  componentDidMount() {
    this.getContributionDetails();
  }

  getContributionDetails = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/find-project-participants-details-by-projectId/' +
        this.state.projectId +
        '?userId=' +
        this.state.userId +
        '&page=' +
        this.state.page +
        '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          contributorList: this.state.contributorList.concat(
            response.data.body.content,
          ),
        });
      })
      .catch((err) => console.log(err));
  };

  render() {
    return (
      <View>
        <Text
          style={[
            typography.Button_Lead,
            {color: COLORS.dark_800, marginLeft: 15, marginTop: 15},
          ]}>
          Contributors
        </Text>

        <FlatList
          style={{marginBottom: 30}}
          showsVerticalScrollIndicator={false}
          data={this.state.contributorList}
          keyExtractor={(item) => item.id}
          renderItem={(item) => (
            <View
              style={{
                paddingVertical: 6,
                paddingHorizontal: 10,
                backgroundColor: COLORS.white,
                borderRadius: 4,
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 10,
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                onPress={() => {
                  item.item.userType === 'INDIVIDUAL' &&
                  item.item.userId === this.state.userId
                    ? this.props.navigation('ProfileStack')
                    : item.item.userType === 'INDIVIDUAL' &&
                      item.item.userId !== this.state.userId
                    ? this.props.navigation('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.item.userId},
                      })
                    : item.item.userType === 'COMPANY'
                    ? this.props.navigation('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                        params: {userId: item.item.userId},
                      })
                    : this.props.navigation('CircleProfileStack', {
                        screen: 'CircleProfile',
                        params: {slug: item.item.customUrl},
                      });
                }}
                style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  source={
                    item.item.originalProfileImage
                      ? {uri: item.item.originalProfileImage}
                      : defaultProfile
                  }
                  style={{height: 40, width: 40, borderRadius: 20}}
                />
                <View style={{marginLeft: 6}}>
                  <Text
                    numberOfLines={1}
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, maxWidth: 250},
                    ]}>
                    {item.item.userName}
                  </Text>
                  <Text style={[typography.Caption, {color: COLORS.grey_400}]}>
                    {item.item.country}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    );
  }
}
