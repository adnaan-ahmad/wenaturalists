import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import {Badge} from 'react-native-paper';

import defaultShape from '../../Shared/Shape';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {COLORS} from '../../Shared/Colors';
import typography from '../../Shared/Typography';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
export default class HashTagItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  numberShortForm = (data) => {
    if (data != undefined) {
      let number = data;
      var quotient = Math.floor(number / 10);
      var remainder = number % 10;
      if (remainder === 0 || data < 10) {
        return data;
      } else {
        return quotient * 10 + '+';
      }
    } else {
      return null;
    }
  };

  render() {
    const {item} = this.props;
    return (
      <View style={[defaultShape.card8_a, {height: 80, width: '100%'}]}>
        <TouchableOpacity
          onPress={() =>
            this.props.hashTagDetailNavigation('HashTagDetail',{slug: item.hashtag})
          }>
          <Text
            style={[
              typography.Button_2,
              {color: COLORS.dark_800, paddingLeft: 10},
            ]}>
            {' '}
            {'#' + item.hashtag}{' '}
          </Text>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            justifyContent: 'space-evenly',
          }}>
          <TouchableOpacity style={styles.iconStyle}>
            <Badge size={16} style={styles.badgeStyle}>
              {this.numberShortForm(item.image)}
            </Badge>
            <Icon
              name="Photos"
              color={item.image === 0 ? COLORS.altgreen_400 : COLORS.dark_800}
              size={22}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.iconStyle}>
            <Badge size={16} style={styles.badgeStyle}>
              {this.numberShortForm(item.video)}{' '}
            </Badge>
            <Icon
              name="Vid"
              color={item.video === 0 ? COLORS.altgreen_400 : COLORS.dark_800}
              size={22}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.iconStyle}>
            <Badge size={16} style={styles.badgeStyle}>
              {this.numberShortForm(item.projects)}
            </Badge>
            <Icon
              name="Projects_OL"
              color={
                item.projects === 0 ? COLORS.altgreen_400 : COLORS.dark_800
              }
              size={22}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.iconStyle}
            onPress={() =>
              this.props.hashTagDetailNavigation('AllForumHashtags', {
                slug: item.hashtag,
              })
            }>
            <Badge size={16} style={styles.badgeStyle}>
              {this.numberShortForm(item.forum)}
            </Badge>
            <Icon
              name="Forum_F"
              color={item.forum === 0 ? COLORS.altgreen_400 : COLORS.dark_800}
              size={22}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.iconStyle}
            onPress={() =>
              this.props.hashTagDetailNavigation('AllPollHashtag', {
                slug: item.hashtag,
              })
            }>
            <Badge size={16} style={styles.badgeStyle}>
              {this.numberShortForm(item.poll)}
            </Badge>
            <Icon
              name="Polls_FL"
              color={item.poll === 0 ? COLORS.altgreen_400 : COLORS.dark_800}
              size={22}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.iconStyle}>
            <Badge size={16} style={styles.badgeStyle}>
              {this.numberShortForm(item.article)}
            </Badge>
            <Icon
              name="Article"
              color={item.article === 0 ? COLORS.altgreen_400 : COLORS.dark_800}
              size={22}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.iconStyle}>
            <Badge size={16} style={styles.badgeStyle}>
              {this.numberShortForm(item.others)}
            </Badge>
            <Icon
              name="Select_Multiple"
              color={item.others === 0 ? COLORS.altgreen_400 : COLORS.dark_800}
              size={22}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  iconStyle: {
    // paddingHorizontal:10
  },
  badgeStyle: {
    backgroundColor: COLORS.green_500,
    color: COLORS.white,
    marginTop: 3,
    position: 'absolute',
    zIndex: 1,
  },
});
