import React, { Component } from 'react';
import { View, Text,Image } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import manImage from '../../../../assets/manImage.jpeg'
import defaultCover from '../../../../assets/defaultCover.png'
import defaultProfile from '../../../../assets/defaultProfile.png'
import defaultShape from '../../../Components/Shared/Shape';
import typography from '../../../Components/Shared/Typography'
import { COLORS } from '../../../Components/Shared/Colors'
import icoMoonConfig from '../../../../assets/Icons/selection.json'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
export default class TopContriButerItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const {item} = this.props
        return (
            <View style={{ height: 139, width: 148, marginRight: 15 }}>
                <View style={[defaultShape.card8_a, { height: 115, width: 148, position: 'absolute', bottom: 0, zIndex: 1 }]}>
                    <Image source={item.item && item.item.imageUrl ? { uri: item.item.imageUrl } : defaultProfile} style={{ height: 72, width: 72, borderRadius: 36, position: 'absolute', left: 5, top: -25, zIndex: 2 }} />
                    <Image source={item.item && item.item.personalInfo && item.item.personalInfo.coverImage ? { uri: item.item.personalInfo.coverImage } : defaultCover} style={{ height: 42, width: 148, borderTopLeftRadius: 8, borderTopRightRadius: 8 }} />
                    <Text style={[typography.Title_2, { color: '#002D3D', marginLeft: 10, marginTop: 10 }]}>{item.item && item.item.username}</Text>
                    <Text style={[typography.Subtitle_2, { color: COLORS.altgreen_300, marginLeft: 10 }]}>{item.item &&  item.item.persona}</Text>
                    <Text style={[typography.Note_2, { color: COLORS.grey_400, marginLeft: 10 }]}> <Icon name="Location" color={COLORS.altgreen_300} size={10} />{item.item.addressDetail && " "+item.item.addressDetail.country}</Text>
                </View>
            </View>
        );
    }
}
