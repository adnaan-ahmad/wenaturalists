import React, { Component } from 'react'
import { ActivityIndicator, ImageBackground, ScrollView, TextInput, Dimensions, FlatList, View, Text, TouchableOpacity, Image, StyleSheet, Platform, SafeAreaView } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'
import axios from 'axios'
import Snackbar from 'react-native-snackbar'
import AsyncStorage from '@react-native-community/async-storage'

import defaultProfile from '../../../../assets/defaultProfile.png'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultCover from '../../../../assets/defaultCover.png'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultStyle from '../../../Components/Shared/Typography'
import defaultShape from '../../../Components/Shared/Shape'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

const screenHeight = Dimensions.get('window').height

export default class AddCircleMembers extends Component {

    constructor(props) {
        super(props)

        this.state = {
            users: [],
            selectedPage: '',
            name: '',
            searchText: '',
            searchIcon: true,
            userId: ''
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value })


        }).catch((e) => {
            console.log(e)
        })

    }

    searchNames = (query) => {
        if (query === '') {
            return this.props.connectsData
        }

        const regex = new RegExp(`${query.trim()}`, 'i')

        return this.props.connectsData.filter(item => (item.firstName + ' ' + item.lastName).search(regex) >= 0)
    }

    render() {

        // console.log('------------------------------ this.props.selectedMemberList -------------------------------', this.props.selectedMemberList)
        // onPress={() => this.props.changeState({ addCircleMembersModalOpen: false })}

        return (
            <SafeAreaView style={{ backgroundColor: '#F7F7F5', height: '107%', position: 'absolute', bottom: -50, width: '100%' }}>


                <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 12 }] : styles.header}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.changeState({ addCircleMembersModalOpen: false })}>
                                <Icon name="Arrow-Left" size={16} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                            </TouchableOpacity>
                            <Icon name="Circles_Fl" size={18} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'ios' ? 0 : 8, marginLeft: 10 }} />
                            <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_800, marginLeft: 5 }]}>
                                Add Admin
                            </Text>
                        </View>
                        <TouchableOpacity onPress={() => {

                            this.props.selectedMemberList.length >= 5 ?
                                (this.props.handleCircleSubmit(),
                                    this.props.changeState({ addCircleMembersModalOpen: false, createCircleModalOpen: false }))

                                :
                                Snackbar.show({
                                    backgroundColor: '#B22222',
                                    text: 'Please invite 5 members to Continue',
                                    duration: Snackbar.LENGTH_LONG,
                                })
                        }}
                            activeOpacity={0.7} style={[styles.updateButton, { marginTop: 10 }]}>
                            <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_200 }]}>Continue</Text>
                            <Icon name="Arrow_Right" size={13} color={COLORS.altgreen_200} style={{ marginTop: Platform.OS === 'ios' ? 0 : 8, marginLeft: 4 }} />
                        </TouchableOpacity>
                    </View>
                </View>


                <View style={{
                    height: 40, backgroundColor: '#F7F7F5', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginVertical: 6
                }}>

                    <TextInput
                        style={styles.textInput}
                        placeholderTextColor="#D9E1E4"
                        onChangeText={(value) => { this.setState({ searchText: value }) }}
                        color='#154A59'
                        placeholder='Search'
                        onFocus={() => this.setState({ searchIcon: false })}
                        onBlur={() => this.setState({ searchIcon: true })}
                        underlineColorAndroid="transparent"
                        ref={input => { this.textInput = input }}
                    />


                </View>



                {this.props.selectedMemberList.length < 4 ?

                    <View style={{
                        flexDirection: 'row', paddingLeft: 10
                    }}
                        keyboardShouldPersistTaps='handled' horizontal={true}

                    >
                        {this.props.selectedMemberList.map((item) => (
                            <TouchableOpacity onPress={() => this.props.changeState({ selectedMemberList: this.props.selectedMemberList.filter((person) => person !== item) })}
                                key={item.id} activeOpacity={0.6} style={{ height: 32, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', backgroundColor: COLORS.grey_300, borderRadius: 16, paddingRight: 8, marginVertical: 10, marginRight: 8 }} >


                                <ImageBackground source={item.personalInfo.profileImage ? { uri: item.personalInfo.profileImage } : defaultProfile}
                                    imageStyle={{ borderRadius: 15 }}
                                    style={[styles.image, {}]} >

                                </ImageBackground>

                                <Text style={styles.selectedName}>{item.firstName}</Text>

                                <Icon name='Cross' size={10} color={COLORS.grey_400} style={styles.crossIcon} />

                            </TouchableOpacity>))}
                    </View>

                    : this.props.selectedMemberList.length >= 4 ?

                        <View style={{}}>
                            <FlatList
                                keyboardShouldPersistTaps='handled'
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                contentContainerStyle={{ height: 60, paddingLeft: 10, flexGrow: 0 }}
                                // style={{ backgroundColor: 'pink', height: 60 }}
                                keyExtractor={(item) => item.id}
                                data={this.props.selectedMemberList}
                                ref={ref => this.scrollView = ref}
                                onContentSizeChange={() => {
                                    this.scrollView.scrollToEnd({ animated: true })
                                }}
                                renderItem={({ item }) => (

                                    <TouchableOpacity onPress={() => this.props.changeState({ selectedMemberList: this.props.selectedMemberList.filter((person) => person !== item) })}
                                        activeOpacity={0.6} style={{ height: 32, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', backgroundColor: COLORS.grey_300, borderRadius: 16, paddingRight: 8, marginVertical: 10, marginRight: 8 }} >


                                        <ImageBackground source={item.personalInfo.profileImage ? { uri: item.personalInfo.profileImage } : defaultProfile}
                                            imageStyle={{ borderRadius: 15 }}
                                            style={[styles.image, {}]} >

                                        </ImageBackground>

                                        <Text style={styles.selectedName}>{item.firstName}</Text>

                                        <Icon name='Cross' size={10} color={COLORS.grey_400} style={styles.crossIcon} />

                                    </TouchableOpacity>

                                )}
                            />
                        </View>
                        : <></>}

                {/* marginTop: this.props.selectedMemberList.length >= 4 && this.state.searchIcon ? -210 : 0 */}

                <View style={{ backgroundColor: '#E7F3E3', paddingVertical: 6 }}>
                    <Text style={styles.requestEndorsementText}>ADD A MINIMUM OF 5 MEMBERS TO {`\n`} CREATE YOUR CIRCLE</Text>
                </View>




                <View style={{ height: '100%' }}>
                    <FlatList
                        keyboardShouldPersistTaps='handled'
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ paddingBottom: 80 }}
                        style={{ height: '50%' }}
                        keyExtractor={(item) => item.id}
                        data={this.searchNames(this.state.searchText)}
                        initialNumToRender={10}
                        renderItem={({ item }) => (

                            <TouchableOpacity activeOpacity={0.96} style={{}}
                                onPress={() => {
                                    this.props.selectedMemberList.includes(item) ?

                                        this.props.changeState({ selectedMemberList: this.props.selectedMemberList.filter((person) => person !== item) })

                                        : this.props.changeState({ selectedMemberList: [...this.props.selectedMemberList, item] })

                                }} >
                                <View style={styles.item}>

                                    <Image source={item.personalInfo.profileImage ? { uri: item.personalInfo.profileImage } : defaultProfile} style={[styles.image, { marginLeft: '7%' }]} />

                                    <View style={styles.nameMsg}>
                                        <Text style={styles.name}>
                                            {item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1)} {item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1)}
                                        </Text>

                                    </View>
                                    <Text style={styles.time}></Text>

                                    <View style={{ marginRight: 26, width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.green_500, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: this.props.selectedMemberList.includes(item) ? COLORS.green_500 : COLORS.altgreen_t50 }}>
                                        {this.props.selectedMemberList.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>}
                                    </View>

                                </View>

                            </TouchableOpacity>


                        )}
                    />
                </View>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flexDirection: 'column',
        textAlign: 'left',
        fontSize: 15,
        position: "absolute",
        top: 30,
        height: screenHeight,
        backgroundColor: '#fff'
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    updateButton: {
        flexDirection: 'row', width: 110, height: 28, borderRadius: 20, alignItems: 'center', justifyContent: 'center', marginVertical: 10, backgroundColor: COLORS.dark_700
    },
    requestEndorsementText: {
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 12,
        color: '#698F8A',
        textAlign: 'center'
    },
    connectsSelectedText: {
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        color: '#698F8A',
        textAlign: 'center',
        marginVertical: 10
    },
    crossIcon2: {
        marginTop: Platform.OS === 'android' ? 10 : 0
    },
    crossButtonContainer: {
        alignSelf: 'center',
        width: 42,
        height: 42,
        borderRadius: 21,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E7F3E3',
        marginBottom: 10
    },
    linearGradientView2: {
        width: '100%',
        height: 200,
        position: 'absolute',
        top: -50,
        alignSelf: 'center'
    },
    linearGradient2: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6
    },
    sendRequest: {
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 15,
        color: '#E7F3E3',
        marginLeft: 6
    },
    floatingIcon: {
        //flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: 175,
        height: 39,
        borderRadius: 28,
        backgroundColor: '#367681',
        position: 'absolute',
        //top: 416,
        bottom: 66,
        right: 12,
    },
    editIcon: {
        alignSelf: 'center'
    },
    crossIcon: {
        marginTop: Platform.OS === 'android' ? 6 : 0
        // position: 'absolute',
        // right: -10,
        // bottom: -16,
        // alignSelf: 'flex-end'
    },
    selectedName: {
        fontSize: 11,
        color: '#154A59',
        textAlign: 'center',
        marginHorizontal: 6,
        fontFamily: 'Montserrat-Medium'
    },
    searchIcon: {
        position: 'absolute',
        left: 136,
        top: 15,
        // backgroundColor: 'red',
        zIndex: 2
    },
    border: {
        borderWidth: 1,
        marginRight: '6%',
        width: '95%',
        marginTop: '-1%',
        borderColor: '#91B3A2',
        alignSelf: 'center',
        borderRadius: 1,
    },
    textInput: {
        fontSize: 17,
        fontFamily: 'Montserrat-Medium',
        padding: 10,
        backgroundColor: '#FFF',
        width: '90%',
        zIndex: 1,
        textAlign: 'center',
        borderRadius: 8,
        borderColor: '#D9E1E4',
        borderWidth: 1,
        shadowColor: '#36768140',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    tabNavigator: {
        marginTop: '-8%',
    },

    item: {
        // flex: 1,
        flexDirection: 'row',
        alignItems: 'space-around',
        backgroundColor: '#F7F7F500',
        width: '100%',
        height: 48,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',

        // borderBottomWidth: 1,
        //borderBottomRadius: 1,
        // borderBottomColor: '#E2E7E9',
        // borderRadius: 10
    },
    image: {
        height: 28,
        width: 28,
        borderRadius: 14,
        //marginLeft: '7%',
    },
    name: {
        fontWeight: '700',
        color: '#4B4F56',
        fontSize: 14,
        textAlign: 'left',
        // marginLeft: '-6%',
    },
    message: {
        color: '#698F8A',
        fontSize: 10.5,
    },

    // --- New ---

    time: {
        color: '#AABCC3',
        fontSize: 10,
        marginRight: '10%'
    },
    nameMsg: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: '6%',
        //textAlign: 'left'
    },
    imageGroup: {
        height: 26,
        width: 26,
        borderRadius: 13,
    },
})
