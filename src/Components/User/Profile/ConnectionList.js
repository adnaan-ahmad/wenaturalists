import React, { Component } from 'react'
import { Pressable, Modal, ScrollView, TextInput, Dimensions, FlatList, View, Text, TouchableOpacity, Image, StyleSheet, Platform, SafeAreaView } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'
import axios from 'axios'
import Snackbar from 'react-native-snackbar'
import AsyncStorage from '@react-native-community/async-storage'

import defaultProfile from '../../../../assets/defaultProfile.png'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultCover from '../../../../assets/defaultCover.png'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import defaultShape from '../../../Components/Shared/Shape'
import typography from '../../../Components/Shared/Typography'
import { COLORS } from '../../../Components/Shared/Colors'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

const screenHeight = Dimensions.get('window').height

export default class ConnectionList extends Component {

  constructor(props) {
    super(props)

    this.state = {
      users: [],
      selectedPage: '',
      name: '',
      searchText: '',
      selectedContactsState: [],
      searchIcon: true,
      userId: '',
      optionsModalOpen: false,
      pressedUserId: '',
      following: 0,
      isConnected: '',
      reasonForReportingModalOpen: false,
      reasonForReporting: 'FAKE_SPAM_OR_SCAM',
      description: ''
    }
  }

  componentDidMount() {

    AsyncStorage.getItem("refreshToken").then((value) => {
      if (value === null) {
        this.props.navigation.replace("Login")
      }
    })

    AsyncStorage.getItem("userId").then((value) => {
      this.setState({ userId: value })


    }).catch((e) => {
      console.log(e)
    })

  }

  searchNames = (query) => {
    if (query === '') {
      return this.props.mutualConnects.length ? this.props.mutualConnects : this.props.followers
    }

    const regex = new RegExp(`${query.trim()}`, 'i')

    return this.props.mutualConnects.length ? this.props.mutualConnects.filter(item => (item.firstName + ' ' + item.lastName).search(regex) >= 0)
      : this.props.followers.filter(item => (item.firstName + ' ' + item.lastName).search(regex) >= 0)
  }

  getFollowers = () => {
    axios({
      method: "get",
      url:
        REACT_APP_userServiceURL +
        "/network/" +
        this.state.pressedUserId +
        "/followers" +
        "?page=" +
        0 +
        "&size=" +
        1000,
      headers: { "Content-Type": "application/json" },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === "Success!") {
          // console.log('```````````````````` Followers ````````````````````', response.data.body.page.content.filter((item) => item.profileId === this.state.userId).length)
          this.setState({ following: response.data.body.page.content.filter((item) => item.profileId === this.state.userId).length },
            () => this.handleFollowUnfollow())

        }
      })
      .catch((err) => {
        console.log(err)
      })
  }

  handleFollowUnfollow = () => {

    let url
    if (this.state.following === 0) {
      url = REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/follows/' + this.state.pressedUserId
    } else {
      url = REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/unfollows/' + this.state.pressedUserId
    }
    axios({
      method: 'post',
      url: url,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.status === 202) {
        console.log(response.status)
        // this.getFollowers()
        // this.props.personalConnectionInfoRequest({ userId: this.state.pressedUserId })
      } else {
        console.log(response)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  handleConnectStatusChange = () => {

    let url
    if (this.state.isConnected === 'NOT_CONNECTED') {
      url = REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/invite/' + this.state.pressedUserId;
    } else if (this.state.isConnected === 'PENDING_CONNECT') {
      url = REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/removeInvite/' + this.state.pressedUserId
    } else if (this.state.isConnected === 'CONNECTED') {
      url = REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/unconnect/' + this.state.pressedUserId
    }
    else if (this.state.isConnected === 'DEADLOCK_PENDING_STATUS' || this.state.isConnected === 'IGNORE_PENDING_CONNECT') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: "Error sending request to this member",
        duration: Snackbar.LENGTH_LONG,
      })
    }

    if (url && url !== '') {
      axios({
        method: 'post',
        url: url,
        headers: { 'Content-Type': 'application/json' },
        withCredentials: true
      }).then(response => {
        if (response && response.status === 202) {
          console.log(response.status)
          //   this.getConnectStatus()
          //   this.props.personalConnectionInfoRequest({ userId: this.state.pressedUserId })
        }
      }).catch((err) => {
        console.log(err)
        if (err.message === 'Request failed with status code 409') {
          Snackbar.show({
            backgroundColor: '#B22222',
            text: "You can send connection request after 3 days to this member",
            duration: Snackbar.LENGTH_LONG,
          })
        }
        if (err.message === 'Request failed with status code 400') {
          Snackbar.show({
            backgroundColor: '#B22222',
            text: "Cannot send request to an organization",
            duration: Snackbar.LENGTH_LONG,
          })
        }
      })
    }
  }

  getConnectStatus = () => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + "/graph/users/" + this.state.userId + "/connectionStatus/" + this.state.pressedUserId,
      withCredentials: true,
      headers: { "Content-Type": "application/json" },
    }).then((response) => {
      if (response && response.status === 200) {
        console.log(response.data.body.connectStatus)
        this.setState({ isConnected: response.data.body.connectStatus }, () => this.handleConnectStatusChange())
      }
    }).catch((e) => {
      console.log(e)
    })
  }

  handleReportAbuseSubmit = () => {

    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedUserId,
      // entityType: this.state.entityType,
      entityType: 'USER',
      reason: this.state.reasonForReporting,
      description: this.state.description
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true
    }).then(response => {
      this.setState({ reasonForReporting: this.state.reasonForReporting })
      if (response && response.status === 201) {
        Snackbar.show({
          backgroundColor: COLORS.primarydark,
          text: "Your request has been taken and appropriate action will be taken as per our report abuse policy",
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        })
        // console.log(response)
      } else {
        // console.log(response)

      }
    }).catch((err) => {
      this.setState({ reasonForReporting: this.state.reasonForReporting })
      if (err && err.response && err.response.status === 409) {
        // console.log(err.response)
        Snackbar.show({
          backgroundColor: COLORS.primarydark,
          text: "Your report request was already taken",
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        })

      } else {
        // console.log(err)
        Snackbar.show({
          backgroundColor: '#B22222',
          text: "Please check your network or try again later",
          duration: Snackbar.LENGTH_LONG,
        })
      }
    })
  }

  reasonForReportingModal = () => {
    return (

      <Modal visible={this.state.reasonForReportingModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>

          <View style={[defaultShape.Linear_Gradient_View, { height: 700, bottom: 0 }]}>
            <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={[defaultShape.CloseBtn, { marginBottom: 0 }]}
            onPress={() => this.setState({ reasonForReportingModalOpen: false })} >
            <Icon name='Cross' size={13} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }} />
          </TouchableOpacity>


          <View style={{
            borderRadius: 20,
            backgroundColor: COLORS.altgreen_100,
            alignItems: 'center',
            paddingTop: 15,
            paddingBottom: 10,
            // paddingLeft: 20,
            width: '90%',
            alignSelf: 'center',
            marginBottom: 30,
            marginTop: 15
          }}>

            <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Reason for reporting</Text>
            </View>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, borderBottomWidth: 0 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'FAKE_SPAM_OR_SCAM' }) }} >

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                  {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                </View>
                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Fake, spam or scam</Text>
              </View>


            </TouchableOpacity>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'ACCOUNT_MAY_BE_HACKED' }) }} >

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                  {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                </View>
                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Account may be hacked</Text>
              </View>

            </TouchableOpacity>
            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'IMPERSONATING_SOMEONE' }) }} >

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                  {this.state.reasonForReporting === 'IMPERSONATING_SOMEONE' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                </View>
                <Text numberOfLines={2} style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Impersonating someone</Text>
              </View>

            </TouchableOpacity>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'VIOLATES_TERMS_OF_USE' }) }} >

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                  {this.state.reasonForReporting === 'VIOLATES_TERMS_OF_USE' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                </View>
                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Violates Terms Of Use</Text>
              </View>

            </TouchableOpacity>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'OTHERS', description: 'OTHERS' }) }} >

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                  {this.state.reasonForReporting === 'OTHERS' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                </View>
                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Others</Text>
              </View>

            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.5}
              onPress={() => { this.handleReportAbuseSubmit(), this.setState({ reasonForReportingModalOpen: false }) }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 15,
                height: 27,
                marginVertical: 10,
                borderRadius: 16,
                textAlign: 'center',
                borderWidth: 1,
                borderColor: '#698F8A'
              }}>
              <Text style={{
                color: '#698F8A',
                fontSize: 14,
                paddingHorizontal: 14,
                paddingVertical: 20,
                fontFamily: 'Montserrat-Medium',
                fontWeight: 'bold'
              }}>Submit</Text>
            </TouchableOpacity>

          </View>
        </View>

      </Modal>

    )
  }

  optionsModal = () => {
    return (

      <Modal visible={this.state.optionsModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>

          <View style={[defaultShape.Linear_Gradient_View, { bottom: 0 }]}>
            <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
            onPress={() => this.setState({ optionsModalOpen: false })} >
            <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
          </TouchableOpacity>

          {this.props.currentTab === 'connects' ?
            <View style={defaultShape.Modal_Categories_Container}>

              <TouchableOpacity
                style={[defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, justifyContent: 'center', borderBottomWidth: 0 }]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false }, () => this.getConnectStatus()) }} >
                <Text style={[typography.H5, { color: COLORS.dark_900 }]}>Remove Connect</Text>

              </TouchableOpacity>

            </View>
            :
            this.props.currentTab === 'following' || this.props.currentTab === 'followers' ?
              <View style={defaultShape.Modal_Categories_Container}>

                <TouchableOpacity
                  style={[defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, justifyContent: 'center' }]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false }, () => this.getFollowers()) }} >
                  <Text style={[typography.H5, { color: COLORS.dark_900 }]}>Unfollow</Text>

                </TouchableOpacity>

                <TouchableOpacity style={[[defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, justifyContent: 'center', borderBottomWidth: 0 }]]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false }, () => this.getConnectStatus()) }}>
                  <Text style={[typography.H5, { color: COLORS.dark_900 }]}>Connect</Text>

                </TouchableOpacity>

              </View>
              :
              this.props.currentTab === 'mutualConnects' ?
                <View style={defaultShape.Modal_Categories_Container}>

                  <TouchableOpacity
                    style={[defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, justifyContent: 'center' }]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false }, () => this.getFollowers()) }} >
                    <Text style={[typography.H5, { color: COLORS.dark_900 }]}>Unfollow</Text>

                  </TouchableOpacity>

                  <TouchableOpacity style={[[defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, justifyContent: 'center', borderBottomWidth: 0 }]]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false }, () => this.getConnectStatus()) }}>
                    <Text style={[typography.H5, { color: COLORS.dark_900 }]}>Remove Connect</Text>

                  </TouchableOpacity>

                </View>
                :
                <></>}

        </View>

      </Modal>

    )
  }

  render() {
    console.log('------------------------------ this.props.followers[0] -------------------------------', this.props.followers[0])
    return (

      <SafeAreaView style={{ height: '100%', paddingTop: '33%' }}>

        {this.optionsModal()}
        {this.reasonForReportingModal()}

        <View style={{
          height: 60, backgroundColor: '#F7F7F5', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: -1
        }}>

          <TextInput
            style={styles.textInput}
            placeholderTextColor="#D9E1E4"
            onChangeText={(value) => { this.setState({ searchText: value }) }}
            color='#154A59'
            placeholder='Search'
            onFocus={() => this.setState({ searchIcon: false })}
            onBlur={() => this.setState({ searchIcon: true })}
            underlineColorAndroid="transparent"
            ref={input => { this.textInput = input }}
          />


        </View>


        <FlatList
          keyboardShouldPersistTaps='handled'
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 80, paddingTop: 20 }}
          style={{ height: '50%' }}
          keyExtractor={(item) => item.id}
          data={this.searchNames(this.state.searchText)}
          initialNumToRender={10}
          renderItem={({ item }) => (

            <TouchableOpacity activeOpacity={0.6} >
              <View style={styles.item}>

                <Pressable style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} onPress={() => 
                this.props.changeState({ navigateToUserId: item.id, navigate: true })
                // console.log(item)
                }>
                  <Image source={item.personalInfo.profileImage ? { uri: item.personalInfo.profileImage } : defaultProfile} style={[styles.image, { marginLeft: '7%' }]} />

                  <TouchableOpacity
                    onPress={() => this.props.changeState({ navigateToUserId: item.id, navigate: true })}
                    style={styles.nameMsg}>
                    
                    <Text style={styles.name}>{item.firstName.charAt(0).toUpperCase() + item.firstName.slice(1)} {item.lastName.charAt(0).toUpperCase() + item.lastName.slice(1)} <Text style={[{ color: '#888', fontSize: 14 }, typography.Note2]}>
                    {
                      item.connectDepth === 1 ? '• 1st' :
                        item.connectDepth === 2 ? '• 2nd' :
                          item.connectDepth === -1 || item.connectDepth === 0 ? '' : ""
                    }
                  </Text></Text>
                    {item.personalInfo && item.personalInfo.persona ?
                      
                      <Text style={[typography.Caption, { color: '#698f8a' }]}>{item.personalInfo.persona.charAt(0).toUpperCase() + item.personalInfo.persona.slice(1)}</Text> :
                      <></>}
                      {item.addressDetail && item.addressDetail.country ?
                      
                      <Text style={[typography.Caption, { color: '#99b2bf' }]}>{item.addressDetail.country}</Text> :
                      <></>}
                      
                  </TouchableOpacity>

                </Pressable>

                {/* <TouchableOpacity style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: COLORS.altgreen_200, justifyContent: 'center', alignItems: 'center', marginRight: 6 }} activeOpacity={0.6} >
                  <Icon name='Messeges_F' size={14} color={COLORS.altgreen_300} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                </TouchableOpacity> */}

                <TouchableOpacity onPress={() => this.setState({ optionsModalOpen: true, pressedUserId: item.id })}
                  style={{ width: 34, height: 34, justifyContent: 'center', alignItems: 'center', position: 'absolute', right: 16 }} activeOpacity={0.5} >
                  <Icon name='Kebab' size={14} color={COLORS.grey_350} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                </TouchableOpacity>

              </View>

            </TouchableOpacity>


          )}
        />

      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({

  container: {
    flexDirection: 'column',
    textAlign: 'left',
    fontSize: 15,
    position: "absolute",
    top: 30,
    height: screenHeight,
    backgroundColor: '#fff'
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0
  },
  requestEndorsementText: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 14,
    color: '#154A59',
    textAlign: 'center'
  },
  connectsSelectedText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: '#698F8A',
    textAlign: 'center',
    marginVertical: 10
  },
  crossButtonContainer: {
    alignSelf: 'center',
    width: 42,
    height: 42,
    borderRadius: 21,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E7F3E3',
    marginBottom: 10
  },
  linearGradientView2: {
    width: '100%',
    height: 200,
    position: 'absolute',
    top: -50,
    alignSelf: 'center'
  },
  linearGradient2: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6
  },
  sendRequest: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 15,
    color: '#E7F3E3',
    marginLeft: 6
  },
  floatingIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 175,
    height: 39,
    borderRadius: 28,
    backgroundColor: '#367681',
    position: 'absolute',
    bottom: 66,
    right: 12,
  },
  editIcon: {
    alignSelf: 'center'
  },
  selectedName: {
    fontSize: 11,
    color: '#154A59',
    textAlign: 'center',
    fontFamily: 'Montserrat-Medium'
  },
  searchIcon: {
    position: 'absolute',
    left: 136,
    top: 15,
    zIndex: 2
  },
  border: {
    borderWidth: 1,
    marginRight: '6%',
    width: '95%',
    marginTop: '-1%',
    borderColor: '#91B3A2',
    alignSelf: 'center',
    borderRadius: 1,
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '90%',
    zIndex: 1,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5
  },
  tabNavigator: {
    marginTop: '-8%',
  },


  // --- Assignment ---

  item: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'space-around',
    backgroundColor: '#F7F7F500',
    width: '100%',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'center',
    marginBottom: 16
  },
  image: {
    height: 32,
    width: 32,
    borderRadius: 16
  },
  name: {
    fontWeight: '700',
    color: '#4B4F56',
    fontSize: 14,
    textAlign: 'left'
  },
  message: {
    color: '#698F8A',
    fontSize: 10.5,
  },

  // --- New ---

  time: {
    color: '#AABCC3',
    fontSize: 10,
    marginRight: '10%'
  },
  nameMsg: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: '6%'
  },
  imageGroup: {
    height: 26,
    width: 26,
    borderRadius: 13,
  },
})
