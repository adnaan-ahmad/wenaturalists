import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import typography from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class ProfileEditHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={styles.headerView}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.goback()}
                        style={defaultShape.Nav_Gylph_Btn}>
                        <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
                    </TouchableOpacity>
                    <Icon name={this.props.iconName} size={18} color={COLORS.dark_600} style={{ marginLeft: 20, marginTop: 5 }} />
                    <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 10, marginTop: Platform.OS === 'android' ? -5 : 0 }]}>
                        {this.props.name}
                    </Text>
                </View>

                {this.props.updateButton &&
                    <TouchableOpacity onPress={() => this.props.updateButtonFunction()}
                        style={[defaultShape.ContextBtn_FL_Drk, { alignSelf: 'center', marginRight: 20, width: 76 }]}>
                        <Text style={[typography.Caption, { color: COLORS.altgreen_200 }]}>Update</Text>
                    </TouchableOpacity>}

                    {this.props.addButton &&
                    <TouchableOpacity onPress={() => this.props.addButtonFunction()}
                        style={[defaultShape.Nav_Gylph_Btn, { alignSelf: 'center', marginRight: 20 }]}>
                        <Icon name="AddList" color={COLORS.altgreen_300} size={16} />
                    </TouchableOpacity>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 54,
        marginBottom: 5,
        backgroundColor: COLORS.white,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    }
})
