import React, { Component } from 'react';
import { View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Svg, {
  Image,
  Polygon,
  Defs,
  ClipPath
} from 'react-native-svg'
import httpService from '../../../services/AxiosInterceptors'
import defaultProfile from '../../../../assets/defaultProfile.png'

httpService.setupInterceptors()

class OtherProfileSvg extends Component {

    constructor(props) {
      super(props);
      this.state = {

      }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
          if (value === null) {
            this.props.navigation.replace("Login")
          }
        })
    
        AsyncStorage.getItem("userId").then((value) => {

        }).catch((e) => {
          console.log(e)
        })
    
      }

      shouldComponentUpdate(nextProps, nextState) {

        if (nextProps.user !== this.props.user) {
          return true
        }
    
        return false
      }

    render() {
        return (
            <View>

                <Svg height="125" width="110">

                  <Defs>
                    <ClipPath id="clip">
                      <Polygon
                        points="47 0, 0 27, 0 81, 68 123, 63 97, 95 81, 95 27"
                        fill="transparent"
                        stroke="white"
                        strokeWidth="3"
                        zIndex={2}
                        position='absolute'
                      />
                    </ClipPath>
                  </Defs>

                  {(this.props.profileImg) ?
                    <Image
                      width="90%"
                      height="100%"
                      preserveAspectRatio="xMidYMid slice"
                      opacity="1"
                      href={this.props.profileImg}
                      clipPath="url(#clip)"
                      zIndex={1}
                      borderColor="white"
                      borderWidth={3}
                      marginTop={-20}
                    /> :
                    <Image
                      width="90%"
                      height="100%"
                      preserveAspectRatio="xMidYMid slice"
                      opacity="1"
                      href={defaultProfile}
                      clipPath="url(#clip)"
                      zIndex={1}
                      borderColor="white"
                      borderWidth={3}
                      marginTop={-20}
                    />}

                  <Polygon
                    points="47 0, 0 27, 0 81, 68 123, 63 97, 95 81, 95 27"
                    fill="transparent"
                    stroke="white"
                    strokeWidth="3"
                    zIndex={2}
                  />

                </Svg>
              </View>
        )
    }

}

export default OtherProfileSvg
