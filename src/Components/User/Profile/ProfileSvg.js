import React, { Component } from 'react';
import { View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
// import { connect } from 'react-redux'
import Svg, {
  Image,
  Polygon,
  Defs,
  ClipPath
} from 'react-native-svg'
import httpService from '../../../services/AxiosInterceptors'
// import { personalProfileRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions'
import defaultProfile from '../../../../assets/defaultProfile.png'

httpService.setupInterceptors()

class ProfileSvg extends Component {

    constructor(props) {
      super(props);
      this.state = {

      }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
          if (value === null) {
            this.props.navigation.replace("Login")
          }
        })
    
        AsyncStorage.getItem("userId").then((value) => {

          // this.props.personalProfileRequest({ userId: value, otherUserId: '' })
        }).catch((e) => {
          console.log(e)
        })
    
      }

      // shouldComponentUpdate(nextProps, nextState) {

      //   // if (nextProps.user !== this.props.user) {
      //   //   return true
      //   // }
    
      //   return false
      // }

    render() {
        return (
            <View>

                <Svg height="125" width="110">

                  <Defs>
                    <ClipPath id="clip">
                      <Polygon
                        points="47 0, 0 27, 0 81, 68 123, 63 97, 95 81, 95 27"
                        fill="transparent"
                        stroke="white"
                        strokeWidth="3"
                        zIndex={2}
                        position='absolute'
                      />
                    </ClipPath>
                  </Defs>

                  {/* {(this.props.user.body !== undefined && this.props.user.body.originalProfileImage !== null) ?
                    <Image
                      width="100%"
                      height="100%"
                      preserveAspectRatio="xMidYMid slice"
                      opacity="1"
                      href={this.props.user.body.originalProfileImage}
                      clipPath="url(#clip)"
                      zIndex={1}
                      borderColor="white"
                      borderWidth={3}
                      marginTop={-20}
                    /> : */}
                    {(this.props.profileImg) ?
                    <Image
                      width="90%"
                      height="100%"
                      preserveAspectRatio="xMidYMid slice"
                      opacity="1"
                      href={this.props.profileImg}
                      clipPath="url(#clip)"
                      zIndex={1}
                      borderColor="white"
                      borderWidth={3}
                      marginTop={-20}
                    /> :
                    <Image
                      width="90%"
                      height="100%"
                      preserveAspectRatio="xMidYMid slice"
                      opacity="1"
                      href={defaultProfile}
                      clipPath="url(#clip)"
                      zIndex={1}
                      borderColor="white"
                      borderWidth={3}
                      marginTop={-20}
                    />}

                  <Polygon
                    points="47 0, 0 27, 0 81, 68 123, 63 97, 95 81, 95 27"
                    fill="transparent"
                    stroke="white"
                    strokeWidth="3"
                    zIndex={2}
                  />

                </Svg>
              </View>
        )
    }

}

// const mapStateToProps = (state) => {
//     return {
//       userDataProgress: state.personalProfileReducer.userDataProgress,
//       user: state.personalProfileReducer.user,
//       error: state.personalProfileReducer.error,
//     }
// }

// const mapDispatchToProps = (dispatch) => {

//     return {
//       personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
//     }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(ProfileSvg)

export default ProfileSvg
