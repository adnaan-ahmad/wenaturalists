import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform,
  Modal,
  SafeAreaView,
  TextInput,
  FlatList,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import messaging from '@react-native-firebase/messaging';
import axios from 'axios';
import _ from 'lodash';

import {COLORS} from '../Shared/Colors';
import typography from '../Shared/Typography';
import defaultShape from '../Shared/Shape';
import {REACT_APP_userServiceURL} from '../../../env.json';
import httpService from '../../services/AxiosInterceptors';
import {personalProfileRequest} from '../../services/Redux/Actions/User/PersonalProfileActions';
import {trimDescription} from '../Shared/commonFunction';
import defaultProfile from '../../../assets/defaultProfile.png';
import BusinessDefault from '../../../assets/BusinessDefault.png';
import CirclesDefault from '../../../assets/CirclesDefault.png';
import projectDefault from '../../../assets/project-default.jpg';
import icoMoonConfig from '../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
httpService.setupInterceptors();
const moment = require('moment');
const searchTypeData = [
  'PEOPLE',
  'CAUSES',
  'PROJECTS',
  'ORGANIZATION',
  'EXPLORE',
  'CIRCLE',
  'FEEDS',
];

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notificationCount: 0,
      page: 0,
      userId: '',
      profileimg: '',
      searchModalOpen: false,
      searchDescription: '',
      searchType: 'PEOPLE',
      recentSearchList: [],
      searchResponseList: [],
      totalElements: 0,
      searchPlatform: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        value &&
          this.setState({userId: value}, () => this.getRecentSearchList());

        this.getProfileData(value);
        this.getNotificationsCount(value);

        try {
          messaging().onMessage(async (remoteMessage) => {
            this.getNotificationsCount(value);
          });
        } catch (error) {
          console.log(error);
        }
      })
      .catch((e) => {
        console.log(e);
      });

    this.changeTextDebouncer = _.debounce(this.changeTextDebounced, 500);
  }

  changeTextDebounced = () => {
    this.state.searchDescription !== '' ? this.searchSuggestion() : null;
  };

  getProfileData = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get?id=' +
        userId +
        '&otherUserId=' +
        '',
      cache: true,
      withCredentials: true,
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        this.setState({
          profileimg: response.data.body.profileImage,
        });
      })
      .catch((err) => console.log('Profile data error : ', err));
  };

  getNotificationsCount(id) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/notification/notificationCount?userId=' +
        id,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        this.setState({notificationCount: response.data.body});
      })
      .catch((err) => {
        console.log(err);
      });
  }

  markAllNotificationAsRead = (id) => {
    this.props.navigateNotification('Notification');
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.user !== this.props.user) {
      return true;
    }

    if (nextState.notificationCount !== this.state.notificationCount) {
      return true;
    }
    if (nextState.userId !== this.state.userId) {
      return true;
    }
    if (nextState.profileimg !== this.state.profileimg) {
      return true;
    }
    if (nextState.searchModalOpen !== this.state.searchModalOpen) {
      return true;
    }
    if (nextState.searchDescription !== this.state.searchDescription) {
      return true;
    }
    if (nextState.searchType !== this.state.searchType) {
      return true;
    }
    if (nextState.recentSearchList !== this.state.recentSearchList) {
      return true;
    }
    if (nextState.searchResponseList !== this.state.searchResponseList) {
      return true;
    }
    if (nextState.page !== this.state.page) {
      return true;
    }
    if (nextState.totalElements !== this.state.totalElements) {
      return true;
    }
    if (nextState.searchPlatform !== this.state.searchPlatform) {
      return true;
    }

    return false;
  }

  getRecentSearchList = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/search/' +
        this.state.userId +
        '/recentSearches?size=3&searchType=' +
        this.state.searchType,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({recentSearchList: response.data.body});
      })
      .catch((err) => console.log('recent search error: ', err));
  };

  recentSearchListBeforeNavigation = (value, params) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/search/' +
        this.state.userId +
        '/recentSearches?size=3&searchType=' +
        this.state.searchType,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({recentSearchList: response.data.body}, () =>
          this.props.navigation(value, params),
        );
      })
      .catch((err) => console.log('recent search error: ', err));
  };

  searchInPlatform = () => {
    let postBody = {
      searchKeyword: this.state.searchDescription.trim(),
      searchType: this.state.searchType,
      latitude: 0,
      longitude: 0,
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/search/' +
        this.state.userId +
        '?page=' +
        this.state.page +
        '&size=10',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          searchResponseList: this.state.searchResponseList.concat(
            response.data.body.content,
          ),
          totalElements: response.data.body.page.totalElements,
        });
      })
      .catch((err) => console.log('search response error: ', err));
  };

  saveToRecentOnClickItem = (value, params) => {
    let postBody = {
      searchKeyword: this.state.searchDescription.trim(),
      searchType: this.state.searchType,
      latitude: 0,
      longitude: 0,
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/search/' +
        this.state.userId +
        '?page=' +
        this.state.page +
        '&size=10',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({searchType: 'PEOPLE', searchDescription: ''}, () =>
          this.recentSearchListBeforeNavigation(value, params),
        );
      })
      .catch((err) => console.log('search response error: ', err));
  };

  searchSuggestion = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/search/suggestions/' +
        this.state.userId +
        '?query=' +
        this.state.searchDescription +
        '&searchType=' +
        this.state.searchType +
        '&page=' +
        this.state.page +
        '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          searchResponseList: this.state.searchResponseList.concat(
            response.data.body.content,
          ),
          totalElements: response.data.body.page.totalElements,
        });
      })
      .catch((err) => console.log('search response error: ', err));
  };

  handleLoadMore = () => {
    this.setState({page: this.state.page + 1}, () => this.searchSuggestion());
  };

  handleLoadMoreSearchInPlatform = () => {
    this.setState({page: this.state.page + 1}, () => this.searchInPlatform());
  };

  searchModal = () => {
    return (
      <Modal
        visible={this.state.searchModalOpen}
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <SafeAreaView style={{flex: 1}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
              borderBottomColor: COLORS.grey_300,
              borderBottomWidth: 0.3,
              paddingBottom: 8,
            }}>
            <TouchableOpacity
              onPress={() =>
                this.setState(
                  {
                    searchModalOpen: false,
                    searchDescription: '',
                    page: 0,
                    searchResponseList: [],
                    searchType: 'PEOPLE',
                  },
                  () => this.getRecentSearchList(),
                )
              }
              style={{
                height: 36,
                width: 36,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon name="Arrow_Left" size={18} color={COLORS.dark_800} />
            </TouchableOpacity>
            <View
              style={[
                styles.searchBar,
                {
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  width: '85%',
                  marginLeft: 5,
                },
              ]}>
              <TextInput
                placeholder="search in WeNaturalists"
                autoFocus
                style={[
                  // styles.searchBar,
                  {
                    width: '85%',
                    alignSelf: 'center',
                  },
                ]}
                onChangeText={(value) =>
                  this.setState(
                    {
                      searchDescription: value,
                      page: 0,
                      searchResponseList: [],
                      searchPlatform: false,
                    },
                    () => {
                      this.changeTextDebouncer();
                    },
                  )
                }
                value={this.state.searchDescription}
              />
              <TouchableOpacity
                onPress={() =>
                  this.setState(
                    {page: 0, searchPlatform: true, searchResponseList: []},
                    () => this.searchInPlatform(),
                  )
                }
                style={{
                  height: 30,
                  width: 30,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Icon name="Search" size={16} color={COLORS.grey_400} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.searchSuggestionCard}>
            <View
              style={{
                backgroundColor: COLORS.green_200,
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
              }}>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_800, marginTop: 10, marginLeft: 10},
                ]}>
                RECENT SEARCHES
              </Text>
              {this.state.recentSearchList.length > 0 ? (
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  keyboardShouldPersistTaps="handled"
                  data={this.state.recentSearchList}
                  keyExtractor={(item) => item}
                  renderItem={(item) => (
                    <TouchableOpacity
                      onPress={() =>
                        this.setState(
                          {
                            searchDescription: item.item,
                            page: 0,
                            searchResponseList: [],
                          },
                          () => this.searchInPlatform(),
                        )
                      }
                      style={{
                        paddingHorizontal: 12,
                        paddingVertical: 4,
                        borderRadius: 8,
                        borderWidth: 1,
                        borderColor: COLORS.green_500,
                        backgroundColor:
                          item.item === this.state.searchType
                            ? COLORS.dark_800
                            : COLORS.white,
                        marginVertical: 6,
                        marginLeft: 10,
                      }}>
                      <Text
                        style={[
                          typography.Note2,
                          {
                            color:
                              item.item === this.state.searchType
                                ? COLORS.altgreen_100
                                : COLORS.dark_600,
                          },
                        ]}>
                        {item.item}
                      </Text>
                    </TouchableOpacity>
                  )}
                />
              ) : (
                <Text
                  style={[
                    typography.Caption,
                    {color: COLORS.dark_600, marginLeft: 10, marginVertical: 8},
                  ]}>
                  No recent searches
                </Text>
              )}
            </View>

            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              keyboardShouldPersistTaps="handled"
              style={{paddingVertical: 6}}
              data={searchTypeData}
              keyExtractor={(item) => item}
              renderItem={(item) => (
                <TouchableOpacity
                  onPress={() =>
                    this.setState(
                      {
                        searchType:
                          item.item === 'ORGANIZATION'
                            ? 'COMPANY'
                            : item.item === 'EXPLORE'
                            ? 'ARTICLES'
                            : item.item,
                        page: 0,
                        searchResponseList: [],
                        totalElements: 0,
                      },
                      () => this.getRecentSearchList(),
                    )
                  }
                  style={{
                    paddingHorizontal: 12,
                    paddingVertical: 4,
                    borderRadius: 20,
                    borderWidth: 1,
                    borderColor: COLORS.green_500,
                    backgroundColor:
                      item.item === this.state.searchType ||
                      (item.item === 'EXPLORE' &&
                        this.state.searchType === 'ARTICLES') ||
                      (item.item === 'ORGANIZATION' &&
                        this.state.searchType === 'COMPANY')
                        ? COLORS.dark_800
                        : COLORS.altgreen_100,
                    marginVertical: 6,
                    marginLeft: 10,
                  }}>
                  <Text
                    style={[
                      typography.Caption,
                      {
                        color:
                          item.item === this.state.searchType ||
                          (item.item === 'EXPLORE' &&
                            this.state.searchType === 'ARTICLES') ||
                          (item.item === 'ORGANIZATION' &&
                            this.state.searchType === 'COMPANY')
                            ? COLORS.altgreen_100
                            : COLORS.dark_600,
                        textTransform: 'capitalize',
                      },
                    ]}>
                    {item.item}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>

          <FlatList
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled"
            style={{paddingVertical: 6}}
            data={this.state.searchResponseList.filter(
              (value, index) =>
                this.state.searchResponseList.indexOf(value) === index,
            )}
            onEndReached={
              this.state.searchPlatform
                ? this.handleLoadMoreSearchInPlatform
                : this.handleLoadMore
            }
            onEndReachedThreshold={1}
            ListHeaderComponent={
              this.state.searchDescription !== '' ? (
                <Text
                  style={[
                    typography.Button_Lead,
                    {
                      color: COLORS.altgreen_400,
                      marginVertical: 5,
                      marginLeft: 15,
                    },
                  ]}>
                  {this.state.totalElements +
                    `${this.state.totalElements > 1 ? ' Results' : ' Result'}`}
                </Text>
              ) : null
            }
            keyExtractor={(item) =>
              item.id ? item.id : item.projectId ? item.projectId : item.userId
            }
            renderItem={(item) => (
              <View
                style={{
                  paddingHorizontal: 12,
                  paddingVertical: 4,
                  marginVertical: 6,
                  marginLeft: 10,
                  flexDirection: 'row',
                }}>
                {this.state.searchType === 'PEOPLE' ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.setState(
                        {
                          searchModalOpen: false,
                          searchDescription:
                            item.item.userName ||
                            item.item.firstName + ' ' + item.item.lastName,
                          page: 0,
                          searchResponseList: [],
                          searchType: 'PEOPLE',
                        },
                        () => {
                          this.saveToRecentOnClickItem('ProfileStack', {
                            screen: 'OtherProfileScreen',
                            params: {userId: item.item.userId},
                          });
                        },
                      )
                    }
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: '100%',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={
                          item.item.profileImage
                            ? {uri: item.item.profileImage}
                            : defaultProfile
                        }
                        style={{height: 40, width: 40, borderRadius: 20}}
                      />
                      <View style={{justifyContent: 'center', marginLeft: 6}}>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Caption,
                            {
                              color: COLORS.dark_800,
                              maxWidth: 160,
                            },
                          ]}>
                          {item.item.firstName + ' ' + item.item.lastName}
                        </Text>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Note2,
                            {
                              color: COLORS.altgreen_400,
                              textTransform: 'capitalize',
                              maxWidth: 160,
                            },
                          ]}>
                          {item.item.persona}
                        </Text>
                      </View>
                    </View>
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Note2,
                        {
                          color: COLORS.altgreen_400,
                          textTransform: 'capitalize',
                          maxWidth: 160,
                        },
                      ]}>
                      {item.item.connected ? 'Connected' : ''}
                    </Text>
                  </TouchableOpacity>
                ) : this.state.searchType === 'CAUSES' ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.setState(
                        {
                          searchModalOpen: false,
                          searchDescription: item.item.name,
                          page: 0,
                          searchResponseList: [],
                          searchType: 'CAUSES',
                        },
                        () =>
                          this.saveToRecentOnClickItem('CausesStack', {
                            screen: 'CausesDetail',
                            params: {
                              circleData: JSON.stringify(item.item),
                              id: item.item.id,
                            },
                          }),
                      )
                    }
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: '100%',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={{uri: item.item.imageUrl}}
                        style={{height: 40, width: 40, borderRadius: 20}}
                      />
                      <View style={{justifyContent: 'center', marginLeft: 6}}>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Caption,
                            {
                              color: COLORS.dark_800,
                              // textTransform: 'capitalize',
                              maxWidth: 160,
                            },
                          ]}>
                          {item.item.name}
                        </Text>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Note2,
                            {
                              color: COLORS.altgreen_400,
                              // textTransform: 'capitalize',
                              maxWidth: 160,
                            },
                          ]}>
                          #{item.item.slug}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                ) : this.state.searchType === 'PROJECTS' ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.setState(
                        {
                          searchModalOpen: false,
                          searchDescription: item.item.title,
                          page: 0,
                          searchResponseList: [],
                          searchType: 'PROJECTS',
                        },
                        () =>
                          this.saveToRecentOnClickItem('ProjectDetailView', {
                            slug: item.item.slug,
                          }),
                      )
                    }
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: '100%',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={
                          item.item.coverImage
                            ? {uri: item.item.coverImage}
                            : projectDefault
                        }
                        style={{height: 40, width: 40, borderRadius: 20}}
                      />
                      <View style={{justifyContent: 'center', marginLeft: 6}}>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Caption,
                            {
                              color: COLORS.dark_800,
                              textTransform: 'capitalize',
                              maxWidth: 160,
                            },
                          ]}>
                          {item.item.title}
                        </Text>
                      </View>
                    </View>
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Note2,
                        {
                          color: COLORS.altgreen_400,
                          textTransform: 'capitalize',
                          maxWidth: 160,
                        },
                      ]}>
                      {item.item.country ? item.item.country : ''}
                    </Text>
                  </TouchableOpacity>
                ) : this.state.searchType === 'COMPANY' ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.setState(
                        {
                          searchModalOpen: false,
                          searchDescription: item.item.companyName,
                          page: 0,
                          searchResponseList: [],
                          searchType: 'COMPANY',
                        },
                        () =>
                          this.saveToRecentOnClickItem('ProfileStack', {
                            screen: 'CompanyProfileScreen',
                            params: {userId: item.item.userId},
                          }),
                      )
                    }
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: '100%',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={
                          item.item.profileImage
                            ? {uri: item.item.profileImage}
                            : BusinessDefault
                        }
                        style={{height: 40, width: 40, borderRadius: 20}}
                      />
                      <View style={{justifyContent: 'center', marginLeft: 6}}>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Caption,
                            {
                              color: COLORS.dark_800,
                              maxWidth: 160,
                            },
                          ]}>
                          {item.item.companyName}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                ) : this.state.searchType === 'ARTICLES' ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.setState(
                        {
                          searchModalOpen: false,
                          searchDescription:
                            trimDescription(item.item.title).length > 20
                              ? trimDescription(item.item.title).substring(
                                  0,
                                  20,
                                ) + '...'
                              : trimDescription(item.item.title),
                          page: 0,
                          searchResponseList: [],
                          searchType: 'ARTICLES',
                        },
                        () =>
                          this.saveToRecentOnClickItem('Webview', {
                            news: item.item.title,
                            news_url: item.item.postLinkTypeUrl,
                            name: item.item.postType,
                          }),
                      )
                    }
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: '100%',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      {item.item.userProfileImage ? (
                        <Image
                          source={{uri: item.item.userProfileImage}}
                          style={{height: 40, width: 40, borderRadius: 20}}
                        />
                      ) : (
                        <View
                          style={{
                            height: 40,
                            width: 40,
                            borderRadius: 20,
                            backgroundColor: COLORS.altgreen_100,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Icon
                            name="Article"
                            size={20}
                            color={COLORS.dark_600}
                          />
                        </View>
                      )}
                      <View style={{justifyContent: 'center', marginLeft: 6}}>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Caption,
                            {
                              color: COLORS.dark_800,
                              textTransform: 'capitalize',
                              maxWidth: 160,
                            },
                          ]}>
                          {item.item.title}
                        </Text>
                        {item.item.userName ? (
                          <Text
                            numberOfLines={1}
                            style={[
                              typography.Note2,
                              {
                                color: COLORS.dark_800,
                                textTransform: 'capitalize',
                                maxWidth: 160,
                              },
                            ]}>
                            {item.item.userName}
                          </Text>
                        ) : null}
                      </View>
                    </View>
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Note2,
                        {
                          color: COLORS.altgreen_400,
                          textTransform: 'capitalize',
                          maxWidth: 160,
                        },
                      ]}>
                      {moment
                        .unix(item.item.createTime / 1000)
                        .format('Do MMM, YYYY')}
                    </Text>
                  </TouchableOpacity>
                ) : this.state.searchType === 'CIRCLE' ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.setState(
                        {
                          searchModalOpen: false,
                          searchDescription: item.item.title,
                          page: 0,
                          searchResponseList: [],
                          searchType: 'CIRCLE',
                        },
                        () =>
                          this.saveToRecentOnClickItem('CircleProfileStack', {
                            screen: 'CircleProfile',
                            params: {slug: item.item.slug},
                          }),
                      )
                    }
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: '100%',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={
                          item.item.profileImage
                            ? {uri: item.item.profileImage}
                            : CirclesDefault
                        }
                        style={{height: 40, width: 40, borderRadius: 20}}
                      />
                      <View style={{justifyContent: 'center', marginLeft: 6}}>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Caption,
                            {
                              color: COLORS.dark_800,
                              textTransform: 'capitalize',
                              maxWidth: 160,
                            },
                          ]}>
                          {item.item.title}
                        </Text>
                      </View>
                    </View>
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Note2,
                        {
                          color: COLORS.altgreen_400,
                          textTransform: 'capitalize',
                          maxWidth: 160,
                        },
                      ]}>
                      {item.item?.location?.country}
                    </Text>
                  </TouchableOpacity>
                ) : this.state.searchType === 'FEEDS' ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.setState(
                        {
                          searchModalOpen: false,
                          searchDescription:
                            trimDescription(item.item.description).length > 20
                              ? trimDescription(
                                  item.item.description,
                                ).substring(0, 20) + '...'
                              : trimDescription(item.item.description),
                          page: 0,
                          searchResponseList: [],
                          searchType: 'FEEDS',
                        },
                        () =>
                          item.item.postType === 'ARTICLE'
                            ? this.saveToRecentOnClickItem('EditorsDesk', {
                                id: item.item.id,
                                userId: this.state.userId,
                              })
                            : this.saveToRecentOnClickItem(
                                'IndividualFeedsPost',
                                {
                                  id: item.item.id,
                                  commentCount: item.item.commentCount,
                                },
                              ),
                      )
                    }
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: '100%',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      {item.item.attachmentIds.length &&
                      (item.item.attachmentIds[0].attachmentType === 'IMAGE' ||
                        item.item.attachmentIds[0].attachmentType ===
                          'VIDEO') ? (
                        <Image
                          source={{
                            uri:
                              item.item.attachmentIds[0].attachmentType ===
                                'VIDEO' &&
                              item.item?.attachmentIds[0]?.thumbnails &&
                              item.item?.attachmentIds[0]?.thumbnails[0]
                                ? item.item?.attachmentIds[0]?.thumbnails[0]
                                : item.item?.attachmentIds[0]?.attachmentUrl
                                ? item.item?.attachmentIds[0]?.attachmentUrl
                                : defaultProfile,
                          }}
                          style={{height: 40, width: 40, borderRadius: 20}}
                        />
                      ) : (
                        <View
                          style={{
                            height: 40,
                            width: 40,
                            borderRadius: 20,
                            backgroundColor: COLORS.altgreen_100,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Icon
                            name={
                              item.item.postType === 'POST'
                                ? 'TxEdi__AddQuote'
                                : 'Globe'
                            }
                            size={20}
                            color={COLORS.dark_600}
                          />
                        </View>
                      )}
                      <View style={{justifyContent: 'center', marginLeft: 6}}>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Caption,
                            {
                              color: COLORS.dark_800,
                              maxWidth: 160,
                            },
                          ]}>
                          {trimDescription(item.item.description).length > 0
                            ? trimDescription(item.item.description)
                            : '...'}
                        </Text>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Note2,
                            {
                              color: COLORS.altgreen_400,
                              textTransform: 'capitalize',
                              maxWidth: 160,
                            },
                          ]}>
                          {item.item.userName}
                        </Text>
                      </View>
                    </View>
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Note2,
                        {
                          color: COLORS.altgreen_400,
                          textTransform: 'capitalize',
                          maxWidth: 160,
                        },
                      ]}>
                      {moment
                        .unix(item.item.createTime / 1000)
                        .format('do MMM, YYYY')}
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <></>
                )}
              </View>
            )}
          />
        </SafeAreaView>
      </Modal>
    );
  };

  render() {
    return (
      <View style={styles.headerContainer}>
        {this.searchModal()}

        {this.state.profileimg && this.state.profileimg !== '' ? (
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => {
              this.props.changeState({redirectToProfile: true});
              this.props.navigateProfile();
            }}>
            <Image source={{uri: this.state.profileimg}} style={styles.image} />
          </TouchableOpacity>
        ) : (
          <View>
            <Image source={defaultProfile} style={styles.image} />
          </View>
        )}

        <TouchableOpacity
          onPress={() => this.setState({searchModalOpen: true})}
          style={styles.searchBar}>
          <Text style={styles.searchText}>
            Search in <Text style={styles.weNatText}>WeNaturalist</Text>
          </Text>

          <Icon name="Search" size={14} color="#A2B2B8" />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.markAllNotificationAsRead(this.state.userId);
          }}>
          {this.state.notificationCount ? (
            <View>
              <Icon
                name="Bell_notify"
                size={20}
                color="#00394D"
                style={styles.bellIcon}
              />
              <View
                style={
                  Platform.OS === 'ios'
                    ? {
                        width: 8,
                        height: 8,
                        borderRadius: 4,
                        backgroundColor: '#BFC52E',
                        position: 'absolute',
                        top: 1.2,
                        left: 18.8,
                      }
                    : {
                        width: 9,
                        height: 9,
                        borderRadius: 4.5,
                        backgroundColor: '#BFC52E',
                        position: 'absolute',
                        top: 8,
                        left: 18.8,
                      }
                }></View>
            </View>
          ) : (
            <View>
              <Icon
                name="Bell"
                size={20}
                color="#00394D"
                style={styles.bellIcon}
              />
            </View>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchBar: {
    width: '65%',
    backgroundColor: '#F7F7F5',
    height: 36,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: '#E9ECEE',
    borderWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  bellIcon: {
    marginRight: 20,
    marginLeft: 10,
    alignSelf: 'center',
  },
  searchText: {
    color: '#A2B2B8',
    fontSize: 14,
    fontFamily: 'Montserrat-Medium',
  },
  weNatText: {
    color: '#A2B2B8',
    fontSize: 15,
    fontFamily: 'Ubuntu-Bold',
  },
  headerContainer: {
    flexDirection: 'row',
    padding: '2%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  image: {
    height: 40,
    width: 40,
    borderRadius: 20,
    marginLeft: 10,
  },
  searchSuggestionCard: {
    alignSelf: 'center',
    width: '92%',
    marginTop: 10,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
});

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
