import React, {Component} from 'react';
import {
  Share,
  Modal,
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import Snackbar from 'react-native-snackbar';
import TimeAgo from 'react-native-timeago';

import {REACT_APP_userServiceURL} from '../../../../env.json';
import typography from '../../Shared/Typography';
import defaultShape from '../../Shared/Shape';
import {COLORS} from '../../Shared/Colors';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import welogo from '../../../../assets/welogo.png';
import {unixTime2} from '../../Shared/commonFunction';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const randomColor = [
  COLORS.altgreen_100,
  COLORS.green_200,
  COLORS.green_300,
  COLORS.dark_600,
  COLORS.altgreen_200,
  COLORS.green_200,
  COLORS.dark_500,
  COLORS.green_400,
  COLORS.bgFill_200,
  COLORS.altgreen_300,
  COLORS.green_200,
  COLORS.bgfill,
  COLORS.altgreen_100,
  COLORS.green_200,
  COLORS.green_300,
  COLORS.dark_600,
  COLORS.altgreen_200,
  COLORS.green_200,
  COLORS.dark_500,
  COLORS.green_400,
  COLORS.bgFill_200,
  COLORS.altgreen_300,
  COLORS.green_200,
  COLORS.bgfill,
  COLORS.altgreen_100,
  COLORS.green_200,
  COLORS.green_300,
  COLORS.dark_600,
  COLORS.altgreen_200,
  COLORS.green_200,
  COLORS.dark_500,
  COLORS.green_400,
  COLORS.bgFill_200,
  COLORS.altgreen_300,
  COLORS.green_200,
  COLORS.bgfill,
  COLORS.altgreen_100,
  COLORS.green_200,
  COLORS.green_300,
  COLORS.dark_600,
  COLORS.altgreen_200,
  COLORS.green_200,
  COLORS.dark_500,
  COLORS.green_400,
  COLORS.bgFill_200,
  COLORS.altgreen_300,
  COLORS.green_200,
  COLORS.bgfill,
];

export default class Curated extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      pageNumber: 1,
      limit: 4,
      size: 10,
      recommended: [],
      categories: [],
      selectedCategoryId: '',
      categoryName: '',
      blogs: [],
      optionsModalOpen: false,
      shareModalOpen: false,
      categoryModalOpen: false,
      customUrl: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        value && this.setState({userId: value});

        axios({
          method: 'get',
          url:
            REACT_APP_userServiceURL +
            '/profile/get?id=' +
            value +
            '&otherUserId=' +
            '',
          cache: true,
          withCredentials: true,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => {
            this.getRecommenedStory(
              response.data.body.skills,
              response.data.body.specialities,
              response.data.body.interests,
              response.data.body.persona,
              this.state.pageNumber,
              this.state.limit,
              value,
            );

            this.getCategories(value);
          })
          .catch((err) => console.log('Profile data error : ', err));
      })
      .catch((e) => {
        console.log(e);
      });

    // this.getCategories()
  }

  saveUserEntity = (news, news_url, name, blogId, type, index, slug) => {
    let postBody = {
      userId: this.state.userId,
      entityId: blogId,
      timestamp: new Date().valueOf(),
      entityType: type || 'BLOG',
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/recommend/pageclickhook/',
      headers: {'Content-Type': 'application/json'},
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        console.log('res', response.data);
        type === 'BLOG'
          ? this.props.simpleNavigation(
              'EditorsDesk',
              this.state.userId,
              this.state.selectedCategoryId,
              blogId,
              index,
              slug,
            )
          : this.props.webviewNavigation('Webview', news, news_url, name);
      })
      .catch((err) => console.log(err));
  };

  getRecommenedStory = (
    skills,
    specialities,
    interests,
    persona,
    pageNumber,
    limit,
    userId,
  ) => {
    let input = {
      skill_set: skills,
      specialisation: specialities,
      interest: interests,
      persona: persona,
      page_no: pageNumber,
      limit: limit,
      userId: userId,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/recommend/recommendstories/',
      headers: {'Content-Type': 'application/json'},
      data: input,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data !== undefined &&
          response.data.status === 200 &&
          response.data.data !== null &&
          response.data.data !== undefined &&
          response.data.count > 0
        ) {
          console.log('Highlited for you from curated : ', response.data.data);
          this.setState({recommended: response.data.data});
        }
      })
      .catch((err) => console.log('error recommendation: ', err));
  };

  getCategories = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/explore_service/api/explore_category/getAllExplorePublishedCategory',
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data.body !== null &&
          response.data.body.length > 0
        ) {
          this.setState({
            categories: response.data.body,
            selectedCategoryId: response.data.body[0].id,
          });
          this.getBlogs(response.data.body[0].id, 0, userId);
          // console.log('Curated Categories : ', response.data.body)
        } else if (
          response &&
          response.data.body !== null &&
          response.data.body.length === 0
        ) {
          this.setState({
            categories: response.data.body,
          });
          console.log('Curated Categories else part : ', response.data.body);
        }
      })
      .catch((err) => console.log('error in curated category : ', err));
  };

  handleLoadmore = () => {
    this.setState({size: this.state.size + 10}, () =>
      this.getBlogs(
        this.state.selectedCategoryId,
        0,
        this.state.userId,
        this.state.size,
      ),
    );
  };

  getBlogs = (category, page, userId) => {
    this.setState({blogSuccess: false});
    this.setState({blogPreviousBtn: false});
    this.setState({blogNextBtn: false});
    this.setState({lastCategory: category});

    //blogIndex

    let blogIndex;
    if (page === undefined || page === null) {
      blogIndex = 0;
    } else {
      blogIndex = page;
    }
    this.setState({blogIndex: blogIndex});

    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/explore/explore_blog/getPublishedBlogs?page=' +
        blogIndex +
        '&size=' +
        this.state.size,
      data: {userId: userId, category: category},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data.body !== null &&
          response.data.body.content !== null
        ) {
          this.setState({blogs: response.data.body.content});
        }
      })
      .catch((err) => console.log('Editors desk error : ', err));
  };

  handleLike = (activityId, liked) => {
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.getBlogs(this.state.selectedCategoryId, 0, this.state.userId);
        } else {
          console.log('response liked :', response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderCategoryItem = (item) => {
    var rc = '#fff';
    var textcolor = COLORS.dark_800;
    if (item.index < randomColor.length) {
      rc = randomColor[item.index];
    } else {
      rc = randomColor[item.index - randomColor.length];
    }

    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({
            selectedCategoryId: item.item.id,
            categoryName: item.item.name,
            categoryModalOpen: false,
          });
          this.getBlogs(item.item.id, 0, this.state.userId);
        }}
        activeOpacity={0.6}
        style={{
          backgroundColor: rc,
          paddingHorizontal: 20,
          paddingVertical: 8,
          borderRadius: 20,
          marginTop: 10,
          marginLeft: 10,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={[typography.Button_Lead, {color: textcolor}]}>
          {item.item.name}
        </Text>
        {item.item.id === this.state.selectedCategoryId ? (
          <View
            style={{
              marginLeft: 10,
              height: 8,
              width: 8,
              borderRadius: 4,
              backgroundColor: COLORS.primarydark,
            }}></View>
        ) : null}
      </TouchableOpacity>
    );
  };

  renderBlogItem = (item) => {
    return (
      <View
        style={[
          defaultShape.card8_a,
          {
            height: 330,
            width: '92%',
            marginLeft: 15,
            marginTop: 10,
            marginBottom: 10,
          },
        ]}>
        <Image
          source={{uri: item.item.originalCoverImageUrl}}
          style={{
            height: 180,
            width: '100%',
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
          }}
        />

        <LinearGradient
          colors={[COLORS.white + '00', COLORS.white + 'CC', COLORS.white]}
          style={{
            height: 50,
            width: '100%',
            justifyContent: 'flex-end',
            position: 'absolute',
            top: 130,
          }}></LinearGradient>

        <View style={{paddingHorizontal: 15, marginTop: -10}}>
          <TouchableOpacity
            onPress={() =>
              this.saveUserEntity(
                null,
                null,
                null,
                item.item.id,
                'BLOG',
                item.index,
                item.item.slug,
              )
            }>
            <Text
              numberOfLines={1}
              style={[typography.Button_1, {color: COLORS.dark_800}]}>
              {item.item.title}
            </Text>
          </TouchableOpacity>

          <Text
            numberOfLines={4}
            style={[
              typography.Caption,
              {
                color: COLORS.altgreen_300,
                marginTop: 5,
              },
            ]}>
            {item.item.metaDescription}
          </Text>
        </View>

        <View
          style={{
            position: 'absolute',
            bottom: 0,
            paddingVertical: 10,
            paddingHorizontal: 15,
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '95%',
          }}>
          <View style={{flexDirection: 'row'}}>
            {/* <Icon name="LogoMark" size={16} color={COLORS.altgreen_400} /> */}
            <Image source={welogo} style={{height: 35, width: 25}} />
            <View style={{marginLeft: 8}}>
              <Text
                style={[
                  typography.Note,
                  {
                    color: COLORS.altgreen_400,
                    fontFamily: 'Montserrat-Medium',
                    marginTop: 5,
                  },
                ]}>
                FROM EDITOR'S DESK
              </Text>
              <Text
                style={[
                  typography.Note,
                  {color: COLORS.grey_350, fontFamily: 'Montserrat-Medium'},
                ]}>
                <TimeAgo time={item.item?.createTime} />
                {/* {unixTime2(item.item.createTime)} */}
              </Text>
            </View>
          </View>

          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <TouchableOpacity
                onPress={() => this.handleLike(item.item.id, item.item.liked)}
                style={{marginTop: 3, marginRight: 5, alignItems: 'center'}}>
                <Icon
                  name={item.item.liked ? 'Like_FL' : 'Like'}
                  size={16}
                  color={COLORS.green_500}
                  style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                />
              </TouchableOpacity>
              <Text style={{fontSize: 14, color: COLORS.grey_350}}>
                {item.item.likesCount}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginLeft: 10,
              }}>
              <TouchableOpacity
                style={{marginTop: 3, marginRight: 5, alignItems: 'center'}}>
                <Icon
                  name="Comment"
                  size={16}
                  color={COLORS.green_500}
                  style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                />
              </TouchableOpacity>
              <Text style={{fontSize: 14, color: COLORS.grey_350}}>
                {item.item.commentCount}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({optionsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Blog
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({optionsModalOpen: false, shareModalOpen: true});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Share
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({optionsModalOpen: false});
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Bookmark this blog
              </Text>
              <Icon
                name="Bookmark_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({optionsModalOpen: false});
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Don't show me blogs like this
              </Text>
              <Icon
                name="Hide"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}
              onPress={() =>
                this.setState({reasonForReportingModalOpen: true})
              }>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Report Content
              </Text>
              <Icon
                name="ReportComment_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false});
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(this.state.customUrl);
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false});
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share through Mail
              </Text>
              <Icon
                name="Mail_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  categoryModal = () => {
    return (
      <Modal
        visible={this.state.categoryModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({categoryModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {backgroundColor: COLORS.primarydark},
            ]}>
            <View
              style={{
                backgroundColor: COLORS.primarydark,
                paddingVertical: 20,
                paddingHorizontal: 15,
                height: 300,
              }}>
              <FlatList
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{flexWrap: 'wrap', flexDirection: 'row'}}
                data={this.state.categories}
                keyExtractor={(item) => item.id}
                renderItem={(item) => this.renderCategoryItem(item)}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message: this.state.customUrl,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.optionsModal()}
        {this.shareModal()}
        {this.categoryModal()}

        {/****** Editor's Desk Starts ******/}

        <View style={{marginTop: 10, marginBottom: 30}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginRight: 15,
            }}>
            <View style={[styles.recommendedStoriesText]}>
              <Text style={[typography.OVERLINE, {color: COLORS.white}]}>
                {this.state.categoryName === ''
                  ? 'SELECT CATEGORY'
                  : this.state.categoryName}
              </Text>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.setState(
                  {
                    categoryModalOpen: true,
                  },
                  () => console.log(this.state.categoryModalOpen),
                )
              }
              style={[styles.addProj, {backgroundColor: COLORS.NoColor}]}>
              <Icon name="FilterSlide" color={COLORS.altgreen_400} size={18} />
            </TouchableOpacity>
          </View>

          <FlatList
            // horizontal
            showsVerticalScrollIndicator={false}
            // contentContainerStyle={{
            //   flexWrap: 'wrap',
            //   flexDirection: 'row',
            //   paddingRight: 12,
            // }}
            onEndReached={this.handleLoadmore}
            onEndReachedThreshold={2}
            data={this.state.blogs}
            keyExtractor={(item) => item.id}
            renderItem={(item) => this.renderBlogItem(item)}
          />
        </View>

        {/****** Editor's Desk Ends ******/}

        {/* <View
          style={{
            backgroundColor: COLORS.primarydark,
            paddingVertical: 20,
            paddingHorizontal: 15,
          }}>
          <Text
            style={[
              typography.OVERLINE,
              {alignSelf: 'center', marginBottom: 10, color: COLORS.white},
            ]}>
            SEE IN CATEGORIES
          </Text>
          <FlatList
            contentContainerStyle={{flexWrap: 'wrap', flexDirection: 'row'}}
            data={this.state.categories}
            keyExtractor={(item) => item.id}
            renderItem={(item) => this.renderCategoryItem(item)}
          />
        </View> */}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  popularStoriesText: {
    paddingHorizontal: 5,
    paddingVertical: 2,
    backgroundColor: COLORS.dark_900,
    marginLeft: 15,
    borderRadius: 2,
    width: 130,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  popularStoriesView: {
    width: 200,
    marginLeft: 15,
    borderLeftColor: COLORS.grey_300,
    borderLeftWidth: 2,
    height: 175,
    marginTop: 15,
    paddingLeft: 8,
  },
  recommendedStoriesText: {
    paddingHorizontal: 5,
    paddingVertical: 2,
    backgroundColor: COLORS.dark_900,
    marginLeft: 15,
    borderRadius: 2,
    // width: 150,
  },
  popularTagsText: {
    paddingHorizontal: 5,
    paddingVertical: 2,
    backgroundColor: COLORS.altgreen_300,
    borderRadius: 2,
    width: 110,
  },
});
