import React, {Component} from 'react';
import {
  Clipboard,
  Share,
  Modal,
  View,
  Text,
  SafeAreaView,
  ImageBackground,
  FlatList,
  TouchableOpacity,
  Image,
  StyleSheet,
  Platform,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import Snackbar from 'react-native-snackbar';

import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import typography from '../../Shared/Typography';
import defaultShape from '../../Shared/Shape';
import {COLORS} from '../../Shared/Colors';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultStyle from '../../../Components/Shared/Typography';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class DailyEdition extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNumber: 1,
      limit: 5,
      userId: '',
      recommended: [],
      popularStories: [],
      popularHashtags: [],
      shareModalOpen: false,
      customUrl: '',
      currentPressedId: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        value && this.setState({userId: value});

        axios({
          method: 'get',
          url:
            REACT_APP_userServiceURL +
            '/profile/get?id=' +
            value +
            '&otherUserId=' +
            '',
          cache: true,
          withCredentials: true,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => {
            this.getRecommenedStory(
              response.data.body.skills,
              response.data.body.specialities,
              response.data.body.interests,
              response.data.body.persona,
              this.state.pageNumber,
              this.state.limit,
              value,
            );
          })
          .catch((err) => console.log('Profile data error : ', err));
      })
      .catch((e) => {
        console.log(e);
      });

    this.getPopularStory(this.state.pageNumber, this.state.limit);
    this.getTrendingHashtags();
  }

  saveUserEntity = (news, news_url, name, blogId, type) => {
    let postBody = {
      userId: this.state.userId,
      entityId: blogId,
      timestamp: new Date().valueOf(),
      entityType: type || 'BLOG',
    };
    console.log(postBody);
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/recommend/pageclickhook/',
      headers: {'Content-Type': 'application/json'},
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        console.log('res', response.data);
        this.props.webviewNavigation('Webview', news, news_url, name);
      })
      .catch((err) => console.log(err));
  };

  getTrendingHashtags = () => {
    // this.setState({"isHashtagsSuccess": false})
    var limit = 8;
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/story/getPopularHashtags?size=' +
        limit,
      withCredentials: true,
    }).then((response) => {
      //console.log(response);
      if (
        response !== undefined &&
        response.data !== undefined &&
        response.data.body !== undefined &&
        response.data.status === '200 OK'
      ) {
        this.setState({popularHashtags: response.data.body});
        // console.log('popular hashtags : ', response.data.body)
      }
    });
  };

  getPopularStory = (pageNumber, limit) => {
    let input = {
      page_no: pageNumber,
      limit: limit,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/recommend/trendstories/',
      headers: {'Content-Type': 'application/json'},
      data: input,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data !== undefined &&
          response.data.status === 200 &&
          response.data.data !== null &&
          response.data.data !== undefined
        ) {
          // console.log("popular stories from daily edition: ", response.data.data)
          this.setState({popularStories: response.data.data});
        }
      })
      .catch((err) => console.log('error in popular stories: ', err));
  };

  getRecommenedStory = (
    skills,
    specialities,
    interests,
    persona,
    pageNumber,
    limit,
    userId,
  ) => {
    let input = {
      skill_set: skills,
      specialisation: specialities,
      interest: interests,
      persona: persona,
      page_no: pageNumber,
      limit: limit,
      userId: userId,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/recommend/recommendstories/',
      headers: {'Content-Type': 'application/json'},
      data: input,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data !== undefined &&
          response.data.status === 200 &&
          response.data.data !== null &&
          response.data.data !== undefined &&
          response.data.count > 0
        ) {
          // console.log("recomended response from daily edition: ", response.data.data)
          this.setState({recommended: response.data.data});
        }
      })
      .catch((err) => console.log('error recommendation: ', err));
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.hashTagNavigation('RepostOnWenat', {
                    link: false,
                    pressedActivityId: this.state.currentPressedId,
                    entityType: 'STORY',
                    type: 'EXPLORE',
                  }),
                );
              }}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl + '/post/' + this.state.currentPressedId,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            {/* <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false});
              }}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Share through Mail
              </Text>
              <Icon
                name="Mail_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity> */}

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Envelope"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message: this.state.customUrl,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of', result.activityType);
        } else {
          console.log('shared : ', result);
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {/****** Banner Start ******/}

        {this.shareModal()}

        {this.state.popularStories.length > 0 ? (
          <ImageBackground
            source={{uri: this.state.popularStories[0].image_url}}
            style={{height: 360, width: '100%'}}>
            <LinearGradient
              colors={['#F3F3F100', '#F3F3F11A', '#F3F3F1E6', '#F3F3F1']}
              style={{
                width: '100%',
                height: 260,
                position: 'absolute',
                bottom: 0,
                // alignItems: 'center',
                justifyContent: 'flex-end',
                paddingHorizontal: 15,
              }}>
              <TouchableOpacity
                onPress={() =>
                  this.saveUserEntity(
                    this.state.popularStories[0].txt_data,
                    this.state.popularStories[0].news_url,
                    this.state.popularStories[0].name,
                    this.state.popularStories[0]._id,
                    'RECOMMENDED_STORIES_HEADER',
                  )
                }>
                <Text
                  numberOfLines={2}
                  style={[typography.Title_1, {color: COLORS.dark_800}]}>
                  {this.state.popularStories[0].header}
                </Text>
              </TouchableOpacity>

              <Text
                numberOfLines={2}
                style={[
                  typography.Caption,
                  {
                    color: COLORS.altgreen_400,
                    alignSelf: 'flex-start',
                    marginTop: 5,
                  },
                ]}>
                {this.state.popularStories[0].name}
              </Text>

              <View
                style={{
                  height: 30,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: 5,
                }}>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={this.state.popularStories[0].hash_tags}
                  keyExtractor={(item) => item}
                  renderItem={(item) => (
                    <Text
                      style={[
                        typography.Subtitle_2,
                        {color: COLORS.green_600, marginRight: 5},
                      ]}>
                      #{item.item}
                    </Text>
                  )}
                />
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      shareModalOpen: true,
                      customUrl: this.state.recommended[0].news_url,
                      currentPressedId: this.state.recommended[0]._id,
                    })
                  }
                  style={{marginLeft: 20, marginRight: 10, marginTop: -5}}>
                  <Icon name="Share" size={16} color={COLORS.altgreen_300} />
                </TouchableOpacity>
              </View>
            </LinearGradient>
          </ImageBackground>
        ) : (
          <SkeletonPlaceholder backgroundColor={COLORS.grey_300}>
            <View style={{height: 300}}></View>
            <View style={{height: 20, borderRadius: 10, marginTop: 10}}></View>
            <View style={{height: 40, borderRadius: 10, marginTop: 10}}></View>
          </SkeletonPlaceholder>
        )}

        {/****** Banner Ends ******/}

        {/****** Popular Stories Start ******/}

        <View style={{marginTop: 38, flexDirection: 'row', justifyContent:'space-around'}}>
          <TouchableOpacity
            style={styles.popularStoriesText}
            onPress={() =>
              this.props.simpleNavigation(
                'SeeallPopularStories',
                this.state.userId,
              )
            }>
            <Text style={[typography.OVERLINE, {color: COLORS.white}]}>
              POPULAR STORIES
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.recommendedStoriesText}
            onPress={() =>
              this.props.simpleNavigation(
                'SeeallRecommendedStories',
                this.state.userId,
              )
            }>
            <Text style={[typography.OVERLINE, {color: COLORS.white}]}>
              STORIES JUST FOR YOU
            </Text>
          </TouchableOpacity>

          {/* <View style={{height: 211}}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={this.state.popularStories}
              keyExtractor={(item) => item.id}
              ListFooterComponent={
                <View
                  style={{
                    height: 175,
                    width: 150,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.simpleNavigation(
                        'SeeallPopularStories',
                        this.state.userId,
                      )
                    }
                    style={[defaultShape.ContextBtn_OL]}>
                    <Text
                      style={[typography.Caption, {color: COLORS.dark_500}]}>
                      See more
                    </Text>
                  </TouchableOpacity>
                </View>
              }
              renderItem={(item) => (
                <View style={styles.popularStoriesView}>
                  <Text style={[typography.Note, {color: COLORS.altgreen_400}]}>
                    {item.item.name}
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      this.saveUserEntity(
                        item.item.txt_data,
                        item.item.news_url,
                        item.item.name,
                        item.item._id,
                        'POPULAR',
                      )
                    }>
                    <Text
                      numberOfLines={3}
                      style={[
                        typography.Button_Lead,
                        {color: COLORS.dark_800, marginTop: 5},
                      ]}>
                      {item.item.header}
                    </Text>
                  </TouchableOpacity>
                  <Text
                    numberOfLines={3}
                    style={[
                      typography.Note,
                      {
                        color: COLORS.altgreen_300,
                        fontFamily: 'Montserrat-Medium',
                        marginTop: 5,
                      },
                    ]}>
                    {item.item.txt_data}
                  </Text>
                  <FlatList
                    style={{marginTop: 8}}
                    data={item.item.hash_tags.slice(0, 1)}
                    keyExtractor={(item) => item}
                    renderItem={(item) => (
                      <Text
                        style={[
                          typography.Subtitle_2,
                          {color: COLORS.green_600, marginRight: 5},
                        ]}>
                        #{item.item}
                      </Text>
                    )}
                  />
                  <TouchableOpacity
                    onPress={() =>
                      this.setState(
                        {
                          shareModalOpen: true,
                          customUrl: item.item.news_url,
                          currentPressedId: item.item._id,
                        },
                        () => {
                          console.log(
                            'object press',
                            this.state.currentPressedId,
                          );
                        },
                      )
                    }
                    style={{
                      marginTop: 5,
                      height: 24,
                      width: 24,
                      borderRadius: 12,
                      backgroundColor: COLORS.grey_200,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Icon
                      name="Share"
                      size={14}
                      color={COLORS.dark_600}
                      style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                    />
                  </TouchableOpacity>
                </View>
              )}
            />
          </View> */}
        </View>

        {/****** Popular Stories Ends ******/}

        {/****** Recommended Stories Start ******/}

        <View style={{marginTop: 28}}>
          {/* <View>
            <FlatList
              data={this.state.recommended}
              keyExtractor={(item) => item._id}
              ListFooterComponent={
                <TouchableOpacity
                  onPress={() =>
                    this.props.simpleNavigation(
                      'SeeallRecommendedStories',
                      this.state.userId,
                    )
                  }
                  style={[
                    defaultShape.ContextBtn_OL,
                    {alignSelf: 'center', marginTop: 10, marginBottom: 30},
                  ]}>
                  <Text style={[typography.Caption, {color: COLORS.dark_500}]}>
                    See more
                  </Text>
                </TouchableOpacity>
              }
              renderItem={(item) => (
                <View style={{marginHorizontal: 15, marginTop: 15}}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      source={{uri: item.item.image_url}}
                      style={{
                        height: 100,
                        width: 100,
                        borderRadius: 4,
                        marginRight: 10,
                      }}
                    />

                    <View>
                      <Text
                        style={[typography.Note, {color: COLORS.altgreen_400}]}>
                        {item.item.name}
                      </Text>
                      <TouchableOpacity
                        onPress={() =>
                          this.saveUserEntity(
                            item.item.news,
                            item.item.news_url,
                            item.item.name,
                            item.item._id,
                            'RECOMMENDED_STORIES',
                          )
                        }>
                        <Text
                          numberOfLines={2}
                          style={[
                            typography.Button_Lead,
                            {
                              color: COLORS.dark_800,
                              marginTop: 5,
                              maxWidth: '82%',
                            },
                          ]}>
                          {item.item.header}
                        </Text>
                      </TouchableOpacity>
                      <Text
                        numberOfLines={3}
                        style={[
                          typography.Note,
                          {
                            color: COLORS.altgreen_300,
                            fontFamily: 'Montserrat-Medium',
                            marginTop: 5,
                            maxWidth: '82%',
                          },
                        ]}>
                        {item.item.news}
                      </Text>
                    </View>
                  </View>

                  <View
                    style={{
                      height: 30,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginTop: 10,
                    }}>
                    <FlatList
                      horizontal
                      showsHorizontalScrollIndicator={false}
                      data={item.item.hash_tags.slice(0, 3)}
                      keyExtractor={(item) => item}
                      renderItem={(item) => (
                        <Text
                          style={[
                            typography.Subtitle_2,
                            {color: COLORS.green_600, marginRight: 5},
                          ]}>
                          #{item.item}
                        </Text>
                      )}
                    />
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          shareModalOpen: true,
                          customUrl: item.item.news_url,
                        })
                      }
                      style={{
                        height: 24,
                        width: 24,
                        borderRadius: 12,
                        backgroundColor: COLORS.grey_200,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Icon
                        name="Share"
                        size={14}
                        color={COLORS.dark_600}
                        style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              )}
            />
          </View> */}
        </View>

        {/****** Recommended Stories Ends ******/}

        {/****** Popular Hashtags Start ******/}

        <View
          style={{
            backgroundColor: COLORS.white,
            paddingHorizontal: 15,
            paddingVertical: 22,
            marginBottom: 50,
          }}>
          <View style={styles.popularTagsText}>
            <Text style={[typography.OVERLINE, {color: COLORS.white}]}>
              POPULAR TAGS
            </Text>
          </View>

          <View>
            <FlatList
              contentContainerStyle={{flexWrap: 'wrap', flexDirection: 'row'}}
              showsHorizontalScrollIndicator={false}
              data={this.state.popularHashtags}
              ListFooterComponent={
                <TouchableOpacity
                  onPress={() => this.props.hashTagNavigation('HashTagStack')}
                  style={[
                    defaultShape.ContextBtn_OL,
                    {
                      marginTop: 10,
                      marginHorizontal: 5,
                      alignItems: 'center',
                      paddingVertical: Platform.OS === 'ios' ? 5 : -5,
                    },
                  ]}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_500}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                </TouchableOpacity>
              }
              keyExtractor={(item) => item}
              renderItem={(item) => (
                <View
                  style={[
                    defaultShape.ContextBtn_OL,
                    {marginTop: 10, marginHorizontal: 5},
                  ]}>
                  <Text
                    style={[typography.Caption, {color: COLORS.dark_500}]}
                    onPress={() =>
                      this.props.hashTagNavigation('HashTagDetail', {
                        slug: item.item,
                      })
                    }>
                    #{item.item}
                  </Text>
                </View>
              )}
            />
          </View>
        </View>

        {/****** Popular Hashtags Ends ******/}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  popularStoriesText: {
    backgroundColor: COLORS.dark_900,
    marginLeft: 15,
    borderRadius: 4,
    paddingHorizontal:10,
    paddingVertical:5,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  popularStoriesView: {
    width: 200,
    marginLeft: 15,
    borderLeftColor: COLORS.grey_300,
    borderLeftWidth: 2,
    height: 175,
    marginTop: 15,
    paddingLeft: 8,
  },
  recommendedStoriesText: {
    backgroundColor: COLORS.dark_900,
    marginLeft: 15,
    borderRadius: 4,
    paddingHorizontal:10,
    paddingVertical:5,
  },
  popularTagsText: {
    paddingHorizontal: 5,
    paddingVertical: 2,
    backgroundColor: COLORS.altgreen_300,
    borderRadius: 2,
    width: 110,
  },
});
