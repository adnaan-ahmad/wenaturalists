import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ImageBackground,
  FlatList,
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView,
  Platform,
  Modal,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import HTML from 'react-native-render-html';
import VideoPlayer from 'react-native-video-controls';
import {TextInput} from 'react-native-paper';
import Snackbar from 'react-native-snackbar';
import TimeAgo from 'react-native-timeago';

import {REACT_APP_userServiceURL} from '../../../../env.json';
import typography from '../../Shared/Typography';
import defaultShape from '../../Shared/Shape';
import defaultCover from '../../../../assets/defaultCover.png';
import defaultProfile from '../../../../assets/defaultProfile.png';
import CirclesDefault from '../../../../assets/CirclesDefault.png';
import manImage from '../../../../assets/manImage.jpeg';
import {COLORS} from '../../Shared/Colors';
import {unixTime2} from '../../Shared/commonFunction';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultStyle from '../../../Components/Shared/Typography';
import SharedUserList from '../../../Components/User/Common/SharedUserList';
import LikedUserList from '../../../Components/User/Common/LikedUserList';
import Follow from '../Common/Follow';
import ConnectDepth from '../Common/ConnectDepth';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class Trending extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      pageNumber: 0,
      circlePageNumber: 0,
      causesPageNumber: 0,
      limit: 5,
      recentStory: [],
      data: [],
      post: [],
      causes: [],
      circles: [],
      selected: 'ARTICLE',
      currentPressed: {},
      optionsModalOpen: false,
      peopleLiked: [],
      likeModalOpen: false,
      shareModalOpen: false,
      peopleSharedModalOpen: false,
      canReport: '',
      isReported: false,
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        value &&
          this.setState({userId: value}, () => {
            this.getRecentStory(
              this.state.pageNumber,
              this.state.limit,
              this.state.userId,
            );
            this.getPost(this.state.userId);
            this.getCauses();
          });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  componentWillUnmount() {
    // this._unsubscribe.remove();
  }

  getPost = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/post/getTrendingPosts?userId=' +
        this.state.userId +
        '&newsFeedType=' +
        this.state.selected +
        '&page=' +
        this.state.pageNumber +
        '&size=3',
      withCredentials: true,
    })
      .then((response) => {
        console.log('jkjkjkj', response.data);
        if (response.data.status === '200 OK') {
          console.log('object photos', response.data.body);
          this.setState({post: response.data.body.content});
        }
      })
      .catch((err) => console.log('Trending Blogs data error : ', err));
  };

  getCauses = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/cause/trending/list?page=' +
        this.state.causesPageNumber +
        '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        if (
          response !== undefined &&
          response.data !== undefined &&
          response.data.status === '200 OK' &&
          response.data.body !== undefined &&
          response.data.body.content !== undefined
        ) {
          console.log(response.data.body.content);
          this.setState({
            causes: this.state.causes.concat(response.data.body.content),
          });
        }
      })
      .catch((err) => console.log('Trending Blogs data error : ', err));
  };

  getCircle = () => {
    let postBody = {
      page: this.state.circlePageNumber,
      size: 10,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/circle/trending-circle',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        console.log(response.data.body.content);
        this.setState({
          circles: this.state.circles.concat(response.data.body.content),
        });
      })
      .catch((err) => console.log('Trending circle data error : ', err));
  };

  handleLoadMoreCircle = () => {
    this.setState({circlePageNumber: this.state.circlePageNumber + 1}, () =>
      this.getCircle(),
    );
  };

  handleLoadMoreCauses = () => {
    this.setState({causesPageNumber: this.state.causesPageNumber + 1}, () =>
      this.getCauses(),
    );
  };

  getRecentStory = (pageNumber, limit, userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/story/getRecentNews?userId=' +
        this.state.userId +
        '&page=' +
        pageNumber +
        '&size=' +
        limit,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response !== undefined &&
          response.data !== undefined &&
          response.data.status === '200 OK' &&
          response.data.body !== undefined &&
          response.data.body.content !== undefined
        ) {
          console.log(
            'Recent story from Trending : ',
            response.data.body.content,
          );
          this.setState({recentStory: response.data.body.content});
        }
      })
      .catch((err) => console.log('error Recent story: ', err));
  };

  handleLike = (activityId, liked) => {
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log('********* Hurray ************')
          // this.setState({ ...this.state })
          this.getRecentStory(this.state.pageNumber, 0, this.state.userId);
          this.getPost(this.state.userId);
        } else {
          console.log('response liked :', response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getUsersWhoLiked = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/like/getUsers/' +
        this.state.pressedActivityId +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          // console.log('%%%%%%%%%%%%%%%% response.data.body %%%%%%%%%%%%%%%%%%%', response.data.body)
          this.setState({peopleLiked: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderCauses = (item) => {
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation('CausesStack', {
            screen: 'CausesDetail',
            params: {circleData: JSON.stringify(item), id: item.id},
          })
        }
        style={styles.individualCause}>
        {item.imageUrl ? (
          <Image source={{uri: item.imageUrl}} style={styles.causesImage} />
        ) : (
          <Image source={defaultCover} style={styles.causesImage} />
        )}

        <View style={styles.linearGradientView}>
          <LinearGradient
            colors={['#154A59', '#154A59CC', '#154A5900']}
            style={styles.linearGradient}></LinearGradient>
        </View>

        <Text style={styles.causesText}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  renderCircle = (item) => {
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation('CircleProfileStack', {
            screen: 'CircleProfile',
            params: {slug: item.slug},
          })
        }
        style={[
          styles.individualCause,
          {width: 150, height: 150, borderRadius: 6},
        ]}>
        <ImageBackground
          style={{width: '100%', height: '100%'}}
          source={
            item.resizedCoverImages && item.resizedCoverImages.compressed
              ? {uri: item.resizedCoverImages.compressed}
              : defaultCover
          }>
          <Image
            source={
              item.profileImage ? {uri: item.profileImage} : CirclesDefault
            }
            style={{
              height: 60,
              width: 60,
              borderRadius: 30,
              marginTop: 20,
              alignSelf: 'center',
            }}
          />
          <LinearGradient
            colors={[
              COLORS.grey_100 + '00',
              COLORS.grey_100 + 'CC',
              COLORS.grey_100,
            ]}
            style={{
              height: 100,
              width: '100%',
              justifyContent: 'flex-end',
              position: 'absolute',
              bottom: 0,
            }}>
            <Text
              numberOfLines={2}
              style={[
                typography.Title_1,
                {color: COLORS.dark_800, marginTop: 10, marginLeft: 20},
              ]}>
              {item.title}
            </Text>
          </LinearGradient>
        </ImageBackground>
      </TouchableOpacity>
    );
  };

  handleHideModal = () => {
    let data = {
      userId: this.state.userId,
      activityId: this.state.pressedActivityId,
      entityType: 'POST',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          console.log('response.status', response.status);
          this.setState({optionsModalOpen: false}, () => {
            this.getRecentStory(0, this.state.limit, this.state.userId);
            this.getPost(this.state.userId);
            this.getCauses();
          });
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          console.log('response.status', err.response.status);
          this.props.navigation.goBack();
        }
      });
    // this.setState({ optionsModalOpen: false, forumData: this.state.forumData.filter((item) => item.id !== id) })
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({optionsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          {
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
                ]}>
                <Text
                  style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                  Post
                </Text>
              </View>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {optionsModalOpen: false, likeModalOpen: true},
                    () => this.getUsersWhoLiked(),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Like_FL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who liked it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    optionsModalOpen: false,
                    peopleSharedModalOpen: true,
                  });
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Share"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who shared it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    optionsModalOpen: false,
                    shareModalOpen: true,
                  });
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Share"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    Share
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.handleHideModal(this.state.pressedActivityId);
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Hide
                </Text>
                <Icon
                  name="Hide"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              {this.state.currentPressed.creatorId === this.state.userId &&
                this.state.currentPressed.postType !== 'ARTICLE' && (
                  <TouchableOpacity
                    style={
                      Platform.OS === 'ios'
                        ? [
                            defaultShape.ActList_Cell_Gylph_Alt,
                            {paddingVertical: 15},
                          ]
                        : [defaultShape.ActList_Cell_Gylph_Alt]
                    }
                    activeOpacity={0.6}
                    onPress={() => {
                      this.setState({optionsModalOpen: false}, () =>
                        this.props.navigation('EditPost', {
                          pressedActivityId: this.state.currentPressed.id,
                          entityType: this.state.currentPressed.postType,
                          description: this.state.currentPressed.description,
                          hashTags: this.state.currentPressed.hashTags,
                        }),
                      );
                    }}>
                    <Text
                      style={[
                        typography.Button_Lead,
                        {color: COLORS.dark_600},
                      ]}>
                      Edit Post
                    </Text>
                    <Icon
                      name="EditBox"
                      size={17}
                      color={COLORS.altgreen_300}
                      style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                    />
                  </TouchableOpacity>
                )}

              {this.state.canReport ? (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {paddingVertical: 15},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({
                      reasonForReportingModalOpen: !this.state.isReported
                        ? true
                        : false,
                      optionsModalOpen: false,
                    });
                    this.state.isReported
                      ? setTimeout(() => {
                          Snackbar.show({
                            backgroundColor: '#B22222',
                            text: 'Your report request was already taken',
                            duration: Snackbar.LENGTH_LONG,
                          });
                        }, 500)
                      : null;
                  }}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    Report
                  </Text>
                  <Icon
                    name="ReportComment_OL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {paddingVertical: 15},
                        ]
                      : [defaultShape.ActList_Cell_Gylph_Alt]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.deletePostAlert();
                  }}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    Delete Post
                  </Text>
                  <Icon
                    name="Hide"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                </TouchableOpacity>
              )}
            </View>
          }
        </View>
      </Modal>
    );
  };

  navigation = (value, params) => {
    this.setState({likeModalOpen: false, peopleSharedModalOpen: false}, () =>
      this.props.navigation(value, params),
    );
  };

  likeModal = () => {
    return (
      <Modal
        visible={this.state.likeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({likeModalOpen: false, peopleLiked: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Who liked this post
              </Text>
            </View>

            <LikedUserList
              id={this.state.pressedActivityId}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation('RepostOnWenat', {
                    link: false,
                    pressedActivityId: this.state.pressedActivityId,
                    entityType: 'POST',
                  }),
                );
              }}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl + '/post/' + this.state.pressedActivityId,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            {/* <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false});
              }}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Share through Mail
              </Text>
              <Icon
                name="Mail_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity> */}

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Envelope"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + ' s';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + ' m';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + ' h';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + ' d';
    }

    if (difference >= 864000) {
      return day + ' ' + month + ' ' + year;
    }
  };

  // renderHeaderPost = (item, index) => {
  //   return (
  //     <View style={styles.unreadNotiItem}>
  //       <TouchableOpacity
  //         activeOpacity={0.7}
  //         onPress={() => {
  //           item.userType === 'INDIVIDUAL' && item.userId === this.state.userId
  //             ? this.props.navigation('ProfileStack')
  //             : item.userType === 'INDIVIDUAL' &&
  //               item.userId !== this.state.userId
  //             ? this.props.navigation('ProfileStack', {
  //                 screen: 'OtherProfileScreen',
  //                 params: {userId: item.userId},
  //               })
  //             : item.userType === 'COMPANY'
  //             ? this.props.navigation('ProfileStack', {
  //                 screen: 'CompanyProfileScreen',
  //                 params: {userId: item.userId},
  //               })
  //             : this.props.navigation('CircleProfileStack', {
  //                 screen: 'CircleProfile',
  //                 params: {slug: item.params.circleSlug},
  //               });
  //           // console.log(item)
  //         }}
  //         style={{width: '40%', flexDirection: 'row'}}>
  //         {item.originalProfileImage ? (
  //           <Image
  //             style={[defaultShape.Media_Round, {}]}
  //             source={{uri: item.originalProfileImage}}
  //           />
  //         ) : item.params && item.params.circleImage ? (
  //           <Image
  //             style={[defaultShape.Media_Round, {}]}
  //             source={{uri: item.params.circleImage}}
  //           />
  //         ) : item.userType === 'INDIVIDUAL' && !item.originalProfileImage ? (
  //           <Image
  //             style={[defaultShape.Media_Round, {backgroundColor: 'orange'}]}
  //             source={defaultProfile}
  //           />
  //         ) : item.userType === 'COMPANY' && !item.originalProfileImage ? (
  //           <Image
  //             style={[defaultShape.Media_Round, {backgroundColor: 'orange'}]}
  //             source={defaultBusiness}
  //           />
  //         ) : item.params &&
  //           item.params.circleSlug &&
  //           !item.params.circleImage ? (
  //           <Image
  //             style={[defaultShape.Media_Round, {}]}
  //             source={circleDefault}
  //           />
  //         ) : (
  //           <></>
  //         )}

  //         <View style={{}}>
  //           <View
  //             style={{
  //               flexDirection: 'row',
  //               alignItems: 'center',
  //               justifyContent: 'center',
  //             }}>
  //             <Text
  //               numberOfLines={1}
  //               style={[
  //                 defaultStyle.Title_2,
  //                 {
  //                   color: COLORS.dark_800,
  //                   marginLeft: 8,
  //                 },
  //               ]}>
  //               {item.userName ? item.userName : item.params.circleTitle}
  //             </Text>
  //             {item.connectDepth ? (
  //               <ConnectDepth
  //                 connectDepth={item.connectDepth}
  //                 key={item.connectDepth}
  //               />
  //             ) : (
  //               <Text></Text>
  //             )}
  //             {item.userEntityType !== 'CIRCLE' &&
  //               !this.state.isCompany &&
  //               item &&
  //               (!item.connectDepth ||
  //                 (item.connectDepth && item.connectDepth < 1)) && (
  //                 <Follow
  //                   item={item}
  //                   followed={item.followed}
  //                   userId={item.userId}
  //                   index={index}
  //                 />
  //               )}
  //           </View>

  //           <View
  //             style={{
  //               flexDirection: 'row',
  //               justifyContent: 'flex-start',
  //               marginLeft: 5,
  //             }}>
  //             {item.country && (
  //               <Icon
  //                 name="Location"
  //                 color={COLORS.altgreen_300}
  //                 size={10}
  //                 style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
  //               />
  //             )}
  //             {item.country && (
  //               <Text
  //                 onPress={() =>
  //                   this.props.navigation.navigate('IndividualFeedsPost', {
  //                     id: item.id,
  //                   })
  //                 }
  //                 style={[
  //                   {
  //                     color: COLORS.altgreen_300,
  //                     marginLeft: 2,
  //                     fontSize: 10,
  //                     fontFamily: 'Montserrat-Medium',
  //                   },
  //                 ]}>
  //                 {item.country
  //                   ? item.country.split(' ').splice(0, 2).join(' ')
  //                   : null}
  //                 {/* {+ new Date() - item.createTime} */}
  //               </Text>
  //             )}
  //             {item.country && (
  //               <Icon
  //                 name="Bullet_Fill"
  //                 color={COLORS.altgreen_300}
  //                 size={8}
  //                 style={{marginTop: Platform.OS === 'android' ? 0 : 0}}
  //               />
  //             )}

  //             <Text
  //               onPress={() =>
  //                 this.props.navigation.navigate('IndividualFeedsPost', {
  //                   id: item.id,
  //                 })
  //               }
  //               style={[
  //                 {
  //                   marginLeft: 2,
  //                   fontSize: 10,
  //                   fontFamily: 'Montserrat-Medium',
  //                 },
  //               ]}>
  //               {this.unixTime2(item.createTime)}
  //               {/* {+ new Date() - item.createTime} */}
  //             </Text>
  //           </View>
  //         </View>
  //       </TouchableOpacity>
  //       <TouchableOpacity
  //         activeOpacity={0.5}
  //         style={[defaultShape.Nav_Gylph_Btn, {}]}
  //         onPress={() =>
  //           this.setState(
  //             {
  //               optionsModalOpen: true,
  //               pressedActivityId: item.id,
  //               currentUserId: item.userId,
  //               canReport: item.canReport,
  //               currentPressed: item,
  //             },
  //             () => this.verifyReported(),
  //           )
  //         }>
  //         <Icon
  //           name="Kebab"
  //           color={COLORS.altgreen_300}
  //           size={14}
  //           style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
  //         />
  //       </TouchableOpacity>
  //     </View>
  //   );
  // };

  renderHeaderPost = (item, index) => {
    return (
      <View style={styles.unreadNotiItem}>
        <View
          style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              item.userType === 'COMPANY'
                ? this.props.navigation('ProfileStack', {
                    screen: 'CompanyProfileScreen',
                    params: {userId: item.userId},
                  })
                : item.userType === 'INDIVIDUAL' &&
                  item.userId !== this.state.userId
                ? this.props.navigation('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: {userId: item.userId},
                  })
                : item.userType === 'INDIVIDUAL' &&
                  item.userId === this.state.userId
                ? this.props.navigation('ProfileStack', {
                    screen: 'ProfileScreen',
                    // params: {userId: item.userId},
                  })
                : null;
            }}>
            {item.originalProfileImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={{uri: item.originalProfileImage}}
              />
            ) : item.params && item.params.circleImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={{uri: item.params.circleImage}}
              />
            ) : item.userType === 'INDIVIDUAL' && !item.originalProfileImage ? (
              <Image
                style={[defaultShape.Media_Round, {backgroundColor: 'orange'}]}
                source={defaultProfile}
              />
            ) : item.userType === 'COMPANY' && !item.originalProfileImage ? (
              <Image
                style={[defaultShape.Media_Round, {backgroundColor: 'orange'}]}
                source={defaultBusiness}
              />
            ) : item.params &&
              item.params.circleSlug &&
              !item.params.circleImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={CirclesDefault}
              />
            ) : (
              <></>
            )}
          </TouchableOpacity>
          <View
            style={{
              justifyContent: 'flex-start',
              marginLeft: 6,
            }}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Text
                numberOfLines={1}
                onPress={() => {
                  item.userType === 'COMPANY'
                    ? this.props.navigation('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                        params: {userId: item.userId},
                      })
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId !== this.state.userId
                    ? this.props.navigation('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.userId},
                      })
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId === this.state.userId
                    ? this.props.navigation('ProfileStack', {
                        screen: 'ProfileScreen',
                        // params: {userId: item.userId},
                      })
                    : null;
                }}
                style={[
                  typography.Title_2,
                  {
                    color: COLORS.dark_800,
                    maxWidth: 160,
                  },
                ]}>
                {item.userName ? item.userName : item.params.circleTitle}
              </Text>
              {item.connectDepth && item.userId != this.state.userId ? (
                <ConnectDepth
                  connectDepth={item.connectDepth}
                  key={item.connectDepth}
                />
              ) : (
                <Text></Text>
              )}
              {item.userEntityType !== 'CIRCLE' &&
                !this.state.isCompany &&
                item &&
                (!item.connectDepth ||
                  (item.connectDepth && item.connectDepth < 1)) && (
                  <Follow
                    item={item}
                    followed={item.followed}
                    userId={item.userId}
                    index={index}
                  />
                )}
            </View>

            <View style={{flexDirection: 'row'}}>
              {item && item.country ? (
                <Icon
                  name="Location"
                  color={COLORS.altgreen_300}
                  size={10}
                  style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                />
              ) : (
                <></>
              )}
              <Text
                numberOfLines={1}
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigation('EditorsDesk', {
                        id: item.id,
                        userId: this.state.userId,
                      })
                    : this.props.navigation('IndividualFeedsPost', {
                        id: item.id,
                        commentCount: item.commentCount,
                      })
                }
                style={[
                  {
                    color: COLORS.altgreen_300,
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                    maxWidth: 100,
                  },
                ]}>
                {item && item.country ? item.country : null}
                {/* {+ new Date() - item.createTime} */}
              </Text>
              {item && item.country ? (
                <Icon
                  name="Bullet_Fill"
                  color={COLORS.altgreen_300}
                  size={8}
                  style={{marginTop: Platform.OS === 'android' ? 1 : 2}}
                />
              ) : (
                <></>
              )}

              <Text
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigation('EditorsDesk', {
                        id: item.id,
                        userId: this.state.userId,
                      })
                    : this.props.navigation('IndividualFeedsPost', {
                        id: item.id,
                        commentCount: item.commentCount,
                      })
                }
                style={[
                  {
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                  },
                ]}>
                {/* {item && this.unixTime2(item.createTime)} */}
                <TimeAgo time={item.createTime}/>
                {/* {+ new Date() - item.createTime} */}
              </Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          activeOpacity={0.5}
          style={[defaultShape.Nav_Gylph_Btn, {}]}
          onPress={() =>
            this.setState(
              {
                optionsModalOpen: true,
                pressedActivityId: item.id,
                currentUserId: item.userId,
                canReport: item.canReport,
                currentPressed: item,
              },
              () => this.verifyReported(),
            )
          }>
          <Icon
            name="Kebab"
            color={COLORS.altgreen_300}
            size={14}
            style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
          />
        </TouchableOpacity>
      </View>
    );
  };
  trimDescription = (item) => {
    var trimmed = item.split('^^__').join(' ').indexOf('@@@__');

    var str = item
      .split('^^__')
      .join(' ')
      .indexOf(' ', item.split('^^__').join(' ').indexOf('@@@__'));

    var sub = item.substring(trimmed, str);

    item = item.replace(' ' + sub, '');
    item = item.replace('@@@^^^', ' ');
    item = item.replace('@@@__', ' ');
    item = item.replace('  ', '');

    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  renderDescription = (item) => {
    return (
      <View
        style={{
          backgroundColor:
            item.postType === 'ARTICLE' ? COLORS.altgreen_100 : COLORS.white,
        }}>
        {item.postType === 'ARTICLE' && (
          <Text
            numberOfLines={1}
            style={[
              typography.H6,
              {
                color: COLORS.dark_800,
                marginLeft: 6,
                marginRight: 17,
                marginTop: 5,
              },
            ]}>
            {item.title}
          </Text>
        )}
        <Text
          onPress={() =>
            item.postType === 'ARTICLE'
              ? this.props.profileRedirectionNavigation('EditorsDesk', {
                  id: item.id,
                  userId: this.state.userId,
                })
              : this.props.profileRedirectionNavigation('IndividualFeedsPost', {
                  id: item.id,
                })
          }
          // onPress={() => this.props.profileRedirectionNavigation('IndividualFeedsPost', { id: item.id })}
          numberOfLines={item.postType === 'ARTICLE' ? 2 : 5}
          style={[
            typography.Body_1,
            {
              color:
                item.postType === 'ARTICLE' ? COLORS.dark_500 : COLORS.dark_700,
              marginLeft: 6,
              marginRight: 17,
              marginVertical: 8,
            },
          ]}>
          {this.trimDescription(item.description)}
        </Text>
      </View>
    );
  };

  renderImage = (item) => {
    // if (this.state.selected === 'IMAGE') {

    if (item.attachmentIds.length > 5) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            this.props.profileRedirectionNavigation('IndividualFeedsPost', {
              id: item.id,
            })
          }>
          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 182, width: '50%'}}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
            <Image
              style={{height: 182, width: '50%'}}
              source={{uri: item.attachmentIds[1].attachmentUrl}}
            />
          </View>

          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 120, width: '33.33%'}}
              source={{uri: item.attachmentIds[2].attachmentUrl}}
            />
            <Image
              style={{height: 120, width: '33.33%'}}
              source={{uri: item.attachmentIds[3].attachmentUrl}}
            />

            <ImageBackground
              source={{uri: item.attachmentIds[4].attachmentUrl}}
              style={{height: 120, width: '57.8%'}}>
              <View
                style={{
                  height: 120,
                  width: '57.8%',
                  backgroundColor: COLORS.dark_900 + 'BF',
                  opacity: 0.95,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat-Bold',
                    fontSize: 28,
                    color: COLORS.white,
                  }}>
                  +{item.attachmentIds.length - 4}
                </Text>
              </View>
            </ImageBackground>
          </View>
        </TouchableOpacity>
      );
    } else if (item.attachmentIds.length === 5) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            this.props.profileRedirectionNavigation('IndividualFeedsPost', {
              id: item.id,
            })
          }>
          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 182, width: '50%'}}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
            <Image
              style={{height: 182, width: '50%'}}
              source={{uri: item.attachmentIds[1].attachmentUrl}}
            />
          </View>

          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 120, width: '33.33%'}}
              source={{uri: item.attachmentIds[2].attachmentUrl}}
            />
            <Image
              style={{height: 120, width: '33.33%'}}
              source={{uri: item.attachmentIds[3].attachmentUrl}}
            />
            <Image
              style={{height: 120, width: '33.33%'}}
              source={{uri: item.attachmentIds[4].attachmentUrl}}
            />
          </View>
        </TouchableOpacity>
      );
    } else if (item.attachmentIds.length === 4) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            this.props.profileRedirectionNavigation('IndividualFeedsPost', {
              id: item.id,
            })
          }>
          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[1].attachmentUrl}}
            />
          </View>

          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[2].attachmentUrl}}
            />
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[3].attachmentUrl}}
            />
          </View>
        </TouchableOpacity>
      );
    } else if (item.attachmentIds.length === 3) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            this.props.profileRedirectionNavigation('IndividualFeedsPost', {
              id: item.id,
            })
          }>
          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '100%'}}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
          </View>

          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[1].attachmentUrl}}
            />
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[2].attachmentUrl}}
            />
          </View>
        </TouchableOpacity>
      );
    } else if (item.attachmentIds.length === 2) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            this.props.profileRedirectionNavigation('IndividualFeedsPost', {
              id: item.id,
            })
          }>
          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '100%'}}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
          </View>

          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '100%'}}
              source={{uri: item.attachmentIds[1].attachmentUrl}}
            />
          </View>
        </TouchableOpacity>
      );
    } else if (item.attachmentIds.length === 1) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            item.postType === 'ARTICLE'
              ? this.props.profileRedirectionNavigation('EditorsDesk', {
                  id: item.id,
                  userId: this.state.userId,
                })
              : this.props.profileRedirectionNavigation('IndividualFeedsPost', {
                  id: item.id,
                })
          }
          // onPress={() => this.props.profileRedirectionNavigation('IndividualFeedsPost', { id: item.id })}
        >
          {/* {this.state.selected === 'AUDIO' && item.attachmentIds[0].attachmentUrl || this.state.selected === 'VIDEO' ? */}
          {item.attachmentIds[0].attachmentType === 'AUDIO' &&
          this.state.selected !== 'VIDEO' ? (
            <VideoPlayer
              style={{width: '97%', height: 190}}
              tapAnywhereToPause={true}
              disableFullscreen={true}
              disableSeekbar={true}
              disableVolume={true}
              disableTimer={true}
              disableBack={true}
              paused={true}
              audioOnly={true}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
          ) : item.attachmentIds[0].attachmentType === 'VIDEO' &&
            this.state.selected !== 'AUDIO' ? (
            <VideoPlayer
              style={{
                width: this.state.videoWidth,
                height: this.state.videoHeight,
              }}
              tapAnywhereToPause={true}
              disableSeekbar={true}
              disableVolume={true}
              disableTimer={true}
              disableBack={true}
              paused={true}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
              navigator={this.props.navigator}
            />
          ) : (
            <Image
              style={{
                height: item.postType === 'ARTICLE' ? 160 : 280,
                width: '100%',
                marginTop: 8,
              }}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
          )}
        </TouchableOpacity>
      );
    } else return <></>;
  };

  renderHashTags = (item) => {
    return (
      <ScrollView
        contentContainerStyle={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          marginLeft: 6,
          marginTop: 8,
        }}>
        {item.hashTags.map((hashTag, index) => (
          <View key={index}>
            <Text
              style={[
                typography.Subtitle_2,
                {color: COLORS.grey_350, marginRight: 8},
              ]}>
              #{hashTag}
            </Text>
          </View>
        ))}
      </ScrollView>
    );
  };

  renderFooter = (item) => {
    return (
      <View style={[styles.unreadNotiItem, {paddingRight: 26, marginTop: 8}]}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() =>
              this.setState(
                {pressedActivityId: item.id},
                this.handleLike(item.id, item.liked),
              )
            }
            activeOpacity={0.5}
            style={{alignItems: 'center', flexDirection: 'row'}}>
            <Icon
              name={item.liked ? 'Like_FL' : 'Like'}
              color={COLORS.green_500}
              size={14}
              style={{
                marginTop: Platform.OS === 'android' ? 10 : 0,
                marginHorizontal: 6,
              }}
            />
            <Text style={[{color: COLORS.altgreen_300}, typography.Caption]}>
              {item.likesCount}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() =>
              item.postType === 'ARTICLE'
                ? this.props.profileRedirectionNavigation('EditorsDesk', {
                    id: item.id,
                    userId: this.state.userId,
                  })
                : this.props.profileRedirectionNavigation(
                    'IndividualFeedsPost',
                    {id: item.id},
                  )
            }
            // onPress={() => this.props.profileRedirectionNavigation('IndividualFeedsPost', { id: item.id })}
            activeOpacity={0.5}
            style={{
              alignItems: 'center',
              flexDirection: 'row',
              marginLeft: 12,
            }}>
            <Icon
              name="Comment"
              color={COLORS.green_500}
              size={14}
              style={{
                marginTop: Platform.OS === 'android' ? 10 : 0,
                marginRight: 6,
              }}
            />
            <Text style={[{color: COLORS.altgreen_300}, typography.Caption]}>
              {item.commentCount}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  renderRecentStoryItem = (item) => {
    return (
      <View
        style={[
          defaultShape.card8_a,
          {
            height: 292,
            width: 250,
            marginLeft: 15,
            marginTop: 10,
            marginBottom: 10,
          },
        ]}>
        <Image
          source={{uri: item.item.imageUrl}}
          style={{
            height: 110,
            width: 250,
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
          }}
        />

        <View style={{paddingHorizontal: 15}}>
          <TouchableOpacity
            onPress={() =>
              this.props.webviewNavigation(
                'Webview',
                item.item.textData,
                item.item.newsUrl,
                item.item.siteName,
              )
            }>
            <Text
              numberOfLines={3}
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, marginTop: 10},
              ]}>
              {item.item.header}
            </Text>
          </TouchableOpacity>

          <Text
            numberOfLines={4}
            style={[
              typography.Note,
              {
                color: COLORS.altgreen_300,
                fontFamily: 'Montserrat-Medium',
                marginTop: 5,
              },
            ]}>
            {item.item.textData}
          </Text>
        </View>

        <View
          style={{
            position: 'absolute',
            bottom: 0,
            paddingVertical: 10,
            paddingHorizontal: 15,
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: 230,
          }}>
          <View style={{flexDirection: 'row'}}>
            <Icon name="Logo_Fitted" size={26} color={COLORS.altgreen_400} />
            <View style={{marginLeft: 8}}>
              <Text
                style={[
                  typography.Note,
                  {
                    color: COLORS.altgreen_400,
                    fontFamily: 'Montserrat-Medium',
                    marginTop: 5,
                  },
                ]}>
                Recent Stories
              </Text>
              <Text
                style={[
                  typography.Note,
                  {color: COLORS.grey_350, fontFamily: 'Montserrat-Medium'},
                ]}>
                31 aug, 2021
              </Text>
            </View>
          </View>

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={() => this.handleLike(item.item.id, item.item.liked)}
              style={{marginTop: 3, marginRight: 5}}>
              <Icon
                name={item.item.liked ? 'Like_FL' : 'Like'}
                size={16}
                color={COLORS.green_500}
                style={{marginTop: Platform.OS === 'ios' ? 5 : 0}}
              />
            </TouchableOpacity>
            <Text style={{fontSize: 14, color: COLORS.grey_350, marginTop: 8}}>
              {item.item.likesCount}
            </Text>
          </View>

          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={{marginTop: 3, marginRight: 5}}>
              <Icon
                name="Share"
                size={16}
                color={COLORS.green_500}
                style={{marginTop: Platform.OS === 'ios' ? 5 : 0}}
              />
            </TouchableOpacity>
            <Text style={{fontSize: 14, color: COLORS.grey_350, marginTop: 8}}>
              {item.item.sharesCount}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  topContributers = () => {
    return (
      <View style={{height: 139, width: 148, marginRight: 15}}>
        <View
          style={[
            defaultShape.card8_a,
            {
              height: 115,
              width: 148,
              position: 'absolute',
              bottom: 0,
              zIndex: 1,
            },
          ]}>
          <Image
            source={manImage}
            style={{
              height: 72,
              width: 72,
              borderRadius: 36,
              position: 'absolute',
              left: 5,
              top: -25,
              zIndex: 2,
            }}
          />

          <Image
            source={defaultCover}
            style={{
              height: 42,
              width: 148,
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
            }}
          />
          <Text
            style={[
              typography.Title_2,
              {color: '#002D3D', marginLeft: 10, marginTop: 10},
            ]}>
            Derek James
          </Text>
          <Text
            style={[
              typography.Subtitle_2,
              {color: COLORS.altgreen_300, marginLeft: 10},
            ]}>
            Head Naturalists
          </Text>
          <Text
            style={[
              typography.Note_2,
              {color: COLORS.grey_400, marginLeft: 10},
            ]}>
            587 followers
          </Text>
        </View>
      </View>
    );
  };

  onPressBlogs = () => {
    this.setState({selected: 'ARTICLE'}, () => this.getPost(this.state.userId));
  };
  onPressPhotos = () => {
    this.setState({selected: 'IMAGE'}, () => this.getPost(this.state.userId));
  };
  onPressVideos = () => {
    this.setState({selected: 'VIDEO'}, () => this.getPost(this.state.userId));
  };
  onPressCauses = () => {
    this.setState({selected: 'CAUSES', causes: [], causesPageNumber: 0}, () =>
      this.getCauses(),
    );
  };
  onPressCircle = () => {
    this.setState({selected: 'CIRCLE', circles: [], circlePageNumber: 0}, () =>
      this.getCircle(),
    );
  };

  peopleSharedModal = () => {
    return (
      <Modal
        visible={this.state.peopleSharedModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({peopleSharedModalOpen: false, peopleShared: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who Shared this post
              </Text>
            </View>

            <SharedUserList
              id={this.state.pressedActivityId}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {height: 700, bottom: 0},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={[defaultShape.CloseBtn, {marginBottom: 0}]}
            onPress={() =>
              this.setState({
                reasonForReportingModalOpen: false,
                reasonForReporting: '',
              })
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.altgreen_100,
              alignItems: 'center',
              paddingTop: 15,
              paddingBottom: 10,
              // paddingLeft: 20,
              width: '90%',
              alignSelf: 'center',
              marginBottom: 30,
              marginTop: 15,
            }}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Reason for reporting
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '94%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting:
                    'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting ===
                  'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Inappropriate, abusive or offensive content
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting: 'CONTENT_PROMOTING_VIOLENCE_OR_TERRORISM',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting ===
                  'CONTENT_PROMOTING_VIOLENCE_OR_TERRORISM' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Content promoting violence or terrorism
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingVertical: 15, borderBottomWidth: 0},
                    ]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'FAKE_SPAM_OR_SCAM'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Fake, spam or scam
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'ACCOUNT_MAY_BE_HACKED'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Account may be hacked
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '90.5%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting:
                    'DEFAMATION_TRADEMARK_OR_COPYRIGHT_VIOLATION',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting ===
                  'DEFAMATION_TRADEMARK_OR_COPYRIGHT_VIOLATION' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  numberOfLines={2}
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Defamation, trademark or copyright violation
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'HARASSMENT_OR_THREAT'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'HARASSMENT_OR_THREAT' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Harassment or threat
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'OTHERS'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'OTHERS' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Others
                </Text>
              </View>
            </TouchableOpacity>

            {this.state.reasonForReporting === 'OTHERS' && (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label="Write the details"
                  multiline
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Subtitle_1,
                    {
                      width: '90%',
                      height: 56,
                      backgroundColor: COLORS.altgreen_100,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) => this.setState({description: value})}
                  value={this.state.description}
                />
              </View>
            )}

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                this.state.reasonForReporting === 'OTHERS' &&
                this.state.description === ''
                  ? Snackbar.show({
                      backgroundColor: '#B22222',
                      text: 'Please enter the detail',
                      duration: Snackbar.LENGTH_LONG,
                    })
                  : this.state.reasonForReporting === ''
                  ? Snackbar.show({
                      backgroundColor: '#B22222',
                      text: 'Please select an option',
                      duration: Snackbar.LENGTH_LONG,
                    })
                  : (this.handleReportAbuseSubmit(),
                    this.setState({reasonForReportingModalOpen: false}));
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 15,
                height: 27,
                marginVertical: 10,
                borderRadius: 16,
                textAlign: 'center',
                borderWidth: 1,
                borderColor: '#698F8A',
              }}>
              <Text
                style={{
                  color: '#698F8A',
                  fontSize: 14,
                  paddingHorizontal: 14,
                  paddingVertical: 20,
                  fontFamily: 'Montserrat-Medium',
                  fontWeight: 'bold',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedActivityId,
      // entityType: this.state.entityType,
      entityType: 'POST',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    setTimeout(() => {
      this.setState({
        reasonForReporting: 'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT',
        description: '',
      });
    }, 1000);
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.optionsModal()}
        {this.likeModal()}
        {this.shareModal()}
        {this.peopleSharedModal()}
        {this.reasonForReportingModal()}
        <View
          style={{
            alignSelf: 'center',
            flexDirection: 'row',
            backgroundColor: COLORS.altgreen_t50,
            width: 320,
            marginTop: 20,
            justifyContent: 'center',
          }}>
          <TouchableOpacity
            onPress={this.onPressBlogs}
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor:
                  this.state.selected === 'ARTICLE'
                    ? '#fff'
                    : COLORS.altgreen_t50,
                width: 70,
              },
            ]}>
            <Text
              style={[
                typography.Caption,
                {
                  color:
                    this.state.selected === 'ARTICLE'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              Blogs
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.onPressPhotos}
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor:
                  this.state.selected === 'IMAGE'
                    ? '#fff'
                    : COLORS.altgreen_t50,
                width: 70,
              },
            ]}>
            <Text
              style={[
                typography.Caption,
                {
                  color:
                    this.state.selected === 'IMAGE'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              Photos
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.onPressVideos}
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor:
                  this.state.selected === 'VIDEO'
                    ? '#fff'
                    : COLORS.altgreen_t50,
                width: 70,
              },
            ]}>
            <Text
              style={[
                typography.Caption,
                {
                  color:
                    this.state.selected === 'VIDEO'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              Videos
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.onPressCircle}
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor:
                  this.state.selected === 'CIRCLE'
                    ? '#fff'
                    : COLORS.altgreen_t50,
                width: 70,
              },
            ]}>
            <Text
              style={[
                typography.Caption,
                {
                  color:
                    this.state.selected === 'CIRCLE'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              Circle
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.onPressCauses}
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor:
                  this.state.selected === 'CAUSES'
                    ? '#fff'
                    : COLORS.altgreen_t50,
                width: 70,
              },
            ]}>
            <Text
              style={[
                typography.Caption,
                {
                  color:
                    this.state.selected === 'CAUSES'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              Causes
            </Text>
          </TouchableOpacity>
        </View>

        <View style={{marginVertical: 10}}>
          <FlatList
            horizontal={
              this.state.selected === 'CAUSES' ||
              this.state.selected === 'CIRCLE'
                ? true
                : false
            }
            showsHorizontalScrollIndicator={false}
            style={{backgroundColor: COLORS.bgFill_200}}
            showsVerticalScrollIndicator={false}
            alwaysBounceVertical={false}
            keyExtractor={(item) => item.id}
            onEndReached={
              this.state.selected === 'CAUSES'
                ? this.handleLoadMoreCauses
                : this.state.selected === 'CIRCLE'
                ? this.handleLoadMoreCircle
                : null
            }
            onEndReachedThreshold={1}
            data={
              this.state.selected === 'CAUSES'
                ? this.state.causes
                : this.state.selected === 'CIRCLE'
                ? this.state.circles
                : this.state.post
            }
            ListFooterComponent={
              this.state.selected === 'CAUSES' ||
              this.state.selected === 'CIRCLE' ? (
                <></>
              ) : (
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation('SeeallTrendingPost', {
                      userId: this.state.userId,
                      type: this.state.selected === "IMAGE" ? "PHOTOS" : this.state.selected,
                    })
                  }
                  style={[
                    defaultShape.ContextBtn_OL,
                    {width: 120, marginVertical: 15, alignSelf: 'center'},
                  ]}>
                  <Text style={[typography.Caption, {color: COLORS.dark_500}]}>
                    See more
                  </Text>
                </TouchableOpacity>
              )
            }
            renderItem={({item}) => {
              return this.state.selected === 'CAUSES' ? (
                this.renderCauses(item)
              ) : this.state.selected === 'CIRCLE' ? (
                this.renderCircle(item)
              ) : (
                <View style={styles.renderItemStyle}>
                  {this.renderHeaderPost(item)}
                  {item.postType === 'ARTICLE' ? (
                    <>
                      {item.attachmentIds.length ? (
                        this.renderImage(item)
                      ) : (
                        <></>
                      )}
                      {item.description ? (
                        this.renderDescription(item)
                      ) : (
                        <View
                          style={{
                            backgroundColor: 'transparent',
                            height: 10,
                          }}></View>
                      )}
                    </>
                  ) : (
                    <>
                      {item.description ? (
                        this.renderDescription(item)
                      ) : (
                        <View
                          style={{
                            backgroundColor: 'transparent',
                            height: 10,
                          }}></View>
                      )}
                      {item.attachmentIds.length ? (
                        this.renderImage(item)
                      ) : (
                        <></>
                      )}
                    </>
                  )}

                  {item.hashTags.length ? this.renderHashTags(item) : <></>}

                  {this.renderFooter(item)}
                </View>
              );
            }}
          />
        </View>

        {/****** Top Contributers Starts ******/}

        {/* <View
          style={{
            backgroundColor: COLORS.primarydark,
            paddingVertical: 20,
            paddingHorizontal: 15,
          }}>
          <Text
            style={[
              typography.OVERLINE,
              {marginBottom: 10, color: COLORS.white},
            ]}>
            TOP CONTRIBUTERS
          </Text>

          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={[1, 2, 3, 4, 5, 6]}
            keyExtractor={(item) => item}
            renderItem={(item) => this.topContributers()}
          />
        </View> */}

        {/****** Top Contributers Ends ******/}

        {/****** RECENT STORIES Starts ******/}

        <View style={{marginTop: 10, marginBottom: 30}}>
          <View style={[styles.recentStoriesText]}>
            <Text style={[typography.OVERLINE, {color: COLORS.white}]}>
              RECENT STORIES
            </Text>
          </View>

          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{flexWrap: 'wrap', flexDirection: 'row'}}
            data={this.state.recentStory}
            keyExtractor={(item) => item.id}
            renderItem={(item) => this.renderRecentStoryItem(item)}
          />
        </View>

        {/****** RECENT STORIES Ends ******/}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  popularStoriesText: {
    paddingHorizontal: 5,
    paddingVertical: 2,
    backgroundColor: COLORS.dark_900,
    marginLeft: 15,
    borderRadius: 2,
    width: 130,
  },
  popularStoriesView: {
    width: 200,
    marginLeft: 15,
    borderLeftColor: COLORS.grey_300,
    borderLeftWidth: 2,
    height: 175,
    marginTop: 15,
    paddingLeft: 8,
  },
  recentStoriesText: {
    paddingHorizontal: 5,
    paddingVertical: 2,
    backgroundColor: COLORS.dark_900,
    marginLeft: 15,
    borderRadius: 2,
    width: 120,
  },
  popularTagsText: {
    paddingHorizontal: 5,
    paddingVertical: 2,
    backgroundColor: COLORS.altgreen_300,
    borderRadius: 2,
    width: 110,
  },
  unreadNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderRadius: 8,
  },
  renderItemStyle: {
    backgroundColor: COLORS.white,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginHorizontal: 10,
    marginVertical: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: COLORS.grey_300,
  },
  causesText: {
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
    position: 'absolute',
    top: 6,
    left: 8,
    zIndex: 2,
    width: 110,
  },
  causesImage: {
    width: 150,
    height: 150,
    borderRadius: 6,
  },
  individualCause: {
    marginHorizontal: 8,
    marginBottom: 10,
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },
  linearGradientView: {
    width: 150,
    height: 80,
    position: 'absolute',
    top: 0,
  },
});
