import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import Autolink from 'react-native-autolink';
import RNUrlPreview from 'react-native-url-preview';
import TimeAgo from 'react-native-timeago';

import {COLORS} from '../../../Components/Shared/Colors';
import BlogDefault from '../../../../assets/BlogDefault.jpg';
import typography from '../../../Components/Shared/Typography';
import defaultProfile from '../../../../assets/defaultProfile.png';
import circleDefault from '../../../../assets/CirclesDefault.png';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';
import ConnectDepth from '../../../Components/User/Common/ConnectDepth';
import Follow from '../../../Components/User/Common/Follow';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
export default class FeedsItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  trimDescription = (item) => {
    var trimmed = item.split('^^__').join(' ').indexOf('@@@__');

    var str = item
      .split('^^__')
      .join(' ')
      .indexOf(' ', item.split('^^__').join(' ').indexOf('@@@__'));

    var sub = item.substring(trimmed, str);

    item = item.replace(' ' + sub, '');
    item = item.replace('@@@^^^', ' ');
    item = item.replace('@@@__', ' ');
    item = item.replace('  ', '');

    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + ' s';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + ' m';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + ' h';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + ' d';
    }

    if (difference >= 864000) {
      return day + ' ' + month + ' ' + year;
    }
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderHeader = (item, index) => {
    return (
      <View style={styles.unreadNotiItem}>
        <View
          style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              item.userType === 'COMPANY'
                ? this.props.navigate([
                    'ProfileStack',
                    {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.userId},
                    },
                  ])
                : item.userType === 'INDIVIDUAL' &&
                  item.userId !== this.state.userId
                ? this.props.navigate([
                    'ProfileStack',
                    {
                      screen: 'OtherProfileScreen',
                      params: {userId: item.userId},
                    },
                  ])
                : item.userType === 'INDIVIDUAL' &&
                  item.userId === this.state.userId
                ? this.props.navigate([
                    'ProfileStack',
                    {
                      screen: 'ProfileScreen',
                      // params: {userId: item.userId},
                    },
                  ])
                : null;
            }}>
            <Image
              style={[defaultShape.Media_Round, {}]}
              source={
                item?.originalProfileImage
                  ? {uri: item.originalProfileImage}
                  : item.userType && item.userType === 'COMPANY'
                  ? defaultBusiness
                  : defaultProfile
              }
            />
          </TouchableOpacity>

          <View style={{alignItems: 'center', justifyContent: 'flex-start', marginLeft: 6}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                // justifyContent: 'flex-start',
              }}>
              <Text
                numberOfLines={1}
                onPress={() => {
                  item.userType === 'COMPANY'
                    ? this.props.navigate([
                        'ProfileStack',
                        {
                          screen: 'CompanyProfileScreen',
                          params: {userId: item.userId},
                        },
                      ])
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId !== this.state.userId
                    ? this.props.navigate([
                        'ProfileStack',
                        {
                          screen: 'OtherProfileScreen',
                          params: {userId: item.userId},
                        },
                      ])
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId === this.state.userId
                    ? this.props.navigate([
                        'ProfileStack',
                        {
                          screen: 'ProfileScreen',
                          // params: {userId: item.userId},
                        },
                      ])
                    : null;
                }}
                style={[
                  typography.Title_2,
                  {
                    color: COLORS.dark_800,
                    maxWidth: 160,
                  },
                ]}>
                {item &&
                  item.userName &&
                  item.userName.split(' ').splice(0, 2).join(' ')}
              </Text>

              {/* <Follow
                item={item}
                followed={item.followed}
                userId={item.userId}
                index={index}
              /> */}

              {item.userId !== this.state.userId && (
                <TouchableOpacity
                  onPress={() =>
                    this.props.handleFollowUnfollow(item.followed, item.userId)
                  }
                  style={{
                    height: 30,
                    width: 30,
                    alignItems: 'center',
                    marginLeft: 4,
                  }}>
                  <Icon
                    name={item.followed ? 'TickRSS' : 'RSS'}
                    size={13}
                    color={item.followed?COLORS.dark_600:COLORS.green_500}
                    style={{marginTop: Platform.OS === 'android' ? 4 : 2}}
                  />
                </TouchableOpacity>
              )}
            </View>

            <View
              style={{flexDirection: 'row'}}>
              {item && item.country ? (
                <Icon
                  name="Location"
                  color={COLORS.altgreen_300}
                  size={10}
                  style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                />
              ) : (
                <></>
              )}
              <Text
              numberOfLines={1}
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigate([
                        'EditorsDesk',
                        {
                          id: item.id,
                          userId: this.state.userId,
                        },
                      ])
                    : this.props.navigate([
                        'IndividualFeedsPost',
                        {
                          id: item.id,
                          commentCount: item.commentCount,
                        },
                      ])
                }
                style={[
                  {
                    color: COLORS.altgreen_300,
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                    maxWidth: 100,
                  },
                ]}>
                {item && item.country ? item.country : null}
                {/* {+ new Date() - item.createTime} */}
              </Text>
              {item && item.country ? (
                <Icon
                  name="Bullet_Fill"
                  color={COLORS.altgreen_300}
                  size={8}
                  style={{marginTop: Platform.OS === 'android' ? 1 : 2}}
                />
              ) : (
                <></>
              )}

              <Text
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigate([
                        'EditorsDesk',
                        {
                          id: item.id,
                          userId: this.state.userId,
                        },
                      ])
                    : this.props.navigate([
                        'IndividualFeedsPost',
                        {
                          id: item.id,
                          commentCount: item.commentCount,
                        },
                      ])
                }
                style={[
                  {
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                  },
                ]}>
                {/* {item && this.unixTime2(item.createTime)} */}
                <TimeAgo time={item.createTime}/>
                {/* {+ new Date() - item.createTime} */}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  renderDescription = (item) => {
    return (
      <Autolink
        text={this.trimDescription(item.description)}
        email
        hashtag="instagram"
        mention="twitter"
        phone="sms"
        numberOfLines={4}
        style={[
          typography.Body_1,
          {
            color:
              item.postType === 'ARTICLE' ? COLORS.dark_500 : COLORS.dark_700,
            marginLeft: 6,
            marginRight: 17,
            marginVertical: 8,
          },
        ]}
        url
      />
    );
  };

  renderSharedPost = (item, parentItem) => {
    return (
      <TouchableOpacity
        style={styles.renderSharedPost}
        onPress={() =>
          item.postType === 'ARTICLE'
            ? this.props.navigation.navigate('EditorsDesk', {
                id: item.id,
                userId: this.state.userId,
              })
            : this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
        }>
        {item.userId !== parentItem.userId && this.renderSharedHeader(item)}

        {item.news_url ? (
          <RNUrlPreview
            text={item.news_url}
            titleNumberOfLines={1}
            titleStyle={[typography.Title_2, {color: COLORS.dark_800}]}
            descriptionStyle={[
              typography.Note,
              {fontFamily: 'Montserrat-Medium', color: COLORS.altgreen_300},
            ]}
            containerStyle={{
              width: '100%',
              flexDirection: 'column',
              backgroundColor: COLORS.altgreen_100,
            }}
            imageStyle={{width: '100%', height: 150, marginTop: -26}}
            imageProps={{width: '100%', height: 150}}
          />
        ) : (
          <></>
        )}

        <View style={{width: '103%'}}>
          {item.postType === 'ARTICLE' ? (
            <>
              {item && item.attachmentIds && item.attachmentIds.length ? (
                this.renderImage(item)
              ) : (
                <></>
              )}
              {item.description ? (
                this.renderDescription(item)
              ) : (
                <View
                  style={{backgroundColor: 'transparent', height: 10}}></View>
              )}
            </>
          ) : (
            <>
              {item.description ? (
                this.renderDescription(item)
              ) : (
                <View
                  style={{backgroundColor: 'transparent', height: 10}}></View>
              )}
              {item && item.attachmentIds && item.attachmentIds.length ? (
                this.renderImage(item)
              ) : (
                <></>
              )}
            </>
          )}
          {item.question ? (
            <Text
              style={[typography.Button_Lead, {color: COLORS.dark_800}]}
              onPress={() =>
                this.props.navigation.navigate('PollStack', {
                  screen: 'PollDetails',
                  params: {slug: item.slug, userId: item.userId},
                })
              }>
              {item.question}
            </Text>
          ) : (
            <Text></Text>
          )}

          {item.postLocation ? (
            <Text
              style={[
                typography.H6,
                {
                  color: COLORS.dark_800,
                  marginLeft: 6,
                  marginRight: 17,
                  marginTop: 5,
                },
              ]}>
              <Icon
                name="Location"
                color={COLORS.altgreen_300}
                size={10}
                style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
              />
              {item.postLocation}
            </Text>
          ) : (
            <></>
          )}
        </View>

        <View>
          {item.secondaryAttachmentIds &&
          item.secondaryAttachmentIds.length > 0 ? (
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}
              onPress={() =>
                this.setState({attachmentModalopen: true, causeItem: item})
              }>
              <Icon
                name="Clip"
                color={COLORS.green_500}
                size={14}
                style={{
                  marginTop: Platform.OS === 'android' ? 10 : 0,
                  marginRight: 6,
                }}
              />
              <Text style={[{color: COLORS.altgreen_300}, defaultStyle.Note2]}>
                {item.secondaryAttachmentIds.length === 1
                  ? item.secondaryAttachmentIds.length + ' File '
                  : item.secondaryAttachmentIds.length + ' Files '}{' '}
                Attached
              </Text>
            </TouchableOpacity>
          ) : (
            <Text></Text>
          )}
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const {item, index} = this.props;
    return (
      <View style={styles.renderItemStyle}>
        {this.renderHeader(item.item, item.index)}
        {item.item &&
        item.item.description &&
        item.item.postType !== 'ARTICLE' ? (
          this.renderDescription(item.item)
        ) : (
          <View
            style={{
              backgroundColor: 'transparent',
              height: 10,
            }}></View>
        )}
        {item.item && item.item.postType === 'LINK' ? (
          <RNUrlPreview
            text={item.item.postLinkTypeUrl}
            titleNumberOfLines={1}
            titleStyle={[typography.Title_2, {color: COLORS.dark_800}]}
            descriptionStyle={[
              typography.Note,
              {
                fontFamily: 'Montserrat-Medium',
                color: COLORS.altgreen_300,
                marginTop: 8,
              },
            ]}
            containerStyle={{
              marginTop: 15,
              width: '100%',
              flexDirection: 'column',
              backgroundColor: COLORS.altgreen_100,
            }}
            imageStyle={{width: '100%', height: 150}}
            imageProps={{width: '100%', height: 150}}
          />
        ) : (
          <></>
        )}

        {item.item && item.item.sharedEntityId ? (
          this.renderSharedPost(item.item.sharedEntityParams, item.item)
        ) : item.item && item.item.attachmentIds.length ? (
          this.renderImage(item.item)
        ) : item.item.postType === 'ARTICLE' ? (
          <Image source={BlogDefault} style={{width: '100%', height: 150}} />
        ) : (
          <></>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  renderItemStyle: {
    backgroundColor: COLORS.white,
    marginHorizontal: 10,
    marginVertical: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: COLORS.grey_300,
    width: 300,
    height: 300,
  },
  unreadNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderRadius: 8,
    height: 60,
  },
});
