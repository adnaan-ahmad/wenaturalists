import React, { Component } from 'react';
import {
    ActivityIndicator,
    Modal,
    SafeAreaView,
    Text,
    View,
    TouchableOpacity,
    Image,
    StyleSheet,
    Dimensions,
    FlatList,
    ScrollView,
    Platform,
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { connect } from 'react-redux';
import Snackbar from 'react-native-snackbar';
import { TextInput, ProgressBar } from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-crop-picker';
import cloneDeep from 'lodash/cloneDeep';

import projectDefault from '../../../../assets/project-default.jpg';
import circleDefault from '../../../../assets/CirclesDefault.png';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import { personalBusinessPageRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions';
import defaultStyle from '../../../Components/Shared/Typography';
import { personalProfileRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions';
import {
    feedsPhotosRequest,
    feedsVideosRequest,
} from '../../../services/Redux/Actions/User/FeedsActions';
import { REACT_APP_userServiceURL } from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import httpService from '../../../services/AxiosInterceptors';
import { COLORS } from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import typography from '../../../Components/Shared/Typography';

httpService.setupInterceptors();

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

class EditPost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            profileimg: '',
            userName: '',
            attachments: [],
            imageList: [],
            videoList: [],
            audioList: [],
            secondaryList: [],
            progressInfos: [],
            connectionList: [],
            tickList: [],
            selectedConnection: [],
            infoModal: false,
            pageNumber: 0,
            pageSize: 1000,
            getConnectsModal: false,
            searchIcon: true,
            detailsModalOpen: false,
            circleList: [],
            causeList: [],
            projectList: [],
            pageProjectsModalOpen: false,
            pageCausesModalOpen: false,
            pageCirclesModalOpen: false,

            query: this.props.route.params.description,
            query2: '',
            linkUrl: '',
            linkIcon: true,
            addLink: false,
            hashTagModalOpen: false,
            hashTags: this.props.route.params.hashTags,
            keyPressed: '',
            whereToPostModalOpen: false,
            anyone: true,
            yourConnectsOnly: false,
            postAsUserName: true,
            postPrivacy: 'ANYONE',
            postAsModalOpen: false,
            searchValue: '',
            selectedPostAs: {},
            selectedPostIn: {},
            postAsCircleModalOpen: false,
            circleAdminList: [],
            postInFeeds: true,
        };
    }

    componentDidMount() {
        AsyncStorage.getItem('userId')
            .then((value) => {
                this.setState({ userId: value });
                this.props.personalBusinessPageRequest({
                    userId: value,
                    otherUserId: '',
                });
                this.getCircleAdminList(value);

                axios({
                    method: 'get',
                    url:
                        REACT_APP_userServiceURL +
                        '/profile/get?id=' +
                        value +
                        '&otherUserId=' +
                        '',
                    cache: true,
                    withCredentials: true,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                })
                    .then((response) => {
                        this.setState({
                            profileimg: response.data.body.originalProfileImage,
                            userName: response.data.body.userName,
                        });
                    })
                    .catch((err) => console.log('Profile data error : ', err));
            })
            .catch((e) => {
                console.log(e);
            });
    }

    documentPickerImage = async () => {
        try {
            const results = await DocumentPicker.pickMultiple({
                type: [DocumentPicker.types.images],
            });
            for (const res of results) {
                console.log(res);
                this.state.attachments.push(res);
                this.state.progressInfos.push({
                    percentage: 0,
                    fileName: res.name,
                    // url: URL.createObjectURL(e.target.files[i]),
                });
                this.setState({ ...this.state }, () =>
                    this.uploadImageAttachment(this.state.attachments.length - 1, res),
                );
            }
            // this.setState({ ...this.state },()=>this.uploadImageAttachment())
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    };

    uploadImageAttachment = (index, fileupload) => {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            formData.append('file', fileupload);
            formData.append('userId', this.state.userId);
            let attachmentTypeJson = 'IMAGE';
            formData.append('attachmentType', attachmentTypeJson);
            const onUploadProgress = (event) => {
                let progressInfos = cloneDeep(this.state.progressInfos);
                progressInfos[index].percentage = Math.round(
                    (100 * event.loaded) / event.total,
                );
                this.setState({ progressInfos: progressInfos });
            };
            axios({
                method: 'post',
                url: REACT_APP_userServiceURL + '/backend/post/uploadAttachment',
                data: formData,
                withCredentials: true,
                onUploadProgress,
            })
                .then((response) => {
                    if (response && response.data && response.data.body) {
                        let imageList = cloneDeep(this.state.imageList);
                        // let imagePreviewList = cloneDeep(this.state.imagePreviewList);
                        imageList = imageList ? imageList : [];
                        // imagePreviewList = imagePreviewList ? imagePreviewList : [];
                        imageList.push(response.data.body.id);
                        // imagePreviewList.push(response.data.body.attachmentUrl);

                        this.setState({
                            imageList: imageList,
                            // imagePreviewList: imagePreviewList,
                            // currentFile: response.data.body.attachmentUrl,
                        });

                        console.log('upload attachment image: ', response.data);
                    }
                    resolve();
                })
                .catch((e) => {
                    console.log(e);
                    reject(e);
                });
        });
    };

    documentPickerVideo = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.video],
            });
            console.log(res);
            this.state.attachments.push(res);
            this.state.progressInfos.push({
                percentage: 0,
                fileName: res.name,
                // url: URL.createObjectURL(e.target.files[i]),
            });
            this.setState({ ...this.state }, () => this.uploadVideoAttachment(res));
            // this.setState({ ...this.state },()=>this.uploadImageAttachment())
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    };

    // {"duration": 6400, "height": 360, "mime": "video/mp4", "modificationDate": "1624037482000", "path": "file:///data/user/0/com.wenaturalists/cache/react-native-image-crop-picker/VID-20210618-WA0008.mp4", "size": 234098, "width": 360}
    // {"fileCopyUri": "content://com.android.providers.downloads.documents/document/raw%3A%2Fstorage%2Femulated%2F0%2FDownload%2Fimages123.jpeg", "name": "images123.jpeg", "size": 64439, "type": "image/jpeg", "uri": "content://com.android.providers.downloads.documents/document/raw%3A%2Fstorage%2Femulated%2F0%2FDownload%2Fimages123.jpeg"}

    uploadVideoAttachment = (fileupload) => {
        console.log('file upload : ', fileupload);
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            formData.append('file', fileupload);
            formData.append('userId', this.state.userId);
            let attachmentTypeJson = 'VIDEO';
            formData.append('attachmentType', attachmentTypeJson);
            const onUploadProgress = (event) => {
                let progressInfos = cloneDeep(this.state.progressInfos);
                progressInfos[0].percentage = Math.round(
                    (100 * event.loaded) / event.total,
                );
                this.setState({ progressInfos: progressInfos });
            };
            axios({
                method: 'post',
                url: REACT_APP_userServiceURL + '/backend/post/uploadAttachment',
                data: formData,
                withCredentials: true,
                onUploadProgress,
            })
                .then((response) => {
                    if (response && response.data && response.data.body) {
                        let videoList = cloneDeep(this.state.videoList);
                        // let imagePreviewList = cloneDeep(this.state.imagePreviewList);
                        videoList = videoList ? videoList : [];
                        // imagePreviewList = imagePreviewList ? imagePreviewList : [];
                        videoList.push(response.data.body.id);
                        // imagePreviewList.push(response.data.body.attachmentUrl);

                        this.setState({
                            videoList: videoList,
                            // imagePreviewList: imagePreviewList,
                            // currentFile: response.data.body.attachmentUrl,
                        });

                        console.log('upload attachment video: ', response.data);
                    }
                    resolve();
                })
                .catch((e) => {
                    console.log('error video upload', e);
                    reject(e);
                });
        });
    };

    documentPickerAudio = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.audio],
            });
            console.log(res);
            this.state.attachments.push(res);
            this.state.progressInfos.push({
                percentage: 0,
                fileName: res.name,
                // url: URL.createObjectURL(e.target.files[i]),
            });
            this.setState({ ...this.state }, () => this.uploadAudioAttachment(res));
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    };

    uploadAudioAttachment = (fileupload) => {
        console.log('file upload : ', fileupload);
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            formData.append('file', fileupload);
            formData.append('userId', this.state.userId);
            let attachmentTypeJson = 'AUDIO';
            formData.append('attachmentType', attachmentTypeJson);
            const onUploadProgress = (event) => {
                let progressInfos = cloneDeep(this.state.progressInfos);
                progressInfos[0].percentage = Math.round(
                    (100 * event.loaded) / event.total,
                );
                this.setState({ progressInfos: progressInfos });
            };
            axios({
                method: 'post',
                url: REACT_APP_userServiceURL + '/backend/post/uploadAttachment',
                data: formData,
                withCredentials: true,
                onUploadProgress,
            })
                .then((response) => {
                    if (response && response.data && response.data.body) {
                        let audioList = cloneDeep(this.state.audioList);
                        // let imagePreviewList = cloneDeep(this.state.imagePreviewList);
                        audioList = audioList ? audioList : [];
                        // imagePreviewList = imagePreviewList ? imagePreviewList : [];
                        audioList.push(response.data.body.id);
                        // imagePreviewList.push(response.data.body.attachmentUrl);

                        this.setState({
                            audioList: audioList,
                            // imagePreviewList: imagePreviewList,
                            // currentFile: response.data.body.attachmentUrl,
                        });

                        console.log('upload attachment audio: ', response.data);
                    }
                    resolve();
                })
                .catch((e) => {
                    console.log('error audio upload', e);
                    reject(e);
                });
        });
    };

    documentPickerSecondary = async () => {
        try {
            const results = await DocumentPicker.pickMultiple({
                type: [
                    DocumentPicker.types.csv,
                    DocumentPicker.types.xls,
                    DocumentPicker.types.xlsx,
                    DocumentPicker.types.doc,
                    DocumentPicker.types.docx,
                    DocumentPicker.types.ppt,
                    DocumentPicker.types.pptx,
                    DocumentPicker.types.plainText,
                    DocumentPicker.types.pdf,
                ],
            });
            for (const res of results) {
                console.log(res);
                this.state.attachments.push(res);
                this.state.progressInfos.push({
                    percentage: 0,
                    fileName: res.name,
                    // url: URL.createObjectURL(e.target.files[i]),
                });
                this.setState({ ...this.state }, () =>
                    this.uploadSecondaryAttachment(
                        this.state.attachments.length - 1,
                        res,
                    ),
                );
            }
            // this.setState({ ...this.state },()=>this.uploadImageAttachment())
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    };

    uploadSecondaryAttachment = (index, fileupload) => {
        return new Promise((resolve, reject) => {
            const formData = new FormData();

            formData.append('file', fileupload);
            formData.append('userId', this.state.userId);
            let attachmentTypeJson = 'UNKNOWN';
            formData.append('attachmentType', attachmentTypeJson);
            const onUploadProgress = (event) => {
                let progressInfos = cloneDeep(this.state.progressInfos);
                progressInfos[index].percentage = Math.round(
                    (100 * event.loaded) / event.total,
                );
                this.setState({ progressInfos: progressInfos });
            };
            axios({
                method: 'post',
                url: REACT_APP_userServiceURL + '/backend/post/uploadAttachment',
                data: formData,
                withCredentials: true,
                onUploadProgress,
            })
                .then((response) => {
                    if (response && response.data && response.data.body) {
                        let secondaryList = cloneDeep(this.state.secondaryList);
                        // let imagePreviewList = cloneDeep(this.state.imagePreviewList);
                        secondaryList = secondaryList ? secondaryList : [];
                        // imagePreviewList = imagePreviewList ? imagePreviewList : [];
                        secondaryList.push(response.data.body.id);
                        // imagePreviewList.push(response.data.body.attachmentUrl);

                        this.setState({
                            secondaryList: secondaryList,
                            // imagePreviewList: imagePreviewList,
                            // currentFile: response.data.body.attachmentUrl,
                        });

                        console.log('upload secondary attachment: ', response.data);
                    }
                    resolve();
                })
                .catch((e) => {
                    console.log(e);
                    reject(e);
                });
        });
    };

    getConnects = (page, size) => {
        // return new Promise((resolve) => {
        axios({
            method: 'get',
            url:
                REACT_APP_userServiceURL +
                '/network/' +
                this.state.userId +
                '/connects' +
                '?page=' +
                page +
                '&size=' +
                size,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.data && response.data.message === 'Success!') {
                    // resolve(response.data.body.content);
                    this.setState({ connectionList: response.data.body.content });
                    console.log('get connects: ', response.data.body.content);
                }
            })
            .catch((err) => {
                console.log(err);
            });
        // });
    };

    loadMoreConnects = () => {
        this.setState({ pageSize: this.state.pageSize + 10 }, () =>
            this.getConnects(this.state.pageNumber, this.state.pageSize),
        );
    };

    getUserId = () => {
        if (this.state.selectedPostAs && this.state.selectedPostAs.companyId)
            return this.state.selectedPostAs.companyId;
        else if (this.state.selectedPostAs && this.state.selectedPostAs.id)
            return this.state.selectedPostAs.id;
        return this.state.userId;
    };

    getEntityId = () => {
        if (this.state.selectedPostIn && this.state.selectedPostIn.id)
            return this.state.selectedPostIn.id;
        else if (this.state.selectedPostAs && this.state.selectedPostAs.companyId)
            return null;
        else if (this.state.selectedPostAs && this.state.selectedPostAs.id)
            return this.state.selectedPostAs.id;
        return null;
    };

    getPostAsName = () => {
        if (this.state.selectedPostAs && this.state.selectedPostAs.companyId)
            return this.state.selectedPostAs.companyName;
        else if (this.state.selectedPostAs && this.state.selectedPostAs.title)
            return this.state.selectedPostAs.title;
        return this.state.userName;
    };

    getImage = () => {
        if (this.state.selectedPostAs && this.state.selectedPostAs.companyId) {
            if (this.state.selectedPostAs.profileImageUrl)
                return { uri: this.state.selectedPostAs.profileImageUrl };
            else return defaultBusiness;
        } else if (this.state.selectedPostAs && this.state.selectedPostAs.title) {
            if (this.state.selectedPostAs.profileImage)
                return { uri: this.state.selectedPostAs.profileImage };
            else return circleDefault;
        }
        return this.state.profileimg
            ? { uri: this.state.profileimg }
            : defaultProfile;
    };

    handlePostSubmit = () => {
        const formData = new FormData();

        let params = {
            creatorId: this.state.userId,
            userId: this.getUserId(),
            entityId: this.getEntityId(),
            description: this.state.query.trim('\n'),
            postType: 'POST',
            hashTags: this.state.hashTags,
            postVisibility: this.state.postPrivacy,
            visibleToUserIds: this.state.tickList,
            forcePost: false,
        };

        let linkParams = {
            creatorId: this.state.userId,
            userId: this.getUserId(),
            entityId: this.getEntityId(),
            description: this.state.query.trim('\n'),
            postType: 'LINK',
            hashTags: this.state.hashTags,
            postVisibility: this.state.postPrivacy,
            visibleToUserIds: this.state.tickList,
            postLinkTypeUrl: this.state.linkUrl,
            forcePost: false,
        };

        if (this.state.imageList && this.state.imageList.length > 0) {
            params.attachmentType = 'IMAGE';
            params.attachmentIds = this.state.imageList;
        } else if (this.state.videoList && this.state.videoList.length > 0) {
            params.attachmentType = 'VIDEO';
            params.attachmentIds = this.state.videoList;
        } else if (this.state.audioList && this.state.audioList.length > 0) {
            params.attachmentType = 'AUDIO';
            params.attachmentIds = this.state.audioList;
        } else if (
            this.state.secondaryList &&
            this.state.secondaryList.length > 0
        ) {
            // params.attachmentType = 'UNKNOWN';
            params.secondaryAttachmentIds = this.state.secondaryList;
        }

        this.state.addLink
            ? formData.append('data', JSON.stringify(linkParams))
            : formData.append('data', JSON.stringify(params));

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/post/createUpdated',
            data: formData,
            withCredentials: true,
        })
            .then((response) => {
                console.log('post done : ', response.data);
                Snackbar.show({
                    backgroundColor: COLORS.dark_900,
                    text: 'Post uploaded successfully',
                    textColor: COLORS.altgreen_100,
                    duration: Snackbar.LENGTH_LONG,
                });
                this.props.navigation.goBack();
            })
            .catch((err) => console.log('post in feeds data error : ', err.response));
    };

    selectedConnects = (id, index) => {
        let tempTickList = cloneDeep(this.state.tickList);
        // let tempSelectedConnection = cloneDeep(this.state.selectedConnection)
        if (this.state.tickList.includes(id)) {
            tempTickList.splice(tempTickList.indexOf(id), 1);
            // tempSelectedConnection.splice(tempTickList.indexOf(id), 1)
            this.setState({ tickList: tempTickList });
        } else {
            this.setState({ tickList: [...this.state.tickList, id] });
        }
    };

    hashTagModal = () => {
        return (
            <Modal
                visible={this.state.hashTagModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View
                        style={{
                            width: '100%',
                            height: 500,
                            position: 'absolute',
                            bottom: 0,
                            alignSelf: 'center',
                        }}>
                        <LinearGradient
                            colors={['#154A5900', '#154A59CC', '#154A59']}
                            style={{
                                flex: 1,
                                paddingLeft: 15,
                                paddingRight: 15,
                                borderBottomLeftRadius: 6,
                                borderBottomRightRadius: 6,
                            }}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ hashTagModalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color="#367681"
                            style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                        />
                    </TouchableOpacity>

                    <View
                        style={[defaultShape.Modal_Categories_Container, { height: 350 }]}>
                        <Text
                            style={[
                                typography.Caption,
                                {
                                    color: COLORS.altgreen_400,
                                    textAlign: 'center',
                                    marginBottom: 12,
                                },
                            ]}>
                            Type a hashtags and then add space to {`\n`} automatically start a
                            new hashtag.
                        </Text>

                        <FlatList
                            // keyboardDismissMode="on-drag"
                            keyboardShouldPersistTaps="handled"
                            scrollEventThrottle={0}
                            ref={(ref) => (this.scrollView = ref)}
                            onContentSizeChange={() => {
                                this.scrollView.scrollToEnd({ animated: false });
                                this.setState({ hashTagModalOpen: true });
                            }}
                            columnWrapperStyle={{
                                flexDirection: 'row',
                                flexWrap: 'wrap',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                            numColumns={3}
                            showsVerticalScrollIndicator={false}
                            data={[...this.state.hashTags, 'lastData']}
                            keyExtractor={(item, index) => item + index}
                            renderItem={({ item, index }) =>
                                index < this.state.hashTags.length ? (
                                    <TouchableOpacity
                                        onPress={() =>
                                            this.setState({
                                                hashTags: this.state.hashTags.filter(
                                                    (value, index2) => value + index2 !== item + index,
                                                ),
                                            })
                                        }
                                        activeOpacity={0.6}
                                        style={{
                                            flexDirection: 'row',
                                            backgroundColor: COLORS.altgreen_t50 + '80',
                                            paddingHorizontal: 10,
                                            marginVertical: 6,
                                            height: 28,
                                            borderRadius: 17,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginRight: 6,
                                        }}>
                                        <Text
                                            style={[
                                                typography.Caption,
                                                {
                                                    color: COLORS.dark_500,
                                                    marginRight: 4,
                                                    marginTop: Platform.OS === 'ios' ? -2 : 0,
                                                },
                                            ]}>
                                            {item}
                                        </Text>
                                        <Icon
                                            name="Cross_Rounded"
                                            color={COLORS.dark_500}
                                            size={15}
                                            style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                                        />
                                    </TouchableOpacity>
                                ) : (
                                    <TextInput
                                        theme={{
                                            colors: {
                                                text: COLORS.dark_600,
                                                primary: COLORS.altgreen_300,
                                                placeholder: COLORS.altgreen_300,
                                            },
                                        }}
                                        // label="Write something interesting"
                                        placeholder="Add Hashtag"
                                        autoFocus={true}
                                        selectionColor="#C8DB6E"
                                        style={[
                                            typography.H6,
                                            {
                                                height: 28,
                                                backgroundColor: COLORS.white,
                                                color: COLORS.dark_600,
                                                textAlign: 'center',
                                                marginVertical: 6,
                                            },
                                        ]}
                                        onChangeText={(value) => {
                                            this.setState({ query2: value.trim() });
                                            value[value.length - 1] === ' ' && value.trim()
                                                ? this.setState({
                                                    hashTags: [...this.state.hashTags, value.trim()],
                                                    query2: '',
                                                })
                                                : null;
                                        }}
                                        value={this.state.query2}
                                    />
                                )
                            }
                        />

                        <View
                            style={{
                                width: '80%',
                                alignSelf: 'center',
                                backgroundColor: this.state.hashTags.length
                                    ? COLORS.green_300
                                    : COLORS.grey_200,
                                height: 2,
                            }}></View>

                        <TouchableOpacity
                            onPress={() => this.setState({ hashTagModalOpen: false })}
                            activeOpacity={0.7}
                            style={[
                                defaultShape.ContextBtn_FL_Drk,
                                { paddingHorizontal: 20, marginVertical: 10 },
                            ]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_300 }]}>
                                Done
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    };

    whereToPostModal = () => {
        return (
            <Modal
                visible={this.state.whereToPostModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View
                        style={[
                            defaultShape.Linear_Gradient_View,
                            { height: 700, bottom: 0 },
                        ]}>
                        <LinearGradient
                            colors={[
                                COLORS.dark_800 + '00',
                                COLORS.dark_800 + 'CC',
                                COLORS.dark_800,
                            ]}
                            style={defaultShape.Linear_Gradient}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ whereToPostModalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color={COLORS.dark_600}
                            style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
                        />
                    </TouchableOpacity>

                    <View
                        style={{
                            borderRadius: 20,
                            backgroundColor: COLORS.white,
                            alignItems: 'center',
                            paddingTop: 15,
                            paddingBottom: 10,
                            width: '90%',
                            alignSelf: 'center',
                        }}>
                        <View
                            style={[
                                defaultShape.ActList_Cell_Gylph_Alt,
                                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
                            ]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                                Choose where to post
                            </Text>
                        </View>

                        <TouchableOpacity
                            style={
                                Platform.OS === 'ios'
                                    ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                    : [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                            }
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState(
                                    {
                                        whereToPostModalOpen: false,
                                        postAsUserName: true,
                                        detailsModalOpen: true,
                                        selectedPostAs: this.state.postAsUserName && {},
                                    },
                                    () => {
                                        this.getCircleList(),
                                            this.getCauseList(),
                                            this.getProjectList();
                                    },
                                );
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    marginLeft: -6,
                                }}>
                                <Image
                                    source={
                                        this.state.profileimg !== null &&
                                            this.state.profileimg !== ''
                                            ? { uri: this.state.profileimg }
                                            : defaultProfile
                                    }
                                    style={{
                                        width: 28,
                                        height: 28,
                                        borderRadius: 14,
                                    }}
                                />
                                <Text
                                    style={[
                                        typography.Button_Lead,
                                        { color: COLORS.dark_600, marginLeft: 8 },
                                    ]}>
                                    Post as{' '}
                                    <Text style={[typography.H6, { color: COLORS.dark_800 }]}>
                                        {this.state.userName}
                                    </Text>
                                </Text>
                            </View>
                            {/* <TouchableOpacity onPress={() => this.setState({ postAsUserName: !this.state.postAsUserName, selectedPostAs: this.state.postAsUserName && {} })} style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.green_500, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: this.state.postAsUserName ? COLORS.green_500 : COLORS.altgreen_t50 }}>
                                {this.state.postAsUserName ? <Icon name="Tick" size={10} color={COLORS.dark_800} style={{ marginLeft: 0, marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>}
                            </TouchableOpacity> */}
                            <Icon
                                name="Arrow_Right"
                                size={16}
                                color={COLORS.altgreen_300}
                                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={
                                Platform.OS === 'ios'
                                    ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                    : defaultShape.ActList_Cell_Gylph_Alt
                            }
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({
                                    whereToPostModalOpen: false,
                                    postAsModalOpen: true,
                                });
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <Icon
                                    name="Business"
                                    size={18}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                                <Text
                                    style={[
                                        typography.Button_Lead,
                                        { color: COLORS.dark_600, marginLeft: 8 },
                                    ]}>
                                    Organization Pages
                                </Text>
                            </View>
                            <Icon
                                name="Arrow_Right"
                                size={16}
                                color={COLORS.altgreen_300}
                                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={
                                Platform.OS === 'ios'
                                    ? [
                                        defaultShape.ActList_Cell_Gylph_Alt,
                                        { paddingVertical: 15, borderBottomWidth: 0 },
                                    ]
                                    : [
                                        defaultShape.ActList_Cell_Gylph_Alt,
                                        { borderBottomWidth: 0 },
                                    ]
                            }
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({
                                    whereToPostModalOpen: false,
                                    postAsCircleModalOpen: true,
                                });
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <Icon
                                    name="Circle_Ol"
                                    size={18}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                                <Text
                                    style={[
                                        typography.Button_Lead,
                                        { color: COLORS.dark_600, marginLeft: 8 },
                                    ]}>
                                    Circles
                                </Text>
                            </View>
                            <Icon
                                name="Arrow_Right"
                                size={16}
                                color={COLORS.altgreen_300}
                                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                            />
                        </TouchableOpacity>
                    </View>

                    <View
                        style={{
                            borderRadius: 20,
                            backgroundColor: COLORS.altgreen_100,
                            alignItems: 'center',
                            paddingTop: 15,
                            paddingBottom: 10,
                            width: '90%',
                            alignSelf: 'center',
                            marginBottom: 30,
                            marginTop: 15,
                        }}>
                        <View
                            style={[
                                defaultShape.ActList_Cell_Gylph_Alt,
                                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
                            ]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                                Who can see your post?
                            </Text>
                        </View>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    anyone: true,
                                    yourConnectsOnly: false,
                                    postPrivacy: 'ANYONE',
                                    tickList: [],
                                });
                            }}
                            style={
                                Platform.OS === 'ios'
                                    ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                    : defaultShape.ActList_Cell_Gylph_Alt
                            }
                            activeOpacity={0.6}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <Icon
                                    name="Globe2"
                                    size={18}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 9 } : {}}
                                />
                                <Text
                                    style={[
                                        typography.Button_Lead,
                                        { color: COLORS.dark_600, marginLeft: 8 },
                                    ]}>
                                    Anyone
                                </Text>
                            </View>
                            <TouchableOpacity
                                onPress={() =>
                                    this.setState({
                                        anyone: true,
                                        yourConnectsOnly: false,
                                        postPrivacy: 'ANYONE',
                                        tickList: [],
                                    })
                                }
                                style={{
                                    width: 17,
                                    height: 17,
                                    borderRadius: 8.5,
                                    borderColor: COLORS.altgreen_300,
                                    borderWidth: 2,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: COLORS.altgreen_200,
                                }}>
                                {this.state.anyone ? (
                                    <Icon
                                        name="Bullet_Fill"
                                        size={17}
                                        color={COLORS.dark_800}
                                        style={{
                                            marginLeft: -2,
                                            marginTop: Platform.OS === 'android' ? 8 : 0,
                                        }}
                                    />
                                ) : (
                                    <></>
                                )}
                            </TouchableOpacity>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    anyone: false,
                                    yourConnectsOnly: true,
                                    postPrivacy: 'CONNECTED',
                                    tickList: [],
                                });
                            }}
                            style={
                                Platform.OS === 'ios'
                                    ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                    : defaultShape.ActList_Cell_Gylph_Alt
                            }
                            activeOpacity={0.6}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <Icon
                                    name="Network_F"
                                    size={18}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                                <Text
                                    style={[
                                        typography.Button_Lead,
                                        { color: COLORS.dark_600, marginLeft: 8 },
                                    ]}>
                                    Your Connects only
                                </Text>
                            </View>
                            <TouchableOpacity
                                onPress={() =>
                                    this.setState({
                                        anyone: false,
                                        yourConnectsOnly: true,
                                        postPrivacy: 'CONNECTED',
                                        tickList: [],
                                    })
                                }
                                style={{
                                    width: 17,
                                    height: 17,
                                    borderRadius: 8.5,
                                    borderColor: COLORS.altgreen_300,
                                    borderWidth: 2,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: COLORS.altgreen_200,
                                }}>
                                {this.state.yourConnectsOnly ? (
                                    <Icon
                                        name="Bullet_Fill"
                                        size={17}
                                        color={COLORS.dark_800}
                                        style={{
                                            marginLeft: -2,
                                            marginTop: Platform.OS === 'android' ? 8 : 0,
                                        }}
                                    />
                                ) : (
                                    <></>
                                )}
                            </TouchableOpacity>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() =>
                                this.setState(
                                    {
                                        whereToPostModalOpen: false,
                                        getConnectsModal: true,
                                        postPrivacy: 'SELECTED',
                                        anyone: false,
                                        yourConnectsOnly: false,
                                    },
                                    () => this.getConnectsModal(),
                                )
                            }
                            style={
                                Platform.OS === 'ios'
                                    ? [
                                        defaultShape.ActList_Cell_Gylph_Alt,
                                        { paddingVertical: 15, borderBottomWidth: 0 },
                                    ]
                                    : [
                                        defaultShape.ActList_Cell_Gylph_Alt,
                                        { borderBottomWidth: 0 },
                                    ]
                            }
                            activeOpacity={0.6}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <Icon
                                    name={
                                        this.state.postPrivacy === 'SELECTED'
                                            ? 'TickedUser'
                                            : 'AddUser'
                                    }
                                    size={18}
                                    color={
                                        this.state.postPrivacy === 'SELECTED'
                                            ? COLORS.dark_600
                                            : COLORS.altgreen_300
                                    }
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                                <Text
                                    style={[
                                        typography.Button_Lead,
                                        { color: COLORS.dark_600, marginLeft: 8 },
                                    ]}>
                                    Select Connects
                                </Text>
                            </View>
                            <Icon
                                name="Arrow_Right"
                                size={16}
                                color={COLORS.altgreen_300}
                                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    };

    infoModal = () => {
        return (
            <Modal
                visible={this.state.infoModal}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View
                    style={{
                        alignSelf: 'center',
                        height: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                    <View
                        style={[
                            defaultShape.Linear_Gradient_View,
                            { height: 700, bottom: 0 },
                        ]}>
                        <LinearGradient
                            colors={[
                                COLORS.dark_800 + '00',
                                COLORS.dark_800 + 'CC',
                                COLORS.dark_800,
                            ]}
                            style={defaultShape.Linear_Gradient}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ infoModal: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color={COLORS.dark_600}
                            style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
                        />
                    </TouchableOpacity>

                    <View
                        style={{
                            borderRadius: 20,
                            backgroundColor: COLORS.white,
                            paddingHorizontal: 15,
                            paddingTop: 15,
                            paddingBottom: 10,
                            width: '80%',
                        }}>
                        <Text
                            style={[
                                typography.Title_2,
                                { color: COLORS.dark_600, alignSelf: 'center' },
                            ]}>
                            Supporting media file type
                        </Text>
                        <Text
                            style={[
                                typography.Caption,
                                {
                                    color: COLORS.altgreen_400,
                                    alignSelf: 'center',
                                    marginTop: 10,
                                },
                            ]}>
                            You can enhance your WeNaturalists
                        </Text>
                        <Text
                            style={[
                                typography.Caption,
                                { color: COLORS.altgreen_400, alignSelf: 'center' },
                            ]}>
                            experience by adding and sharing
                        </Text>
                        <Text
                            style={[
                                typography.Caption,
                                { color: COLORS.altgreen_400, alignSelf: 'center' },
                            ]}>
                            media samples
                        </Text>

                        <Text
                            style={[
                                typography.Note,
                                {
                                    color: COLORS.dark_600,
                                    fontFamily: 'Montserrat-SemiBold',
                                    marginTop: 10,
                                },
                            ]}>
                            The following file formats of media samples are supported:
                        </Text>
                        <View>
                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <Text style={{ color: COLORS.altgreen_300 }}>{'\u2022'}</Text>
                                <Text
                                    style={[
                                        typography.Note,
                                        {
                                            color: COLORS.altgreen_300,
                                            fontFamily: 'Montserrat-SemiBold',
                                            marginLeft: 5,
                                        },
                                    ]}>
                                    Adobe pdf (.pdf)
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 2 }}>
                                <Text style={{ color: COLORS.altgreen_300 }}>{'\u2022'}</Text>
                                <Text
                                    style={[
                                        typography.Note,
                                        {
                                            color: COLORS.altgreen_300,
                                            fontFamily: 'Montserrat-SemiBold',
                                            marginLeft: 5,
                                        },
                                    ]}>
                                    Microsoft Powerpoint (.ppt/.pptx)
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 2 }}>
                                <Text style={{ color: COLORS.altgreen_300 }}>{'\u2022'}</Text>
                                <Text
                                    style={[
                                        typography.Note,
                                        {
                                            color: COLORS.altgreen_300,
                                            fontFamily: 'Montserrat-SemiBold',
                                            marginLeft: 5,
                                        },
                                    ]}>
                                    Microsoft Excel (.xls/.xlsx)
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 2 }}>
                                <Text style={{ color: COLORS.altgreen_300 }}>{'\u2022'}</Text>
                                <Text
                                    style={[
                                        typography.Note,
                                        {
                                            color: COLORS.altgreen_300,
                                            fontFamily: 'Montserrat-SemiBold',
                                            marginLeft: 5,
                                        },
                                    ]}>
                                    Microsoft Word (.doc/.docx)
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 2 }}>
                                <Text style={{ color: COLORS.altgreen_300 }}>{'\u2022'}</Text>
                                <Text
                                    style={[
                                        typography.Note,
                                        {
                                            color: COLORS.altgreen_300,
                                            fontFamily: 'Montserrat-SemiBold',
                                            marginLeft: 5,
                                        },
                                    ]}>
                                    Other Documents (.csv/.txt)
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 2 }}>
                                <Text style={{ color: COLORS.altgreen_300 }}>{'\u2022'}</Text>
                                <Text
                                    style={[
                                        typography.Note,
                                        {
                                            color: COLORS.altgreen_300,
                                            fontFamily: 'Montserrat-SemiBold',
                                            marginLeft: 5,
                                        },
                                    ]}>
                                    Image formats (.jpg/.jpeg/.png)
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 2 }}>
                                <Text style={{ color: COLORS.altgreen_300 }}>{'\u2022'}</Text>
                                <Text
                                    style={[
                                        typography.Note,
                                        {
                                            color: COLORS.altgreen_300,
                                            fontFamily: 'Montserrat-SemiBold',
                                            marginLeft: 5,
                                        },
                                    ]}>
                                    Audio formats (.mp3/.ogg/.mpeg/.aac/.wav)
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 2 }}>
                                <Text style={{ color: COLORS.altgreen_300 }}>{'\u2022'}</Text>
                                <Text
                                    style={[
                                        typography.Note,
                                        {
                                            color: COLORS.altgreen_300,
                                            fontFamily: 'Montserrat-SemiBold',
                                            marginLeft: 5,
                                        },
                                    ]}>
                                    Video formats (.mp4/.m4a/.webm/.mkv/.flv)
                                </Text>
                            </View>
                        </View>

                        <Text
                            style={[
                                typography.Note,
                                {
                                    color: COLORS.dark_600,
                                    fontFamily: 'Montserrat-SemiBold',
                                    marginTop: 10,
                                },
                            ]}>
                            Important
                        </Text>
                        <View>
                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <Text style={{ color: COLORS.altgreen_300 }}>{'\u2022'}</Text>
                                <Text
                                    style={[
                                        typography.Note,
                                        {
                                            color: COLORS.altgreen_300,
                                            fontFamily: 'Montserrat-SemiBold',
                                            marginLeft: 5,
                                        },
                                    ]}>
                                    Individual file size cannot exceed 100 MB
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 2 }}>
                                <Text style={{ color: COLORS.altgreen_300 }}>{'\u2022'}</Text>
                                <Text
                                    style={[
                                        typography.Note,
                                        {
                                            color: COLORS.altgreen_300,
                                            fontFamily: 'Montserrat-SemiBold',
                                            marginLeft: 5,
                                        },
                                    ]}>
                                    Blog cover image size cannot exceed 15 MB
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    };

    getConnectsModal = () => {
        this.state.getConnectsModal &&
            this.state.connectionList.length === 0 &&
            this.getConnects(this.state.pageNumber, this.state.pageSize);

        return (
            <Modal
                visible={this.state.getConnectsModal}
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View
                    style={{
                        height: '100%',
                        width: '100%',
                        backgroundColor: COLORS.bgfill,
                    }}>
                    <View style={styles.header}>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ getConnectsModal: false })}
                                style={defaultShape.Nav_Gylph_Btn}>
                                <Icon
                                    name="Arrow-Left"
                                    color={COLORS.altgreen_300}
                                    size={16}
                                    style={{ marginTop: Platform.OS === 'android' ? 8 : 0 }}
                                />
                            </TouchableOpacity>

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ marginLeft: 10 }}>
                                    <Text
                                        numberOfLines={1}
                                        style={[
                                            typography.Button_Lead,
                                            { color: COLORS.dark_600, maxWidth: 160 },
                                        ]}>
                                        Select Connects
                                    </Text>
                                </View>
                            </View>
                        </View>

                        <TouchableOpacity
                            onPress={() => this.setState({ getConnectsModal: false })}
                            activeOpacity={0.6}
                            style={[
                                defaultShape.ContextBtn_FL_Drk,
                                { marginRight: 10, paddingHorizontal: 16 },
                            ]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_300 }]}>
                                Done
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View
                        style={{
                            backgroundColor: '#F7F7F5',
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 15,
                        }}>
                        <TextInput
                            style={styles.textInput}
                            placeholderTextColor="#D9E1E4"
                            onChangeText={(value) => {
                                this.setState({ searchText: value });
                            }}
                            color="#154A59"
                            placeholder="Search"
                            onFocus={() => this.setState({ searchIcon: false })}
                            onBlur={() => this.setState({ searchIcon: true })}
                            underlineColorAndroid="transparent"
                            ref={(input) => {
                                this.textInput = input;
                            }}
                        />
                    </View>

                    <View>
                        <FlatList
                            data={this.state.connectionList}
                            keyExtractor={(item) => item.id}
                            renderItem={(item) => (
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        marginTop: 15,
                                        paddingHorizontal: 20,
                                    }}>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            alignItems: 'center',
                                        }}>
                                        <Image
                                            source={
                                                item.item.personalInfo.profileImage
                                                    ? { uri: item.item.personalInfo.profileImage }
                                                    : defaultProfile
                                            }
                                            style={{ height: 32, width: 32, borderRadius: 16 }}
                                        />
                                        <Text
                                            numberOfLines={1}
                                            style={[
                                                typography.Button_Lead,
                                                { color: COLORS.dark_600, maxWidth: 150, marginLeft: 8 },
                                            ]}>
                                            {item.item.username}
                                        </Text>
                                    </View>

                                    <TouchableOpacity
                                        onPress={() =>
                                            this.selectedConnects(item.item.id, item.index)
                                        }
                                        style={{
                                            width: 17,
                                            height: 17,
                                            borderRadius: 8.5,
                                            borderColor: COLORS.green_500,
                                            borderWidth: 2,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            backgroundColor: this.state.tickList.includes(
                                                item.item.id,
                                            )
                                                ? COLORS.green_500
                                                : COLORS.altgreen_t50,
                                        }}>
                                        {this.state.tickList.includes(item.item.id) ? (
                                            <Icon
                                                name="Tick"
                                                size={10}
                                                color={COLORS.dark_800}
                                                style={{
                                                    marginLeft: 0,
                                                    marginTop: Platform.OS === 'android' ? 6 : 0,
                                                }}
                                            />
                                        ) : (
                                            <></>
                                        )}
                                    </TouchableOpacity>
                                </View>
                            )}
                        />
                    </View>
                </View>
            </Modal>
        );
    };

    searchValue = (query) => {
        if (query === '') {
            return this.state.whatDefinesYouSuggestions;
        }

        // const regex = new RegExp(`${query.trim()}`, 'i')

        // return this.state.whatDefinesYouSuggestions.filter(item => (item).search(regex) >= 0)
    };

    renderItemBusinessPage = (item) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() =>
                    this.setState(
                        {
                            selectedPostAs: item,
                            postAsModalOpen: false,
                            postAsUserName: false,
                            detailsModalOpen: true,
                        },
                        () => {
                            this.getCircleList(), this.getCauseList(), this.getProjectList();
                        },
                    )
                }
                style={styles.businessPageList}>
                {/* <View style={styles.businessPageList}> */}

                {item.profileImageUrl ? (
                    <Image
                        source={{ uri: item.profileImageUrl }}
                        style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
                    />
                ) : (
                    <Image
                        source={defaultBusiness}
                        style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
                    />
                )}

                <View>
                    <Text
                        numberOfLines={2}
                        style={{
                            color: '#00394D',
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 12,
                            maxWidth: 200,
                        }}>
                        {item.companyName.charAt(0).toUpperCase() +
                            item.companyName.slice(1)}
                    </Text>
                </View>

                <Icon
                    name="Arrow_Right"
                    size={13}
                    color="#367681"
                    style={{
                        marginTop: Platform.OS === 'android' ? 6 : 0,
                        position: 'absolute',
                        right: 36,
                    }}
                />

                {/* </View> */}

                {/* <View style={styles.borderView}></View> */}
            </TouchableOpacity>
        );
    };

    renderItemCirclePage = (item) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() =>
                    this.setState(
                        {
                            selectedPostAs: item,
                            postAsCircleModalOpen: false,
                            postAsUserName: false,
                            detailsModalOpen: true,
                        },
                        () => {
                            this.getCircleList(), this.getCauseList(), this.getProjectList();
                        },
                    )
                }
                style={styles.businessPageList}>
                {/* <View style={styles.businessPageList}> */}

                {item.profileImage ? (
                    <Image
                        source={{ uri: item.profileImage }}
                        style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
                    />
                ) : (
                    <Image
                        source={circleDefault}
                        style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
                    />
                )}

                <View>
                    <Text
                        numberOfLines={2}
                        style={{
                            color: '#00394D',
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 12,
                            textTransform: 'capitalize',
                        }}>
                        {item.title}
                    </Text>
                </View>

                <Icon
                    name="Arrow_Right"
                    size={13}
                    color="#367681"
                    style={{
                        marginTop: Platform.OS === 'android' ? 6 : 0,
                        position: 'absolute',
                        right: 36,
                    }}
                />

                {/* </View> */}

                {/* <View style={styles.borderView}></View> */}
            </TouchableOpacity>
        );
    };

    renderPageProjects = (item) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() =>
                    this.setState({
                        selectedPostIn: item,
                        pageProjectsModalOpen: false,
                        postInFeeds: false,
                        detailsModalOpen: false,
                    })
                }
                style={styles.businessPageList}>
                {/* <View style={styles.businessPageList}> */}

                {item.imageUrl ? (
                    <Image
                        source={{ uri: item.imageUrl }}
                        style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
                    />
                ) : (
                    <Image
                        source={projectDefault}
                        style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
                    />
                )}

                <View style={{ marginLeft: 6 }}>
                    <Text
                        numberOfLines={2}
                        style={{
                            color: '#00394D',
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 12,
                            textTransform: 'capitalize',
                            maxWidth: 260,
                        }}>
                        {item.title}
                    </Text>

                    <Text
                        numberOfLines={2}
                        style={{
                            color: '#00394d',
                            fontFamily: 'Montserrat-Medium',
                            fontSize: 10,
                            textTransform: 'capitalize',
                        }}>
                        {item.projectType}
                    </Text>
                </View>

                {/* </View> */}

                {/* <View style={styles.borderView}></View> */}
            </TouchableOpacity>
        );
    };

    renderPageCauses = (item) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() =>
                    this.setState({
                        selectedPostIn: item,
                        pageCausesModalOpen: false,
                        postInFeeds: false,
                        detailsModalOpen: false,
                    })
                }
                style={styles.businessPageList}>
                {/* <View style={styles.businessPageList}> */}

                {item.imageUrl ? (
                    <Image
                        source={{ uri: item.imageUrl }}
                        style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
                    />
                ) : (
                    <Image
                        source={projectDefault}
                        style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
                    />
                )}

                <View style={{ marginLeft: 6 }}>
                    <Text
                        numberOfLines={2}
                        style={{
                            color: '#00394D',
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 12,
                            textTransform: 'capitalize',
                            maxWidth: 260,
                        }}>
                        {item.name}
                    </Text>

                    <Text
                        numberOfLines={2}
                        style={{
                            color: '#00394d',
                            fontFamily: 'Montserrat-Medium',
                            fontSize: 10,
                            textTransform: 'capitalize',
                        }}>
                        {item.primaryHashtags}
                    </Text>
                </View>

                {/* </View> */}

                {/* <View style={styles.borderView}></View> */}
            </TouchableOpacity>
        );
    };

    renderPageCircles = (item) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                onPress={() =>
                    this.setState({
                        selectedPostIn: item,
                        pageCirclesModalOpen: false,
                        postInFeeds: false,
                        detailsModalOpen: false,
                    })
                }
                style={styles.businessPageList}>
                {/* <View style={styles.businessPageList}> */}

                {item.profileImage ? (
                    <Image
                        source={{ uri: item.profileImage }}
                        style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
                    />
                ) : (
                    <Image
                        source={circleDefault}
                        style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
                    />
                )}

                <View style={{ marginLeft: 6 }}>
                    <Text
                        numberOfLines={2}
                        style={{
                            color: '#00394D',
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 12,
                            textTransform: 'capitalize',
                            maxWidth: 260,
                        }}>
                        {item.title}
                    </Text>
                </View>

                {/* </View> */}

                {/* <View style={styles.borderView}></View> */}
            </TouchableOpacity>
        );
    };

    postAsModal = () => {
        return (
            <Modal
                onRequestClose={() => this.setState({ postAsModalOpen: false })}
                visible={this.state.postAsModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View
                        style={{
                            width: '100%',
                            height: 700,
                            position: 'absolute',
                            bottom: 0,
                            alignSelf: 'center',
                        }}>
                        <LinearGradient
                            colors={['#154A5900', '#154A59CC', '#154A59']}
                            style={{
                                flex: 1,
                                paddingLeft: 15,
                                paddingRight: 15,
                                borderBottomLeftRadius: 6,
                                borderBottomRightRadius: 6,
                            }}></LinearGradient>
                    </View>

                    {/* <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ postAsModalOpen: false })} >
                        <Icon name='Cross' size={13} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
                    </TouchableOpacity> */}

                    <View
                        style={[
                            defaultShape.Modal_Categories_Container,
                            {
                                height: '100%',
                                backgroundColor: COLORS.bgfill,
                                paddingBottom: 10,
                            },
                        ]}>
                        <View
                            style={{
                                flexDirection: 'row',
                                width: '100%',
                                height: 58,
                                backgroundColor: COLORS.altgreen_100,
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                                position: 'absolute',
                                top: 0,
                                zIndex: 2,
                            }}>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                style={[
                                    defaultShape.CloseBtn,
                                    { marginLeft: 6, marginBottom: 0 },
                                ]}
                                onPress={() => this.setState({ postAsModalOpen: false })}>
                                <Icon
                                    name="Arrow-Left"
                                    size={13}
                                    color="#367681"
                                    style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                                />
                            </TouchableOpacity>
                            <Text
                                style={[
                                    defaultStyle.Button_2,
                                    { marginLeft: 16, color: COLORS.dark_900, textAlign: 'center' },
                                ]}>
                                Choose where to post
                            </Text>
                        </View>

                        {this.props.userBusinessPage.body ? (
                            <View>
                                <FlatList
                                    keyboardShouldPersistTaps="handled"
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{
                                        paddingBottom: 30,
                                        paddingTop: 50,
                                        paddingHorizontal: 10,
                                        alignItems: 'flex-start',
                                    }}
                                    style={{ height: '50%' }}
                                    keyExtractor={(item) => item.id}
                                    // data={this.searchValue(this.state.searchValue)}
                                    data={this.props.userBusinessPage.body.businessPage.sort(
                                        (a, b) =>
                                            a.companyName.toUpperCase(0) >
                                            b.companyName.toUpperCase(0),
                                    )}
                                    initialNumToRender={10}
                                    renderItem={({ item }) => this.renderItemBusinessPage(item)}
                                />
                            </View>
                        ) : (
                            <ActivityIndicator size="small" color="#00394D" />
                        )}
                    </View>
                </View>
            </Modal>
        );
    };

    postAsCircleModal = () => {
        return (
            <Modal
                onRequestClose={() => this.setState({ postAsCircleModalOpen: false })}
                visible={this.state.postAsCircleModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View
                        style={{
                            width: '100%',
                            height: 700,
                            position: 'absolute',
                            bottom: 0,
                            alignSelf: 'center',
                        }}>
                        <LinearGradient
                            colors={['#154A5900', '#154A59CC', '#154A59']}
                            style={{
                                flex: 1,
                                paddingLeft: 15,
                                paddingRight: 15,
                                borderBottomLeftRadius: 6,
                                borderBottomRightRadius: 6,
                            }}></LinearGradient>
                    </View>

                    {/* <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ postAsCircleModalOpen: false })} >
                        <Icon name='Cross' size={13} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
                    </TouchableOpacity> */}

                    <View
                        style={[
                            defaultShape.Modal_Categories_Container,
                            {
                                height: '100%',
                                backgroundColor: COLORS.bgfill,
                                paddingBottom: 10,
                            },
                        ]}>
                        <View
                            style={{
                                flexDirection: 'row',
                                width: '100%',
                                height: 58,
                                backgroundColor: COLORS.altgreen_100,
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                                position: 'absolute',
                                top: 0,
                                zIndex: 2,
                            }}>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                style={[
                                    defaultShape.CloseBtn,
                                    { marginLeft: 6, marginBottom: 0 },
                                ]}
                                onPress={() => this.setState({ postAsCircleModalOpen: false })}>
                                <Icon
                                    name="Arrow-Left"
                                    size={13}
                                    color="#367681"
                                    style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                                />
                            </TouchableOpacity>
                            <Text
                                style={[
                                    defaultStyle.Button_2,
                                    { marginLeft: 16, color: COLORS.dark_900, textAlign: 'center' },
                                ]}>
                                Choose where to post
                            </Text>
                        </View>

                        {this.props.userBusinessPage.body ? (
                            <View>
                                <FlatList
                                    keyboardShouldPersistTaps="handled"
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{
                                        paddingBottom: 0,
                                        paddingTop: 50,
                                        paddingLeft: 10,
                                        alignItems: 'flex-start',
                                    }}
                                    style={{ height: '50%' }}
                                    keyExtractor={(item) => item.id}
                                    // data={this.searchValue(this.state.searchValue)}
                                    data={this.state.circleAdminList.sort(
                                        (a, b) => a.title.toUpperCase(0) > b.title.toUpperCase(0),
                                    )}
                                    initialNumToRender={10}
                                    renderItem={({ item }) => this.renderItemCirclePage(item)}
                                />
                            </View>
                        ) : (
                            <ActivityIndicator size="small" color="#00394D" />
                        )}
                    </View>
                </View>
            </Modal>
        );
    };

    getCircleList = () => {
        axios({
            method: 'get',
            url:
                REACT_APP_userServiceURL +
                '/backend/circle/get/list-for-post/' +
                this.getUserId(),
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.data && response.data.message === 'Success!') {
                    if (response.data.body) {
                        // console.log('circleList', response.data.body[0])
                        this.setState({
                            circleList: response.data.body,
                        });
                    }
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };

    getCauseList = () => {
        axios({
            method: 'get',
            url:
                REACT_APP_userServiceURL +
                '/backend/public/cause/joined/list?userId=' +
                this.getUserId() +
                '&page=0&size=1000',
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.data && response.data.message === 'Success!') {
                    if (response.data.body && response.data.body.content) {
                        // console.log('causeList', response.data.body.content[0])
                        this.setState({
                            causeList: response.data.body.content,
                        });
                    }
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };

    getProjectList = () => {
        axios({
            method: 'get',
            url:
                REACT_APP_userServiceURL +
                '/backend/getProjects/' +
                this.getUserId() +
                '?excludeJobs=true',
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.data && response.status === 200) {
                    if (response.data.body) {
                        // console.log('projectList', response.data.body[0])
                        this.setState({
                            projectList: response.data.body,
                        });
                    }
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };

    detailsModal = () => {
        return (
            <Modal
                onRequestClose={() => this.setState({ detailsModalOpen: false })}
                visible={this.state.detailsModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View
                        style={{
                            width: '100%',
                            height: 700,
                            position: 'absolute',
                            bottom: 0,
                            alignSelf: 'center',
                        }}>
                        <LinearGradient
                            colors={['#154A5900', '#154A59CC', '#154A59']}
                            style={{
                                flex: 1,
                                paddingLeft: 15,
                                paddingRight: 15,
                                borderBottomLeftRadius: 6,
                                borderBottomRightRadius: 6,
                            }}></LinearGradient>
                    </View>

                    {/* <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ detailsModalOpen: false })} >
                        <Icon name='Cross' size={13} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
                    </TouchableOpacity> */}

                    <View
                        style={[
                            defaultShape.Modal_Categories_Container,
                            {
                                height: '100%',
                                backgroundColor: COLORS.bgfill,
                                paddingBottom: 10,
                            },
                        ]}>
                        <View
                            style={{
                                flexDirection: 'row',
                                width: '100%',
                                height: 58,
                                backgroundColor: COLORS.altgreen_100,
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                                position: 'absolute',
                                top: 0,
                                zIndex: 2,
                            }}>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                style={[
                                    defaultShape.CloseBtn,
                                    { marginLeft: 6, marginBottom: 0 },
                                ]}
                                onPress={() => this.setState({ detailsModalOpen: false })}>
                                <Icon
                                    name="Arrow-Left"
                                    size={13}
                                    color="#367681"
                                    style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                                />
                            </TouchableOpacity>
                            <Text
                                style={[
                                    defaultStyle.Button_2,
                                    { marginLeft: 16, color: COLORS.dark_900, textAlign: 'center' },
                                ]}>
                                Choose where to post
                            </Text>
                        </View>

                        <TouchableOpacity
                            activeOpacity={0.8}
                            // onPress={() => this.props.navigation.navigate('CompanyProfileScreen', { userId: item.companyId })}
                            style={styles.businessCradView}>
                            <View style={styles.businessProfileCard}>
                                <Image
                                    style={{ width: 68, height: 68, borderRadius: 6 }}
                                    source={this.getImage()}
                                />
                            </View>
                            <View style={styles.businessDetailsCard}>
                                <Text numberOfLines={1} style={styles.businessName}>
                                    {this.getPostAsName()}
                                </Text>
                                {/* <Text numberOfLines={1} style={styles.businessLocation}>{item.country}</Text> */}
                            </View>
                        </TouchableOpacity>

                        <View
                            style={{
                                borderRadius: 20,
                                backgroundColor: COLORS.white,
                                alignItems: 'center',
                                paddingTop: 15,
                                paddingBottom: 10,
                                width: '90%',
                                alignSelf: 'center',
                                marginTop: 20,
                            }}>
                            <View
                                style={[
                                    defaultShape.ActList_Cell_Gylph_Alt,
                                    { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
                                ]}>
                                <Text
                                    style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                                    Post in
                                </Text>
                            </View>

                            <TouchableOpacity
                                style={
                                    Platform.OS === 'ios'
                                        ? [
                                            defaultShape.ActList_Cell_Gylph_Alt,
                                            { paddingVertical: 15 },
                                        ]
                                        : [defaultShape.ActList_Cell_Gylph_Alt, {}]
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState({
                                        whereToPostModalOpen: false,
                                        detailsModalOpen: false,
                                        selectedPostIn: {},
                                    });
                                }}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                    <Icon
                                        name="WN_Feeds_OL"
                                        size={18}
                                        color={COLORS.altgreen_300}
                                        style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                    />
                                    <Text
                                        style={[
                                            typography.Button_Lead,
                                            { color: COLORS.dark_600, marginLeft: 8 },
                                        ]}>
                                        Page Feeds
                                    </Text>
                                </View>
                                <Icon
                                    name="Arrow_Right"
                                    size={16}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    Platform.OS === 'ios'
                                        ? [
                                            defaultShape.ActList_Cell_Gylph_Alt,
                                            { paddingVertical: 15 },
                                        ]
                                        : defaultShape.ActList_Cell_Gylph_Alt
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState({
                                        whereToPostModalOpen: false,
                                        pageProjectsModalOpen: true,
                                    });
                                }}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                    <Icon
                                        name="Projects_OL"
                                        size={18}
                                        color={COLORS.altgreen_300}
                                        style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                    />
                                    <Text
                                        style={[
                                            typography.Button_Lead,
                                            { color: COLORS.dark_600, marginLeft: 8 },
                                        ]}>
                                        Page Projects
                                    </Text>
                                </View>
                                <Icon
                                    name="Arrow_Right"
                                    size={16}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    Platform.OS === 'ios'
                                        ? [
                                            defaultShape.ActList_Cell_Gylph_Alt,
                                            { paddingVertical: 15 },
                                        ]
                                        : defaultShape.ActList_Cell_Gylph_Alt
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState({
                                        whereToPostModalOpen: false,
                                        pageCausesModalOpen: true,
                                    });
                                }}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                    <Icon
                                        name="Causes_OL"
                                        size={18}
                                        color={COLORS.altgreen_300}
                                        style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                    />
                                    <Text
                                        style={[
                                            typography.Button_Lead,
                                            { color: COLORS.dark_600, marginLeft: 8 },
                                        ]}>
                                        Page Causes
                                    </Text>
                                </View>
                                <Icon
                                    name="Arrow_Right"
                                    size={16}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    Platform.OS === 'ios'
                                        ? [
                                            defaultShape.ActList_Cell_Gylph_Alt,
                                            { paddingVertical: 15, borderBottomWidth: 0 },
                                        ]
                                        : [
                                            defaultShape.ActList_Cell_Gylph_Alt,
                                            { borderBottomWidth: 0 },
                                        ]
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState({
                                        whereToPostModalOpen: false,
                                        pageCirclesModalOpen: true,
                                    });
                                }}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                    <Icon
                                        name="Circle_Ol"
                                        size={18}
                                        color={COLORS.altgreen_300}
                                        style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                    />
                                    <Text
                                        style={[
                                            typography.Button_Lead,
                                            { color: COLORS.dark_600, marginLeft: 8 },
                                        ]}>
                                        Page Circles
                                    </Text>
                                </View>
                                <Icon
                                    name="Arrow_Right"
                                    size={16}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    };

    getCircleAdminList = (userId) => {
        axios({
            method: 'get',
            url:
                REACT_APP_userServiceURL +
                '/backend/circle/get/list-for-post/' +
                userId +
                '?isAdmin=true',
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.data && response.status === 200) {
                    if (response.data.body) {
                        // console.log(response.data.body[0])
                        this.setState({
                            circleAdminList: response.data.body,
                        });
                    }
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };

    pageProjectsModal = () => {
        return (
            <Modal
                visible={this.state.pageProjectsModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View
                        style={{
                            width: '100%',
                            height: 700,
                            position: 'absolute',
                            bottom: 0,
                            alignSelf: 'center',
                        }}>
                        <LinearGradient
                            colors={['#154A5900', '#154A59CC', '#154A59']}
                            style={{
                                flex: 1,
                                paddingLeft: 15,
                                paddingRight: 15,
                                borderBottomLeftRadius: 6,
                                borderBottomRightRadius: 6,
                            }}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ pageProjectsModalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color="#367681"
                            style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                        />
                    </TouchableOpacity>

                    <View
                        style={[
                            defaultShape.Modal_Categories_Container,
                            { height: 450, backgroundColor: COLORS.bgfill, paddingBottom: 10 },
                        ]}>
                        <View
                            style={{
                                width: '100%',
                                height: 58,
                                backgroundColor: COLORS.altgreen_100,
                                justifyContent: 'center',
                                alignItems: 'center',
                                position: 'absolute',
                                top: 0,
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20,
                                zIndex: 2,
                            }}>
                            <Text
                                style={[
                                    defaultStyle.Button_2,
                                    { color: COLORS.dark_900, textAlign: 'center' },
                                ]}>
                                SELECT PROJECT
                            </Text>
                        </View>

                        {this.state.projectList ? (
                            <View>
                                <FlatList
                                    keyboardShouldPersistTaps="handled"
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{
                                        paddingBottom: 0,
                                        paddingTop: 50,
                                        paddingLeft: 10,
                                        alignItems: 'flex-start',
                                    }}
                                    style={{ height: '50%' }}
                                    keyExtractor={(item) => item.id}
                                    // data={this.searchValue(this.state.searchValue)}
                                    data={this.state.projectList.sort(
                                        (a, b) => a.title.toUpperCase(0) > b.title.toUpperCase(0),
                                    )}
                                    initialNumToRender={10}
                                    renderItem={({ item }) => this.renderPageProjects(item)}
                                />
                            </View>
                        ) : (
                            <ActivityIndicator size="small" color="#00394D" />
                        )}
                    </View>
                </View>
            </Modal>
        );
    };

    pageCausesModal = () => {
        return (
            <Modal
                visible={this.state.pageCausesModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View
                        style={{
                            width: '100%',
                            height: 700,
                            position: 'absolute',
                            bottom: 0,
                            alignSelf: 'center',
                        }}>
                        <LinearGradient
                            colors={['#154A5900', '#154A59CC', '#154A59']}
                            style={{
                                flex: 1,
                                paddingLeft: 15,
                                paddingRight: 15,
                                borderBottomLeftRadius: 6,
                                borderBottomRightRadius: 6,
                            }}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ pageCausesModalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color="#367681"
                            style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                        />
                    </TouchableOpacity>

                    <View
                        style={[
                            defaultShape.Modal_Categories_Container,
                            { height: 450, backgroundColor: COLORS.bgfill, paddingBottom: 10 },
                        ]}>
                        <View
                            style={{
                                width: '100%',
                                height: 58,
                                backgroundColor: COLORS.altgreen_100,
                                justifyContent: 'center',
                                alignItems: 'center',
                                position: 'absolute',
                                top: 0,
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20,
                                zIndex: 2,
                            }}>
                            <Text
                                style={[
                                    defaultStyle.Button_2,
                                    { color: COLORS.dark_900, textAlign: 'center' },
                                ]}>
                                SELECT CAUSE
                            </Text>
                        </View>

                        {this.state.causeList ? (
                            <View>
                                <FlatList
                                    keyboardShouldPersistTaps="handled"
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{
                                        paddingBottom: 0,
                                        paddingTop: 50,
                                        paddingLeft: 10,
                                        alignItems: 'flex-start',
                                    }}
                                    style={{ height: '50%' }}
                                    keyExtractor={(item) => item.id}
                                    // data={this.searchValue(this.state.searchValue)}
                                    data={this.state.causeList.sort(
                                        (a, b) => a.name.toUpperCase(0) > b.name.toUpperCase(0),
                                    )}
                                    initialNumToRender={10}
                                    renderItem={({ item }) => this.renderPageCauses(item)}
                                />
                            </View>
                        ) : (
                            <ActivityIndicator size="small" color="#00394D" />
                        )}
                    </View>
                </View>
            </Modal>
        );
    };

    pageCirclesModal = () => {
        return (
            <Modal
                visible={this.state.pageCirclesModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View
                        style={{
                            width: '100%',
                            height: 700,
                            position: 'absolute',
                            bottom: 0,
                            alignSelf: 'center',
                        }}>
                        <LinearGradient
                            colors={['#154A5900', '#154A59CC', '#154A59']}
                            style={{
                                flex: 1,
                                paddingLeft: 15,
                                paddingRight: 15,
                                borderBottomLeftRadius: 6,
                                borderBottomRightRadius: 6,
                            }}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ pageCirclesModalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color="#367681"
                            style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                        />
                    </TouchableOpacity>

                    <View
                        style={[
                            defaultShape.Modal_Categories_Container,
                            { height: 450, backgroundColor: COLORS.bgfill, paddingBottom: 10 },
                        ]}>
                        <View
                            style={{
                                width: '100%',
                                height: 58,
                                backgroundColor: COLORS.altgreen_100,
                                justifyContent: 'center',
                                alignItems: 'center',
                                position: 'absolute',
                                top: 0,
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20,
                                zIndex: 2,
                            }}>
                            <Text
                                style={[
                                    defaultStyle.Button_2,
                                    { color: COLORS.dark_900, textAlign: 'center' },
                                ]}>
                                SELECT CIRCLE
                            </Text>
                        </View>

                        {this.state.circleList ? (
                            <View>
                                <FlatList
                                    keyboardShouldPersistTaps="handled"
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{
                                        paddingBottom: 0,
                                        paddingTop: 50,
                                        paddingLeft: 10,
                                        alignItems: 'flex-start',
                                    }}
                                    style={{ height: '50%' }}
                                    keyExtractor={(item) => item.id}
                                    // data={this.searchValue(this.state.searchValue)}
                                    data={this.state.circleList.sort(
                                        (a, b) => a.title.toUpperCase(0) > b.title.toUpperCase(0),
                                    )}
                                    initialNumToRender={10}
                                    renderItem={({ item }) => this.renderPageCircles(item)}
                                />
                            </View>
                        ) : (
                            <ActivityIndicator size="small" color="#00394D" />
                        )}
                    </View>
                </View>
            </Modal>
        )
    }

    getSelectedPostIn = () => {
        if (this.state.selectedPostIn && this.state.selectedPostIn.title)
            return this.state.selectedPostIn.title
        else if (this.state.selectedPostIn && this.state.selectedPostIn.name)
            return this.state.selectedPostIn.name
        return 'Feeds'
    }

    handlePostUpdate = () => {
        const formData = new FormData();
        formData.append("postId", this.props.route.params.pressedActivityId);
        formData.append("description", this.state.query && this.state.query.trim('\n'));
        formData.append("hashTags", this.state.hashTags ? this.state.hashTags.join(',') : '');

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/post/edit',
            // headers: { 'Content-Type': 'multipart/form-data' },
            data: formData,
            withCredentials: true
        }).then(response => {
            if (response && response.status === 202) {
                // console.log(response.status)
            }
        }).catch((err) => {
            console.log(err)
        })
        Snackbar.show({
            backgroundColor: COLORS.dark_900,
            text: 'Your post is updated Successfully',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
        })
        setTimeout(() => {
            this.props.navigation.goBack()
        }, 2000)
    }

    render() {
        // console.log('this.state.hashTags:', this.state.hashTags)
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {this.hashTagModal()}
                {this.whereToPostModal()}
                {this.infoModal()}
                {this.getConnectsModal()}
                {this.postAsModal()}
                {this.postAsCircleModal()}
                {this.detailsModal()}
                {this.pageProjectsModal()}
                {this.pageCausesModal()}
                {this.pageCirclesModal()}

                {/****** Header start ******/}

                <View style={styles.header}>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            style={defaultShape.Nav_Gylph_Btn}>
                            <Icon
                                name="Cross"
                                color={COLORS.altgreen_300}
                                size={16}
                                style={{ marginTop: Platform.OS === 'android' ? 8 : 0 }}
                            />
                        </TouchableOpacity>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ marginLeft: 10 }}>
                                <Text
                                    numberOfLines={1}
                                    style={[
                                        typography.Button_Lead,
                                        { color: COLORS.dark_600, maxWidth: 160 },
                                    ]}>
                                    Edit Post
                                </Text>
                            </View>
                        </View>
                    </View>


                    <TouchableOpacity
                        onPress={() => this.handlePostUpdate()}
                        activeOpacity={0.6}
                        style={[
                            defaultShape.ContextBtn_FL_Drk,
                            { marginRight: 10, paddingHorizontal: 16 },
                        ]}>
                        <Text style={[typography.Caption, { color: COLORS.altgreen_300 }]}>
                            Update
                        </Text>
                    </TouchableOpacity>
                </View>

                {/****** Header ends ******/}

                <ScrollView keyboardShouldPersistTaps="handled">
                    {/****** Post in xxx start ******/}

                    {/****** Post in xxx ends ******/}

                    <View style={styles.commentCountBar}>
                        <TextInput
                            theme={{
                                colors: {
                                    text: COLORS.dark_600,
                                    primary: COLORS.altgreen_300,
                                    placeholder: COLORS.altgreen_300,
                                },
                            }}
                            placeholder="Write something interesting"
                            multiline={true}
                            selectionColor="#C8DB6E"
                            style={[
                                typography.H5,
                                {
                                    width: '96%',
                                    marginTop: 10,
                                    backgroundColor: COLORS.bgFill_200,
                                    color: COLORS.dark_600,
                                },
                            ]}
                            onChangeText={(value) => {
                                this.setState({ query: value });
                            }}
                            value={this.state.query}
                        />

                        {this.state.addLink ? (
                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginTop: 16,
                                    alignItems: 'center',
                                }}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}>
                                    {this.state.linkIcon && this.state.linkUrl === '' ? (
                                        <Icon
                                            name="Link_Post"
                                            color={COLORS.primarydark}
                                            size={12}
                                            style={{
                                                marginTop: Platform.OS === 'android' ? 13 : 0,
                                                marginRight: -20,
                                                zIndex: 2,
                                            }}
                                        />
                                    ) : (
                                        <></>
                                    )}

                                    <TextInput
                                        theme={{
                                            colors: {
                                                text: COLORS.grey_500,
                                                primary: COLORS.altgreen_300,
                                                placeholder: COLORS.altgreen_300,
                                            },
                                        }}
                                        label="    Paste URL or type"
                                        mode="outlined"
                                        selectionColor="#C8DB6E"
                                        style={[
                                            typography.Body_1,
                                            {
                                                width: '90%',
                                                height: 34,
                                                backgroundColor: COLORS.altgreen_t50,
                                                color: COLORS.dark_600,
                                            },
                                        ]}
                                        onChangeText={(value) => this.setState({ linkUrl: value })}
                                        value={this.state.linkUrl}
                                        onFocus={() => this.setState({ linkIcon: false })}
                                        onBlur={() => this.setState({ linkIcon: true })}
                                    />
                                </View>

                            </View>
                        ) : (
                            <></>
                        )}

                        {this.state.progressInfos.length > 0 ? (
                            <View style={{ marginTop: 20 }}>
                                <FlatList
                                    data={this.state.progressInfos}
                                    keyExtractor={(item, index) => index + item.fileName}
                                    renderItem={(item) => (
                                        <View
                                            style={{
                                                backgroundColor: COLORS.altgreen_100,
                                                width: '90%',
                                                paddingTop: 5,
                                                marginTop: 10,
                                                borderColor: COLORS.dark_600,
                                                borderWidth: 0.5,
                                                borderRadius: 6,
                                            }}>
                                            <Text
                                                style={[
                                                    typography.Caption,
                                                    {
                                                        marginBottom: 5,
                                                        marginLeft: 5,
                                                        color: COLORS.dark_600,
                                                    },
                                                ]}>
                                                {item.item.fileName}
                                            </Text>
                                            <ProgressBar
                                                progress={item.item.percentage}
                                                color={COLORS.dark_600}
                                                style={{ borderRadius: 6 }}
                                            />
                                        </View>
                                    )}
                                />
                            </View>
                        ) : (
                            <></>
                        )}
                    </View>
                </ScrollView>

                {this.state.hashTags.length > 0 ? (
                    <View
                        style={{
                            position: 'absolute',
                            bottom: 100,
                            zIndex: 1,
                            backgroundColor: COLORS.bgFill_200,
                        }}>
                        <FlatList
                            style={{ marginLeft: 10 }}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            data={this.state.hashTags}
                            keyExtractor={(item) => item}
                            renderItem={(item) => (
                                <Text
                                    style={[
                                        typography.Subtitle_2,
                                        { color: COLORS.dark_500, marginLeft: 10 },
                                    ]}>
                                    #{item.item}
                                </Text>
                            )}
                        />
                    </View>
                ) : (
                    <></>
                )}

                <View style={styles.hashTagLink}>
                    <TouchableOpacity
                        onPress={() => this.setState({ hashTagModalOpen: true })}
                        activeOpacity={0.6}
                        style={{
                            flexDirection: 'row',
                            backgroundColor: COLORS.grey_200,
                            width: 137,
                            height: 28,
                            borderRadius: 17,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                        <Icon
                            name="Hashtag"
                            color={COLORS.dark_500}
                            size={14}
                            style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
                        />
                        <Text
                            style={[
                                typography.Caption,
                                {
                                    color: COLORS.dark_500,
                                    marginLeft: 5,
                                    marginTop: Platform.OS === 'ios' ? -2 : 0,
                                },
                            ]}>
                            Add Hashtags
                        </Text>
                    </TouchableOpacity>
                </View>

                {/****** Attachment upload Box start ******/}

                {/****** Attachment upload Box ends ******/}
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: 52,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: COLORS.white,
        alignItems: 'center',
        // paddingVertical: 10
    },
    businessCradView: {
        height: 124,
        width: 140,
        alignItems: 'center',
        marginBottom: 11,
        marginTop: 80,
        marginHorizontal: 8,
    },
    businessDetailsCard: {
        height: 84,
        width: 140,
        borderRadius: 4,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.0,

        elevation: 1,
    },
    businessProfileCard: {
        height: 68,
        width: 68,
        borderRadius: 6,
        position: 'absolute',
        top: 0,
        zIndex: 1,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,

        elevation: 2,
    },
    businessName: {
        color: COLORS.primarydark,
        fontSize: 12,
        fontFamily: 'Montserrat-Bold',
        marginTop: 20,
        maxWidth: '90%',
    },
    businessPageList: {
        height: 56,
        width: screenWidth,
        // backgroundColor: 'orange',
        alignItems: 'center',
        alignSelf: 'flex-start',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        paddingHorizontal: 23,
        paddingBottom: 10,
        marginTop: 8,
        marginRight: 200,
    },
    borderView: {
        borderColor: '#E8ECEB',
        borderWidth: 0.5,
        width: '92%',
        alignSelf: 'center',
    },
    itemBox: {
        // width: 90,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomColor: '#D9E1E4',
        borderBottomWidth: 3,
        marginHorizontal: 5,
        marginVertical: 8,
    },
    commentCountBar: {
        // backgroundColor: COLORS.altgreen_200,
        paddingVertical: 6,
        paddingLeft: 16,
        marginTop: 20
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    commentItemView: {
        paddingLeft: 15,
        backgroundColor: COLORS.white,
        paddingTop: 12,
        borderBottomColor: COLORS.altgreen_400,
        borderBottomWidth: 0.2,
    },
    commentBody: {
        color: COLORS.altgreen_400,
        marginTop: -25,
        marginLeft: 38,
        maxWidth: '80%',
    },
    commentBoxView: {
        position: 'absolute',
        bottom: 0,
        paddingVertical: 6,
        backgroundColor: COLORS.white,
        width: '100%',
        height: 70,
        zIndex: 1,
        // paddingBottom: 40
    },
    hashTagLink: {
        position: 'absolute',
        bottom: 30,
        paddingVertical: 6,
        backgroundColor: COLORS.bgFill_200,
        width: '100%',
        height: 70,
        zIndex: 1,
        flexDirection: 'row',
        paddingHorizontal: 12,
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 60,
    },
    inputBox: {
        marginLeft: 10,
        width: '80%',
        height: 40,
        backgroundColor: COLORS.white,
        borderRadius: 6,
        paddingHorizontal: 10,
    },
    textInput: {
        fontSize: 17,
        fontFamily: 'Montserrat-Medium',
        padding: 10,
        backgroundColor: COLORS.white,
        width: '90%',
        zIndex: 1,
        height: 25,
        textAlign: 'center',
        borderRadius: 8,
        borderColor: '#D9E1E4',
        borderWidth: 1,
        shadowColor: '#36768140',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
    },
});

const mapStateToProps = (state) => {
    return {
        userDataProgress: state.personalProfileReducer.userDataProgress,
        user: state.personalProfileReducer.user,
        error: state.personalProfileReducer.error,

        userFeedsPhotosProgress: state.feedsReducer.userFeedsPhotosProgress,
        userFeedsPhotos: state.feedsReducer.userFeedsPhotos,
        errorFeedsPhotos: state.feedsReducer.errorFeedsPhotos,

        userFeedsVideosProgress: state.feedsReducer.userFeedsVideosProgress,
        userFeedsVideos: state.feedsReducer.userFeedsVideos,
        errorFeedsVideos: state.feedsReducer.errorFeedsVideos,

        userBusinessPageProgress:
            state.personalProfileReducer.userBusinessPageProgress,
        userBusinessPage: state.personalProfileReducer.userBusinessPage,
        errorBusinessPage: state.personalProfileReducer.errorBusinessPage,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
        feedsPhotosRequest: (data) => dispatch(feedsPhotosRequest(data)),
        feedsVideosRequest: (data) => dispatch(feedsVideosRequest(data)),
        personalBusinessPageRequest: (data) =>
            dispatch(personalBusinessPageRequest(data)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPost);
