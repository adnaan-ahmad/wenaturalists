import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Linking,
  Image,
  FlatList,
  Alert,
  Platform,
  Modal,
  SafeAreaView,
  PermissionsAndroid,
  Share,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import Autolink from 'react-native-autolink';
import LinearGradient from 'react-native-linear-gradient';
import * as Progress from 'react-native-progress';
import Snackbar from 'react-native-snackbar';
import {v4 as uuidv4} from 'uuid';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';

import {REACT_APP_userServiceURL} from '../../../../env.json';
import {COLORS} from '../../Shared/Colors';
import typography from '../../Shared/Typography';
import defaultShape from '../../Shared/Shape';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import defaultCircle from '../../../../assets/CirclesDefault.png';
import {nFormatter} from '../../Shared/commonFunction';
import Follow from './Follow';

const moment = require('moment');
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class ProjectView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.userId,
      projDetails: this.props.projDetails ? this.props.projDetails : {},
      isCompany: this.props.isCompany,
      isParticipantAvailable: false,
      slug: this.props.slug ? this.props.slug : '',

      // download
      downloadStart: false,
      isDownloading: false,
      progressLimit: 0,
    };
  }
  componentDidMount() {
    this.getProjectBySlug();
  }

  getProjectBySlug = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/find-project-description-details-by-userId-and-slug/' +
        this.state.userId +
        '/' +
        this.state.slug,
      withCredentials: true,
    })
      .then((response) => {
        console.log('object');
        this.setState({
          projDetails: response.data.body,
        });
      })
      .catch((err) => console.log(err));
  };

  applyInProject = () => {
    let postBody = {
      participantUserId: this.state.userId,
      participantionPartyType: this.state.projDetails.partyType,
      projectId: this.state.projDetails.project.id,
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/backend/event/apply-in-project?isVolunteerProject=false',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState(
          {
            ...this.state,
            projDetails: {
              ...this.state.projDetails,
              isUserAlreadyApplied: true,
              canWithdraw: true,
            },
          },
          () => this.getProjectBySlug(),
        );
      })
      .catch((err) => console.log(err));
  };

  pinProject = (isPinned) => {
    let postBody = {
      entityId: this.state.projDetails.project.id,
      entityType: this.state.projDetails.project.type,
      pinned: !isPinned,
      userId: this.state.userId,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/pinned/create/',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          ...this.state,
          projDetails: {...this.state.projDetails, pinned: !isPinned},
        });
      })
      .catch((err) => console.log(err));
  };

  contribute = () => {
    let postBody = {
      projectId: this.state.projDetails.project.id,
      participantionPartyType: this.state.projDetails.partyType,
      participantUserId: this.state.userId,
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/backend/fundraise/apply-in-project?isVolunteerProject=false',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          ...this.state,
          projDetails: {...this.state.projDetails, isUserAlreadyApplied: true},
        });
      })
      .catch((err) => console.log(err));
  };

  alertContribute = () => {
    Alert.alert(
      '',
      this.state.projDetails?.deactivationRequestSent
        ? 'Your profile is undergoing deactivation process. During this period you will not be able to apply a project.'
        : 'Are you sure you want to contribute to the ' +
            this.state.projDetails?.project?.type +
            ' project, ' +
            this.state.projDetails?.project?.title +
            '? Your application will be accepted as soon as you apply.',
      !this.state.projDetails?.deactivationRequestSent
        ? [
            {
              text: 'YES',
              onPress: () =>
                this.state.projDetails?.deactivationRequestSent
                  ? null
                  : this.contribute(),
              style: 'cancel',
            },
            {
              text: 'NO',
              // onPress: () => console.log('user cancelled deletion'),
            },
          ]
        : [{text: 'OK'}],
    );
  };

  withdrawApplication = () => {
    let postBody = {
      projectId: this.state.projDetails.project.id,
      otherParticipatingUserId: this.state.userId,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/participation/withdraw',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          ...this.state,
          projDetails: {...this.state.projDetails, isUserAlreadyApplied: false},
        });
      })
      .catch((err) => console.log(err));
  };

  withdrawApplyAlert = () => {
    Alert.alert(
      '',
      this.state.projDetails?.deactivationRequestSent
        ? 'Your profile is undergoing deactivation process. During this period you will not be able to apply a project.'
        : 'Are you sure you want to withdraw participant request for the ' +
            this.state.projDetails?.project?.type +
            ' ' +
            this.state.projDetails?.project?.title +
            '?',
      !this.state.projDetails?.deactivationRequestSent
        ? [
            {
              text: 'YES',
              onPress: () =>
                this.state.projDetails?.deactivationRequestSent
                  ? null
                  : this.withdrawApplication(),
              style: 'cancel',
            },
            {
              text: 'NO',
              // onPress: () => console.log('user cancelled deletion'),
            },
          ]
        : [{text: 'OK'}],
    );
  };

  applyInProjectAlert = () => {
    Alert.alert(
      '',
      this.state.projDetails?.deactivationRequestSent
        ? 'Your profile is undergoing deactivation process. During this period you will not be able to apply a project.'
        : 'Are you sure you want to apply for the ' +
            this.state.projDetails?.project?.type +
            ' ' +
            this.state.projDetails?.project?.title +
            '?',
      !this.state.projDetails?.deactivationRequestSent
        ? [
            {
              text: 'YES',
              onPress: () =>
                this.state.projDetails?.deactivationRequestSent
                  ? null
                  : this.applyInProject(),
              style: 'cancel',
            },
            {
              text: 'NO',
              // onPress: () => console.log('user cancelled deletion'),
            },
          ]
        : [{text: 'OK'}],
    );
  };

  participantAvailableForHideAlert = () => {
    Alert.alert(
      '',
      'The ' +
        this.state.projDetails?.project?.type +
        ', ' +
        this.state.projDetails?.project?.title +
        ' you wish to hide will continue to be visible to other participants as before.',

      [
        {
          text: 'YES',
          onPress: () => this.hideProject(),
          style: 'cancel',
        },
        {
          text: 'NO',
          // onPress: () => console.log('user cancelled deletion'),
        },
      ],
    );
  };

  isParticipantAvailable = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/participation/isparticipant-available?projectId=' +
        this.state.projDetails.project.id,
      withCredentials: true,
    })
      .then((response) => {
        response.data.body === true
          ? this.participantAvailableForHideAlert()
          : this.hideProject();
      })
      .catch((err) => console.log(err));
  };

  hideProject = () => {
    let postBody = {
      userId: this.state.userId,
      activityId: this.state.projDetails.project.id,
      entityType: this.state.projDetails.project.type,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          ...this.state,
          projDetails: {...this.state.projDetails, hidden: true},
        });
        this.props.changeFun();
      })
      .catch((err) => console.log(err.response.data));
  };

  // *************** Download file start *******************

  downloadProgressWrapper = () => {
    return (
      <Modal
        visible={this.state.isDownloading}
        transparent
        animationType="fade"
        supportedOrientations={['portrait', 'landscape']}>
        <SafeAreaView
          style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {bottom: 0, height: '100%'},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          {this.state.progressLimit == 1 ? null : (
            <Progress.Pie
              progress={this.state.progressLimit}
              size={120}
              color={COLORS.white}
            />
          )}
        </SafeAreaView>
      </Modal>
    );
  };

  readStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.writeStorage(data);
      } else {
        // Permission Denied
        alert('Read Storage Permission Denied');
      }
    } else {
      this.getDownloadFile(data); //work on ios
    }
  };

  writeStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.getDownloadFile(data);
      } else {
        // Permission Denied
        alert('Write Storage Permission Denied');
      }
    }
  };

  getDownloadFile = (urlDownload) => {
    if (this.state.downloadStart) {
      return;
    } else {
      this.setState({downloadStart: true});
    }

    let directoryFile;
    let dirs;

    let fileName;
    try {
      fileName = urlDownload.split('-').pop();
    } catch (e) {
      fileName = uuidv4();
    }
    if (Platform.OS === 'android') {
      directoryFile = RNFetchBlob.fs.dirs;
      dirs = directoryFile.DownloadDir + '/Download/' + fileName;
    } else {
      directoryFile = RNFS.DocumentDirectoryPath;
      dirs = directoryFile + '/' + fileName;
    }

    try {
      RNFetchBlob.config({
        path: dirs,
      })
        .fetch('GET', urlDownload, {})
        .progress((received, total) => {
          let temp = parseFloat(received / total);

          if (temp > 0.9) {
            temp = 1;
            setTimeout(() => {
              this.setState({isDownloading: false});
            }, 1000);
          }
          this.setState({
            isDownloading: true,
            progressLimit: temp,
          });
        })
        .then((res) => {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'File downloaded successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });

          this.setState({
            ...this.state,
            downloadStart: false,
          });
          setTimeout(() => {
            this.setState({isDownloading: false});
          }, 1000);
        });
    } catch (error) {}
  };

  // *************** Download file ends *******************

  render() {
    let {projDetails} = this.state;
    return (
      <ScrollView
        style={{
          height: 420,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          backgroundColor: COLORS.white,
        }}>
        {this.downloadProgressWrapper()}
        <View
          style={{
            backgroundColor: COLORS.dark_900,
            paddingVertical: 15,
            width: '100%',
            paddingHorizontal: 15,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View>
              <Text
                style={[
                  typography.Title_2,
                  {
                    color: COLORS.green_500,
                  },
                ]}>
                {projDetails.project.volunteerProject ? 'VOLUNTEERING' : ''}{' '}
                {projDetails.project && projDetails.project.type}
              </Text>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={[typography.Note2, {color: COLORS.altgreen_300}]}>
                  Posted on{' '}
                  {moment
                    .unix(projDetails.dateOfPosting / 1000)
                    .format('DD MMM YYYY')}
                </Text>
                {projDetails?.shareCount > 0 && (
                  <TouchableOpacity
                    onPress={() => this.props.seeWhoShared()}
                    style={{
                      flexDirection: 'row',
                      marginLeft: 10,
                      alignItems: 'center',
                    }}>
                    <Icon name="Share" size={12} color={COLORS.green_500} />
                    <Text
                      style={[
                        typography.Note2,
                        {color: COLORS.altgreen_300, marginLeft: 5},
                      ]}>
                      {projDetails.shareCount} Share
                    </Text>
                  </TouchableOpacity>
                )}
                {!projDetails?.hidden ? (
                  <TouchableOpacity
                    onPress={() => this.isParticipantAvailable()}
                    style={{
                      height: 20,
                      width: 20,
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginLeft: 10,
                    }}>
                    <Icon
                      name={'Eye_OL'}
                      size={12}
                      color={COLORS.altgreen_400}
                    />
                  </TouchableOpacity>
                ) : (
                  <Text
                    style={[
                      typography.Note2,
                      {color: COLORS.white, marginLeft: 10},
                    ]}>
                    Hidden
                  </Text>
                )}

                <TouchableOpacity
                  onPress={() => this.pinProject(projDetails.pinned)}
                  style={{
                    height: 20,
                    width: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginLeft: 5,
                  }}>
                  <Icon
                    name="Pin"
                    size={12}
                    color={
                      projDetails.pinned
                        ? COLORS.green_500
                        : COLORS.altgreen_400
                    }
                  />
                </TouchableOpacity>
              </View>
            </View>

            {projDetails?.project?.websiteLink &&
              projDetails?.project?.websiteLink[0] !== '' && (
                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL(projDetails.project.websiteLink[0])
                  }
                  style={{height: 30, width: 30, alignItems: 'center'}}>
                  <Icon name="Globe" size={14} color={COLORS.altgreen_300} />
                </TouchableOpacity>
              )}
          </View>

          <Text
            style={[
              typography.Button_Lead,
              {color: COLORS.white, marginTop: 10},
            ]}>
            {projDetails.project && projDetails.project.title}
          </Text>
          <Text style={[typography.Note2, {color: COLORS.altgreen_300}]}>
            {/* <Icon name="Location" size={12} color={COLORS.altgreen_300} /> */}
            {projDetails.project &&
              projDetails.project.location &&
              ' ' +
                projDetails.project.location.city +
                ', ' +
                projDetails.project.location.state +
                ', ' +
                projDetails.project.location.country}
          </Text>

          {projDetails?.project?.cost > 0 && (
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <Icon name="Wallet" size={12} color={COLORS.green_500} />
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.green_500, marginLeft: 6},
                ]}>
                {projDetails?.project?.currency +
                  ' ' +
                  nFormatter(projDetails?.project?.cost, 1)}
              </Text>
            </View>
          )}

          <Text
            style={[typography.Note2, {color: COLORS.dark_700, marginTop: 10}]}>
            Creator
          </Text>
          <TouchableOpacity
            onPress={() => {
              projDetails.userType === 'INDIVIDUAL' &&
              projDetails.userId === this.state.userId
                ? this.props.navigation('ProfileStack')
                : projDetails.userType === 'INDIVIDUAL' &&
                  projDetails.userId !== this.state.userId
                ? this.props.navigation('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: {userId: projDetails.userId},
                  })
                : projDetails.userType === 'COMPANY'
                ? this.props.navigation('ProfileStack', {
                    screen: 'CompanyProfileScreen',
                    params: {userId: projDetails.userId},
                  })
                : this.props.navigation('CircleProfileStack', {
                    screen: 'CircleProfile',
                    params: {slug: projDetails.customUrl},
                  });
            }}
            style={{
              paddingVertical: 5,
              paddingHorizontal: 10,
              backgroundColor: COLORS.dark_700,
              flexDirection: 'row',
              maxWidth: 160,
              borderRadius: 4,
              marginTop: 2,
              alignItems: 'center',
            }}>
            <Image
              source={
                projDetails.imageUrl
                  ? {uri: projDetails.imageUrl}
                  : !this.state.isCompany &&
                    projDetails.userType === 'INDIVIDUAL'
                  ? defaultProfile
                  : this.state.isCompany || projDetails.userType === 'COMPANY'
                  ? defaultBusiness
                  : defaultCircle
              }
              style={{height: 30, width: 30, borderRadius: 15}}
            />
            <Text
              numberOfLines={1}
              style={[
                typography.Caption,
                {
                  color: COLORS.white,
                  marginLeft: 5,
                  maxWidth: Platform.OS === 'android' ? 110 : 105,
                },
              ]}>
              {projDetails.creatorName}
            </Text>
          </TouchableOpacity>

          {projDetails?.project?.companyName && (
            <Text
              style={[
                typography.Note2,
                {color: COLORS.dark_700, marginTop: 10},
              ]}>
              Organization
            </Text>
          )}
          {projDetails?.project?.companyName && (
            <View
              style={{
                // paddingVertical: 5,
                // paddingHorizontal: 10,
                // backgroundColor: COLORS.dark_700,
                borderRadius: 4,
                marginTop: 2,
              }}>
              <Text
                numberOfLines={1}
                style={[typography.Caption, {color: COLORS.white}]}>
                {projDetails.project && projDetails.project.companyName}
              </Text>
            </View>
          )}

          {!projDetails?.project?.isOffLine && (
            <Text
              style={[
                typography.Note2,
                {color: COLORS.dark_700, marginTop: 10},
              ]}>
              Application
            </Text>
          )}
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              {!projDetails?.project?.isOffLine && (
                <Text
                  style={[typography.Caption, {color: COLORS.altgreen_300}]}>
                  Last date:{' '}
                  {projDetails.project &&
                    moment
                      .unix(projDetails.project.lastTimeOfApplication / 1000)
                      .format('DD MMM YYYY')}
                </Text>
              )}
              {!projDetails?.project?.isOffLine ? (
                <Text
                  style={[typography.Caption, {color: COLORS.altgreen_300}]}>
                  {projDetails?.project?.type === 'FUNDRAISE' &&
                  projDetails?.project?.creatorUserId === this.state.userId
                    ? 'No of people contributed'
                    : projDetails?.project?.type === 'FUNDRAISE' &&
                      projDetails?.project?.creatorUserId !== this.state.userId
                    ? ''
                    : 'Openings'}
                  {projDetails?.project?.type === 'FUNDRAISE' &&
                  projDetails?.project?.creatorUserId === this.state.userId
                    ? ': '
                    : projDetails?.project?.type === 'FUNDRAISE' &&
                      projDetails?.project?.creatorUserId !== this.state.userId
                    ? ''
                    : ': '}
                  {projDetails?.project?.type === 'FUNDRAISE' &&
                  projDetails?.project?.creatorUserId === this.state.userId
                    ? projDetails?.noOfParticipants
                    : projDetails?.project?.type === 'FUNDRAISE' &&
                      projDetails?.project?.creatorUserId !== this.state.userId
                    ? ''
                    : projDetails?.noOfOpenings}
                </Text>
              ) : (
                <></>
              )}
            </View>
            {(!projDetails.isUserAlreadyApplied &&
              !projDetails?.project?.isOffLine) ||
            (!projDetails.isApprover &&
              projDetails.isUserAlreadyApplied &&
              projDetails.canWithdraw &&
              !projDetails.isAutoApproveApplicable &&
              !projDetails?.project?.isOffLine) ||
            (projDetails?.project?.type === 'FUNDRAISE' &&
              !projDetails.isApprover &&
              !projDetails.isUserAlreadyApplied &&
              projDetails.canApply &&
              !projDetails?.project?.isOffLine) ||
            (projDetails?.project?.creatorUserId === this.state.userId &&
              projDetails?.project?.type === 'FUNDRAISE' &&
              !projDetails?.project?.isOffLine) ||
            (projDetails?.project?.creatorUserId === this.state.userId &&
              projDetails?.project?.type !== 'FUNDRAISE' &&
              !projDetails?.project?.isOffLine) ? (
              <TouchableOpacity
                onPress={() =>
                  projDetails?.project?.creatorUserId === this.state.userId &&
                  projDetails?.project?.type !== 'FUNDRAISE'
                    ? this.props.goToParticipants()
                    : projDetails?.project?.creatorUserId ===
                        this.state.userId &&
                      projDetails?.project?.type === 'FUNDRAISE'
                    ? this.props.goToContributors()
                    : !projDetails.isApprover &&
                      projDetails.isUserAlreadyApplied &&
                      projDetails.canWithdraw &&
                      !projDetails.isAutoApproveApplicable
                    ? this.withdrawApplyAlert()
                    : projDetails?.project?.type === 'FUNDRAISE' &&
                      !projDetails.isApprover &&
                      !projDetails.isUserAlreadyApplied &&
                      projDetails.canApply
                    ? this.alertContribute()
                    : this.applyInProjectAlert()
                }
                style={{
                  backgroundColor: COLORS.green_500,
                  borderRadius: 20,
                  // width: 80,
                  paddingHorizontal: 12,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_800}]}>
                  {projDetails?.project?.creatorUserId === this.state.userId &&
                  projDetails?.project?.type !== 'FUNDRAISE'
                    ? 'See All Applicants'
                    : projDetails?.project?.creatorUserId ===
                        this.state.userId &&
                      projDetails?.project?.type === 'FUNDRAISE'
                    ? 'See All Contributors'
                    : projDetails?.project?.type === 'FUNDRAISE' &&
                      !projDetails.isApprover &&
                      !projDetails.isUserAlreadyApplied &&
                      projDetails.canApply
                    ? 'Contribute'
                    : projDetails?.project?.type !== 'FUNDRAISE' &&
                      !projDetails.isApprover &&
                      !projDetails.isUserAlreadyApplied &&
                      projDetails.canApply
                    ? 'Apply'
                    : !projDetails.isApprover &&
                      projDetails.isUserAlreadyApplied &&
                      projDetails.canApply &&
                      projDetails.isAutoApproveApplicable &&
                      projDetails?.project?.type === 'FUNDRAISE'
                    ? 'Already Contributed'
                    : !projDetails.isApprover &&
                      projDetails.isUserAlreadyApplied &&
                      projDetails.canApply &&
                      projDetails.isAutoApproveApplicable &&
                      projDetails?.project?.type !== 'FUNDRAISE'
                    ? 'Already Applied'
                    : 'Withdraw'}
                </Text>
              </TouchableOpacity>
            ) : !projDetails?.project?.isOffLine ? (
              <View
                style={{
                  backgroundColor: COLORS.green_500,
                  borderRadius: 20,
                  // width: 80,
                  paddingHorizontal: 12,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_800}]}>
                  {!projDetails.isApprover &&
                  projDetails.isUserAlreadyApplied &&
                  projDetails.canApply &&
                  projDetails?.project?.type !== 'FUNDRAISE'
                    ? 'Already Applied'
                    : 'Already Contributed'}
                </Text>
              </View>
            ) : (
              <></>
            )}
          </View>
        </View>

        {projDetails?.project?.shortBrief && (
          <View>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, marginTop: 15, marginLeft: 15},
              ]}>
              Brief Description
            </Text>
            <Autolink
              style={[
                typography.Caption,
                {
                  color: COLORS.grey_400,
                  marginTop: 10,
                  marginLeft: 15,
                  marginRight: 10,
                },
              ]}
              linkStyle={{color: COLORS.green_500}}
              text={projDetails.project.shortBrief}
              url
            />
          </View>
        )}

        {projDetails?.project?.description ? (
          <View>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, marginTop: 15, marginLeft: 15},
              ]}>
              Detailed Description
            </Text>
            <Autolink
              style={[
                typography.Caption,
                {
                  color: COLORS.grey_400,
                  marginTop: 10,
                  marginLeft: 15,
                  marginRight: 10,
                },
              ]}
              linkStyle={{color: COLORS.green_500}}
              text={projDetails.project.description}
              url
            />
          </View>
        ) : null}

        {projDetails?.project?.attachments.length > 0 ? (
          <View
            style={{
              paddingVertical: 8,
              backgroundColor: COLORS.white,
              borderRadius: 4,
              marginTop: 5,
              width: '90%',
              marginLeft: 15,
            }}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={projDetails?.project?.attachments}
              keyExtractor={(item) => item}
              renderItem={(item) => (
                <TouchableOpacity
                  onPress={() => this.readStorage(item.item)}
                  style={{
                    paddingHorizontal: 8,
                    paddingVertical: 5,
                    backgroundColor: COLORS.altgreen_200,
                    borderRadius: 20,
                    marginHorizontal: 5,
                  }}>
                  <Text style={[typography.Note2, {color: COLORS.dark_800}]}>
                    {item.item.split('-').pop()}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
        ) : null}

        {projDetails?.faculties.length > 0 ? (
          <View
            style={{
              paddingVertical: 8,
              backgroundColor: COLORS.white,
              borderRadius: 4,
              marginTop: 5,
              width: '90%',
            }}>
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_800,
                  marginLeft: 15,
                  textTransform: 'capitalize',
                },
              ]}>
              {projDetails?.project?.type} leader
            </Text>

            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{marginLeft: 12}}
              data={projDetails?.faculties}
              keyExtractor={(item) => item.userId}
              renderItem={(item) => (
                <TouchableOpacity
                  onPress={() => {
                    item.item.userType === 'INDIVIDUAL' &&
                    item.item.userId === this.state.userId
                      ? this.props.navigation('ProfileStack')
                      : item.item.userType === 'INDIVIDUAL' &&
                        item.item.userId !== this.state.userId
                      ? this.props.navigation('ProfileStack', {
                          screen: 'OtherProfileScreen',
                          params: {userId: item.item.userId},
                        })
                      : item.item.userType === 'COMPANY'
                      ? this.props.navigation('ProfileStack', {
                          screen: 'CompanyProfileScreen',
                          params: {userId: item.item.userId},
                        })
                      : this.props.navigation('CircleProfileStack', {
                          screen: 'CircleProfile',
                          params: {slug: item.item.customUrl},
                        });
                  }}
                  style={{
                    paddingRight: 8,
                    backgroundColor: COLORS.altgreen_200,
                    borderRadius: 15,
                    marginHorizontal: 5,
                    marginTop: 10,
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={
                      item.item.imageUrl
                        ? {uri: item.item.imageUrl}
                        : defaultProfile
                    }
                    style={{height: 30, width: 30, borderRadius: 15}}
                  />
                  <Text
                    style={[
                      typography.Caption,
                      {color: COLORS.dark_800, marginRight: 5, marginLeft: 6},
                    ]}>
                    {item.item.username}
                  </Text>
                  <Follow
                    item={item.item}
                    followed={item.item.followed}
                    userId={item.item.userId}
                    index={item.index}
                  />
                </TouchableOpacity>
              )}
            />
          </View>
        ) : null}

        {projDetails?.project?.qualifications.length > 0 ? (
          <View
            style={{
              paddingVertical: 8,
              backgroundColor: COLORS.white,
              borderRadius: 4,
              marginTop: 5,
              width: '90%',
            }}>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, marginLeft: 15},
              ]}>
              Qualification
            </Text>

            <FlatList
              showsVerticalScrollIndicator={false}
              style={{marginLeft: 12}}
              data={projDetails.project.qualifications}
              keyExtractor={(item) => item}
              renderItem={(item) => (
                <Text
                  style={[
                    typography.Caption,
                    {color: COLORS.grey_400, marginTop: 10, marginLeft: 15},
                  ]}>
                  <Text style={[typography.Caption, {color: COLORS.grey_400}]}>
                    {'\u2022'}
                  </Text>
                  {' ' + item.item}
                </Text>
              )}
            />
          </View>
        ) : null}

        {projDetails?.project?.inclusions.length > 0 &&
        projDetails?.project?.inclusions[0] !== '' ? (
          <View>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, marginTop: 15, marginLeft: 15},
              ]}>
              Inclusion
            </Text>
            <Text
              style={[
                typography.Caption,
                {
                  color: COLORS.grey_400,
                  marginTop: 10,
                  marginLeft: 15,
                  marginRight: 10,
                },
              ]}>
              {projDetails.project.inclusions[0]}
            </Text>
          </View>
        ) : null}

        {projDetails?.project?.exclusions.length > 0 &&
        projDetails?.project?.exclusions[0] !== '' ? (
          <View>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, marginTop: 15, marginLeft: 15},
              ]}>
              Exclusion
            </Text>
            <Text
              style={[
                typography.Caption,
                {
                  color: COLORS.grey_400,
                  marginTop: 10,
                  marginLeft: 15,
                  marginRight: 10,
                },
              ]}>
              {projDetails.project.exclusions[0]}
            </Text>
          </View>
        ) : null}

        {projDetails?.project?.endTakeAway ? (
          <View>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, marginTop: 15, marginLeft: 15},
              ]}>
              Mementos or Event Kits
            </Text>
            <Text
              style={[
                typography.Note2,
                {color: COLORS.grey_400, marginLeft: 15},
              ]}>
              (Received by participant post completion)
            </Text>
            <Text
              style={[
                typography.Caption,
                {
                  color: COLORS.grey_400,
                  marginTop: 10,
                  marginLeft: 15,
                  marginRight: 10,
                },
              ]}>
              {projDetails.project.endTakeAway}
            </Text>
          </View>
        ) : null}

        {projDetails?.project?.mediaAttachments.length > 0 &&
        projDetails?.project?.mediaAttachments[0] !== '' ? (
          <View>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, marginTop: 15, marginLeft: 15},
              ]}>
              Media Coverage
            </Text>
            <Autolink
              style={[
                typography.Caption,
                {
                  color: COLORS.grey_400,
                  marginTop: 10,
                  marginLeft: 15,
                  marginRight: 10,
                },
              ]}
              linkStyle={{color: COLORS.green_500}}
              text={projDetails.project.mediaAttachments[0]}
              url
            />
          </View>
        ) : null}

        {!projDetails?.project?.isOffLine && (
          <Text
            style={[
              typography.Button_Lead,
              {color: COLORS.dark_800, marginTop: 15, marginLeft: 15},
            ]}>
            Skills and Specialization
          </Text>
        )}
        {!projDetails?.project?.isOffLine && (
          <View
            style={{
              paddingVertical: 8,
              backgroundColor: COLORS.grey_100,
              borderRadius: 4,
              marginTop: 5,
              width: '90%',
              marginLeft: 15,
            }}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={
                projDetails.project
                  ? projDetails.project.specialization.concat(
                      projDetails.project.skills,
                    )
                  : []
              }
              keyExtractor={(item) => item}
              renderItem={(item) => (
                <View
                  style={{
                    paddingHorizontal: 8,
                    paddingVertical: 5,
                    backgroundColor: COLORS.dark_500,
                    borderRadius: 20,
                    marginHorizontal: 5,
                  }}>
                  <Text style={[typography.Note2, {color: COLORS.white}]}>
                    {item.item}
                  </Text>
                </View>
              )}
            />
          </View>
        )}

        {projDetails?.project?.location?.address ? (
          <View>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, marginTop: 15, marginLeft: 15},
              ]}>
              Detailed Address
            </Text>
            <Text
              style={[
                typography.Body_1_bold,
                {color: COLORS.dark_600, marginTop: 10, marginLeft: 15},
              ]}>
              {projDetails.project.location.address}
            </Text>
          </View>
        ) : null}

        <Text
          style={[
            typography.Button_Lead,
            {color: COLORS.dark_800, marginTop: 15, marginLeft: 15},
          ]}>
          Duration
        </Text>
        <View style={{marginBottom: 20, marginLeft: 15, marginTop: 5}}>
          <Text style={[typography.Caption, {color: COLORS.dark_600}]}>
            Start Date:{'   '}
            {projDetails.project &&
              moment
                .unix(projDetails.project.beginningTime / 1000)
                .format('DD MMM YYYY')}
          </Text>
          <Text style={[typography.Caption, {color: COLORS.dark_600}]}>
            End Date:{'   '}
            {projDetails.project && projDetails.project.endingTime !== 0
              ? moment
                  .unix(projDetails.project.endingTime / 1000)
                  .format('DD MMM YYYY')
              : 'NA'}
          </Text>
        </View>
      </ScrollView>
    );
  }
}
