import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';

import {REACT_APP_userServiceURL} from '../../../../env.json';
import typography from '../../../Components/Shared/Typography';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {COLORS} from '../../Shared/Colors';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
export default class  Follow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {},
      followed: props.followed,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userData')
      .then((value) => {
        let userData = JSON.parse(value);
        value && this.setState({userData: userData});
      })
      .catch((e) => {
        console.log(e);
      });
  }

  handleFollowUnfollow = () => {
    let newState = !this.state.followed;
    this.setState({followed: newState});
    let url;
    if (newState === true) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userData.userId +
        '/follows/' +
        this.props.userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userData.userId +
        '/unfollows/' +
        this.props.userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
      })
      .catch((err) => {
        this.setState({followed: !newState});
        console.log(err);
      });
  };

  render() {
    const {item} = this.props;
    return (
      <TouchableOpacity
        style={{flexDirection: 'row', alignItems: 'center'}}
        onPress={() => this.handleFollowUnfollow()}>
        {this.state.userData.userId !== item.userId && (
          <Text
            style={[
              {
                color:
                  item.connectDepth > 0 && !this.state.followed
                    ? '#97a600'
                    : '#888',
                fontSize: 14,
                marginLeft: 4,
              },
              typography.Note2,
            ]}>
            {item.userConnectStatus &&
            item.userConnectStatus.connectStatus === 'PENDING_CONNECT' ? (
              'Pending'
            ) : item.connectDepth === 1 ? (
              '• 1st'
            ) : item.connectDepth === 2 ? (
              '• 2nd'
            ) : this.state.followed && this.state.userId !== item.userId ? (
              <Icon
                name={'TickRSS'}
                size={13}
                color={COLORS.dark_600}
                style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
              />
            ) : !this.state.followed && this.state.userId !== item.userId ? (
              <Icon
                name={'RSS'}
                size={13}
                color={COLORS.green_600}
                style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
              />
            ) : (
              ''
            )}
          </Text>
        )}
      </TouchableOpacity>
    );
  }
}
