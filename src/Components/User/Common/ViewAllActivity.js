import React, { Component } from 'react'
import { SafeAreaView, ScrollView, FlatList, Platform, StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import axios from 'axios'
import LinearGradient from 'react-native-linear-gradient'

import BlogDefault from '../../../../assets/BlogDefault.jpg'
import circleDefault from '../../../../assets/CirclesDefault.png'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import projectDefault from '../../../../assets/project-default.jpg'
import defaultStyle from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
export default class ViewAllActivity extends Component {

    constructor(props) {
        super(props)
        this.state = {
            allActivities: [],
            selected: 'ALL'
        }
    }

    componentDidMount() {
        this.getRecentActivity()
    }

    getRecentActivity = () => {
        let cancelToken
        if (typeof cancelToken != typeof undefined) {
            cancelToken.cancel("Cancelling the previous request")
        }
        cancelToken = axios.CancelToken.source()
        axios({
            method: "GET",
            url: `${REACT_APP_userServiceURL}/backend/activity/activityList?userId=${this.props.route.params.userId}&page=${0}&size=${100}&filterType=${this.state.selected}`,
            withCredentials: true,
            cancelToken: cancelToken.token,
        }).then((response) => {
            if (response && response.data && response.data.status === "200 OK") {
                this.setState({ allActivities: response.data.body.content })
                // console.log(response.data.body.content[0])
            }
        })
    }

    unixTime = (UNIX_timestamp) => {
        var date = new Date(UNIX_timestamp)
        var months = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
        ]
        var year = date.getFullYear()
        var month = months[date.getMonth()]
        var day = date.getDate()
        return day + ' ' + month + ' ' + year
    }

    convertToFirstCapital = (s) => {
        if (s) {
            if (s.length <= 1) {
                return s.toUpperCase()
            }
            return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase()
        }
        return s
    }

    getProjectDetails = (projectId) => {
        axios({
            method: 'get',
            url:
                REACT_APP_userServiceURL +
                "/participants/" +
                "find-project-description-details-by-userId-and-projectId/" +
                this.props.route.params.userId +
                "/" +
                projectId,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.body && response.data.message === 'Success!') {
                this.props.navigation.navigate('ProjectDetailView', {
                    slug: response.data.body.slug
                })
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    redirect = (data) => {
        if (data.parentType === 'FORUM') {
            this.props.navigation.navigate('ForumStack', {
                screen: 'ForumDetails',
                params: { slug: data.parentId, userId: this.props.route.params.userId }
            })
        }
        if (data.parentType === 'POST') {
            this.props.navigation.navigate(
                'IndividualFeedsPost',
                {
                    id: data.parentId
                },
            )
        }
        if (data.parentType === 'POLL' || data.parentType === 'POLLVOTE') {
            this.props.navigation.navigate('PollStack', {
                screen: 'PollDetails',
                params: { slug: data.parentId, userId: this.props.route.params.userId }
            })
        }
        if (data.parentType == "APPLYINFUNDRAISE" || data.parentType == "APPLYINSTORYBOOK" ||
            data.parentType == "APPLYINEXPEDITION" || data.parentType == "APPLYINASSIGNMENT" || data.parentType == "APPLYINTRAINING" ||
            data.parentType == "APPLYINEVENT" || data.parentType == "APPLYINJOB") {
            this.props.navigation.navigate('ProjectDetailView', {
                slug: data.parentId,
            })
        }
        if (data.parentType === 'STORY') {
            this.props.navigation.navigate('Explore')
        }
        if (data.parentType === 'ASSIGNMENT' || data.parentType === 'TRAINING' || data.parentType === 'EVENT'
            || data.parentType === 'EXPEDITION' || data.parentType === 'STORYBOOK' || data.parentType === 'FUNDRAISE' || data.parentType === 'JOB') {
            this.getProjectDetails(data.parentId)
        }
        if (data.parentType === 'FEEDBACK') {
            this.props.navigation.navigate('FeedBackStack')
        }
        if (data.type === 'CIRCLE') {
            this.props.navigation.navigate("CircleProfileStack", {
                screen: 'CircleProfile',
                params: { slug: data.parentId },
            })
        }
    }

    renderItem = (data) => {
        return (
            <View style={{ marginHorizontal: 6, marginTop: 10, marginLeft: this.state.allActivities && this.state.allActivities.length === 1 ? 30 : 0 }}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => {
                        // console.log(data)
                        this.redirect(data)
                    }}>
                    <View style={{ marginRight: 10 }}>

                        {(data.parentType === 'POLL' || data.secondaryType === 'POLL') ?
                            <View style={styles.usersico}><Icon name="Polls" size={80} color={COLORS.grey_400} style={styles.icon} /></View> :
                            (data.parentType === 'FORUM' || data.secondaryType === 'FORUM') ?
                                <View style={styles.usersico}><Icon name="WN_Forum_OL" size={80} color={COLORS.grey_400} style={styles.icon} /></View> :
                                data.activityAttachmentType === "POST_AUDIO" ?
                                    <View style={styles.usersico}><Icon name="Audio" size={80} color={COLORS.grey_400} style={styles.icon} /></View> :
                                    data.activityAttachmentType === "POST_LINK" ?
                                        <View style={styles.usersico}><Icon name="Link" size={80} color={COLORS.grey_400} style={styles.icon} />
                                        </View> :
                                        (data.activityAttachmentType === "POST_COVER_IMAGE" && data.coverImage === null) ?
                                            <View style={styles.usersico}><Image style={styles.image}
                                                source={BlogDefault}
                                            />
                                            </View>

                                            :
                                            <View style={styles.usersico}>
                                                {data.coverImage != null && data.coverImage != "" ?
                                                    <Image style={styles.image}
                                                        source={{ uri: data.coverImage }} /> :
                                                    (data.parentType === 'JOB' || data.parentType === 'ASSIGNMENT' || data.parentType === 'TRAINING' || data.parentType === 'EVENT' || data.parentType === 'EXPEDITION' || data.parentType === 'STORYBOOK' || data.parentType === 'FUNDRAISE'
                                                        || data.secondaryType === 'JOB' || data.secondaryType === 'ASSIGNMENT' || data.secondaryType === 'TRAINING' || data.secondaryType === 'EVENT' || data.secondaryType === 'EXPEDITION' || data.secondaryType === 'STORYBOOK' || data.secondaryType === 'FUNDRAISE' ||
                                                        data.type == "APPLYINJOB" || data.type == "APPLYINFUNDRAISE" || data.type == "APPLYINSTORYBOOK" ||
                                                        data.type == "APPLYINEXPEDITION" || data.type == "APPLYINASSIGNMENT" || data.type == "APPLYINTRAINING" ||
                                                        data.type == "APPLYINEVENT") ?
                                                        <Image style={styles.image} source={projectDefault} />
                                                        : data.type === 'CIRCLE' ?
                                                            <Image style={styles.image} source={circleDefault} />
                                                            : <Icon name="Img" size={80} color={COLORS.grey_400} style={styles.icon} />}
                                            </View>}
                        <View style={styles.profrcadesc}>

                            <View style={styles.linearGradientView}>
                                <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={styles.linearGradient}>
                                </LinearGradient>
                            </View>

                            {data.secondaryType == "COMMENT_REPLY" ?
                                <Text style={styles.textStyle}>Replied on a comment on</Text> :
                                data.type == "POLLVOTE" ?
                                    <Text style={styles.textStyle}>Voted on a Poll on</Text> :
                                    (data.type == "APPLYINJOB" || data.type == "APPLYINFUNDRAISE" || data.type == "APPLYINSTORYBOOK" ||
                                        data.type == "APPLYINEXPEDITION" || data.type == "APPLYINASSIGNMENT" || data.type == "APPLYINTRAINING" ||
                                        data.type == "APPLYINEVENT") ?
                                        <Text style={styles.textStyle}>Applied for{(data.type === "APPLYINEVENT" || data.type === "APPLYINASSIGNMENT" || data.type === "APPLYINEXPEDITION") ? "" : ""} {data.type === "APPLYINJOB" ? "Job" : data.type === "APPLYINSTORYBOOK" ? "Story Book" : data.type === "APPLYINFUNDRAISE" ? "FundRaise" :
                                            data.type === "APPLYINEXPEDITION" ? "Expedition" : data.type === "APPLYINASSIGNMENT" ? "Assignment" : data.type === "APPLYINTRAINING" ? "Training" : data.type === "APPLYINEVENT" ? "Event" : ""} on</Text> :
                                        data.type == "CIRCLE" ?
                                            <Text style={styles.textStyle}>Created a {this.convertToFirstCapital(data.type)} on</Text> :
                                            data.sharedAttachmentType != null ? (<Text style={styles.textStyle}>Shared
                                                {data.sharedAttachmentSize > 1 ? "" : (data.sharedAttachmentType == "IMAGE" || data.sharedAttachmentType == "AUDIO") ? " an " : " a "} {data.sharedAttachmentType == "COVER_IMAGE" ? "Blog" :
                                                    data.sharedAttachmentType == "VIDEO" ? "Video" :
                                                        data.sharedAttachmentType == "AUDIO" ? "Audio" :
                                                            data.sharedAttachmentType == "IMAGE" && data.sharedAttachmentSize > 1 ? "Images" :
                                                                data.sharedAttachmentType == "IMAGE" && data.sharedAttachmentSize === 1 ? "Image" :
                                                                    data.sharedAttachmentType} on</Text>) :

                                                data.type === "COMMENT" ? (<Text style={styles.textStyle}>Commented
                                                    on {(data.activityAttachmentType == "POST_IMAGE" || data.activityAttachmentType == "POST_AUDIO" || data.type == "EVENT" || data.type == "ASSIGNMENT" || data.type == "EXPEDITION") ? " an " : " a "} {data.parentType == 'FORUM' || data.secondaryType == 'FORUM' ? 'Forum' :
                                                        data.activityAttachmentType == "POST_AUDIO" ? " Audio" :
                                                        data.activityAttachmentType == "POST_COVER_IMAGE" ? " Blog" :
                                                            data.activityAttachmentType == "POST_VIDEO" ? " Video" :
                                                                data.activityAttachmentType == "POST_IMAGE" ? " Image" :
                                                                    data.activityAttachmentType == "POST_LINK" ? " Link" :
                                                                        data.parentType == "POST" && data.type == "COMMENT" ? "Post" :
                                                                            this.convertToFirstCapital(data.secondaryType)} on</Text>) :
                                                    data.secondaryType && data.secondaryType != null ?
                                                        <Text style={styles.textStyle}>{data.type == 'LIKE' ? 'Liked' : 'Shared'} {data.sharedAttachmentSize === 1 ? "" : (data.activityAttachmentType == "POST_IMAGE" || data.activityAttachmentType == "POST_AUDIO" || data.type == "EVENT" || data.type == "ASSIGNMENT" || data.type == "EXPEDITION") ? " an " : " a "}{data.activityAttachmentType == "POST_AUDIO" ? " Audio" :
                                                            data.activityAttachmentType == "POST_COVER_IMAGE" ? " Blog" :
                                                                data.activityAttachmentType == "POST_VIDEO" ? " Video" :
                                                                    data.activityAttachmentType == "IMAGE" && data.sharedAttachmentSize > 1 ? "Images" :
                                                                        data.activityAttachmentType == "IMAGE" && data.sharedAttachmentSize === 1 ? "Image" :
                                                                            data.activityAttachmentType == "POST_LINK" ? " Link" :
                                                                                data.secondaryType == "EXPLOREBLOG" ? " Blog" :
                                                                                    this.convertToFirstCapital(data.secondaryType)} on</Text> :
                                                        <Text style={styles.textStyle}>{data.type == 'LIKE' ? 'Liked' : 'Posted'} {data.sharedAttachmentSize > 1 ? "" : (data.activityAttachmentType == "POST_IMAGE" || data.activityAttachmentType == "POST_AUDIO" || data.type == "EVENT" || data.type == "ASSIGNMENT" || data.type == "EXPEDITION") ? " an " : " a "}{data.activityAttachmentType == "POST_AUDIO"
                                                            ? " Audio "
                                                            : data.activityAttachmentType == "POST_COVER_IMAGE"
                                                                ? " Blog "
                                                                : data.activityAttachmentType == "POST_VIDEO"
                                                                    ? " Video "
                                                                    : data.activityAttachmentType == "POST_IMAGE" && data.sharedAttachmentSize > 1
                                                                        ? " Images "
                                                                        : data.activityAttachmentType == "POST_IMAGE" && data.sharedAttachmentSize === 1
                                                                        ? " Image "
                                                                        : data.activityAttachmentType == "POST_LINK"
                                                                            ? " Link "
                                                                            : data.parentType == "POST" && data.type == "COMMENT"
                                                                                ? "Post " :
                                                                                this.convertToFirstCapital(data.secondaryType !== null
                                                                                    ? data.secondaryType
                                                                                    : data.type === "LIKE"
                                                                                        ? data.parentType
                                                                                         : data.type)} on</Text>}

                            <Text style={[defaultStyle.Body_1, { marginHorizontal: 12, color: COLORS.white }]}>{this.unixTime(data.createTime)}</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                {(data.activityAttachmentType == "POST_IMAGE" || data.activityAttachmentType == "IMAGE" || data.sharedAttachmentType == "IMAGE") && (data.sharedAttachmentSize > 1) ?
                    <Icon name="Dashboard" size={24} color={COLORS.white} style={[{ position: 'absolute', top: 0, right: 20 }]} />
                    : (data.activityAttachmentType == "POST_VIDEO" || data.sharedAttachmentType == "VIDEO") ?
                        <Icon name="Play" size={24} color={COLORS.white} style={[{ position: 'absolute', top: 0, right: 20 }]} />
                        : null}
            </View>
        )
    }

    listHeaderComponent = () => {
        return (
            <View style={styles.container}>
                <View style={styles.feedDetails}>
                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        alwaysBounceHorizontal={false}>
                        <TouchableOpacity
                            onPress={
                                () =>
                                    this.setState(
                                        {
                                            selected: 'ALL',
                                            allActivities: []
                                        },
                                        () => this.getRecentActivity()
                                    )
                            }
                            style={
                                this.state.selected === 'ALL'
                                    ? styles.selected
                                    : styles.notSelected
                            }>
                            <Text
                                style={
                                    this.state.selected === 'ALL'
                                        ? styles.selectedText
                                        : styles.notSelectedText
                                }>
                                Show All
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={
                                () =>
                                    this.setState(
                                        {
                                            selected: 'POST',
                                            allActivities: []
                                        },
                                        () => this.getRecentActivity()
                                    )
                            }
                            style={
                                this.state.selected === 'POST'
                                    ? styles.selected
                                    : styles.notSelected
                            }>
                            <Text
                                style={
                                    this.state.selected === 'POST'
                                        ? styles.selectedText
                                        : styles.notSelectedText
                                }>
                                Post
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={
                                () =>
                                    this.setState(
                                        {
                                            selected: 'PROJECT',
                                            allActivities: []
                                        },
                                        () => this.getRecentActivity()
                                    )
                            }
                            style={
                                this.state.selected === 'PROJECT'
                                    ? styles.selected
                                    : styles.notSelected
                            }>
                            <Text
                                style={
                                    this.state.selected === 'PROJECT'
                                        ? styles.selectedText
                                        : styles.notSelectedText
                                }>
                                Projects
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={
                                () =>
                                    this.setState(
                                        {
                                            selected: 'CIRCLE',
                                            allActivities: []
                                        },
                                        () => this.getRecentActivity()
                                    )
                            }
                            style={
                                this.state.selected === 'CIRCLE'
                                    ? styles.selected
                                    : styles.notSelected
                            }>
                            <Text
                                style={
                                    this.state.selected === 'CIRCLE'
                                        ? styles.selectedText
                                        : styles.notSelectedText
                                }>
                                Circle
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={
                                () =>
                                    this.setState(
                                        {
                                            selected: 'FORUM',
                                            allActivities: []
                                        },
                                        () => this.getRecentActivity()
                                    )
                            }
                            style={
                                this.state.selected === 'FORUM'
                                    ? styles.selected
                                    : styles.notSelected
                            }>
                            <Text
                                style={
                                    this.state.selected === 'FORUM'
                                        ? styles.selectedText
                                        : styles.notSelectedText
                                }>
                                Forum
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={
                                () =>
                                    this.setState(
                                        {
                                            selected: 'POLL',
                                            allActivities: []
                                        },
                                        () => this.getRecentActivity()
                                    )
                            }
                            style={
                                this.state.selected === 'POLL'
                                    ? styles.selected
                                    : styles.notSelected
                            }>
                            <Text
                                style={
                                    this.state.selected === 'POLL'
                                        ? styles.selectedText
                                        : styles.notSelectedText
                                }>
                                Poll
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={
                                () =>
                                    this.setState(
                                        {
                                            selected: 'FEEDBACK',
                                            allActivities: []
                                        },
                                        () => this.getRecentActivity()
                                    )
                            }
                            style={
                                this.state.selected === 'FEEDBACK'
                                    ? styles.selected
                                    : styles.notSelected
                            }>
                            <Text
                                style={
                                    this.state.selected === 'FEEDBACK'
                                        ? styles.selectedText
                                        : styles.notSelectedText
                                }>
                                Feedback
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={
                                () =>
                                    this.setState(
                                        {
                                            selected: 'LIKE',
                                            allActivities: []
                                        },
                                        () => this.getRecentActivity()
                                    )
                            }
                            style={
                                this.state.selected === 'LIKE'
                                    ? styles.selected
                                    : styles.notSelected
                            }>
                            <Text
                                style={
                                    this.state.selected === 'LIKE'
                                        ? styles.selectedText
                                        : styles.notSelectedText
                                }>
                                Like
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={
                                () =>
                                    this.setState(
                                        {
                                            selected: 'COMMENT',
                                            allActivities: []
                                        },
                                        () => this.getRecentActivity()
                                    )
                            }
                            style={
                                this.state.selected === 'COMMENT'
                                    ? styles.selected
                                    : styles.notSelected
                            }>
                            <Text
                                style={
                                    this.state.selected === 'COMMENT'
                                        ? styles.selectedText
                                        : styles.notSelectedText
                                }>
                                Comment
                            </Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </View>
        )
    }

    render() {

        if (this.state.allActivities.length === 0) {
            return (
                <>
                    {this.listHeaderComponent()}
                    <SafeAreaView
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 0
                        }}>

                        <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                            You have no activity in this section
                        </Text>
                    </SafeAreaView>
                </>
            )
        }
        else {
            return (
                <SafeAreaView style={{ flex: 1 }}>

                    {/* <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 12 }] : styles.header}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack()}>
                            <Icon name="Arrow-Left" size={14} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                        </TouchableOpacity>
                        <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_800, marginLeft: 5 }]}>
                            ALL ACTIVITIES
                        </Text>
                    </View>
                </View> */}

                    {this.listHeaderComponent()}

                    <FlatList
                        style={{}}
                        contentContainerStyle={{ alignSelf: this.state.allActivities && this.state.allActivities.length === 1 ? 'flex-start' : 'center', paddingTop: 10, paddingBottom: 20 }}
                        showsHorizontalScrollIndicator={false}
                        alwaysBounceHorizontal={false}
                        keyExtractor={(item) => item.activityId}
                        data={this.state.allActivities}
                        numColumns={2}
                        renderItem={(item) => this.renderItem(item.item)}
                    />
                </SafeAreaView>
            )
        }
    }
}

const styles = StyleSheet.create({
    usersico: {
        width: 150,
        height: 150,
        borderRadius: 3,
        backgroundColor: COLORS.grey_350,
        alignItems: 'center',
        justifyContent: 'center'
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    selected: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#367681',
        marginRight: 15,
        height: 27,
        borderRadius: 16,
        textAlign: 'center',
    },
    notSelected: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15,
        height: 27,
        borderWidth: 1,
        borderColor: '#698F8A',
        borderRadius: 16,
    },
    selectedText: {
        color: '#F7F7F5',
        fontSize: 12,
        paddingHorizontal: 14,
        fontFamily: 'Montserrat-Medium',
    },
    notSelectedText: {
        color: '#698F8A',
        fontSize: 12,
        paddingHorizontal: 14,
        fontFamily: 'Montserrat-Medium',
    },
    feedDetails: {
        marginTop: 20,
        marginLeft: 20,
        paddingBottom: 16,
    },
    container: {
        backgroundColor: '#00394D'
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6
    },
    linearGradientView: {
        width: '100%',
        height: 60,
        position: 'absolute',
        bottom: 0
    },
    icon: {
        marginTop: Platform.OS === 'android' ? 30 : 0
    },
    textStyle: {
        fontSize: 12,
        fontFamily: 'Montserrat-SemiBold',
        letterSpacing: 0,
        lineHeight: 18,
        marginHorizontal: 12,
        color: COLORS.white,
    },
    profrcadesc: {
        width: 150,
        height: 60,
        position: 'absolute',
        bottom: 0,
        justifyContent: 'center'
    },
    image: {
        width: 150,
        height: 150,
        borderRadius: 3
    }
})
