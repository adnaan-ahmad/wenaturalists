import React, {Component} from 'react';
import {
  Alert,
  TouchableHighlight,
  Linking,
  ActivityIndicator,
  Dimensions,
  Modal,
  ImageBackground,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Share,
  Clipboard,
  FlatList,
  Image,
  SafeAreaView,
  Platform,
  RefreshControl,
} from 'react-native';
import {cloneDeep} from 'lodash';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {connect} from 'react-redux';
import VideoPlayer from 'react-native-video-controls';
import LinearGradient from 'react-native-linear-gradient';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Snackbar from 'react-native-snackbar';
import RNUrlPreview from 'react-native-url-preview';
import {TextInput} from 'react-native-paper';
import Autolink from 'react-native-autolink';
import TimeAgo from 'react-native-timeago';

import circleDefault from '../../../../assets/CirclesDefault.png';
import BlogDefault from '../../../../assets/BlogDefault.jpg';
import LikedUserList from '../../../Components/User/Common/LikedUserList';
import Report from '../../../Components/User/Common/Report';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import {
  feedsPhotosRequest,
  feedsVideosRequest,
} from '../../../services/Redux/Actions/User/FeedsActions';
import {personalProfileRequest} from '../../../services/Redux/Actions/User/PersonalProfileActions';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import SearchBar from '../../../Components/User/SearchBar';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import httpService from '../../../services/AxiosInterceptors';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import typography from '../../../Components/Shared/Typography';
import postIcon from '../../../../assets/Post_Icon.png';
import blogIcon from '../../../../assets/Blog_Icon.png';
import linkIcon from '../../../../assets/Link_Icon.png';
import AudioThumb from '../../../../assets/AudioThumb.png';
import VideoThumb from '../../../../assets/VideoThumb.png';
import defaultStyle from '../../../Components/Shared/Typography';
import SharedUserList from '../../../Components/User/Common/SharedUserList';
import ConnectDepth from './ConnectDepth';
import Follow from './Follow';

httpService.setupInterceptors();
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const skeletonCount = [1, 2, 3, 4];
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
  'i',
);

class CommonFeeds extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 'LATEST',
      userId: '',
      notification: false,
      redirectToProfile: false,
      pageNumber: 0,
      pageSize: 30,
      postModalOpen: false,
      optionsModalOpen: false,
      shareModalOpen: false,
      likeModalOpen: false,
      readMore: false,
      currentScrollPosition: 0,
      hideControls: false,
      pressedActivityId: '',
      peopleLiked: [],
      isLoading: true,
      videoHeight: 190,
      videoWidth: '97%',
      entityType: '',
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      userType: '',
      selectedVideoId: '',
      isReported: false,
      feedsData: [],
      peopleShared: [],
      peopleSharedModalOpen: false,
      currentPressed: {},
      attachmentModalopen: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {});
    AsyncStorage.getItem('userId')
      .then((value) => {
        this.fetchFeedsData(value);
        this.setState({userId: value}, () => {});
        axios({
          method: 'get',
          url:
            REACT_APP_userServiceURL +
            '/profile/get?id=' +
            value +
            '&otherUserId=' +
            '',
          cache: true,
          withCredentials: true,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => {
            this.setState({userType: response.data.body.type});
          })
          .catch((err) => {});
      })
      .catch((e) => {
        // console.log(e)
      });

    try {
      messaging().onNotificationOpenedApp((remoteMessage) => {
        if (remoteMessage) {
        }
      });
    } catch (error) {
      // console.log(error)
    }

    try {
      messaging()
        .getInitialNotification()
        .then((remoteMessage) => {
          if (remoteMessage) {
          }
        });
    } catch (error) {
      // console.log(error)
    }
  }

  fetchFeedsData = (userId) => {
    let url;
    url =
      this.props.type === 'PROJECTFEEDS'
        ? REACT_APP_userServiceURL +
          '/post/project/getNewsFeed?projectId=' +
          this.props.projectId +
          '&userId=' +
          userId +
          '&newsFeedType=' +
          this.state.selected +
          '&page=' +
          this.state.pageNumber +
          '&size=10'
        : this.props.type === 'OWNFEEDS' && this.state.selected === 'TOP'
        ? REACT_APP_userServiceURL +
          '/post/getNewsFeed/trending?userId=' +
          userId +
          '&newsFeedType=' +
          this.state.selected +
          '&page=' +
          this.state.pageNumber +
          '&size=10'
        : this.props.type === 'OWNFEEDS' && this.state.selected !== 'TOP'
        ? REACT_APP_userServiceURL +
          '/post/getNewsFeed?userId=' +
          userId +
          '&newsFeedType=' +
          this.state.selected +
          '&page=' +
          this.state.pageNumber +
          '&size=10'
        : this.props.type === 'OTHERACTIVITY'
        ? REACT_APP_userServiceURL +
          '/post/user/getNewsFeed?userId=' +
          this.props.userId +
          '&postRequestingUserId=' +
          userId +
          '&newsFeedType=' +
          this.state.selected +
          '&page=' +
          this.state.pageNumber +
          '&size=10'
        : this.props.type === 'OWNACTIVITY'
        ? REACT_APP_userServiceURL +
          '/post/user/getNewsFeed?userId=' +
          userId +
          '&postRequestingUserId=' +
          userId +
          '&newsFeedType=' +
          this.state.selected +
          '&page=' +
          this.state.pageNumber +
          '&size=10'
        : this.props.type === 'TRENDING'
        ? REACT_APP_userServiceURL +
          '/post/getTrendingPosts?userId=' +
          userId +
          '&newsFeedType=' +
          this.props.newsFeedType +
          '&page=' +
          this.state.pageNumber +
          '&size=10'
        : '';
    axios({
      method: 'get',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          this.setState({
            feedsData: this.state.feedsData.concat(response.data.body.content),
            isLoading: false,
          });
        }
      })
      .catch((err) => {
        this.setState({isLoading: false});
        console.log(err);
      });
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleLoadMore = () => {
    this.setState({pageNumber: this.state.pageNumber + 1}, () =>
      this.fetchFeedsData(this.state.userId),
    );
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.user !== this.props.user) {
      return true;
    }
    if (nextProps.userFeedsPhotos !== this.props.userFeedsPhotos) {
      return true;
    }
    if (nextProps.userFeedsVideos !== this.props.userFeedsVideos) {
      return true;
    }
    if (
      nextProps.userFeedsPhotosProgress !== this.props.userFeedsPhotosProgress
    ) {
      return true;
    }
    if (
      nextProps.userFeedsVideosProgress !== this.props.userFeedsVideosProgress
    ) {
      return true;
    }

    if (nextState.currentPressed !== this.state.currentPressed) {
      return true;
    }
    if (nextState.feedsData !== this.state.feedsData) {
      return true;
    }
    if (nextState.selectedVideoId !== this.state.selectedVideoId) {
      return true;
    }
    if (nextState.userType !== this.state.userType) {
      return true;
    }
    if (nextState.description !== this.state.description) {
      return true;
    }
    if (nextState.reasonForReporting !== this.state.reasonForReporting) {
      return true;
    }
    if (
      nextState.reasonForReportingModalOpen !==
      this.state.reasonForReportingModalOpen
    ) {
      return true;
    }
    if (nextState.entityType !== this.state.entityType) {
      return true;
    }
    if (nextState.isLoading !== this.state.isLoading) {
      return true;
    }
    if (nextState.pageNumber !== this.state.pageNumber) {
      return true;
    }
    if (nextState.pageSize !== this.state.pageSize) {
      return true;
    }
    if (nextState.userId !== this.state.userId) {
      return true;
    }
    if (nextState.notification !== this.state.notification) {
      return true;
    }
    if (nextState.redirectToProfile !== this.state.redirectToProfile) {
      return true;
    }
    if (nextState.selected !== this.state.selected) {
      return true;
    }
    if (nextState.postModalOpen !== this.state.postModalOpen) {
      return true;
    }
    if (nextState.readMore !== this.state.readMore) {
      return true;
    }
    if (nextState.currentScrollPosition !== this.state.currentScrollPosition) {
      return true;
    }
    if (nextState.hideControls !== this.state.hideControls) {
      return true;
    }
    if (nextState.pressedActivityId !== this.state.pressedActivityId) {
      return true;
    }
    if (nextState.optionsModalOpen !== this.state.optionsModalOpen) {
      return true;
    }
    if (nextState.shareModalOpen !== this.state.shareModalOpen) {
      return true;
    }
    if (nextState.likeModalOpen !== this.state.likeModalOpen) {
      return true;
    }
    if (nextState.peopleLiked !== this.state.peopleLiked) {
      return true;
    }
    if (nextState.videoHeight !== this.state.videoHeight) {
      return true;
    }
    if (nextState.videoWidth !== this.state.videoWidth) {
      return true;
    }
    if (nextState.peopleSharedModalOpen !== this.state.peopleSharedModalOpen) {
      return true;
    }
    return false;
  }

  changeState = (value) => {
    this.setState(value);
  };

  handleFollowUnfollow = (isFollowed, userId) => {
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          console.log(response.status);
          let tempFeedsData = cloneDeep(this.state.feedsData);
          for (let i of tempFeedsData) {
            if (i.userId === userId) {
              // console.log(i.followed, !isFollowed);
              i.followed = !isFollowed;
            }
          }
          this.setState({feedsData: tempFeedsData});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderHeader = (item, index) => {
    return (
      <View style={styles.unreadNotiItem}>
        <View
          style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              item.userType === 'COMPANY'
                ? this.props.navigate('ProfileStack', {
                    screen: 'CompanyProfileScreen',
                    params: {userId: item.userId},
                  })
                : item.userType === 'INDIVIDUAL' &&
                  item.userId !== this.state.userId
                ? this.props.navigate('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: {userId: item.userId},
                  })
                : item.userType === 'INDIVIDUAL' &&
                  item.userId === this.state.userId
                ? this.props.navigate('ProfileStack', {
                    screen: 'ProfileScreen',
                    // params: {userId: item.userId},
                  })
                : null;
            }}>
            {item && item.originalProfileImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={{uri: item.originalProfileImage}}
              />
            ) : item && item.params && item.params.circleImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={{uri: item.params.circleImage}}
              />
            ) : item &&
              item.userType === 'INDIVIDUAL' &&
              !item.originalProfileImage ? (
              <Image
                style={[defaultShape.Media_Round, {backgroundColor: 'orange'}]}
                source={defaultProfile}
              />
            ) : item &&
              item.userType === 'COMPANY' &&
              !item.originalProfileImage ? (
              <Image
                style={[defaultShape.Media_Round, {backgroundColor: 'orange'}]}
                source={defaultBusiness}
              />
            ) : item &&
              item.params &&
              item.params.circleSlug &&
              !item.params.circleImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={circleDefault}
              />
            ) : (
              <></>
            )}
          </TouchableOpacity>
          <View
            style={{
              justifyContent: 'flex-start',
              marginLeft: 6,
            }}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Text
                numberOfLines={1}
                onPress={() => {
                  item.userType === 'COMPANY'
                    ? this.props.navigate('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                        params: {userId: item.userId},
                      })
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId !== this.state.userId
                    ? this.props.navigate('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.userId},
                      })
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId === this.state.userId
                    ? this.props.navigate('ProfileStack', {
                        screen: 'ProfileScreen',
                        // params: {userId: item.userId},
                      })
                    : null;
                }}
                style={[
                  typography.Title_2,
                  {
                    color: COLORS.dark_800,
                    maxWidth: 160,
                  },
                ]}>
                {item?.userName ? item.userName : item?.params?.circleTitle}
              </Text>
              {item?.connectDepth && item.userId != this.state.userId ? (
                <ConnectDepth
                  connectDepth={item.connectDepth}
                  key={item.connectDepth}
                />
              ) : (
                <Text></Text>
              )}
              {item.userEntityType !== 'CIRCLE' &&
                !this.state.isCompany &&
                item &&
                (!item.connectDepth ||
                  (item.connectDepth && item.connectDepth < 1)) && (
                  <Follow
                    item={item}
                    followed={item.followed}
                    userId={item.userId}
                    index={index}
                  />
                )}
            </View>

            <View style={{flexDirection: 'row'}}>
              {item && item.country ? (
                <Icon
                  name="Location"
                  color={COLORS.altgreen_300}
                  size={10}
                  style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                />
              ) : (
                <></>
              )}
              <Text
                numberOfLines={1}
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigate('EditorsDesk', {
                        id: item.id,
                        userId: this.state.userId,
                      })
                    : this.props.navigate('IndividualFeedsPost', {
                        id: item.id,
                        commentCount: item.commentCount,
                      })
                }
                style={[
                  {
                    color: COLORS.altgreen_300,
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                    maxWidth: 100,
                  },
                ]}>
                {item && item.country ? item.country : null}
                {/* {+ new Date() - item.createTime} */}
              </Text>
              {item && item.country ? (
                <Icon
                  name="Bullet_Fill"
                  color={COLORS.altgreen_300}
                  size={8}
                  style={{marginTop: Platform.OS === 'android' ? 1 : 2}}
                />
              ) : (
                <></>
              )}

              <Text
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigate('EditorsDesk', {
                        id: item.id,
                        userId: this.state.userId,
                      })
                    : this.props.navigate('IndividualFeedsPost', {
                        id: item.id,
                        commentCount: item.commentCount,
                      })
                }
                style={[
                  {
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                  },
                ]}>
                {/* {item && this.unixTime2(item.createTime)} */}
                <TimeAgo time={item.createTime} />
                {/* {+ new Date() - item.createTime} */}
              </Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          activeOpacity={0.5}
          style={[defaultShape.Nav_Gylph_Btn, {}]}
          onPress={() =>
            this.setState(
              {
                optionsModalOpen: true,
                pressedActivityId: item.id,
                currentUserId: item.userId,
                canReport: item.canReport,
                currentPressed: item,
              },
              () => this.verifyReported(),
            )
          }>
          <Icon
            name="Kebab"
            color={COLORS.altgreen_300}
            size={14}
            style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
          />
        </TouchableOpacity>
      </View>
    );
  };

  renderSharedHeader = (item) => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          item.userType === 'INDIVIDUAL' && item.userId === this.state.userId
            ? this.props.navigate('ProfileStack', {
                screen: 'ProfileScreen',
                // params: {userId: item.userId},
              })
            : item.userType === 'INDIVIDUAL' &&
              item.userId !== this.state.userId
            ? this.props.navigate('ProfileStack', {
                screen: 'OtherProfileScreen',
                params: {userId: item.userId},
              })
            : item.userType === 'COMPANY'
            ? this.props.navigate('ProfileStack', {
                screen: 'CompanyProfileScreen',
                params: {userId: item.userId},
              })
            : this.props.navigate('CircleProfileStack', {
                screen: 'CircleProfile',
                params: {slug: item?.params?.circleSlug},
              });
          // console.log(item)
        }}
        style={styles.unreadNotiItem}>
        {item?.username || (item.params && item.params.circleTitle) ? (
          <View style={{width: '40%', flexDirection: 'row'}}>
            {item.profileImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={{uri: item.profileImage}}
              />
            ) : item.params && item.params.circleImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={{uri: item.params.circleImage}}
              />
            ) : (
              <Image
                style={[defaultShape.Media_Round]}
                source={
                  item.userType === 'INDIVIDUAL'
                    ? defaultProfile
                    : item.userType === 'INDIVIDUAL'
                    ? defaultProfile
                    : item.userType === 'COMPANY'
                    ? defaultBusiness
                    : circleDefault
                }
              />
            )}

            <View style={{alignItems: 'flex-start'}}>
              <Text
                // onPress={() => console.log(item)}
                numberOfLines={1}
                style={[
                  defaultStyle.Title_2,
                  {color: COLORS.dark_800, marginLeft: 8},
                ]}>
                {item?.username
                  ? item.username.split(' ').slice(0, 2).join(' ')
                  : item?.params && item.params.circleTitle
                  ? item.params.circleTitle
                  : null}
              </Text>

              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                {item && item.addressDetail && item.addressDetail.country ? (
                  <Icon
                    name="Location"
                    color={COLORS.altgreen_300}
                    size={10}
                    style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                  />
                ) : (
                  <></>
                )}
                <Text
                  onPress={() =>
                    item && item.postType === 'ARTICLE'
                      ? this.props.navigate('EditorsDesk', {
                          id: item.id,
                          userId: this.state.userId,
                        })
                      : this.props.navigate('IndividualFeedsPost', {
                          id: item.id,
                          commentCount: item.commentCount,
                        })
                  }
                  style={[
                    {
                      color: COLORS.altgreen_300,
                      marginLeft: 2,
                      fontSize: 10,
                      fontFamily: 'Montserrat-Medium',
                    },
                  ]}>
                  {item && item.addressDetail && item.addressDetail.country
                    ? item.addressDetail &&
                      item.addressDetail.country
                        .split(' ')
                        .splice(0, 2)
                        .join(' ')
                    : null}
                  {/* {+ new Date() - item.createTime} */}
                </Text>
                {item && item.addressDetail && item.addressDetail.country ? (
                  <Icon
                    name="Bullet_Fill"
                    color={COLORS.altgreen_300}
                    size={8}
                    style={{marginTop: Platform.OS === 'android' ? 0 : 2}}
                  />
                ) : (
                  <></>
                )}

                <Text
                  onPress={() =>
                    item && item.postType === 'ARTICLE'
                      ? this.props.navigate('EditorsDesk', {
                          id: item.id,
                          userId: this.state.userId,
                        })
                      : this.props.navigate('IndividualFeedsPost', {
                          id: item.id,
                          commentCount: item.commentCount,
                        })
                  }
                  style={[
                    {
                      marginLeft: 2,
                      fontSize: 10,
                      fontFamily: 'Montserrat-Medium',
                    },
                  ]}>
                  <TimeAgo time={item.createTime} />
                  {/* {+ new Date() - item.createTime} */}
                </Text>
              </View>
            </View>
          </View>
        ) : (
          <View style={{}}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 2},
              ]}>
              {item.title}
            </Text>
            {item.location ? (
              <View
                style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                <Icon
                  name="Location"
                  color={COLORS.altgreen_300}
                  size={10}
                  style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                />
                <Text
                  style={[
                    {
                      color: COLORS.altgreen_300,
                      marginLeft: 2,
                      fontSize: 10,
                      fontFamily: 'Montserrat-Medium',
                    },
                  ]}>
                  {item.location.city}, {item.location.state},{' '}
                  {item.location.country}
                </Text>
              </View>
            ) : (
              <></>
            )}
          </View>
        )}
      </TouchableOpacity>
    );
  };

  renderFooter = (item, index) => {
    return (
      <View style={[styles.unreadNotiItem, {paddingRight: 26, marginTop: 10}]}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() =>
              this.setState(
                {pressedActivityId: item.id},
                this.handleLike(item.id, item.liked, index),
              )
            }
            activeOpacity={0.5}
            style={{alignItems: 'center', flexDirection: 'row'}}>
            <Icon
              name={item && item.liked ? 'Like_FL' : 'Like'}
              color={COLORS.green_500}
              size={14}
              style={{
                marginTop: Platform.OS === 'android' ? 10 : 0,
                marginHorizontal: 6,
              }}
            />
            <Text style={[{color: COLORS.altgreen_300}, typography.Caption]}>
              {item && item.likesCount}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.setState({selectedVideoId: ''});
              item && item.postType === 'ARTICLE'
                ? this.props.navigate('EditorsDesk', {
                    id: item.id,
                    userId: this.state.userId,
                  })
                : this.props.navigate('IndividualFeedsPost', {
                    id: item.id,
                    commentCount: item.commentCount,
                  });
            }}
            activeOpacity={0.5}
            style={{
              alignItems: 'center',
              flexDirection: 'row',
              marginLeft: 12,
            }}>
            <Icon
              name="Comment"
              color={COLORS.green_500}
              size={14}
              style={{
                marginTop: Platform.OS === 'android' ? 10 : 0,
                marginRight: 6,
              }}
            />
            <Text style={[{color: COLORS.altgreen_300}, typography.Caption]}>
              {item && item.commentCount}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  getUsersWhoLiked = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/like/getUsers/' +
        this.state.pressedActivityId +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          this.setState({peopleLiked: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleLike = (activityId, liked, index) => {
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log(response.data.message);
          let tempFeedsData = cloneDeep(this.state.feedsData);
          tempFeedsData[index].liked = !liked;
          liked === false
            ? (tempFeedsData[index].likesCount += 1)
            : (tempFeedsData[index].likesCount -= 1);
          this.setState({feedsData: tempFeedsData});
        } else {
          // this.setState({ 'liked': !newState })
        }
      })
      .catch((err) => {
        // this.setState({ 'liked': !newState, 'likeSuccess': true })
        console.log(err);
      });
  };

  unixTime = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    return day + ' ' + month + ' ' + year;
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + 's';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + 'm';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + 'h';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + 'd';
    }

    if (difference >= 864000) {
      return day + ' ' + month + ' ' + year;
    }
  };

  renderImage = (item) => {
    if (item && item.attachmentIds?.length > 5) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            this.props.navigate('IndividualFeedsPost', {
              id: item.id,
              commentCount: item.commentCount,
            })
          }>
          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 182, width: '50%'}}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
            <Image
              style={{height: 182, width: '50%'}}
              source={{uri: item.attachmentIds[1].attachmentUrl}}
            />
          </View>

          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 120, width: '33.33%'}}
              source={{uri: item.attachmentIds[2].attachmentUrl}}
            />
            <Image
              style={{height: 120, width: '33.33%'}}
              source={{uri: item.attachmentIds[3].attachmentUrl}}
            />

            <ImageBackground
              source={{uri: item.attachmentIds[4].attachmentUrl}}
              imageStyle={{}}
              style={{height: 120, width: '57.8%'}}>
              <View
                style={{
                  height: 120,
                  width: '57.8%',
                  backgroundColor: COLORS.dark_900 + 'BF',
                  opacity: 0.95,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'Montserrat-Bold',
                    fontSize: 28,
                    color: COLORS.white,
                  }}>
                  +{item && item.attachmentIds.length - 4}
                </Text>
              </View>
            </ImageBackground>
          </View>
        </TouchableOpacity>
      );
    } else if (item && item.attachmentIds.length === 5) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            this.props.navigate('IndividualFeedsPost', {
              id: item.id,
              commentCount: item.commentCount,
            })
          }>
          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 182, width: '50%'}}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
            <Image
              style={{height: 182, width: '50%'}}
              source={{uri: item.attachmentIds[1].attachmentUrl}}
            />
          </View>

          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 120, width: '33.33%'}}
              source={{uri: item.attachmentIds[2].attachmentUrl}}
            />
            <Image
              style={{height: 120, width: '33.33%'}}
              source={{uri: item.attachmentIds[3].attachmentUrl}}
            />
            <Image
              style={{height: 120, width: '33.33%'}}
              source={{uri: item.attachmentIds[4].attachmentUrl}}
            />
          </View>
        </TouchableOpacity>
      );
    } else if (item && item.attachmentIds.length === 4) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            this.props.navigate('IndividualFeedsPost', {
              id: item.id,
              commentCount: item.commentCount,
            })
          }>
          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[1].attachmentUrl}}
            />
          </View>

          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[2].attachmentUrl}}
            />
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[3].attachmentUrl}}
            />
          </View>
        </TouchableOpacity>
      );
    } else if (item && item.attachmentIds.length === 3) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            this.props.navigate('IndividualFeedsPost', {
              id: item.id,
              commentCount: item.commentCount,
            })
          }>
          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '100%'}}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
          </View>

          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[1].attachmentUrl}}
            />
            <Image
              style={{height: 151, width: '50%'}}
              source={{uri: item.attachmentIds[2].attachmentUrl}}
            />
          </View>
        </TouchableOpacity>
      );
    } else if (item && item.attachmentIds.length === 2) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            this.props.navigate('IndividualFeedsPost', {
              id: item.id,
              commentCount: item.commentCount,
            })
          }>
          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '100%'}}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
          </View>

          <View style={{flexDirection: 'row', marginLeft: -10}}>
            <Image
              style={{height: 151, width: '100%'}}
              source={{uri: item.attachmentIds[1].attachmentUrl}}
            />
          </View>
        </TouchableOpacity>
      );
    } else if (item && item.attachmentIds.length === 1) {
      return (
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            item && item.postType === 'ARTICLE'
              ? this.props.navigate('EditorsDesk', {
                  id: item.id,
                  userId: this.state.userId,
                })
              : this.props.navigate('IndividualFeedsPost', {
                  id: item.id,
                  commentCount: item.commentCount,
                })
          }>
          {item &&
          item.attachmentIds[0].attachmentType === 'AUDIO' &&
          this.state.selected !== 'VIDEOS' ? (
            <VideoPlayer
              style={{width: '97%', height: 80}}
              tapAnywhereToPause={true}
              disableFullscreen={true}
              disableSeekbar={true}
              disableVolume={true}
              disableTimer={false}
              disableBack={true}
              poster="https://www.agbiz.co.za/Content/images/audio.jpg"
              paused={true}
              audioOnly={true}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
          ) : item &&
            item.attachmentIds[0].attachmentType === 'VIDEO' &&
            this.state.selected !== 'AUDIO' ? (
            this.state.selectedVideoId !== item.id ? (
              <TouchableOpacity
                activeOpacity={0.9}
                // onPress={() => this.setState({ selectedVideoId: item.id })}
                onPress={() => {
                  this.setState({selectedVideoId: ''});
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigate('EditorsDesk', {
                        id: item.id,
                        userId: this.state.userId,
                      })
                    : this.props.navigate('IndividualFeedsPost', {
                        id: item.id,
                        commentCount: item.commentCount,
                      });
                }}>
                <ImageBackground
                  style={{
                    height: this.state.videoHeight,
                    width: this.state.videoWidth,
                    marginLeft: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  source={{
                    uri:
                      item.attachmentIds[0] && item.attachmentIds[0].thumbnails
                        ? item.attachmentIds[0].thumbnails[0]
                        : defaultShape,
                  }}>
                  <Icon
                    name="Play"
                    size={36}
                    color={COLORS.white}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </ImageBackground>
              </TouchableOpacity>
            ) : (
              <VideoPlayer
                style={{
                  width: this.state.videoWidth,
                  height: this.state.videoHeight,
                }}
                // poster={item.attachmentIds[0].thumbnails[0]}
                // disableBack={true}
                tapAnywhereToPause={true}
                disableFullscreen={true}
                disableSeekbar={true}
                disableVolume={true}
                disableTimer={true}
                disableBack={true}
                paused={false}
                source={{uri: item ? item.attachmentIds[0].attachmentUrl : ''}}
                navigator={this.props.navigator}
              />
            )
          ) : (
            <Image
              style={{height: 345, marginLeft: 0}}
              resizeMode={'contain'}
              source={
                item.attachmentIds &&
                item.attachmentIds[0] &&
                item.attachmentIds[0].attachmentUrl
                  ? {uri: item.attachmentIds[0].attachmentUrl}
                  : ''
              }
            />
          )}
        </TouchableOpacity>
      );
    } else return <></>;
  };

  postModal = () => {
    return (
      <Modal
        visible={this.state.postModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({postModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              },
            ]}>
            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigate('NewFeedsPost', {link: false}),
                );
              }}>
              {/* <Icon name="Add_Post" size={44} color={COLORS.primarygreen} style={{ alignSelf: 'center' }} /> */}
              <Image style={{alignSelf: 'center'}} source={postIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    alignSelf: 'center',
                    position: 'absolute',
                    top: 50,
                  },
                ]}>
                Post
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigate('NewBlogPost'),
                );
              }}>
              {/* <Icon name='Blog' size={44} color={COLORS.primarygreen} /> */}
              <Image style={{alignSelf: 'center'}} source={blogIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    position: 'absolute',
                    top: 47,
                  },
                ]}>
                Blog
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigate('NewFeedsPost', {link: true}),
                );
              }}>
              {/* <Icon name='Link_Post' size={44} color={COLORS.primarygreen} /> */}
              <Image style={{alignSelf: 'center'}} source={linkIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    position: 'absolute',
                    top: 50,
                  },
                ]}>
                Link
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleHideModal = (id) => {
    let data = {
      userId: this.state.userId,
      activityId: id,
      entityType: 'POST',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          this.setState({optionsModalOpen: false});

          // this.props.feedsPhotosRequest({
          //   userId: this.state.userId,
          //   type: this.state.selected,
          //   pageNumber: this.state.pageNumber,
          //   size: this.state.pageSize,
          // })
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response.status)
        }
      });
    this.setState({
      feedsData: this.state.feedsData.filter((item) => item.id !== id),
    });
  };

  peopleSharedModal = () => {
    return (
      <Modal
        visible={this.state.peopleSharedModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({peopleSharedModalOpen: false, peopleShared: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who Shared this post
              </Text>
            </View>

            <SharedUserList
              id={this.state.pressedActivityId}
              navigation={this.navigation2}
            />
          </View>
        </View>
      </Modal>
    );
  };

  getUsersWhoShared = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/share/getUsers/' +
        this.state.pressedActivityId +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        100,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          this.setState({peopleShared: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({optionsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Post
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState(
                  {optionsModalOpen: false, likeModalOpen: true},
                  () => this.getUsersWhoLiked(),
                );
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Like_FL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  See who liked it
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState(
                  {optionsModalOpen: false, peopleSharedModalOpen: true},
                  () => this.getUsersWhoShared(),
                );
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  See who shared it
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({optionsModalOpen: false, shareModalOpen: true});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Share
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.handleHideModal(this.state.pressedActivityId);
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Hide
              </Text>
              <Icon
                name="Hide"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            {this.state.currentPressed.creatorId === this.state.userId &&
              this.state.currentPressed.postType !== 'ARTICLE' && (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {paddingVertical: 15},
                        ]
                      : [defaultShape.ActList_Cell_Gylph_Alt]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({optionsModalOpen: false}, () =>
                      this.props.navigate('EditPost', {
                        pressedActivityId: this.state.currentPressed.id,
                        entityType: this.state.currentPressed.postType,
                        description: this.state.currentPressed.description,
                        hashTags: this.state.currentPressed.hashTags,
                      }),
                    );
                  }}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    Edit Post
                  </Text>
                  <Icon
                    name="EditBox"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                </TouchableOpacity>
              )}

            {this.state.currentPressed.creatorId === this.state.userId ? (
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [defaultShape.ActList_Cell_Gylph_Alt]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.deletePostAlert();
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Delete Post
                </Text>
                <Icon
                  name="Hide"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    optionsModalOpen: false,
                    reasonForReportingModalOpen: !this.state.isReported
                      ? true
                      : false,
                  });
                  this.state.isReported
                    ? setTimeout(() => {
                        Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Your report request was already taken',
                          duration: Snackbar.LENGTH_LONG,
                        });
                      }, 1000)
                    : null;
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Report
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </Modal>
    );
  };

  likeModal = () => {
    return (
      <Modal
        visible={this.state.likeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({likeModalOpen: false, peopleLiked: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who liked this post
              </Text>
            </View>

            <LikedUserList
              id={this.state.pressedActivityId}
              navigation={this.navigation2}
            />
          </View>
        </View>
      </Modal>
    );
  };

  navigation2 = (value, params) => {
    this.setState({likeModalOpen: false, peopleSharedModalOpen: false}, () =>
      this.props.navigate(value, params),
    );
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message: REACT_APP_domainUrl + '/post/' + this.state.pressedActivityId,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // console.log('shared with activity type of result.activityType')
        } else {
          // console.log('shared')
        }
      } else if (result.action === Share.dismissedAction) {
        // console.log('dismissed')
      }
    } catch (error) {
      // console.log(error.message)
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigate('RepostOnWenat', {
                    link: false,
                    pressedActivityId: this.state.pressedActivityId,
                    entityType: 'POST',
                  }),
                );
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl + '/post/' + this.state.pressedActivityId,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleScroll = (event) => {
    // console.log(event.nativeEvent.contentOffset.y)
    this.setState({currentScrollPosition: event.nativeEvent.contentOffset.y});
  };

  goToTop = () => {
    this.flatListRef.scrollToOffset({animated: true, offset: 0});
  };

  listHeaderComponent = () => {
    return this.props.type !== 'PROJECTFEEDS' &&
      this.props.type !== 'TRENDING' ? (
      <View style={styles.container}>
        <View
          style={[
            styles.feedDetails,
            {
              marginRight:
                this.props.type === 'OWNACTIVITY' ||
                this.props.type === 'OTHERACTIVITY'
                  ? 56
                  : 0,
            },
          ]}>
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            alwaysBounceHorizontal={false}>
            <TouchableOpacity
              onPress={
                () =>
                  this.setState(
                    {
                      pageNumber: 0,
                      selected: 'LATEST',
                      isLoading: true,
                      feedsData: [],
                    },
                    () => this.fetchFeedsData(this.state.userId),
                  )
                // this.setState({ selected: 'LATEST', isLoading: true }, () =>
                //   this.props.feedsPhotosRequest({
                //     userId: this.state.userId,
                //     type: this.state.selected,
                //     pageNumber: this.state.pageNumber,
                //     size: this.state.pageSize,
                //   }),
                // )
              }
              style={
                this.state.selected === 'LATEST'
                  ? styles.selected
                  : styles.notSelected
              }>
              <Text
                style={
                  this.state.selected === 'LATEST'
                    ? styles.selectedText
                    : styles.notSelectedText
                }>
                Latest
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={
                () =>
                  this.setState(
                    {
                      pageNumber: 0,
                      selected: 'TOP',
                      isLoading: true,
                      feedsData: [],
                    },
                    () => this.fetchFeedsData(this.state.userId),
                  )
                // this.setState({ selected: 'TOP', isLoading: true }, () =>
                //   this.props.feedsPhotosRequest({
                //     userId: this.state.userId,
                //     type: this.state.selected,
                //     pageNumber: this.state.pageNumber,
                //     size: this.state.pageSize,
                //   }),
                // )
              }
              style={
                this.state.selected === 'TOP'
                  ? styles.selected
                  : styles.notSelected
              }>
              <Text
                style={
                  this.state.selected === 'TOP'
                    ? styles.selectedText
                    : styles.notSelectedText
                }>
                Trending
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={
                () =>
                  this.setState(
                    {
                      pageNumber: 0,
                      selected: 'PHOTOS',
                      isLoading: true,
                      feedsData: [],
                    },
                    () => this.fetchFeedsData(this.state.userId),
                  )
                // this.setState({ selected: 'PHOTOS', isLoading: true }, () =>
                //   this.props.feedsPhotosRequest({
                //     userId: this.state.userId,
                //     type: this.state.selected,
                //     pageNumber: this.state.pageNumber,
                //     size: this.state.pageSize,
                //   }),
                // )
              }
              style={
                this.state.selected === 'PHOTOS'
                  ? styles.selected
                  : styles.notSelected
              }>
              <Text
                style={
                  this.state.selected === 'PHOTOS'
                    ? styles.selectedText
                    : styles.notSelectedText
                }>
                Photos
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={
                () =>
                  this.setState(
                    {
                      pageNumber: 0,
                      selected: 'VIDEOS',
                      isLoading: true,
                      feedsData: [],
                    },
                    () => this.fetchFeedsData(this.state.userId),
                  )
                // this.setState({ selected: 'VIDEOS', isLoading: true }, () =>
                //   this.props.feedsPhotosRequest({
                //     userId: this.state.userId,
                //     type: this.state.selected,
                //     pageNumber: this.state.pageNumber,
                //     size: this.state.pageSize,
                //   }),
                // )
              }
              style={
                this.state.selected === 'VIDEOS'
                  ? styles.selected
                  : styles.notSelected
              }>
              <Text
                style={
                  this.state.selected === 'VIDEOS'
                    ? styles.selectedText
                    : styles.notSelectedText
                }>
                Videos
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={
                () =>
                  this.setState(
                    {
                      pageNumber: 0,
                      selected: 'ARTICLES',
                      isLoading: true,
                      feedsData: [],
                    },
                    () => this.fetchFeedsData(this.state.userId),
                  )
                // this.setState({ selected: 'ARTICLES', isLoading: true }, () =>
                //   this.props.feedsPhotosRequest({
                //     userId: this.state.userId,
                //     type: this.state.selected,
                //     pageNumber: this.state.pageNumber,
                //     size: this.state.pageSize,
                //   }),
                // )
              }
              style={
                this.state.selected === 'ARTICLES'
                  ? styles.selected
                  : styles.notSelected
              }>
              <Text
                style={
                  this.state.selected === 'ARTICLES'
                    ? styles.selectedText
                    : styles.notSelectedText
                }>
                Blogs
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={
                () =>
                  this.setState(
                    {
                      pageNumber: 0,
                      selected: 'AUDIO',
                      isLoading: true,
                      feedsData: [],
                    },
                    () => this.fetchFeedsData(this.state.userId),
                  )
                // this.setState({ selected: 'AUDIO', isLoading: true }, () =>
                //   this.props.feedsPhotosRequest({
                //     userId: this.state.userId,
                //     type: this.state.selected,
                //     pageNumber: this.state.pageNumber,
                //     size: this.state.pageSize,
                //   }),
                // )
              }
              style={
                this.state.selected === 'AUDIO'
                  ? styles.selected
                  : styles.notSelected
              }>
              <Text
                style={
                  this.state.selected === 'AUDIO'
                    ? styles.selectedText
                    : styles.notSelectedText
                }>
                Audio
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>

        {(this.props.type === 'OWNACTIVITY' ||
          this.props.type === 'OTHERACTIVITY') && (
          <TouchableOpacity
            onPress={() =>
              this.props.navigate('ViewAllActivity', {
                userId: this.props.userId,
              })
            }
            style={{
              borderRadius: 17,
              borderColor: '#698F8A',
              borderWidth: 1,
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              right: 14,
              top: 16,
              alignSelf: 'center',
              paddingHorizontal: 10,
              height: 32,
              zIndex: 2,
            }}
            activeOpacity={0.6}>
            <Icon
              name="Dashboard"
              size={14}
              color="#698F8A"
              style={{marginTop: Platform.OS === 'android' && 6}}
            />
          </TouchableOpacity>
        )}
      </View>
    ) : (
      <></>
    );
  };

  trimDescription = (item) => {
    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  openWebsite = (website) => {
    if (website && website.includes('http')) return Linking.openURL(website);
    else if (website && !website.includes('http'))
      return Linking.openURL('https://' + website);
  };

  getUserDetailsByCustomUrl = (customurl) => {
    console.log('initial');
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get_by_custom_url?customUrl=' +
        customurl +
        '&otherUserId=' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        console.log('initial res', response.data.body);
        if (response && response.data && response.data.message === 'Success!') {
          response.data.body &&
          response.data.body.type === 'INDIVIDUAL' &&
          response.data.body.userId === this.state.userId
            ? this.props.navigate('ProfileStack')
            : response.data.body.type === 'INDIVIDUAL' &&
              response.data.body.userId !== this.state.userId
            ? this.props.navigate('ProfileStack', {
                screen: 'OtherProfileScreen',
                params: {userId: response.data.body.userId},
              })
            : response.data.body.type === 'COMPANY'
            ? this.props.navigate('ProfileStack', {
                screen: 'CompanyProfileScreen',
                params: {userId: response.data.body.userId},
              })
            : this.props.navigate('CircleProfileStack', {
                screen: 'CircleProfile',
                params: {slug: customurl},
              });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // mention auto link
  tagDescription = (item) => {
    item = item.split('@@@__').join('@[');
    item = item.split('^^__').join('](');
    item = item.split('@@@^^^').join(')');
    item = item.split('###__').join('#[');
    item = item.split('&&__').join('](');
    item = item.split('###^^^').join(')');
    return item;
  };

  renderDescription = (item) => {
    return item.postType ? (
      <View
        style={{
          backgroundColor:
            item.postType === 'ARTICLE' ? COLORS.altgreen_100 : COLORS.white,
        }}>
        {item.postType === 'ARTICLE' ? (
          <Text
            numberOfLines={1}
            style={[
              typography.H6,
              {
                color: COLORS.dark_800,
                marginLeft: 6,
                marginRight: 17,
                marginTop: 5,
              },
            ]}>
            {item.title}
          </Text>
        ) : (
          <Text></Text>
        )}
        <Autolink
          text={this.trimDescription(this.tagDescription(item.description))}
          email
          hashtag="instagram"
          mention="twitter"
          phone="sms"
          numberOfLines={5}
          matchers={[
            {
              pattern: /@\[([^[]*)]\(([^(^)]*)\)/g,
              style: {color: COLORS.mention_color, fontWeight: 'bold'},
              getLinkText: (replacerArgs) => `${replacerArgs[2]}`,
              onPress: (match) => {
                this.getUserDetailsByCustomUrl(match.getReplacerArgs()[1]);
              },
            },
            {
              pattern: /#\[([^[]*)]\(([^(^)]*)\)/g,
              style: {color: COLORS.mention_color, fontWeight: 'bold'},
              getLinkText: (replacerArgs) => `#${replacerArgs[2]}`,
              onPress: (match) => {
                this.props.navigate('HashTagDetail', {
                  slug: match.getReplacerArgs()[1],
                });
              },
            },
          ]}
          style={[
            typography.Body_1,
            {
              color:
                item.postType === 'ARTICLE' ? COLORS.dark_500 : COLORS.dark_700,
              marginLeft: 6,
              marginRight: 17,
              marginVertical: 8,
            },
          ]}
          url
        />
      </View>
    ) : (
      <Autolink
        text={this.tagDescription(item.description)}
        email
        hashtag="instagram"
        mention="twitter"
        phone="sms"
        numberOfLines={5}
        matchers={[
          {
            pattern: /@\[([^[]*)]\(([^(^)]*)\)/g,
            style: {color: COLORS.mention_color, fontWeight: 'bold'},
            getLinkText: (replacerArgs) => `${replacerArgs[2]}`,
            onPress: (match) => {
              this.getUserDetailsByCustomUrl(match.getReplacerArgs()[1]);
            },
          },
          {
            pattern: /#\[([^[]*)]\(([^(^)]*)\)/g,
            style: {color: COLORS.mention_color, fontWeight: 'bold'},
            getLinkText: (replacerArgs) => `#${replacerArgs[2]}`,
            onPress: (match) => {
              this.props.navigate('HashTagDetail', {
                slug: match.getReplacerArgs()[1],
              });
            },
          },
        ]}
        style={[
          typography.Body_1,
          {
            color:
              item.postType === 'ARTICLE' ? COLORS.dark_500 : COLORS.dark_700,
            marginLeft: 6,
            marginRight: 17,
            marginVertical: 8,
          },
        ]}
        url
      />
    );
  };

  renderSharedPost = (item, sharedEntityType) => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          this.setState({selectedVideoId: ''});
          sharedEntityType && sharedEntityType === 'FORUM'
            ? this.props.navigate('ForumStack', {
                screen: 'ForumDetails',
                params: {slug: item.slug, userId: this.state.userId},
              })
            : sharedEntityType && sharedEntityType === 'POLL'
            ? this.props.navigate('PollStack', {
                screen: 'PollDetails',
                params: {slug: item.slug, userId: this.state.userId},
              })
            : item && item.postType === 'ARTICLE'
            ? this.props.navigate('EditorsDesk', {
                id: item.id,
                userId: this.state.userId,
              })
            : sharedEntityType === 'JOB' ||
              sharedEntityType === 'EVENT' ||
              sharedEntityType === 'TRAINING' ||
              sharedEntityType === 'ASSIGNMENT' ||
              sharedEntityType === 'EXPEDITION' ||
              sharedEntityType === 'FUNDRAISE' ||
              sharedEntityType === 'STORYBOOK'
            ? this.props.navigate('ProjectDetailView', {
                slug: item.slug,
              })
            : this.props.navigate('IndividualFeedsPost', {
                id: item.id,
                commentCount: item.commentCount,
              });
        }}
        style={styles.renderSharedPost}>
        {item.userId !== this.state.userId && this.renderSharedHeader(item)}

        {item.description && item.postType !== 'ARTICLE' ? (
          this.renderDescription(item)
        ) : (
          <View style={{backgroundColor: 'transparent', height: 10}}></View>
        )}
        {item && (item.postType === 'LINK' || item.news_url) ? (
          <RNUrlPreview
            text={item.postLinkTypeUrl || item.news_url}
            titleNumberOfLines={1}
            titleStyle={[typography.Title_2, {color: COLORS.dark_800}]}
            descriptionStyle={[
              typography.Note,
              {
                fontFamily: 'Montserrat-Medium',
                color: COLORS.altgreen_300,
                marginTop: 8,
              },
            ]}
            containerStyle={{
              marginTop: 15,
              width: '100%',
              flexDirection: 'column',
              backgroundColor: COLORS.altgreen_100,
            }}
            imageStyle={{width: '100%', height: 150}}
            imageProps={{width: '100%', height: 150}}
          />
        ) : (
          <></>
        )}

        {(sharedEntityType === 'EVENT' ||
          sharedEntityType === 'ASSIGNMENT' ||
          sharedEntityType === 'JOB' ||
          sharedEntityType === 'TRAINING' ||
          sharedEntityType === 'EXPEDITION' ||
          sharedEntityType === 'FUNDRAISE' ||
          sharedEntityType === 'STORYBOOK') && (
          <Image
            style={{height: 150, width: '100%'}}
            resizeMode={'contain'}
            source={
              item.coverImageUrl ? {uri: item.coverImageUrl} : BlogDefault
            }
          />
        )}

        <View style={{width: '103%'}}>
          {item && item.attachmentIds && item.attachmentIds.length ? (
            this.renderImage(item)
          ) : (
            <></>
          )}
          {item.question ? (
            <Text style={[typography.Button_Lead, {color: COLORS.dark_800}]}>
              {item.question}
            </Text>
          ) : (
            <Text></Text>
          )}
          {item.title ? (
            <Text style={[typography.Button_Lead, {color: COLORS.dark_800}]}>
              {item.title}
            </Text>
          ) : (
            <Text></Text>
          )}

          {item.addressDetail && item.addressDetail.country ? (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignSelf: 'flex-start',
                marginTop: 10,
              }}>
              <Icon
                name="Location"
                color={COLORS.altgreen_300}
                size={10}
                style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
              />
              <Text
                style={[
                  {
                    color: COLORS.altgreen_300,
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                    maxWidth: 200,
                  },
                ]}>
                {item.addressDetail.country}
                {/* {+ new Date() - item.createTime} */}
              </Text>
            </View>
          ) : null}

          {item.shortBrief && (
            <Autolink
              text={this.trimDescription(item.shortBrief)}
              email
              hashtag="instagram"
              mention="twitter"
              style={[
                typography.Body_1,
                {
                  color:
                    item.postType === 'ARTICLE'
                      ? COLORS.dark_500
                      : COLORS.dark_700,
                  marginRight: 17,
                  marginVertical: 8,
                },
              ]}
              phone="sms"
              url
            />
          )}

          {item.description && item.postType === 'ARTICLE' ? (
            this.renderDescription(item)
          ) : (
            <Text></Text>
          )}
        </View>

        <View style={{marginLeft: 8}}>
          {item.secondaryAttachmentIds &&
          item.secondaryAttachmentIds.length > 0 ? (
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}
              // onPress={() => this.setState({ attachmentModalopen: true, causeItem: item })}
              onPress={() => {
                this.setState({selectedVideoId: ''});
                item && item.postType === 'ARTICLE'
                  ? this.props.navigate('EditorsDesk', {
                      id: item.id,
                      userId: this.state.userId,
                    })
                  : this.props.navigate('IndividualFeedsPost', {
                      id: item.id,
                      commentCount: item.commentCount,
                    });
              }}>
              <Icon
                name="Clip"
                color={COLORS.green_500}
                size={14}
                style={{
                  marginTop: Platform.OS === 'android' ? 10 : 0,
                  marginRight: 6,
                }}
              />
              <Text style={[{color: COLORS.altgreen_300}, defaultStyle.Note2]}>
                {item.secondaryAttachmentIds.length === 1
                  ? item.secondaryAttachmentIds.length + ' File '
                  : item.secondaryAttachmentIds.length + ' Files '}{' '}
                Attached
              </Text>
            </TouchableOpacity>
          ) : (
            <Text></Text>
          )}
        </View>

        {/* {item &&
        item.hashTags &&
        item.hashTags.length &&
        item.postType !== 'ARTICLE' ? (
          this.renderHashTags(item)
        ) : (
          <></>
        )} */}
      </TouchableOpacity>
    );
  };

  renderHashTags = (item) => {
    return (
      <ScrollView
        contentContainerStyle={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          marginLeft: 6,
          marginTop: 8,
        }}>
        {item &&
          item.hashTags.map((hashTag, index) => (
            <View key={index}>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigate('HashTagDetail', {
                    slug: hashTag,
                  })
                }>
                <Text
                  style={[
                    typography.Subtitle_2,
                    {color: '#367681', marginRight: 8},
                  ]}>
                  #{hashTag}
                </Text>
              </TouchableOpacity>
            </View>
          ))}
      </ScrollView>
    );
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <Report
          changeState={this.changeState}
          entityId={this.state.pressedActivityId}
          entityType="POST"
        />
      </Modal>
    );
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedActivityId,
      // entityType: this.state.entityType,
      entityType: 'POST',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    setTimeout(() => {
      this.setState({reasonForReporting: '', description: ''});
    }, 1000);
  };

  navigateProfile = () => {
    if (this.state.userType === 'INDIVIDUAL') {
      this.props.navigate('ProfileStack');
    } else if (this.state.userType === 'COMPANY') {
      this.props.navigate('ProfileStack', {
        screen: 'CompanyProfileScreen',
      });
    }
  };

  deletePostAlert = () => {
    this.setState({optionsModalOpen: false});
    Alert.alert('', 'Are you sure you want to delete this post?', [
      {
        text: 'YES',
        onPress: () => this.handleDeleteSubmit(),
        style: 'cancel',
      },
      {
        text: 'NO',
        // onPress: () => console.log('user cancelled deletion'),
      },
    ]);
  };

  handleDeleteSubmit = () => {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/backend/post/delete/' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          // console.log(response.status)
        }
      })
      .catch((err) => {
        // console.log(err)
      });
    this.setState({
      optionsModalOpen: false,
      feedsData: this.state.feedsData.filter(
        (item) => item.id !== this.state.pressedActivityId,
      ),
    });
  };

  onRefresh = () => {
    this.setState({pageNumber: 0, feedsData: [], isLoading: true}, () =>
      this.fetchFeedsData(this.state.userId),
    );
  };

  attachmentModal = () => {
    return (
      <Modal
        visible={this.state.attachmentModalopen}
        transparent
        animationType="slide"
        supportedOrientation={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({attachmentModalopen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>
          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
              },
            ]}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Attachments{' '}
              </Text>
            </View>
            <FlatList
              keyboardShouldPersistTaps="handled"
              showsVerticalScrollIndicator={false}
              // contentContainerStyle={{ justifyContent: 'flex-start' }}
              style={{height: '60%', width: '100%', marginTop: 6}}
              keyExtractor={(item) => item.id}
              data={this.state.causeItem?.secondaryAttachmentIds}
              // initialNumToRender={10}
              renderItem={({item, index}) => (
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Icon
                    name="Clip"
                    color={COLORS.green_500}
                    size={14}
                    style={{
                      marginTop: Platform.OS === 'android' ? 10 : 0,
                      marginRight: 6,
                    }}
                  />
                  <TouchableOpacity
                    onPress={() => {
                      this.readStorage(item.attachmentUrl);
                    }}>
                    <Text>
                      {item.attachmentUrl
                        .substring(item.attachmentUrl.lastIndexOf('/') + 1)
                        .substring(
                          item.attachmentUrl
                            .substring(item.attachmentUrl.lastIndexOf('/') + 1)
                            .indexOf('-') + 1,
                        )}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    if (!this.state.isLoading && this.state.feedsData.length === 0) {
      return (
        <SafeAreaView
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: this.props.type === 'PROJECTFEEDS' ? 100 : 0,
          }}>
          {this.props.canPost ? (
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity onPress={() => this.props.postModalOpen()}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.green_500}]}>
                  Share
                </Text>
              </TouchableOpacity>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                {' your thoughts'}
              </Text>
            </View>
          ) : (
            <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
              No Activity to Show
            </Text>
          )}
        </SafeAreaView>
      );
    } else {
      return (
        <SafeAreaView style={{flex: 1}}>
          {this.postModal()}
          {this.optionsModal()}
          {this.shareModal()}
          {this.likeModal()}
          {this.peopleSharedModal()}
          {this.reasonForReportingModal()}
          {this.attachmentModal()}

          <View style={[styles.container, {marginTop: this.props.marginTop}]}>
            {this.state.feedsData.length > 0 && !this.state.isLoading ? (
              <FlatList
                ref={(ref) => {
                  this.flatListRef = ref;
                }}
                refreshControl={
                  <RefreshControl
                    refreshing={false}
                    onRefresh={this.onRefresh}
                  />
                }
                ListHeaderComponent={this.listHeaderComponent()}
                // onScroll={this.handleScroll}
                style={{backgroundColor: COLORS.bgFill_200}}
                showsVerticalScrollIndicator={false}
                alwaysBounceVertical={false}
                keyExtractor={(item) => (item && item.id ? item.id : item)}
                // data={this.props.userFeedsPhotos.body.content.filter((item) => item.description.indexOf('@@@__') > -1)}
                data={
                  this.state.feedsData.length > 0
                    ? this.state.feedsData
                    : [{id: 0}]
                }
                onEndReached={this.handleLoadMore}
                onEndReachedThreshold={1}
                renderItem={({item, index}) => {
                  return (
                    <View style={styles.renderItemStyle}>
                      {this.renderHeader(item)}
                      {item &&
                      item.description &&
                      item.postType !== 'ARTICLE' ? (
                        this.renderDescription(item)
                      ) : (
                        <View
                          style={{
                            backgroundColor: 'transparent',
                            height: 10,
                          }}></View>
                      )}

                      {item && item.postType === 'LINK' ? (
                        <RNUrlPreview
                          text={item.postLinkTypeUrl}
                          titleNumberOfLines={1}
                          titleStyle={[
                            typography.Title_2,
                            {color: COLORS.dark_800},
                          ]}
                          descriptionStyle={[
                            typography.Note,
                            {
                              fontFamily: 'Montserrat-Medium',
                              color: COLORS.altgreen_300,
                              marginTop: 8,
                            },
                          ]}
                          containerStyle={{
                            marginTop: 15,
                            width: '100%',
                            flexDirection: 'column',
                            backgroundColor: COLORS.altgreen_100,
                          }}
                          imageStyle={{width: '100%', height: 150}}
                          imageProps={{width: '100%', height: 150}}
                        />
                      ) : (
                        <></>
                      )}

                      {item && item.sharedEntityId ? (
                        this.renderSharedPost(
                          item.sharedEntityParams,
                          item.sharedEntityType,
                        )
                      ) : item && item.attachmentIds.length ? (
                        this.renderImage(item)
                      ) : !item.attachmentIds.length &&
                        this.state.selected === 'ARTICLES' ? (
                        <Image
                          style={{height: 345, marginLeft: 0}}
                          source={{
                            uri:
                              'https://cdn.dscovr.com/images/banner-explore-blog-small.webp',
                          }}
                        />
                      ) : (
                        <></>
                      )}

                      {item &&
                      item.description &&
                      item.postType === 'ARTICLE' ? (
                        this.renderDescription(item)
                      ) : (
                        <View
                          style={{
                            backgroundColor: 'transparent',
                            height: 10,
                          }}></View>
                      )}

                      <View style={{marginLeft: 8}}>
                        {item.secondaryAttachmentIds &&
                        item.secondaryAttachmentIds.length > 0 ? (
                          <TouchableOpacity
                            style={{flexDirection: 'row', alignItems: 'center'}}
                            // onPress={() => this.setState({ attachmentModalopen: true, causeItem: item })}
                            onPress={() => {
                              this.setState({
                                selectedVideoId: '',
                                attachmentModalopen: true,
                              });
                            }}>
                            <Icon
                              name="Clip"
                              color={COLORS.green_500}
                              size={14}
                              style={{
                                marginTop: Platform.OS === 'android' ? 10 : 0,
                                marginRight: 6,
                              }}
                            />
                            <Text
                              style={[
                                {color: COLORS.altgreen_300},
                                defaultStyle.Note2,
                              ]}>
                              {item.secondaryAttachmentIds.length === 1
                                ? item.secondaryAttachmentIds.length + ' File '
                                : item.secondaryAttachmentIds.length +
                                  ' Files '}{' '}
                              Attached
                            </Text>
                          </TouchableOpacity>
                        ) : (
                          <Text></Text>
                        )}
                      </View>

                      {item && item.hashTags.length ? (
                        this.renderHashTags(item)
                      ) : (
                        <></>
                      )}

                      {this.renderFooter(item, index)}
                    </View>
                  );
                }}
              />
            ) : this.state.isLoading ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {this.props.type !== 'PROJECTFEEDS' &&
                  this.listHeaderComponent()}
                <View
                  style={{marginTop: 0, backgroundColor: COLORS.bgFill_200}}>
                  {skeletonCount.map((item, index) => (
                    <ScrollView
                      style={{
                        alignSelf: 'flex-start',
                        marginLeft: 20,
                        paddingTop: index === 0 ? 30 : 0,
                      }}>
                      <SkeletonPlaceholder backgroundColor={COLORS.grey_300}>
                        <View style={{flexDirection: 'row', width: 1000}}>
                          <View
                            style={{width: 32, height: 32, borderRadius: 16}}
                          />
                          <View style={{marginLeft: 10}}>
                            <View
                              style={{width: 800, height: 20, borderRadius: 4}}
                            />
                            <View
                              style={{
                                marginTop: 6,
                                width: 800,
                                height: 80,
                                borderRadius: 4,
                              }}
                            />
                            {/* <View style={{ width: 300, height: 20, borderRadius: 4 }} /> */}
                          </View>
                        </View>
                      </SkeletonPlaceholder>
                    </ScrollView>
                  ))}
                </View>

                {/* <ActivityIndicator size="large" color="white" style={{ position: 'absolute', top: '55%' }} /> */}
              </View>
            ) : (
              // <Text>No Data</Text>
              <></>
            )}

            {this.state.feedsData.length > 0 && !this.state.isLoading && (
              <TouchableOpacity
                onPress={this.goToTop}
                activeOpacity={0.7}
                style={styles.floatingIcon2}>
                <Icon
                  name="Arrow-Up"
                  size={18}
                  color="#00394D"
                  style={styles.editIcon2}
                />
              </TouchableOpacity>
            )}

            {this.props.type !== 'PROJECTFEEDS' && (
              <TouchableHighlight
                activeOpacity={0.2}
                underlayColor="#rgb(216 222 33 / 1%)"
                onPress={() => this.setState({postModalOpen: true})}
                style={styles.floatingIcon}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Icon
                    name="AddPost"
                    size={18}
                    color={COLORS.primarydark}
                    style={styles.editIcon}
                  />
                  <Text style={styles.floatingIconText}>POST</Text>
                </View>
              </TouchableHighlight>
            )}
          </View>
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#00394D',
    flex: 1,
  },
  renderSharedPost: {
    width: '94%',
    marginTop: 5,
    marginLeft: 6,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: COLORS.grey_300,
    padding: 10,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  renderItemStyle: {
    backgroundColor: COLORS.white,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginHorizontal: 10,
    marginVertical: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: COLORS.grey_300,
  },
  unreadNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderRadius: 8,
    // borderBottomColor: COLORS.grey_400,
    // borderBottomWidth: 0.3,
    // height: 44
  },
  scrollViewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editIcon: {
    marginTop: Platform.OS === 'android' ? 12 : 0,
    marginRight: 8,
  },
  editIcon2: {
    marginTop: Platform.OS === 'android' ? 8 : 0,
    // marginRight: 8
  },
  floatingIconText: {
    color: '#00394D',
    fontSize: 18,
    fontWeight: 'bold',
    // marginTop: 2,
  },
  floatingIcon: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 113,
    height: 44,
    borderRadius: 40,
    backgroundColor: '#D8DE21',
    position: 'absolute',
    bottom: 16,
    right: 12,
  },
  floatingIcon2: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 28,
    height: 42,
    borderRadius: 40,
    backgroundColor: '#1A4D5F80',
    position: 'absolute',
    bottom: 66,
    right: 12,
  },
  feedDetails: {
    marginTop: 20,
    marginLeft: 16,
    paddingBottom: 16,
  },
  selected: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#367681',
    marginRight: 15,
    height: 27,
    // width: 66,
    borderRadius: 16,
    textAlign: 'center',
  },
  notSelected: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    height: 27,
    // width: 66,
    borderWidth: 1,
    borderColor: '#698F8A',
    borderRadius: 16,
  },
  selectedText: {
    color: '#F7F7F5',
    fontSize: 12,
    paddingHorizontal: 14,
    fontFamily: 'Montserrat-Medium',
  },
  notSelectedText: {
    color: '#698F8A',
    fontSize: 12,
    paddingHorizontal: 14,
    fontFamily: 'Montserrat-Medium',
  },
  searchBar: {
    // marginTop: 8
  },
  explore: {
    flexDirection: 'row',
    paddingTop: 8,
    marginLeft: 15,
  },
  feed: {
    flexDirection: 'row',
    paddingTop: 8,
    marginLeft: 20,
    borderBottomColor: '#BFC52E',
    borderBottomWidth: 4,
    width: 100,
  },
  header: {
    flexDirection: 'row',
    borderBottomColor: '#154A59',
    borderBottomWidth: 1,
    paddingTop: 10,
    height: 60,
  },
  feedText: {
    color: '#FFFFFF',
    fontSize: 22,
    marginLeft: 5,
    fontFamily: 'Montserrat-SemiBold',
  },
  exploreText: {
    color: '#91B3A2',
    fontSize: 22,
    marginLeft: 5,
    fontFamily: 'Montserrat-Regular',
  },
});

const mapStateToProps = (state) => {
  return {
    userFeedsPhotosProgress: state.feedsReducer.userFeedsPhotosProgress,
    userFeedsPhotos: state.feedsReducer.userFeedsPhotos,
    errorFeedsPhotos: state.feedsReducer.errorFeedsPhotos,

    userFeedsVideosProgress: state.feedsReducer.userFeedsVideosProgress,
    userFeedsVideos: state.feedsReducer.userFeedsVideos,
    errorFeedsVideos: state.feedsReducer.errorFeedsVideos,

    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    feedsPhotosRequest: (data) => dispatch(feedsPhotosRequest(data)),
    feedsVideosRequest: (data) => dispatch(feedsVideosRequest(data)),
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CommonFeeds);
