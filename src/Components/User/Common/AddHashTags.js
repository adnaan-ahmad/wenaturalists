import React, { Component } from 'react'
import { ActivityIndicator, ImageBackground, ScrollView, TextInput, Dimensions, FlatList, View, Text, TouchableOpacity, Image, StyleSheet, Platform, SafeAreaView } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'
import axios from 'axios'
import Snackbar from 'react-native-snackbar'
import AsyncStorage from '@react-native-community/async-storage'

import defaultShape from '../../../Components/Shared/Shape'
import { COLORS } from '../../../Components/Shared/Colors'
import typography from '../../../Components/Shared/Typography'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultCover from '../../../../assets/defaultCover.png'
import { REACT_APP_userServiceURL } from '../../../../env.json'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

const screenHeight = Dimensions.get('window').height

export default class AddHashTags extends Component {

    constructor(props) {
        super(props)

        this.state = {
            users: [],
            selectedPage: '',
            name: '',
            searchText: '',
            searchIcon: true,
            userId: '',
            suggestions: []
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value })


        }).catch((e) => {
            console.log(e)
        })

    }

    searchNames = (query) => {
        if (query === '') {
            return this.props.connectsData
        }

        const regex = new RegExp(`${query.trim()}`, 'i')

        return this.props.connectsData.filter(item => (item.firstName + ' ' + item.lastName).search(regex) >= 0)
    }

    onSuggestionsFetchRequested = (value) => {
        if(value.charAt(0) === '#'){
            value = value.substring(1)
        }
        let postBody = {
            "description": value
        }
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/tags/recommended',
            headers: {'Content-Type': 'application/json'},
            data: postBody,
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.message === 'Success!') {
                console.log(response.data.body)
                this.setState({ suggestions: response.data.body 
                    // && response.data.body.map((tag) => ({
                    //     id: uuid(),
                    //     name: '#' + tag
                    // }))
                })
            }
        }).catch((err) => {
            console.log(err)
        })

    }

    render() {
        // console.log('------------------------------ this.props.hashTags -------------------------------', this.state.suggestions)
        return (
            <SafeAreaView style={{ backgroundColor: '#F7F7F5', height: 612, position: 'absolute', bottom: -50, width: '100%' }}>

                <View style={styles.linearGradientView2}>
                    <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={styles.linearGradient2}>
                    </LinearGradient>
                </View>

                <TouchableOpacity activeOpacity={0.7} style={styles.crossButtonContainer}
                    onPress={() => this.props.changeHashTagsState({ addHashTagsModalOpen: false, createForumModalOpen: this.props.currentOpen === 'FORUM' && true, createPollModalOpen: this.props.currentOpen === 'POLL' && true, seconds: 59 })} >
                    <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon2} />
                </TouchableOpacity>


                <View style={{ backgroundColor: '#E7F3E3', paddingTop: 10, paddingBottom: 6, borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
                    <Text style={styles.requestEndorsementText}>ADD HASHTAGS</Text>
                    {this.props.hashTags.length === 1 ? <Text style={styles.connectsSelectedText}>1 Hashtag Added</Text> :
                        <Text style={styles.connectsSelectedText}>{this.props.hashTags.length} Hashtags Added</Text>}



                    {this.props.hashTags.length < 5 ? <View style={{
                        flexDirection: 'row', paddingLeft: 10, paddingRight: 6
                    }}
                        keyboardShouldPersistTaps='handled' horizontal={true}

                    >
                        {this.props.hashTags.map((item, index) => (
                            <TouchableOpacity
                          onPress={() =>
                            this.props.changeHashTagsState({
                              hashTags: this.props.hashTags.filter(
                                (value, index2) =>
                                  value + index2 !== item + index,
                              ), seconds: 59
                            })
                          }
                          activeOpacity={0.6}
                          style={{
                            flexDirection: 'row',
                            backgroundColor: COLORS.altgreen_t50 + '80',
                            paddingHorizontal: 10,
                            marginVertical: 6,
                            height: 28,
                            borderRadius: 17,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginRight: 6,
                          }}>
                          <Text
                            style={[
                              typography.Caption,
                              {
                                color: COLORS.dark_500,
                                marginRight: 4,
                                marginTop: Platform.OS === 'ios' ? -2 : 0,
                              },
                            ]}>
                            {item}
                          </Text>
                          <Icon
                            name="Cross_Rounded"
                            color={COLORS.dark_500}
                            size={15}
                            style={{
                              marginTop: Platform.OS === 'android' ? 10 : 0,
                            }}
                          />
                        </TouchableOpacity>))}
                    </View>

                        :

                        <FlatList
                            keyboardShouldPersistTaps='handled'
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{ paddingLeft: 10, paddingRight: 6 }}
                            keyExtractor={(item) => item.id}
                            data={this.props.hashTags}
                            ref={ref => this.scrollView = ref}
                            onContentSizeChange={() => {
                                this.scrollView.scrollToEnd({ animated: true })
                            }}
                            renderItem={({ item, index }) => (

                                <TouchableOpacity
                          onPress={() =>
                            this.props.changeHashTagsState({
                              hashTags: this.props.hashTags.filter(
                                (value, index2) =>
                                  value + index2 !== item + index,
                              ), seconds: 59
                            })
                          }
                          activeOpacity={0.6}
                          style={{
                            flexDirection: 'row',
                            backgroundColor: COLORS.altgreen_t50 + '80',
                            paddingHorizontal: 10,
                            marginVertical: 6,
                            height: 28,
                            borderRadius: 17,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginRight: 6,
                          }}>
                          <Text
                            style={[
                              typography.Caption,
                              {
                                color: COLORS.dark_500,
                                marginRight: 4,
                                marginTop: Platform.OS === 'ios' ? -2 : 0,
                              },
                            ]}>
                            {item}
                          </Text>
                          <Icon
                            name="Cross_Rounded"
                            color={COLORS.dark_500}
                            size={15}
                            style={{
                              marginTop: Platform.OS === 'android' ? 10 : 0,
                            }}
                          />
                        </TouchableOpacity>


                            )}
                        />}


                </View>



                <View style={{
                    height: 60, backgroundColor: '#F7F7F5', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: -1
                }}>

                    <TextInput
                        style={styles.textInput}
                        placeholderTextColor="#D9E1E4"
                        onChangeText={(value) => { 
                            value[value.length - 1] !== ' ' ? this.setState({ searchText: value }, () => this.onSuggestionsFetchRequested(value)) 
                            : value[value.length - 1] === ' ' && value.trim() ? (this.props.changeHashTagsState({ hashTags: [...this.props.hashTags, value.trim()] }, () => this.setState({ searchText: '' }))) : null
                            }}
                        color='#154A59'
                        placeholder='Search'
                        onFocus={() => this.setState({ searchIcon: false })}
                        onBlur={() => this.setState({ searchIcon: true })}
                        underlineColorAndroid="transparent"
                        ref={input => { this.textInput = input }}
                    />


                </View>


                <FlatList
                    keyboardShouldPersistTaps='handled'
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 80, paddingLeft: 10 }}
                    style={{ height: '50%' }}
                    keyExtractor={(item) => item.id}
                    data={this.state.suggestions}
                    initialNumToRender={10}
                    renderItem={({ item }) => (

                        <TouchableOpacity activeOpacity={0.96} onPress={() => {

                                this.props.changeHashTagsState({ hashTags: [...this.props.hashTags, item], seconds: 59 })

                        }} >
                            <View style={styles.item}>

                                <View style={styles.nameMsg}>
                                    <Text style={styles.name}>
                                        # {item}
                                    </Text>

                                </View>
                                <Text style={styles.time}></Text>

                            </View>

                        </TouchableOpacity>


                    )}
                />

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    // --- Hubroot ---

    container: {
        flexDirection: 'column',
        textAlign: 'left',
        fontSize: 15,
        position: "absolute",
        top: 30,
        height: screenHeight,
        backgroundColor: '#fff',
        //width: '100%'
    },
    requestEndorsementText: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 14,
        color: '#154A59',
        textAlign: 'center'
    },
    connectsSelectedText: {
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        color: '#698F8A',
        textAlign: 'center',
        marginVertical: 10
    },
    crossIcon2: {
        marginTop: Platform.OS === 'android' ? 10 : 0
    },
    crossButtonContainer: {
        alignSelf: 'center',
        width: 42,
        height: 42,
        borderRadius: 21,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E7F3E3',
        marginBottom: 10
    },
    linearGradientView2: {
        width: '100%',
        height: 200,
        position: 'absolute',
        top: -50,
        alignSelf: 'center'
    },
    linearGradient2: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6
    },
    sendRequest: {
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 15,
        color: '#E7F3E3',
        marginLeft: 6
    },
    floatingIcon: {
        //flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: 175,
        height: 39,
        borderRadius: 28,
        backgroundColor: '#367681',
        position: 'absolute',
        //top: 416,
        bottom: 66,
        right: 12,
    },
    editIcon: {
        alignSelf: 'center'
    },
    crossIcon: {
        position: 'absolute',
        right: -10,
        bottom: -16,
        alignSelf: 'flex-end'
    },
    selectedName: {
        fontSize: 11,
        color: '#154A59',
        textAlign: 'center',
        // marginBottom: 6,
        fontFamily: 'Montserrat-Medium'
    },
    searchIcon: {
        position: 'absolute',
        left: 136,
        top: 15,
        // backgroundColor: 'red',
        zIndex: 2
    },
    border: {
        borderWidth: 1,
        marginRight: '6%',
        width: '95%',
        marginTop: '-1%',
        borderColor: '#91B3A2',
        alignSelf: 'center',
        borderRadius: 1,
    },
    textInput: {
        fontSize: 17,
        fontFamily: 'Montserrat-Medium',
        padding: 10,
        backgroundColor: '#FFF',
        width: '90%',
        zIndex: 1,
        textAlign: 'center',
        borderRadius: 8,
        borderColor: '#D9E1E4',
        borderWidth: 1,
        shadowColor: '#36768140',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    tabNavigator: {
        marginTop: '-8%',
    },


    // --- Assignment ---

    item: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'space-around',
        backgroundColor: '#F7F7F500',
        width: '100%',
        height: 48,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',

        // borderBottomWidth: 1,
        //borderBottomRadius: 1,
        // borderBottomColor: '#E2E7E9',
        // borderRadius: 10
    },
    image: {
        height: 30,
        width: 30,
        borderRadius: 15,
        //marginLeft: '7%',
    },
    name: {
        fontWeight: '700',
        color: '#4B4F56',
        fontSize: 14,
        textAlign: 'left',
        // marginLeft: '-6%',
    },
    message: {
        color: '#698F8A',
        fontSize: 10.5,
    },

    // --- New ---

    time: {
        color: '#AABCC3',
        fontSize: 10,
        marginRight: '10%'
    },
    nameMsg: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: '6%',
        //textAlign: 'left'
    },
    imageGroup: {
        height: 26,
        width: 26,
        borderRadius: 13,
    },
})
