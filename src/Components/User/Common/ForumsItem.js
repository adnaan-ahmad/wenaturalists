import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import Autolink from 'react-native-autolink';
import AsyncStorage from '@react-native-community/async-storage';
import TimeAgo from 'react-native-timeago';

import {COLORS} from '../../../Components/Shared/Colors';
import BlogDefault from '../../../../assets/BlogDefault.jpg';
import typography from '../../../Components/Shared/Typography';
import defaultProfile from '../../../../assets/defaultProfile.png';
import circleDefault from '../../../../assets/CirclesDefault.png';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';
import ConnectDepth from '../../../Components/User/Common/ConnectDepth';
import Follow from '../../../Components/User/Common/Follow';
import {unixTime2} from '../../Shared/commonFunction';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
export default class ForumsItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {},
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userData')
      .then((value) => {
        let userData = JSON.parse(value);
        value && this.setState({userData: userData});
      })
      .catch((e) => {
        console.log(e);
      });
  }

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + ' s';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + ' m';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + ' h';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + ' d';
    }

    if (difference >= 864000) {
      return day + ' ' + month + ' ' + year;
    }
  };

  renderHeader = (item, index) => {
    return (
      <View style={styles.unreadNotiItem}>
        <View
          style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              item.userType === 'COMPANY'
                ? this.props.navigation('ProfileStack', {
                    screen: 'CompanyProfileScreen',
                    params: {userId: item.userId},
                  })
                : item.userType === 'INDIVIDUAL' &&
                  item.userId !== this.state.userId
                ? this.props.navigation('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: {userId: item.userId},
                  })
                : item.userType === 'INDIVIDUAL' &&
                  item.userId === this.state.userId
                ? this.props.navigation('ProfileStack', {
                    screen: 'ProfileScreen',
                    // params: {userId: item.userId},
                  })
                : null;
            }}>
            <Image
              style={[defaultShape.Media_Round, {}]}
              source={
                item?.originalProfileImage
                  ? {uri: item.originalProfileImage}
                  : item.userType && item.userType === 'COMPANY'
                  ? defaultBusiness
                  : defaultProfile
              }
            />
          </TouchableOpacity>

          <View
            style={{
              alignItems: 'center',
              justifyContent: 'flex-start',
              marginLeft: 6,
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                // justifyContent: 'flex-start',
              }}>
              <Text
                numberOfLines={1}
                onPress={() => {
                  item.userType === 'COMPANY'
                    ? this.props.navigation('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                        params: {userId: item.userId},
                      })
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId !== this.state.userId
                    ? this.props.navigation('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.userId},
                      })
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId === this.state.userId
                    ? this.props.navigation('ProfileStack', {
                        screen: 'ProfileScreen',
                        // params: {userId: item.userId},
                      })
                    : null;
                }}
                style={[
                  typography.Title_2,
                  {
                    color: COLORS.dark_800,
                    maxWidth: 160,
                  },
                ]}>
                {item &&
                  item.userName &&
                  item.userName.split(' ').splice(0, 2).join(' ')}
              </Text>

              {/* <Follow
                item={item}
                followed={item.followed}
                userId={item.userId}
                index={index}
              /> */}

              <TouchableOpacity
                onPress={() =>
                  this.props.handleFollowUnfollow(item.followed, item.userId)
                }
                style={{
                  height: 30,
                  width: 30,
                  alignItems: 'center',
                  marginLeft: 4,
                }}>
                {this.state.userData.userId !== item.userId && (
                  <Text
                    style={[
                      {
                        color:
                          item.connectDepth > 0 && !item.followed
                            ? '#97a600'
                            : '#888',
                        fontSize: 14,
                        marginLeft: 4,
                      },
                      typography.Note2,
                    ]}>
                    {item.userConnectStatus &&
                    item.userConnectStatus.connectStatus ===
                      'PENDING_CONNECT' ? (
                      'Pending'
                    ) : item.connectDepth === 1 ? (
                      '• 1st'
                    ) : item.connectDepth === 2 ? (
                      '• 2nd'
                    ) : (
                      <Icon
                        name={item.followed ? 'TickRSS' : 'RSS'}
                        size={13}
                        color={
                          item.followed ? COLORS.dark_600 : COLORS.green_500
                        }
                        style={{marginTop: Platform.OS === 'android' ? 4 : 2}}
                      />
                    )}
                  </Text>
                )}
              </TouchableOpacity>
            </View>

            <View style={{flexDirection: 'row'}}>
              {item && item.country ? (
                <Icon
                  name="Location"
                  color={COLORS.altgreen_300}
                  size={10}
                  style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                />
              ) : (
                <></>
              )}
              <Text
                numberOfLines={1}
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigation([
                        'EditorsDesk',
                        {
                          id: item.id,
                          userId: this.state.userId,
                        },
                      ])
                    : this.props.navigation([
                        'IndividualFeedsPost',
                        {
                          id: item.id,
                          commentCount: item.commentCount,
                        },
                      ])
                }
                style={[
                  {
                    color: COLORS.altgreen_300,
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                    maxWidth: 100,
                  },
                ]}>
                {item && item.country ? item.country : null}
                {/* {+ new Date() - item.createTime} */}
              </Text>
              {item && item.country ? (
                <Icon
                  name="Bullet_Fill"
                  color={COLORS.altgreen_300}
                  size={8}
                  style={{marginTop: Platform.OS === 'android' ? 1 : 2}}
                />
              ) : (
                <></>
              )}

              <Text
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigation([
                        'EditorsDesk',
                        {
                          id: item.id,
                          userId: this.state.userId,
                        },
                      ])
                    : this.props.navigation([
                        'IndividualFeedsPost',
                        {
                          id: item.id,
                          commentCount: item.commentCount,
                        },
                      ])
                }
                style={[
                  {
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                  },
                ]}>
                <TimeAgo time={item.createTime} />
                {/* {+ new Date() - item.createTime} */}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  trimDescription = (item) => {
    var trimmed = item.split('^^__').join(' ').indexOf('@@@__');

    var str = item
      .split('^^__')
      .join(' ')
      .indexOf(' ', item.split('^^__').join(' ').indexOf('@@@__'));

    var sub = item.substring(trimmed, str);

    item = item.replace(' ' + sub, '');
    item = item.replace('@@@^^^', ' ');
    item = item.replace('@@@__', ' ');
    item = item.replace('  ', '');

    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  renderDescription = (item) => {
    return (
      <Autolink
        text={this.trimDescription(item.comment)}
        email
        hashtag="instagram"
        mention="twitter"
        phone="sms"
        numberOfLines={3}
        style={[
          typography.Body_1,
          {
            color:
              item.postType === 'ARTICLE' ? COLORS.dark_500 : COLORS.dark_700,
            marginRight: 17,
            marginVertical: 8,
          },
        ]}
        url
      />
    );
  };

  render() {
    const {item, index, detail} = this.props;
    return (
      <View
        style={detail ? styles.renderDeailItemStyle : styles.renderItemStyle}>
        {this.renderHeader(item.item, item.index)}
        {item.item.title ? (
          <Text
            style={[typography.Button_Lead, {color: COLORS.dark_800}]}
            onPress={() =>
              // console.log(item.item)
              this.props.navigation('ForumStack', {
                screen: 'ForumDetails',
                params: {
                  userId: this.state.userData.userId,
                  slug: item.item.slug,
                },
              })
            }>
            {item.item.title}
          </Text>
        ) : (
          <Text></Text>
        )}
        {item.item.comment ? (
          this.renderDescription(item.item)
        ) : (
          <View style={{backgroundColor: 'transparent', height: 10}}></View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  renderItemStyle: {
    backgroundColor: COLORS.white,
    marginHorizontal: 10,
    marginVertical: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: COLORS.grey_300,
    width: 300,
    padding: 8,
  },
  renderDeailItemStyle: {
    backgroundColor: COLORS.white,
    marginHorizontal: 10,
    marginVertical: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: COLORS.grey_300,
    width: '100%',
    padding: 8,
  },
  unreadNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderRadius: 8,
    height: 60,
  },
});
