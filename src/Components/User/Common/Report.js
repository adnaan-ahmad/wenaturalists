import React, {Component} from 'react';
import {View, Text, Platform, TouchableOpacity} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import {TextInput} from 'react-native-paper';

import {REACT_APP_userServiceURL} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultShape from '../../../Components/Shared/Shape';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import defaultStyle from '../../../Components/Shared/Typography';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class Report extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reasonForReporting: '',
      description: '',
      userId: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      this.setState({userId: value});
    });
  }

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.props.entityId,
      entityType: this.props.entityType,
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    setTimeout(() => {
      this.setState({reasonForReporting: '', description: ''});
    }, 1000);
  };

  render() {
    return (
      <View style={{marginTop: 'auto'}}>
        <View
          style={[defaultShape.Linear_Gradient_View, {height: 700, bottom: 0}]}>
          <LinearGradient
            colors={[
              COLORS.dark_800 + '00',
              COLORS.dark_800 + 'CC',
              COLORS.dark_800,
            ]}
            style={defaultShape.Linear_Gradient}></LinearGradient>
        </View>

        <TouchableOpacity
          activeOpacity={0.7}
          style={[defaultShape.CloseBtn, {marginBottom: 0}]}
          onPress={() => {
            this.props.changeState({reasonForReportingModalOpen: false}),
              this.setState({reasonForReporting: ''});
          }}>
          <Icon
            name="Cross"
            size={13}
            color={COLORS.dark_600}
            style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
          />
        </TouchableOpacity>

        <View
          style={{
            borderRadius: 20,
            backgroundColor: COLORS.white,
            paddingTop: 15,
            paddingBottom: 10,
            paddingHorizontal:10,
            width: '100%',
            alignSelf: 'center',
            marginTop: 15,
          }}>
          <View
            style={[
              defaultShape.ActList_Cell_Gylph_Alt,
              {
                borderBottomWidth: 0,
                justifyContent: 'center',
                paddingBottom: 10,
                paddingTop: 0,
              },
            ]}>
            <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
              Reason for reporting
            </Text>
          </View>

          <TouchableOpacity
            style={
              Platform.OS === 'ios'
                ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, paddingVertical: 10},
                  ]
                : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, width:'100%'},
                  ]
            }
            activeOpacity={0.6}
            onPress={() => {
              this.setState({
                reasonForReporting:
                  'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT',
              });
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 17,
                  height: 17,
                  borderRadius: 8.5,
                  borderColor: COLORS.altgreen_300,
                  borderWidth: 2,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLORS.altgreen_200,
                }}>
                {this.state.reasonForReporting ===
                'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT' ? (
                  <Icon
                    name="Bullet_Fill"
                    size={17}
                    color={COLORS.dark_800}
                    style={{
                      marginLeft: -2,
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                    }}
                  />
                ) : (
                  <></>
                )}
              </View>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8},
                ]}>
                Inappropriate, abusive or offensive content
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={
              Platform.OS === 'ios'
                ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, paddingVertical: 10},
                  ]
                : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, width: '100%'},
                  ]
            }
            activeOpacity={0.6}
            onPress={() => {
              this.setState({
                reasonForReporting: 'CONTENT_PROMOTING_VIOLENCE_OR_TERRORISM',
              });
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 17,
                  height: 17,
                  borderRadius: 8.5,
                  borderColor: COLORS.altgreen_300,
                  borderWidth: 2,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLORS.altgreen_200,
                }}>
                {this.state.reasonForReporting ===
                'CONTENT_PROMOTING_VIOLENCE_OR_TERRORISM' ? (
                  <Icon
                    name="Bullet_Fill"
                    size={17}
                    color={COLORS.dark_800}
                    style={{
                      marginLeft: -2,
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                    }}
                  />
                ) : (
                  <></>
                )}
              </View>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8},
                ]}>
                Content promoting violence or terrorism
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={
              Platform.OS === 'ios'
                ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {
                      borderBottomWidth: 0,
                      paddingVertical: 10,
                    },
                  ]
                : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, width: '100%'},
                  ]
            }
            activeOpacity={0.6}
            onPress={() => {
              this.setState({reasonForReporting: 'FAKE_SPAM_OR_SCAM'});
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 17,
                  height: 17,
                  borderRadius: 8.5,
                  borderColor: COLORS.altgreen_300,
                  borderWidth: 2,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLORS.altgreen_200,
                }}>
                {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? (
                  <Icon
                    name="Bullet_Fill"
                    size={17}
                    color={COLORS.dark_800}
                    style={{
                      marginLeft: -2,
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                    }}
                  />
                ) : (
                  <></>
                )}
              </View>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8},
                ]}>
                Fake, spam or scam
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={
              Platform.OS === 'ios'
                ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, paddingVertical: 10},
                  ]
                : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, width: '100%'},
                  ]
            }
            activeOpacity={0.6}
            onPress={() => {
              this.setState({reasonForReporting: 'ACCOUNT_MAY_BE_HACKED'});
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 17,
                  height: 17,
                  borderRadius: 8.5,
                  borderColor: COLORS.altgreen_300,
                  borderWidth: 2,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLORS.altgreen_200,
                }}>
                {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? (
                  <Icon
                    name="Bullet_Fill"
                    size={17}
                    color={COLORS.dark_800}
                    style={{
                      marginLeft: -2,
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                    }}
                  />
                ) : (
                  <></>
                )}
              </View>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8},
                ]}>
                Account may be hacked
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={
              Platform.OS === 'ios'
                ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, paddingVertical: 10},
                  ]
                : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, width: '100%'},
                  ]
            }
            activeOpacity={0.6}
            onPress={() => {
              this.setState({
                reasonForReporting:
                  'DEFAMATION_TRADEMARK_OR_COPYRIGHT_VIOLATION',
              });
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 17,
                  height: 17,
                  borderRadius: 8.5,
                  borderColor: COLORS.altgreen_300,
                  borderWidth: 2,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLORS.altgreen_200,
                }}>
                {this.state.reasonForReporting ===
                'DEFAMATION_TRADEMARK_OR_COPYRIGHT_VIOLATION' ? (
                  <Icon
                    name="Bullet_Fill"
                    size={17}
                    color={COLORS.dark_800}
                    style={{
                      marginLeft: -2,
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                    }}
                  />
                ) : (
                  <></>
                )}
              </View>
              <Text
                numberOfLines={2}
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8, flexWrap: 'wrap'},
                ]}>
                Defamation, trademark or copyright
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={
              Platform.OS === 'ios'
                ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, paddingVertical: 10},
                  ]
                : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, width: '100%'},
                  ]
            }
            activeOpacity={0.6}
            onPress={() => {
              this.setState({reasonForReporting: 'HARASSMENT_OR_THREAT'});
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 17,
                  height: 17,
                  borderRadius: 8.5,
                  borderColor: COLORS.altgreen_300,
                  borderWidth: 2,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLORS.altgreen_200,
                }}>
                {this.state.reasonForReporting === 'HARASSMENT_OR_THREAT' ? (
                  <Icon
                    name="Bullet_Fill"
                    size={17}
                    color={COLORS.dark_800}
                    style={{
                      marginLeft: -2,
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                    }}
                  />
                ) : (
                  <></>
                )}
              </View>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8},
                ]}>
                Harassment or threat
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={
              Platform.OS === 'ios'
                ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, paddingVertical: 10},
                  ]
                : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    {borderBottomWidth: 0, width: '100%'},
                  ]
            }
            activeOpacity={0.6}
            onPress={() => {
              this.setState({reasonForReporting: 'OTHERS'});
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 17,
                  height: 17,
                  borderRadius: 8.5,
                  borderColor: COLORS.altgreen_300,
                  borderWidth: 2,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLORS.altgreen_200,
                }}>
                {this.state.reasonForReporting === 'OTHERS' ? (
                  <Icon
                    name="Bullet_Fill"
                    size={17}
                    color={COLORS.dark_800}
                    style={{
                      marginLeft: -2,
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                    }}
                  />
                ) : (
                  <></>
                )}
              </View>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8},
                ]}>
                Others
              </Text>
            </View>
          </TouchableOpacity>

          {this.state.reasonForReporting === 'OTHERS' && (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingLeft: 6,
                paddingRight: 12,
              }}>
              <TextInput
                theme={{
                  colors: {
                    text: COLORS.dark_700,
                    primary: COLORS.altgreen_300,
                    placeholder: COLORS.altgreen_300,
                  },
                }}
                label="Write the details"
                multiline
                selectionColor="#C8DB6E"
                style={[
                  defaultStyle.Subtitle_1,
                  {
                    width: '90%',
                    height: 56,
                    backgroundColor: COLORS.white,
                    color: COLORS.dark_700,
                  },
                ]}
                onChangeText={(value) => this.setState({description: value})}
                value={this.state.description}
              />
            </View>
          )}

          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => {
              this.state.reasonForReporting === ''
                ? Snackbar.show({
                    backgroundColor: '#B22222',
                    text: 'Please select an option',
                    duration: Snackbar.LENGTH_LONG,
                  })
                : this.state.reasonForReporting === 'OTHERS' &&
                  this.state.description.trim() === ''
                ? Snackbar.show({
                    backgroundColor: '#B22222',
                    text: 'Please enter the detail',
                    duration: Snackbar.LENGTH_LONG,
                  })
                : (this.handleReportAbuseSubmit(),
                  this.props.changeState({reasonForReportingModalOpen: false}));
            }}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginRight: 15,
              height: 27,
              width:'80%',
              alignSelf:'center',
              marginVertical: 10,
              borderRadius: 16,
              textAlign: 'center',
              borderWidth: 1,
              borderColor: '#698F8A',
            }}>
            <Text
              style={{
                color: '#698F8A',
                fontSize: 14,
                paddingHorizontal: 14,
                paddingVertical: 20,
                fontFamily: 'Montserrat-Medium',
                fontWeight: 'bold',
                marginBottom: Platform.OS === 'ios' ? 15 : 0,
              }}>
              Submit
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
