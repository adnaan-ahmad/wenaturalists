import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

import Snackbar from 'react-native-snackbar';
import { cloneDeep } from 'lodash';
import { REACT_APP_userServiceURL } from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultProfile from '../../../../assets/defaultProfile.png';
import DefaultBusiness from '../../../../assets/DefaultBusiness.png';
import { COLORS } from '../../Shared/Colors';
import typography from '../../Shared/Typography';
import ConnectDepth from './ConnectDepth';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class LikedUserList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      page: 0,
      peopleLiked: [],
      isCompany: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        value && this.setState({ userId: value }, () => this.getUsersWhoLiked());
      })
      .catch((e) => {
        console.log(e);
      });

    AsyncStorage.getItem('userData')
      .then((value) => {
        let objValue = JSON.parse(value);
        objValue.type === 'COMPANY' && this.setState({ isCompany: true });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  getUsersWhoLiked = () => {
    console.log("object new", this.props.id)
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/like/getUsers/' +
        this.props.id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        this.state.page +
        '&size=10',
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          this.setState({
            peopleLiked: this.state.peopleLiked.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleFollowUnfollow = (isFollowed, userId, index, item) => {
    let tempLikeList = cloneDeep(this.state.peopleLiked);
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        console.log(response.status);
        if (response && response.status === 202) {
          tempLikeList[index].followed = !item.followed;
          this.setState({ peopleLiked: tempLikeList });
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleLoadMore = () => {
    this.setState({ page: this.state.page + 1 }, () => this.getUsersWhoLiked());
  };

  handleConnectStatusChange = (userId) => {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/invite/' +
        userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          // console.log(response.status)
          this.setState({ page: 0, peopleLiked: [] }, () =>
            this.getUsersWhoLiked(),
          );
        }
      })
      .catch((err) => {
        // console.log(err)
        if (err.message === 'Request failed with status code 409') {
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'You can send connection request after 3 days to this member',
            duration: Snackbar.LENGTH_LONG,
          });
        }
        if (err.message === 'Request failed with status code 400') {
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Cannot send request to an organization',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
  };

  render() {
    return (
      <View style={{ width: '100%' }}>
        <FlatList
          keyboardShouldPersistTaps="handled"
          showsVerticalScrollIndicator={false}
          style={{ height: '60%', width: '100%', marginTop: 6 }}
          // contentContainerStyle={{ alignSelf: 'flex-start' }}
          keyExtractor={(item) => item.userId}
          data={this.state.peopleLiked}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={2}
          renderItem={({ item, index }) => (
            <View
              style={{
                alignItems: 'flex-start',
                alignSelf: 'flex-start',
                width: '96%',
                marginVertical: 6,
              }}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({ likeModalOpen: false }, () => {
                    this.state.userId === item.userId &&
                      item.userType === 'COMPANY'
                      ? this.props.navigation('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                        // params: { userId: pollData.userId },
                      })
                      : item.userType === 'COMPANY'
                        ? this.props.navigation('ProfileStack', {
                          screen: 'CompanyProfileScreen',
                          params: { userId: item.userId },
                        })
                        : this.state.userId === item.userConnectStatus.profileId
                          ? this.props.navigation('ProfileStack', {
                            screen: 'ProfileScreen',
                          })
                          : this.props.navigation('ProfileStack', {
                            screen: 'OtherProfileScreen',
                            params: { userId: item.userConnectStatus.profileId },
                          });
                  })
                }
                activeOpacity={0.6}
                style={{
                  flexDirection: 'row',
                  height: 44,
                  alignItems: 'center',
                }}>
                <Image
                  source={
                    item.userProfileImage
                      ? { uri: item.userProfileImage }
                      : item.userType === 'COMPANY'
                        ? DefaultBusiness
                        : defaultProfile
                  }
                  style={{
                    marginLeft: '7%',
                    marginRight: 10,
                    width: 32,
                    height: 32,
                    borderRadius: 16,
                  }}
                />
                <View
                  style={{
                    alignItems: 'flex-start',
                    justifyContent: 'center',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Button_Lead,
                        {
                          color: COLORS.dark_600,
                          maxWidth: 150,
                          textTransform:
                            item.userType === 'COMPANY' ? 'none' : 'capitalize',
                        },
                      ]}>
                      {item.userId === this.state.userId
                        ? 'You'
                        : item.userName}
                    </Text>
                    {item.connectDepth ? (
                      <ConnectDepth connectDepth={item.connectDepth} />
                    ) : (
                      <Text></Text>
                    )}
                  </View>
                  {item.userId !== this.state.userId && (
                    <>
                      {item.persona || item.organizationType ? (
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Note2,
                            { color: COLORS.altgreen_400, maxWidth: 150 },
                          ]}>
                          {item.persona ? item.persona : item.organizationType}
                        </Text>
                      ) : null}
                      {item.addressDetail && item.addressDetail.country ? (
                        <Text
                          style={[
                            typography.Note2,
                            { color: COLORS.altgreen_400 },
                          ]}>
                          {item.addressDetail &&
                            item.addressDetail.country &&
                            item.addressDetail.country}
                        </Text>
                      ) : null}
                    </>
                  )}
                </View>
              </TouchableOpacity>

              {item.userId !== this.state.userId && (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    position: 'absolute',
                    right: 0,
                  }}>
                  {item.userConnectStatus.connectStatus ===
                    'REQUEST_RECEIVED' && (
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation('NetworkInvitationStack', {})
                        }
                        style={{
                          height: 30,
                          width: 30,
                          alignItems: 'center',
                          marginRight: 6,
                        }}>
                        <Icon
                          name="TickedUser"
                          size={13}
                          color={COLORS.dark_600}
                          style={{
                            marginTop: Platform.OS === 'android' ? 5 : 0,
                          }}
                        />
                      </TouchableOpacity>
                    )}

                  {item.userConnectStatus.connectStatus === 'NOT_CONNECTED' &&
                    item.userType !== 'COMPANY' &&
                    !this.state.isCompany && (
                      <TouchableOpacity
                        onPress={() =>
                          this.handleConnectStatusChange(item.userId)
                        }
                        style={{
                          height: 30,
                          width: 30,
                          alignItems: 'center',
                          marginRight: 6,
                        }}>
                        <Icon
                          name="AddUser"
                          size={13}
                          color={COLORS.dark_600}
                          style={{
                            marginTop: Platform.OS === 'android' ? 5 : 0,
                          }}
                        />
                      </TouchableOpacity>
                    )}

                  {item.userConnectStatus.connectStatus === 'PENDING_CONNECT' &&
                    item.connectDepth >= 2 && (
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation('MyNetworkStack', {
                            screen: 'YourRequests',
                          })
                        }
                        style={{
                          height: 30,
                          width: 30,
                          alignItems: 'center',
                          marginRight: 6,
                        }}>
                        <Icon
                          name="TickedUser"
                          size={13}
                          color={COLORS.dark_600}
                          style={{
                            marginTop: Platform.OS === 'android' ? 5 : 0,
                          }}
                        />
                      </TouchableOpacity>
                    )}

                  {!this.state.isCompany && (
                    <TouchableOpacity
                      onPress={() =>
                        this.handleFollowUnfollow(
                          item.followed,
                          item.userId,
                          index,
                          item,
                        )
                      }
                      style={{
                        height: 30,
                        width: 30,
                        alignItems: 'center',
                        marginLeft: 2,
                      }}>
                      <Icon
                        name={item.followed ? 'TickRSS' : 'RSS'}
                        size={13}
                        color={
                          item.followed ? COLORS.dark_600 : COLORS.green_600
                        }
                        style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
                      />
                    </TouchableOpacity>
                  )}
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation('Chats', {
                        userId: this.state.userId,
                        otherUserId: item.userId,
                        lastActive: null,
                        grpType: 'Private',
                        name: item.userName,
                        otherUserProfile: item.userProfileImage
                          ? item.userProfileImage
                          : null,
                      })
                    }
                    style={{
                      height: 30,
                      width: 30,
                      alignItems: 'center',
                      marginLeft: 2,
                    }}>
                    <Icon
                      name="WN_Messeges_OL"
                      size={13}
                      color={COLORS.dark_600}
                      style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
                    />
                  </TouchableOpacity>
                </View>
              )}
            </View>
          )}
        />
      </View>
    );
  }
}
