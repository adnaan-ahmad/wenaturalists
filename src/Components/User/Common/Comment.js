import React, {Component} from 'react';
import {
  Linking,
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
  ScrollView,
  Platform,
  PermissionsAndroid,
  Share,
  TextInput,
  Modal,
  Alert,
} from 'react-native';
import VideoPlayer from 'react-native-video-controls';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import Clipboard from '@react-native-community/clipboard';
import Autolink from 'react-native-autolink';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {cloneDeep} from 'lodash';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import * as Progress from 'react-native-progress';
import DocumentPicker from 'react-native-document-picker';
import Snackbar from 'react-native-snackbar';
import {v4 as uuidv4} from 'uuid';
import LinearGradient from 'react-native-linear-gradient';
import TimeAgo from 'react-native-timeago';

import Report from '../../../Components/User/Common/Report';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultShape from '../../../Components/Shared/Shape';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import ConnectDepth from '../../../Components/User/Common/ConnectDepth';
import LikedUserList from '../../../Components/User/Common/LikedUserList';
import SharedUserList from '../../../Components/User/Common/SharedUserList';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import {tagDescription} from '../../Shared/commonFunction';

const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
  'i',
); // fragment locator
const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');

export default class ForumDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      forumData: {},
      commentData: [],
      commentAttachments: [],
      commentBody: '',
      page: 0,
      replyExpand: false,
      replyExpandId: '',
      reply: false,
      downloadStart: false,
      isDownloading: false,
      progressLimit: 0,
      shareModalOpen: false,
      likeModalOpen: false,
      userId: '',
      pressedActivityId: '',

      pressedId: '',
      peopleLiked: [],
      isReported: false,
      optionsModalOpen: false,
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      optionsModal2Open: false,
      edit: false,
      peopleShared: [],
      peopleSharedModalOpen: false,
      isCompany: false,
      commentCount: 0,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        value && this.setState({userId: value});
      })
      .catch((e) => {
        console.log(e);
      });

    AsyncStorage.getItem('userData').then((value) => {
      let objValue = JSON.parse(value);
      objValue.type === 'COMPANY' && this.setState({isCompany: true});
    });

    this.setState({
      commentData: this.props.commentData,
      commentCount: this.props.commentCount,
    });
  }

  navigation = (value, params) => {
    this.setState({likeModalOpen: false, peopleSharedModalOpen: false}, () =>
      this.props.navigate(value, params),
    );
  };

  getUsersWhoShared = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/share/getUsers/' +
        this.props.id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        100,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          this.setState({peopleShared: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  documentPicker = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      for (const res of results) {
        // console.log(res);
        this.state.commentAttachments.push(res);
      }
      this.setState({...this.state});
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  downloadProgressWrapper = () => {
    return (
      <Modal
        visible={this.state.isDownloading}
        transparent
        animationType="fade"
        supportedOrientations={['portrait', 'landscape']}>
        <SafeAreaView
          style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {bottom: 0, height: '100%'},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          {this.state.progressLimit == 1 ? null : (
            <Progress.Pie
              progress={this.state.progressLimit}
              size={120}
              color={COLORS.white}
            />
          )}
        </SafeAreaView>
      </Modal>
    );
  };

  readStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.writeStorage(data);
      } else {
        // Permission Denied
        alert('Read Storage Permission Denied');
      }
    } else {
      this.getDownloadFile(data); //work on ios
    }
  };

  writeStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.getDownloadFile(data);
      } else {
        // Permission Denied
        alert('Write Storage Permission Denied');
      }
    }
  };

  getDownloadFile = (urlDownload) => {
    if (this.state.downloadStart == true) {
      return;
    } else {
      this.setState({downloadStart: true});
    }

    let directoryFile;
    let dirs;

    let fileName;
    try {
      fileName = urlDownload.split('-').pop();
    } catch (e) {
      fileName = uuidv4();
    }
    if (Platform.OS === 'android') {
      directoryFile = RNFetchBlob.fs.dirs;
      dirs = directoryFile.DownloadDir + '/Download' + fileName;
    } else {
      directoryFile = RNFS.DocumentDirectoryPath;
      dirs = directoryFile + fileName;
    }

    try {
      RNFetchBlob.config({
        path: dirs,
      })
        .fetch('GET', urlDownload, {})
        .progress((received, total) => {
          let temp = parseFloat(received / total);

          if (temp > 0.9) {
            temp = 1;
            setTimeout(() => {
              this.setState({isDownloading: false});
            }, 1000);
          }
          this.setState({
            isDownloading: true,
            progressLimit: temp,
          });
        })
        .then((res) => {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'File downloaded successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });

          this.setState({
            ...this.state,
            downloadStart: false,
          });
          setTimeout(() => {
            this.setState({isDownloading: false});
          }, 1000);
        });
    } catch (error) {}
  };

  handleCommentSubmit = () => {
    const formData = new FormData();

    this.setState({commentBody: '', commentAttachments: []});

    let params = {
      userId: this.state.userId,
      activityId: this.props.id,
      userComment: this.state.commentBody.trim('\n'),
    };

    let replyParams = {
      userId: this.state.userId,
      activityId: this.state.replyExpandId,
      userComment: this.state.commentBody.trim('\n'),
      commentType: 'REPLY',
    };

    if (
      this.state.commentAttachments &&
      this.state.commentAttachments.length > 0
    ) {
      this.state.commentAttachments.forEach((file) => {
        formData.append('files', file);
      });
    }

    if (this.state.reply) {
      formData.append('data', JSON.stringify(replyParams));
    } else {
      formData.append('data', JSON.stringify(params));
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/comment/create',
      data: formData,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          this.setState({
            commentCount: this.state.commentCount + 1,
          });
          this.getComments(this.props.id);
        } else {
          console.log(response);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getComments = (activityId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/comment/getCommentsByActivityId/' +
        activityId +
        '?userId=' +
        this.state.userId +
        '&page=' +
        this.state.page +
        '&size=1000',
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '200 OK') {
          this.setState({commentData: res.body.content});
        }
      })
      .catch((err) => {
        console.log('err in get comment : ', err);
      });
  };

  handleLike = (activityId, liked) => {
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.getComments(this.props.id);
        } else {
          console.log(response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + 1 + ' s';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + ' m';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + ' h';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + ' d';
    }

    if (difference >= 864000) {
      return day + ' ' + month + ' ' + year;
    }
  };

  deleteComment = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/comment/delete?commentId=' +
        this.state.pressedActivityId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          console.log(res.status);
          this.getComments(this.props.id);
          this.setState({commentCount: this.state.commentCount - 1});
        }
      })
      .catch((err) => {
        console.log(err);
      });
    this.setState({optionsModal2Open: false, commentBody: ''});
  };

  handleCommentEdit = () => {
    let formData = {
      commentId: this.state.pressedActivityId,
      description: this.state.commentBody.trim(),
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/comment/edit',
      data: formData,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          // console.log(res.status)
          this.getComments(this.props.id);
        }
      })
      .catch((err) => {
        console.log(err);
      });
    this.setState({edit: false, commentBody: '', reply: false});
  };

  getUserDetailsByCustomUrl = (customurl) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get_by_custom_url?customUrl=' +
        customurl +
        '&otherUserId=' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          response.data.body &&
          response.data.body.type === 'INDIVIDUAL' &&
          response.data.body.userId === this.state.userId
            ? this.props.navigate('ProfileStack')
            : response.data.body.type === 'INDIVIDUAL' &&
              response.data.body.userId !== this.state.userId
            ? this.props.navigate('ProfileStack', {
                screen: 'OtherProfileScreen',
                params: {userId: response.data.body.userId},
              })
            : response.data.body.type === 'COMPANY'
            ? this.props.navigate('ProfileStack', {
                screen: 'CompanyProfileScreen',
                params: {userId: response.data.body.userId},
              })
            : this.props.navigate('CircleProfileStack', {
                screen: 'CircleProfile',
                params: {slug: customurl},
              });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderCommentItem = (item) => {
    let str = item.item.userComment.replace(/&nbsp;/g, ' ');
    return (
      <>
        <View style={styles.commentItemView}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => this.props.navigateToProfile(item.item)}
              style={{flexDirection: 'row'}}>
              <Image
                source={
                  item.item.profileImage
                    ? {uri: item.item.profileImage}
                    : defaultProfile
                }
                style={defaultShape.Media_Round}
              />
              <Text
                numberOfLines={1}
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 6,
                    maxWidth: 120,
                    textTransform:
                      item.item.userType === 'COMPANY' ? 'none' : 'capitalize',
                  },
                ]}>
                {item.item.userName}
              </Text>
              {item.item.connectDepth && item.item.connectDepth !== -1 ? (
                <ConnectDepth connectDepth={item.item.connectDepth} />
              ) : item.item.userId !== this.state.userId ? (
                <TouchableOpacity
                  onPress={() =>
                    this.handleFollowUnfollow(
                      item.item.followed,
                      item.item.userId,
                      // index,
                      // item,
                    )
                  }
                  style={{
                    height: 30,
                    width: 30,
                    alignItems: 'center',
                    marginLeft: 4,
                  }}>
                  <Icon
                    name={item.item.followed ? 'TickRSS' : 'RSS'}
                    size={13}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 0 : 0}}
                  />
                </TouchableOpacity>
              ) : (
                <></>
              )}
            </TouchableOpacity>

            <View style={{flexDirection: 'row'}}>
              <Text
                style={[
                  typography.Note,
                  {
                    color: COLORS.altgreen_300,
                    fontFamily: 'Montserrat-Medium',
                    marginTop: 5,
                  },
                ]}>
                {/* {moment.unix(item.item.time / 1000).format('Do MMM, YYYY')} */}
                <TimeAgo time={item.item?.time}/>
              </Text>
              <TouchableOpacity
                style={defaultShape.Nav_Gylph_Btn}
                onPress={() => {
                  this.setState(
                    {
                      optionsModal2Open: true,
                      pressedUserId: item.item.userId,
                      pressedActivityId: item.item.id,
                    },
                    () => this.verifyReported2(),
                  );
                  setTimeout(() => {
                    this.setState({
                      commentBody: str.replace(/<br\s*[\/]?>/gi, '\n'),
                    });
                  }, 500);
                }}>
                <Icon
                  name="Kebab"
                  color={COLORS.altgreen_400}
                  size={14}
                  style={{marginTop: -8}}
                />
              </TouchableOpacity>
            </View>
          </View>

          {/* <Text style={[typography.Body_2, styles.commentBody]}>
            {str.replace(/<br\s*[\/]?>/gi, '\n')}
          </Text> */}

          <Autolink
            text={tagDescription(str)}
            email
            hashtag="instagram"
            mention="twitter"
            phone="sms"
            numberOfLines={5}
            matchers={[
              {
                pattern: /@\[([^[]*)]\(([^(^)]*)\)/g,
                style: {color: COLORS.mention_color, fontWeight: 'bold'},
                getLinkText: (replacerArgs) => `${replacerArgs[2]}`,
                onPress: (match) => {
                  this.getUserDetailsByCustomUrl(match.getReplacerArgs()[1]);
                },
              },
              {
                pattern: /#\[([^[]*)]\(([^(^)]*)\)/g,
                style: {color: COLORS.mention_color, fontWeight: 'bold'},
                getLinkText: (replacerArgs) => `#${replacerArgs[2]}`,
                onPress: (match) => {
                  this.props.navigate('HashTagDetail', {
                    slug: match.getReplacerArgs()[1],
                  });
                },
              },
            ]}
            style={[typography.Body_2, styles.commentBody]}
            url
          />

          {item.item.attachmentIds.length > 0 ? (
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{marginTop: 10, paddingLeft: 32, paddingRight: 30}}
              contentContainerStyle={{paddingRight: 40}}
              data={item.item.attachmentIds}
              keyExtractor={(attachmentItem) => attachmentItem.id}
              renderItem={(attachmentItem) => (
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {
                    this.readStorage(attachmentItem.item.attachmentUrl);
                    console.log(attachmentItem.item);
                  }}
                  style={[
                    defaultShape.ContextBtn_FL,
                    {paddingHorizontal: 5, marginRight: 10},
                  ]}>
                  {/* <Text style={[typography.Caption, { color: COLORS.dark_500 }]}>{attachmentItem.item.attachmentUrl.split("-").pop()}</Text> */}
                  {attachmentItem.item.attachmentType !== 'VIDEO' ? (
                    <Image
                      source={{uri: attachmentItem.item.attachmentUrl}}
                      style={{width: 250, height: 160, borderRadius: 10}}
                    />
                  ) : (
                    <VideoPlayer
                      style={{
                        width: 250,
                        height: 160,
                        borderRadius: 10,
                      }}
                      poster={
                        attachmentItem.item.thumbnails &&
                        attachmentItem.item.thumbnails
                      }
                      tapAnywhereToPause={true}
                      disableFullscreen={true}
                      disableSeekbar={true}
                      disableVolume={true}
                      disableTimer={true}
                      disableBack={true}
                      paused={true}
                      source={{uri: attachmentItem.item.attachmentUrl}}
                      // navigator={this.props.navigator}
                    />
                  )}
                </TouchableOpacity>
              )}
            />
          ) : (
            <></>
          )}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: 5,
            }}>
            {item.item.replies.length > 0 ? (
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    replyExpandId: item.item.id,
                    replyExpand: !this.state.replyExpand,
                  })
                }
                style={{flexDirection: 'row', marginLeft: 38}}>
                <View
                  style={{
                    backgroundColor: COLORS.altgreen_300,
                    height: 14,
                    width: 14,
                    borderRadius: 7,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginRight: 5,
                  }}>
                  <Icon
                    name="Arrow_Down"
                    color={COLORS.white}
                    size={10}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                </View>

                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-Medium',
                    },
                  ]}>
                  {item.item.replies.length}{' '}
                  {item.item.replies.length > 1 ? 'replies' : 'reply'}
                </Text>
              </TouchableOpacity>
            ) : (
              <View></View>
            )}

            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    reply: true,
                    replyExpand: true,
                    replyExpandId: item.item.id,
                  });
                  this.textInputField.focus();
                }}
                style={[
                  defaultShape.Nav_Gylph_Btn,
                  {flexDirection: 'row', marginRight: -10, minWidth: 70},
                ]}>
                <Icon name="Reply" color={COLORS.altgreen_300} size={14} />
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.altgreen_300,
                      marginLeft: 5,
                      marginTop: Platform.OS === 'ios' ? 0 : -8,
                    },
                  ]}>
                  Reply
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleLike(item.item.id, item.item.liked)}
                style={defaultShape.Nav_Gylph_Btn}>
                <Icon
                  name={item.item.liked ? 'Like_FL' : 'Like'}
                  color={COLORS.green_500}
                  size={14}
                  style={{marginTop: 3}}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        {this.state.replyExpandId === item.item.id &&
        this.state.replyExpand === true ? (
          <View>
            <FlatList
              // style={{ marginTop: this.state.setreadmore ? 0 : 10 }}
              // horizontal
              // showsHorizontalScrollIndicator={false}
              data={item.item.replies}
              keyExtractor={(rep) => rep.id}
              renderItem={(rep) => this.renderReplyItem(rep)}
            />
          </View>
        ) : (
          <></>
        )}
      </>
    );
  };

  renderReplyItem = (item) => {
    let str = item.item.userComment.replace(/&nbsp;/g, ' ');
    return (
      <View style={{backgroundColor: COLORS.grey_300}}>
        <View
          style={[
            styles.commentItemView,
            {
              backgroundColor: COLORS.bgfill,
              width: '95%',
              alignSelf: 'flex-end',
            },
          ]}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => this.props.navigateToProfile(item.item)}
              style={{flexDirection: 'row'}}>
              <Image
                source={
                  item.item.profileImage
                    ? {uri: item.item.profileImage}
                    : defaultProfile
                }
                style={defaultShape.Media_Round}
              />
              <Text
                numberOfLines={1}
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 6,
                    maxWidth: 120,
                    textTransform:
                      item.item.userType === 'COMPANY' ? 'none' : 'capitalize',
                  },
                ]}>
                {item.item.userName}
              </Text>

              {item.item.connectDepth && item.item.connectDepth !== -1 ? (
                <ConnectDepth connectDepth={item.item.connectDepth} />
              ) : item.item.userId !== this.state.userId ? (
                <TouchableOpacity
                  onPress={() =>
                    this.handleFollowUnfollow(
                      item.item.followed,
                      item.item.userId,
                      // index,
                      // item,
                    )
                  }
                  style={{
                    height: 30,
                    width: 30,
                    alignItems: 'center',
                    marginLeft: 4,
                  }}>
                  <Icon
                    name={item.item.followed ? 'TickRSS' : 'RSS'}
                    size={13}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 0 : 0}}
                  />
                </TouchableOpacity>
              ) : (
                <></>
              )}
            </TouchableOpacity>

            <View style={{flexDirection: 'row'}}>
              <Text
                style={[
                  typography.Note,
                  {
                    color: COLORS.altgreen_300,
                    fontFamily: 'Montserrat-Medium',
                    marginTop: 5,
                  },
                ]}>
                {/* {moment.unix(item.item.time / 1000).format('Do MMM, YYYY')} */}
                <TimeAgo time={item.item?.time}/>
                {/* {this.unixTime2(item.item.time)} */}
              </Text>
              <TouchableOpacity
                style={defaultShape.Nav_Gylph_Btn}
                onPress={() => {
                  this.setState(
                    {
                      optionsModal2Open: true,
                      pressedUserId: item.item.userId,
                      pressedActivityId: item.item.id,
                    },
                    () => this.verifyReported2(),
                  );
                  setTimeout(() => {
                    this.setState({
                      commentBody: str.replace(/<br\s*[\/]?>/gi, '\n'),
                    });
                  }, 500);
                }}>
                <Icon
                  name="Kebab"
                  color={COLORS.altgreen_400}
                  size={14}
                  style={{marginTop: -8}}
                />
              </TouchableOpacity>
            </View>
          </View>

          {/* <Text style={[typography.Body_2, styles.commentBody]}>
            {str.replace(/<br\s*[\/]?>/gi, '\n')}
          </Text> */}

          <Autolink
            text={tagDescription(str)}
            email
            hashtag="instagram"
            mention="twitter"
            phone="sms"
            numberOfLines={5}
            matchers={[
              {
                pattern: /@\[([^[]*)]\(([^(^)]*)\)/g,
                style: {color: COLORS.mention_color, fontWeight: 'bold'},
                getLinkText: (replacerArgs) => `${replacerArgs[2]}`,
                onPress: (match) => {
                  this.getUserDetailsByCustomUrl(match.getReplacerArgs()[1]);
                },
              },
              {
                pattern: /#\[([^[]*)]\(([^(^)]*)\)/g,
                style: {color: COLORS.mention_color, fontWeight: 'bold'},
                getLinkText: (replacerArgs) => `#${replacerArgs[2]}`,
                onPress: (match) => {
                  this.props.navigate('HashTagDetail', {
                    slug: match.getReplacerArgs()[1],
                  });
                },
              },
            ]}
            style={[typography.Body_2, styles.commentBody]}
            url
          />

          {item.item.attachmentIds.length > 0 ? (
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{marginTop: 10, paddingLeft: 32, paddingRight: 30}}
              contentContainerStyle={{paddingRight: 40}}
              data={item.item.attachmentIds}
              keyExtractor={(attachmentItem) => attachmentItem.id}
              renderItem={(attachmentItem) => (
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {
                    this.readStorage(attachmentItem.item.attachmentUrl);
                  }}
                  style={[
                    defaultShape.ContextBtn_FL,
                    {paddingHorizontal: 5, marginRight: 10},
                  ]}>
                  {/* <Text style={[typography.Caption, { color: COLORS.dark_500 }]}>{attachmentItem.item.attachmentUrl.split("-").pop()}</Text> */}
                  <Image
                    source={{uri: attachmentItem.item.attachmentUrl}}
                    style={{width: 250, height: 160, borderRadius: 10}}
                  />
                </TouchableOpacity>
              )}
            />
          ) : (
            <></>
          )}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: 5,
            }}>
            <View></View>

            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({reply: true, replyExpand: true});
                  this.textInputField.focus();
                }}
                style={[
                  defaultShape.Nav_Gylph_Btn,
                  {flexDirection: 'row', marginRight: -10, minWidth: 70},
                ]}>
                <Icon name="Reply" color={COLORS.altgreen_300} size={14} />
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.altgreen_300,
                      marginLeft: 5,
                      marginTop: Platform.OS === 'ios' ? 0 : -8,
                    },
                  ]}>
                  Reply
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleLike(item.item.id, item.item.liked)}
                style={defaultShape.Nav_Gylph_Btn}>
                <Icon
                  name={item.item.liked ? 'Like_FL' : 'Like'}
                  color={COLORS.green_500}
                  size={14}
                  style={{marginTop: 3}}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.props.id,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  verifyReported2 = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedActivityId,
      // entityType: this.state.entityType,
      entityType: 'FORUM',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    setTimeout(() => {
      this.setState({reasonForReporting: '', description: ''});
    }, 1000);
  };

  changeState = (value) => {
    this.setState(value);
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <Report
          changeState={this.changeState}
          entityId={this.state.pressedActivityId}
          entityType="FORUM"
        />
      </Modal>
    );
  };

  handleHideModal = () => {
    let data = {
      userId: this.state.userId,
      activityId: this.props.id,
      entityType: 'FORUM',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          console.log('response.status', response.status);
          this.props.navigation.goBack();
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          console.log('response.status', err.response.status);
          this.props.navigation.goBack();
        }
      });
    // this.setState({ optionsModalOpen: false, forumData: this.state.forumData.filter((item) => item.id !== id) })
  };

  deleteForum = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/forum/delete?forumId=' +
        this.props.id,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          console.log('response.status', res.status);
          this.props.navigation.goBack();
        }
      })
      .catch((err) => {
        console.log('response.status', err.response.data.message);
        this.props.navigation.goBack();
      });
    // this.setState({ optionsModalOpen: false, forumData: this.state.forumData.filter((item) => item.id !== this.state.pressedActivityId) })
  };

  likeModal = () => {
    return (
      <Modal
        visible={this.state.likeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({likeModalOpen: false, peopleLiked: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who liked this post
              </Text>
            </View>

            <LikedUserList
              id={this.state.pressedActivityId}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  optionsModal2 = () => {
    return (
      <Modal
        visible={this.state.optionsModal2Open}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({optionsModal2Open: false, commentBody: ''})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          {this.state.userId !== this.state.pressedUserId ? (
            <View style={defaultShape.Modal_Categories_Container}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {optionsModal2Open: false, likeModalOpen: true},
                    // () => this.getUsersWhoLiked(),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Like_FL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who liked it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    reasonForReportingModalOpen: !this.state.isReported
                      ? true
                      : false,
                    optionsModal2Open: false,
                    commentBody: '',
                  });
                  this.state.isReported
                    ? setTimeout(() => {
                        Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Your report request was already taken',
                          duration: Snackbar.LENGTH_LONG,
                        });
                      }, 500)
                    : null;
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Report
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={defaultShape.Modal_Categories_Container}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {optionsModal2Open: false, likeModalOpen: true},
                    // () => this.getUsersWhoLiked(),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Like_FL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who liked it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({optionsModal2Open: false, edit: true});
                  this.textInputField.focus();
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Edit
                </Text>
                <Icon
                  name="EditBox"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.setState({commentBody: ''}), this.createTwoButtonAlert();
                }}
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Delete
                </Text>
                <Icon
                  name="TrashBin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  };

  createTwoButtonAlert = () => {
    this.setState({optionsModal2Open: false});
    Alert.alert('', 'Are you sure you want to delete this comment?', [
      {
        text: 'YES',
        onPress: () => this.deleteComment(),
        style: 'cancel',
      },
      {
        text: 'NO',
        onPress: () => this.setState({commentBody: ''}),
      },
    ]);
  };

  deleteForumAlert = () => {
    this.setState({optionsModalOpen: false});
    Alert.alert('', 'Are you sure you want to delete this forum?', [
      {
        text: 'YES',
        onPress: () => this.deleteForum(),
        style: 'cancel',
      },
      {
        text: 'NO',
        onPress: () => console.log('user cancelled deletion'),
      },
    ]);
  };

  openWebsite = (website) => {
    if (website && website.includes('http')) return Linking.openURL(website);
    else if (website && !website.includes('http'))
      return Linking.openURL('https://' + website);
  };

  handleFollowUnfollow = (isFollowed, userId) => {
    // let tempLikeList = cloneDeep(this.state.peopleLiked);
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          let tempCommentData = cloneDeep(
            this.state.commentData.length
              ? this.state.commentData
              : this.props.commentData,
          );
          for (let i of tempCommentData) {
            if (i.userId === userId) {
              // console.log(i.followed, !isFollowed);
              i.followed = !isFollowed;
            }
          }
          this.setState({commentData: tempCommentData});
        } else {
          console.log('');
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    let {forumData} = this.state;
    return (
      <View style={{flex: 1}}>
        {this.downloadProgressWrapper()}
        {this.reasonForReportingModal()}
        {this.likeModal()}
        {this.optionsModal2()}

        {/*********** Header starts ***********/}

        {/*********** Header ends ***********/}

        <View style={styles.commentCountBar}>
          {this.state.commentCount <= 1 ? (
            <Text
              style={[
                typography.Note,
                {color: COLORS.altgreen_300, fontFamily: 'Montserrat-Medium'},
              ]}>
              {this.state.commentCount} Comment
            </Text>
          ) : (
            <Text
              style={[
                typography.Note,
                {color: COLORS.altgreen_300, fontFamily: 'Montserrat-Medium'},
              ]}>
              {this.state.commentCount} Comments
            </Text>
          )}
        </View>

        {/****** Comments start ******/}

        <View>
          <FlatList
            style={{paddingBottom: 50}}
            data={
              this.state.commentData.length
                ? this.state.commentData
                : this.props.commentData
            }
            keyExtractor={(item) => item.id}
            renderItem={(item) => this.renderCommentItem(item)}
          />
        </View>

        {/****** Comments ends ******/}

        {/****** Comment Box start ******/}

        <View
          style={
            this.state.focus && Platform.OS === 'ios'
              ? [styles.commentBoxView, {bottom: 260}]
              : styles.commentBoxView
          }>
          {this.state.commentAttachments.length > 0 ? (
            <FlatList
              data={this.state.commentAttachments}
              keyExtractor={(item) => item.name}
              renderItem={(item) => (
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.dark_500,
                      marginLeft: 60,
                      marginBottom: 10,
                    },
                  ]}>
                  {item.item.name}
                </Text>
              )}
            />
          ) : (
            <></>
          )}

          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 15,
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={this.documentPicker}
              style={defaultShape.AdjunctBtn_Small_Prim}>
              <Icon
                name="Clip"
                color={COLORS.dark_600}
                size={14}
                style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
              />
            </TouchableOpacity>

            <TextInput
              ref={(ref) => {
                this.textInputField = ref;
              }}
              placeholder="Write a comment"
              placeholderTextColor={COLORS.grey_400}
              multiline
              style={styles.inputBox}
              onFocus={() => this.setState({sendButton: true, focus: true})}
              onBlur={() =>
                this.setState({sendButton: false, focus: false, reply: false})
              }
              onChangeText={(data) => this.setState({commentBody: data})}
              value={this.state.commentBody}
            />

            <TouchableOpacity
              onPress={() =>
                this.state.edit
                  ? this.handleCommentEdit()
                  : this.handleCommentSubmit()
              }
              style={{marginLeft: 10}}>
              {/* {(this.state.sendButton || this.state.commentAttachments.length > 0) && */}
              {this.state.commentBody.trim().length ||
              this.state.commentAttachments.length ? (
                <Icon
                  name="Send_fl"
                  color={COLORS.altgreen_400}
                  size={14}
                  style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
                />
              ) : (
                <></>
              )}
            </TouchableOpacity>
          </View>
        </View>

        {/****** Comment Box ends ******/}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  forumItemView: {
    width: '94%',
    marginTop: 15,
    backgroundColor: COLORS.white,
    borderRadius: 4,
    alignSelf: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
    marginBottom: 12,
  },
  profileImage: {
    height: 30,
    width: 30,
    borderRadius: 15,
    marginRight: 8,
  },
  title: {
    marginTop: 15,
    color: COLORS.dark_800,
    marginHorizontal: 15,
  },
  subtitle: {
    color: COLORS.dark_600,
    marginTop: 5,
  },
  body: {
    color: COLORS.altgreen_400,
    marginTop: 5,
  },
  commentCountBar: {
    backgroundColor: COLORS.altgreen_100,
    paddingVertical: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  commentItemView: {
    paddingLeft: 15,
    backgroundColor: COLORS.white,
    paddingTop: 12,
    borderBottomColor: COLORS.altgreen_400,
    borderBottomWidth: 0.2,
  },
  commentBody: {
    color: COLORS.altgreen_400,
    marginTop: -25,
    marginLeft: 38,
    maxWidth: '80%',
  },
  commentBoxView: {
    position: 'absolute',
    bottom: 0,
    paddingVertical: 6,
    backgroundColor: COLORS.altgreen_200,
    width: '100%',
    zIndex: 1,
  },
  inputBox: {
    marginLeft: 10,
    width: '80%',
    height: 40,
    backgroundColor: COLORS.white,
    borderRadius: 6,
    paddingHorizontal: 10,
  },
});
