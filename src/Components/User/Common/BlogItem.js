import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Modal,
  Platform,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import Clipboard from '@react-native-community/clipboard';
import Snackbar from 'react-native-snackbar';
import TimeAgo from 'react-native-timeago';

import {COLORS} from '../../../Components/Shared/Colors';
import BlogDefault from '../../../../assets/BlogDefault.jpg';
import typography from '../../../Components/Shared/Typography';
import defaultProfile from '../../../../assets/defaultProfile.png';
import circleDefault from '../../../../assets/CirclesDefault.png';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';
import ConnectDepth from '../../../Components/User/Common/ConnectDepth';
import Follow from '../../../Components/User/Common/Follow';
import {unixTime2} from '../../Shared/commonFunction';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import LikedUserList from './LikedUserList';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
export default class BlogItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionsModalOpen: false,
      pressedActivityId: '',
      peopleLiked: [],
      shareModalOpen: false,
    };
  }

  trimDescription = (item) => {
    var trimmed = item.split('^^__').join(' ').indexOf('@@@__');

    var str = item
      .split('^^__')
      .join(' ')
      .indexOf(' ', item.split('^^__').join(' ').indexOf('@@@__'));

    var sub = item.substring(trimmed, str);

    item = item.replace(' ' + sub, '');
    item = item.replace('@@@^^^', ' ');
    item = item.replace('@@@__', ' ');
    item = item.replace('  ', '');

    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  renderDescription = (item) => {
    return (
      <View
        style={{
          backgroundColor: COLORS.altgreen_100,
        }}>
        {item.postType === 'ARTICLE' && (
          <Text
            numberOfLines={1}
            style={[
              typography.H6,
              {
                color: COLORS.dark_800,
                marginLeft: 6,
                marginRight: 17,
                marginTop: 5,
              },
            ]}>
            {item.title}
          </Text>
        )}
        <Text
          onPress={() =>
            item.postType === 'ARTICLE'
              ? this.props.navigation('EditorsDesk', {
                  id: item.id,
                  userId: this.state.userId,
                })
              : this.props.navigation('IndividualFeedsPost', {
                  id: item.id,
                })
          }
          // onPress={() => this.props.profileRedirectionNavigation('IndividualFeedsPost', { id: item.id })}
          numberOfLines={item.postType === 'ARTICLE' ? 2 : 5}
          style={[
            typography.Body_1,
            {
              color:
                item.postType === 'ARTICLE' ? COLORS.dark_500 : COLORS.dark_700,
              marginLeft: 6,
              marginRight: 17,
              marginVertical: 8,
            },
          ]}>
          {this.trimDescription(item.description)}
        </Text>
      </View>
    );
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + 's';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + 'm';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + 'h';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + 'd';
    }

    if (difference >= 864000) {
      return day + ' ' + month + ' ' + year;
    }
  };

  renderHeader = (item, index) => {
    return (
      <View style={styles.unreadNotiItem}>
        <View
          style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              item.userType === 'COMPANY'
                ? this.props.navigate([
                    'ProfileStack',
                    {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.userId},
                    },
                  ])
                : item.userType === 'INDIVIDUAL' &&
                  item.userId !== this.state.userId
                ? this.props.navigate([
                    'ProfileStack',
                    {
                      screen: 'OtherProfileScreen',
                      params: {userId: item.userId},
                    },
                  ])
                : item.userType === 'INDIVIDUAL' &&
                  item.userId === this.state.userId
                ? this.props.navigate([
                    'ProfileStack',
                    {
                      screen: 'ProfileScreen',
                      // params: {userId: item.userId},
                    },
                  ])
                : null;
            }}>
            <Image
              style={[defaultShape.Media_Round, {}]}
              source={
                item?.originalProfileImage
                  ? {uri: item.originalProfileImage}
                  : item.userType && item.userType === 'COMPANY'
                  ? defaultBusiness
                  : defaultProfile
              }
            />
          </TouchableOpacity>

          <View style={{alignItems: 'center', justifyContent: 'flex-start', marginLeft: 6}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                // justifyContent: 'flex-start',
              }}>
              <Text
                numberOfLines={1}
                onPress={() => {
                  item.userType === 'COMPANY'
                    ? this.props.navigate([
                        'ProfileStack',
                        {
                          screen: 'CompanyProfileScreen',
                          params: {userId: item.userId},
                        },
                      ])
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId !== this.state.userId
                    ? this.props.navigate([
                        'ProfileStack',
                        {
                          screen: 'OtherProfileScreen',
                          params: {userId: item.userId},
                        },
                      ])
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId === this.state.userId
                    ? this.props.navigate([
                        'ProfileStack',
                        {
                          screen: 'ProfileScreen',
                          // params: {userId: item.userId},
                        },
                      ])
                    : null;
                }}
                style={[
                  typography.Title_2,
                  {
                    color: COLORS.dark_800,
                    maxWidth: 160,
                  },
                ]}>
                {item &&
                  item.userName &&
                  item.userName.split(' ').splice(0, 2).join(' ')}
              </Text>

              {/* <Follow
                item={item}
                followed={item.followed}
                userId={item.userId}
                index={index}
              /> */}

              {item.userId !== this.state.userId && (
                <TouchableOpacity
                  onPress={() =>
                    this.props.handleFollowUnfollow(item.followed, item.userId)
                  }
                  style={{
                    height: 30,
                    width: 30,
                    alignItems: 'center',
                    marginLeft: 4,
                  }}>
                  <Icon
                    name={item.followed ? 'TickRSS' : 'RSS'}
                    size={13}
                    color={item.followed?COLORS.dark_600:COLORS.green_500}
                    style={{marginTop: Platform.OS === 'android' ? 4 : 2}}
                  />
                </TouchableOpacity>
              )}
            </View>

            <View
              style={{flexDirection: 'row'}}>
              {item && item.country ? (
                <Icon
                  name="Location"
                  color={COLORS.altgreen_300}
                  size={10}
                  style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                />
              ) : (
                <></>
              )}
              <Text
              numberOfLines={1}
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigate([
                        'EditorsDesk',
                        {
                          id: item.id,
                          userId: this.state.userId,
                        },
                      ])
                    : this.props.navigate([
                        'IndividualFeedsPost',
                        {
                          id: item.id,
                          commentCount: item.commentCount,
                        },
                      ])
                }
                style={[
                  {
                    color: COLORS.altgreen_300,
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                    maxWidth: 100,
                  },
                ]}>
                {item && item.country ? item.country : null}
                {/* {+ new Date() - item.createTime} */}
              </Text>
              {item && item.country ? (
                <Icon
                  name="Bullet_Fill"
                  color={COLORS.altgreen_300}
                  size={8}
                  style={{marginTop: Platform.OS === 'android' ? 1 : 2}}
                />
              ) : (
                <></>
              )}

              <Text
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigate([
                        'EditorsDesk',
                        {
                          id: item.id,
                          userId: this.state.userId,
                        },
                      ])
                    : this.props.navigate([
                        'IndividualFeedsPost',
                        {
                          id: item.id,
                          commentCount: item.commentCount,
                        },
                      ])
                }
                style={[
                  {
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                  },
                ]}>
                {/* {item && this.unixTime2(item.createTime)} */}
                <TimeAgo time={item.createTime}/>
                {/* {+ new Date() - item.createTime} */}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };
  navigation = (value, params) => {
    this.setState({likeModalOpen: false, peopleSharedModalOpen: false}, () =>
      this.props.navigation.navigate(value, params),
    );
  };

  render() {
    const {item, index} = this.props;
    return (
      <View style={styles.renderItemStyle}>
        {/* {this.optionsModal()} */}
        {/* {this.likeModal()}
        {this.shareModal()} */}
        {this.renderHeader(item, index)}
        {item && item.attachmentIds && item.attachmentIds[0] ? (
          <Image
            style={{width: '100%', height: 100}}
            source={{uri: item.attachmentIds[0].attachmentUrl}}
          />
        ) : (
          <Image source={BlogDefault} style={{width: '100%', height: 100}} />
        )}
        {item && item.description ? (
          this.renderDescription(item)
        ) : (
          <View
            style={{
              backgroundColor: 'transparent',
              height: 10,
            }}></View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  renderItemStyle: {
    backgroundColor: COLORS.white,
    marginHorizontal: 10,
    marginVertical: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: COLORS.grey_300,
    width: 250,
    height: 200,
  },
  unreadNotiItem: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderRadius: 8,
    paddingVertical: 3,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
});
