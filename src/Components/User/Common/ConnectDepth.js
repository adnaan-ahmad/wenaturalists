import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {COLORS} from '../../Shared/Colors';
import typography from '../../Shared/Typography';

export default class ConnectDepth extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        {this.props.connectDepth ? (
          this.props.connectDepth === 1 ? (
            <Text style={[typography.Note2, {color: COLORS.altgreen_400}]}>
              {' '}
              • 1st
            </Text>
          ) : this.props.connectDepth === 2 ? (
            <Text style={[typography.Note2, {color: COLORS.altgreen_400}]}>
              {' '}
              • 2nd
            </Text>
          ) : this.props.connectDepth === -1 ||
            this.props.connectDepth === 0 ? (
            <Text></Text>
          ) : (
            <Text></Text>
          )
        ) : (
          <Text></Text>
        )}
      </View>
    );
  }
}
