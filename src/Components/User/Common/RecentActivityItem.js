import React, { Component } from 'react'
import { Platform, StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import axios from 'axios'

import BlogDefault from '../../../../assets/BlogDefault.jpg'
import circleDefault from '../../../../assets/CirclesDefault.png'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import projectDefault from '../../../../assets/project-default.jpg'
import defaultStyle from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
export default class RecentActivityItem extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    unixTime = (UNIX_timestamp) => {
        var date = new Date(UNIX_timestamp)
        var months = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
        ]
        var year = date.getFullYear()
        var month = months[date.getMonth()]
        var day = date.getDate()
        return day + ' ' + month + ' ' + year
    }

    convertToFirstCapital = (s) => {
        if (s) {
            if (s.length <= 1) {
                return s.toUpperCase()
            }
            return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase()
        }
        return s
    }

    getProjectDetails = (projectId) => {
        axios({
            method: 'get',
            url:
                REACT_APP_userServiceURL +
                "/participants/" +
                "find-project-description-details-by-userId-and-projectId/" +
                this.props.userId +
                "/" +
                projectId,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.body && response.data.message === 'Success!') {
                this.props.navigation('ProjectDetailView', {
                    slug: response.data.body.slug
                })
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    redirect = (data) => {
        if (data.parentType === 'FORUM') {
            this.props.navigation('ForumStack', {
                screen: 'ForumDetails',
                params: { slug: data.parentId, userId: this.props.userId }
            })
        }
        if (data.parentType === 'POST') {
            this.props.navigation(
                'IndividualFeedsPost',
                {
                    id: data.parentId
                },
            )
        }
        if (data.parentType === 'POLL' || data.parentType === 'POLLVOTE') {
            this.props.navigation('PollStack', {
                screen: 'PollDetails',
                params: { slug: data.parentId, userId: this.props.userId }
            })
        }
        if (data.parentType == "APPLYINFUNDRAISE" || data.parentType == "APPLYINSTORYBOOK" ||
            data.parentType == "APPLYINEXPEDITION" || data.parentType == "APPLYINASSIGNMENT" || data.parentType == "APPLYINTRAINING" ||
            data.parentType == "APPLYINEVENT" || data.parentType == "APPLYINJOB") {
            this.props.navigation('ProjectDetailView', {
                slug: data.parentId,
            })
        }
        if (data.parentType === 'STORY') {
            this.props.navigation('Explore')
        }
        if (data.parentType === 'ASSIGNMENT' || data.parentType === 'TRAINING' || data.parentType === 'EVENT'
            || data.parentType === 'EXPEDITION' || data.parentType === 'STORYBOOK' || data.parentType === 'FUNDRAISE' || data.parentType === 'JOB') {
            this.getProjectDetails(data.parentId)
        }
        if (data.parentType === 'FEEDBACK') {
            this.props.navigation('FeedBackStack')
        }
        if (data.type === 'CIRCLE') {
            this.props.navigation("CircleProfileStack", {
                screen: 'CircleProfile',
                params: { slug: data.parentId },
            })
        }
    }

    render() {
        const { data } = this.props
        return (
            <View style={{}}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => {
                        this.redirect(data)
                    }}>
                    <View style={{ flexDirection: 'row', marginRight: 10 }}>

                        {(data.parentType === 'POLL' || data.secondaryType === 'POLL') ?
                            <View style={styles.usersico}><Icon name="Polls" size={26} color={COLORS.grey_400} style={styles.icon} /></View> :
                            (data.parentType === 'FORUM' || data.secondaryType === 'FORUM') ?
                                <View style={styles.usersico}><Icon name="WN_Forum_OL" size={26} color={COLORS.grey_400} style={styles.icon} /></View> :
                                data.activityAttachmentType === "POST_AUDIO" ?
                                    <View style={styles.usersico}><Icon name="Audio" size={26} color={COLORS.grey_400} style={styles.icon} /></View> :
                                    data.activityAttachmentType === "POST_LINK" ?
                                        <View style={styles.usersico}><Icon name="Link" size={26} color={COLORS.grey_400} style={styles.icon} />
                                        </View> :
                                        (data.activityAttachmentType === "POST_COVER_IMAGE" && data.coverImage === null) ?
                                            <View style={styles.usersico}><Image style={{ width: 60, height: 60, borderTopLeftRadius: 5, borderBottomLeftRadius: 5 }}
                                                source={BlogDefault}
                                            />
                                            </View>

                                            :
                                            <View style={styles.usersico}>
                                                {data.coverImage != null && data.coverImage != "" ?
                                                    <Image style={{ width: 60, height: 60, borderTopLeftRadius: 5, borderBottomLeftRadius: 5 }}
                                                        source={{ uri: data.coverImage }} /> :
                                                    (data.parentType === 'JOB' || data.parentType === 'ASSIGNMENT' || data.parentType === 'TRAINING' || data.parentType === 'EVENT' || data.parentType === 'EXPEDITION' || data.parentType === 'STORYBOOK' || data.parentType === 'FUNDRAISE'
                                                        || data.secondaryType === 'JOB' || data.secondaryType === 'ASSIGNMENT' || data.secondaryType === 'TRAINING' || data.secondaryType === 'EVENT' || data.secondaryType === 'EXPEDITION' || data.secondaryType === 'STORYBOOK' || data.secondaryType === 'FUNDRAISE' ||
                                                        data.type == "APPLYINJOB" || data.type == "APPLYINFUNDRAISE" || data.type == "APPLYINSTORYBOOK" ||
                                                        data.type == "APPLYINEXPEDITION" || data.type == "APPLYINASSIGNMENT" || data.type == "APPLYINTRAINING" ||
                                                        data.type == "APPLYINEVENT") ?
                                                        <Image style={{ width: 60, height: 60, borderTopLeftRadius: 5, borderBottomLeftRadius: 5 }} source={projectDefault} />
                                                        : data.type === 'CIRCLE' ?
                                                            <Image style={{ width: 60, height: 60, borderTopLeftRadius: 5, borderBottomLeftRadius: 5 }} source={circleDefault} />
                                                            : <Icon name="Img" size={26} color={COLORS.grey_400} style={styles.icon} />}
                                            </View>}
                        <View style={styles.profrcadesc}>
                            {data.secondaryType == "COMMENT_REPLY" ?
                                <Text style={styles.textStyle}>Replied on a comment on</Text> :
                                data.type == "POLLVOTE" ?
                                    <Text style={styles.textStyle}>Voted on a Poll on</Text> :
                                    (data.type == "APPLYINJOB" || data.type == "APPLYINFUNDRAISE" || data.type == "APPLYINSTORYBOOK" ||
                                        data.type == "APPLYINEXPEDITION" || data.type == "APPLYINASSIGNMENT" || data.type == "APPLYINTRAINING" ||
                                        data.type == "APPLYINEVENT") ?
                                        <Text style={styles.textStyle}>Applied for {(data.type === "APPLYINEVENT" || data.type === "APPLYINASSIGNMENT" || data.type === "APPLYINEXPEDITION") ? "an" : "a"} {data.type === "APPLYINJOB" ? "Job" : data.type === "APPLYINSTORYBOOK" ? "Story Book" : data.type === "APPLYINFUNDRAISE" ? "FundRaise" :
                                            data.type === "APPLYINEXPEDITION" ? "Expedition" : data.type === "APPLYINASSIGNMENT" ? "Assignment" : data.type === "APPLYINTRAINING" ? "Training" : data.type === "APPLYINEVENT" ? "Event" : ""} on</Text> :
                                        data.type == "CIRCLE" ?
                                            <Text style={styles.textStyle}>Created a {this.convertToFirstCapital(data.type)} on</Text> :
                                            data.sharedAttachmentType != null ? (<Text style={styles.textStyle}>Shared
                                                {data.sharedAttachmentSize > 1 ? "" : (data.sharedAttachmentType == "IMAGE" || data.sharedAttachmentType == "AUDIO") ? " an " : " a "} {data.sharedAttachmentType == "COVER_IMAGE" ? "Blog" :
                                                    data.sharedAttachmentType == "VIDEO" ? "Video" :
                                                        data.sharedAttachmentType == "AUDIO" ? "Audio" :
                                                            data.sharedAttachmentType == "IMAGE" && data.sharedAttachmentSize > 1 ? "Images" :
                                                                data.sharedAttachmentType == "IMAGE" && data.sharedAttachmentSize === 1 ? "Image" :
                                                                    data.sharedAttachmentType} on</Text>) :

                                                data.type === "COMMENT" ? (<Text style={styles.textStyle}>Commented
                                                    on {(data.activityAttachmentType == "POST_IMAGE" || data.activityAttachmentType == "POST_AUDIO" || data.type == "EVENT" || data.type == "ASSIGNMENT" || data.type == "EXPEDITION") ? " an " : " a "} {data.activityAttachmentType == "POST_AUDIO" ? " Audio" :
                                                        data.activityAttachmentType == "POST_COVER_IMAGE" ? " Blog" :
                                                            data.activityAttachmentType == "POST_VIDEO" ? " Video" :
                                                                data.activityAttachmentType == "POST_IMAGE" ? " Image" :
                                                                    data.activityAttachmentType == "POST_LINK" ? " Link" :
                                                                        data.parentType == "POST" && data.type == "COMMENT" ? "Post" :
                                                                            this.convertToFirstCapital(data.secondaryType)} </Text>) :
                                                    data.secondaryType && data.secondaryType != null ?
                                                        <Text style={styles.textStyle}>Shared {data.sharedAttachmentSize === 1 ? "" : (data.activityAttachmentType == "POST_IMAGE" || data.activityAttachmentType == "POST_AUDIO" || data.type == "EVENT" || data.type == "ASSIGNMENT" || data.type == "EXPEDITION") ? " an " : " a "}{data.activityAttachmentType == "POST_AUDIO" ? " Audio" :
                                                            data.activityAttachmentType == "POST_COVER_IMAGE" ? " Blog" :
                                                                data.activityAttachmentType == "POST_VIDEO" ? " Video" :
                                                                    data.activityAttachmentType == "IMAGE" && data.sharedAttachmentSize > 1 ? "Images" :
                                                                        data.activityAttachmentType == "IMAGE" && data.sharedAttachmentSize === 1 ? "Image" :
                                                                            data.activityAttachmentType == "POST_LINK" ? " Link" :
                                                                                data.secondaryType == "EXPLOREBLOG" ? " Blog" :
                                                                                    this.convertToFirstCapital(data.secondaryType)} on</Text> :
                                                        <Text style={styles.textStyle}>Posted {data.sharedAttachmentSize > 1 ? "" : (data.activityAttachmentType == "POST_IMAGE" || data.activityAttachmentType == "POST_AUDIO" || data.type == "EVENT" || data.type == "ASSIGNMENT" || data.type == "EXPEDITION") ? " an " : " a "}{data.activityAttachmentType == "POST_AUDIO" ? " Audio" :
                                                            data.activityAttachmentType == "POST_COVER_IMAGE" ? " Blog" :
                                                                data.activityAttachmentType == "POST_VIDEO" ? " Video" :
                                                                    data.activityAttachmentType == "POST_IMAGE" && data.sharedAttachmentSize > 1 ? " Images" :
                                                                        data.activityAttachmentType == "POST_IMAGE" && data.sharedAttachmentSize === 1 ? " Image" :
                                                                            data.activityAttachmentType == "POST_LINK" ? " Link" :
                                                                                this.convertToFirstCapital(data.type)} on</Text>}
                            <Text style={styles.textStyle}>{this.unixTime(data.createTime)}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    usersico: {
        width: 60,
        height: 60,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        backgroundColor: COLORS.grey_350,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        marginTop: Platform.OS === 'android' ? 10 : 0
    },
    textStyle: {
        fontSize: 12,
        fontFamily: 'Montserrat-SemiBold',
        letterSpacing: 0,
        lineHeight: 18,
        marginHorizontal: 12,
        color: COLORS.primarydark,
    },
    profrcadesc: {
        width: 150,
        height: 60,
        backgroundColor: '#FFF',
        justifyContent: 'center',
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    }
})
