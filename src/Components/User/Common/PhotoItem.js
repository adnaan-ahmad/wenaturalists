import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import Autolink from 'react-native-autolink';
import TimeAgo from 'react-native-timeago';

import {COLORS} from '../../../Components/Shared/Colors';
import BlogDefault from '../../../../assets/BlogDefault.jpg';
import typography from '../../../Components/Shared/Typography';
import defaultProfile from '../../../../assets/defaultProfile.png';
import circleDefault from '../../../../assets/CirclesDefault.png';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';
import ConnectDepth from '../../../Components/User/Common/ConnectDepth';
import Follow from '../../../Components/User/Common/Follow';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
export default class PhotoItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  trimDescription = (item) => {
    var trimmed = item.split('^^__').join(' ').indexOf('@@@__');

    var str = item
      .split('^^__')
      .join(' ')
      .indexOf(' ', item.split('^^__').join(' ').indexOf('@@@__'));

    var sub = item.substring(trimmed, str);

    item = item.replace(' ' + sub, '');
    item = item.replace('@@@^^^', ' ');
    item = item.replace('@@@__', ' ');
    item = item.replace('  ', '');

    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + ' s';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + ' m';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + ' h';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + ' d';
    }

    if (difference >= 864000) {
      return day + ' ' + month + ' ' + year;
    }
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderHeader = (item, index) => {
    return (
      <View style={styles.unreadNotiItem}>
        <View
          style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              item.userType === 'COMPANY'
                ? this.props.navigate([
                    'ProfileStack',
                    {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.userId},
                    },
                  ])
                : item.userType === 'INDIVIDUAL' &&
                  item.userId !== this.state.userId
                ? this.props.navigate([
                    'ProfileStack',
                    {
                      screen: 'OtherProfileScreen',
                      params: {userId: item.userId},
                    },
                  ])
                : item.userType === 'INDIVIDUAL' &&
                  item.userId === this.state.userId
                ? this.props.navigate([
                    'ProfileStack',
                    {
                      screen: 'ProfileScreen',
                      // params: {userId: item.userId},
                    },
                  ])
                : null;
            }}>
            <Image
              style={[defaultShape.Media_Round, {}]}
              source={
                item?.originalProfileImage
                  ? {uri: item.originalProfileImage}
                  : item.userType && item.userType === 'COMPANY'
                  ? defaultBusiness
                  : defaultProfile
              }
            />
          </TouchableOpacity>

          <View style={{alignItems: 'center', justifyContent: 'flex-start', marginLeft: 6}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                // justifyContent: 'flex-start',
              }}>
              <Text
                numberOfLines={1}
                onPress={() => {
                  item.userType === 'COMPANY'
                    ? this.props.navigate([
                        'ProfileStack',
                        {
                          screen: 'CompanyProfileScreen',
                          params: {userId: item.userId},
                        },
                      ])
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId !== this.state.userId
                    ? this.props.navigate([
                        'ProfileStack',
                        {
                          screen: 'OtherProfileScreen',
                          params: {userId: item.userId},
                        },
                      ])
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId === this.state.userId
                    ? this.props.navigate([
                        'ProfileStack',
                        {
                          screen: 'ProfileScreen',
                          // params: {userId: item.userId},
                        },
                      ])
                    : null;
                }}
                style={[
                  typography.Title_2,
                  {
                    color: COLORS.dark_800,
                    maxWidth: 160,
                  },
                ]}>
                {item &&
                  item.userName &&
                  item.userName.split(' ').splice(0, 2).join(' ')}
              </Text>

              {/* <Follow
                item={item}
                followed={item.followed}
                userId={item.userId}
                index={index}
              /> */}

              {item.userId !== this.state.userId && (
                <TouchableOpacity
                  onPress={() =>
                    this.props.handleFollowUnfollow(item.followed, item.userId)
                  }
                  style={{
                    height: 30,
                    width: 30,
                    alignItems: 'center',
                    marginLeft: 4,
                  }}>
                  <Icon
                    name={item.followed ? 'TickRSS' : 'RSS'}
                    size={13}
                    color={item.followed?COLORS.dark_600:COLORS.green_500}
                    style={{marginTop: Platform.OS === 'android' ? 4 : 2}}
                  />
                </TouchableOpacity>
              )}
            </View>

            <View
              style={{flexDirection: 'row'}}>
              {item && item.country ? (
                <Icon
                  name="Location"
                  color={COLORS.altgreen_300}
                  size={10}
                  style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                />
              ) : (
                <></>
              )}
              <Text
              numberOfLines={1}
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigate([
                        'EditorsDesk',
                        {
                          id: item.id,
                          userId: this.state.userId,
                        },
                      ])
                    : this.props.navigate([
                        'IndividualFeedsPost',
                        {
                          id: item.id,
                          commentCount: item.commentCount,
                        },
                      ])
                }
                style={[
                  {
                    color: COLORS.altgreen_300,
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                    maxWidth: 100,
                  },
                ]}>
                {item && item.country ? item.country : null}
                {/* {+ new Date() - item.createTime} */}
              </Text>
              {item && item.country ? (
                <Icon
                  name="Bullet_Fill"
                  color={COLORS.altgreen_300}
                  size={8}
                  style={{marginTop: Platform.OS === 'android' ? 1 : 2}}
                />
              ) : (
                <></>
              )}

              <Text
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigate([
                        'EditorsDesk',
                        {
                          id: item.id,
                          userId: this.state.userId,
                        },
                      ])
                    : this.props.navigate([
                        'IndividualFeedsPost',
                        {
                          id: item.id,
                          commentCount: item.commentCount,
                        },
                      ])
                }
                style={[
                  {
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                  },
                ]}>
                {/* {item && this.unixTime2(item.createTime)} */}
                <TimeAgo time={item.createTime}/>
                {/* {+ new Date() - item.createTime} */}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  renderImage = (item) => {
    if (item.attachmentIds) {
      if (item.attachmentIds.length > 5) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[3].attachmentUrl}}
              />

              <ImageBackground
                source={{uri: item.attachmentIds[4].attachmentUrl}}
                imageStyle={{}}
                style={{height: 120, width: '57.8%'}}>
                <View
                  style={{
                    height: 120,
                    width: '57.8%',
                    backgroundColor: COLORS.dark_900 + 'BF',
                    opacity: 0.95,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat-Bold',
                      fontSize: 28,
                      color: COLORS.white,
                    }}>
                    +{item.attachmentIds.length - 4}
                  </Text>
                </View>
              </ImageBackground>
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 5) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[3].attachmentUrl}}
              />
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[4].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 4) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[3].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 3) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '100%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 2) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '100%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '100%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 1) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <Image
              style={{width: '100%', height: 150}}
              source={{uri: item.attachmentIds[0].attachmentUrl}}
            />
          </TouchableOpacity>
        );
      }
    } else return <></>;
  };

  renderDescription = (item) => {
    return (
      <Autolink
        text={this.trimDescription(item.description)}
        email
        hashtag="instagram"
        mention="twitter"
        phone="sms"
        numberOfLines={4}
        style={[
          typography.Body_1,
          {
            color:
              item.postType === 'ARTICLE' ? COLORS.dark_500 : COLORS.dark_700,
            marginLeft: 6,
            marginRight: 17,
            marginVertical: 8,
          },
        ]}
        url
      />
    );
  };

  render() {
    const {item, index} = this.props;
    return (
      <View style={styles.renderItemStyle}>
        {this.renderHeader(item.item, item.index)}
        {this.renderImage(item.item)}
        {this.renderDescription(item.item)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  renderItemStyle: {
    backgroundColor: COLORS.white,
    marginHorizontal: 10,
    marginVertical: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: COLORS.grey_300,
    width: 300,
    height: 300,
  },
  unreadNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderRadius: 8,
    height: 60,
  },
});
