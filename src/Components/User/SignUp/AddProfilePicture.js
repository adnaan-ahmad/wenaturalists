import React, { Component } from 'react';
import {
    ScrollView,
    View,
    Text,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    StyleSheet,
    FlatList,
    TextInput,
    Platform,
    Modal
} from 'react-native';
import Svg, {
    Image,
    Polygon,
    Defs,
    ClipPath,
} from 'react-native-svg'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import coverDefault from '../../../../assets/coverDefault.png'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import ManImage from '../../../../assets/manImage.jpeg'
import Header from './Header'
import MoreDetails from './MoreDetails'
import ImagePicker from 'react-native-image-crop-picker'
// import MyStatusBar from '../../Shared/MyStatusBar'

const windowHeight = Dimensions.get('window').height
const Icon = createIconSetFromIcoMoon(icoMoonConfig)

var skills = [], interests = []

export default class AddProfilePicture extends Component {

    constructor(props) {
        super(props)
        this.state = {
            detail: 'profile',
            suggestions: ['Botany', 'Sadas', 'Tourism', 'Wildlife', 'Photography', 'Climbing', 'Paraglyding', 'Skiing'],
            defines: ['Botany', 'Sadas', 'Tourism', 'Wildlife', 'Photography', 'Climbing', 'Paraglyding', 'Skiing'],
            addSkill: '',
            addInterest: '',
            focus: '',
            modalCountry: false,
            query: '',
            country: 'What defines you best',
            countryCodes: ['Botany', 'Sadas', 'Tourism', 'Wildlife', 'Photography', 'Climbing', 'Paraglyding', 'Skiing'],
            profilePic: ManImage,
            // coverPic: "https://cdn.dscovr.com/images/prof-banner.png",
            coverPic: 'https://i.pinimg.com/originals/24/4a/70/244a70a7916bf9b411deb294af95076c.jpg'
        }
    }

    changeSuggestion = (value) => {
        // this.state.suggestions.filter((item) => value.toUpperCase().indexOf(item !== -1))

        const regex = new RegExp(`${value.trim()}`, 'i')

        return this.state.suggestions.filter(item => (item).search(regex) >= 0)
    }

    removeSkill = (value) => {

        for (let i = 0; i < skills.length; i++) {

            if (skills[i] == value) {
                skills.splice(i, 1)
                i--
                this.setState({ ...this.state })
                return skills

            }
        }
    }

    removeInterest = (value) => {

        for (let i = 0; i < interests.length; i++) {

            if (interests[i] == value) {
                interests.splice(i, 1)
                i--
                this.setState({ ...this.state })
                return interests

            }
        }
    }

    modifiedSkills = () => {

        return skills.map((item) => (
            <View key={item} style={{ flexDirection: 'row', marginTop: 15, marginLeft: 10, backgroundColor: '#367681', paddingHorizontal: 10, height: 27, borderRadius: 20, justifyContent: 'center', alignItems: 'center' }} >

                <Text style={{ fontSize: 12, color: '#FFFFFF', fontWeight: '300' }}>
                    {item}
                </Text>

                <TouchableOpacity onPress={() => this.removeSkill(item)} style={{ marginRight: -5, width: 20, height: 20, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.5}>
                    <Icon name='Cross' size={5} color='#D0E8C8' />
                </TouchableOpacity>
            </View>
        ))

    }

    modifiedInterest = () => {

        return interests.map((item) => (
            <View key={item} style={{ flexDirection: 'row', marginTop: 15, marginLeft: 10, backgroundColor: '#367681', paddingHorizontal: 10, height: 27, borderRadius: 20, justifyContent: 'center', alignItems: 'center' }} >

                <Text style={{ fontSize: 12, color: '#FFFFFF', fontWeight: '300' }}>
                    {item}
                </Text>

                <TouchableOpacity onPress={() => this.removeInterest(item)} style={{ marginRight: -5, width: 20, height: 20, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.5}>
                    <Icon name='Cross' size={5} color='#D0E8C8' />
                </TouchableOpacity>
            </View>
        ))
    }

    findCountry = (query) => {
        if (query === '') {
            // console.log(this.state.countryCodes[20])
            return this.state.countryCodes
        }

        const regex = new RegExp(`${query.trim()}`, 'i')

        return this.state.countryCodes.filter(item => item.search(regex) >= 0)
    }

    changeScreen = () => {
        if (this.state.detail === 'profile') {
            return (

                <ScrollView style={{ flex: 1, backgroundColor: '#00394D' }}>
                    {/* <MyStatusBar barStyle="light-content" /> */}
                    <View style={{ alignItems: 'center' }}>

                        {/* Image Backgroung Header*/}

                        <Header text='Personalize' progress={0.75} />

                        {/* Image Backgroung Header*/}

                        <Text style={{ fontSize: 16, color: '#C8DB6E', marginTop: 20, fontWeight: '700' }}>
                            Personalize your profile
                    </Text>
                        <Text style={{ marginTop: 5, fontSize: 12, color: '#91B3A2' }}>
                            Make your profile more visual
                    </Text>

                        {/* Cover Image */}

                        <View style={{ width: '80%', marginTop: 50, alignItems: 'center' }}>
                            <ImageBackground style={{ width: '100%', height: 140, alignItems: 'center' }} source={{ uri: this.state.coverPic }}>
                                <TouchableOpacity style={styles.changeCover} activeOpacity={0.5} onPress={() => ImagePicker.openPicker({
                                    width: 300,
                                    height: 400,
                                    mediaType: 'photo'
                                    // cropping: true
                                }).then(image => {
                                    // this.setState({ coverPic: image.sourceURL })
                                    // console.log('Testing', image)
                                    ImagePicker.openCropper({
                                        path: image.path,
                                        width: 300,
                                        height: 400
                                    }).then(cropImage => {
                                        this.setState({ coverPic: cropImage.path })

                                    }).catch(err => console.log(err))

                                })
                                    .catch(err => console.log(err))}>
                                    <Icon name='TakePhoto' size={18} color='#D0E8C8' style={Platform.OS === 'ios' ? { alignSelf: 'center' } : { marginTop: 10 }} />
                                </TouchableOpacity>

                                {/* Profile Image */}

                                <View style={{ marginTop: 20 }}>

                                    <Svg height="125" width="95">

                                        <Defs>
                                            <ClipPath id="clip">
                                                <Polygon
                                                    points="47 0, 0 27, 0 81, 68 123, 63 97, 95 81, 95 27"
                                                    fill="transparent"
                                                    stroke="white"
                                                    strokeWidth="10"
                                                    zIndex={2}
                                                    position='absolute'
                                                />
                                            </ClipPath>
                                        </Defs>

                                        <Image
                                            width="110"
                                            height="125"
                                            preserveAspectRatio="xMidYMid slice"
                                            opacity="1"
                                            href={this.state.profilePic}
                                            clipPath="url(#clip)"
                                            zIndex={1}
                                        />

                                        <Polygon
                                            points="47 0, 0 27, 0 81, 68 123, 63 97, 95 81, 95 27"
                                            stroke="white"
                                            strokeWidth="1.5"
                                            zIndex={2}
                                        />

                                    </Svg>
                                </View>

                                {/* Profile Image */}

                            </ImageBackground>
                            <TouchableOpacity activeOpacity={0.8}
                                onPress={() => ImagePicker.openCamera({
                                    width: 300,
                                    height: 400,
                                    cropping: true
                                }).then(image => {
                                    this.setState({ profilePic: image.path })

                                })
                                    .catch(err => console.log(err))}
                                style={[styles.AddProfile, { marginTop: 90 }]}>
                                <Text style={{ color: '#D0E8C8' }}>
                                    <Icon name='UploadPhoto' size={14} color='#D0E8C8' />
                                    {'  '}Add profile image
                            </Text>
                            </TouchableOpacity>
                        </View>

                        {/* Cover Image */}

                        <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 40, marginTop: 60 }}>
                            <TouchableOpacity activeOpacity={0.5}
                                onPress={() => this.setState({ detail: 'moreDetails' })}
                                style={styles.continue}>
                                <Text style={styles.continueText}>CONTINUE</Text>
                            </TouchableOpacity>

                            <TouchableOpacity activeOpacity={0.5}
                                onPress={() => {
                                    this.props.changeState({ skip: true })
                                    // this.props.navi
                                    // console.log(this.props.navi) 
                                }}

                                // this.props.navigation.navigate("Signup")
                                // this.setState({ testing: 'test' })

                                // }
                                style={{ width: 280, height: 39, borderRadius: 5, alignItems: 'center', backgroundColor: '#002D3D', justifyContent: 'center' }}>
                                <Text style={styles.skip}>SKIP</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>
            )
        }

        else if (this.state.detail === 'moreDetails') return (

            <ScrollView keyboardShouldPersistTaps='handled' style={this.state.modalCountry ? { flex: 1, backgroundColor: '#00394D', opacity: 0.8 } : { flex: 1, backgroundColor: '#00394D' }}>
                {/* <MyStatusBar barStyle="light-content" /> */}
                <View style={this.state.focus !== '' && Platform.OS === 'ios' ? { alignItems: 'center', height: windowHeight * 1.5 } : { alignItems: 'center' }}>

                    {/* Image Backgroung Header*/}

                    {this.state.modalCountry ? <Header text='Personalize' progress={1} opacity={0.2} /> : <Header text='Personalize' progress={1} />}

                    {/* Image Backgroung Header*/}

                    <Text style={{ fontSize: 16, color: '#C8DB6E', marginTop: 20, fontWeight: '700' }}>
                        Personalize your profile
                    </Text>
                    <Text style={{ marginTop: 5, fontSize: 12, color: '#91B3A2' }}>
                        Make your profile more visual
                    </Text>


                    {/* What defines you best Modal */}

                    <TouchableOpacity style={styles.inputBlurAddress} activeOpacity={0.5}
                        onPress={() => this.setState({ ...this.state, modalCountry: true })}>
                        {/* {console.log(this.state.modalCountry)} */}
                        <Text numberOfLines={1}
                            style={this.state.country === 'What defines you best' ? { flexWrap: 'wrap', color: "#D0E8C8" } : { flexWrap: 'wrap', color: "#C8DB6E" }}>
                            {this.state.country}
                        </Text>
                        <Icon name='Arrow2_Down' size={14} color='#91B3A2' style={{}} />
                    </TouchableOpacity>


                    <Modal visible={this.state.modalCountry}
                        onRequestClose={() => (this.setState({ modalCountry: false }))}
                        transparent={true} backdropColor="transparent" supportedOrientations={['portrait', 'landscape']}>
                        <View style={styles.modalCountry}>
                            <TouchableOpacity style={{ alignSelf: 'flex-end', width: 50, height: 50, justifyContent: 'center', alignItems: 'center', marginRight: -20, marginTop: -15 }} onPress={() => this.setState({ modalCountry: false })} >
                                <Icon name='Cross' size={16} color='#00394D' />
                            </TouchableOpacity>
                            <TextInput
                                // theme={{ colors: { text: '#D0E8C8', primary: '#C8DB6E', placeholder: '#367681' } }}
                                // mode='outlined'
                                // label="Search here"
                                // selectionColor='#C8DB6E'
                                style={[styles.inputFocus, { marginBottom: 6, width: '90%', borderRadius: 10, backgroundColor: "#00394D", color: "#D0E8C8" }]}
                                placeholder="Search here"
                                placeholderTextColor="#D0E8C8"
                                // style={[styles.inputFocus,{width:'90%',marginTop:10,borderRadius:10,backgroundColor:"#00394D",color:"#D0E8C8",marginBottom:10}]}
                                onChangeText={(value) => this.setState({ query: value })}
                                value={this.state.query}
                            />
                            <FlatList
                                keyboardShouldPersistTaps='handled'
                                data={this.findCountry(this.state.query)}
                                style={{ backgroundColor: "#00394D", width: '90%', marginBottom: 10, borderRadius: 10 }}
                                showsVerticalScrollIndicator={false}
                                keyExtractor={(item) => item}
                                renderItem={({ item }) => <Text style={{ padding: 10, color: "#D0E8C8" }}
                                    onPress={() => this.setState({ country: item, modalCountry: false })}>
                                    {item}</Text>
                                }
                            />
                        </View>
                    </Modal>

                    {/* What defines you best Modal */}

                    <View>
                        <Text style={{ fontSize: 14, color: '#D0E8C8', marginTop: 20, marginLeft: 16, fontWeight: '300' }}>
                            Add your skills
                    </Text>
                        <View style={{ width: 297, backgroundColor: '#154A59', marginTop: '3%', borderRadius: 10, paddingHorizontal: 15 }}>

                            <ScrollView keyboardShouldPersistTaps='handled' horizontal={true} showsHorizontalScrollIndicator={false}
                                ref={ref => skillScrollview = ref}
                                onContentSizeChange={() => {
                                    skillScrollview.scrollToEnd({ animated: true });
                                }}>
                                {this.modifiedSkills()}

                            </ScrollView>

                            <TextInput
                                // theme={{ colors: { text: '#D0E8C8', primary: '#C8DB6E', placeholder: '#367681' } }}
                                // mode='outlined'
                                onFocus={() => this.setState({ focus: 'skill' })}
                                onBlur={() => this.setState({ focus: '' })}
                                placeholder="botany, sadas, etc."
                                placeholderTextColor='#D0E8C8'
                                style={this.state.addSkill === '' ? { backgroundColor: "#154A59", height: 40, color: "#D0E8C8", marginTop: 10, marginBottom: 20, } : { backgroundColor: "#154A59", color: "#D0E8C8", marginTop: 30, marginBottom: 10 }}
                                onChangeText={(value) => {
                                    this.setState({ addSkill: value })
                                }}
                                value={this.state.addSkill}
                            />
                            {this.state.addSkill !== '' ?
                                <FlatList
                                    keyboardShouldPersistTaps='handled'
                                    data={this.changeSuggestion(this.state.addSkill)}
                                    style={{ backgroundColor: "#00394D", width: '100%', borderRadius: 10, borderWidth: 0.15, borderColor: '#D0E8C8' }}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={(item) => item}
                                    renderItem={({ item }) => <View style={{ marginLeft: 10, marginRight: 10 }}><Text style={{ padding: 10, color: "#D0E8C8" }}
                                        onPress={() => { this.setState({ addSkill: '' }), skills.includes(item) ? null : skills.push(item) }}>
                                        {item}</Text></View>
                                    }
                                /> : <></>}

                        </View>
                    </View>

                    <View>
                        <Text style={{ fontSize: 14, color: '#D0E8C8', marginTop: 20, marginLeft: 16, fontWeight: '300' }}>
                            Add your interests
                    </Text>
                        <View style={{ width: 297, backgroundColor: '#154A59', marginTop: '3%', borderRadius: 10, paddingHorizontal: 15 }}>

                            <ScrollView keyboardShouldPersistTaps='handled' horizontal={true} showsHorizontalScrollIndicator={false}
                                ref={ref => interestScrollview = ref}
                                onContentSizeChange={() => {
                                    interestScrollview.scrollToEnd({ animated: true });
                                }}
                            >
                                {this.modifiedInterest()}

                            </ScrollView>

                            <TextInput
                                // theme={{ colors: { text: '#D0E8C8', primary: '#C8DB6E', placeholder: '#367681' } }}
                                onFocus={() => this.setState({ focus: 'interest' })}
                                onBlur={() => this.setState({ focus: '' })}
                                placeholder="botany, sadas, etc."
                                placeholderTextColor='#D0E8C8'
                                style={this.state.addInterest === '' ? { backgroundColor: "#154A59", height: 40, color: "#D0E8C8", marginTop: 10, marginBottom: 20 } : { backgroundColor: "#154A59", color: "#D0E8C8", marginTop: 30, marginBottom: 10 }}
                                onChangeText={(value) => {
                                    this.setState({ addInterest: value })
                                }}
                                value={this.state.addInterest}
                            />
                            {this.state.addInterest !== '' ?
                                <FlatList
                                    keyboardShouldPersistTaps='handled'
                                    data={this.changeSuggestion(this.state.addInterest)}
                                    style={{ backgroundColor: "#00394D", width: '100%', borderRadius: 10, borderWidth: 0.15, borderColor: '#D0E8C8' }}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={(item) => item}
                                    renderItem={({ item }) => <View style={{ marginLeft: 10, marginRight: 10 }}><Text style={{ padding: 10, color: "#D0E8C8" }}
                                        onPress={() => { this.setState({ addInterest: '' }), interests.includes(item) ? null : interests.push(item) }}>
                                        {item}</Text></View>
                                    }
                                /> : <></>}

                        </View>
                    </View>

                    <View style={this.state.modalCountry ? { justifyContent: 'center', alignItems: 'center', marginBottom: 40, marginTop: 60, opacity: 0.2 } : { justifyContent: 'center', alignItems: 'center', marginBottom: 40, marginTop: 60 }}>
                        <TouchableOpacity activeOpacity={0.5}
                            onPress={() => this.setState({ detail: 'moreDetails' })}
                            style={styles.continue}>
                            <Text style={styles.continueText}>CONTINUE</Text>
                        </TouchableOpacity>

                    </View>

                </View>
            </ScrollView>
        )
    }
    render() {
        return this.changeScreen()
    }
}

const styles = StyleSheet.create({
    changeCover: {
        backgroundColor: '#154A59',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        borderWidth: 1,
        borderColor: '#D0E8C8',
        marginTop: 20,
        opacity: 0.8
    },
    inputBlurAddress: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        elevation: 3,
        borderWidth: 1.1,
        borderColor: '#4F7D561A',
        fontSize: 14,
        borderRadius: 6,
        backgroundColor: '#154A59',
        width: 297,
        height: 50,
        marginBottom: '2.5%',
        marginTop: '10%',
        shadowColor: "#4F7D561A",
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 2.84,
    },
    inputFocus: {
        // borderWidth: 1.1,
        // borderColor: '#BFC52E',
        padding: 10,
        fontSize: 14,
        // borderRadius: 6,
        backgroundColor: '#FFFFFF',
        // width: 300,
        height: 43,
        // marginBottom: '1.5%',
    },
    modalCountry: {
        // flex: 1,
        // backgroundColor:'#367681',
        height: '90%',
        width: '92%',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 'auto',
        marginBottom: 'auto',
        borderRadius: 15,
        padding: '2%'
    },
    AddProfile: {
        width: 208,
        height: 43,
        borderRadius: 5,
        backgroundColor: '#154A59',
        justifyContent: 'center',
        alignItems: 'center',
    },
    continue: {
        width: 280,
        height: 39,
        alignSelf: 'center',
        borderRadius: 5,
        marginTop: 25,
        marginBottom: 15,
        fontWeight: 'bold',
        backgroundColor: '#C8DB6E',
        textAlign: 'center',
        fontSize: 13,
        paddingHorizontal: 10,
        paddingVertical: 10,
        alignItems: 'center',
    },
    continueText: {
        alignSelf: 'center',
        fontWeight: 'bold',
        color: '#00394D',
        textAlign: 'center',
        fontSize: 13,
    },
    skip: {
        color: '#C8DB6E',
        fontWeight: 'bold'
    },
})