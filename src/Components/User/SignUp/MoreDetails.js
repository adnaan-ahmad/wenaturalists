import React, { Component } from 'react';
import {
  ScrollView,
  View,
  Text,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
  Modal,
  FlatList
} from 'react-native';
import Svg, {
  Image,
  Polygon,
  Defs,
  ClipPath,
} from 'react-native-svg'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import coverDefault from '../../../../assets/coverDefault.png'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import ManImage from '../../../../assets/manImage.jpeg'
import Header from './Header'
import { TextInput } from 'react-native-paper'

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class MoreDetails extends Component {

  constructor(props) {
    super(props)
    this.state = {
      suggestions: ['Javascript', 'Node', 'Express', 'Golang', 'React'],
      addSkill: ''
    }
  }

  changeSuggestion = (value) => {
    // this.state.suggestions.filter((item) => value.toUpperCase().indexOf(item !== -1))

    const regex = new RegExp(`${value.trim()}`, 'i')

    return this.state.suggestions.filter(item => (item).search(regex) >= 0)
  }
  
  render() {
    // console.log(this.props.photo)
    return (

      <ScrollView style={{ flex: 1, backgroundColor: '#00394D' }}>
        <View style={{ alignItems: 'center' }}>

          {/* Image Backgroung Header*/}

          <Header text='Personalize' progress={1} />

          {/* Image Backgroung Header*/}

          <Text style={{ fontSize: 16, color: '#C8DB6E', marginTop: 20, fontWeight: '700' }}>
            Personalize your profile
                    </Text>
          <Text style={{ marginTop: 5, fontSize: 12, color: '#91B3A2' }}>
            Make your profile more visual
                    </Text>


          <View style={{ width: 297, height: 100, backgroundColor: '#154A59', marginTop: '10%' }}>
            <TextInput
              theme={{ colors: { text: '#D0E8C8', primary: '#C8DB6E', placeholder: '#367681', fontSize: 14 } }}
              // mode='outlined'
              label="botany, sadas, etc."
              selectionColor='#C8DB6E'
              style={[styles.inputFocus, { width: '94%', backgroundColor: "#154A59", color: "#D0E8C8" }]}
              // placeholder="Search here"
              // placeholderTextColor="#D0E8C8"
              // style={[styles.inputFocus,{width:'90%',marginTop:10,borderRadius:10,backgroundColor:"#00394D",color:"#D0E8C8",marginBottom:10}]}
              onChangeText={(value) => {this.setState({addSkill: value})
              // this.changeSuggestion(value)
              }}
              value={this.state.addSkill}
            />
            {this.state.addSkill !== '' ? <FlatList
              data={this.changeSuggestion(this.state.addSkill)}
              style={{ backgroundColor: "#00394D", width: '90%', marginBottom: 10, borderRadius: 10 }}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item) => item}
              renderItem={({ item }) => <Text style={{ padding: 10, color: "#D0E8C8" }}
                onPress={() => this.setState({addSkill: ''})}>
                {item}</Text>
              }
            /> : <></>}

          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 40, marginTop: 60 }}>
            <TouchableOpacity activeOpacity={0.5}
              onPress={() => this.setState({ detail: 'moreDetails' })}
              style={styles.continue}>
              <Text style={styles.continueText}>CONTINUE</Text>
            </TouchableOpacity>

            {/* <TouchableOpacity activeOpacity={0.5}
                            onPress={() => { 
                                this.props.changeState({skip: true})
                                // this.props.navi
                                // console.log(this.props.navi) 
                            }}
                                
                                // this.props.navigation.navigate("Signup")
                                // this.setState({ testing: 'test' })
                                
                            // }
                            style={{ width: 280, height: 39, borderRadius: 5, alignItems: 'center', backgroundColor: '#002D3D', justifyContent: 'center' }}>
                            <Text style={styles.skip}>SKIP</Text>
                        </TouchableOpacity> */}
          </View>

        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  changeCover: {
    backgroundColor: '#154A59',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
    borderWidth: 1,
    borderColor: '#D0E8C8',
    marginTop: 20,
    opacity: 0.8
  },
  AddProfile: {
    width: 208,
    height: 43,
    borderRadius: 5,
    backgroundColor: '#154A59',
    justifyContent: 'center',
    alignItems: 'center',
  },
  continue: {
    width: 280,
    height: 39,
    alignSelf: 'center',
    borderRadius: 5,
    marginTop: 25,
    marginBottom: 15,
    fontWeight: 'bold',
    backgroundColor: '#C8DB6E',
    textAlign: 'center',
    fontSize: 13,
    paddingHorizontal: 10,
    paddingVertical: 10,
    alignItems: 'center',
  },
  continueText: {
    alignSelf: 'center',
    fontWeight: 'bold',
    color: '#00394D',
    textAlign: 'center',
    fontSize: 13,
  },
  skip: {
    color: '#C8DB6E',
    fontWeight: 'bold'
  },
})