import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, Dimensions, Platform } from 'react-native';
// import { ProgressBar } from 'react-native-paper';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../../../assets/Icons/selection.json'

import bg from '../../../../assets/BackgroundImg.png'
import welogo from '../../../../assets/welogo.png'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

export default class Header extends Component {

    ProgressBar = (progress) => {
        if (progress === 0) {
            return (
                <View style={{ width: '75%', height: 3, backgroundColor: '#E2E7E9', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', marginBottom: 10 }}>
                    <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#154A59' }}>

                    </View>
                    <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#E2E7E9' }}>

                    </View>
                    <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#E2E7E9' }}>

                    </View>
                    <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#E2E7E9' }}>

                    </View>
                </View>
            )
        }
        else if (progress === 1) {
            return (
                <View style={{ width: '75%', height: 3, backgroundColor: '#E2E7E9', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', marginBottom: 10 }}>

                    <View style={{ width: '33%', height: 3, justifyContent: 'space-between', alignItems: 'center', backgroundColor: '#91B3A2', flexDirection: 'row' }}>
                        <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#154A59' }}>

                        </View>
                        <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#91B3A2' }}>

                        </View>
                    </View>

                    <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#E2E7E9' }}>

                    </View>
                    <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#E2E7E9' }}>

                    </View>
                </View>
            )
        }
        else if(progress === 2){
            return (
                <View style={{ width: '75%', height: 3, backgroundColor: '#E2E7E9', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', marginBottom: 10 }}>

                    <View style={{ width: '67%', height: 3, justifyContent: 'space-between', alignItems: 'center', backgroundColor: '#91B3A2', flexDirection: 'row' }}>
                        <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#154A59' }}>

                        </View>
                        <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#367681', alignItems: 'center', justifyContent: 'center' }}>
                            <Icon name="Tick" color="#FFFFFF" size={7} style={{ marginTop: Platform.OS === 'android' ? 3 : 0 }}/>
                        </View>
                        <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#91B3A2' }}>

                        </View>
                    </View>


                    <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#E2E7E9' }}>

                    </View>
                </View>
            )
        }
        else if(progress === 3){
            return (
                <View style={{ width: '75%', height: 3, backgroundColor: '#E2E7E9', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', marginBottom: 10 }}>

                    <View style={{ width: '100%', height: 3, justifyContent: 'space-between', alignItems: 'center', backgroundColor: '#91B3A2', flexDirection: 'row' }}>
                        <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#154A59' }}>

                        </View>
                        <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#367681', alignItems: 'center', justifyContent: 'center' }}>
                            <Icon name="Tick" color="#FFFFFF" size={7} style={{ marginTop: Platform.OS === 'android' ? 3 : 0 }}/>
                        </View>
                        <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#367681', alignItems: 'center', justifyContent: 'center' }}>
                            <Icon name="Tick" color="#FFFFFF" size={7} style={{ marginTop: Platform.OS === 'android' ? 3 : 0 }}/>
                        </View>
                        <View style={{ height: 14, width: 14, borderRadius: 7, backgroundColor: '#91B3A2' }}>

                        </View>
                    </View>
                </View>
            )
        }
        else {
            return <></>
        }
    }

    render() {
        return (
            <View style={[this.props.customStyle, { width: '100%', alignItems: 'center', paddingLeft: '4%', paddingBottom: 15 }]}>
                <Image source={welogo} style={{ width: 47, height: 63, marginTop: 16, opacity: this.props.opacity }} />
                <View style={this.props.marginTop !== undefined ? {  marginTop : this.props.marginTop} : {marginTop : 20} }>
                    <Text style={{
                        fontSize: 22,
                        fontStyle: 'italic',
                        color: '#00394D',
                        marginBottom: '3%',
                        opacity: this.props.opacity,
                        marginBottom: 20,
                        // fontFamily:'Montserrat-Medium'
                    }}>{this.props.text}</Text>
                </View>

                {this.ProgressBar(this.props.progress)}

            </View>
        )
    }
}
