import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

import Snackbar from 'react-native-snackbar';
import {cloneDeep} from 'lodash';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultProfile from '../../../../assets/defaultProfile.png';
import {COLORS} from '../../Shared/Colors';
import typography from '../../Shared/Typography';
import ConnectDepth from '../Common/ConnectDepth';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class ViewVoters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      page: 0,
      votersList: [],
      isCompany: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        value &&
          this.setState(
            {userId: value},
            // , () => this.getUsersWhoLiked()
          );
      })
      .catch((e) => {
        console.log(e);
      });

    AsyncStorage.getItem('userData')
      .then((value) => {
        let objValue = JSON.parse(value);
        objValue.type === 'COMPANY' && this.setState({isCompany: true});
      })
      .catch((e) => {
        console.log(e);
      });

    this.setState({votersList: this.props.votersList});
  }

  componentWillUnmount() {
    this.setState({votersList: []});
  }

  handleFollowUnfollow = (isFollowed, userId, index, item) => {
    let tempLikeList = cloneDeep(this.state.votersList);
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        console.log(response.status);
        if (response && response.status === 202) {
          tempLikeList[index].followed = !item.followed;
          this.setState({votersList: tempLikeList});
        } else {
          // tempLikeList[index].followed = !item.followed
          // this.setState({ votersList: tempLikeList })
        }
      })
      .catch((err) => {
        console.log(err);
        // tempLikeList[index].followed = !item.followed
        // this.setState({ votersList: tempLikeList })
      });
  };

  handleConnectStatusChange = (userId) => {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/invite/' +
        userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          // console.log(response.status)
          // this.setState({ page: 0, peopleShared: [] }, () => this.getUsersWhoShared())
        }
      })
      .catch((err) => {
        // console.log(err)
        if (err.message === 'Request failed with status code 409') {
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'You can send connection request after 3 days to this member',
            duration: Snackbar.LENGTH_LONG,
          });
        }
        if (err.message === 'Request failed with status code 400') {
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Cannot send request to an organization',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
  }

  render() {
    return (
      <View style={{width: '100%'}}>
        <FlatList
          keyboardShouldPersistTaps="handled"
          showsVerticalScrollIndicator={false}
          style={{height: '60%', width: '100%', marginTop: 6}}
          keyExtractor={(item) => item.id}
          data={this.state.votersList}
          renderItem={({item, index}) => (
            <View
              style={{
                alignItems: 'flex-start',
                alignSelf: 'flex-start',
                width: '96%',
                marginTop: 12,
              }}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({likeModalOpen: false}, () => {
                    this.state.userId === item.connectStatus.profileId &&
                    item.userType === 'COMPANY'
                      ? this.props.navigation('ProfileStack', {
                          screen: 'CompanyProfileScreen',
                        })
                      : item.userType === 'COMPANY'
                      ? this.props.navigation('ProfileStack', {
                          screen: 'CompanyProfileScreen',
                          params: {userId: item.connectStatus.profileId},
                        })
                      : this.state.userId === item.connectStatus.profileId
                      ? this.props.navigation('ProfileStack', {
                          screen: 'ProfileScreen',
                        })
                      : this.props.navigation('ProfileStack', {
                          screen: 'OtherProfileScreen',
                          params: {userId: item.connectStatus.profileId},
                        });
                  })
                }
                activeOpacity={0.6}
                style={{
                  flexDirection: 'row',
                  // height: 44,
                  alignItems: 'center',
                }}>
                <Image
                  source={
                    item.originalProfileImage
                      ? {uri: item.originalProfileImage}
                      : defaultProfile
                  }
                  style={{
                    marginLeft: '7%',
                    marginRight: 10,
                    width: 32,
                    height: 32,
                    borderRadius: 16,
                  }}
                />
                <View
                  style={{
                    alignItems: 'flex-start',
                    justifyContent: 'center',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Button_Lead,
                        {
                          color: COLORS.dark_600,
                          textTransform: 'capitalize',
                          maxWidth: 190,
                        },
                      ]}>
                      {item.id === this.state.userId ? 'You' : item.username}
                    </Text>
                    {item.connectDepth ? (
                      <ConnectDepth connectDepth={item.connectDepth} />
                    ) : (
                      <Text></Text>
                    )}
                  </View>

                  {item.id !== this.state.userId && (
                    <>
                      {item.persona ? (
                        <Text
                          style={[
                            typography.Note2,
                            {color: COLORS.altgreen_400},
                          ]}>
                          {item.persona}
                        </Text>
                      ) : null}
                      {item.addressDetail && item.addressDetail.country ? (
                        <Text
                          style={[
                            typography.Note2,
                            {color: COLORS.altgreen_400},
                          ]}>
                          {item.addressDetail &&
                            item.addressDetail.country &&
                            item.addressDetail.country}
                        </Text>
                      ) : null}
                    </>
                  )}
                </View>
              </TouchableOpacity>

              {item.id !== this.state.userId && (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    position: 'absolute',
                    right: 0,
                  }}>
                  {item.connectStatus.connectStatus === 'REQUEST_RECEIVED' && (
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation('NetworkInvitationStack', {})
                      }
                      style={{
                        height: 30,
                        width: 30,
                        alignItems: 'center',
                        marginRight: 6,
                      }}>
                      <Icon
                        name="TickedUser"
                        size={13}
                        color={COLORS.dark_600}
                        style={{
                          marginTop: Platform.OS === 'android' ? 5 : 0,
                        }}
                      />
                    </TouchableOpacity>
                  )}

                  {item.connectStatus.connectStatus === 'NOT_CONNECTED' &&
                    item.userType !== 'COMPANY' &&
                    !this.state.isCompany && (
                      <TouchableOpacity
                        onPress={() => this.handleConnectStatusChange(item.id)}
                        style={{
                          height: 30,
                          width: 30,
                          alignItems: 'center',
                          marginRight: 6,
                        }}>
                        <Icon
                          name="AddUser"
                          size={13}
                          color={COLORS.dark_600}
                          style={{
                            marginTop: Platform.OS === 'android' ? 5 : 0,
                          }}
                        />
                      </TouchableOpacity>
                    )}

                  {item.connectStatus.connectStatus === 'PENDING_CONNECT' &&
                    item.connectDepth === 2 && (
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation('MyNetworkStack', {
                            screen: 'YourRequests',
                          })
                        }
                        style={{
                          height: 30,
                          width: 30,
                          alignItems: 'center',
                          marginRight: 6,
                        }}>
                        <Icon
                          name="TickedUser"
                          size={13}
                          color={COLORS.dark_600}
                          style={{
                            marginTop: Platform.OS === 'android' ? 5 : 0,
                          }}
                        />
                      </TouchableOpacity>
                    )}

                  {this.props.userType !== 'COMPANY' && (
                    <TouchableOpacity
                      onPress={() =>
                        this.handleFollowUnfollow(
                          item.followed,
                          item.id,
                          index,
                          item,
                        )
                      }
                      style={{
                        height: 30,
                        width: 30,
                        alignItems: 'center',
                        marginLeft: 2,
                      }}>
                      <Icon
                        name={item.followed ? 'TickRSS' : 'RSS'}
                        size={13}
                        color={COLORS.dark_600}
                        style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
                      />
                    </TouchableOpacity>
                  )}

                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation('Chats', {
                        userId: this.state.userId,
                        otherUserId: item.id,
                        lastActive: null,
                        grpType: 'Private',
                        name: item.username,
                        otherUserProfile: item.originalProfileImage
                          ? item.originalProfileImage
                          : null,
                      })
                    }
                    style={{
                      height: 30,
                      width: 30,
                      alignItems: 'center',
                      marginLeft: 2,
                    }}>
                    <Icon
                      name="WN_Messeges_OL"
                      size={13}
                      color={COLORS.dark_600}
                      style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
                    />
                  </TouchableOpacity>
                </View>
              )}
            </View>
          )}
        />
      </View>
    );
  }
}
