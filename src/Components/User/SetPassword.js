import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, TouchableHighlight, ScrollView, Keyboard } from 'react-native'
import wenat from '../../../assets/we-naturalists.png'
import Snackbar from 'react-native-snackbar'
import Icon from 'react-native-vector-icons/MaterialIcons'
class SetPassword extends Component {

  render() {
    return (
      <ScrollView keyboardDismissMode="on-drag" keyboardShouldPersistTaps='handled' showsVerticalScrollIndicator={false} style={Platform.OS === 'ios' ? [{ marginTop: 40 }, styles.container] : styles.container}>

        <View style={styles.mainSection}>
          <Image style={styles.logo} source={wenat} />
          <View style={styles.headers}><Text style={styles.header}>Set Password</Text>
          </View>

          <View style={styles.form}>
          <View style={styles.searchSection}>

            <TextInput
              style={this.state.focus === 'password' ? styles.inputFocus : styles.inputBlur}
              onFocus={() => { this.changeState({ focus: 'password' }) }}
              onBlur={() => { this.changeState({ focus: '' }) }}
              placeholder='Enter Password'
              onChangeText={(value) => this.changeState({ password: value })}
              secureTextEntry={this.state.passwordIcon === 'visibility' ? false : true }
            />
            <Icon style={styles.icon}
              name={this.state.passwordIcon}
              size={20}
              color='#00394D'
              onPress={() => {
                this.state.passwordIcon === 'visibility' ? this.changeState({ passwordIcon: 'visibility-off' }) : this.changeState({ passwordIcon: 'visibility' })
              }}
            />
            </View>

            <View style={styles.searchSection}>
            <TextInput
              style={this.state.focus === 'reEnterPassword' ? styles.inputFocus : styles.inputBlur}
              onFocus={() => { this.changeState({ focus: 'reEnterPassword' }) }}
              onBlur={() => { this.changeState({ focus: '' }) }}
              placeholder='Re-enter Password'
              onChangeText={(value) => this.changeState({ reEnterPassword: value })}
              secureTextEntry={this.state.reEnterPasswordIcon === 'visibility' ? false : true }
            />
            <Icon style={styles.icon}
              name={this.state.reEnterPasswordIcon}
              size={20}
              color='#00394D'
              onPress={() => {
                this.state.reEnterPasswordIcon === 'visibility' ? this.changeState({ reEnterPasswordIcon: 'visibility-off' }) : this.changeState({ reEnterPasswordIcon: 'visibility' })
              }}
            />
            </View>
            <TouchableHighlight underlayColor="#00394dba" style={styles.logIn} onPress={() => {

              if (this.state.password.length < 6) {
                Snackbar.show({
                  backgroundColor: '#B22222',
                  text: "Password should contain min of 6 chars",
                  duration: Snackbar.LENGTH_LONG,
                })
              }
              else if (this.state.password !== this.state.reEnterPassword) {
                Snackbar.show({
                  backgroundColor: '#B22222',
                  text: "Passwords doesn't match",
                  duration: Snackbar.LENGTH_LONG,
                })
              }
              else this.changeState({ display: '' })
              // this.changeState({ display: '' })
              // Keyboard.dismiss()
            }}>
              <Text style={styles.logInText}>CONFIRM</Text>
            </TouchableHighlight>
          </View>

          <TouchableOpacity onPress={() => { this.changeState({ display: 'SignUp' }), this.changeState({ modalOpen: false }) }}>
            <Text style={styles.signUp}>BACK</Text>
          </TouchableOpacity>

        </View>
        <View style={styles.footer}>

        </View>

      </ScrollView>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  mainSection: {
    alignItems: 'center',
  },
  searchSection: {
    flexDirection: 'row',
  },
  icon:{
    padding:10,
    marginLeft : -45,
  },
  footer: {
    backgroundColor: '#00394D',
    alignItems: 'center',
    alignSelf: 'stretch',
    height: 118,
    marginTop: 30,
  },
  logo: {
    width: 215,
    height: 80,
    marginTop: 75
  },
  header: {
    fontSize: 20,
    fontStyle: 'italic',
    color: '#00394D',
    fontWeight: '700',
    marginTop: '15%',
    lineHeight: 24,
    marginTop: 30
  },
  form: {
    marginTop: '10%'
  },
  inputFocus: {
    borderWidth: 1.1,
    borderColor: '#BFC52E',
    padding: 10,
    fontSize: 14,
    borderRadius: 6,
    backgroundColor: '#FFFFFF',
    width: 280,
    height: 43,
    marginBottom: '1.5%',
  },
  inputBlur: {
    elevation: 3,
    borderWidth: 1.1,
    borderColor: '#4F7D561A',
    padding: 10,
    fontSize: 14,
    borderRadius: 6,
    backgroundColor: '#FFFFFF',
    width: 280,
    height: 43,
    marginBottom: '1.5%',
    shadowColor: "#4F7D561A",
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2.84,
  },
  logIn: {
    width: 150,
    height: 39,
    alignSelf: 'center',
    borderRadius: 5,
    marginTop: 25,
    marginBottom: 24,
    fontWeight: 'bold',
    backgroundColor: '#154A59',
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 13,
    paddingHorizontal: 10,
    paddingVertical: 10,
    alignItems: 'center',
  },
  logInText: {
    alignSelf: 'center',
    fontWeight: 'bold',
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 13,
  },
  signUp: {
    color: '#00394D',
    marginTop: '5%',
    fontWeight: 'bold'
  },
  forgotPassword: {
    color: '#698F8A',
    alignSelf: 'flex-end',
    fontSize: 14,
    fontWeight: '700',
    marginTop: '20%',
  },
});

export default SetPassword
