import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Modal,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import LinearGradient from 'react-native-linear-gradient';

import defaultShape from '../../../Components/Shared/Shape';
import typography from '../../../Components/Shared/Typography';
import {COLORS} from '../../Shared/Colors';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class ForumHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 'ALL',
      filterModalOpen: false,
      sortBy: 'recent',
    };
  }

  filterModal = () => {
    return (
      <Modal
        visible={this.state.filterModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({filterModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <Text
            style={[
              typography.Caption,
              {
                fontSize: 12,
                color: COLORS.dark_500,
                textAlign: 'center',
                alignSelf: 'center',
                position: 'absolute',
                bottom: 110,
                zIndex: 2,
              },
            ]}>
            Sort By
          </Text>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
                flexDirection: 'row',
              },
            ]}>
            <TouchableOpacity
              style={
                this.state.sortBy === 'recent'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({filterModalOpen: false, sortBy: 'recent'});
                this.props.changeState({
                  selectedTrending: false,
                  forumData: [],
                  page: 0,
                });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'recent'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Recent
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                this.state.sortBy === 'trending'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({filterModalOpen: false, sortBy: 'trending'});
                this.props.changeState({
                  selectedTrending: true,
                  forumData: [],
                  page: 0,
                });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'trending'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Trending
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }

  render() {
    return (
      <SafeAreaView style={styles.rowView}>
        {this.filterModal()}

        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() => {
              this.props.changeState({selectedTab: '', forumData: [], page: 0});
              this.setState({selectedTab: 'ALL'});
            }}
            activeOpacity={0.5}
            style={
              this.props.selectedTab === ''
                ? styles.selectedTab
                : styles.unselectedTab
            }>
            <Text
              style={
                this.props.selectedTab === ''
                  ? [typography.Title_2, styles.selectedTabText]
                  : [typography.Title_2, styles.unselectedTabText]
              }>
              All FORUMS
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.changeState({
                selectedTab: 'MYFORUM',
                forumData: [],
                page: 0,
                selectedTrending: false,
              });
              this.setState({selectedTab: 'MYFORUM'});
            }}
            activeOpacity={0.5}
            style={
              this.props.selectedTab === 'MYFORUM'
                ? styles.selectedTab
                : styles.unselectedTab
            }>
            <Text
              style={
                this.props.selectedTab === 'MYFORUM'
                  ? [typography.Title_2, styles.selectedTabText]
                  : [typography.Title_2, styles.unselectedTabText]
              }>
              My FORUM
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.changeState({
                selectedTab: 'PINNED',
                forumData: [],
                page: 0,
                selectedTrending: false,
              });
              this.setState({selectedTab: 'PINNED'});
            }}
            activeOpacity={0.5}
            style={
              this.props.selectedTab === 'PINNED'
                ? styles.selectedTab
                : styles.unselectedTab
            }>
            <Text
              style={
                this.props.selectedTab === 'PINNED'
                  ? [typography.Title_2, styles.selectedTabText]
                  : [typography.Title_2, styles.unselectedTabText]
              }>
              Pinned
            </Text>
          </TouchableOpacity>
        </View>

        {this.props.selectedTab === '' && (
          <TouchableOpacity
            onPress={() => this.setState({filterModalOpen: true})}>
            <Icon
              name="FilterSlide"
              size={18}
              color={COLORS.altgreen_400}
              style={{
                marginTop: Platform.OS === 'android' ? -7 : 0,
                marginRight: 15,
              }}
            />
          </TouchableOpacity>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  rowView: {
    flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'space-between',
  },
  selectedTab: {
    borderBottomColor: COLORS.dark_800,
    borderBottomWidth: 4,
    paddingBottom: 4,
    marginLeft: 15,
  },
  unselectedTab: {
    marginLeft: 15,
  },
  selectedTabText: {
    color: COLORS.dark_800,
  },
  unselectedTabText: {
    color: COLORS.altgreen_300,
  },
  unselectedText: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLORS.altgreen_300,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginLeft: 15,
  },
  selectedText: {
    backgroundColor: COLORS.dark_700,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginLeft: 15,
  },
});
