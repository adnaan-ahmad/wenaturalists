import React, {Component} from 'react';
import {View, Text} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

import {COLORS} from './Colors';

export default class SkeletonLoader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <SkeletonPlaceholder backgroundColor={COLORS.grey_300}>
        <View style={{flexDirection: 'row', marginTop: 30, marginLeft: 15}}>
          <View style={{height: 120, width: 100, borderRadius: 6}}></View>
          <View style={{marginLeft: 10}}>
            <View style={{height: 20, width: 100, borderRadius: 6}}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
          </View>
        </View>
        <View
          style={{
            height: 20,
            borderRadius: 6,
            marginTop: 10,
            marginLeft: 15,
          }}></View>

        <View style={{flexDirection: 'row', marginTop: 30, marginLeft: 15}}>
          <View style={{height: 120, width: 100, borderRadius: 6}}></View>
          <View style={{marginLeft: 10}}>
            <View style={{height: 20, width: 100, borderRadius: 6}}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
          </View>
        </View>
        <View
          style={{
            height: 20,
            borderRadius: 6,
            marginTop: 10,
            marginLeft: 15,
          }}></View>

        <View style={{flexDirection: 'row', marginTop: 30, marginLeft: 15}}>
          <View style={{height: 120, width: 100, borderRadius: 6}}></View>
          <View style={{marginLeft: 10}}>
            <View style={{height: 20, width: 100, borderRadius: 6}}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
          </View>
        </View>
        <View
          style={{
            height: 20,
            borderRadius: 6,
            marginTop: 10,
            marginLeft: 15,
          }}></View>

        <View style={{flexDirection: 'row', marginTop: 30, marginLeft: 15}}>
          <View style={{height: 120, width: 100, borderRadius: 6}}></View>
          <View style={{marginLeft: 10}}>
            <View style={{height: 20, width: 100, borderRadius: 6}}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
          </View>
        </View>
        <View
          style={{
            height: 20,
            borderRadius: 6,
            marginTop: 10,
            marginLeft: 15,
          }}></View>

        <View style={{flexDirection: 'row', marginTop: 30, marginLeft: 15}}>
          <View style={{height: 120, width: 100, borderRadius: 6}}></View>
          <View style={{marginLeft: 10}}>
            <View style={{height: 20, width: 100, borderRadius: 6}}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
          </View>
        </View>
        <View
          style={{
            height: 20,
            borderRadius: 6,
            marginTop: 10,
            marginLeft: 15,
          }}></View>

        <View style={{flexDirection: 'row', marginTop: 30, marginLeft: 15}}>
          <View style={{height: 120, width: 100, borderRadius: 6}}></View>
          <View style={{marginLeft: 10}}>
            <View style={{height: 20, width: 100, borderRadius: 6}}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
            <View
              style={{
                height: 40,
                width: 300,
                borderRadius: 6,
                marginTop: 10,
              }}></View>
          </View>
        </View>
        <View
          style={{
            height: 20,
            borderRadius: 6,
            marginTop: 10,
            marginLeft: 15,
          }}></View>
      </SkeletonPlaceholder>
    );
  }
}
