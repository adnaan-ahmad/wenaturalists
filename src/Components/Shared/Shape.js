import { ColorPropType, StyleSheet } from 'react-native'
import { COLORS } from './Colors'

const defaultShape = StyleSheet.create({
    card4_a: {
        width: 150,
        height: 150,
        backgroundColor: COLORS.white,
        borderColor: COLORS.grey_300,
        borderWidth: 1,
        borderRadius: 4,
        shadowColor: COLORS.dark_800_t50,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
    },
    card4_b: {
        width: 150,
        height: 150,
        backgroundColor: COLORS.white,
        borderColor: COLORS.grey_300,
        borderWidth: 1,
        borderRadius: 4,
        shadowColor: COLORS.dark_800_t50,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    card4_c: {
        width: 150,
        height: 150,
        backgroundColor: COLORS.white,
        borderColor: COLORS.grey_300,
        borderWidth: 1,
        borderRadius: 4,
        shadowColor: COLORS.dark_800_t50,
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 12,
    },
    card8_a: {
        // width: 150,
        // height: 150,

        backgroundColor: COLORS.white,
        borderColor: COLORS.grey_300,
        borderWidth: 1,
        borderRadius: 8,
        shadowColor: COLORS.dark_800_t50,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,

        elevation: 1,
    },
    card8_b: {
        width: 150,
        height: 150,
        backgroundColor: COLORS.white,
        borderColor: COLORS.grey_300,
        borderWidth: 1,
        borderRadius: 8,
        shadowColor: COLORS.dark_800_t50,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    card8_c: {
        width: 150,
        height: 150,
        backgroundColor: COLORS.white,
        borderColor: COLORS.grey_300,
        borderWidth: 1,
        borderRadius: 8,
        shadowColor: COLORS.dark_800_t50,
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 12,
    },
    card16_a: {
        width: 150,
        height: 150,
        backgroundColor: COLORS.white,
        borderColor: COLORS.grey_300,
        borderWidth: 1,
        borderRadius: 16,
        shadowColor: COLORS.dark_800_t50,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,

        elevation: 1,
    },
    card16_b: {
        width: 150,
        height: 150,
        backgroundColor: COLORS.white,
        borderColor: COLORS.grey_300,
        borderWidth: 1,
        borderRadius: 16,
        shadowColor: COLORS.dark_800_t50,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    card16_c: {
        width: 150,
        height: 150,
        backgroundColor: COLORS.white,
        borderColor: COLORS.grey_300,
        borderWidth: 1,
        borderRadius: 16,
        shadowColor: COLORS.dark_800_t50,
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 12,
    },
    Input_Overlay: {
        width: '80%',
        height: 43,
        backgroundColor: COLORS.dark_800_t25,
        borderRadius: 6,
        paddingLeft: 12
    },
    Input_Small: {
        width: '65%',
        height: 27,
        backgroundColor: COLORS.darkershade,
        borderRadius: 4,
        padding: 6,
        paddingLeft: 10,
        borderWidth: 0.5,
        borderColor: COLORS.dark_600
    },
    Search_Nav: {
        width: '80%',
        height: 36,
        backgroundColor: COLORS.bgfill,
        borderRadius: 4,
        justifyContent: 'center',
        paddingLeft: 12,
        borderWidth: 1,
        borderColor: COLORS.grey_200
    },
    Search_Cell: {
        width: '80%',
        height: 40,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        textAlign: 'center',
        borderWidth: 1,
        borderColor: COLORS.grey_300
    },
    Search_Float: {
        width: '80%',
        height: 40,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        textAlign: 'center',
        borderWidth: 1,
        borderColor: COLORS.grey_300,
        shadowColor: COLORS.dark_800_t50,
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    SimpleCell: {
        width: '100%',
        height: 44,
        backgroundColor: COLORS.white,
        paddingLeft: 16,
        justifyContent: 'center'
    },
    SimpleCell_Close: {
        width: '100%',
        height: 44,
        backgroundColor: COLORS.white,
        paddingHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    Media_Round: {
        width: 32,
        height: 32,
        borderRadius: 16
    },
    Media_Squared: {
        width: 44,
        height: 44
    },


    FullwidthBtn_Primary: {
        width: '80%',
        height: 50,
        borderRadius: 27,
        backgroundColor: COLORS.dark_800,
        justifyContent: 'center',
        alignItems: 'center'
    },
    FullwidthBtn_Primary_Lead: {
        width: '80%',
        height: 50,
        borderRadius: 27,
        backgroundColor: COLORS.dark_800,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    FullwidthBtn_Secondary: {
        width: '80%',
        height: 50,
        borderRadius: 27,
        backgroundColor: COLORS.grey_200,
        justifyContent: 'center',
        alignItems: 'center'
    },
    FullwidthBtn_Secondary_Lead: {
        width: '80%',
        height: 50,
        borderRadius: 27,
        backgroundColor: COLORS.grey_200,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    FullwidthBtn_Counter: {
        width: '80%',
        height: 50,
        borderRadius: 27,
        backgroundColor: COLORS.NoColor,
        justifyContent: 'center',
        alignItems: 'center'
    },
    FullwidthBtn_Counter_Lead: {
        width: '80%',
        height: 50,
        borderRadius: 27,
        backgroundColor: COLORS.NoColor,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    AdpWidthBtn_Primary: {
        width: 117,
        height: 38,
        borderRadius: 20,
        backgroundColor: COLORS.dark_600,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdpWidthBtn_Secondary: {
        width: 117,
        height: 38,
        borderRadius: 20,
        backgroundColor: COLORS.grey_300,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdpWidthBtn_Counter: {
        width: 117,
        height: 38,
        borderRadius: 20,
        backgroundColor: COLORS.NoColor,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdpWidthBtn_Primary_Drk: {
        width: 117,
        height: 38,
        borderRadius: 20,
        backgroundColor: COLORS.altgreen_200,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdpWidthBtn_Secondary_Drk: {
        width: 117,
        height: 38,
        borderRadius: 20,
        backgroundColor: COLORS.dark_600,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdpWidthBtn_Counter_Drk: {
        width: 117,
        height: 38,
        borderRadius: 20,
        backgroundColor: COLORS.NoColor,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ContextBtn_OL: {
        // width: 75,
        paddingHorizontal: 10,
        paddingVertical: 5,
        // height: 28,
        borderRadius: 20,
        backgroundColor: COLORS.NoColor,
        borderWidth: 1,
        borderColor: COLORS.altgreen_300,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ContextBtn_FL: {
        // width: 75,
        paddingHorizontal: 10,
        paddingVertical: 5,
        // height: 28,
        borderRadius: 17,
        backgroundColor: COLORS.altgreen_100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ContextBtn_OL_Drk: {
        paddingHorizontal: 10,
        height: 28,
        borderRadius: 20,
        backgroundColor: COLORS.NoColor,
        borderWidth: 1,
        borderColor: COLORS.dark_600,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ContextBtn_FL_Drk: {
        paddingHorizontal: 10,
        height: 28,
        borderRadius: 17,
        backgroundColor: COLORS.dark_700,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ContentSlideCtrl_Right: {
        height: 55,
        width: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30
    },
    ContentSlideCtrl_Left: {
        height: 55,
        width: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30
    },
    AdjunctBtn_Label_Sec: {
        width: 86,
        height: 30,
        borderRadius: 17,
        backgroundColor: COLORS.grey_200,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdjunctBtn_Small_Sec: {
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: COLORS.grey_200,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdjunctBtn_Sec: {
        width: 42,
        height: 42,
        borderRadius: 21,
        backgroundColor: COLORS.grey_200,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdjunctBtn_Label_Prim: {
        width: 86,
        height: 30,
        borderRadius: 17,
        backgroundColor: COLORS.altgreen_100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdjunctBtn_Small_Prim: {
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: COLORS.altgreen_100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdjunctBtn_Prim: {
        width: 42,
        height: 42,
        borderRadius: 21,
        backgroundColor: COLORS.altgreen_100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdjunctBtn_Label_Blurred: {
        width: 120,
        height: 30,
        borderRadius: 17,
        backgroundColor: COLORS.altgreen_t50,
        opacity: 0.8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdjunctBtn_Small_Blurred: {
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: COLORS.altgreen_t50,
        opacity: 0.8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    AdjunctBtn_Blurred: {
        width: 42,
        height: 42,
        borderRadius: 21,
        backgroundColor: COLORS.altgreen_t50,
        opacity: 0.8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ActionBtn_Prim: {
        width: 128,
        height: 38,
        borderRadius: 6,
        backgroundColor: COLORS.green_500,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ActionBtn_Sec: {
        width: 88,
        height: 38,
        borderRadius: 4,
        backgroundColor: COLORS.NoColor,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ActionBtn_Gylph: {
        width: 37,
        height: 28,
        borderRadius: 4,
        backgroundColor: COLORS.green_400
    },
    Nav_GylphType_Sec: {
        width: 114,
        height: 44,
        backgroundColor: COLORS.white,
        paddingRight: 12,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    Nav_Gylph_Btn: {
        height: 44,
        width: 44,
        justifyContent: 'center',
        alignItems: 'center'
    },
    Nav_Image_Round_Type: {
        width: 162,
        height: 44,
        paddingLeft: 12,
        backgroundColor: COLORS.white,
        flexDirection: 'row',
        alignItems: 'center'
    },
    Nav_Type2: {
        // width: 134,
        height: 44,
        // paddingLeft: 12,
        // paddingTop: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    NavTabs: {
        width: '100%',
        height: 44,
        backgroundColor: COLORS.white,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    NavTab_Button: {
        width: 114,
        height: 5,
        backgroundColor: COLORS.dark_800,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4
    },
    SegmentedTab: {
        backgroundColor: COLORS.altgreen_t50,
        width: '80%',
        height: 28,
        borderRadius: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    InTab_Btn: {
        // height: '100%',
        paddingVertical: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    CloseBtn: {
        width: 42,
        height: 42,
        borderRadius: 21,
        backgroundColor: COLORS.altgreen_100,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 12
    },
    Linear_Gradient_View: {
        width: '100%',
        height: 500,
        position: 'absolute',
        bottom: 150,
        alignSelf: 'center'
    },
    Linear_Gradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6
    },
    Modal_Categories_Container: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: COLORS.white,
        alignItems: 'center',
        paddingVertical: 20
    },
    ActList_Cell_Gylph_Alt: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '85%',
        paddingVertical: 5.5,
        borderBottomWidth: 0.55,
        borderBottomColor: COLORS.grey_200
    }
})

export default defaultShape
