export const trimDescription = (item) => {
  var trimmed = item.split('^^__').join(' ').indexOf('@@@__');
  var trimmedHash = item.split('&&__').join(' ').indexOf('###__');

  var str = item
    .split('^^__')
    .join(' ')
    .indexOf(' ', item.split('^^__').join(' ').indexOf('@@@__'));

  var strHash = item
    .split('&&__')
    .join(' ')
    .indexOf(' ', item.split('&&__').join(' ').indexOf('###__'));

  var sub = item.substring(trimmed, str);
  var subHash = item.substring(trimmed, strHash);

  item = item.replace(' ' + sub, '');
  item = item.replace(' ' + subHash, '');
  item = item.replace('@@@^^^', ' ');
  item = item.replace('@@@__', ' ');
  item = item.replace('###^^^', ' ');
  item = item.replace('###__', ' ');
  item = item.replace('  ', '');

  item = item.replace(/&nbsp;/g, ' ');
  item = item.replace(/<br\s*[\/]?>/gi, '\n');

  const regex = /(<([^>]+)>)/gi;
  item = item.replace(regex, '');

  var final = item.split('^^__').join(' ');
  final = final.split('&&__').join(' ');
  return final;
};

// mention auto link
export const tagDescription = (item) => {
  item = item.split('@@@__').join('@[');
  item = item.split('^^__').join('](');
  item = item.split('@@@^^^').join(')');
  item = item.split('###__').join('#[');
  item = item.split('&&__').join('](');
  item = item.split('###^^^').join(')');
  return item;
};

export const unixTime = (UNIX_timestamp) => {
  var date = new Date(UNIX_timestamp);
  var months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  var year = date.getFullYear();
  var month = months[date.getMonth()];
  var day = date.getDate();
  var hour = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();

  return day + ' ' + month + ' ' + year;
};

export const unixTime2 = (UNIX_timestamp) => {
  var date = new Date(UNIX_timestamp);
  var months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  var year = date.getFullYear();
  var month = months[date.getMonth()];
  var day = date.getDate();
  var time = date.getTime();

  var todaysDate = new Date();
  var time2 = todaysDate.getTime();

  var difference = Math.floor((time2 - time) / 1000);

  if (difference < 60) {
    return difference + 's';
  }

  if (difference > 59 && difference < 3600) {
    return Math.floor(difference / 60) + 'm';
  }

  if (difference >= 3600 && difference < 86400) {
    return Math.floor(difference / 3600) + 'h';
  }

  if (difference >= 86400 && difference < 864000) {
    return Math.floor(difference / 72000) + 'd';
  }

  if (difference >= 864000) {
    return day + ' ' + month + ' ' + year;
  }
};

export const validateEmail = (e) => {
  let filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
  return String(e).search(filter) != -1;
};

export const nFormatter = (num, digits) => {
  if (!num) {
    return num;
  }
  var si = [
    {value: 1, symbol: ''},
    {value: 1e3, symbol: 'k'},
    {value: 1e6, symbol: 'M'},
    {value: 1e9, symbol: 'G'},
    {value: 1e12, symbol: 'T'},
    {value: 1e15, symbol: 'P'},
    {value: 1e18, symbol: 'E'},
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (num / si[i].value).toFixed(digits).replace(rx, '$1') + si[i].symbol;
};
