import { StyleSheet } from 'react-native';

const defaultStyle = StyleSheet.create({
    H1: {
        fontSize: 52,
        fontFamily: 'Montserrat-Bold',
        letterSpacing: 0,
        lineHeight: 64
    },
    H2: {
        fontSize: 36,
        fontFamily: 'Montserrat-Medium',
        letterSpacing: 0,
        lineHeight: 40
    },
    H3: {
        fontSize: 28,
        fontFamily: 'Montserrat-Bold',
        letterSpacing: 0,
        lineHeight: 34
    },
    H4: {
        fontSize: 18,
        fontFamily: 'Montserrat-Medium',
        letterSpacing: 0,
        lineHeight: 24
    },
    H5: {
        fontSize: 16,
        fontFamily: 'Montserrat-Medium',
        letterSpacing: 0,
        lineHeight: 20
    },
    H6: {
        fontSize: 14,
        fontFamily: 'Montserrat-Bold',
        letterSpacing: 0,
        lineHeight: 18
    },
    Title_S: {
        fontSize: 22,
        fontFamily: 'Montserrat-MediumItalic',
        letterSpacing: 0,
        lineHeight: 28
    },
    Title_1: {
        fontSize: 20,
        fontFamily: 'Montserrat-Bold',
        letterSpacing: 0,
        lineHeight: 24
    },
    Title_2: {
        fontSize: 14,
        fontFamily: 'Montserrat-Bold',
        letterSpacing: 0,
        lineHeight: 18
    },
    Subtitle_1: {
        fontSize: 14,
        fontFamily: 'Montserrat-Medium',
        letterSpacing: 0,
        lineHeight: 18
    },
    Subtitle_2: {
        fontSize: 12,
        fontFamily: 'Montserrat-Medium',
        letterSpacing: 0,
        lineHeight: 16
    },
    Body_1: {
        fontSize: 12,
        fontFamily: 'Montserrat-Medium',
        letterSpacing: 0,
        lineHeight: 18
    },
    Body_1_italic: {
        fontSize: 12,
        fontFamily: 'Montserrat-MediumItalic',
        letterSpacing: 0,
        lineHeight: 18
    },
    Body_1_bold: {
        fontSize: 12,
        fontFamily: 'Montserrat-Bold',
        letterSpacing: 0,
        lineHeight: 18
    },
    Body_1_boldItalic: {
        fontSize: 12,
        fontFamily: 'Montserrat-BoldItalic',
        letterSpacing: 0,
        lineHeight: 18
    },
    Body_2: {
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
        letterSpacing: 0,
        lineHeight: 18
    },
    Body_2_italic: {
        fontSize: 12,
        fontFamily: 'Roboto-Italic',
        letterSpacing: 0,
        lineHeight: 18
    },
    Body_2_bold: {
        fontSize: 12,
        fontFamily: 'Roboto-Bold',
        letterSpacing: 0,
        lineHeight: 18
    },
    Body_2_boldItalic: {
        fontSize: 12,
        fontFamily: 'Roboto-BoldItalic',
        letterSpacing: 0,
        lineHeight: 18
    },
    Button_1: {
        fontSize: 16,
        fontFamily: 'Montserrat-Bold',
        letterSpacing: 0,
        lineHeight: 18
    },
    Button_2: {
        fontSize: 14,
        fontFamily: 'Montserrat-Bold',
        letterSpacing: 0,
        lineHeight: 18
    },
    Button_Lead: {
        fontSize: 14,
        fontFamily: 'Montserrat-SemiBold',
        letterSpacing: 0,
        lineHeight: 18
    },
    Caption: {
        fontSize: 12,
        fontFamily: 'Montserrat-SemiBold',
        letterSpacing: 0,
        lineHeight: 18
    },
    Caption_Rescaled: {
        fontSize: 10,
        fontFamily: 'Montserrat-Medium',
        letterSpacing: 0,
        lineHeight: 18
    },
    Note: {
        fontSize: 10,
        fontFamily: 'Montserrat-MediumItalic',
        letterSpacing: 0,
        lineHeight: 14
    },
    Note2: {
        fontSize: 10,
        fontFamily: 'Montserrat-Medium',
        letterSpacing: 0,
        lineHeight: 14
    },
    OVERLINE: {
        fontSize: 12,
        fontFamily: 'Montserrat-SemiBold',
        letterSpacing: 0,
        lineHeight: 18
    },
    BrandRep: {
        fontSize: 16,
        fontFamily: 'Ubuntu-Bold',
        letterSpacing: 0,
        lineHeight: 18
    },
});

export default defaultStyle;