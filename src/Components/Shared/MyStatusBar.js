import React, { Component } from 'react';
import { View, StatusBar, SafeAreaView } from 'react-native';

export default class MyStatusBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{height: StatusBar.currentHeight}}>
                <SafeAreaView>
                    <StatusBar translucent {...this.props} />
                </SafeAreaView>
            </View>
        )
    }
}
