import { StyleSheet, Dimensions } from 'react-native'

const windowWidth = Dimensions.get('window').width
const styles = StyleSheet.create({

  cityText: {
    marginTop: -40,
    color: 'black',
    zIndex: 1,
  },
  placeholder: {
    color: 'red',
  },
  container: {
    flex: 1,
    backgroundColor: '#F7F7F5',
    // height: windowHeight * 2
  },
  mainSection: {
    alignItems: 'center',
    // backgroundColor: 'red'
  },
  footer: {
    backgroundColor: '#00394D',
    alignItems: 'center',
    alignSelf: 'stretch',
    height: 50,
    // marginTop: 30,
  },
  headerSetPassword: {
    fontSize: 20,
    fontStyle: 'italic',
    color: '#00394D',
    fontWeight: '700',
    marginTop: '5%'
  },
  inputBlurAddress: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 16,
    borderWidth: 1.1,
    borderColor: '#4F7D561A',
    fontSize: 14,
    borderRadius: 10,
    backgroundColor: '#FFFFFF',
    width: 345,
    height: 51,
    marginBottom: 10
  },
  phoneNumberContainer: {
    flexDirection: 'row',
    //flex: 1
    // marginLeft: 6,
    // shadowColor: "#4F7D561A",
    // shadowOffset: {
    //   width: 2,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 2.84,
  },
  countryCodeFocus: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 3,
    borderWidth: 1.1,
    borderColor: '#BFC52E',
    paddingTop: -10,
    fontSize: 14,
    borderRadius: 6,
    backgroundColor: '#FFFFFF',
    width: 100,
    height: 43,
    marginBottom: '1.5%',
    shadowColor: "#4F7D561A",
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2.84,
  },
  countryCodeBlur: {
    width: '30%',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 14,
    backgroundColor: '#F7F7F5',
    height: 60,
  },
  phoneNumber: {
    color: '#367681',
    fontFamily:'Montserrat-SemiBold',
    // marginTop: '2%',
    // marginBottom: '5%',
    textAlign: 'center',
    fontSize: 16
  },
  phoneNoFocus: {
    marginLeft: -2,
    elevation: 3,
    borderWidth: 1.1,
    borderColor: '#BFC52E',
    padding: 10,
    fontSize: 14,
    borderRadius: 6,
    backgroundColor: '#FFFFFF',
    width: 162,
    height: 43,
    marginBottom: '1.5%',
    shadowColor: "#4F7D561A",
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2.84,
  },
  phoneNoBlur: {
    marginLeft: -2,
    elevation: 3,
    borderWidth: 1.1,
    borderColor: '#4F7D561A',
    padding: 10,
    fontSize: 14,
    borderRadius: 6,
    backgroundColor: '#FFFFFF',
    width: 162,
    height: 43,
    marginBottom: '1.5%',
    shadowColor: "#4F7D561A",
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2.84,
  },
  pickerSelectStyles: {
    fontSize: 20,
    paddingLeft: 10,
    paddingTop: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'blue',
    paddingRight: 30,
  },
  inputFocusSetPassword: {
    marginLeft: -2,
    elevation: 3,
    borderWidth: 1.1,
    borderColor: '#BFC52E',
    padding: 10,
    fontSize: 14,
    borderRadius: 6,
    backgroundColor: '#FFFFFF',
    width: 280,
    height: 43,
    marginBottom: '1.5%',
    shadowColor: "#4F7D561A",
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2.84,
  },
  inputBlur: {
    //marginLeft: -2,
    // elevation: 3,
    fontFamily:'Montserrat-Medium',
    padding: 10,
    fontSize: 16,
    // borderRadius: 6,
    backgroundColor: '#F7F7F5',
    width: '100%',
    height: 50,
    marginBottom: 4,
    borderBottomWidth: 0,
    borderBottomColor: '#FFFFFF'
    // shadowColor: "#4F7D561A",
    // shadowOffset: {
    //   width: 2,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 2.84,
    // borderBottomColor: '#FFFFFF',
    // borderBottomWidth: 2,
    
  },
  agree: {
    flex: 1,
    justifyContent: 'center',
    width: 290,
    height: 54,
    alignSelf: 'center',
    borderRadius: 30,
    marginVertical: 25,
    // marginBottom: 24,
    fontWeight: 'bold',
    backgroundColor: '#154A59',
    textAlign: 'center',
    fontSize: 12,
    
    // paddingVertical:10,
    alignItems: 'center',
  },
  agreeLocation: {
    flex: 1,
    justifyContent: 'center',
    width: 290,
    height: 54,
    alignSelf: 'center',
    borderRadius: 30,
    // marginTop: 25,
    // marginBottom: 24,
    fontWeight: 'bold',
    backgroundColor: '#154A59',
    textAlign: 'center',
    fontSize: 12,
    
    // paddingVertical:10,
    alignItems: 'center',
  },
  agreeTxt: {
    // flex: 2,
    // marginLeft: '-80%',
    // position: 'absolute',
    // bottom: 0,
    fontFamily:'Montserrat-Medium',
    alignSelf: 'center',
    fontWeight: '500',
    color: '#FFFFFF',
    // textAlign: 'center',
    fontSize: 18,
    // paddingHorizontal: 10,
    // paddingVertical: 10,
  },
  agreeTxt2: {
    // flex: 2,
    // marginLeft: '-80%',
    // position: 'absolute',
    // bottom: 0,
    // left: 0,
    alignSelf: 'center',
    fontFamily:'Montserrat-Medium',
    color: '#FFFFFF',
    // textAlign: 'center',
    fontSize: 18,
    // paddingHorizontal: 10,
    // paddingVertical: 10,
  },
  signup: {
    color: '#00394D',
    marginTop: '10%',
    fontWeight: 'bold'
  },
  termsConditions: {
    marginTop: 26,
    lineHeight: 0.5,
    width: 284,
    alignSelf: 'center',
    flexDirection: 'row'
  },
  term: {
    fontFamily:'Montserrat-Regular',
    color: '#698F8A',
    fontSize: 12,
    // marginBottom: '-2%'
  },
  terms: {
    fontFamily:'Montserrat-SemiBold',
    color: '#698F8A',
    fontSize: 12,
    fontWeight: '700'
    // textDecorationLine: 'underline',
    // marginTop: '2%',
    // marginBottom: '-2%'
  },

  modalCountry: {
    // flex: 1,
    // backgroundColor:'#367681',
    // height: '90%',
    // width: '100%',
    // justifyContent: 'center',
    // alignItems: 'center',
    // alignSelf: 'center',
    marginTop: 'auto',
    // marginBottom: 'auto',
    // borderRadius: 15,
    // padding: '2%'
  },

  modalContent: {
    backgroundColor: '#FFFFFF',
    alignItems: 'center'
    // justifyContent: 'center'
    // height: '100%',
    // marginTop: '7%',
  },
  modalHeader: {
    color: '#91B3A2',
    fontSize: 15,
    fontWeight: '200',
    textAlign: 'center',
    marginTop: '10%',
  },
  modalOtpContainer: {
    marginTop: '15%',
  },
  modalOtpText: {
    textAlign: 'center',
    color: '#91B3A2',
    fontSize: 12,
    fontFamily:'Montserrat-Medium'
  },
  continueButton: {
    width: 290,
    height: 54,
    backgroundColor: '#154A59',
    borderRadius: 40,
    alignSelf: 'center',
    // position:'absolute',
    // bottom:40,
    marginTop: '10%',
    justifyContent: 'center'
  },
  continue: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontFamily:'Montserrat-Medium',
    fontSize: 18,
  },
  resend: {
    fontSize: 14,
    textAlign: 'center',
    color: '#367681',
    // marginTop: '5%',
    fontFamily:'Montserrat-Medium',
  },
  back: {
    color: '#698F8A',
    textAlign: 'center',
    marginVertical: '15%',
    fontWeight: 'bold',
  },
  otp: {
    marginTop: '4%',
    fontSize: 42,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: "#00394D"
  },
  border: {
    borderWidth: 1.5,
    marginTop: '1%',
    width: '53%',
    borderStyle: 'dashed',
    borderColor: '#91B3A2',
    alignSelf: 'center',
    borderRadius: 1,
  },
  logIN: {
    color: '#C8DB6E',
    fontWeight: 'bold',
    fontSize: 12
  },

  // --- Set Password ---

  searchSection: {
    flexDirection: 'row',
  },
  icon: {
    padding: 10,
    position: 'absolute',
    top: 26,
    right: 10,
    // backgroundColor: 'orange',
    // marginLeft : -45,
    // marginTop: 19,
    zIndex: 2
  },
  footerSetPassword: {
    backgroundColor: '#00394D',
    alignItems: 'center',
    alignSelf: 'stretch',
    height: 118,
    marginTop: 30,
  },
  logo: {
    width: 215,
    height: 80,
    marginTop: 75
  },
  header: {
    fontSize: 16,
    textAlign: 'center',
    color: '#C8DB6E',
    fontWeight: 'bold',
    marginBottom: '1%',
    lineHeight: 24,
    marginTop: '6%'
  },
  form: {
    marginTop: 10,
    backgroundColor: '#F7F7F5',
    width: '100%'
  },
  inputFocus: {
    // borderWidth: 1.1,
    // borderColor: '#BFC52E',
    padding: 10,
    fontSize: 14,
    // borderRadius: 6,
    backgroundColor: '#FFFFFF',
    // width: 300,
    height: 43,
    // marginBottom: '1.5%',
  },
  logIn: {
    width: 150,
    height: 39,
    alignSelf: 'center',
    borderRadius: 5,
    marginTop: 25,
    marginBottom: 24,
    fontWeight: 'bold',
    backgroundColor: '#154A59',
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 13,
    paddingHorizontal: 10,
    paddingVertical: 10,
    alignItems: 'center',
  },
  logInText: {
    alignSelf: 'center',
    fontWeight: 'bold',
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 13,
  },
  signUp: {
    color: '#00394D',
    marginTop: '5%',
    fontWeight: 'bold'
  },
  forgotPassword: {
    color: '#698F8A',
    alignSelf: 'flex-end',
    fontSize: 14,
    fontWeight: '700',
    marginTop: '20%',
  },
  borderStyleBase: {
    // // width: 42,
    // // height: 50,
    color: '#00394D',
    // borderWidth: 0,
    // // borderRadius:10,
    // // backgroundColor:'#154A59',
    // // borderColor:'#00394D',
    // borderBottomColor: '#BFC52E',
    // borderBottomWidth: 2,
    fontWeight:'bold',
    fontSize: 24,

    width: 36,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 2,
    borderBottomColor: '#BFC52E'
  },
 
  borderStyleHighLighted: {
    // color: "#91B3A257",
    borderColor: "#03DAC6",
  },
  termsmodal: {
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    width: '85%',
    padding:10,
    borderBottomWidth:1,
    borderBottomColor:'#ccc'
  }
})

export default styles