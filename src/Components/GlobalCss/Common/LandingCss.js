import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    loginButton: {
        width:148,
        height:48,
        marginTop:20,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#CFE7C733',
        borderRadius:29
    },
    signupButton: {
        width:280,
        height:54,
        borderRadius:29,
        backgroundColor:'#BFC52E',
        alignItems:'center',
        justifyContent:'center',
    },
})

export default styles