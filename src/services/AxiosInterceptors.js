import axios from 'axios';
import {REACT_APP_userServiceURL} from '../../env.json';
import AsyncStorage from '@react-native-community/async-storage';

const qs = require('query-string');

export default {

    setupInterceptors: () => {

        let isRefreshing = false;
        let failedQueue = [];

        const processQueue = (error) => {
            failedQueue.forEach(prom => {
                if (error) {
                    prom.reject(error);
                } else {
                    prom.resolve();
                }
            })

            failedQueue = [];
        }

        axios.interceptors.response.use((response) => {
            // Return a successful response back to the calling service
            return response;

        }, (error) => {

            // Return any error which is not due to authentication back to the calling service
            if (error.response === undefined || error.response.status !== 401) {
                return new Promise((resolve, reject) => {
                    reject(error);
                });
            }

            //Exclude User Login
            if (error.config.url === REACT_APP_userServiceURL + '/user/login') {
                return new Promise((resolve, reject) => {
                    reject(error);
                });
            }

            // Logout user if token refresh didn't work or user is disabled
            if (error.config.url === REACT_APP_userServiceURL + '/user/renew/token'
                || (error.response && error.response.message === 'Account is disabled.')) {

                AsyncStorage.removeItem("userData")
                AsyncStorage.removeItem("userId")
                AsyncStorage.removeItem("refreshToken")

                return new Promise((resolve, reject) => {
                    reject(error);
                });
            }

            const originalRequest = error.config;


            if (!originalRequest._retry) {

                if (isRefreshing) {
                    return new Promise(function (resolve, reject) {
                        failedQueue.push({resolve, reject})
                    }).then(() => {
                        return axios(originalRequest);
                    }).catch(err => {
                        return Promise.reject(err);
                    })
                }

                originalRequest._retry = true;
                isRefreshing = true;

                return new Promise((resolve, reject) => {
                    let url = REACT_APP_userServiceURL + '/user/renew/token';
                    AsyncStorage.getItem('refreshToken').then(value => {
                        // console.log(value);

                        return axios({
                            method: 'post',
                            url: url,
                            data: qs.stringify({
                                // refreshToken: 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ZWNiNWQ3MzI3ZTY2YzVhZGE0NTU3OWUiLCJhdXRoIjpbXSwiaWF0IjoxNjE1NTI5MTkzLCJleHAiOjE2NDcwNjUxOTN9.CuCeJ0AWK-g6k-POY_-H9nmab5nsfMv48PneOH5dtVE'
                                refreshToken: value
                            }),
                            withCredentials: true,
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                        }).then((response) => {
                            if (
                                response &&
                                response.data &&
                                response.data.statusCode === 200
                            ) {
                                processQueue(null);
                                resolve(axios(originalRequest));
                            } else {
                                console.log(response);
                                reject(response);
                            }
                        }).catch((e) => {
                            processQueue(e);
                            reject(e);
                        }).finally(() => {
                            isRefreshing = false
                        });
                    }).catch((e) => console.log("error",e))

                    
                });
            }

            return Promise.reject(error);
        });

    }
}