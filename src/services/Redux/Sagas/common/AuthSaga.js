import {call,put,takeEvery} from "redux-saga/effects";
import {Auth} from '../../ActionTypes/common/Auth';
import axios from 'axios';
import _ from 'lodash';
import AsyncStorage from '@react-native-community/async-storage';
import {
    loginSuccess,
    loginFailure
} from '../../Actions/common/LoginActions';
import {REACT_APP_userServiceURL} from '../../../../../env.json';

function* asyncUserLogin(action) {
    const body = {
        "username" : action.payload.username,
        "password" : action.payload.password,
    };

    try {
        const response = yield call(()=>
            axios.post(`${REACT_APP_userServiceURL}/user/login`,
                        body,
                        {headers: {'Content-Type': 'application/json'}},
                        {withCredentials: true})
        );
        // console.log("hhhhhhhhhhh",response.data.body)
        AsyncStorage.setItem("refreshToken",response.headers['authorization'])
        AsyncStorage.setItem("userId",response.data.body.userId)
        AsyncStorage.setItem("userData", JSON.stringify({

            'userId':response.data.body.userId,
            'mobile':response.data.body.mobile,
            'email':response.data.body.email,
            'firstName':response.data.body.firstName,
            'lastName':response.data.body.lastName,
            'userDataPending':response.data.body.userDataPending,
            'interests':response.data.body.interests,
            'persona':response.data.body.persona,
            'skills':response.data.body.skills,
            'profileImage':response.data.body.profileImage,
            'resizedProfileImages':response.data.body.resizedProfileImages,
            'cookiesAccepted':response.data.body.cookiesAccepted,
            'type':response.data.body.type,
            'specialities':response.data.body.specialities,
            'indianResidence':response.data.body.indianResidence,
            'customUrl':response.data.body.customUrl,
            'companyName':response.data.body.companyName
            
        }))
        yield put (loginSuccess(response.data));

    }
    catch(err){
        
        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to login, Please try after sometime',
        );
        yield put (loginFailure(errorResponse));
    }
}

export function* watchUserLogin () {
    yield takeEvery (Auth.USER_LOGIN_REQUEST, asyncUserLogin);
}