import {call, put, takeEvery} from 'redux-saga/effects';
import {Notification} from '../../ActionTypes/User/Notification';
import axios from 'axios';
import _ from 'lodash';
import AsyncStorage from '@react-native-community/async-storage';
import {
  notificationSuccess,
  notificationFailure,
} from '../../Actions/User/NotificationActions';
import {REACT_APP_userServiceURL} from '../../../../../env.json';
import httpService from '../../../AxiosInterceptors';

let id = '';
httpService.setupInterceptors();

function* asyncNotificationData(action) {
  try {
    const response = yield call(() =>
      axios({
        method: 'get',
        url:
          REACT_APP_userServiceURL +
          '/notification/list?userId=' +
          action.payload.userId +
          '&notificationCategory=' +
          action.payload.notificationCategory +
          '&page=' +
          action.payload.pageNo +
          '&size=10',
        headers: {'Content-Type': 'application/json'},
        withCredentials: true,
      }),
    );
    // console.log('Notifications: ', response.data.body.content)
    let tempData = response.data.body.content

    yield put(notificationSuccess(tempData.concat(response.data.body.content)));
  } catch (err) {
    let errorResponse = _.get(err, 'response.data', 'Unable to fetch data');
    yield put(notificationFailure(errorResponse));
  }
}

export function* watchNotificationData() {
  yield takeEvery(
    Notification.USER_NOTIFICATION_REQUEST,
    asyncNotificationData,
  );
}
