import { call, put, takeEvery } from "redux-saga/effects"
import { Feeds } from '../../ActionTypes/User/FeedsType'
import axios from 'axios'
import _ from 'lodash'
import AsyncStorage from '@react-native-community/async-storage'
import {
    feedsPhotosSuccess,
    feedsPhotosFailure,
    feedsVideosSuccess,
    feedsVideosFailure
} from '../../Actions/User/FeedsActions'
import { REACT_APP_userServiceURL } from '../../../../../env.json'
import httpService from '../../../AxiosInterceptors'

httpService.setupInterceptors()


function* asyncFeedsPhotosData(action) {

    try {
        const response = yield call(() =>
            axios({
                method: 'get',
                url: REACT_APP_userServiceURL + '/post/getNewsFeed?userId=' + action.payload.userId
                    + '&newsFeedType=' + action.payload.type + '&page=' + action.payload.pageNumber + '&size='
                    + action.payload.size,
                headers: { 'Content-Type': 'application/json' },
                withCredentials: true
            })
        )
        yield put(feedsPhotosSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(feedsPhotosFailure(errorResponse))
    }
}



function* asyncFeedsVideosData(action) {

    try {
        const response = yield call(() =>
            axios({
                method: 'get',
                url: REACT_APP_userServiceURL + '/post/getNewsFeed?userId=' + action.payload
                    + '&newsFeedType=VIDEOS&page=' + 0 + '&size=' + 500,
                headers: { 'Content-Type': 'application/json' },
                withCredentials: true
            })
        )
        yield put(feedsVideosSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(feedsVideosFailure(errorResponse))
    }
}


export function* watchFeedsPhotosData() {
    yield takeEvery(Feeds.USER_FEEDSPHOTOS_REQUEST, asyncFeedsPhotosData)
}

export function* watchFeedsVideosData() {
    yield takeEvery(Feeds.USER_FEEDSVIDEOS_REQUEST, asyncFeedsVideosData)
}
