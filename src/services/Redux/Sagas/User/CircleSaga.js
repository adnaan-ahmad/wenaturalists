import { call, put, takeEvery } from "redux-saga/effects"
import { Circle } from '../../ActionTypes/User/CircleType'
import axios from 'axios'
import _ from 'lodash'
import AsyncStorage from '@react-native-community/async-storage'
import {
    userCircleSuccess,
    userCircleFailure
} from '../../Actions/User/CircleActions'
import { REACT_APP_userServiceURL } from '../../../../../env.json'
import httpService from '../../../AxiosInterceptors'

httpService.setupInterceptors()

function* asyncCircleData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            // url: REACT_APP_userServiceURL + '/backend/circle/get/created/list/' + action.payload.userId + "?page=" + 0 + "$size=" + 100,
            url: REACT_APP_userServiceURL + '/backend/circle/get/created/circleList/' + action.payload.userId + "?page=" + 0 + "&size=" + 100,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        })
        )
        // console.log('$$$$$$$$$$$$$$$$$$$$$$$$$$$$ Circle $$$$$$$$$$$$$$$$$$$$$$$$$$$$', response.data.body.content[0])

        yield put (userCircleSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(userCircleFailure(errorResponse))
    }
}

export function* watchCircleData() {
    yield takeEvery(Circle.USER_CIRCLE_REQUEST, asyncCircleData)
}
