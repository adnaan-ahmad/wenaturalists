import { call, put, takeEvery } from "redux-saga/effects"
import { Connects } from '../../ActionTypes/User/ConnectsType'
import axios from 'axios'
import _ from 'lodash'
import AsyncStorage from '@react-native-community/async-storage'
import {
    connectsSuccess,
    connectsFailure
} from '../../Actions/User/ConnectsActions'
import { REACT_APP_userServiceURL } from '../../../../../env.json'
import httpService from '../../../AxiosInterceptors'

httpService.setupInterceptors()


function* asyncConnectsData(action) {

    try {
        const response = yield call(() =>
            axios({
                method: 'get',
                url:
                    REACT_APP_userServiceURL +
                    "/network/" +
                    action.payload.userId +
                    "/connects" +
                    "?page=" +
                    0 +
                    "&size=" +
                    action.payload.size,
                headers: { 'Content-Type': 'application/json' },
                withCredentials: true
            })
        )
        
        yield put(connectsSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(connectsFailure(errorResponse))
    }
}



export function* watchConnectsData() {
    yield takeEvery(Connects.USER_CONNECTS_REQUEST, asyncConnectsData)
}
