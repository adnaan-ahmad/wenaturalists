import { call, put, takeEvery } from "redux-saga/effects"
import { PersonalProfileType } from '../../ActionTypes/User/PersonalProfileType'
import axios from 'axios'
import _ from 'lodash'
import AsyncStorage from '@react-native-community/async-storage'
import {
    personalProfileSuccess,
    personalProfileFailure,
    personalContactSuccess,
    personalContactFailure,
    personalAddressSuccess,
    personalAddressFailure,
    personalBioSuccess,
    personalBioFailure,
    personalSkillsSuccess,
    personalSkillsFailure,
    personalSkillsSpecializationSuccess,
    personalSkillsSpecializationFailure,
    personalRecentActivitySuccess,
    personalRecentActivityFailure,
    personalExperienceSuccess,
    personalExperienceFailure,
    personalEducationSuccess,
    personalEducationFailure,
    personalBusinessPageSuccess,
    personalBusinessPageFailure,
    personalInterestsSuccess,
    personalInterestsFailure,
    personalHashTagsSuccess,
    personalHashTagsFailure,
    personalSpecialitiesSuccess,
    personalSpecialitiesFailure,
    personalConnectionInfoSuccess,
    personalConnectionInfoFailure
} from '../../Actions/User/PersonalProfileActions'
import { REACT_APP_userServiceURL } from '../../../../../env.json'
import httpService from '../../../AxiosInterceptors'

httpService.setupInterceptors()

function* asyncUserData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
        })
        )

        yield put (personalProfileSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalProfileFailure(errorResponse))
    }
}


function* asyncContactData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/contact/info?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
        })
        )
        yield put (personalContactSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalContactFailure(errorResponse))
    }
}


function* asyncAddressData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/address?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
        })
        )
        yield put (personalAddressSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalAddressFailure(errorResponse))
    }
}


function* asyncBioData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/bio?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (personalBioSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalBioFailure(errorResponse))
    }
}


function* asyncSkillsData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/skills?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (personalSkillsSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalSkillsFailure(errorResponse))
    }
}



function* asyncSkillsSpecializationData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/skills-and-specialization?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (personalSkillsSpecializationSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalSkillsSpecializationFailure(errorResponse))
    }
}



function* asyncRecentActivityData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/activity/list?userId=' + action.payload.userId + "&page=" + 0 + "&size=" + 30,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (personalRecentActivitySuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalRecentActivityFailure(errorResponse))
    }
}



function* asyncExperienceData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/experience?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId + "&page=" + 0 + "&size=" + 100,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (personalExperienceSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalExperienceFailure(errorResponse))
    }
}



function* asyncEducationData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/education?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (personalEducationSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalEducationFailure(errorResponse))
    }
}



function* asyncBusinessPageData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/all-business/pages?userId=' + action.payload.userId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (personalBusinessPageSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalBusinessPageFailure(errorResponse))
    }
}




function* asyncInterestsData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/interests?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (personalInterestsSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalInterestsFailure(errorResponse))
    }
}




function* asyncHashTagsData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/post/getUserHashtags?userId=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (personalHashTagsSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalHashTagsFailure(errorResponse))
    }
}




function* asyncSpecialitiesData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/specialities?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (personalSpecialitiesSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalSpecialitiesFailure(errorResponse))
    }
}



function* asyncConnectionInfoData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/connection/info?userId=' + action.payload.userId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (personalConnectionInfoSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(personalConnectionInfoFailure(errorResponse))
    }
}




export function* watchUserData() {
    yield takeEvery(PersonalProfileType.USER_DATA_REQUEST, asyncUserData)
}

export function* watchContactData() {
    yield takeEvery(PersonalProfileType.USER_CONTACT_REQUEST, asyncContactData)
}

export function* watchAddressData() {
    yield takeEvery(PersonalProfileType.USER_ADDRESS_REQUEST, asyncAddressData)
}

export function* watchBioData() {
    yield takeEvery(PersonalProfileType.USER_BIO_REQUEST, asyncBioData)
}

export function* watchSkillsData() {
    yield takeEvery(PersonalProfileType.USER_SKILLS_REQUEST, asyncSkillsData)
}

export function* watchSkillsSpecializationData() {
    yield takeEvery(PersonalProfileType.USER_SKILLSSPECIALIZATION_REQUEST, asyncSkillsSpecializationData)
}

export function* watchRecentActivityData() {
    yield takeEvery(PersonalProfileType.USER_RECENTACTIVITY_REQUEST, asyncRecentActivityData)
}

export function* watchExperienceData() {
    yield takeEvery(PersonalProfileType.USER_EXPERIENCE_REQUEST, asyncExperienceData)
}

export function* watchEducationData() {
    yield takeEvery(PersonalProfileType.USER_EDUCATION_REQUEST, asyncEducationData)
}

export function* watchBusinessPageData() {
    yield takeEvery(PersonalProfileType.USER_BUSINESS_PAGE_REQUEST, asyncBusinessPageData)
}

export function* watchInterestsData() {
    yield takeEvery(PersonalProfileType.USER_INTERESTS_REQUEST, asyncInterestsData)
}

export function* watchHashTagsData() {
    yield takeEvery(PersonalProfileType.USER_HASHTAGS_REQUEST, asyncHashTagsData)
}

export function* watchSpecialitiesData() {
    yield takeEvery(PersonalProfileType.USER_SPECIALITIES_REQUEST, asyncSpecialitiesData)
}

export function* watchConnectionInfoData() {
    yield takeEvery(PersonalProfileType.USER_CONNECTIONINFO_REQUEST, asyncConnectionInfoData)
}
