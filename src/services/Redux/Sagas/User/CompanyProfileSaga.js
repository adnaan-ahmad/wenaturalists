import { call, put, takeEvery } from "redux-saga/effects"
import { CompanyProfileType } from '../../ActionTypes/User/CompanyProfileType'
import axios from 'axios'
import _ from 'lodash'
import AsyncStorage from '@react-native-community/async-storage'
import {
    companyProfileSuccess,
    companyProfileFailure,
    companyContactSuccess,
    companyContactFailure,
    companyAddressSuccess,
    companyAddressFailure,
    companyBioSuccess,
    companyBioFailure,
    companySkillsSuccess,
    companySkillsFailure,
    companySkillsSpecializationSuccess,
    companySkillsSpecializationFailure,
    companyRecentActivitySuccess,
    companyRecentActivityFailure,
    companyExperienceSuccess,
    companyExperienceFailure,
    companyEducationSuccess,
    companyEducationFailure,
    companyBusinessPageSuccess,
    companyBusinessPageFailure,
    companyInterestsSuccess,
    companyInterestsFailure,
    companyHashTagsSuccess,
    companyHashTagsFailure,
    companySpecialitiesSuccess,
    companySpecialitiesFailure,
    companyConnectionInfoSuccess,
    companyConnectionInfoFailure
} from '../../Actions/User/CompanyProfileActions'
import { REACT_APP_userServiceURL } from '../../../../../env.json'
import httpService from '../../../AxiosInterceptors'

httpService.setupInterceptors()

function* asyncUserData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
        })
        )
        // console.log(':::: Checking ::::::')
        yield put (companyProfileSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companyProfileFailure(errorResponse))
    }
}


function* asyncContactData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/contact/info?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
        })
        )
        yield put (companyContactSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companyContactFailure(errorResponse))
    }
}


function* asyncAddressData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/address?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
        })
        )
        yield put (companyAddressSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companyAddressFailure(errorResponse))
    }
}


function* asyncBioData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/bio?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (companyBioSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companyBioFailure(errorResponse))
    }
}


function* asyncSkillsData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/skills?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (companySkillsSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companySkillsFailure(errorResponse))
    }
}



function* asyncSkillsSpecializationData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/skills-and-specialization?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (companySkillsSpecializationSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companySkillsSpecializationFailure(errorResponse))
    }
}



function* asyncRecentActivityData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/activity/list?userId=' + action.payload.userId + "&page=" + 0 + "&size=" + 30,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (companyRecentActivitySuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companyRecentActivityFailure(errorResponse))
    }
}



function* asyncExperienceData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/experience?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId + "&page=" + 0 + "&size=" + 100,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (companyExperienceSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companyExperienceFailure(errorResponse))
    }
}



function* asyncEducationData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/education?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (companyEducationSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companyEducationFailure(errorResponse))
    }
}



function* asyncBusinessPageData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/all-business/pages?userId=' + action.payload.userId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (companyBusinessPageSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companyBusinessPageFailure(errorResponse))
    }
}




function* asyncInterestsData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/interests?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (companyInterestsSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companyInterestsFailure(errorResponse))
    }
}




function* asyncHashTagsData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/post/getUserHashtags?userId=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (companyHashTagsSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companyHashTagsFailure(errorResponse))
    }
}




function* asyncSpecialitiesData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/specialities?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (companySpecialitiesSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companySpecialitiesFailure(errorResponse))
    }
}



function* asyncConnectionInfoData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/connection/info?userId=' + action.payload.userId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (companyConnectionInfoSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(companyConnectionInfoFailure(errorResponse))
    }
}




export function* watchCompanyUserData() {
    yield takeEvery(CompanyProfileType.USER_DATA_REQUEST, asyncUserData)
}

export function* watchCompanyContactData() {
    yield takeEvery(CompanyProfileType.USER_CONTACT_REQUEST, asyncContactData)
}

export function* watchCompanyAddressData() {
    yield takeEvery(CompanyProfileType.USER_ADDRESS_REQUEST, asyncAddressData)
}

export function* watchCompanyBioData() {
    yield takeEvery(CompanyProfileType.USER_BIO_REQUEST, asyncBioData)
}

export function* watchCompanySkillsData() {
    yield takeEvery(CompanyProfileType.USER_SKILLS_REQUEST, asyncSkillsData)
}

export function* watchCompanySkillsSpecializationData() {
    yield takeEvery(CompanyProfileType.USER_SKILLSSPECIALIZATION_REQUEST, asyncSkillsSpecializationData)
}

export function* watchCompanyRecentActivityData() {
    yield takeEvery(CompanyProfileType.USER_RECENTACTIVITY_REQUEST, asyncRecentActivityData)
}

export function* watchCompanyExperienceData() {
    yield takeEvery(CompanyProfileType.USER_EXPERIENCE_REQUEST, asyncExperienceData)
}

export function* watchCompanyEducationData() {
    yield takeEvery(CompanyProfileType.USER_EDUCATION_REQUEST, asyncEducationData)
}

export function* watchCompanyBusinessPageData() {
    yield takeEvery(CompanyProfileType.USER_BUSINESS_PAGE_REQUEST, asyncBusinessPageData)
}

export function* watchCompanyInterestsData() {
    yield takeEvery(CompanyProfileType.USER_INTERESTS_REQUEST, asyncInterestsData)
}

export function* watchCompanyHashTagsData() {
    yield takeEvery(CompanyProfileType.USER_HASHTAGS_REQUEST, asyncHashTagsData)
}

export function* watchCompanySpecialitiesData() {
    yield takeEvery(CompanyProfileType.USER_SPECIALITIES_REQUEST, asyncSpecialitiesData)
}

export function* watchCompanyConnectionInfoData() {
    yield takeEvery(CompanyProfileType.USER_CONNECTIONINFO_REQUEST, asyncConnectionInfoData)
}
