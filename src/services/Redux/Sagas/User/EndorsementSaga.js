import { call, put, takeEvery } from "redux-saga/effects"
import { EndorsementType } from '../../ActionTypes/User/EndorsementType'
import axios from 'axios'
import _ from 'lodash'
import AsyncStorage from '@react-native-community/async-storage'
import {
    endorsementSuccess,
    endorsementFailure,
    endorsementDetailSuccess,
    endorsementDetailFailure
} from '../../Actions/User/EndorsementActions'
import { REACT_APP_userServiceURL } from '../../../../../env.json'
import httpService from '../../../AxiosInterceptors'

httpService.setupInterceptors()


function* asyncEndorsementData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/endorsement/list?userId=' + action.payload.userId,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        })
        )
        // console.log('$$$$$$$$$$$$$$$$$$$$$$$$$$$$ Endorsement $$$$$$$$$$$$$$$$$$$$$$$$$$$$', response.data.body)

        yield put (endorsementSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(endorsementFailure(errorResponse))
    }
}




function* asyncEndorsementDetailData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/endorsement/list?userId=' + action.payload.userId + '&topic=' + action.payload.topic + "&page=" + 0 + "&size=" + 10,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        })
        )
        // console.log('$$$$$$$$$$$$$$$$$$$$$$$$$$$$ EndorsementDetail $$$$$$$$$$$$$$$$$$$$$$$$$$$$', response.data.body)

        yield put (endorsementDetailSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(endorsementDetailFailure(errorResponse))
    }
}



export function* watchEndorsementData() {
    yield takeEvery(EndorsementType.USER_ENDORSEMENT_REQUEST, asyncEndorsementData)
}

export function* watchEndorsementDetailData() {
    yield takeEvery(EndorsementType.USER_ENDORSEMENTDETAIL_REQUEST, asyncEndorsementDetailData)
}
