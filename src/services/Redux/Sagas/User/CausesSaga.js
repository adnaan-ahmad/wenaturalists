import { call, put, takeEvery } from "redux-saga/effects"
import { Causes } from '../../ActionTypes/User/CausesType'
import axios from 'axios'
import _ from 'lodash'
import AsyncStorage from '@react-native-community/async-storage'
import {
    causesSuccess,
    causesFailure
} from '../../Actions/User/CausesActions'
import { REACT_APP_userServiceURL } from '../../../../../env.json'
import httpService from '../../../AxiosInterceptors'

httpService.setupInterceptors()


function* asyncCausesData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/cause/joined/list?userId=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId + "&page=" + 0 + "&size=" + 500,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
            cache: true
        })
        )

        yield put (causesSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(causesFailure(errorResponse))
    }
}



export function* watchCausesData() {
    yield takeEvery(Causes.USER_CAUSES_REQUEST, asyncCausesData)
}
