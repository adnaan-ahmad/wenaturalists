import { call, put, takeEvery } from "redux-saga/effects"
import { OtherProfileType } from '../../ActionTypes/User/OtherProfileType'
import axios from 'axios'
import _ from 'lodash'
import AsyncStorage from '@react-native-community/async-storage'
import {
    otherProfileSuccess,
    otherProfileFailure,
    otherContactSuccess,
    otherContactFailure,
    otherAddressSuccess,
    otherAddressFailure,
    otherBioSuccess,
    otherBioFailure,
    otherSkillsSuccess,
    otherSkillsFailure,
    otherSkillsSpecializationSuccess,
    otherSkillsSpecializationFailure,
    otherRecentActivitySuccess,
    otherRecentActivityFailure,
    otherExperienceSuccess,
    otherExperienceFailure,
    otherEducationSuccess,
    otherEducationFailure,
    otherBusinessPageSuccess,
    otherBusinessPageFailure,
    otherInterestsSuccess,
    otherInterestsFailure,
    otherHashTagsSuccess,
    otherHashTagsFailure,
    otherSpecialitiesSuccess,
    otherSpecialitiesFailure,
    otherConnectionInfoSuccess,
    otherConnectionInfoFailure
} from '../../Actions/User/OtherProfileActions'
import { REACT_APP_userServiceURL } from '../../../../../env.json'
import httpService from '../../../AxiosInterceptors'

httpService.setupInterceptors()

function* asyncUserData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
        })
        )
        // console.log(':::: Checking ::::::')
        yield put (otherProfileSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherProfileFailure(errorResponse))
    }
}


function* asyncContactData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/contact/info?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
        })
        )
        yield put (otherContactSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherContactFailure(errorResponse))
    }
}


function* asyncAddressData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/address?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
        })
        )
        yield put (otherAddressSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherAddressFailure(errorResponse))
    }
}


function* asyncBioData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/bio?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (otherBioSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherBioFailure(errorResponse))
    }
}


function* asyncSkillsData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/skills?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (otherSkillsSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherSkillsFailure(errorResponse))
    }
}



function* asyncSkillsSpecializationData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/skills-and-specialization?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (otherSkillsSpecializationSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherSkillsSpecializationFailure(errorResponse))
    }
}



function* asyncRecentActivityData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/activity/list?userId=' + action.payload.userId + "&page=" + 0 + "&size=" + 30,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (otherRecentActivitySuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherRecentActivityFailure(errorResponse))
    }
}



function* asyncExperienceData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/experience?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId + "&page=" + 0 + "&size=" + 100,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (otherExperienceSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherExperienceFailure(errorResponse))
    }
}



function* asyncEducationData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/education?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (otherEducationSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherEducationFailure(errorResponse))
    }
}



function* asyncBusinessPageData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/all-business/pages?userId=' + action.payload.userId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (otherBusinessPageSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherBusinessPageFailure(errorResponse))
    }
}




function* asyncInterestsData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/interests?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (otherInterestsSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherInterestsFailure(errorResponse))
    }
}




function* asyncHashTagsData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/post/getUserHashtags?userId=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (otherHashTagsSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherHashTagsFailure(errorResponse))
    }
}




function* asyncSpecialitiesData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/specialities?id=' + action.payload.userId + '&otherUserId=' + action.payload.otherUserId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (otherSpecialitiesSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherSpecialitiesFailure(errorResponse))
    }
}



function* asyncConnectionInfoData(action) {

    try {
        const response = yield call(() => 
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/connection/info?userId=' + action.payload.userId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }))
        yield put (otherConnectionInfoSuccess(response.data))

    }
    catch (err) {

        let errorResponse = _.get(
            err,
            'response.data',
            'Unable to fetch data',
        );
        yield put(otherConnectionInfoFailure(errorResponse))
    }
}




export function* watchOtherUserData() {
    yield takeEvery(OtherProfileType.USER_DATA_REQUEST, asyncUserData)
}

export function* watchOtherContactData() {
    yield takeEvery(OtherProfileType.USER_CONTACT_REQUEST, asyncContactData)
}

export function* watchOtherAddressData() {
    yield takeEvery(OtherProfileType.USER_ADDRESS_REQUEST, asyncAddressData)
}

export function* watchOtherBioData() {
    yield takeEvery(OtherProfileType.USER_BIO_REQUEST, asyncBioData)
}

export function* watchOtherSkillsData() {
    yield takeEvery(OtherProfileType.USER_SKILLS_REQUEST, asyncSkillsData)
}

export function* watchOtherSkillsSpecializationData() {
    yield takeEvery(OtherProfileType.USER_SKILLSSPECIALIZATION_REQUEST, asyncSkillsSpecializationData)
}

export function* watchOtherRecentActivityData() {
    yield takeEvery(OtherProfileType.USER_RECENTACTIVITY_REQUEST, asyncRecentActivityData)
}

export function* watchOtherExperienceData() {
    yield takeEvery(OtherProfileType.USER_EXPERIENCE_REQUEST, asyncExperienceData)
}

export function* watchOtherEducationData() {
    yield takeEvery(OtherProfileType.USER_EDUCATION_REQUEST, asyncEducationData)
}

export function* watchOtherBusinessPageData() {
    yield takeEvery(OtherProfileType.USER_BUSINESS_PAGE_REQUEST, asyncBusinessPageData)
}

export function* watchOtherInterestsData() {
    yield takeEvery(OtherProfileType.USER_INTERESTS_REQUEST, asyncInterestsData)
}

export function* watchOtherHashTagsData() {
    yield takeEvery(OtherProfileType.USER_HASHTAGS_REQUEST, asyncHashTagsData)
}

export function* watchOtherSpecialitiesData() {
    yield takeEvery(OtherProfileType.USER_SPECIALITIES_REQUEST, asyncSpecialitiesData)
}

export function* watchOtherConnectionInfoData() {
    yield takeEvery(OtherProfileType.USER_CONNECTIONINFO_REQUEST, asyncConnectionInfoData)
}
