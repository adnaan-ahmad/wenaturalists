import { all } from 'redux-saga/effects'
import { watchUserLogin } from './common/AuthSaga'
import {
    watchUserData,
    watchContactData,
    watchAddressData,
    watchBioData,
    watchSkillsData,
    watchSkillsSpecializationData,
    watchRecentActivityData,
    watchExperienceData,
    watchEducationData,
    watchBusinessPageData,
    watchInterestsData,
    watchHashTagsData,
    watchSpecialitiesData,
    watchConnectionInfoData
} from './User/PersonalProfileSaga'
import {
    watchOtherUserData,
    watchOtherContactData,
    watchOtherAddressData,
    watchOtherBioData,
    watchOtherSkillsData,
    watchOtherSkillsSpecializationData,
    watchOtherRecentActivityData,
    watchOtherExperienceData,
    watchOtherEducationData,
    watchOtherBusinessPageData,
    watchOtherInterestsData,
    watchOtherHashTagsData,
    watchOtherSpecialitiesData,
    watchOtherConnectionInfoData
} from './User/OtherProfileSaga'
import {
    watchCompanyUserData,
    watchCompanyContactData,
    watchCompanyAddressData,
    watchCompanyBioData,
    watchCompanySkillsData,
    watchCompanySkillsSpecializationData,
    watchCompanyRecentActivityData,
    watchCompanyExperienceData,
    watchCompanyEducationData,
    watchCompanyBusinessPageData,
    watchCompanyInterestsData,
    watchCompanyHashTagsData,
    watchCompanySpecialitiesData,
    watchCompanyConnectionInfoData
} from './User/CompanyProfileSaga'
import { watchNotificationData } from './User/NotificationSaga'
import { watchCircleData } from './User/CircleSaga'
import { watchEndorsementData, watchEndorsementDetailData } from './User/EndorsementSaga'
import { watchCausesData } from './User/CausesSaga'
import { watchConnectsData } from './User/ConnectsSaga'
import { watchFeedsPhotosData, watchFeedsVideosData } from './User/FeedsSaga'

export default function* rootSaga() {
    yield all([
        watchUserLogin(),
        watchUserData(),
        watchOtherUserData(),
        watchContactData(),
        watchOtherContactData(),
        watchAddressData(),
        watchOtherAddressData(),
        watchNotificationData(),
        watchBioData(),
        watchOtherBioData(),
        watchSkillsData(),
        watchOtherSkillsData(),
        watchSkillsSpecializationData(),
        watchOtherSkillsSpecializationData(),
        watchRecentActivityData(),
        watchOtherRecentActivityData(),
        watchExperienceData(),
        watchOtherExperienceData(),
        watchEducationData(),
        watchOtherEducationData(),
        watchCircleData(),
        watchEndorsementData(),
        watchEndorsementDetailData(),
        watchBusinessPageData(),
        watchOtherBusinessPageData(),
        watchCausesData(),
        watchInterestsData(),
        watchOtherInterestsData(),
        watchHashTagsData(),
        watchOtherHashTagsData(),
        watchSpecialitiesData(),
        watchOtherSpecialitiesData(),
        watchConnectionInfoData(),
        watchOtherConnectionInfoData(),
        watchConnectsData(),
        
        watchFeedsPhotosData(),
        watchFeedsVideosData(),
        
        watchCompanyUserData(),
        watchCompanyContactData(),
        watchCompanyAddressData(),
        watchCompanyBioData(),
        watchCompanySkillsData(),
        watchCompanySkillsSpecializationData(),
        watchCompanyRecentActivityData(),
        watchCompanyExperienceData(),
        watchCompanyEducationData(),
        watchCompanyBusinessPageData(),
        watchCompanyInterestsData(),
        watchCompanyHashTagsData(),
        watchCompanySpecialitiesData(),
        watchCompanyConnectionInfoData()
    ])
}
