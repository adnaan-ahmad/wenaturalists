import { combineReducers } from 'redux'
import loginReducer from './common/loginReducer'
import personalProfileReducer from './User/PersonalProfileReducer'
import otherProfileReducer from './User/OtherProfileReducer'
import companyProfileReducer from './User/CompanyProfileReducer'
import notificationReducer from './User/NotificationReducer'
import circleReducer from './User/CircleReducer'
import endorsementReducer from './User/EndorsementReducer'
import causesReducer from './User/CausesReducer'
import connectsReducer from './User/ConnectsReducer'
import feedsReducer from './User/FeedsReducer'

const RootReducer = combineReducers ({
    loginReducer, 
    personalProfileReducer,
    otherProfileReducer,
    companyProfileReducer,
    notificationReducer,
    circleReducer,
    endorsementReducer,
    causesReducer,
    connectsReducer,
    feedsReducer
})

export default RootReducer
