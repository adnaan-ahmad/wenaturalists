import {Auth} from '../../ActionTypes/common/Auth'

const initialState = {
    loginProgress : false,
    user : {},
    error : {}
}

const loginReducer = (state = initialState, action)=>{
    switch (action.type){
        case Auth.USER_LOGIN_REQUEST :
            return {
                ...state,
                loginProgress:true,
            };
        case Auth.USER_LOGIN_SUCCESS :
            return {
                ...state,
                loginProgress:false,
                user: action.payload,
                error: {}
            };
        case Auth.USER_LOGIN_FAILURE :
            return {
                ...state,
                loginProgress:false,
                user: {},
                error: action.error
            };
        case Auth.USER_LOGIN_CLEAR :
            return {
                ...state,
                loginProgress:false,
                user: {},
                error: {}
            };
        default :
        return state;
    }
}

export default loginReducer;