import { Feeds } from '../../ActionTypes/User/FeedsType'

const initialState = {
    userFeedsPhotosProgress: false,
    userFeedsPhotos: {},
    errorFeedsPhotos: {},

    userFeedsVideosProgress: false,
    userFeedsVideos: {},
    errorFeedsVideos: {}
}

const feedsReducer = (state = initialState, action) => {
    switch (action.type) {
        case Feeds.USER_FEEDSPHOTOS_REQUEST:
            return {
                ...state,
                userFeedsPhotosProgress: true,
            }
        case Feeds.USER_FEEDSPHOTOS_SUCCESS:
            return {
                ...state,
                userFeedsPhotosProgress: false,
                userFeedsPhotos: action.payload,
                errorFeedsPhotos: {}
            }
        case Feeds.USER_FEEDSPHOTOS_FAILURE:
            return {
                ...state,
                userFeedsPhotosProgress: false,
                userFeedsPhotos: {},
                errorFeedsPhotos: action.error
            }


        case Feeds.USER_FEEDSVIDEOS_REQUEST:
            return {
                ...state,
                userFeedsVideosProgress: true,
            }
        case Feeds.USER_FEEDSVIDEOS_SUCCESS:
            return {
                ...state,
                userFeedsVideosProgress: false,
                userFeedsVideos: action.payload,
                errorFeedsVideos: {}
            }
        case Feeds.USER_FEEDSVIDEOS_FAILURE:
            return {
                ...state,
                userFeedsVideosProgress: false,
                userFeedsVideos: {},
                errorFeedsVideos: action.error
            }

        default:
            return state
    }
}

export default feedsReducer
