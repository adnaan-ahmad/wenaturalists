import { Notification } from '../../ActionTypes/User/Notification'

const initialState = {
    notificationProgress : false,
    user : [],
    error : {}
}

const notificationReducer = (state = initialState, action) => {
    switch (action.type){
        case Notification.USER_NOTIFICATION_REQUEST :
            return {
                ...state,
                notificationProgress: true,
            }
        case Notification.USER_NOTIFICATION_SUCCESS :
            return {
                ...state,
                notificationProgress: false,
                user: action.payload,
                error: {}
            }
        case Notification.USER_NOTIFICATION_FAILURE :
            return {
                ...state,
                notificationProgress: false,
                user: {},
                error: action.error
            }
        default :
        return state
    }
}

export default notificationReducer