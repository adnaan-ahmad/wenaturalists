import { Connects } from '../../ActionTypes/User/ConnectsType'

const initialState = {
    userConnectsProgress : false,
    userConnects : {},
    errorConnects : {}
}

const connectsReducer = (state = initialState, action) => {
    switch (action.type){
        case Connects.USER_CONNECTS_REQUEST :
            return {
                ...state,
                userConnectsProgress: true,
            }
        case Connects.USER_CONNECTS_SUCCESS :
            return {
                ...state,
                userConnectsProgress: false,
                userConnects: action.payload,
                errorConnects: {}
            }
        case Connects.USER_CONNECTS_FAILURE :
            return {
                ...state,
                userConnectsProgress: false,
                userConnects: {},
                errorConnects: action.error
            }
        default :
        return state
    }
}

export default connectsReducer
