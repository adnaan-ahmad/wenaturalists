import { Causes } from '../../ActionTypes/User/CausesType'

const initialState = {
    userCausesProgress : false,
    userCauses : {},
    errorCauses : {}
}

const causesReducer = (state = initialState, action) => {
    switch (action.type){
        case Causes.USER_CAUSES_REQUEST :
            return {
                ...state,
                userCausesProgress: true,
            }
        case Causes.USER_CAUSES_SUCCESS :
            return {
                ...state,
                userCausesProgress: false,
                userCauses: action.payload,
                errorCauses: {}
            }
        case Causes.USER_CAUSES_FAILURE :
            return {
                ...state,
                userCausesProgress: false,
                userCauses: {},
                errorCauses: action.error
            }
        default :
        return state
    }
}

export default causesReducer
