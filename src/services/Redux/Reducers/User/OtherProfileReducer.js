import { OtherProfileType } from '../../ActionTypes/User/OtherProfileType'

const initialState = {
    userDataProgress: false,
    user: {},
    error: {},

    userContactProgress: false,
    userContact: {},
    errorContact: {},

    userAddressProgress: false,
    userAddress: {},
    errorAddress: {},

    userBioProgress: false,
    userBio: {},
    errorBio: {},

    userSkillsProgress: false,
    userSkills: {},
    errorSkills: {},

    userSkillsSpecializationProgress: false,
    userSkillsSpecialization: {},
    errorSkillsSpecialization: {},

    userRecentActivityProgress: false,
    userRecentActivity: {},
    errorRecentActivity: {},

    userExperienceProgress: false,
    userExperience: {},
    errorExperience: {},

    userEducationProgress: false,
    userEducation: {},
    errorEducation: {},

    userBusinessPageProgress: false,
    userBusinessPage: {},
    errorBusinessPage: {},

    userInterestsProgress: false,
    userInterests: {},
    errorInterests: {},

    userHashTagsProgress: false,
    userHashTags: {},
    errorHashTags: {},

    userSpecialitiesProgress: false,
    userSpecialities: {},
    errorSpecialities: {},

    userConnectionInfoProgress: false,
    userConnectionInfo: {},
    errorConnectionInfo: {},
}

const otherProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case OtherProfileType.USER_DATA_REQUEST:
            return {
                ...state,
                userDataProgress: true,
            };
        case OtherProfileType.USER_DATA_SUCCESS:
            return {
                ...state,
                userDataProgress: false,
                user: action.payload,
                error: {}
            };
        case OtherProfileType.USER_DATA_FAILURE:
            return {
                ...state,
                userDataProgress: false,
                user: {},
                error: action.error
            };



        case OtherProfileType.USER_CONTACT_REQUEST:
            return {
                ...state,
                userContactProgress: true,
            };
        case OtherProfileType.USER_CONTACT_SUCCESS:
            return {
                ...state,
                userContactProgress: false,
                userContact: action.payload,
                errorContact: {}
            };
        case OtherProfileType.USER_CONTACT_FAILURE:
            return {
                ...state,
                userContactProgress: false,
                userContact: {},
                errorContact: action.error
            };



        case OtherProfileType.USER_ADDRESS_REQUEST:
            return {
                ...state,
                userAddressProgress: true,
            };
        case OtherProfileType.USER_ADDRESS_SUCCESS:
            return {
                ...state,
                userAddressProgress: false,
                userAddress: action.payload,
                errorAddress: {}
            };
        case OtherProfileType.USER_ADDRESS_FAILURE:
            return {
                ...state,
                userAddressProgress: false,
                userAddress: {},
                errorAddress: action.error
            };



        case OtherProfileType.USER_BIO_REQUEST:
            return {
                ...state,
                userBioProgress: true,
            };
        case OtherProfileType.USER_BIO_SUCCESS:
            return {
                ...state,
                userBioProgress: false,
                userBio: action.payload,
                errorBio: {}
            };
        case OtherProfileType.USER_BIO_FAILURE:
            return {
                ...state,
                userBioProgress: false,
                userBio: {},
                errorBio: action.error
            };



        case OtherProfileType.USER_SKILLS_REQUEST:
            return {
                ...state,
                userSkillsProgress: true,
            };
        case OtherProfileType.USER_SKILLS_SUCCESS:
            return {
                ...state,
                userSkillsProgress: false,
                userSkills: action.payload,
                errorSkills: {}
            };
        case OtherProfileType.USER_SKILLS_FAILURE:
            return {
                ...state,
                userSkillsProgress: false,
                userSkills: {},
                errorSkills: action.error
            };



        case OtherProfileType.USER_SKILLSSPECIALIZATION_REQUEST:
            return {
                ...state,
                userSkillsSpecializationProgress: true,
            };
        case OtherProfileType.USER_SKILLSSPECIALIZATION_SUCCESS:
            return {
                ...state,
                userSkillsSpecializationProgress: false,
                userSkillsSpecialization: action.payload,
                errorSkillsSpecialization: {}
            };
        case OtherProfileType.USER_SKILLSSPECIALIZATION_FAILURE:
            return {
                ...state,
                userSkillsSpecializationProgress: false,
                userSkillsSpecialization: {},
                errorSkillsSpecialization: action.error
            };



        case OtherProfileType.USER_RECENTACTIVITY_REQUEST:
            return {
                ...state,
                userRecentActivityProgress: true,
            };
        case OtherProfileType.USER_RECENTACTIVITY_SUCCESS:
            return {
                ...state,
                userRecentActivityProgress: false,
                userRecentActivity: action.payload,
                errorRecentActivity: {}
            };
        case OtherProfileType.USER_RECENTACTIVITY_FAILURE:
            return {
                ...state,
                userRecentActivityProgress: false,
                userRecentActivity: {},
                errorRecentActivity: action.error
            };




        case OtherProfileType.USER_EXPERIENCE_REQUEST:
            return {
                ...state,
                userExperienceProgress: true,
            };
        case OtherProfileType.USER_EXPERIENCE_SUCCESS:
            return {
                ...state,
                userExperienceProgress: false,
                userExperience: action.payload,
                errorExperience: {}
            };
        case OtherProfileType.USER_EXPERIENCE_FAILURE:
            return {
                ...state,
                userExperienceProgress: false,
                userExperience: {},
                errorExperience: action.error
            };




        case OtherProfileType.USER_EDUCATION_REQUEST:
            return {
                ...state,
                userEducationProgress: true,
            };
        case OtherProfileType.USER_EDUCATION_SUCCESS:
            return {
                ...state,
                userEducationProgress: false,
                userEducation: action.payload,
                errorEducation: {}
            };
        case OtherProfileType.USER_EDUCATION_FAILURE:
            return {
                ...state,
                userEducationProgress: false,
                userEducation: {},
                errorEducation: action.error
            };



        case OtherProfileType.USER_BUSINESS_PAGE_REQUEST:
            return {
                ...state,
                userBusinessPageProgress: true,
            };
        case OtherProfileType.USER_BUSINESS_PAGE_SUCCESS:
            return {
                ...state,
                userBusinessPageProgress: false,
                userBusinessPage: action.payload,
                errorBusinessPage: {}
            };
        case OtherProfileType.USER_BUSINESS_PAGE_FAILURE:
            return {
                ...state,
                userBusinessPageProgress: false,
                userBusinessPage: {},
                errorBusinessPage: action.error
            };



        case OtherProfileType.USER_INTERESTS_REQUEST:
            return {
                ...state,
                userInterestsProgress: true,
            };
        case OtherProfileType.USER_INTERESTS_SUCCESS:
            return {
                ...state,
                userInterestsProgress: false,
                userInterests: action.payload,
                errorInterests: {}
            };
        case OtherProfileType.USER_INTERESTS_FAILURE:
            return {
                ...state,
                userInterestsProgress: false,
                userInterests: {},
                errorInterests: action.error
            };




        case OtherProfileType.USER_HASHTAGS_REQUEST:
            return {
                ...state,
                userHashTagsProgress: true,
            };
        case OtherProfileType.USER_HASHTAGS_SUCCESS:
            return {
                ...state,
                userHashTagsProgress: false,
                userHashTags: action.payload,
                errorHashTags: {}
            };
        case OtherProfileType.USER_HASHTAGS_FAILURE:
            return {
                ...state,
                userHashTagsProgress: false,
                userHashTags: {},
                errorHashTags: action.error
            };




        case OtherProfileType.USER_SPECIALITIES_REQUEST:
            return {
                ...state,
                userSpecialitiesProgress: true,
            };
        case OtherProfileType.USER_SPECIALITIES_SUCCESS:
            return {
                ...state,
                userSpecialitiesProgress: false,
                userSpecialities: action.payload,
                errorSpecialities: {}
            };
        case OtherProfileType.USER_SPECIALITIES_FAILURE:
            return {
                ...state,
                userSpecialitiesProgress: false,
                userSpecialities: {},
                errorSpecialities: action.error
            };




        case OtherProfileType.USER_CONNECTIONINFO_REQUEST:
            return {
                ...state,
                userConnectionInfoProgress: true,
            };
        case OtherProfileType.USER_CONNECTIONINFO_SUCCESS:
            return {
                ...state,
                userConnectionInfoProgress: false,
                userConnectionInfo: action.payload,
                errorConnectionInfo: {}
            };
        case OtherProfileType.USER_CONNECTIONINFO_FAILURE:
            return {
                ...state,
                userConnectionInfoProgress: false,
                userConnectionInfo: {},
                errorConnectionInfo: action.error
            };



        default:
            return state
    }
}

export default otherProfileReducer
