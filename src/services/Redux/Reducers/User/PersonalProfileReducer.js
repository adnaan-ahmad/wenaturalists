import { PersonalProfileType } from '../../ActionTypes/User/PersonalProfileType'

const initialState = {
    userDataProgress: false,
    user: {},
    error: {},

    userContactProgress: false,
    userContact: {},
    errorContact: {},

    userAddressProgress: false,
    userAddress: {},
    errorAddress: {},

    userBioProgress: false,
    userBio: {},
    errorBio: {},

    userSkillsProgress: false,
    userSkills: {},
    errorSkills: {},

    userSkillsSpecializationProgress: false,
    userSkillsSpecialization: {},
    errorSkillsSpecialization: {},

    userRecentActivityProgress: false,
    userRecentActivity: {},
    errorRecentActivity: {},

    userExperienceProgress: false,
    userExperience: {},
    errorExperience: {},

    userEducationProgress: false,
    userEducation: {},
    errorEducation: {},

    userBusinessPageProgress: false,
    userBusinessPage: {},
    errorBusinessPage: {},

    userInterestsProgress: false,
    userInterests: {},
    errorInterests: {},

    userHashTagsProgress: false,
    userHashTags: {},
    errorHashTags: {},

    userSpecialitiesProgress: false,
    userSpecialities: {},
    errorSpecialities: {},

    userConnectionInfoProgress: false,
    userConnectionInfo: {},
    errorConnectionInfo: {},
}

const personalProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case PersonalProfileType.USER_DATA_REQUEST:
            return {
                ...state,
                userDataProgress: true,
            };
        case PersonalProfileType.USER_DATA_SUCCESS:
            return {
                ...state,
                userDataProgress: false,
                user: action.payload,
                error: {}
            };
        case PersonalProfileType.USER_DATA_FAILURE:
            return {
                ...state,
                userDataProgress: false,
                user: {},
                error: action.error
            };



        case PersonalProfileType.USER_CONTACT_REQUEST:
            return {
                ...state,
                userContactProgress: true,
            };
        case PersonalProfileType.USER_CONTACT_SUCCESS:
            return {
                ...state,
                userContactProgress: false,
                userContact: action.payload,
                errorContact: {}
            };
        case PersonalProfileType.USER_CONTACT_FAILURE:
            return {
                ...state,
                userContactProgress: false,
                userContact: {},
                errorContact: action.error
            };



        case PersonalProfileType.USER_ADDRESS_REQUEST:
            return {
                ...state,
                userAddressProgress: true,
            };
        case PersonalProfileType.USER_ADDRESS_SUCCESS:
            return {
                ...state,
                userAddressProgress: false,
                userAddress: action.payload,
                errorAddress: {}
            };
        case PersonalProfileType.USER_ADDRESS_FAILURE:
            return {
                ...state,
                userAddressProgress: false,
                userAddress: {},
                errorAddress: action.error
            };



        case PersonalProfileType.USER_BIO_REQUEST:
            return {
                ...state,
                userBioProgress: true,
            };
        case PersonalProfileType.USER_BIO_SUCCESS:
            return {
                ...state,
                userBioProgress: false,
                userBio: action.payload,
                errorBio: {}
            };
        case PersonalProfileType.USER_BIO_FAILURE:
            return {
                ...state,
                userBioProgress: false,
                userBio: {},
                errorBio: action.error
            };



        case PersonalProfileType.USER_SKILLS_REQUEST:
            return {
                ...state,
                userSkillsProgress: true,
            };
        case PersonalProfileType.USER_SKILLS_SUCCESS:
            return {
                ...state,
                userSkillsProgress: false,
                userSkills: action.payload,
                errorSkills: {}
            };
        case PersonalProfileType.USER_SKILLS_FAILURE:
            return {
                ...state,
                userSkillsProgress: false,
                userSkills: {},
                errorSkills: action.error
            };



        case PersonalProfileType.USER_SKILLSSPECIALIZATION_REQUEST:
            return {
                ...state,
                userSkillsSpecializationProgress: true,
            };
        case PersonalProfileType.USER_SKILLSSPECIALIZATION_SUCCESS:
            return {
                ...state,
                userSkillsSpecializationProgress: false,
                userSkillsSpecialization: action.payload,
                errorSkillsSpecialization: {}
            };
        case PersonalProfileType.USER_SKILLSSPECIALIZATION_FAILURE:
            return {
                ...state,
                userSkillsSpecializationProgress: false,
                userSkillsSpecialization: {},
                errorSkillsSpecialization: action.error
            };



        case PersonalProfileType.USER_RECENTACTIVITY_REQUEST:
            return {
                ...state,
                userRecentActivityProgress: true,
            };
        case PersonalProfileType.USER_RECENTACTIVITY_SUCCESS:
            return {
                ...state,
                userRecentActivityProgress: false,
                userRecentActivity: action.payload,
                errorRecentActivity: {}
            };
        case PersonalProfileType.USER_RECENTACTIVITY_FAILURE:
            return {
                ...state,
                userRecentActivityProgress: false,
                userRecentActivity: {},
                errorRecentActivity: action.error
            };




        case PersonalProfileType.USER_EXPERIENCE_REQUEST:
            return {
                ...state,
                userExperienceProgress: true,
            };
        case PersonalProfileType.USER_EXPERIENCE_SUCCESS:
            return {
                ...state,
                userExperienceProgress: false,
                userExperience: action.payload,
                errorExperience: {}
            };
        case PersonalProfileType.USER_EXPERIENCE_FAILURE:
            return {
                ...state,
                userExperienceProgress: false,
                userExperience: {},
                errorExperience: action.error
            };




        case PersonalProfileType.USER_EDUCATION_REQUEST:
            return {
                ...state,
                userEducationProgress: true,
            };
        case PersonalProfileType.USER_EDUCATION_SUCCESS:
            return {
                ...state,
                userEducationProgress: false,
                userEducation: action.payload,
                errorEducation: {}
            };
        case PersonalProfileType.USER_EDUCATION_FAILURE:
            return {
                ...state,
                userEducationProgress: false,
                userEducation: {},
                errorEducation: action.error
            };



        case PersonalProfileType.USER_BUSINESS_PAGE_REQUEST:
            return {
                ...state,
                userBusinessPageProgress: true,
            };
        case PersonalProfileType.USER_BUSINESS_PAGE_SUCCESS:
            return {
                ...state,
                userBusinessPageProgress: false,
                userBusinessPage: action.payload,
                errorBusinessPage: {}
            };
        case PersonalProfileType.USER_BUSINESS_PAGE_FAILURE:
            return {
                ...state,
                userBusinessPageProgress: false,
                userBusinessPage: {},
                errorBusinessPage: action.error
            };



        case PersonalProfileType.USER_INTERESTS_REQUEST:
            return {
                ...state,
                userInterestsProgress: true,
            };
        case PersonalProfileType.USER_INTERESTS_SUCCESS:
            return {
                ...state,
                userInterestsProgress: false,
                userInterests: action.payload,
                errorInterests: {}
            };
        case PersonalProfileType.USER_INTERESTS_FAILURE:
            return {
                ...state,
                userInterestsProgress: false,
                userInterests: {},
                errorInterests: action.error
            };




        case PersonalProfileType.USER_HASHTAGS_REQUEST:
            return {
                ...state,
                userHashTagsProgress: true,
            };
        case PersonalProfileType.USER_HASHTAGS_SUCCESS:
            return {
                ...state,
                userHashTagsProgress: false,
                userHashTags: action.payload,
                errorHashTags: {}
            };
        case PersonalProfileType.USER_HASHTAGS_FAILURE:
            return {
                ...state,
                userHashTagsProgress: false,
                userHashTags: {},
                errorHashTags: action.error
            };




        case PersonalProfileType.USER_SPECIALITIES_REQUEST:
            return {
                ...state,
                userSpecialitiesProgress: true,
            };
        case PersonalProfileType.USER_SPECIALITIES_SUCCESS:
            return {
                ...state,
                userSpecialitiesProgress: false,
                userSpecialities: action.payload,
                errorSpecialities: {}
            };
        case PersonalProfileType.USER_SPECIALITIES_FAILURE:
            return {
                ...state,
                userSpecialitiesProgress: false,
                userSpecialities: {},
                errorSpecialities: action.error
            };




        case PersonalProfileType.USER_CONNECTIONINFO_REQUEST:
            return {
                ...state,
                userConnectionInfoProgress: true,
            };
        case PersonalProfileType.USER_CONNECTIONINFO_SUCCESS:
            return {
                ...state,
                userConnectionInfoProgress: false,
                userConnectionInfo: action.payload,
                errorConnectionInfo: {}
            };
        case PersonalProfileType.USER_CONNECTIONINFO_FAILURE:
            return {
                ...state,
                userConnectionInfoProgress: false,
                userConnectionInfo: {},
                errorConnectionInfo: action.error
            };



        default:
            return state
    }
}

export default personalProfileReducer
