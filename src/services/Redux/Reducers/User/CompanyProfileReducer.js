import { CompanyProfileType } from '../../ActionTypes/User/CompanyProfileType'

const initialState = {
    userDataProgress: false,
    user: {},
    error: {},

    userContactProgress: false,
    userContact: {},
    errorContact: {},

    userAddressProgress: false,
    userAddress: {},
    errorAddress: {},

    userBioProgress: false,
    userBio: {},
    errorBio: {},

    userSkillsProgress: false,
    userSkills: {},
    errorSkills: {},

    userSkillsSpecializationProgress: false,
    userSkillsSpecialization: {},
    errorSkillsSpecialization: {},

    userRecentActivityProgress: false,
    userRecentActivity: {},
    errorRecentActivity: {},

    userExperienceProgress: false,
    userExperience: {},
    errorExperience: {},

    userEducationProgress: false,
    userEducation: {},
    errorEducation: {},

    userBusinessPageProgress: false,
    userBusinessPage: {},
    errorBusinessPage: {},

    userInterestsProgress: false,
    userInterests: {},
    errorInterests: {},

    userHashTagsProgress: false,
    userHashTags: {},
    errorHashTags: {},

    userSpecialitiesProgress: false,
    userSpecialities: {},
    errorSpecialities: {},

    userConnectionInfoProgress: false,
    userConnectionInfo: {},
    errorConnectionInfo: {},
}

const companyProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case CompanyProfileType.USER_DATA_REQUEST:
            return {
                ...state,
                userDataProgress: true,
            };
        case CompanyProfileType.USER_DATA_SUCCESS:
            return {
                ...state,
                userDataProgress: false,
                user: action.payload,
                error: {}
            };
        case CompanyProfileType.USER_DATA_FAILURE:
            return {
                ...state,
                userDataProgress: false,
                user: {},
                error: action.error
            };



        case CompanyProfileType.USER_CONTACT_REQUEST:
            return {
                ...state,
                userContactProgress: true,
            };
        case CompanyProfileType.USER_CONTACT_SUCCESS:
            return {
                ...state,
                userContactProgress: false,
                userContact: action.payload,
                errorContact: {}
            };
        case CompanyProfileType.USER_CONTACT_FAILURE:
            return {
                ...state,
                userContactProgress: false,
                userContact: {},
                errorContact: action.error
            };



        case CompanyProfileType.USER_ADDRESS_REQUEST:
            return {
                ...state,
                userAddressProgress: true,
            };
        case CompanyProfileType.USER_ADDRESS_SUCCESS:
            return {
                ...state,
                userAddressProgress: false,
                userAddress: action.payload,
                errorAddress: {}
            };
        case CompanyProfileType.USER_ADDRESS_FAILURE:
            return {
                ...state,
                userAddressProgress: false,
                userAddress: {},
                errorAddress: action.error
            };



        case CompanyProfileType.USER_BIO_REQUEST:
            return {
                ...state,
                userBioProgress: true,
            };
        case CompanyProfileType.USER_BIO_SUCCESS:
            return {
                ...state,
                userBioProgress: false,
                userBio: action.payload,
                errorBio: {}
            };
        case CompanyProfileType.USER_BIO_FAILURE:
            return {
                ...state,
                userBioProgress: false,
                userBio: {},
                errorBio: action.error
            };



        case CompanyProfileType.USER_SKILLS_REQUEST:
            return {
                ...state,
                userSkillsProgress: true,
            };
        case CompanyProfileType.USER_SKILLS_SUCCESS:
            return {
                ...state,
                userSkillsProgress: false,
                userSkills: action.payload,
                errorSkills: {}
            };
        case CompanyProfileType.USER_SKILLS_FAILURE:
            return {
                ...state,
                userSkillsProgress: false,
                userSkills: {},
                errorSkills: action.error
            };



        case CompanyProfileType.USER_SKILLSSPECIALIZATION_REQUEST:
            return {
                ...state,
                userSkillsSpecializationProgress: true,
            };
        case CompanyProfileType.USER_SKILLSSPECIALIZATION_SUCCESS:
            return {
                ...state,
                userSkillsSpecializationProgress: false,
                userSkillsSpecialization: action.payload,
                errorSkillsSpecialization: {}
            };
        case CompanyProfileType.USER_SKILLSSPECIALIZATION_FAILURE:
            return {
                ...state,
                userSkillsSpecializationProgress: false,
                userSkillsSpecialization: {},
                errorSkillsSpecialization: action.error
            };



        case CompanyProfileType.USER_RECENTACTIVITY_REQUEST:
            return {
                ...state,
                userRecentActivityProgress: true,
            };
        case CompanyProfileType.USER_RECENTACTIVITY_SUCCESS:
            return {
                ...state,
                userRecentActivityProgress: false,
                userRecentActivity: action.payload,
                errorRecentActivity: {}
            };
        case CompanyProfileType.USER_RECENTACTIVITY_FAILURE:
            return {
                ...state,
                userRecentActivityProgress: false,
                userRecentActivity: {},
                errorRecentActivity: action.error
            };




        case CompanyProfileType.USER_EXPERIENCE_REQUEST:
            return {
                ...state,
                userExperienceProgress: true,
            };
        case CompanyProfileType.USER_EXPERIENCE_SUCCESS:
            return {
                ...state,
                userExperienceProgress: false,
                userExperience: action.payload,
                errorExperience: {}
            };
        case CompanyProfileType.USER_EXPERIENCE_FAILURE:
            return {
                ...state,
                userExperienceProgress: false,
                userExperience: {},
                errorExperience: action.error
            };




        case CompanyProfileType.USER_EDUCATION_REQUEST:
            return {
                ...state,
                userEducationProgress: true,
            };
        case CompanyProfileType.USER_EDUCATION_SUCCESS:
            return {
                ...state,
                userEducationProgress: false,
                userEducation: action.payload,
                errorEducation: {}
            };
        case CompanyProfileType.USER_EDUCATION_FAILURE:
            return {
                ...state,
                userEducationProgress: false,
                userEducation: {},
                errorEducation: action.error
            };



        case CompanyProfileType.USER_BUSINESS_PAGE_REQUEST:
            return {
                ...state,
                userBusinessPageProgress: true,
            };
        case CompanyProfileType.USER_BUSINESS_PAGE_SUCCESS:
            return {
                ...state,
                userBusinessPageProgress: false,
                userBusinessPage: action.payload,
                errorBusinessPage: {}
            };
        case CompanyProfileType.USER_BUSINESS_PAGE_FAILURE:
            return {
                ...state,
                userBusinessPageProgress: false,
                userBusinessPage: {},
                errorBusinessPage: action.error
            };



        case CompanyProfileType.USER_INTERESTS_REQUEST:
            return {
                ...state,
                userInterestsProgress: true,
            };
        case CompanyProfileType.USER_INTERESTS_SUCCESS:
            return {
                ...state,
                userInterestsProgress: false,
                userInterests: action.payload,
                errorInterests: {}
            };
        case CompanyProfileType.USER_INTERESTS_FAILURE:
            return {
                ...state,
                userInterestsProgress: false,
                userInterests: {},
                errorInterests: action.error
            };




        case CompanyProfileType.USER_HASHTAGS_REQUEST:
            return {
                ...state,
                userHashTagsProgress: true,
            };
        case CompanyProfileType.USER_HASHTAGS_SUCCESS:
            return {
                ...state,
                userHashTagsProgress: false,
                userHashTags: action.payload,
                errorHashTags: {}
            };
        case CompanyProfileType.USER_HASHTAGS_FAILURE:
            return {
                ...state,
                userHashTagsProgress: false,
                userHashTags: {},
                errorHashTags: action.error
            };




        case CompanyProfileType.USER_SPECIALITIES_REQUEST:
            return {
                ...state,
                userSpecialitiesProgress: true,
            };
        case CompanyProfileType.USER_SPECIALITIES_SUCCESS:
            return {
                ...state,
                userSpecialitiesProgress: false,
                userSpecialities: action.payload,
                errorSpecialities: {}
            };
        case CompanyProfileType.USER_SPECIALITIES_FAILURE:
            return {
                ...state,
                userSpecialitiesProgress: false,
                userSpecialities: {},
                errorSpecialities: action.error
            };




        case CompanyProfileType.USER_CONNECTIONINFO_REQUEST:
            return {
                ...state,
                userConnectionInfoProgress: true,
            };
        case CompanyProfileType.USER_CONNECTIONINFO_SUCCESS:
            return {
                ...state,
                userConnectionInfoProgress: false,
                userConnectionInfo: action.payload,
                errorConnectionInfo: {}
            };
        case CompanyProfileType.USER_CONNECTIONINFO_FAILURE:
            return {
                ...state,
                userConnectionInfoProgress: false,
                userConnectionInfo: {},
                errorConnectionInfo: action.error
            };



        default:
            return state
    }
}

export default companyProfileReducer
