import { Circle } from '../../ActionTypes/User/CircleType'

const initialState = {
    userCircleProgress : false,
    userCircle : {},
    errorCircle : {}
}

const circleReducer = (state = initialState, action) => {
    switch (action.type){
        case Circle.USER_CIRCLE_REQUEST :
            return {
                ...state,
                userCircleProgress: true,
            }
        case Circle.USER_CIRCLE_SUCCESS :
            return {
                ...state,
                userCircleProgress: false,
                userCircle: action.payload,
                errorCircle: {}
            }
        case Circle.USER_CIRCLE_FAILURE :
            return {
                ...state,
                userCircleProgress: false,
                userCircle: {},
                errorCircle: action.error
            }
        default :
        return state
    }
}

export default circleReducer
