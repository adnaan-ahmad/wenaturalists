import { EndorsementType } from '../../ActionTypes/User/EndorsementType'

const initialState = {
    userEndorsementProgress: false,
    userEndorsement: {},
    errorEndorsement: {},

    userEndorsementDetailProgress: false,
    userEndorsementDetail: {},
    errorEndorsementDetail: {}
}

const endorsementReducer = (state = initialState, action) => {
    switch (action.type) {

        case EndorsementType.USER_ENDORSEMENT_REQUEST:
            return {
                ...state,
                userEndorsementProgress: true,
            };
        case EndorsementType.USER_ENDORSEMENT_SUCCESS:
            return {
                ...state,
                userEndorsementProgress: false,
                userEndorsement: action.payload,
                errorEndorsement: {}
            };
        case EndorsementType.USER_ENDORSEMENT_FAILURE:
            return {
                ...state,
                userEndorsementProgress: false,
                userEndorsement: {},
                errorEndorsement: action.error
            }


        
        case EndorsementType.USER_ENDORSEMENTDETAIL_REQUEST:
            return {
                ...state,
                userEndorsementDetailProgress: true,
            };
        case EndorsementType.USER_ENDORSEMENTDETAIL_SUCCESS:
            return {
                ...state,
                userEndorsementDetailProgress: false,
                userEndorsementDetail: action.payload,
                errorEndorsementDetail: {}
            };
        case EndorsementType.USER_ENDORSEMENTDETAIL_FAILURE:
            return {
                ...state,
                userEndorsementDetailProgress: false,
                userEndorsementDetail: {},
                errorEndorsementDetail: action.error
            }


        default:
            return state
    }
}

export default endorsementReducer
