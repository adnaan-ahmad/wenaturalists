export const Causes = {
    USER_CAUSES_REQUEST : 'USER_CAUSES_REQUEST',
    USER_CAUSES_SUCCESS : 'USER_CAUSES_SUCCESS',
    USER_CAUSES_FAILURE : 'USER_CAUSES_FAILURE'
}
