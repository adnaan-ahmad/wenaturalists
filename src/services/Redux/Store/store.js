import {createStore, applyMiddleware} from 'redux';
import {createLogger} from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../Sagas';
import RootReducer from '../Reducers/rootReducer';

const logger = createLogger({
    predicate: (getState,action)=>action.type !== 'CHANGE_FORM'
});

const sagaMiddleware = createSagaMiddleware();
const store = createStore(RootReducer, applyMiddleware(logger,sagaMiddleware));
sagaMiddleware.run(rootSaga);

export default store;