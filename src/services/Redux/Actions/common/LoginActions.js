import {Auth} from '../../ActionTypes/common/Auth'

export const loginRequest = (data) =>{
    return {
        type : Auth.USER_LOGIN_REQUEST,
        payload: data
    }
}

export const loginSuccess = (user) =>{
    return {
        type : Auth.USER_LOGIN_SUCCESS,
        payload : user,
        error : {}
    }
}

export const loginFailure = (error) =>{
    return {
        type : Auth.USER_LOGIN_FAILURE,
        payload : {},
        error : error
    }
}

export const onClear = () =>{
    return {
        type : Auth.USER_LOGIN_CLEAR,
        payload : {},
        error : {}
    }
}