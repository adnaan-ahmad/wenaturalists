import { Circle } from '../../ActionTypes/User/CircleType'

export const userCircleRequest = (data) => {
    return {
        type : Circle.USER_CIRCLE_REQUEST,
        payload: data
    }
}

export const userCircleSuccess = (user) => {
    return {
        type : Circle.USER_CIRCLE_SUCCESS,
        payload : user,
        error : {}
    }
}

export const userCircleFailure = (error) => {
    return {
        type : Circle.USER_CIRCLE_FAILURE,
        payload : {},
        error : error
    }
}
