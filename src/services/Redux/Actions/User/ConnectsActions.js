import { Connects } from '../../ActionTypes/User/ConnectsType'

export const connectsRequest = (data) => {
    return {
        type : Connects.USER_CONNECTS_REQUEST,
        payload: data
    }
}

export const connectsSuccess = (user) => {
    return {
        type : Connects.USER_CONNECTS_SUCCESS,
        payload : user,
        error : {}
    }
}

export const connectsFailure = (error) => {
    return {
        type : Connects.USER_CONNECTS_FAILURE,
        payload : {},
        error : error
    }
}
