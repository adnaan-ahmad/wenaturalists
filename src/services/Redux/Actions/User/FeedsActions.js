import { Feeds } from '../../ActionTypes/User/FeedsType'

export const feedsPhotosRequest = (data) => {
    return {
        type : Feeds.USER_FEEDSPHOTOS_REQUEST,
        payload: data
    }
}

export const feedsPhotosSuccess = (user) => {
    return {
        type : Feeds.USER_FEEDSPHOTOS_SUCCESS,
        payload : user,
        error : {}
    }
}

export const feedsPhotosFailure = (error) => {
    return {
        type : Feeds.USER_FEEDSPHOTOS_FAILURE,
        payload : {},
        error : error
    }
}




export const feedsVideosRequest = (data) => {
    return {
        type : Feeds.USER_FEEDSVIDEOS_REQUEST,
        payload: data
    }
}

export const feedsVideosSuccess = (user) => {
    return {
        type : Feeds.USER_FEEDSVIDEOS_SUCCESS,
        payload : user,
        error : {}
    }
}

export const feedsVideosFailure = (error) => {
    return {
        type : Feeds.USER_FEEDSVIDEOS_FAILURE,
        payload : {},
        error : error
    }
}
