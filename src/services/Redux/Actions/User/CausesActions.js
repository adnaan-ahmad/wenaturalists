import { Causes } from '../../ActionTypes/User/CausesType'

export const causesRequest = (data) => {
    return {
        type : Causes.USER_CAUSES_REQUEST,
        payload: data
    }
}

export const causesSuccess = (user) => {
    return {
        type : Causes.USER_CAUSES_SUCCESS,
        payload : user,
        error : {}
    }
}

export const causesFailure = (error) => {
    return {
        type : Causes.USER_CAUSES_FAILURE,
        payload : {},
        error : error
    }
}
