import {PersonalProfileType} from '../../ActionTypes/User/PersonalProfileType'

export const personalProfileRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_DATA_REQUEST,
        payload: data
    }
}
export const personalProfileSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_DATA_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalProfileFailure = (error) => {
    return {
        type : PersonalProfileType.USER_DATA_FAILURE,
        payload : {},
        error : error
    }
}




export const personalContactRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_CONTACT_REQUEST,
        payload: data
    }
}
export const personalContactSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_CONTACT_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalContactFailure = (error) => {
    return {
        type : PersonalProfileType.USER_CONTACT_FAILURE,
        payload : {},
        error : error
    }
}




export const personalAddressRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_ADDRESS_REQUEST,
        payload: data
    }
}
export const personalAddressSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_ADDRESS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalAddressFailure = (error) => {
    return {
        type : PersonalProfileType.USER_ADDRESS_FAILURE,
        payload : {},
        error : error
    }
}



export const personalBioRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_BIO_REQUEST,
        payload: data
    }
}
export const personalBioSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_BIO_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalBioFailure = (error) => {
    return {
        type : PersonalProfileType.USER_BIO_FAILURE,
        payload : {},
        error : error
    }
}


export const personalSkillsRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_SKILLS_REQUEST,
        payload: data
    }
}
export const personalSkillsSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_SKILLS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalSkillsFailure = (error) => {
    return {
        type : PersonalProfileType.USER_SKILLS_FAILURE,
        payload : {},
        error : error
    }
}



export const personalSkillsSpecializationRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_SKILLSSPECIALIZATION_REQUEST,
        payload: data
    }
}
export const personalSkillsSpecializationSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_SKILLSSPECIALIZATION_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalSkillsSpecializationFailure = (error) => {
    return {
        type : PersonalProfileType.USER_SKILLSSPECIALIZATION_FAILURE,
        payload : {},
        error : error
    }
}




export const personalRecentActivityRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_RECENTACTIVITY_REQUEST,
        payload: data
    }
}
export const personalRecentActivitySuccess = (user) => {
    return {
        type : PersonalProfileType.USER_RECENTACTIVITY_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalRecentActivityFailure = (error) => {
    return {
        type : PersonalProfileType.USER_RECENTACTIVITY_FAILURE,
        payload : {},
        error : error
    }
}



export const personalExperienceRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_EXPERIENCE_REQUEST,
        payload: data
    }
}
export const personalExperienceSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_EXPERIENCE_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalExperienceFailure = (error) => {
    return {
        type : PersonalProfileType.USER_EXPERIENCE_FAILURE,
        payload : {},
        error : error
    }
}




export const personalEducationRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_EDUCATION_REQUEST,
        payload: data
    }
}
export const personalEducationSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_EDUCATION_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalEducationFailure = (error) => {
    return {
        type : PersonalProfileType.USER_EDUCATION_FAILURE,
        payload : {},
        error : error
    }
}




export const personalBusinessPageRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_BUSINESS_PAGE_REQUEST,
        payload: data
    }
}
export const personalBusinessPageSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_BUSINESS_PAGE_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalBusinessPageFailure = (error) => {
    return {
        type : PersonalProfileType.USER_BUSINESS_PAGE_FAILURE,
        payload : {},
        error : error
    }
}




export const personalInterestsRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_INTERESTS_REQUEST,
        payload: data
    }
}
export const personalInterestsSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_INTERESTS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalInterestsFailure = (error) => {
    return {
        type : PersonalProfileType.USER_INTERESTS_FAILURE,
        payload : {},
        error : error
    }
}



export const personalHashTagsRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_HASHTAGS_REQUEST,
        payload: data
    }
}
export const personalHashTagsSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_HASHTAGS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalHashTagsFailure = (error) => {
    return {
        type : PersonalProfileType.USER_HASHTAGS_FAILURE,
        payload : {},
        error : error
    }
}




export const personalSpecialitiesRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_SPECIALITIES_REQUEST,
        payload: data
    }
}
export const personalSpecialitiesSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_SPECIALITIES_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalSpecialitiesFailure = (error) => {
    return {
        type : PersonalProfileType.USER_SPECIALITIES_FAILURE,
        payload : {},
        error : error
    }
}



export const personalConnectionInfoRequest = (data) => {
    
    return {
        type : PersonalProfileType.USER_CONNECTIONINFO_REQUEST,
        payload: data
    }
}
export const personalConnectionInfoSuccess = (user) => {
    return {
        type : PersonalProfileType.USER_CONNECTIONINFO_SUCCESS,
        payload : user,
        error : {}
    }
}
export const personalConnectionInfoFailure = (error) => {
    return {
        type : PersonalProfileType.USER_CONNECTIONINFO_FAILURE,
        payload : {},
        error : error
    }
}
