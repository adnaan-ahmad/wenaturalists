import {CompanyProfileType} from '../../ActionTypes/User/CompanyProfileType'

export const companyProfileRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_DATA_REQUEST,
        payload: data
    }
}
export const companyProfileSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_DATA_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companyProfileFailure = (error) => {
    return {
        type : CompanyProfileType.USER_DATA_FAILURE,
        payload : {},
        error : error
    }
}




export const companyContactRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_CONTACT_REQUEST,
        payload: data
    }
}
export const companyContactSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_CONTACT_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companyContactFailure = (error) => {
    return {
        type : CompanyProfileType.USER_CONTACT_FAILURE,
        payload : {},
        error : error
    }
}




export const companyAddressRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_ADDRESS_REQUEST,
        payload: data
    }
}
export const companyAddressSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_ADDRESS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companyAddressFailure = (error) => {
    return {
        type : CompanyProfileType.USER_ADDRESS_FAILURE,
        payload : {},
        error : error
    }
}



export const companyBioRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_BIO_REQUEST,
        payload: data
    }
}
export const companyBioSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_BIO_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companyBioFailure = (error) => {
    return {
        type : CompanyProfileType.USER_BIO_FAILURE,
        payload : {},
        error : error
    }
}


export const companySkillsRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_SKILLS_REQUEST,
        payload: data
    }
}
export const companySkillsSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_SKILLS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companySkillsFailure = (error) => {
    return {
        type : CompanyProfileType.USER_SKILLS_FAILURE,
        payload : {},
        error : error
    }
}



export const companySkillsSpecializationRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_SKILLSSPECIALIZATION_REQUEST,
        payload: data
    }
}
export const companySkillsSpecializationSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_SKILLSSPECIALIZATION_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companySkillsSpecializationFailure = (error) => {
    return {
        type : CompanyProfileType.USER_SKILLSSPECIALIZATION_FAILURE,
        payload : {},
        error : error
    }
}




export const companyRecentActivityRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_RECENTACTIVITY_REQUEST,
        payload: data
    }
}
export const companyRecentActivitySuccess = (user) => {
    return {
        type : CompanyProfileType.USER_RECENTACTIVITY_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companyRecentActivityFailure = (error) => {
    return {
        type : CompanyProfileType.USER_RECENTACTIVITY_FAILURE,
        payload : {},
        error : error
    }
}



export const companyExperienceRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_EXPERIENCE_REQUEST,
        payload: data
    }
}
export const companyExperienceSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_EXPERIENCE_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companyExperienceFailure = (error) => {
    return {
        type : CompanyProfileType.USER_EXPERIENCE_FAILURE,
        payload : {},
        error : error
    }
}




export const companyEducationRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_EDUCATION_REQUEST,
        payload: data
    }
}
export const companyEducationSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_EDUCATION_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companyEducationFailure = (error) => {
    return {
        type : CompanyProfileType.USER_EDUCATION_FAILURE,
        payload : {},
        error : error
    }
}




export const companyBusinessPageRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_BUSINESS_PAGE_REQUEST,
        payload: data
    }
}
export const companyBusinessPageSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_BUSINESS_PAGE_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companyBusinessPageFailure = (error) => {
    return {
        type : CompanyProfileType.USER_BUSINESS_PAGE_FAILURE,
        payload : {},
        error : error
    }
}




export const companyInterestsRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_INTERESTS_REQUEST,
        payload: data
    }
}
export const companyInterestsSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_INTERESTS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companyInterestsFailure = (error) => {
    return {
        type : CompanyProfileType.USER_INTERESTS_FAILURE,
        payload : {},
        error : error
    }
}



export const companyHashTagsRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_HASHTAGS_REQUEST,
        payload: data
    }
}
export const companyHashTagsSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_HASHTAGS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companyHashTagsFailure = (error) => {
    return {
        type : CompanyProfileType.USER_HASHTAGS_FAILURE,
        payload : {},
        error : error
    }
}




export const companySpecialitiesRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_SPECIALITIES_REQUEST,
        payload: data
    }
}
export const companySpecialitiesSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_SPECIALITIES_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companySpecialitiesFailure = (error) => {
    return {
        type : CompanyProfileType.USER_SPECIALITIES_FAILURE,
        payload : {},
        error : error
    }
}



export const companyConnectionInfoRequest = (data) => {
    
    return {
        type : CompanyProfileType.USER_CONNECTIONINFO_REQUEST,
        payload: data
    }
}
export const companyConnectionInfoSuccess = (user) => {
    return {
        type : CompanyProfileType.USER_CONNECTIONINFO_SUCCESS,
        payload : user,
        error : {}
    }
}
export const companyConnectionInfoFailure = (error) => {
    return {
        type : CompanyProfileType.USER_CONNECTIONINFO_FAILURE,
        payload : {},
        error : error
    }
}
