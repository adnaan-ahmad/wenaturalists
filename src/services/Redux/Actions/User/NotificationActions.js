import { Notification } from '../../ActionTypes/User/Notification'

export const notificationRequest = (data) => {
    return {
        type : Notification.USER_NOTIFICATION_REQUEST,
        payload: data
    }
}

export const notificationSuccess = (user) => {
    return {
        type : Notification.USER_NOTIFICATION_SUCCESS,
        payload : user,
        error : {}
    }
}

export const notificationFailure = (error) => {
    return {
        type : Notification.USER_NOTIFICATION_FAILURE,
        payload : {},
        error : error
    }
}
