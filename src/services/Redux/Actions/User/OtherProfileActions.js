import {OtherProfileType} from '../../ActionTypes/User/OtherProfileType'

export const otherProfileRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_DATA_REQUEST,
        payload: data
    }
}
export const otherProfileSuccess = (user) => {
    return {
        type : OtherProfileType.USER_DATA_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherProfileFailure = (error) => {
    return {
        type : OtherProfileType.USER_DATA_FAILURE,
        payload : {},
        error : error
    }
}




export const otherContactRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_CONTACT_REQUEST,
        payload: data
    }
}
export const otherContactSuccess = (user) => {
    return {
        type : OtherProfileType.USER_CONTACT_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherContactFailure = (error) => {
    return {
        type : OtherProfileType.USER_CONTACT_FAILURE,
        payload : {},
        error : error
    }
}




export const otherAddressRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_ADDRESS_REQUEST,
        payload: data
    }
}
export const otherAddressSuccess = (user) => {
    return {
        type : OtherProfileType.USER_ADDRESS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherAddressFailure = (error) => {
    return {
        type : OtherProfileType.USER_ADDRESS_FAILURE,
        payload : {},
        error : error
    }
}



export const otherBioRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_BIO_REQUEST,
        payload: data
    }
}
export const otherBioSuccess = (user) => {
    return {
        type : OtherProfileType.USER_BIO_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherBioFailure = (error) => {
    return {
        type : OtherProfileType.USER_BIO_FAILURE,
        payload : {},
        error : error
    }
}


export const otherSkillsRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_SKILLS_REQUEST,
        payload: data
    }
}
export const otherSkillsSuccess = (user) => {
    return {
        type : OtherProfileType.USER_SKILLS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherSkillsFailure = (error) => {
    return {
        type : OtherProfileType.USER_SKILLS_FAILURE,
        payload : {},
        error : error
    }
}



export const otherSkillsSpecializationRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_SKILLSSPECIALIZATION_REQUEST,
        payload: data
    }
}
export const otherSkillsSpecializationSuccess = (user) => {
    return {
        type : OtherProfileType.USER_SKILLSSPECIALIZATION_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherSkillsSpecializationFailure = (error) => {
    return {
        type : OtherProfileType.USER_SKILLSSPECIALIZATION_FAILURE,
        payload : {},
        error : error
    }
}




export const otherRecentActivityRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_RECENTACTIVITY_REQUEST,
        payload: data
    }
}
export const otherRecentActivitySuccess = (user) => {
    return {
        type : OtherProfileType.USER_RECENTACTIVITY_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherRecentActivityFailure = (error) => {
    return {
        type : OtherProfileType.USER_RECENTACTIVITY_FAILURE,
        payload : {},
        error : error
    }
}



export const otherExperienceRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_EXPERIENCE_REQUEST,
        payload: data
    }
}
export const otherExperienceSuccess = (user) => {
    return {
        type : OtherProfileType.USER_EXPERIENCE_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherExperienceFailure = (error) => {
    return {
        type : OtherProfileType.USER_EXPERIENCE_FAILURE,
        payload : {},
        error : error
    }
}




export const otherEducationRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_EDUCATION_REQUEST,
        payload: data
    }
}
export const otherEducationSuccess = (user) => {
    return {
        type : OtherProfileType.USER_EDUCATION_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherEducationFailure = (error) => {
    return {
        type : OtherProfileType.USER_EDUCATION_FAILURE,
        payload : {},
        error : error
    }
}




export const otherBusinessPageRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_BUSINESS_PAGE_REQUEST,
        payload: data
    }
}
export const otherBusinessPageSuccess = (user) => {
    return {
        type : OtherProfileType.USER_BUSINESS_PAGE_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherBusinessPageFailure = (error) => {
    return {
        type : OtherProfileType.USER_BUSINESS_PAGE_FAILURE,
        payload : {},
        error : error
    }
}




export const otherInterestsRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_INTERESTS_REQUEST,
        payload: data
    }
}
export const otherInterestsSuccess = (user) => {
    return {
        type : OtherProfileType.USER_INTERESTS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherInterestsFailure = (error) => {
    return {
        type : OtherProfileType.USER_INTERESTS_FAILURE,
        payload : {},
        error : error
    }
}



export const otherHashTagsRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_HASHTAGS_REQUEST,
        payload: data
    }
}
export const otherHashTagsSuccess = (user) => {
    return {
        type : OtherProfileType.USER_HASHTAGS_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherHashTagsFailure = (error) => {
    return {
        type : OtherProfileType.USER_HASHTAGS_FAILURE,
        payload : {},
        error : error
    }
}




export const otherSpecialitiesRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_SPECIALITIES_REQUEST,
        payload: data
    }
}
export const otherSpecialitiesSuccess = (user) => {
    return {
        type : OtherProfileType.USER_SPECIALITIES_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherSpecialitiesFailure = (error) => {
    return {
        type : OtherProfileType.USER_SPECIALITIES_FAILURE,
        payload : {},
        error : error
    }
}



export const otherConnectionInfoRequest = (data) => {
    
    return {
        type : OtherProfileType.USER_CONNECTIONINFO_REQUEST,
        payload: data
    }
}
export const otherConnectionInfoSuccess = (user) => {
    return {
        type : OtherProfileType.USER_CONNECTIONINFO_SUCCESS,
        payload : user,
        error : {}
    }
}
export const otherConnectionInfoFailure = (error) => {
    return {
        type : OtherProfileType.USER_CONNECTIONINFO_FAILURE,
        payload : {},
        error : error
    }
}
