import { EndorsementType } from '../../ActionTypes/User/EndorsementType'

export const endorsementRequest = (data) => {
    
    return {
        type : EndorsementType.USER_ENDORSEMENT_REQUEST,
        payload: data
    }
}
export const endorsementSuccess = (user) => {
    return {
        type : EndorsementType.USER_ENDORSEMENT_SUCCESS,
        payload : user,
        error : {}
    }
}
export const endorsementFailure = (error) => {
    return {
        type : EndorsementType.USER_ENDORSEMENT_FAILURE,
        payload : {},
        error : error
    }
}




export const endorsementDetailRequest = (data) => {
    
    return {
        type : EndorsementType.USER_ENDORSEMENTDETAIL_REQUEST,
        payload: data
    }
}
export const endorsementDetailSuccess = (user) => {
    return {
        type : EndorsementType.USER_ENDORSEMENTDETAIL_SUCCESS,
        payload : user,
        error : {}
    }
}
export const endorsementDetailFailure = (error) => {
    return {
        type : EndorsementType.USER_ENDORSEMENTDETAIL_FAILURE,
        payload : {},
        error : error
    }
}
