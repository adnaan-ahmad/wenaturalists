import React, { Component } from 'react'
import {
    Text,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    SafeAreaView,
    ScrollView,
    Modal,
    TouchableHighlight,
    ActivityIndicator,
    FlatList,
    ImageBackground,
    Platform,
    StyleSheet,
} from 'react-native'
import welogo from '../../assets/welogo.png'

export default class UserAgreement extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (

            <ScrollView stickyHeaderIndices={[0]}
                showsHorizontalScrollIndicator={false}
                // ref={ref => skillScrollview = ref}
                // onContentSizeChange={() => {
                //     skillScrollview.scrollToEnd({ animated: true });
                // }}
                showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled' contentContainerStyle={Platform.OS === 'ios' ? { marginTop: 0, paddingBottom: 40, backgroundColor: '#F7F7F5' } : { marginBottom: 20, paddingBottom: 40, backgroundColor: '#F7F7F5' }}>

                <View style={{ width: '100%', backgroundColor: '#FFF', paddingVertical: 8, paddingLeft: 12 }}>
                    <Image source={welogo} style={{ width: 47, height: 63, alignSelf: 'flex-start', justifyContent: 'flex-start' }} />
                </View>

                <View style={{ alignSelf: 'center', marginTop: 20, marginBottom:10 }}>
                    <Text style={{ textAlign: 'center', fontWeight: '700', fontSize: 24, color: '#00394d' }}>USER AGREEMENT</Text>
                </View>

                <View style={{ borderRadius: 8, backgroundColor: '#FFF', width: '90%', marginHorizontal: 'auto', paddingHorizontal: '8%', paddingVertical: '6%', alignSelf: 'center' }}>
                    <Text style={styles.text}>
                        This User Agreement ("Agreement") is an agreement between Dscovr Journeys Private Limited ("Company" or "we" or "us" or "our") and you ("you" or "your" or "user"). This Agreement explains our obligations to you, and your obligations to us with regard to your use of www.wenaturalists.com ("Website") in respect of the Services. By agreeing to comply with the terms of this Agreement, you agree to be bound by the Terms and Conditions, the Privacy Policy and any other legal notices or conditions or guidelines posted on the Website, which shall be binding on you, your successors and/or assigns. The Company reserves all rights to change, modify, update, or alter ("update") this Agreement at any point of time with or without notifying you. Your continued use of the Website means that you accept any updated terms and conditions that we come up with. If you do not agree to any of the terms and conditions mentioned herein or if you do not wish to accept any update to this Agreement, you are advised not to use this Website for the Services. Please re-visit the User Agreement on our Website from time to time to stay abreast of any changes that we may introduce.
                    </Text>

                    <Text style={styles.heading}>1. DEFINITIONS AND INTERPRETATION</Text>
                    <Text style={styles.text}>In this Agreement, unless the context otherwise requires, the following expressions have the following meanings:</Text>
                    <Text style={styles.text}>"Associate Websites" means and includes all other websites, platforms, and applications owned and operated by the Company;</Text>
                    <Text style={styles.text}>"Content" means and includes the content and information provided on the Website in any form and capable of being stored on a computer that appears on, or forms part of, including its any trademarks, information related to any services, contact details, graphics, functions and other materials available, whether publicly posted or privately transmitted as well as all derivative works thereof; including the Website's design structure and compilation and all content such as text, images, audio, video, scripts, code, documents, databases, the mascot and the source code of the Website and any other form of information made available on the Website by us and capable of being stored on a computer that appears on, or forms part of, our Website;</Text>

                    <Text style={styles.text}>"Information" shall mean any information you provide to the Website and shall include your picture, name, contact details, skills, specializations, interests, causes, experience, qualifications, email address(es), data, text, software, music, sound, photographs, graphics, video, messages or other materials uploaded, posted or stored in connection with your use of the Services;</Text>
                    <Text style={styles.text}>"Members" shall mean and include those users of this Website who have created an account on this Website and can access the Services;</Text>
                    <Text style={styles.text}>"Premium Services" shall mean and include certain Services that Members may purchase in consideration for a Fee on the Website and includes subscription of certain Services and/or purchasing certain products offered on the Website.</Text>
                    <Text style={styles.text}>"Services" means all features provided on the Website and the Associate Websites including but not limited to all applications, communications, software, feeds, networking, posting and any other services that are offered and available on this Website and the Associate Websites to the Members from time to time, including any Premium Services.</Text>
                    <Text style={styles.text}>"Fee" shall mean the fee paid by a Member in consideration for the subscription and access to the Premium Services on the Website and/or purchase of products as per the rates assigned to each product under the Premium Services.</Text>
                    <Text style={styles.text}>In this Agreement: (a) headings are for convenience only and do not affect interpretation; (b) words in the singular include the plural; and (c) including means including but not limited to.</Text>


                    <Text style={styles.heading}>2. SCOPE AND REGISTRATION</Text>
                    <Text style={styles.text}>If you choose to register as a Member on this Website, you shall be bound by this Agreement and the Agreement shall at all times be read together with the Terms and Conditions and the Privacy Policy. In the event of any conflict in the terms or interpretation, this Agreement shall prevail over the Terms and Conditions. The Privacy Policy and Terms and Conditions are incorporated into this Agreement by reference.</Text>
                    <Text style={styles.text}>The Company reserves the right, in its sole discretion, to deny you access to the Services or any portion thereof without notice for the following reasons, immediately (a) for any unauthorized access or use by you (b) if you assign or transfer (or attempt the same) any rights granted to you under this Agreement; (c) if you violate any of the other terms and conditions of this Agreement.</Text>
                    <Text style={styles.text}>The Company reserves the right to limit your use of the Services (including any Premium Services), including the number of your connections and your ability to contact other Members.</Text>
                    <Text style={styles.text}>The Company reserves the right, in its sole discretion, to amend, alter, modify, change or discontinue the Services (including the Premium Services) or a portion thereof at any time, without penalty and without notice, for any reason whatsoever.</Text>

                    <Text style={styles.heading}>3. YOUR ACCOUNT</Text>
                    <Text style={styles.text}>When you create an account on the Website you register yourself as a Member. By creating an account on the Website and registering as a Member, you certify that all Information you provide, now or in the future, is accurate. You shall not misrepresent any Information that you disclosed to us.</Text>
                    <Text style={styles.text}>You agree to fully and accurately provide the Information requested by us when setting up your account and to regularly update such Information. Your failure to do so may affect your experience using this Website and may limit your ability to use your account and/or the Services (including any Premium Services).</Text>
                    <Text style={styles.text}>You hereby agree and confirm that you are legally responsible for all Information you share in connection with your use of the Services and/or the Premium Services. You agree to only provide Information that does not violate the law nor anyone's rights (including intellectual property rights). You hereby agree and acknowledge that the Company may be required by law to remove certain Information in certain countries or under certain circumstances. The Company is not responsible for your Information in any manner whatsoever.</Text>
                    <Text style={styles.text}>You acknowledge that your account shall be set to default privacy settings at the time of creation which allows your Information to be viewed by all users of the Website.</Text>
                    <Text style={styles.text}>Should you wish to change the privacy settings of your account, you may do so under the Settings and Privacy tab on the Website. Of course, if you choose not to disclose certain information, your experience using this Website may be affected and your ability to use your account and/or the Services (including any Premium Services) may become limited.</Text>
                    <Text style={styles.text}>You hereby grant the Company a worldwide, royalty-free, transferable, sub-licensable and non-exclusive license to use, reproduce, display, perform, copy, modify, edit, distribute, publish, exhibit and process your Information for any and all non-commercial purposes and in accordance with the terms of this Agreement, the Terms and Conditions and the Privacy Policy and without us requiring any further consent from you and/or providing any notice and/or compensation to you or other. This license is limited in the following ways:</Text>
                    <Text style={styles.textbullet}>a. You may terminate this license for specific Information by deleting such Information from your account or by closing your account. However, this license may not be terminated by you with respect to Information that (a) you may have shared with other users as part of the Website and they have copied, re-shared it or stored it and (b) for the reasonable time it takes to remove such Information from the Website backup and other such systems.</Text>
                    <Text style={styles.textbullet}>b. We will not include your Information in advertisements for the products and services of third-party advertisers or ad networks or to others without your separate permission. However, we may, without any compensation to you or others, use your Information to show you ads similar to your Information and your social actions may be visible and included with ads, as more specifically described in the Privacy Policy. If you use a Website feature, we may mention that with your name or photo to promote that feature within our Website, subject to your settings.</Text>
                    <Text style={styles.textbullet}>c. While we may edit and format the Information you share, however, we shall not change the meaning of your expression of such Information.</Text>
                    <Text style={styles.textbullet}>d. You may choose to make your Information available to other Users of the Website.</Text>
                    <Text style={styles.text}>You agree and acknowledge that we may access, store, process, and use your Information in accordance with the terms of the Privacy Policy and your choices including your account settings.</Text>

                    <Text style={styles.heading}>4. LOGIN ID AND PASSWORD DISCLOSURE</Text>
                    <Text style={styles.text}>To ensure security of access to the Information you share with us while registering as a Member, your use of the Services on our Website is restricted by the unique login ID and password selected by you. You should be careful in handling the login ID and password and you should ensure that you do not reveal it to anybody. You should keep changing your password periodically.</Text>
                    <Text style={styles.text}>You are entirely responsible for maintaining the confidentiality of your login ID and password. In the event you believe that there is unauthorized use of your account, you shall immediately notify the Company in writing at support@wenaturalists.com. You are responsible for anything that happens through your account unless you close it or report misuse.</Text>
                    <Text style={styles.text}>The login ID provided to you when you register as a Member permits you simultaneous access to the Services on multiple devices (including computers and mobile phones) at a time through the Website. Further, we provide you an option to enable an additional security feature two-factor authentication ("2FA") which requires you to provide two means of authentication when you access your account, including your password and security code. 2FA provides you with: (1) a security code when you access your account for the first time, (2) a security code when you access your account for the first time from a new device. The 2FA feature may require you to provide the security code sent to you when accessing your account for the first time or when accessing your account for the first time from a new device, as the case may be.</Text>
                    <Text style={styles.text}>Sharing of your login ID and password by any Member to a third party is prohibited and in contravention of this Agreement. Upon breach, the Company reserves the right, in its sole discretion, to terminate your access to the Website and the related Services (including any Premium Services) or any portion thereof at any time, without notice.</Text>

                    <Text style={styles.heading}>5. LICENSE</Text>

                    <Text style={styles.text}>We hereby grant you a limited, revocable, non-assignable, non-transferable and non-exclusive license to access the Website and use the Content for the purpose of availing the Services and/or Premium Services, provided and expressly conditioned upon your agreement that all such access and use shall be governed by all of the terms and conditions set forth in this Agreement.</Text>

                    <Text style={styles.heading}>6. PREMIUM SERVICES</Text>
                    
                    <Text style={styles.text}>In consideration of the Fee, the Company shall provide certain Premium Services to its Members. The Fee will be charged to you in United States Dollars (USD) for such Premium Services. The Fee, any promotional offers, discounts, etc. and the scope of the Premium Services shall be as provided on the Website from time to time.</Text>
                    <Text style={styles.text}>Certain portions of the Website are only accessible to Members who have paid the Fee and subscribed to such Premium Services. By subscribing to receive any of these Premium Services, you additionally agree to the following terms and conditions:</Text>
                    <Text style={styles.textbullet}>a. You agree to pay any Fee at the rates in effect when the charges are incurred;</Text>
                    <Text style={styles.textbullet}>b. You must provide us with complete and accurate payment information. You can pay by either Credit / Debit card / Net Banking through an internet payment gateway. By submitting credit or debit card payment details to the Company you warrant that you are entitled to purchase the Premium Services using those payment details. In the case of unauthorized payments, the Company reserves the right to suspend or terminate your access to the Premium Services. If we do not receive payment authorization or any authorization is subsequently canceled, we may immediately terminate or suspend your access to any Premium Services.</Text>
                    <Text style={styles.textbullet}>c. Your purchase may be subject to foreign exchange fees or differences in prices based on location (e.g. exchange rates, bank charges)</Text>
                    <Text style={styles.textbullet}>d. We may store and continue billing your payment method (e.g. credit card) even after it has expired, to avoid interruptions in your Premium Services.</Text>
                    <Text style={styles.textbullet}>e. If you purchase a subscription to our Premium Services, your payment method automatically will be charged at the start of each subscription period for the Fees and taxes applicable to that period. To avoid future charges, please cancel your subscription to such Premium Services before the renewal date.</Text>
                    <Text style={styles.textbullet}>f. All your purchases of Premium Services are subject to our refund policy herein.</Text>
                    <Text style={styles.textbullet}>g. We may calculate taxes payable by you based on the billing information that you provide us at the time of purchase.</Text>
                    <Text style={styles.text}>The Company will try to process your subscription to such Premium Services promptly but does not guarantee that the Premium Services will be available to you by any specified time. A contract with you for rendering the Premium Services will come into effect when the Company sends you an email message confirming your subscription details.</Text>
                    <Text style={styles.text}>You can get a copy of your invoice through your account settings under "Purchase History".</Text>
                    <Text style={styles.text}>The delivery of Premium Services from the Company will be directly on the Website. Members who have subscribed to such Premium Services may also log in to the Website to view the Premium Services that they have subscribed to.</Text>

                    <Text style={styles.heading}>7. REFUND POLICY</Text>
                    <Text style={styles.text}>The Company does not offer any refund of the Fees and any other type of fees or charges under any circumstances whatsoever. However, any change or modification in this no-refund policy shall be at the Company's sole discretion.</Text>

                    <Text style={styles.heading}>8. CANCELLATION</Text>
                    <Text style={styles.text}>Premium Services once subscribed to and paid for, cannot be canceled. A request for termination of Premium Services may be given by the Member, however, no Fee or any other amounts shall be refunded by the Company.</Text>
                    <Text style={styles.text}>You agree that your subscription to the Premium Services may automatically renew for an additional period until explicitly canceled by you. Any cancellation of your subscription to our Premium Services must be done at least 1 (one) day prior to the end of the subscription period to allow for adequate processing time.</Text>

                    <Text style={styles.heading}>9. TAXES</Text>
                    <Text style={styles.text}>You take full responsibility for all taxes and Fees of any nature associated with the Premium Services, including any indirect taxes by whatever name called (service tax or value added tax or goods and services tax, etc.) related to the subscription of the Premium Services. Except for collecting the indirect taxes payable on the Fee and remitting it to the tax authorities, the Company shall not be liable for any taxes or other fees to be paid in accordance with or related to the Premium Services.</Text>
                    

                    <Text style={styles.heading}>10. TERM AND TERMINATION</Text>
                    <Text style={styles.text}>You agree to avail the Services (including the Premium Services) offered on this Website for a period commencing on the day you become a Member and expiring on the day you delete your account ("Term"). You agree that the Term shall expire only upon explicit deletion of your account.</Text>
                    <Text style={styles.text}>You may cancel or terminate this Agreement any time whatsoever via by deleting your account or writing to us at support@wenaturalists.com.</Text>
                    <Text style={styles.text}>This Agreement and the license rights granted hereunder shall remain in full force and effect unless terminated or canceled for any reason whatsoever. Termination or cancellation of this Agreement shall not affect any right or relief to which the Company may be entitled, at law or in equity. Upon termination of this Agreement, all rights granted to you will terminate and revert to the Company.</Text>
                    <Text style={styles.text}>We may revoke the Services (including the Premium Services) at any time, without penalty and without notice, if you fail to comply with any of the terms of this Agreement.</Text>
                    <Text style={styles.text}>Notice of revocation of the Services and/or Premium Services by the Company may be sent to the primary e-mail address associated with your account. Upon termination, the Company has the right but no obligation to delete all data, files, or other Information that is stored in your account.</Text>
                    <Text style={styles.text}>The following shall survive termination:</Text>
                    <Text style={styles.textbullet}>Our rights to use and disclose your feedback;</Text>
                    <Text style={styles.textbullet}>Members' and/or users' rights to further re-share content and information you shared through the Services and/or Premium Services;</Text>
                    <Text style={styles.textbullet}>Restrictions and Permissions of User, Disclaimer and Limitation of Liability (under the Terms & Conditions) shall survive the termination of this Agreement;</Text>
                    <Text style={styles.textbullet}>Any amounts owed by the Company prior to termination remain owed after termination.</Text>

                    <Text style={styles.heading}>11. USE OF COMMUNICATION FACILITIES</Text>
                    <Text style={styles.text}>This Website contains messages/bulletin boards, WeBuzz, forums, chat rooms, group posts, project postings, or other message or communication facilities (collectively, "Communication Facilities"), you agree to use the Communication Facilities only to send and receive messages, Information and materials that are proper and related to the particular Communication Facility. Any messages and materials that you share, or post may be seen by other users. Including but without limitation, you agree that when using a Communication Facility, you shall not do any of the following:</Text>
                    <Text style={styles.textbullet}>Publish, post, distribute, or disseminate any defamatory, infringing, obscene, indecent, or unlawful material or information.</Text>
                    <Text style={styles.textbullet}>Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of privacy and publicity) of others.</Text>
                    <Text style={styles.textbullet}>Upload files that contain viruses, corrupted files, or any other similar software or programs that may damage the operation of another's computer or files that contain software or other material protected by intellectual property laws (or by rights of privacy of publicity) unless you own or control the rights thereto or have received all necessary consents.</Text>
                    <Text style={styles.textbullet}>Download any file posted by another user of a Communication Facility that you know, or reasonably should know, cannot be legally distributed in such manner.</Text>
                    <Text style={styles.textbullet}>Conduct contests, surveys, or forward chain letters or spam.</Text>
                    <Text style={styles.textbullet}>Not to misuse by posting malicious and ill-intentioned messages</Text>
                    <Text style={styles.text}>In the event you violate the manner in which you post any materials or Information or messages on our Communication Facilities for any reason whatsoever, we shall respond or provide an acknowledgment to the complainant within 36 (thirty six) hours of becoming aware of any such violation or within 36 (thirty six) hours of any such violation being brought to our notice by any other user, person or entity and initiate appropriate action under law which may include disabling such information. Further, we shall preserve records of such violative materials and/or Information and/or messages for a period of 90 (ninety) days for investigation purposes and in accordance with the law. We have the right to immediately terminate your account and restrict your use and access to the Website, the Services (including the Premium Services).</Text>
                    <Text style={styles.text}>Where we have made settings available, we will honor the choices you make about who can see messages, Information and materials posted by you (e.g., sharing content only to your connections, profile visibility on search engines, or opting not to notify others of your profile updates). For project searches, by default, we do not notify your connections or the public. So, if you apply for a project through any of the Services, our default is to share it only with the project poster.</Text>
                    <Text style={styles.text}>We are not a storage service and we do not promise to store or keep showing any messages, material, or the Information that you have posted. You agree that we have no obligation to store, maintain or provide you a copy of any messages, materials or information that you provide, except to the extent required by applicable law and as noted in our Privacy Policy.</Text>
                    <Text style={styles.text}>We expect you to exercise due caution before transacting on any message or material posted on the Communication Facilities. The messages and materials posted on the Communication Facilities are the views of the individuals posting them; The Company or the Associate Websites do not assume any liability on account of any message posted therein.</Text>

                    <Text style={styles.heading}>12. EXCLUSIVE REMEDY</Text>
                    <Text style={styles.text}>In the event of any problem with the Website, the Services (including the Premium Services) and/or the Content, you agree that your sole and exclusive remedy is to cease using the Website, the Services and/or Premium Services. Under no circumstances shall the Company and/or its associate entities and/or Associate Websites be liable in any way for your use of the Website, the Services and/or Premium Services, on or through the Website, including, but not limited to, any errors or omissions, any infringement of the intellectual property rights or other rights of third parties, or for any loss or damage of any kind incurred as a result of, or related to, the use of the Website, the Services and/or Premium Services available on or through the Website.</Text>

                    <Text style={styles.heading}>13. PROJECTS</Text>
                    <Text style={styles.text}>Please note that while our Services include project information and the Website may contain information, postings or contact details of persons in relation to the project information accessible through our Services, any such projects and/or training events and/or workshops and any other events hosted by third parties are not under the control of the Company. The Company is not responsible for the acts or omissions of any third parties that may host such projects and/or training events and/or workshops and any other events. The Company does not warrant or guarantee the availability, completeness, usefulness, safety, reliability of any such third-party projects and/or training events and/or workshops, and any other events. You acknowledge that you access and attend such third-party projects and/or training events and/or workshops and any other events solely at your own risk. You agree that under no circumstances will the Company be liable in any way for any errors or omissions in any such projects and/or training events and/or workshops and any other events, or for any loss or damage of any kind incurred as a result of your attendance of any such third-party projects and/or training events and/or workshops and any other events.</Text>


                    <Text style={styles.heading}>14. OTHER REMEDIES</Text>
                    <Text style={styles.text}>You acknowledge that a violation or attempted violation of this Agreement will cause such damage to the Company and/or the Associate Websites as will be irreparable, the exact amount of which would be impossible to ascertain and for which there will be no adequate remedy at law. Accordingly, you agree that the Company shall be entitled as a matter of right to an injunction issued by any court of competent jurisdiction, restraining such violation or attempted violation of this Agreement by you, or your affiliates, partners, or agents, as well as to recover from you any and all costs and expenses sustained or incurred by the Company in obtaining such an injunction, including, without limitation, reasonable attorney's fees. You agree that no bond or other security shall be required in connection with such an injunction.</Text>
                    <Text style={styles.text}>In no event shall you be entitled to rescission, injunctive or other equitable relief, or to enjoin or restrain the operation of the Company, the exploitation of any advertising or other materials issued in connection therewith, or the exploitation of the Website or any Content used or displayed through the Website.</Text>

                    <Text style={styles.heading}>15. COMMUNICATION METHODS</Text>
                    <Text style={styles.text}>Please note that the Company reserves the right to communicate with you in the mode and manner it deems appropriate. Any communications with you may be in the form of emails and/or notifications and/or notices sent by the Company to you, at its sole discretion. While you may opt to switch off certain notifications and manage your preferences, you agree and confirm that the Company reserves the right to communicate with you regarding the Services, the Website, the Agreement, the Terms and Conditions and the Privacy Policy, in any mode or manner whatsoever. You acknowledge and agree that any communication sent to you by the Company in any mode or manner chosen by the Company shall be deemed to be an adequate manner of communicating with you.</Text>
                    
                    <Text style={styles.heading}>16. MISCELLANEOUS</Text>
                    <Text style={styles.textbullet}>a. If any portion of this Agreement is found to be unenforceable, the remaining portion will remain in full force and effect.</Text>
                    <Text style={styles.textbullet}>b. Our failure to enforce any part of this Agreement shall not be considered as a waiver of any rights and claims that we may be entitled to in any manner whatsoever.</Text>
                    <Text style={styles.textbullet}>c. You will not transfer any of your rights or obligations under this Agreement to anyone else without our prior written consent.</Text>
                    <Text style={styles.textbullet}>d. All of our rights and obligations under this Agreement are freely assignable by us in connection with a merger, acquisition, or sale of assets, or by operation of law or otherwise.</Text>
                    <Text style={styles.textbullet}>e. This Agreement does not confer any third-party beneficiary rights.</Text>
                    <Text style={styles.textbullet}>f. A printed version of this Agreement and of any related notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this Agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form.</Text>

                    <Text style={styles.heading}>17. GOVERNING LAW AND JURISDICTION</Text>
                    <Text style={styles.text}>This Agreement shall be governed by and construed in accordance with laws of the Republic of India. You explicitly agree that irrespective of the country you are a citizen/resident of and whether or not you are a resident of India, you shall be bound by the laws and regulations relating to data protection of the Republic of India. You explicitly agree that the courts at Mumbai, Maharashtra, India shall have exclusive jurisdiction over any disputes arising under hereunder.</Text>

                    <Text style={styles.heading}>18. RELATIONSHIP</Text>
                    <Text style={styles.text}>You understand and agree that no joint venture, partnership, employment, or agency relationship exists between you and us on account of you using the Website or us rendering the Services (including the Premium Services).</Text>
                </View>
            </ScrollView>

        )
    }
}

const styles = StyleSheet.create({
    text:{
        fontWeight: '400',
        fontSize: 14,
        color: '#326169',
        marginBottom: 14
    },
    textbullet:{
        fontWeight: '400',
        fontSize: 14,
        color: '#326169',
        marginBottom: 14,
        marginLeft: 10
    },
    heading:{
        textAlign: 'center',
        fontWeight: '700',
        fontSize: 18,
        color: '#00394d',
        marginVertical: 20 
    }
})