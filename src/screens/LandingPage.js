import React, { Component } from 'react';
import { View, Text, ImageBackground, ScrollView, Image, TouchableOpacity, Linking, Platform } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import AsyncStorage from '@react-native-community/async-storage';

import icoMoonConfig from '../../assets/Icons/selection.json'
import landingbg from '../../assets/landingbg.png'
import wenat from '../../assets/we-naturalists.png'
import styles from '../Components/GlobalCss/Common/LandingCss'
import { COLORS } from '../Components/Shared/Colors'
import defaultStyle from '../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
export default class LandingPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        AsyncStorage.getItem("refreshToken").then((value) => {
            if(value!==null){
              this.props.navigation.replace("BottomTab")
            }
          })
    }

    render() {
        return (
            <View>
                <ImageBackground style={{ height: '100%', width: '100%', alignItems: 'center' }} source={landingbg}>

                    <ScrollView contentContainerStyle={{ justifyContent: 'space-between', alignItems: 'center', flex: 1, paddingBottom: 10 }}>

                        <Image source={wenat} style={{ height: 75, width: 202, marginTop: 20 }} />

                        <View style={{ alignItems: 'center', marginTop: 40, marginBottom: 40 }}>
                            <Text style={[defaultStyle.H4, { color: COLORS.altgreen_200 }]}>Welcome to the</Text>
                            <Text style={[defaultStyle.H1, { color: COLORS.white }]}>Ecosystem</Text>
                            <Text style={[defaultStyle.H3, { color: COLORS.altgreen_200 }]}>for Nature Lovers</Text>
                            <TouchableOpacity style={styles.loginButton} activeOpacity={0.5}
                                // onPress={() => this.props.navigation.replace("Login")}
                                onPress={() => this.props.navigation.replace("LoginWebview")}
                                >
                                <Text style={[defaultStyle.Button_1, { color: COLORS.white }]}>LOGIN</Text>
                            </TouchableOpacity>
                        </View>

                        <View>

                            <TouchableOpacity style={styles.signupButton} activeOpacity={0.5}
                                onPress={() => this.props.navigation.replace("Signup")}>
                                <Text style={[defaultStyle.Button_1, { color: COLORS.primarydark }]}>SIGN UP</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                            onPress={() => Linking.openURL('mailto:support@wenaturalists.com')} 
                            activeOpacity={0.5} style={{ marginTop: 12, marginBottom: 12, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Icon name='Help' size={18} color='#91B3A2' style={{}} />
                                    <Text style={[defaultStyle.Subtitle_1, { color: COLORS.altgreen_300, marginBottom: 8, marginLeft: 8 }]}>Customer Support</Text>
                                </View>
                                <View style={{ backgroundColor: '#CFE7C71A', width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center', marginBottom: 8 }}>
                                    <Icon name='Arrow-Right' size={15} color='#91B3A2' style={ Platform.OS === 'android' ? { marginTop: 8 }: null} />
                                </View>
                            </TouchableOpacity>

                        </View>

                    </ScrollView>

                </ImageBackground>
            </View>
        );
    }
}
