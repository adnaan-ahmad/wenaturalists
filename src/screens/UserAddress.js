import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  ActivityIndicator,
  Modal,
  FlatList,
  TouchableHighlight,
  ImageBackground,
  Dimensions,
  StatusBar,
  Platform
} from 'react-native';
import { ProgressBar, TextInput } from 'react-native-paper'
import Snackbar from 'react-native-snackbar'
import axios from "axios"
// import Icon from 'react-native-vector-icons/MaterialIcons'
import { REACT_APP_userServiceURL } from '../../env.json'

import styles from '../Components/GlobalCss/User/SignUpCss'
import CountryCode from '../Json/CountryCode.json'
import bg from '../../assets/BackgroundImg.png'
import icoMoonConfig from '../../assets/Icons/selection.json'
import welogo from '../../assets/welogo.png'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import Header from '../Components/User/SignUp/Header'
import httpService from '../services/AxiosInterceptors'
import AsyncStorage from '@react-native-community/async-storage';
// import MyStatusBar from '../Components/Shared/MyStatusBar'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

var formData = new FormData()

httpService.setupInterceptors();

export default class UserAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      companyId: '1',
      country: 'Select Country',
      statevalue: 'Select Province/State',
      city: 'Select City/District',
      isLoading: false,
      modalCountry: false,
      modalState: false,
      modalCity: false,
      query: '',
      queryState: '',
      queryCity: '',
      countryCodes: [],
      stateArr: [],
      cityArr: [],
      officeAddress: ''
    };
  }

  componentDidMount() {
    if (this.state.countryCodes.length === 0) CountryCode.sort((a, b) => a.Name - b.Name).map((item) => {
      if (item.Name !== 'select') {
        this.state.countryCodes.push({ label: item.Unicode + ' ' + item.Name, value: { code: item.Dial, country: item.Name } })
        // this.setState({ countryCodes : [...this.state.countryCodes, { label: item.Unicode + ' ' + item.Name, value: { code: item.Dial, country: item.Name } }] })
      }
    })
  }

  handleSubmitCompany = () => {
    this.setState({ isLoading: true })
    let postBody = {
      "companyId": this.props.newId,
      "firstName": this.props.firstName,
      "lastName": this.props.lastName,
      "designation": this.props.designation,
      "officeAddress": this.state.officeAddress,
      "country": this.state.country,
      "state": this.state.statevalue,
      "city": this.state.city,
      "mobile": this.props.phoneNo,
      "countryISDCode": this.props.selectedCountryCode
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/user/company/update/operator/info',
      headers: { 'Content-Type': 'application/json' },
      data: postBody,
      withCredentials: true
    }).then((response) => {
      let res = response.data;
      // console.log(response.data)
      if (res.message === 'Success!') {
        this.setState({ isLoading: false })
        this.props.changeState({ currentScreen: 3, transactionid: res.body.transactionId, minutes: 9, seconds: 59, animatingWidth: 20 })
        console.log(res)
      }
    }).catch((err) => {
      console.log(err);
      this.setState({ isLoading: false })
      if (err && err.response && err.response.data) {
        Snackbar.show({
          backgroundColor: '#B22222',
          text: err.response.data.message,
          duration: Snackbar.LENGTH_LONG,
        })

      }
    })
  }


  handleSubmit = () => {
    // event.preventDefault();
    if (this.state.country === 'Select Country' || this.state.statevalue === 'Select Province/State' || this.state.city === 'Select City/District') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: "Please fill all the field",
        duration: Snackbar.LENGTH_LONG,
      })
    }
    else {
      this.setState({ isLoading: true })

      let postBody = {
        "userId": this.props.userId,
        "country": this.state.country,
        "state": this.state.statevalue,
        "district": this.state.city
      };

      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/user/update/address',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true
      }).then((response) => {
        console.log("user address : ", response.data);
        if (response && response.status === 200 && response.data && response.data.message === 'Success!') {

          let postBody = {
            "userId": this.props.userId,
            "skills": [],
            "interest": [],
            "persona": ''
          }
          formData.append('info', JSON.stringify(postBody));
          axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/user/update/personal/info',
            headers: { 'Content-Type': 'multipart/form-data' },
            data: formData,
            withCredentials: true
          }).then((response) => {
            let res = response.data;
            if (res.message === 'Success!') {
              AsyncStorage.getItem('refreshTokenSignup').then((value) => {
                value ? AsyncStorage.setItem('refreshToken', value) : null
                this.setState({ isLoading: false })
                // this.props.navigation.replace("BottomTab")
                this.props.changeState({ currentScreen: 4 })
              })
            }
          }).catch((err) => {
            console.log(err);
            this.setState({ isLoading: false })
            if (err && err.response && err.response.data) {
              Snackbar.show({
                backgroundColor: '#B22222',
                text: err.response.data.message,
                duration: Snackbar.LENGTH_LONG,
              })
            }
          });
        }
      }).catch((err) => {
        console.log(err);
        this.setState({ isLoading: false })
        if (err && err.response && err.response.data) {
          Snackbar.show({
            backgroundColor: '#B22222',
            text: err.response.data.message,
            duration: Snackbar.LENGTH_LONG,
          })

        }
      });
    }
  }

  findCountry = (query) => {

    return this.state.countryCodes.sort((a,b) => a.value.country > b.value.country).filter(item => item.value.country.toUpperCase().includes(query.toUpperCase()))
  }

  findState = (query) => {
    // if (query === '') {
    //   return this.state.stateArr
    // }

    // const regex = new RegExp(`${query.trim()}`, 'i');

    // return this.state.stateArr.filter(item => item.value.search(regex) >= 0);

    return this.state.stateArr.filter(item => item.value.toUpperCase().includes(query.toUpperCase()))
  }

  findCity = (query) => {

    return this.state.cityArr.filter(item => item.value.toUpperCase().includes(query.toUpperCase()))
  }

  render() {
    // console.log(this.props.userType)
    return (
      <ScrollView
        keyboardShouldPersistTaps='handled'
        showsVerticalScrollIndicator={false}
        style={
          (this.state.modalCountry || this.state.modalState || this.state.modalCity) ? { flex: 1, backgroundColor: '#00394D8C' }
            :
            styles.container}>

        {/* <MyStatusBar barStyle="light-content" /> */}

        <View style={[styles.mainSection, { height: windowHeight }]}>

          <Header progress={(this.state.modalCountry || this.state.modalState || this.state.modalCity) ? -1 :
            this.props.userType === 'Company' ? 2 : 3}
            text='Tell us where you are' customStyle={(this.state.modalCountry || this.state.modalState || this.state.modalCity) ? {} : { backgroundColor: '#fff' }}
            opacity={(this.state.modalCountry || this.state.modalState || this.state.modalCity) ? 0.3 : 1} />

          {/* country picker */}

          <Modal visible={this.state.modalCountry} animationType="slide"
            onRequestClose={() => (this.setState({ modalCountry: false }))}
            transparent={true} backdropColor="transparent" supportedOrientations={['portrait', 'landscape']}>
            <View style={styles.modalCountry}>
              <TouchableOpacity style={{ alignSelf: 'center', width: 42, height: 42, borderRadius: 42 / 2, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F7F7F5', marginRight: 10, marginBottom: 10 }}
                onPress={() => this.setState({ modalCountry: false })} >
                <Icon name='Cross' size={16} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 9 : 0 }} />
              </TouchableOpacity>
              <View style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundColor: '#fff', alignItems: 'center', flexDirection: 'row' }}>
                <Icon name="Search" color="#A9A9A9" size={14} style={{ marginLeft: 20, marginTop: 20, alignSelf: 'center' }} />
                <TextInput
                  theme={{ colors: { text: '#154A59', primary: '#154A59', placeholder: '#A9A9A9' } }}
                  // mode='outlined'
                  placeholder="Search by Country Name"
                  selectionColor='#C8DB6E'
                  style={[styles.inputFocus, { height: 30, width: '100%', marginTop: 10, backgroundColor: "#fff", color: "#D0E8C8" }]}
                  onChangeText={(value) => this.setState({ query: value })}
                  value={this.state.query}
                />
              </View>
              <FlatList
                keyboardShouldPersistTaps='handled'
                data={this.findCountry(this.state.query)}
                style={{ backgroundColor: "#F7F7F5", width: '100%', height: '70%' }}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item) => item.value.code + item.value.country}
                renderItem={({ item }) => <Text style={{ padding: 10, color: "#154A59", fontSize: 16, marginLeft: 15, borderBottomColor: '#ccc', borderBottomWidth: 0.8 }}
                  onPress={() => this.setState({ selectedCountryCode: item.value.code, country: item.value.country, modalCountry: false })}>
                  {item.label}</Text>
                }
              />
            </View>
          </Modal>

          {/* country picker */}

          {/* State picker */}

          <Modal visible={this.state.modalState} animationType="slide"
            onRequestClose={() => (this.setState({ modalState: false }))}
            transparent={true} backdropColor="transparent" supportedOrientations={['portrait', 'landscape']}>
            <View style={styles.modalCountry}>
              <TouchableOpacity style={{ alignSelf: 'center', width: 42, height: 42, borderRadius: 42 / 2, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F7F7F5', marginRight: 10, marginBottom: 10 }}
                onPress={() => this.setState({ modalState: false })} >
                <Icon name='Cross' size={16} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 9 : 0 }} />
              </TouchableOpacity>
              <View style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundColor: '#fff', alignItems: 'center', flexDirection: 'row' }}>
                <Icon name="Search" color="#A9A9A9" size={14} style={{ marginLeft: 20, marginTop: 20, alignSelf: 'center' }} />
                <TextInput
                  theme={{ colors: { text: '#154A59', primary: '#154A59', placeholder: '#A9A9A9' } }}
                  // mode='outlined'
                  placeholder="Search by Name"
                  selectionColor='#C8DB6E'
                  style={[styles.inputFocus, { height: 30, width: '100%', marginTop: 10, backgroundColor: "#fff", color: "#D0E8C8" }]}
                  onChangeText={(value) => this.setState({ queryState: value })}
                  value={this.state.queryState}
                />
              </View>
              {!this.state.isLoading ? <FlatList
                keyboardShouldPersistTaps='handled'
                data={this.findState(this.state.queryState)}
                style={{ backgroundColor: "#F7F7F5", width: '100%', height: '70%' }}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item) => item.value}
                renderItem={({ item }) => <Text style={{ padding: 10, color: "#154A59", fontSize: 16, marginLeft: 15, borderBottomColor: '#ccc', borderBottomWidth: 0.8 }}
                  onPress={() => this.setState({ statevalue: item.value, modalState: false })}>
                  {item.label}</Text>
                }
              /> : <ActivityIndicator size="large" color="#00394D" style={{ backgroundColor: "#F7F7F5", width: '100%', height: '70%' }} />}
            </View>
          </Modal>

          {/* State picker */}

          {/* City picker */}

          <Modal visible={this.state.modalCity} animationType="slide"
            onRequestClose={() => (this.setState({ modalCity: false }))}
            transparent={true} backdropColor="transparent" supportedOrientations={['portrait', 'landscape']}>
            <View style={styles.modalCountry}>
              <TouchableOpacity style={{ alignSelf: 'center', width: 42, height: 42, borderRadius: 42 / 2, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F7F7F5', marginBottom: 10 }}
                onPress={() => this.setState({ modalCity: false })} >
                <Icon name='Cross' size={16} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 9 : 0 }} />
              </TouchableOpacity>
              <View style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundColor: '#fff', alignItems: 'center', flexDirection: 'row' }}>
                <Icon name="Search" color="#A9A9A9" size={14} style={{ marginLeft: 20, marginTop: 20, alignSelf: 'center' }} />
                <TextInput
                  theme={{ colors: { text: '#154A59', primary: '#154A59', placeholder: '#A9A9A9' } }}
                  // mode='outlined'
                  placeholder="Search by Name"
                  selectionColor='#C8DB6E'
                  style={[styles.inputFocus, { height: 30, width: '100%', marginTop: 10, backgroundColor: "#fff", color: "#D0E8C8" }]}
                  onChangeText={(value) => this.setState({ queryCity: value })}
                  value={this.state.queryCity}
                />
              </View>
              {!this.state.isLoading ? <FlatList
                keyboardShouldPersistTaps='handled'
                data={this.findCity(this.state.queryCity)}
                style={{ backgroundColor: "#F7F7F5", width: '100%', height: '70%' }}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item) => item.value}
                renderItem={({ item }) => <Text style={{ padding: 10, color: "#154A59", fontSize: 16, marginLeft: 15, borderBottomColor: '#ccc', borderBottomWidth: 0.8 }}
                  onPress={() => this.setState({ city: item.value, modalCity: false })}>
                  {item.label}</Text>
                }
              /> : <ActivityIndicator size="large" color="#00394D" style={{ backgroundColor: "#F7F7F5", width: '100%', height: '70%' }} />}
            </View>
          </Modal>

          {/* City picker */}

          <View style={[styles.form, { marginTop: '20%' }]}>

            <TouchableOpacity style={(this.state.modalCountry || this.state.modalState || this.state.modalCity) ? [styles.inputBlurAddress, { backgroundColor: '#FFFFFF' }] : styles.inputBlurAddress} activeOpacity={0.5}
              onPress={() => this.setState({ ...this.state, modalCountry: true })}>
              {/* {console.log(this.state.modalCountry)} */}
              <Text numberOfLines={1}
                style={this.state.country === 'Select Country' ? { fontFamily: 'Montserrat-Medium', fontSize: 16, width: '90%', color: "#367681" } : { fontFamily: 'Montserrat-Medium', fontSize: 16, width: '90%', color: "#154A59" }}>
                {this.state.country}
              </Text>
              <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: '#E7F3E3', justifyContent: 'center', alignItems: 'center' }}><Icon name='Arrow_Down' size={14} color='#91B3A2' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /></View>
            </TouchableOpacity>


            {this.state.country === 'Select Country' ?
              <View style={[styles.inputBlurAddress, { opacity: 0.5 }]}>
                <Text numberOfLines={1}
                  style={this.state.statevalue === 'Select Province/State' ? { fontFamily: 'Montserrat-Medium', fontSize: 16, width: '90%', color: "#91B3A2" } : { fontFamily: 'Montserrat-Medium', fontSize: 16, width: '90%', color: "#154A59" }}>
                  {this.state.statevalue}
                </Text>
                <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: '#E7F3E3', justifyContent: 'center', alignItems: 'center' }}><Icon name='Arrow_Down' size={14} color='#91B3A2' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /></View>
              </View>
              :

              <TouchableOpacity style={styles.inputBlurAddress} activeOpacity={0.5}
                onPress={() => {
                  this.setState({ ...this.state, modalState: true, stateArr: [], isLoading: true })
                  axios({
                    method: "get",
                    url: REACT_APP_userServiceURL + "/backend/region/states/" + this.state.country,
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true,
                  })
                    .then((response) => {
                      response && response.data && response.data.body.map((value) => {
                        this.state.stateArr.push({ label: value, value: value })
                      })
                    }).then(() => this.setState({ ...this.state, isLoading: false }))
                    .catch((err) => {
                      console.log(err);
                      this.setState({ stateArr: [{ label: 'Please select country first', value: '' }], isLoading: false })
                    })
                }
                }>
                <Text numberOfLines={1}
                  style={this.state.statevalue === 'Select Province/State' ? { fontFamily: 'Montserrat-Medium', fontSize: 16, width: '90%', color: "#367681" } : { fontFamily: 'Montserrat-Medium', fontSize: 16, width: '90%', color: "#154A59" }}>
                  {this.state.statevalue}
                </Text>
                <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: '#E7F3E3', justifyContent: 'center', alignItems: 'center' }}><Icon name='Arrow_Down' size={14} color='#91B3A2' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /></View>
              </TouchableOpacity>
            }


            {this.state.statevalue === 'Select Province/State' ?
              <View style={[styles.inputBlurAddress, { opacity: 0.5 }]}>
                <Text numberOfLines={1}
                  style={this.state.city === 'Select City/District' ? { fontFamily: 'Montserrat-Medium', fontSize: 16, width: '90%', color: "#91B3A2" } : { fontFamily: 'Montserrat-Medium', fontSize: 16, width: '90%', color: "#154A59" }}>
                  {this.state.city}
                </Text>
                <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: '#E7F3E3', justifyContent: 'center', alignItems: 'center' }}><Icon name='Arrow_Down' size={14} color='#91B3A2' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /></View>
              </View>
              :

              <TouchableOpacity style={styles.inputBlurAddress} activeOpacity={0.5}
                onPress={() => {
                  this.setState({ ...this.state, modalCity: true, cityArr: [], isLoading: true })
                  axios({
                    method: "get",
                    url: REACT_APP_userServiceURL + "/backend/region/cities/" + this.state.statevalue,
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true,
                  })
                    .then((response) => {
                      response && response.data && response.data.body.map((value) => {
                        this.state.cityArr.push({ label: value, value: value })
                      })
                    }).then(() => this.setState({ ...this.state, isLoading: false }))
                    .catch((err) => {
                      console.log(err);
                      this.setState({ cityArr: [{ label: 'Please select state first', value: '' }], isLoading: false })
                    })
                }
                }>
                <Text numberOfLines={1}
                  style={this.state.city === 'Select City/District' ? { fontFamily: 'Montserrat-Medium', fontSize: 16, width: '90%', color: "#367681" } : { fontFamily: 'Montserrat-Medium', fontSize: 16, width: '90%', color: "#154A59" }}>
                  {this.state.city}
                </Text>
                <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: '#E7F3E3', justifyContent: 'center', alignItems: 'center' }}><Icon name='Arrow_Down' size={14} color='#91B3A2' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /></View>
              </TouchableOpacity>
            }
            {this.props.userType === 'Company' ?
              <TextInput
                theme={{ colors: { text: '#154A59', primary: '#91B3A2', placeholder: '#91B3A2' } }}
                // mode='outlined'
                label="Office Address"
                selectionColor='#C8DB6E'
                style={[styles.inputBlur, { backgroundColor: '#FFFFFF', height: 50, alignSelf: 'center', width: 311 }]}
                // onFocus={() => { this.setState({ focus: 'officeAddress' }) }}
                // onBlur={() => { this.setState({ focus: '' }) }}
                value={this.state.officeAddress}
                onChangeText={(value) => this.setState({ officeAddress: value })}
              /> :
              <></>
            }
          </View>

          {this.state.isLoading ?
            <View style={[styles.agreeLocation, { position: 'absolute', bottom: 50 }]}>
              <ActivityIndicator color="#fff" size="large" />
            </View>
            :
            <TouchableHighlight
              underlayColor="#00394D"
              style={[styles.agreeLocation, { position: 'absolute', bottom: 50 }]}
              onPress={this.handleSubmit}>
              {/* // style={[styles.agree, { marginTop: '45%' }]}
              // onPress={this.props.userType === 'Company' ? this.handleSubmitCompany : this.handleSubmit}> */}
              <Text style={styles.agreeTxt2}>Done</Text>
            </TouchableHighlight>}

        </View>

      </ScrollView>
    )
  }
}
