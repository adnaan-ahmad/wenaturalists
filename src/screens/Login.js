import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Platform,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  Linking
} from 'react-native'
import _ from "lodash";
import { TextInput } from 'react-native-paper'
import Snackbar from 'react-native-snackbar';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage'
import Header from '../Components/User/SignUp/Header'
import { loginRequest, onClear } from '../services/Redux/Actions/common/LoginActions'
import welogo from '../../assets/welogo.png'
import bg from '../../assets/BackgroundImg.png'
import AddProfilePicture from '../Components/User/SignUp/AddProfilePicture'
import Otp from './Otp'
import UserAddress from './UserAddress'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../assets/Icons/selection.json'
import EyeIcon from 'react-native-vector-icons/MaterialIcons'
import loginbg from '../../assets/loginbg.png'
import { REACT_APP_environment } from '../../env.json'
import messaging from '@react-native-firebase/messaging'
import { COLORS } from '../Components/Shared/Colors';

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const windowHeight = Dimensions.get('window').height

class Login extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: true,
      loggedinUser: null,
      focus: '',
      username: '',
      password: '',
      errmsg: '',
      passwordIcon: 'visibility-off',
      currentScreen: 0,
      userId: '',
      firstName: ''
    };
  }

  changeState = (value) => {
    this.setState(value)
  }

  componentDidMount() {
    // this.props.onClear()
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  errfun = () => {
    this.props.error ?
      this.setState({ errmsg: this.props.error.message })
      : null
  }

  componentDidUpdate(prevProps) {
    if (this.props.error !== prevProps.error) {
      this.errfun()
    }
  }

  componentDidCatch(error) {
    console.log("didcatch error: ", error)
  }

  handleSubmit = () => {
    let postBody = {
      "username": this.state.username,
      "password": this.state.password
    };

    if (this._isMounted) {

      if (this.state.username !== '' && this.state.password !== '') {
        this.props.loginRequest(postBody)
      }

      if (this.state.username === '' || this.state.password === '') {
        Snackbar.show({
          backgroundColor: '#B22222',
          text: "Please enter all credentials",
          duration: Snackbar.LENGTH_LONG,
        });
      }
    }
  };

  render() {

    // console.log('.................. this.props.user.body ..................', this.props.user.body)

    if (!_.isEmpty(this.props.user) && this.props.user.message === 'Success!' && this.props.user.body.transactionId === null && !this.props.user.body.userDataPending) {
      console.log("render userdata : ", this.props.user);

      messaging()
        .subscribeToTopic(REACT_APP_environment + "_push_notifi_" + this.props.user.body.userId)
        .then(() => {
          console.log('Subscribed to topic : ', REACT_APP_environment + "_push_notifi_" + this.props.user.body.userId)
          this.props.onClear()
          this.props.navigation.replace("BottomTab");
        })
        .catch(e => console.log("messaging err", e))

      return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size='large' color={COLORS.primarydark} />
      </View>
    }

    else if (!_.isEmpty(this.props.user) && this.props.user.message === 'Success!' && this.props.user.body.transactionId !== null && this.state.currentScreen !== 3 && this.state.currentScreen !== 4) {
      AsyncStorage.removeItem("refreshToken")
      return <Otp
        mobileNumber={this.props.user.body.mobile}
        userId={this.props.user.body.userId}
        transactionId={this.props.user.body.transactionId}
        country={this.props.user.body.country}
        changeState={this.changeState}
      />
    }

    else if (this.state.currentScreen === 3 || (!_.isEmpty(this.props.user) && this.props.user.message === 'Success!' && this.props.user.body.userDataPending && this.state.currentScreen !== 4)) {
      console.log("current screen: ", this.state.currentScreen)
      return <UserAddress
        changeState={this.changeState}
        userId={this.props.user.body.userId}
        userType='Individual'
      />
    }

    else if (this.state.currentScreen === 4) {

      messaging()
        .subscribeToTopic(REACT_APP_environment + "_push_notifi_" + this.props.user.body.userId)
        .then(() => {
          console.log('Subscribed to topic : ', REACT_APP_environment + "_push_notifi_" + this.props.user.body.userId)
          this.props.onClear()
          this.props.navigation.replace("BottomTab");
        })
        .catch(e => console.log("messaging err", e))
      
      return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size='large' color={COLORS.primarydark} />
      </View>
    }

    else {
      return (
        <View>
          <ImageBackground style={{ height: '100%', width: '100%' }} source={loginbg}>

            <ScrollView keyboardShouldPersistTaps='handled'
              showsVerticalScrollIndicator={false}>

              <View style={this.state.focus.length !== 0 && Platform.OS === 'ios' ? { alignItems: 'center', height: windowHeight * 1.22 } : { alignItems: 'center', height: windowHeight }}>

                {/* Header */}

                <Image source={welogo} style={{ height: 120, width: 90, marginTop: 20 }} />

                <View style={{ alignItems: 'center', marginTop: '6%' }}>
                  <Text style={{ color: '#FFFFFF', fontSize: 32, fontStyle: 'italic' }}>Welcome Back!</Text>
                  <Text style={{ color: '#D0E8C8', fontSize: 14, fontFamily: 'Montserrat-Medium', }}>Sign in to keep updated with</Text>
                  <Text style={{ color: '#D0E8C8', fontSize: 14, fontFamily: 'Montserrat-Medium', }}>the community</Text>
                </View>

                {/* Header */}

                {/* Email & Password */}

                <View style={styles.form}>
                  <TextInput

                    theme={(this.state.errmsg && this.state.errmsg !== '' && this.state.errmsg.includes("username") === true) ?
                      { colors: { text: '#FFFFFF', primary: '#EE8973', placeholder: '#EE8973' } } : { colors: { text: '#FFFFFF', primary: '#D0E8C8', placeholder: '#D0E8C8' } }}
                    //mode='outlined'
                    label={(this.state.errmsg && this.state.errmsg !== '' && this.state.errmsg.includes("username") === true) ?
                      this.state.errmsg : "Email or Phone"}
                    selectionColor='#C8DB6E'
                    style={this.state.username !== '' || this.state.focus === 'username' ? [styles.inputBlur, {
                      backgroundColor: '#F7F7F533', height: 50
                    }] : [styles.inputBlur]}
                    onFocus={() => { this.changeState({ ...this.state, focus: 'username' }) }}
                    onBlur={() => { this.changeState({ ...this.state, focus: '' }) }}
                    // value={this.state.username}
                    onChangeText={(value) => this.changeState({ ...this.state, username: value, errmsg: '' })}

                  />


                  {this.state.focus === 'username' ? <View style={{ backgroundColor: '#BFC52E', width: '100%', height: 2, marginTop: -6 }}></View> : <></>}

                  <View style={{ flexDirection: 'row' }}>
                    <TextInput
                      secureTextEntry={this.state.passwordIcon === 'visibility' ? false : true}
                      style={this.state.password !== '' || this.state.focus === 'password' ? [styles.inputBlur, {
                        backgroundColor: '#F7F7F533', height: 50
                      }] : styles.inputBlur}
                      onFocus={() => { this.setState({ focus: 'password' }) }}
                      onBlur={() => { this.setState({ focus: '' }) }}
                      label={(this.state.errmsg && this.state.errmsg !== '' && this.state.errmsg.includes("password") === true) ?
                        this.state.errmsg : "Password"}
                      onChangeText={(value) => this.setState({ password: value, errmsg: '' })}
                      theme={(this.state.errmsg && this.state.errmsg !== '' && this.state.errmsg.includes("password") === true) ?
                        { colors: { text: '#FFFFFF', primary: '#EE8973', placeholder: '#EE8973' } } : { colors: { text: '#FFFFFF', primary: '#D0E8C8', placeholder: '#D0E8C8' } }}
                      selectionColor='#C8DB6E'
                    />

                    {this.state.focus === 'password' || this.state.password !== '' ? <EyeIcon style={styles.icon}
                      name={this.state.passwordIcon}
                      size={17}
                      color='#91B3A2'
                      onPress={() => {
                        this.state.passwordIcon === 'visibility' ? this.changeState({ passwordIcon: 'visibility-off' }) : this.changeState({ passwordIcon: 'visibility' })
                      }}
                    /> : <></>}
                  </View>
                  {this.state.focus === 'password' ? <View style={{ backgroundColor: '#BFC52E', width: '100%', height: 2, marginTop: -6 }}></View> : <></>}
                </View>

                {/* Email & Password */}

                <View style={this.state.focus.length === 0 ? { justifyContent: 'center', alignItems: 'center' } :
                  { justifyContent: 'center', alignItems: 'center' }}>

                  <TouchableOpacity activeOpacity={0.5} onPress={this.handleSubmit}
                    style={styles.logIn}>
                    {
                      this.props.loginProgress ?
                        <ActivityIndicator size="small" color="#fff" style={{ alignSelf: 'center' }} />
                        :
                        <Text style={styles.logInText}>LOGIN</Text>
                    }
                  </TouchableOpacity>

                </View>

                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('ResetPassword')}
                  activeOpacity={0.5}
                  style={{ marginTop: '3%', justifyContent: 'center', alignItems: 'center', width: '36%' }}>
                  <Text style={{ marginTop: '1.5%', color: '#91B3A2', fontSize: 14, fontWeight: '500' }}>
                    <Icon name='PrivacyLock' size={18} color='#91B3A2' style={{}} /> Forgot Password
                  </Text>

                </TouchableOpacity>


              </View>

              <View style={{ position: 'absolute', bottom: 20, alignSelf: 'center' }}>

                <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigation.navigate('Signup')}
                  style={styles.signUp}>
                  <Text style={{ fontSize: 20, color: '#00394D', fontFamily: 'Montserrat-Bold' }}>SIGN UP</Text>
                </TouchableOpacity>


                <TouchableOpacity onPress={() => Linking.openURL('mailto:support@wenaturalists.com')} activeOpacity={0.5} style={{ flexDirection: 'row', justifyContent: 'space-around', width: '100%', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <Icon name='Help' size={18} color='#91B3A2' style={{}} />
                    <Text style={Platform.OS === 'ios' ? { marginLeft: 8, color: '#91B3A2', fontSize: 14, marginTop: 1, fontFamily: 'Montserrat-Medium' } : { marginLeft: 8, color: '#91B3A2', fontSize: 14, marginTop: 5, fontFamily: 'Montserrat-Medium' }}>Customer Support</Text>
                  </View>
                  <View style={{ backgroundColor: '#CFE7C71A', width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center', marginTop: 10 }}><Icon name='Arrow-Right' size={15} color='#91B3A2' style={{ marginTop: Platform.OS === 'android' ? 8 : 0 }} /></View>
                </TouchableOpacity>


              </View>

            </ScrollView>

          </ImageBackground>
        </View>
      )
    }

  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 0
  },
  icon: {
    padding: 10,
    position: 'absolute',
    top: 24,
    right: 10,
    zIndex: 2
  },
  mainSection: {

  },
  footer: {
    backgroundColor: '#00394D',
    alignItems: 'center',
    alignSelf: 'stretch',
    height: 100
  },
  logo: {
    width: 215,
    height: 80,
    marginTop: 75
  },
  headers: {
    marginTop: 30
  },
  header: {
    fontSize: 20,
    lineHeight: 24,
    color: '#698F8A',
    fontWeight: '600'
  },
  header2: {
    fontSize: 20,
    lineHeight: 24,
    color: '#698F8A',
    textAlign: 'center',
    fontWeight: '700'
  },
  form: {
    marginTop: '10%',
    width: '100%'
  },
  inputFocus: {
    fontSize: 14,
    borderRadius: 6,
    backgroundColor: '#FFFFFF',
    width: 280,
    height: 43,
    marginBottom: '1.5%',
  },

  inputBlur: {
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    fontSize: 16,
    backgroundColor: '#F7F7F51A',
    width: '100%',
    height: 40,
    marginBottom: 4,
    borderBottomWidth: 0,
    borderBottomColor: 'transparent'
  },
  logIn: {
    width: 148,
    height: 48,
    alignSelf: 'center',
    borderRadius: 35,
    marginTop: 25,
    marginBottom: 15,
    fontWeight: 'bold',
    backgroundColor: '#CFE7C71A',
    textAlign: 'center',
    fontSize: 13,
    paddingHorizontal: 10,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  signUp: {
    width: 280,
    height: 54,
    alignSelf: 'center',
    borderRadius: 35,
    fontWeight: 'bold',
    backgroundColor: '#BFC52E',
    textAlign: 'center',
    fontSize: 13,
    paddingHorizontal: 10,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logInText: {
    alignSelf: 'center',
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
  },
  forgotPassword: {
    color: '#698F8A',
    fontSize: 15,
    fontFamily: 'Montserrat-Medium',
  },
});

const mapStateToProps = (state) => {
  return {
    loginProgress: state.loginReducer.loginProgress,
    user: state.loginReducer.user,
    error: state.loginReducer.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loginRequest: (data) => dispatch(loginRequest(data)),
    onClear: () => dispatch(onClear())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
