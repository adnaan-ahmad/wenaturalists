// src/screens/Home.js

import React, {Component} from 'react'
import LottieView from 'lottie-react-native';

import lottieAnimation from '../lottie/splashanimation.json'


export default class Lottie extends Component {

    render () {
        return (
                <LottieView source={lottieAnimation} autoPlay loop={false} 
                onAnimationFinish={()=>this.props.navigation.replace('Login')} />
        )
    }
        
}
  