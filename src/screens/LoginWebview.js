import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Platform,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {WebView} from 'react-native-webview';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import messaging from '@react-native-firebase/messaging';

import {REACT_APP_environment, REACT_APP_domainUrl} from '../../env.json';
import {COLORS} from '../Components/Shared/Colors';
import icoMoonConfig from '../../assets/Icons/selection.json';
import typography from '../Components/Shared/Typography';
import defaultShape from '../Components/Shared/Shape';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class ProjectsWebview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: REACT_APP_domainUrl + '/login-mob',
    };
  }

  webLoader = () => {
    return (
      <View
        style={{
          height: '100%',
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: COLORS.grey_100,
        }}>
        <ActivityIndicator size="large" color={COLORS.primarydark} />
      </View>
    );
  };

  saveUserDetails = (data) => {
    let userDetails = JSON.parse(data);
    console.log('user data', userDetails);
    if (userDetails.type === 'login') {
      AsyncStorage.setItem('refreshToken', userDetails.token);
      AsyncStorage.setItem('userId', userDetails.userId);
      AsyncStorage.setItem(
        'userData',
        JSON.stringify({
          userId: userDetails.userData.userId,
          mobile: userDetails.userData.mobile,
          email: userDetails.userData.email,
          firstName: userDetails.userData.firstName,
          lastName: userDetails.userData.lastName,
          userDataPending: userDetails.userData.userDataPending,
          interests: userDetails.userData.interests,
          persona: userDetails.userData.persona,
          skills: userDetails.userData.skills,
          profileImage: userDetails.userData.profileImage,
          resizedProfileImages: userDetails.userData.resizedProfileImages,
          cookiesAccepted: userDetails.userData.cookiesAccepted,
          type: userDetails.userData.type,
          specialities: userDetails.userData.specialities,
          indianResidence: userDetails.userData.indianResidence,
          customUrl: userDetails.userData.customUrl,
          companyName: userDetails.userData.companyName,
        }),
      );

      messaging()
        .subscribeToTopic(
          REACT_APP_environment + '_push_notifi_' + userDetails.userId,
        )
        .then(() => {
          console.log(
            'Subscribed to topic : ',
            REACT_APP_environment +
              '_push_notifi_' +
              userDetails.userId,
          );
          this.props.navigation.replace('BottomTab');
        })
        .catch((e) => console.log('messaging err', e));
    }
  };

  //   onAndroidBackPress = () => {
  //     if (this.webView.canGoBack && this.webView.ref && Platform.OS === 'android') {
  //       this.webView.ref.goBack();
  //       return true;
  //     }
  //     return false;
  //   };

  //   componentDidMount() {
  //     if (Platform.OS === 'android') {
  //       BackHandler.addEventListener(
  //         'hardwareBackPress',
  //         this.onAndroidBackPress,
  //       );
  //     }
  //   }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <WebView
          source={{uri: this.state.url}}
          useWebKit={true}
          allowsBackForwardNavigationGestures={true}
          allowsInlineMediaPlayback={true}
          userAgent={Platform.OS === 'android' ? 'wenat_android' : 'wenat_ios'}
          //   ref={(webView) => {
          //     this.webView.ref = webView;
          //   }}
          startInLoadingState={true}
          originWhitelist={['*']}
          renderLoading={this.webLoader}
          allowUniversalAccessFromFileURLs={true}
          javaScriptEnabled={true}
          javaScriptEnabledAndroid={true}
          domStorageEnabled={true}
          bounces={false}
          mixedContentMode={'always'}
          onMessage={(event) => {
            const {data} = event.nativeEvent;
            this.saveUserDetails(data);
          }}
          //   onNavigationStateChange={(navState) => {
          //     this.webView.canGoBack = navState.canGoBack;
          //   }}
          onError={(syntheticEvent) => {
            const {nativeEvent} = syntheticEvent;
            console.log('Error occured : ', nativeEvent);
            this.setState({errPage: true});
          }}
        />
      </SafeAreaView>
    );
  }
}
