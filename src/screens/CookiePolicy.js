import React, { Component } from 'react'
import {
    Text,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    SafeAreaView,
    ScrollView,
    Modal,
    TouchableHighlight,
    ActivityIndicator,
    FlatList,
    ImageBackground,
    Platform
} from 'react-native'
import welogo from '../../assets/welogo.png'

export default class CookiePolicy extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (

            <ScrollView stickyHeaderIndices={[0]}
                showsHorizontalScrollIndicator={false}
                // ref={ref => skillScrollview = ref}
                // onContentSizeChange={() => {
                //     skillScrollview.scrollToEnd({ animated: true });
                // }}
                showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled' contentContainerStyle={Platform.OS === 'ios' ? { marginTop: 0, paddingBottom: 40, backgroundColor: '#F7F7F5' } : { marginBottom: 20, paddingBottom: 40, backgroundColor: '#F7F7F5' }}>

                <View style={{ width: '100%', backgroundColor: '#FFF', paddingVertical: 8, paddingLeft: 12 }}>
                    <Image source={welogo} style={{ width: 47, height: 63, alignSelf: 'flex-start', justifyContent: 'flex-start' }} />
                </View>

                <View style={{ alignSelf: 'center', marginTop: 20 }}>
                    <Text style={{ textAlign: 'center', fontWeight: '700', fontSize: 24, color: '#00394d', marginBottom: 15 }}>COOKIE POLICY</Text>
                </View>

                <View style={{ borderRadius: 8, backgroundColor: '#FFF', width: '90%', marginHorizontal: 'auto', paddingHorizontal: '8%', paddingVertical: '6%', alignSelf: 'center' }}>
                    
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>This Cookie Policy explains how we use cookies provided to us through or in association with your use of the website www.wenaturalists.com ("Website"), its related features and functions and content as defined in the Terms and Conditions and also explains how we use similar technologies to collect and use data as part of our Services and which includes our sites, communications, mobile applications and off-site Services, such as our ad services and the "Apply with WeNaturalists" and "Share with WeNaturalists" plugins or tags ("Technologies"). In the spirit of transparency, this Cookie Policy provides detailed information about how and when we use these Technologies.</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Please note that any users and third parties such as our customers, partners, and service providers may also use cookies and similar Technologies, over which we have no control.</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>You can block cookies by activating the setting on your browser which allows you to refuse the setting of all or some cookies. However, if you use your browser settings to block all cookies (including essential cookies) you may not be able to access all or parts of the Website. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies as soon you visit the Website. Further, some browsers may allow you to review and erase cookies as well, including the cookies related to this Website. By visiting this Website and continuing to use this Website you are agreeing to the use of Technologies for the purposes described in this Cookie Policy. If you do not agree please do not use or access our Website.</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>This Cookie Policy is subject to change at any time without notice. The Company reserves all rights to change, modify, update, or alter ("update") this Cookie Policy at any point of time with or without notifying you. Your continued use of the Website means that you accept any updated terms and conditions that we come up with. If you do not wish to accept any update to this Cookie Policy, then you shall immediately stop accessing and/or using the Website. To make sure you are aware of any changes, please review this Cookie Policy periodically.</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>This Cookie Policy is incorporated into and subject to the Privacy Policy.</Text>
                    
                    <Text style={{ textAlign: 'center', fontWeight: '700', fontSize: 18, color: '#00394d', marginVertical: 20 }}>1. TECHNOLOGIES USED</Text>                    
                    <Text style={{ fontWeight: '600', fontSize: 16, color: '#326169', marginBottom: 14 }}>(A) Cookies:</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Our Website uses "cookies" to collect data to recognize you and your devices and devices where you have accessed the Services (as defined in the Privacy Policy). A cookie is a small amount of data that is sent to your browser from an Internet server and stored on your computer. It enables the Website features and functionality. Any visitor may receive cookies from us or cookies from third parties such as our customers, partners, or service providers. We or third parties may also place cookies in your browser when you visit third-party sites that display ads, host our plugins (for example, the "Share with WeNaturalists" button) or tags.</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Generally, we use cookies to remind us of who you are, tailor the Content and the Services to suit your personal interests, estimate our audience size, track your status in our promotions, and/or analyzing your visiting patterns. We also collect (or rely on others who collect) information about your device (such as IP address, operating system, and browser information) where you have not engaged with the Content so we can provide our users with relevant ads and better understand their effectiveness.</Text>

                    <Text style={{ textDecorationLine: 'underline', fontWeight: '600', fontSize: 15, color: '#326169', marginBottom: 14 }}>We use two types of cookies:</Text>

                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14, marginLeft: 10 }}>i.  Persistent Cookies: A persistent cookie may help us recognize you as an existing user, so it's easier to return to our Website without having to log in again. A persistent cookie stays in your browser and will be read by us when you return to our Websites or an Associate Website (as described in the Privacy Policy).</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14, marginLeft: 10 }}>ii.  Session Cookies. Session cookies last only as long as the session (usually the current visit to a website or a browser session).</Text>

                    <Text style={{ fontWeight: '600', fontSize: 16, color: '#326169', marginBottom: 14 }}>(B) Pixels:</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>A pixel is a tiny image that may be found within web pages and emails, requiring a call (which provides device and visit information) to our servers in order for the pixel to be rendered in those web pages and emails. We use pixels to learn more about your interactions with email content or web content, such as whether you interact with ads or posts. Pixels can also enable us and third parties to place cookies on your browser.</Text>

                    <Text style={{ fontWeight: '600', fontSize: 16, color: '#326169', marginBottom: 14 }}>(C) Local Storage:</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Local storage allows an application or a website to store information locally on your device(s). We may use local storage to improve the experience when you use our Website and to, inter alia, remember your preferences, speed up the functionality of our Website and to enable certain features on our Website.</Text>

                    <Text style={{ fontWeight: '600', fontSize: 16, color: '#326169', marginBottom: 14 }}>(D) Other Similar Technologies:</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Please note that we also use other tracking technologies for similar purposes as described in this Cookie Policy. Such similar technologies may include mobile advertising IDs and tags. The similar technologies referred to in this Cookie Policy include pixels, local storage, and other tracking technologies.</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Please note that the names of Technologies may change over time.</Text>

                    <Text style={{ textAlign: 'center', fontWeight: '700', fontSize: 18, color: '#00394d', marginVertical: 20 }}>2. USE OF TECHNOLOGIES</Text>                    
                    <Text style={{ textDecorationLine: 'underline', fontWeight: '600', fontSize: 15, color: '#326169', marginBottom: 14 }}>The Technologies mentioned herein are used for the following purposes:</Text>
                    <Text style={{ fontWeight: '600', fontSize: 16, color: '#326169', marginBottom: 14 }}>(A) For Authentication Purposes:</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>We use Technologies to recognize you when you visit our Website. If you are a member (as described in the Privacy Policy) and are logged into your account on our Website, the technologies, we use, assist us in showing you the correct information, help us to personalize your experience in line with your settings and allow us to identify you and verify your account.</Text>

                    <Text style={{ fontWeight: '600', fontSize: 16, color: '#326169', marginBottom: 14 }}>(B) For Security Purposes:</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>We use Technologies to make your interactions with our Website faster and more secure such as to enable and support our security features and functions, ensure the safety of your account when you register as a Member on our Website and to help up identify any suspicious and/or malicious activities and violation of our User Agreement.</Text>

                    <Text style={{ fontWeight: '600', fontSize: 16, color: '#326169', marginBottom: 14 }}>(C) For our Preferences, Features, and Services:</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>We use Technologies to enable the functionality of our Website. This includes, inter alia, assisting you in filling out forms with more ease on our Website and providing you with features, insights, and customized content in line with our plugins. We also use Technologies to remember information about your browser and your preferences such as your language of preference and your communications preferences. We may also use local storage to speed up our Website functionality.</Text>

                    <Text style={{ fontWeight: '600', fontSize: 16, color: '#326169', marginBottom: 14 }}>(D) For Customized Content:</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>We use Technologies to customize your experience on our Website. For example, we may use cookies to remember previous searches so that when you return to our Website, we can offer additional information that relates to your previous search.</Text>

                    <Text style={{ fontWeight: '600', fontSize: 16, color: '#326169', marginBottom: 14 }}>(E) For Plug-ins On And Off Our Website:</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>We use Technologies to enable our Website plugins both on and off our Website and Associate Websites.</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>For example, our plugins, including the "Apply with WeNaturalists" button or the "Share" button may be found on our Website or Associate Websites or third-party sites. Our plugins use Technologies to provide analytics and to recognize you on our Website, Associate Websites, and third-party sites. If you interact with a plugin, the plugin will use cookies to identify you and initiate your request to apply.</Text>

                    <Text style={{ fontWeight: '600', fontSize: 16, color: '#326169', marginBottom: 14 }}>(F) For Advertising Purposes:</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Technologies help us show relevant advertising to you more effectively, both on and off our Website and/or Associate Websites, and to measure the performance of such ads. We use these Technologies to learn whether the content has been shown to you or whether someone who was presented with an ad later came back and took an action (e.g. made a purchase) on another site. Similarly, our partners or service providers may use these Technologies to determine whether we have shown an ad or a post and how it performed or provide us with information about how you interact with ads.</Text>

                    <Text style={{ fontWeight: '600', fontSize: 16, color: '#326169', marginBottom: 14 }}>(G) For Performance, Analytics, and Research Purposes:</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Technologies help us learn more about how well our Website and plugins perform in different locations. We or our service providers may use Technologies to understand, improve, and research products, features, and services. We may even use Technologies to understand the manner in which you navigate through our Website and/or Associate Websites or when you access our Website from other sites, applications, or devices.</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Further, we or our service providers use Technologies to determine and measure the performance of ads or posts on and off our Website and to learn whether you have interacted with our Associate Websites, content or emails and to then provide analytics based on those interactions.</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>We also use Technologies to provide aggregated information to our customers and partners as described in our Privacy Policy.</Text>
                
                    <Text style={{ textAlign: 'center', fontWeight: '700', fontSize: 18, color: '#00394d', marginVertical: 20 }}>3. THIRD PARTIES USING TECHNOLOGIES IN CONNECTION WITH OUR WEBSITE</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Third parties such as our customers, partners, and service providers may use Technologies in connection with our Website and for purposes which may include their own marketing purposes. Further, we may also work with third parties to help us provide our Website and for our own marketing purposes.</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Third parties may also use Technologies in connection with our off-site Website, such as WeNaturalists ad services, plugins, or tags.</Text>

                    <Text style={{ textAlign: 'center', fontWeight: '700', fontSize: 18, color: '#00394d', marginVertical: 20 }}>4. YOUR CHOICES</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>You have choices on how we use Technologies. However, any limitations you set on our ability to use Technologies may affect your experience using our Website and its features and functions and our Website may also no longer be personalized to you. You may also be restricted from saving your custom settings or preferences such as your account log in information.</Text>

                    <Text style={{ textAlign: 'center', fontWeight: '700', fontSize: 18, color: '#00394d', marginVertical: 20 }}>5. OPT-OUT OF TARGETED ADVERTISING</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>You have choices regarding the personalized ads you may see. Some mobile device operating systems such as Android and iOS provide the ability to opt-out of the use of mobile advertising IDs for ads personalization. You can learn how to use these controls by visiting the manufacturer's website for both Android and iOS.</Text>

                    <Text style={{ textAlign: 'center', fontWeight: '700', fontSize: 18, color: '#00394d', marginVertical: 20 }}>6. BROWSER CONTROLS</Text>
                    <Text style={{ fontWeight: '400', fontSize: 14, color: '#326169', marginBottom: 14 }}>Most browsers allow you to control cookies through their settings, which may be adapted to reflect your consent to the use of cookies. Further, most browsers also enable you to review and erase cookies, including our Website cookies. To learn more about browser controls, please consult the documents that your browser manufacturer provides.</Text>
                    
                </View>
            </ScrollView >

        )
    }
}
