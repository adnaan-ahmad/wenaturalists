import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Modal,
  TouchableHighlight,
  ActivityIndicator,
  FlatList,
  ImageBackground,
  Linking
} from 'react-native'
// import Modal from 'react-native-modal';
import { ProgressBar, TextInput } from 'react-native-paper'
import AsyncStorage from '@react-native-community/async-storage';
import CountryCode from '../Json/CountryCode.json'
import Snackbar from 'react-native-snackbar'
import axios from "axios"
import messaging from '@react-native-firebase/messaging'
import styles from '../Components/GlobalCss/User/SignUpCss'
import EyeIcon from 'react-native-vector-icons/MaterialIcons'
import { REACT_APP_userServiceURL, REACT_APP_environment } from '../../env.json'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../assets/Icons/selection.json'
import welogo from '../../assets/welogo.png'
import bg from '../../assets/BackgroundImg.png'
import UserAddress from './UserAddress'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import AddProfilePicture from '../Components/User/SignUp/AddProfilePicture'
import Header from '../Components/User/SignUp/Header'
import httpService from '../services/AxiosInterceptors'
import { COLORS } from '../Components/Shared/Colors';
// import MyStatusBar from '../Components/Shared/MyStatusBar'

const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const uppercaseRegex = /[A-Z]/
const numberRegex = /[0-9]/
const specialCharRegex = /[.!@#$%^&*)_(+=]/

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

class Signup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: {
        firstName: false,
        lastName: false,
        email: false,
        phone: false,
        phnerr: '',
        password: false,
        blank: false
      },
      keyPressed: '',
      focus: '',
      firstName: '',
      lastName: '',
      phoneNo: '',
      email: '',
      modalOpen: false,
      otp: '',
      userid: '',
      transactionid: '',
      password: '',
      country: '',
      selectedCountryCode: 'Code',
      passwordIcon: 'visibility-off',
      isIndianResident: true,
      agreeToTerms: true,
      emailAvailable: false,
      isLoading: false,
      modalCountry: false,
      modalterm: false,
      query: '',
      countryCodes: [],
      currentScreen: 1,
      seconds: 59,
      minutes: 9,
      skip: false,
      animatingWidth: 20
    }
  }

  changeState = (value) => {
    this.setState(value)
  }

  componentDidMount() {
    if (this.state.countryCodes.length === 0) CountryCode.sort((a, b) => a.Dial - b.Dial).map((item) => {
      if (item.Name !== 'select') {
        this.state.countryCodes.push({ label: item.Unicode + ' +' + item.Dial + ' ' + item.Name, value: { code: item.Dial, country: item.Name } })
      }
    })

    this.interval = setInterval(
      () => this.setState((prevState) => ({ seconds: prevState.seconds - 1, animatingWidth: this.state.animatingWidth + 250 / 600 })),
      1000
    )
  }

  componentDidUpdate() {

    if (this.state.skip === true) {
      this.setState({ skip: false })
      this.props.navigation.navigate('Login')
    }

    if (this.state.minutes === 0 && this.state.seconds === 0) {
      clearInterval(this.interval);
    }
    if (this.state.seconds === -1) {
      this.setState({ minutes: this.state.minutes - 1, seconds: 59 })
    }
    if (this.state.seconds.toString().length === 1) {
      this.setState({ seconds: '0' + this.state.seconds })
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  findCountry = (query) => {
    if (query === '') {
      return this.state.countryCodes
    }

    const regex = new RegExp(`${query.trim()}`, 'i')

    return this.state.countryCodes.filter(item => (item.value.code + item.value.country).search(regex) >= 0)
  }

  signup1stSubmit = () => {
    if (this.state.firstName === '' || this.state.lastName === '' || this.state.selectedCountryCode === 'Code' || this.state.phoneNo === '' || this.state.email === '' || this.state.password === '') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: "Please enter all credentials",
        duration: Snackbar.LENGTH_LONG,
      })
      // this.setState({error:{blank:true}})
    }
    else if (this.state.firstName.length < 2 || this.state.lastName.length < 2 || numberRegex.test(String(this.state.firstName))=== true || numberRegex.test(String(this.state.lastName))=== true || specialCharRegex.test(String(this.state.phoneNo))=== true || emailRegex.test(String(this.state.email).toLowerCase()) === false || (this.state.password.length < 8 || uppercaseRegex.test(String(this.state.password)) === false)) {
      if (this.state.firstName.length < 2 || numberRegex.test(String(this.state.firstName))=== true) {
        // this.setState({error:{firstName:true}})
        this.state.error.firstName = true;
      }
      if (this.state.lastName.length < 2 || numberRegex.test(String(this.state.lastName))=== true) {
        // this.setState({error:{lastName:true}})
        this.state.error.lastName = true;
      }
      if (specialCharRegex.test(String(this.state.phoneNo))=== true){
        this.state.error.phone = true;
        this.state.error.phnerr = 'Invalid phone number'
      }
      if (emailRegex.test(String(this.state.email).toLowerCase()) === false) {
        // this.setState({error:{email:true}})
        this.state.error.email = true;
      }
      if (this.state.password.length < 8 || uppercaseRegex.test(String(this.state.password)) === false) {
        // this.setState({error:{password:true}})
        this.state.error.password = true;
      }
    }
    else {
      this.setState({ isLoading: true })
      let postBody = {
        "firstName": this.state.firstName.trim(),
        "lastName": this.state.lastName.trim(),
        "password": this.state.password,
        "agreeToTerms": this.state.agreeToTerms,
        "indianResident": this.state.isIndianResident,
        "mobile": this.state.phoneNo,
        "email": this.state.email.toLowerCase(),
        "country": this.state.country,
        "countryISDCode": this.state.selectedCountryCode
      };

      // axios.interceptors.request.use(config => {
      //   // perform a task before the request is sent
      //   console.log('Request was sent');

      //   return config;
      // }, error => {
      //   // handle the error
      //   return Promise.reject(error);
      // })

      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/user/signup/individual',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true
      }).then((response) => {
        let res = response.data;
        // console.log(response.data)
        if (res.message === 'Success!') {
          AsyncStorage.setItem("userId",res.body.userId)
          AsyncStorage.setItem("refreshTokenSignup",response.headers['authorization'])
          this.changeState({ minutes: 9, seconds: 59, animatingWidth: 20, transactionid: res.body.transactionId, userid: res.body.userId, isLoading: false, currentScreen: 2 })
        }
      }).catch((err) => {
        if (err && err.response && err.response.data) {
          if (err.response.data.message.includes("Mobile")) {
            this.setState({ isLoading: false, error: { phone: true, phnerr: 'Invalid mobile number' } })
          } else {
            this.setState({ isLoading: false })
            Snackbar.show({
              backgroundColor: '#B22222',
              text: err.response.data.message,
              duration: Snackbar.LENGTH_LONG,
            })
          }
        }
      });
    }
  }

  displayCurrentScreen = () => {

    // if (this.state.isLoading) {
    //   return <View style={{ flex: 1, justifyContent: 'center' }}>
    //     <ActivityIndicator color="#97A600" size="large" />
    //   </View>
    // }

    if (this.state.currentScreen === 1) {
      return (
        <SafeAreaView style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled' style={{ backgroundColor: '#F7F7F5' }}>
            {/* <MyStatusBar barStyle="light-content" /> */}
            <View style={Platform.OS === 'ios' && this.state.focus !== '' ? [styles.mainSection, { height: windowHeight * 1.65 }] : styles.mainSection}>

              <Header progress={(this.state.modalCountry || this.state.modalterm) ? -1 : 0}
                text='Join Our Community' customStyle={(this.state.modalCountry || this.state.modalterm) ? { backgroundColor: '#00394D8C', fontFamily:'Montserrat-Medium' } : { backgroundColor: '#F7F7F5', fontFamily:'Montserrat-Medium' }}
                opacity={(this.state.modalCountry || this.state.modalterm) ? 0.3 : 1} />

              {/* country picker */}

              <Modal visible={this.state.modalCountry} animationType="slide"
                transparent={true} backdropColor="transparent" supportedOrientations={['portrait', 'landscape']}>
                <View style={styles.modalCountry}>
                  <TouchableOpacity style={{ alignSelf: 'center', width: 42, height: 42, borderRadius: 42 / 2, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F7F7F5', marginRight: 10, marginBottom: 10 }}
                    onPress={() => this.setState({ modalCountry: false })} >
                    <Icon name='Cross' size={16} color='#367681' style={{ marginTop: 5 }} />
                  </TouchableOpacity>

                  <View style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundColor: '#fff', alignItems: 'center', flexDirection: 'row' }}>
                    <Icon name="Search" color="#A9A9A9" size={14} style={{ marginLeft: 20, marginTop: 20, alignSelf: 'center' }} />
                    <TextInput
                      theme={{ colors: { text: '#367681', primary: '#154A59', placeholder: '#A9A9A9' } }}
                      // mode='outlined'
                      placeholder="Search by country name or countrycode"
                      selectionColor='#C8DB6E'
                      style={[styles.inputFocus, { height: 30, width: '100%', marginTop: 10, backgroundColor: "#fff", color: "#D0E8C8" }]}
                      onChangeText={(value) => this.setState({ query: value })}
                      value={this.state.query}
                    />
                  </View>

                  <FlatList
                    keyboardShouldPersistTaps='handled'
                    data={this.findCountry(this.state.query)}
                    style={{ backgroundColor: "#F7F7F5", width: '100%', height: '65%' }}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item) => item.value.code + item.value.country}
                    renderItem={({ item }) => <Text style={{ padding: 10, color: "#154A59", fontSize: 16, marginLeft: 15, borderBottomColor: '#ccc', borderBottomWidth: 0.8 }}
                      onPress={() => this.setState({ selectedCountryCode: item.value.code, country: item.value.country, modalCountry: false })}>
                      {item.label}</Text>
                    }
                  />
                </View>
              </Modal>

              {/* country picker */}

              {/* terms and condition picker */}

              <Modal visible={this.state.modalterm} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={styles.modalCountry}>

                  <TouchableOpacity style={{ alignSelf: 'center', width: 42, height: 42, borderRadius: 42 / 2, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F7F7F5', marginRight: 10, marginBottom: 10 }}
                    onPress={() => this.setState({ modalterm: false })} >
                    <Icon name='Cross' size={16} color='#367681' style={{ marginTop: 5 }} />
                  </TouchableOpacity>

                  <View style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundColor: '#EEF1EC', alignItems: 'center', paddingVertical: 20, height: 250 }}>

                    <TouchableOpacity style={styles.termsmodal} activeOpacity={0.7} onPress={() => { this.setState({ modalterm: false }), this.props.navigation.navigate('TermsConditions')}} >
                      <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>Terms & Conditions</Text>
                      <Icon name="Export" color="#367681" />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.termsmodal} activeOpacity={0.7} onPress={() => { this.setState({ modalterm: false }), this.props.navigation.navigate('UserAgreement')}}>
                      <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>User Agreement</Text>
                      <Icon name="Export" color="#367681" />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.termsmodal} activeOpacity={0.7} onPress={() => { this.setState({ modalterm: false }), this.props.navigation.navigate('PrivacyPolicy')}}>
                      <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>Privacy Policy</Text>
                      <Icon name="Export" color="#367681" />
                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.termsmodal, { borderBottomWidth: 0 }]} activeOpacity={0.7} onPress={() => { this.setState({ modalterm: false }), this.props.navigation.navigate('CookiePolicy')}}>
                      <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>Cookie Policy</Text>
                      <Icon name="Export" color="#367681" />
                    </TouchableOpacity>

                  </View>

                </View>

              </Modal>

              {/* terms and condition picker */}

              {/***** SignUp form *****/}

              {
                (this.state.modalterm || this.state.modalCountry) ?
                  <View style={{ width: '100%', height: windowHeight, backgroundColor: '#00394D8C' }}></View>
                  :
                  <View style={styles.form}>

                    <TextInput
                      theme={(this.state.error.firstName || this.state.error.blank) ? { colors: { text: '#367681', primary: 'red', placeholder: 'red' } } : { colors: { text: '#367681', primary: '#91B3A2', placeholder: '#91B3A2' } }}

                      label={this.state.error.blank ? "Please enter your first name" :
                        ((this.state.focus === 'firstName' && this.state.firstName.length < 2) ? "First name must have minimum 2 characters" : ((this.state.firstName !== '' && this.state.firstName.length < 2) ? "First name must have minimum 2 characters" : 
                        numberRegex.test(String(this.state.firstName))=== true ? "First name cannot contain numbers" : "First Name"))
                      }
                      selectionColor='#C8DB6E'
                      style={
                        (this.state.focus === 'firstName' ? [styles.inputBlur, {
                          backgroundColor: '#FFFFFF', height: 50, borderBottomWidth: 1.5, borderTopWidth: 0.1, borderBottomColor: '#FFF', borderTopColor: '#FFF'
                        }] : [styles.inputBlur])
                      }
                      onFocus={() => { this.changeState({ focus: 'firstName', error: { firstName: false, blank: false } }) }}
                      onBlur={() => { this.changeState({ focus: '' }) }}
                      value={this.state.firstName}
                      onChangeText={(value) => this.changeState({ firstName: value })}

                    />
                    <View style={this.state.focus === 'firstName' ? { backgroundColor: '#BFC52E', width: '100%', height: 1, marginTop: -7.5 }
                      : { backgroundColor: '#F7F7F5', width: '100%', height: 10, marginTop: -7.5 }}></View>


                    <TextInput
                      theme={this.state.error.lastName ? { colors: { text: '#367681', primary: 'red', placeholder: 'red' } } : { colors: { text: '#367681', primary: '#91B3A2', placeholder: '#91B3A2' } }}
                      //mode='outlined'
                      label={(this.state.focus === 'lastName' && this.state.lastName.length < 2) ? "Last name must have minimum 2 characters" : ((this.state.lastName !== '' && this.state.lastName.length < 2) ? "Last name must have minimum 2 characters" : 
                      numberRegex.test(String(this.state.lastName))=== true ? "Last name cannot contain numbers" : "Last Name")}
                      selectionColor='#C8DB6E'
                      style={
                        (this.state.focus === 'lastName' ? [styles.inputBlur, {
                          backgroundColor: '#FFFFFF', height: 50, borderBottomWidth: 1.5, borderTopWidth: 0.1, borderBottomColor: '#FFF', borderTopColor: '#FFF'
                        }] : [styles.inputBlur])
                      }
                      onFocus={() => { this.changeState({ focus: 'lastName', error: { lastName: false } }) }}
                      onBlur={() => { this.changeState({ focus: '' }) }}
                      value={this.state.lastName}
                      onChangeText={(value) => this.changeState({ lastName: value })}

                    />

                    <View style={this.state.focus === 'lastName' ? { backgroundColor: '#BFC52E', width: '100%', height: 1, marginTop: -7.5 }
                      : { backgroundColor: '#F7F7F5', width: '100%', height: 10, marginTop: -7.5 }}></View>

                    <View style={styles.phoneNumberContainer}>
                      {/* <View> */}
                      <TouchableHighlight style={this.state.selectedCountryCode !== 'Code' ? [styles.countryCodeBlur, { backgroundColor: '#F7F7F5', height: 70, borderBottomWidth: 1.5, borderTopWidth: 0.1, borderBottomColor: '#FFF', borderTopColor: '#FFF' }] : styles.countryCodeBlur} underlayColor="#F7F7F5"
                        onPress={() => this.setState({ ...this.state, modalCountry: true })}>
                        <View style={{ backgroundColor: '#E2E7E9', height: 30, width: 70, borderRadius: 20, marginTop: 10, alignSelf: 'flex-start', marginLeft: 20, alignItems: 'center', justifyContent: 'center' }}>
                          {/* {this.state.selectedCountryCode !== 'Code' ? 
                        <Text style={{ color: '#91B3A2', fontSize: 12, marginTop: 6, marginRight: 16 }}>Code</Text> : null} */}
                          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                            <Text numberOfLines={1}
                              style={this.state.selectedCountryCode === 'Code' ? { fontSize: 16, color: "#91B3A2" } : { fontSize: 16, color: "#154A59" }}>
                              {this.state.selectedCountryCode !== 'Code' ? '+' + this.state.selectedCountryCode : this.state.selectedCountryCode}
                            </Text>
                            <Icon name='Arrow2_Down' size={14} color='#91B3A2' style={Platform.OS === 'android' ? { marginLeft: 5, marginTop: 8 } : { marginLeft: 5 }} />
                          </View>
                        </View>
                      </TouchableHighlight>
                      {/* <View style={{ backgroundColor: '#F7F7F5', width: '30%', height: 0, marginTop: 0 }}></View> */}
                      {/* </View> */}
                      <TextInput
                        theme={this.state.error.phone && this.state.focus !== 'phoneNo' ? { colors: { text: '#367681', primary: 'red', placeholder: 'red' } } : { colors: { text: '#367681', primary: '#91B3A2', placeholder: '#91B3A2' } }}
                        // onKeyPress={(e) => this.setState({ keyPressed: e.nativeEvent.key })}
                        label={this.state.error.phone && this.state.focus !== 'phoneNo' ? this.state.error.phnerr : "Phone Number"}
                        selectionColor='#C8DB6E'
                        style={this.state.focus === 'phoneNo' ? [styles.inputBlur, { backgroundColor: '#FFFFFF', height: 50, width: '70%', borderBottomWidth: 1.5, borderTopWidth: 0.1, borderBottomColor: '#FFF', borderTopColor: '#FFF' }] : [styles.inputBlur, { width: '70%', height: 50 }]}
                        onFocus={() => { this.changeState({ focus: 'phoneNo' }) }}
                        onBlur={() => { this.changeState({ focus: '' }) }}
                        value={this.state.phoneNo}
                        onChangeText={(value) => this.changeState({ phoneNo: value.trim(), error: {...this.state.error, phone: false} })}
                        keyboardType='number-pad'
                        maxLength={10}
                      />
                    </View>
                    <View style={this.state.focus === 'phoneNo' ? { backgroundColor: '#BFC52E', width: '100%', height: 1, marginTop: -7.5 }
                      : { backgroundColor: '#F7F7F5', width: '100%', height: 10, marginTop: -7.5 }}></View>


                    <TextInput
                      theme={this.state.error.email ? { colors: { text: '#367681', primary: 'red', placeholder: 'red' } } : { colors: { text: '#367681', primary: '#91B3A2', placeholder: '#91B3A2' } }}
                      // mode='outlined'
                      label={this.state.error.email ? "Please enter a valid email address" : "Email"}
                      selectionColor='#C8DB6E'
                      style={this.state.focus === 'email' ? [styles.inputBlur, { backgroundColor: '#FFFFFF', height: 50, borderBottomWidth: 1.5, borderTopWidth: 0.1, borderBottomColor: '#FFF', borderTopColor: '#FFF' }] : styles.inputBlur}
                      onFocus={() => { this.changeState({ focus: 'email', error: { email: false } }) }}
                      onBlur={() => { this.changeState({ focus: '' }) }}
                      value={this.state.email}
                      onChangeText={(value) => this.changeState({ email: value })}
                    />
                    <View style={this.state.focus === 'email' ? { backgroundColor: '#BFC52E', width: '100%', height: 1, marginTop: -7.5 }
                      : { backgroundColor: '#F7F7F5', width: '100%', height: 10, marginTop: -7.5 }}></View>

                    <View style={styles.searchSection}>

                      <TextInput
                        theme={this.state.error.password ? { colors: { text: '#367681', primary: 'red', placeholder: 'red' } } : { colors: { text: '#367681', primary: '#91B3A2', placeholder: '#91B3A2' } }}
                        // mode='outlined'
                        // label="Password"
                        label={(this.state.focus === 'password' && (this.state.password.length < 8 || uppercaseRegex.test(String(this.state.password)) === false)) ? "Atleast 8 characters with 1 uppercase" : ((this.state.password !== '' && (this.state.password.length < 8 || uppercaseRegex.test(String(this.state.password)) === false)) ? "Atleast 8 characters with 1 uppercase" : "Password")}
                        selectionColor='#C8DB6E'
                        style={this.state.focus === 'password' ? [styles.inputBlur, { backgroundColor: '#FFFFFF', height: 50, zIndex: 1, borderBottomWidth: 1.5, borderTopWidth: 0.1, borderBottomColor: '#FFF', borderTopColor: '#FFF' }] : [styles.inputBlur, { zIndex: 1 }]}
                        onFocus={() => { this.changeState({ focus: 'password', error: { password: false } }) }}
                        onBlur={() => { this.changeState({ focus: '' }) }}
                        value={this.state.password}
                        onChangeText={(value) => this.changeState({ password: value })}
                        secureTextEntry={this.state.passwordIcon === 'visibility' ? false : true}
                      />

                      {this.state.focus === 'password' || this.state.password !== '' ? <EyeIcon style={[styles.icon, { marginRight: 19 }]}
                        name={this.state.passwordIcon}
                        size={17}
                        color='#91B3A2'
                        onPress={() => {
                          this.state.passwordIcon === 'visibility' ? this.changeState({ passwordIcon: 'visibility-off' }) : this.changeState({ passwordIcon: 'visibility' })
                        }}
                      /> : <></>}

                    </View>
                    <View style={this.state.focus === 'password' ? { backgroundColor: '#BFC52E', width: '100%', height: 1, marginTop: -7.5 }
                      : { backgroundColor: '#F7F7F5', width: '100%', height: 10, marginTop: -7.5 }}></View>


                    <View style={{ backgroundColor: '#F7F7F5' }}>

                      <View style={styles.termsConditions}>
                        <View style={{ width: 34, height: 34, borderRadius: 17, backgroundColor: '#E2E7E9', justifyContent: 'center', alignItems: 'center' }}><Icon name='FAQ' size={18} color='#367681' style={{}} /></View>

                        <View style={{ marginTop: 3 }}>
                          <Text style={[styles.term, { marginLeft: 10 }]}>By continuing you agree with</Text>
                          <TouchableOpacity activeOpacity={0.45}
                            onPress={() => this.setState({ modalterm: true })}>

                            <Text style={[styles.terms, { marginLeft: 6 }]}> WeNaturalists Terms & Conditions</Text>
                          </TouchableOpacity>
                        </View>

                      </View>

                      {
                        this.state.isLoading ?
                          <View style={[styles.agree, { alignItems: 'center' }]}>
                            <ActivityIndicator color="#fff" size="large" />
                          </View>
                          :
                          <TouchableHighlight underlayColor="#00394D"
                            style={styles.agree}
                            onPress={() => this.signup1stSubmit()}>
                            <View style={{ flexDirection: 'row' }}>
                              {/* <View style={{ width: '20%', height: 54, backgroundColor: '#00394D', borderTopLeftRadius: 26, borderBottomLeftRadius: 26 }}></View> */}
                              <Text style={styles.agreeTxt}>Agree & Continue</Text>
                            </View>
                          </TouchableHighlight>
                      }
                    </View>

                    <View style={{ backgroundColor: '#E8ECEB', paddingBottom: 10 }}>

                      <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')} activeOpacity={0.5} style={{ marginTop: 20, height: 40, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                        <Icon name='SingleUser' size={18} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 11 : 4 }} />
                        <Text style={{ fontFamily:'Montserrat-Medium', color: '#367681', fontSize: 14 }}>Already a member? <Text style={{ fontFamily:'Montserrat-Bold', fontSize: 16 }}>LOGIN</Text></Text>
                        <View></View>
                        <View></View>
                        <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: '#E2E7E9', justifyContent: 'center', alignItems: 'center' }}><Icon name='Arrow-Right' size={15} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 8 : 0 }} /></View>
                      </TouchableOpacity>

                      <TouchableOpacity onPress={() => Linking.openURL('mailto:support@wenaturalists.com')} activeOpacity={0.5} style={{ height: 40, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                        <Icon name='Help' size={18} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 11 : 4 }} />
                        <Text style={{ color: '#367681', fontSize: 14, fontFamily:'Montserrat-Medium' }}>Customer Support               </Text>
                        <View></View>
                        <View></View>
                        <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: '#E2E7E9', justifyContent: 'center', alignItems: 'center' }}><Icon name='Arrow-Right' size={15} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 8 : 0 }} /></View>
                      </TouchableOpacity>

                    </View>


                  </View>
              }

              {/***** SignUp form *****/}

            </View>
            {/* <View style={styles.footer}>

            </View> */}
          </ScrollView>
        </SafeAreaView>
      )
    }

    // {/***** OTP screen *****/}

    else if (this.state.currentScreen === 2) {
      httpService.setupInterceptors();
      return (
        <SafeAreaView style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {/* <MyStatusBar barStyle="light-content" /> */}
            <View style={[styles.modalContent, { height: windowHeight }]}>


              <Header text="Authentication" progress={2} />


              {/* <Text style={styles.phoneNumber}>+91 9876543210</Text> */}


              <View style={{ height: 70, justifyContent: 'center', alignItems: 'center', marginTop: '8%', marginBottom: '-8%' }}>
                <OTPInputView
                  placeholderCharacter=''
                  placeholderTextColor='#91B3A257'
                  style={{ width: 260, color: '#00394D', marginLeft: -12 }}
                  pinCount={6}
                  code={this.state.otp}
                  onCodeChanged={code => this.setState({ otp: code })}
                  autoFocusOnLoad={false}
                  codeInputFieldStyle={styles.borderStyleBase}
                  codeInputHighlightStyle={styles.borderStyleHighLighted}
                  onCodeFilled={(code => {
                    console.log(`Code is ${code}, you are good to go!`)
                  })}
                />
              </View>

              <View style={styles.modalOtpContainer}>
                <Text style={styles.modalOtpText}>Enter the 6 digit OTP sent on your</Text>
                <Text style={styles.modalOtpText}>registered phone</Text>
              </View>

              <View style={{ width: 156, height: 31, backgroundColor: '#E7F3E3', borderRadius: 20, alignSelf: 'center', marginTop: 10, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.phoneNumber}>+{this.state.selectedCountryCode} {this.state.phoneNo}</Text>
              </View>


              <TouchableOpacity activeOpacity={0.5} style={{ marginTop: 30, height: 40, flexDirection: 'row', justifyContent: 'center', paddingHorizontal: 30 }}>
                {/* <Icon name='Exit' size={14} color='#367681' style={{}} /> */}
                <Icon name='EditBox' size={14} color='#367681' style={{}} />
                <Text style={{ color: '#367681', fontSize: 14, marginLeft: 4, fontFamily:'Montserrat-Medium' }}>Edit number</Text>
              </TouchableOpacity>

              {
                this.state.isLoading ?
                  <View style={styles.continueButton}>
                    <ActivityIndicator color="#fff" size="large" />
                  </View>
                  :
                  <TouchableOpacity
                    activeOpacity={.9}
                    style={styles.continueButton}
                    onPress={() => {
                      this.setState({ isLoading: true })

                      let otpBody = {
                        "userId": this.state.userid,
                        "transactionId": this.state.transactionid,
                        "otp": this.state.otp
                      }

                      axios({
                        method: 'post',
                        url: REACT_APP_userServiceURL + '/otp/signup/verify',
                        headers: { 'Content-Type': 'application/json' },
                        data: otpBody,
                        withCredentials: true
                      }).then((response) => {
                        this.setState({ isLoading: false, currentScreen: 3, otp: '' })
                        let res = response.data;
                        if (res.message === 'Success!') {
                          Snackbar.show({
                            backgroundColor: '#97A600',
                            text: "OTP verified successfully",
                            textColor: "#00394D",
                            duration: Snackbar.LENGTH_LONG,
                          })
                        }
                      }).catch((err) => {
                        this.setState({ isLoading: false, otp: '' })
                        if (err && err.response && err.response.data) {
                          Snackbar.show({
                            backgroundColor: '#B22222',
                            text: 'Incorrect OTP',
                            duration: Snackbar.LENGTH_LONG
                          })
                        }
                      });
                    }
                    }>
                    <Text style={styles.continue}>Submit</Text>
                  </TouchableOpacity>
              }

              {this.state.minutes === 0 && this.state.seconds === '0' + 0 ?
                <TouchableOpacity activeOpacity={.5} style={{
                  justifyContent: 'center', backgroundColor: '#D0E8C8', width: 194, height: 38, alignSelf: 'center', marginTop: '5%', borderRadius: 40
                }}>

                  <Text style={styles.resend}>Resend OTP</Text>

                </TouchableOpacity>
                :
                <View style={{
                  justifyContent: 'center', alignItems: 'center', backgroundColor: '#EDEFEF', width: 194, height: 38, alignSelf: 'center', marginTop: '5%', borderRadius: 40, flexDirection: 'row',
                }}>
                  <View style={{ width: this.state.animatingWidth, alignSelf: 'flex-start', height: 38, backgroundColor: '#698F8A', borderTopLeftRadius: 26, borderBottomLeftRadius: 26, position: 'absolute', left: 0, bottom: 0, }}></View>
                  <Text style={styles.resend}>Resend OTP in {this.state.minutes}:{this.state.seconds}</Text>

                </View>
              }


              {/* <TouchableOpacity style={styles.backButton} onPress={() => this.changeState({ modalOpen: false })}>
              <Text style={styles.back}>BACK</Text>
            </TouchableOpacity> */}

            </View>
            {/* <View style={styles.footer}>

            </View> */}
          </ScrollView>
        </SafeAreaView>
      )
    }

    // {/***** OTP screen *****/}

    else if (this.state.currentScreen === 3) {
      return <UserAddress
        changeState={this.changeState}
        firstName={this.state.firstName}
        userId={this.state.userid}
        userType='Individual'
      />
    }

    else if (this.state.currentScreen === 4) {
      
      messaging()
        .subscribeToTopic(REACT_APP_environment + "_push_notifi_" + this.state.userid)
        .then(() => {
          console.log('Subscribed to topic : ', REACT_APP_environment + "_push_notifi_" + this.state.userid)
          // this.props.onClear()
          this.props.navigation.replace("BottomTab");
        })
        .catch(e => console.log("messaging err", e))

      return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size='large' color={COLORS.primarydark} />
      </View>
    }
  }

  render() {
    return this.displayCurrentScreen()
  }
}

export default Signup