import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Platform,
  StatusBarIOS,
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import Svg, { Image, Polygon, Defs, ClipPath } from 'react-native-svg';
import messaging from '@react-native-firebase/messaging';
import axios from 'axios';

import {
  REACT_APP_environment,
  REACT_APP_userServiceURL,
} from '../../../env.json';
import icoMoonConfig from '../../../assets/Icons/selection.json';
import httpService from '../../services/AxiosInterceptors';
import {
  personalProfileRequest,
  personalContactRequest,
} from '../../services/Redux/Actions/User/PersonalProfileActions';
import defaultProfile from '../../../assets/defaultProfile.png';
import defaultCover from '../../../assets/defaultCover.png';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

httpService.setupInterceptors();

class Drawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {},
      coverImage: '',
      userId: '',
      profileimg: '',
      userName: '',
      userType: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        // console.log("userId from Drawer.js : ", value)
        value &&
          this.setState({ userId: value }, () => {
            this.getProfileData();
            this._unsubscribe = this.props.navigation.addListener(
              'focus',
              () => {
                this.getProfileData();
              },
            );
          });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  getProfileData = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get?id=' +
        this.state.userId +
        '&otherUserId=' +
        '',
      cache: true,
      withCredentials: true,
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        this.setState({
          userType: response.data.body.type,
          profileimg: response.data.body.originalProfileImage,
          coverImage: response.data.body.originalCoverImage,
          userName: response.data.body.userName,
        });
      })
      .catch((err) => console.log('Profile data error : ', err));
  };

  render() {
    return (
      <ScrollView
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps="handled"
        showsVerticalScrollIndicator={false}
        style={
          Platform.OS === 'ios'
            ? [{ marginTop: StatusBarIOS._nativeModule.HEIGHT }, styles.container]
            : styles.container
        }>

        <View
          style={styles.imageContainer}
          source={{ uri: this.state.coverImage }}>
          <View
            style={{
              width: '100%',
              height: '20%',
              position: 'absolute',
              bottom: 0,
            }}>
            <LinearGradient
              colors={['#002D3D00', '#00394D']}
              style={styles.linearGradient}></LinearGradient>
          </View>

          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('PrivacySettingStack')
            }
            activeOpacity={0.5}
            style={{
              backgroundColor: '#36768180',
              alignSelf: 'flex-end',
              alignItems: 'center',
              justifyContent: 'center',
              height: 42,
              width: 42,
              borderRadius: 21,
              marginRight: 16,
              marginTop: 12,
            }}>
            <Icon
              name="Setting"
              size={18}
              color="#fff"
              style={{ marginTop: Platform.OS === 'ios' ? 0 : 8 }}
            />
          </TouchableOpacity>
        </View>


        <View style={styles.mainSection}>
          <View style={styles.imageUsername}>
            <View style={styles.header}>
              <View style={styles.manImageContainer}>
                <Svg height="125" width="110" style={styles.svg}>
                  <Defs>
                    <ClipPath id="clip">
                      <Polygon
                        points="47 0, 0 27, 0 81, 68 123, 63 97, 95 81, 95 27"
                        fill="transparent"
                        stroke="white"
                        strokeWidth="3"
                        zIndex={2}
                        position="absolute"
                      />
                    </ClipPath>
                  </Defs>

                  {this.state.profileimg !== '' &&
                    this.state.profileimg !== null ? (
                    <Image
                      width="100%"
                      height="100%"
                      preserveAspectRatio="xMidYMid slice"
                      opacity="1"
                      href={this.state.profileimg}
                      clipPath="url(#clip)"
                      zIndex={1}
                      borderColor="white"
                      borderWidth={3}
                      marginTop={-20}
                    />
                  ) : (
                    <Image
                      width="100%"
                      height="100%"
                      preserveAspectRatio="xMidYMid slice"
                      opacity="1"
                      href={defaultProfile}
                      clipPath="url(#clip)"
                      zIndex={1}
                      borderColor="white"
                      borderWidth={3}
                      marginTop={-20}
                    />
                  )}

                  <Polygon
                    points="47 0, 0 27, 0 81, 68 123, 63 97, 95 81, 95 27"
                    fill="transparent"
                    stroke="white"
                    strokeWidth="3"
                    zIndex={2}
                  />
                </Svg>
              </View>

              <View style={styles.userProfile}>
                {this.state.userName !== '' ? (
                  <Text style={styles.userName} numberOfLines={1}>
                    {this.state.userName}
                    {/* This is a very large username */}
                  </Text>
                ) : (
                  <></>
                )}
                <TouchableOpacity
                  onPress={() => {
                    if (this.state.userType === 'INDIVIDUAL') {
                      this.props.navigation.navigate('ProfileStack');
                    }
                    if (this.state.userType === 'COMPANY') {
                      this.props.navigation.navigate('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                      });
                    }
                  }}>
                  <Text style={styles.viewProfile}>View Profile</Text>
                </TouchableOpacity>
              </View>
            </View>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('ProfileEditStack')
              }>
              <View style={styles.editSection}>
                <Icon
                  name="EditBox"
                  size={14}
                  color="#698F8A"
                  style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                />
                <Text
                  style={
                    Platform.OS === 'ios'
                      ? [styles.editText]
                      : [styles.editText, { marginTop: 3 }]
                  }>
                  Edit
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.causes}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('CircleStack')}
              style={styles.touchable1}>
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={
                    Platform.OS === 'android'
                      ? {
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        backgroundColor: '#D0E8C8',
                        alignItems: 'center',
                      }
                      : {
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        backgroundColor: '#D0E8C8',
                        alignItems: 'center',
                        paddingTop: 8,
                      }
                  }>
                  <Icon name="Circle_Ol" size={20} color="#367681" />
                </View>
                <Text style={styles.whiteText}>Circles</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('CausesStack', {
                  screen: 'RecommendedCauses',
                })
              }
              style={styles.touchable1}>
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={
                    Platform.OS === 'android'
                      ? {
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        backgroundColor: '#367681',
                        alignItems: 'center',
                      }
                      : {
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        backgroundColor: '#367681',
                        alignItems: 'center',
                        paddingTop: 8,
                      }
                  }>
                  <Icon name="Causes_OL" size={20} color="#91B3A2" />
                </View>
                <Text style={styles.whiteText}>Causes</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('ForumStack')}
              style={styles.touchable1}>
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={
                    Platform.OS === 'android'
                      ? {
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        backgroundColor: '#91B3A2',
                        alignItems: 'center',
                      }
                      : {
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        backgroundColor: '#91B3A2',
                        alignItems: 'center',
                        paddingTop: 8,
                      }
                  }>
                  <Icon name="WN_Forum_OL" size={20} color="#154A59" />
                </View>
                <Text style={styles.whiteText}>Forum</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.touchable1}
              onPress={() => this.props.navigation.navigate('PollStack')}>
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={
                    Platform.OS === 'android'
                      ? {
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        backgroundColor: '#698F8A',
                        alignItems: 'center',
                      }
                      : {
                        height: 40,
                        width: 40,
                        borderRadius: 20,
                        backgroundColor: '#698F8A',
                        alignItems: 'center',
                        paddingTop: 8,
                      }
                  }>
                  <Icon name="Polls" size={20} color="#D0E8C8" />
                </View>
                <Text style={styles.whiteText}>Poll</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.border}></View>

          <View
            style={
              Platform.OS === 'ios'
                ? styles.settings
                : [styles.settings, { marginTop: 10 }]
            }>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('DraftStack')}
              style={
                Platform.OS === 'ios'
                  ? [styles.touchable2, { marginBottom: 25 }]
                  : styles.touchable2
              }>
              <View style={styles.row}>
                <Icon
                  name="Draft_F"
                  size={20}
                  color="#698F8A"
                  style={
                    Platform.OS === 'android' ? { marginTop: 10 } : { marginTop: 0 }
                  }
                />
                <Text style={styles.greenText}>Drafts</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('NetworkInvitationStack')
              }
              style={
                Platform.OS === 'ios'
                  ? { marginBottom: 25, width: 200 }
                  : { width: 200 }
              }>
              <View style={styles.row}>
                <Icon
                  name="Mail_OL"
                  size={18}
                  color="#698F8A"
                  style={
                    Platform.OS === 'android' ? { marginTop: 10 } : { marginTop: 0 }
                  }
                />
                <Text style={styles.greenText}>Manage Invitations</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('HiddenContentStack')
              }
              style={
                Platform.OS === 'ios'
                  ? [styles.touchable2, { marginBottom: 25 }]
                  : styles.touchable2
              }>
              <View style={styles.row}>
                <Icon
                  name="Sheild_Lock"
                  size={20}
                  color="#698F8A"
                  style={
                    Platform.OS === 'android' ? { marginTop: 10 } : { marginTop: 0 }
                  }
                />
                <Text style={styles.greenText}>Hidden Content</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('FeedBackStack')}
              style={
                Platform.OS === 'ios'
                  ? [styles.touchable2, { marginBottom: 25 }]
                  : styles.touchable2
              }>
              <View style={styles.row}>
                <Icon
                  name="Star"
                  size={20}
                  color="#698F8A"
                  style={
                    Platform.OS === 'android' ? { marginTop: 10 } : { marginTop: 0 }
                  }
                />
                <Text style={styles.greenText}>FeedBack</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('FaqAndSupportStack')
              }
              style={
                Platform.OS === 'ios'
                  ? { marginBottom: 25, width: 150 }
                  : styles.touchable2
              }>
              <View style={styles.row}>
                <Icon
                  name="Help"
                  size={20}
                  color="#698F8A"
                  style={
                    Platform.OS === 'android' ? { marginTop: 10 } : { marginTop: 0 }
                  }
                />
                <Text style={styles.greenText}>FAQ & Support</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                AsyncStorage.removeItem('userData');
                AsyncStorage.removeItem('userId');
                AsyncStorage.removeItem('refreshToken');

                messaging()
                  .unsubscribeFromTopic(
                    REACT_APP_environment + '_push_notifi_' + this.state.userId,
                  )
                  .then(() => {
                    console.log(
                      'Unsubscribed from topic : ',
                      REACT_APP_environment +
                      '_push_notifi_' +
                      this.state.userId,
                    );
                    // this.props.navigation.replace('Login');
                    this.props.navigation.replace('LoginWebview');
                  })
                  .catch((e) => console.log(e));
              }}
              style={
                Platform.OS === 'ios'
                  ? [styles.touchable2, { marginBottom: 30 }]
                  : styles.touchable2
              }>
              <View style={styles.row}>
                <Icon
                  name="Exit"
                  size={19}
                  color="#698F8A"
                  style={
                    Platform.OS === 'android' ? { marginTop: 10 } : { marginTop: 0 }
                  }
                />
                <Text style={styles.greenText}>Log out</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
  },
  container: {
    backgroundColor: '#00394D',
  },
  manImageContainer: {
    marginTop: -50,
  },
  imageContainer: {
    width: '100%',
    height: screenHeight * 0.17,
  },
  imageUsername: {
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  header: {
    flexDirection: 'row',
    marginBottom: 38,
  },
  editSection: {
    flexDirection: 'row',
    marginLeft: 32,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editText: {
    color: '#91B3A2',
    fontSize: 12,
    fontFamily: 'Montserrat-SemiBold',
    marginLeft: 5,
  },
  userProfile: {
    marginLeft: 2,
    // marginTop: 40
  },
  userName: {
    color: '#FFFFFF',
    fontSize: 20,
    fontFamily: 'Montserrat-SemiBold',
    maxWidth: 140,
    textTransform: 'capitalize'
  },
  viewProfile: {
    fontSize: 12,
    color: '#BFC52E',
    fontFamily: 'Montserrat-Regular',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  touchable1: {
    width: 110,
    marginBottom: 15,
  },
  touchable2: {
    width: 150,
  },
  touchable3: {
    width: 140,
  },
  manImage: {
    height: 110,
    width: 110,
    marginTop: -20,
    borderWidth: 2,
    borderColor: 'white',
    zIndex: 1,
  },
  causes: {
    marginLeft: 49,
    marginBottom: 6,
  },
  border: {
    borderBottomWidth: 0.6,
    borderBottomColor: '#91B3A2',
    width: '84%',
    marginLeft: 30,
  },
  settings: {
    marginLeft: 56,
    marginTop: 30,
  },
  greenText: {
    color: '#367681',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
    marginLeft: 10,
  },
  whiteText: {
    color: '#F7F7F5',
    fontSize: 16,
    fontFamily: 'Montserrat-Medium',
    marginLeft: 10,
    marginTop: 8,
  },
  image: {
    height: screenHeight * 0.25,
    width: '190',
  },
  mainSection: {
    backgroundColor: '#00394D',
    width: '100%',
    borderTopWidth: 0,
  },
});

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,

    userContactProgress: state.personalProfileReducer.userContactProgress,
    userContact: state.personalProfileReducer.userContact,
    errorContact: state.personalProfileReducer.errorContact,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
    personalContactRequest: (data) => dispatch(personalContactRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Drawer);
