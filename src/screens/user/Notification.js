import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Image,
  SafeAreaView,
  StyleSheet,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import {cloneDeep} from 'lodash';

import {REACT_APP_userServiceURL} from '../../../env.json';
import defaultShape from '../../Components/Shared/Shape';
import typography from '../../Components/Shared/Typography';
import {COLORS} from '../../Components/Shared/Colors';
import {notificationRequest} from '../../services/Redux/Actions/User/NotificationActions';
import icoMoonConfig from '../../../assets/Icons/selection.json';
import defaultProfile from '../../../assets/defaultProfile.png';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: [],
      pageNo: 0,
      selected: 'Show All',
      optionsModalOpen: false,
      userId: '',

      notificationCategory: 'ALL',
      notiData: [],
      totalNotiCount: 0,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({userId: value}, () => this.getNotification());
      })
      .catch((e) => {
        console.log(e);
      });
  }

  getNotification = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/notification/list?userId=' +
        this.state.userId +
        '&notificationCategory=' +
        this.state.notificationCategory +
        '&page=' +
        this.state.pageNo +
        '&size=10',
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          notiData: this.state.notiData.concat(response.data.body.content),
          totalNotiCount: response.data.body.page.totalElements,
        });
      })
      .catch((err) => console.log('get noti err', err));
  };

  handleLoadMore = () => {
    this.setState({pageNo: this.state.pageNo + 1}, () =>
      this.getNotification(),
    );
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.user !== this.props.user) {
      return true;
    }

    if (nextState.notifications !== this.state.notifications) {
      return true;
    }
    if (nextState.pageNo !== this.state.pageNo) {
      return true;
    }
    if (nextState.selected !== this.state.selected) {
      return true;
    }
    if (nextState.optionsModalOpen !== this.state.optionsModalOpen) {
      return true;
    }
    if (nextState.userId !== this.state.userId) {
      return true;
    }
    if (nextState.notiData !== this.state.notiData) {
      return true;
    }
    if (nextState.notificationCategory !== this.state.notificationCategory) {
      return true;
    }

    return false;
  }

  handleNavigation = (item, type) => {
    if (type === 'FORUM') {
      this.props.navigation.navigate('ForumStack', {
        screen: 'ForumDetails',
        params: {
          slug: item.params.title.split('/').pop(),
          userId: this.state.userId,
        },
      });
    } else if (type === 'POLL') {
      this.props.navigation.navigate('PollStack', {
        screen: 'PollDetails',
        params: {
          slug: item.params.title.split('/').pop(),
          userId: this.state.userId,
        },
      });
    } else if (type === 'POST') {
      this.props.navigation.navigate('IndividualFeedsPost', {
        id: item.params?.postId || item.params.entityId,
      });
    } else if (type === 'PROJECT') {
      item.userEvent === 'MAXTHRESHOLDREPORTEDABUSE'
        ? null
        : item.params.entityType === 'POST'
        ? this.props.navigation.navigate('IndividualFeedsPost', {
            id: item.params?.postId || item.params.entityId,
          })
        : this.props.navigation.navigate('ProjectDetailView', {
            slug: item.params.slug,
          });
    } else if (type === 'CIRCLE') {
      this.props.navigation.navigate('CircleProfileStack', {
        screen: 'CircleProfile',
        params: {slug: item.params.slug},
      });
    } else if (type === 'NETWORK') {
      item.userEvent === 'CONNECT_INVITE_ACCEPTED'
        ? this.props.navigation.navigate('ProfileStack', {
            screen: 'OtherProfileScreen',
            params: {userId: item.params.otherUserId},
          })
        : this.props.navigation.navigate('NetworkInvitationStack', {});
    } else {
    }
  };

  renderItem = (item) => {
    return (
      <TouchableOpacity
        onPress={() => this.markNotificationAsRead(item, item.type)}
        style={[
          styles.unreadNotiItem,
          {
            backgroundColor: item.read
              ? COLORS.white
              : COLORS.altgreen_t20_blur,
          },
        ]}>
        <View style={{width: '80%', padding: 12}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              style={[defaultShape.Media_Round, {marginRight: 10}]}
              source={
                item.params.imageUrl
                  ? {uri: item.params.imageUrl}
                  : defaultProfile
              }
            />
            <Text style={[typography.Body_1, {color: COLORS.grey_500}]}>
              {item.message}
            </Text>
          </View>
          <Text
            style={[
              typography.Note,
              {color: COLORS.altgreen_300, marginLeft: 42},
            ]}>
            {item.time}
          </Text>
        </View>

        <TouchableOpacity
          activeOpacity={0.5}
          style={defaultShape.Nav_Gylph_Btn}
          onPress={() => this.deleteNotificationAlert(item.id)}>
          <Icon name="TrashBin" color={COLORS.altgreen_300} size={15} />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  deleteNotificationAlert = (id) => {
    this.setState({optionsModalOpen: false});
    Alert.alert('', 'Are you sure you want to delete this notification?', [
      {
        text: 'YES',
        onPress: () => this.deleteNotification(id),
        style: 'cancel',
      },
      {
        text: 'NO',
      },
    ]);
  };

  deleteNotification = (id) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/notification/delete?notificationIds=' +
        id,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          let tempNotiData = cloneDeep(this.state.notiData);
          let tempindex = tempNotiData.findIndex((x) => x.id === id);
          tempNotiData.splice(tempindex, 1);
          this.setState({
            notiData: tempNotiData,
            totalNotiCount: this.state.totalNotiCount - 1,
          });
        }
      })
      .catch((err) => {
        console.log(err.response.data.message);
      });
  };

  markNotificationAsRead = (item, type) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/notification/mark-as-read?notificationId=' +
        item.id,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let tempNotiData = cloneDeep(this.state.notiData);
        let tempindex = tempNotiData.findIndex((x) => x.id === item.id);
        tempNotiData[tempindex].read = true;
        this.setState(
          {
            notiData: tempNotiData,
            totalNotiCount: this.state.totalNotiCount - 1,
          },
          () => this.handleNavigation(item, type),
        );
      })
      .catch((err) => {
        console.log(err.response.data.message);
      });
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {/* ***** Header Starts ***** */}

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 64,
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_300} />
          </TouchableOpacity>

          <View style={defaultShape.Nav_Type2}>
            <Text style={[typography.Title_1, {color: COLORS.dark_800}]}>
              Notifications
            </Text>
            <Text style={[typography.Note, {color: COLORS.altgreen_300}]}>
              {this.state.totalNotiCount} new Notifications
            </Text>
          </View>

          <View></View>
        </View>

        {/* ***** Header ends ***** */}

        {/* ***** Filter Starts ***** */}

        <View
          style={{
            height: 56,
            flexDirection: 'row',
            backgroundColor: COLORS.dark_900,
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <TouchableOpacity
              onPress={() =>
                this.setState(
                  {
                    pageNo: 0,
                    notiData: [],
                    notificationCategory: 'ALL',
                    selected: 'Show All',
                  },
                  () => this.getNotification(),
                )
              }
              style={[
                this.state.selected === 'Show All'
                  ? defaultShape.ContextBtn_FL_Drk
                  : defaultShape.ContextBtn_OL_Drk,
                {marginHorizontal: 3},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_300}]}>
                Show All
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.setState(
                  {
                    pageNo: 0,
                    notiData: [],
                    notificationCategory: 'POST',
                    selected: 'Post',
                  },
                  () => this.getNotification(),
                )
              }
              style={[
                this.state.selected === 'Post'
                  ? defaultShape.ContextBtn_FL_Drk
                  : defaultShape.ContextBtn_OL_Drk,
                {marginHorizontal: 3},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_300}]}>
                Post
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.setState(
                  {
                    pageNo: 0,
                    notiData: [],
                    notificationCategory: 'PROJECT',
                    selected: 'Projects',
                  },
                  () => this.getNotification(),
                )
              }
              style={[
                this.state.selected === 'Projects'
                  ? defaultShape.ContextBtn_FL_Drk
                  : defaultShape.ContextBtn_OL_Drk,
                {marginHorizontal: 3},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_300}]}>
                Projects
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.setState(
                  {
                    pageNo: 0,
                    notiData: [],
                    notificationCategory: 'CIRCLE',
                    selected: 'Circle',
                  },
                  () => this.getNotification(),
                )
              }
              style={[
                this.state.selected === 'Circle'
                  ? defaultShape.ContextBtn_FL_Drk
                  : defaultShape.ContextBtn_OL_Drk,
                {marginHorizontal: 3},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_300}]}>
                Circle
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.setState(
                  {
                    pageNo: 0,
                    notiData: [],
                    notificationCategory: 'FORUM',
                    selected: 'Forum',
                  },
                  () => this.getNotification(),
                )
              }
              style={[
                this.state.selected === 'Forum'
                  ? defaultShape.ContextBtn_FL_Drk
                  : defaultShape.ContextBtn_OL_Drk,
                {marginHorizontal: 3},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_300}]}>
                Forum
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.setState(
                  {
                    pageNo: 0,
                    notiData: [],
                    notificationCategory: 'POLL',
                    selected: 'Poll',
                  },
                  () => this.getNotification(),
                )
              }
              style={[
                this.state.selected === 'Poll'
                  ? defaultShape.ContextBtn_FL_Drk
                  : defaultShape.ContextBtn_OL_Drk,
                {marginHorizontal: 3},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_300}]}>
                Poll
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.setState(
                  {
                    pageNo: 0,
                    notiData: [],
                    notificationCategory: 'NETWORK',
                    selected: 'Network',
                  },
                  () => this.getNotification(),
                )
              }
              style={[
                this.state.selected === 'Network'
                  ? defaultShape.ContextBtn_FL_Drk
                  : defaultShape.ContextBtn_OL_Drk,
                {marginHorizontal: 3},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_300}]}>
                Network
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.setState(
                  {
                    pageNo: 0,
                    notiData: [],
                    notificationCategory: 'OTHER',
                    selected: 'Others',
                  },
                  () => this.getNotification(),
                )
              }
              style={[
                this.state.selected === 'Others'
                  ? defaultShape.ContextBtn_FL_Drk
                  : defaultShape.ContextBtn_OL_Drk,
                {marginHorizontal: 3},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_300}]}>
                Others
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>

        {/* ***** Filter ends ***** */}

        <FlatList
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.id}
          data={this.state.notiData}
          renderItem={({item}) => this.renderItem(item)}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={2}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  unreadNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.altgreen_t20_blur,
    borderBottomColor: COLORS.grey_400,
    borderBottomWidth: 0.3,
  },
  readNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderBottomColor: COLORS.grey_400,
    borderBottomWidth: 0.3,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
});

const mapStateToProps = (state) => {
  return {
    notificationProgress: state.notificationReducer.notificationProgress,
    user: state.notificationReducer.user,
    error: state.notificationReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    notificationRequest: (data) => dispatch(notificationRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Notification);
