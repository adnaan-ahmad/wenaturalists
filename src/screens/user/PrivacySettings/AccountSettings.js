import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Modal,
  KeyboardAvoidingView,
  Image,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import {TextInput} from 'react-native-paper';
import Snackbar from 'react-native-snackbar';
import messaging from '@react-native-firebase/messaging';
import OTPInputView from '@twotalltotems/react-native-otp-input';

import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {COLORS} from '../../../Components/Shared/Colors';
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader';
import {
  REACT_APP_userServiceURL,
  REACT_APP_environment,
} from '../../../../env.json';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');

export default class AccountSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      lastUpdatedPassword: 0,
      changePassModalOpen: false,
      displayNameModal: false,
      deleteModalOpen: false,
      reasonModal: false,
      chooseOptionsModal: false,
      currentPassVisible: false,
      currentPass: '',
      newPass: '',
      confirmPass: '',
      description: '',
      deletedUser: {},
      emailOtpModal: {},
      config: {},
      seconds: 59,
      minutes: 9,
      userRelatedData: {},
      showProfileImage: null,
      showusername: null,
      previousActivityOriginal: true,
    };
  }

  goback = () => this.props.navigation.goBack();

  componentDidMount() {
    AsyncStorage.getItem('refreshToken')
      .then((value) => {
        if (value === null) {
          this.props.navigation.replace('Login');
        }
      })
      .catch((err) => console.log(err));

    AsyncStorage.getItem('userId')
      .then((value) => {
        value &&
          this.setState({userId: value}, () => {
            this.getUserDataByUserId();
            this.getCurrentDeactivationConfig();
          });
        value && this.lastUpdatedPass(value);
      })
      .catch((err) => console.log(err));

    this.interval = setInterval(
      () => this.setState((prevState) => ({seconds: prevState.seconds - 1})),
      1000,
    );
  }

  componentDidUpdate() {
    if (this.state.minutes === 0 && this.state.seconds === 0) {
      clearInterval(this.interval);
    }
    if (this.state.seconds === -1) {
      this.setState({minutes: this.state.minutes - 1, seconds: 59});
    }
    if (this.state.seconds.toString().length === 1) {
      this.setState({seconds: '0' + this.state.seconds});
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  lastUpdatedPass = (userId) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/user/passwordDetails/' + userId,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          lastUpdatedPassword: response.data.body.lastUpdateTimestamp,
        });
      })
      .catch((err) => console.log(err));
  };

  updatePassword = () => {
    let postBody = {
      userId: this.state.userId,
      oldPassword: this.state.currentPass,
      newPassword: this.state.newPass,
      confirmPassword: this.state.confirmPass,
    };
    if (this.state.newPass !== this.state.confirmPass) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: "Password doesn't match",
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
    } else {
      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/user/update/password',
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          Snackbar.show({
            backgroundColor: COLORS.dark_900,
            text: 'Password updated Successfully',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          AsyncStorage.removeItem('userData');
          AsyncStorage.removeItem('userId');
          AsyncStorage.removeItem('refreshToken');

          messaging()
            .unsubscribeFromTopic(
              REACT_APP_environment + '_push_notifi_' + this.state.userId,
            )
            .then(() => {
              console.log(
                'Unsubscribed from topic : ',
                REACT_APP_environment + '_push_notifi_' + this.state.userId,
              );
              this.props.navigation.replace('Login');
            })
            .catch((e) => console.log(e));
        })
        .catch((err) => {
          if (err.response.status === 400) {
            Snackbar.show({
              backgroundColor: COLORS.alert_red,
              text: 'You cannot reuse last 3 passwords',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          } else {
            Snackbar.show({
              backgroundColor: COLORS.alert_red,
              text: err.response.message,
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    }
  };

  changePassModal = () => {
    return (
      <Modal
        visible={this.state.changePassModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{flex: 1, width: '100%'}}>
          <View style={{marginTop: 'auto'}}>
            <View
              style={{
                width: '100%',
                height: 560,
                position: 'absolute',
                bottom: 0,
                alignSelf: 'center',
              }}>
              <LinearGradient
                colors={['#154A5900', '#154A59CC', '#154A59']}
                style={{
                  flex: 1,
                  paddingLeft: 15,
                  paddingRight: 15,
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6,
                }}></LinearGradient>
            </View>

            <TouchableOpacity
              activeOpacity={0.7}
              style={defaultShape.CloseBtn}
              onPress={() => this.setState({changePassModalOpen: false})}>
              <Icon
                name="Cross"
                size={13}
                color="#367681"
                style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
              />
            </TouchableOpacity>

            <View
              style={[
                defaultShape.Modal_Categories_Container,
                {
                  height: 320,
                  backgroundColor: COLORS.bgfill,
                  paddingBottom: 10,
                  paddingTop: 0,
                  alignItems: 'flex-start',
                },
              ]}>
              <View
                style={{
                  width: '100%',
                  height: 58,
                  backgroundColor: COLORS.altgreen_100,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                }}>
                <Text
                  style={[
                    typography.Button_2,
                    {color: COLORS.dark_900, textAlign: 'center'},
                  ]}>
                  CHANGE PASSWORD
                </Text>
              </View>

              <TextInput
                theme={{
                  colors: {
                    text: COLORS.dark_700,
                    primary: COLORS.altgreen_300,
                    placeholder: COLORS.altgreen_300,
                  },
                }}
                label="Current Password"
                selectionColor="#C8DB6E"
                secureTextEntry={this.state.currentPassVisible ? false : true}
                right={
                  <TextInput.Icon
                    onPress={() =>
                      this.setState({
                        currentPassVisible: !this.state.currentPassVisible,
                      })
                    }
                    name={this.state.currentPassVisible ? 'eye' : 'eye-off'}
                    size={18}
                    color={COLORS.altgreen_300}
                  />
                }
                style={[
                  typography.Subtitle_1,
                  {
                    width: '90%',
                    backgroundColor: COLORS.bgfill,
                    alignSelf: 'center',
                    color: COLORS.dark_700,
                    borderRadius: 8,
                    height: 52,
                  },
                ]}
                onChangeText={(value) => this.setState({currentPass: value})}
                value={this.state.currentPass}
              />

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: '90%',
                  alignSelf: 'center',
                  alignItems: 'center',
                  marginTop: 20,
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label="New Password"
                  selectionColor="#C8DB6E"
                  secureTextEntry
                  style={[
                    typography.Subtitle_1,
                    {
                      width: '45%',
                      backgroundColor: COLORS.bgfill,
                      alignSelf: 'center',
                      color: COLORS.dark_700,
                      borderRadius: 8,
                      height: 52,
                    },
                  ]}
                  onChangeText={(value) => this.setState({newPass: value})}
                  value={this.state.newPass}
                />

                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label="Confirm Password"
                  selectionColor="#C8DB6E"
                  secureTextEntry
                  style={[
                    typography.Subtitle_1,
                    {
                      width: '45%',
                      backgroundColor: COLORS.bgfill,
                      alignSelf: 'center',
                      color: COLORS.dark_700,
                      borderRadius: 8,
                      height: 52,
                    },
                  ]}
                  onChangeText={(value) => this.setState({confirmPass: value})}
                  value={this.state.confirmPass}
                />
              </View>

              {this.state.currentPass === '' ||
              this.state.newPass === '' ||
              this.state.confirmPass === '' ? (
                <View
                  style={[
                    styles.updateBtn,
                    {backgroundColor: COLORS.grey_350},
                  ]}>
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.altgreen_100},
                    ]}>
                    UPDATE
                  </Text>
                </View>
              ) : (
                <TouchableOpacity
                  onPress={this.updatePassword}
                  style={styles.updateBtn}>
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.altgreen_100},
                    ]}>
                    UPDATE
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  };

  handleDeleteSubmit = () => {
    axios({
      method: 'GET',
      url:
        REACT_APP_userServiceURL +
        `/deactivation/validate?userId=${this.state.userId}`,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            deletedUser: response.data.body,
            emailOtpModal: true,
          });
        }
      })
      .catch((error) => {});
  };

  reasonModal = () => {
    return (
      <Modal
        visible={this.state.deleteModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{flex: 1, width: '100%'}}>
          <View style={{marginTop: 'auto'}}>
            <View
              style={{
                width: '100%',
                height: 560,
                position: 'absolute',
                bottom: 0,
                alignSelf: 'center',
              }}>
              <LinearGradient
                colors={['#154A5900', '#154A59CC', '#154A59']}
                style={{
                  flex: 1,
                  paddingLeft: 15,
                  paddingRight: 15,
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6,
                }}></LinearGradient>
            </View>
            <TouchableOpacity
              activeOpacity={0.7}
              style={defaultShape.CloseBtn}
              onPress={() => this.setState({deleteModalOpen: false})}>
              <Icon
                name="Cross"
                size={13}
                color="#367681"
                style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
              />
            </TouchableOpacity>
            <View
              style={[
                defaultShape.Modal_Categories_Container,
                {
                  height: 400,
                  backgroundColor: COLORS.bgfill,
                  paddingBottom: 10,
                  paddingTop: 0,
                  alignItems: 'flex-start',
                },
              ]}>
              <View
                style={{
                  width: '100%',
                  height: 58,
                  backgroundColor: COLORS.altgreen_100,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                }}>
                <Text
                  style={[
                    typography.Button_2,
                    {color: COLORS.dark_900, textAlign: 'center'},
                  ]}>
                  Delete your Profile
                </Text>
              </View>
              <View style={{paddingHorizontal: 15, marginTop: 10}}>
                <Text
                  style={[
                    typography.Note2,
                    {
                      color: COLORS.grey_400,
                      marginTop: 1,
                      marginLeft: 5,
                      marginRight: 10,
                    },
                  ]}>
                  We are sorry to see you go. Please choose your reason for
                  deleting your profile.
                </Text>
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {
                            borderBottomWidth: 0,
                            paddingVertical: 10,
                            marginTop: 10,
                          },
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0, width: '100%', marginTop: 10},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({
                      reasonForDeleting: 'Duplicate Account',
                    });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        borderColor: COLORS.altgreen_300,
                        borderWidth: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: COLORS.altgreen_200,
                      }}>
                      {this.state.reasonForDeleting === 'Duplicate Account' ? (
                        <Icon
                          name="Bullet_Fill"
                          size={17}
                          color={COLORS.dark_800}
                          style={{
                            marginLeft: -2,
                            marginTop: Platform.OS === 'android' ? 8 : 0,
                          }}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                    <Text
                      style={[
                        typography.Button_Lead,
                        {color: COLORS.dark_600, marginLeft: 8},
                      ]}>
                      Duplicate Account
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0, paddingVertical: 10},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0, width: '100%'},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({
                      reasonForDeleting: 'Account created by mistake',
                    });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        borderColor: COLORS.altgreen_300,
                        borderWidth: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: COLORS.altgreen_200,
                      }}>
                      {this.state.reasonForDeleting ===
                      'Account created by mistake' ? (
                        <Icon
                          name="Bullet_Fill"
                          size={17}
                          color={COLORS.dark_800}
                          style={{
                            marginLeft: -2,
                            marginTop: Platform.OS === 'android' ? 8 : 0,
                          }}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                    <Text
                      style={[
                        typography.Button_Lead,
                        {color: COLORS.dark_600, marginLeft: 8},
                      ]}>
                      Account created by mistake
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0, paddingVertical: 10},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0, width: '100%'},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({
                      reasonForDeleting: 'No longer interested',
                    });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        borderColor: COLORS.altgreen_300,
                        borderWidth: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: COLORS.altgreen_200,
                      }}>
                      {this.state.reasonForDeleting ===
                      'No longer interested' ? (
                        <Icon
                          name="Bullet_Fill"
                          size={17}
                          color={COLORS.dark_800}
                          style={{
                            marginLeft: -2,
                            marginTop: Platform.OS === 'android' ? 8 : 0,
                          }}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                    <Text
                      style={[
                        typography.Button_Lead,
                        {color: COLORS.dark_600, marginLeft: 8},
                      ]}>
                      No longer interested
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0, paddingVertical: 10},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0, width: '100%'},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({
                      reasonForDeleting: "I don't see value in being a member",
                    });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        borderColor: COLORS.altgreen_300,
                        borderWidth: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: COLORS.altgreen_200,
                      }}>
                      {this.state.reasonForDeleting ===
                      "I don't see value in being a member" ? (
                        <Icon
                          name="Bullet_Fill"
                          size={17}
                          color={COLORS.dark_800}
                          style={{
                            marginLeft: -2,
                            marginTop: Platform.OS === 'android' ? 8 : 0,
                          }}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                    <Text
                      style={[
                        typography.Button_Lead,
                        {color: COLORS.dark_600, marginLeft: 8},
                      ]}>
                      I don't see value in being a member
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0, paddingVertical: 10},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0, width: '100%'},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({
                      reasonForDeleting: 'I am getting spammed by emails',
                    });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        borderColor: COLORS.altgreen_300,
                        borderWidth: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: COLORS.altgreen_200,
                      }}>
                      {this.state.reasonForDeleting ===
                      'I am getting spammed by emails' ? (
                        <Icon
                          name="Bullet_Fill"
                          size={17}
                          color={COLORS.dark_800}
                          style={{
                            marginLeft: -2,
                            marginTop: Platform.OS === 'android' ? 8 : 0,
                          }}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                    <Text
                      style={[
                        typography.Button_Lead,
                        {color: COLORS.dark_600, marginLeft: 8},
                      ]}>
                      I am getting spammed by emails
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0, paddingVertical: 10},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0, width: '100%'},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({
                      reasonForDeleting: 'Others',
                    });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        borderColor: COLORS.altgreen_300,
                        borderWidth: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: COLORS.altgreen_200,
                      }}>
                      {this.state.reasonForDeleting === 'Others' ? (
                        <Icon
                          name="Bullet_Fill"
                          size={17}
                          color={COLORS.dark_800}
                          style={{
                            marginLeft: -2,
                            marginTop: Platform.OS === 'android' ? 8 : 0,
                          }}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                    <Text
                      style={[
                        typography.Button_Lead,
                        {color: COLORS.dark_600, marginLeft: 8},
                      ]}>
                      Others (Please specify)
                    </Text>
                  </View>
                </TouchableOpacity>
                {this.state.reasonForDeleting === 'Others' && (
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      paddingLeft: 6,
                      paddingRight: 12,
                    }}>
                    <TextInput
                      theme={{
                        colors: {
                          text: COLORS.dark_700,
                          primary: COLORS.altgreen_300,
                          placeholder: COLORS.altgreen_300,
                        },
                      }}
                      label="Write the details"
                      multiline
                      selectionColor="#C8DB6E"
                      style={[
                        defaultStyle.Subtitle_1,
                        {
                          width: '90%',
                          height: 56,
                          backgroundColor: COLORS.white,
                          color: COLORS.dark_700,
                        },
                      ]}
                      onChangeText={(value) =>
                        this.setState({description: value})
                      }
                      value={this.state.description}
                    />
                  </View>
                )}
                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => {
                    this.state.reasonForDeleting === ''
                      ? Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Please select an option',
                          duration: Snackbar.LENGTH_LONG,
                        })
                      : this.state.reasonForDeleting === 'OTHERS' &&
                        this.state.description.trim() === ''
                      ? Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Please enter the detail',
                          duration: Snackbar.LENGTH_LONG,
                        })
                      : (this.handleDeleteSubmit(),
                        this.setState({
                          deleteModalOpen: false,
                        }));
                  }}
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginRight: 15,
                    height: 27,
                    width: '80%',
                    alignSelf: 'center',
                    marginVertical: 10,
                    borderRadius: 16,
                    textAlign: 'center',
                    borderWidth: 1,
                    borderColor: '#698F8A',
                  }}>
                  <Text
                    style={{
                      color: '#698F8A',
                      fontSize: 14,
                      paddingHorizontal: 14,
                      paddingVertical: 20,
                      fontFamily: 'Montserrat-Medium',
                      fontWeight: 'bold',
                      marginBottom: Platform.OS === 'ios' ? 15 : 0,
                    }}>
                    Submit
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  };

  emailOtpModal = () => {
    return (
      <Modal
        visible={this.state.emailOtpModal}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            alignSelf: 'center',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {height: 700, bottom: 0},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.white,
              paddingTop: 15,
              width: '80%',
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {
                  color: COLORS.dark_600,
                  alignSelf: 'center',
                  textAlign: 'center',
                },
              ]}>
              Enter the 6 digit OTP sent on {`\n`} your email
            </Text>

            <View
              style={{
                paddingHorizontal: 16,
                height: 27,
                backgroundColor: COLORS.grey_100,
                borderRadius: 20,
                alignSelf: 'center',
                marginTop: 10,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={[defaultStyle.Caption, {color: COLORS.dark_500}]}>
                {this.state.deletedUser.email}
              </Text>
            </View>

            <View
              style={{
                height: 70,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '3%',
              }}>
              <OTPInputView
                placeholderCharacter=""
                placeholderTextColor="#91B3A257"
                style={{width: 260, color: '#00394D', marginLeft: -12}}
                pinCount={6}
                code={this.state.otp}
                onCodeChanged={(code) => this.setState({otp: code})}
                autoFocusOnLoad={false}
                codeInputFieldStyle={styles.borderStyleBase}
                codeInputHighlightStyle={styles.borderStyleHighLighted}
                onCodeFilled={(code) => {
                  console.log(`Code is ${code}, you are good to go!`);
                }}
              />
            </View>

            {this.state.minutes === 0 && this.state.seconds === '0' + 0 ? (
              <TouchableOpacity
                activeOpacity={0.5}
                style={{
                  justifyContent: 'center',
                  backgroundColor: COLORS.altgreen_100,
                  width: 118,
                  height: 28,
                  alignSelf: 'center',
                  marginTop: '5%',
                  borderRadius: 40,
                  alignItems: 'center',
                }}>
                <Text style={[defaultStyle.Caption, {color: COLORS.dark_500}]}>
                  Resend OTP
                </Text>
              </TouchableOpacity>
            ) : (
              <Text style={styles.resend}>
                OTP will expire in {this.state.minutes}:{this.state.seconds}
              </Text>
            )}

            <View
              style={{
                width: '100%',
                height: 52,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignItems: 'center',
                borderTopWidth: 1,
                borderTopColor: COLORS.grey_300,
                marginTop: 20,
              }}>
              <Text
                onPress={() =>
                  this.setState({
                    emailOtpModal: false,
                    emailAddress: this.state.deletedUser.email,
                    seconds: 59,
                    minutes: 9,
                    otp: '',
                  })
                }
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Cancel
              </Text>
              <View
                style={{
                  height: '100%',
                  width: 1,
                  backgroundColor: COLORS.grey_300,
                }}></View>
              <Text
                onPress={this.handleOtpSubmit}
                style={[defaultStyle.Button_2, {color: COLORS.dark_800}]}>
                DONE
              </Text>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  clearOtp = () => {
    this.setState({otp: ''});
  };

  setUserPreferances = (flag) => {
    let showProfileImage = null;
    let showusername = null;
    if (flag === true) {
      if (this.state.userRelatedData.profileImage) {
        showProfileImage = this.getOptimizedImage(this.state.userRelatedData);
      }
      showusername = this.state.userRelatedData.userName;
    } else {
      showProfileImage = 'https://cdn.dscovr.com/images/welogo-notific.png';
      showusername = 'WeNaturalists member';
    }
    this.setState({
      previousActivityOriginal: flag,
      showProfileImage: showProfileImage,
      showusername: showusername,
      displayNameModal: true,
      chooseOptionsModal: false,
    });
  };

  chooseNameModal = () => {
    return (
      <Modal
        visible={this.state.displayNameModal}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            alignSelf: 'center',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {height: 700, bottom: 0},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.white,
              paddingTop: 15,
              width: '80%',
            }}>
            <Text
              style={[
                typography.Note2,
                {
                  color: COLORS.grey_400,
                  textAlign: 'center',
                  paddingHorizontal: 10,
                  paddingVertical: 15,
                },
              ]}>
              On deleting your profile, other members may continue to have
              access to your past activities on WeNaturalists. But the members
              will not be able to view your profile. You can choose the name
              under which you wish to display your activities
            </Text>
            <Text
              style={[
                defaultStyle.Title_2,
                {
                  color: COLORS.dark_600,
                  alignSelf: 'center',
                  textAlign: 'center',
                },
              ]}>
              Please choose a display name
            </Text>

            <TouchableOpacity
              activeOpacity={0.7}
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                alignItems: 'center',
                backgroundColor: COLORS.grey_100,
                borderRadius: 20,
                paddingRight: 15,
                marginTop: 10,
              }}
              onPress={() =>
                this.setState({
                  displayNameModal: false,
                  chooseOptionsModal: true,
                })
              }>
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={{uri: this.state.showProfileImage}}
              />
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Title_2,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 8,
                  },
                ]}>
                {this.state.showusername}
              </Text>
            </TouchableOpacity>

            <View
              style={{
                width: '100%',
                height: 52,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignItems: 'center',
                borderTopWidth: 1,
                borderTopColor: COLORS.grey_300,
                marginTop: 20,
              }}>
              <Text
                onPress={() =>
                  this.setState({
                    displayNameModal: false,
                  })
                }
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Cancel
              </Text>
              <View
                style={{
                  height: '100%',
                  width: 1,
                  backgroundColor: COLORS.grey_300,
                }}></View>
              <Text
                onPress={this.handleUsernameSubmit}
                style={[defaultStyle.Button_2, {color: COLORS.dark_800}]}>
                DONE
              </Text>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  chooseOptionsModalView = () => {
    return (
      <Modal
        visible={this.state.chooseOptionsModal}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{flex: 1, width: '100%'}}>
          <View style={{marginTop: 'auto'}}>
            <View
              style={{
                width: '100%',
                height: 560,
                position: 'absolute',
                bottom: 0,
                alignSelf: 'center',
              }}>
              <LinearGradient
                colors={['#154A5900', '#154A59CC', '#154A59']}
                style={{
                  flex: 1,
                  paddingLeft: 15,
                  paddingRight: 15,
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6,
                }}></LinearGradient>
            </View>
            <TouchableOpacity
              activeOpacity={0.7}
              style={defaultShape.CloseBtn}
              onPress={() => this.setState({chooseOptionsModal: false})}>
              <Icon
                name="Cross"
                size={13}
                color="#367681"
                style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
              />
            </TouchableOpacity>
            <View
              style={[
                defaultShape.Modal_Categories_Container,
                {
                  height: 200,
                  backgroundColor: COLORS.bgfill,
                  paddingBottom: 10,
                  paddingTop: 0,
                  alignItems: 'flex-start',
                },
              ]}>
              <View
                style={{
                  width: '100%',
                  height: 58,
                  backgroundColor: COLORS.altgreen_100,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                }}>
                <Text
                  style={[
                    typography.Button_2,
                    {color: COLORS.dark_900, textAlign: 'center'},
                  ]}>
                  Please choose display name
                </Text>
              </View>
              {!this.state.previousActivityOriginal && (
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={{
                    flexDirection: 'row',
                    borderRadius: 20,
                    paddingHorizontal: 15,
                    paddingVertical: 15,
                    marginTop: 10,
                  }}
                  onPress={() => this.setUserPreferances(true)}>
                  <Image
                    style={[defaultShape.Media_Round, {}]}
                    source={{uri: this.state.userRelatedData?.profileImage}}
                  />
                  <Text
                    numberOfLines={1}
                    style={[
                      defaultStyle.Title_2,
                      {
                        color: COLORS.dark_800,
                        marginLeft: 8,
                        textAlign: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                      },
                    ]}>
                    {this.state.userRelatedData?.userName}
                  </Text>
                </TouchableOpacity>
              )}
              {this.state.previousActivityOriginal && (
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={{
                    flexDirection: 'row',
                    borderRadius: 20,
                    paddingHorizontal: 15,
                    paddingVertical: 5,
                  }}
                  onPress={() => this.setUserPreferances(false)}>
                  <Image
                    style={[defaultShape.Media_Round, {}]}
                    source={{
                      uri: 'https://cdn.dscovr.com/images/welogo-notific.webp',
                    }}
                  />
                  <Text
                    numberOfLines={1}
                    style={[
                      defaultStyle.Title_2,
                      {
                        color: COLORS.dark_800,
                        marginLeft: 8,
                        textAlign: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                      },
                    ]}>
                    WeNaturalists member
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  };

  handleOtpSubmit = () => {
    let postBody = {
      userId: this.state.userId,
      transactionId: this.state.deletedUser.transactionId,
      otp: this.state.otp,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/otp/verify/',
      headers: {'Content-Type': 'application/json'},
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        if (response.status === 202) {
          this.setState({
            displayNameModal: true,
            emailOtpModal: false,
          });
        }
      })
      .catch((error) => {
        if (err && err.response.data) {
          Snackbar.show({
            backgroundColor: COLORS.alert_red,
            text: err.response.data.message,
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
  };

  getOptimizedImage(personalInfo) {
    if (
      personalInfo.resizedProfileImages &&
      personalInfo.resizedProfileImages['200x200'] != null
    ) {
      return personalInfo.resizedProfileImages['200x200'];
    }
    return personalInfo.profileImage;
  }

  getUserDataByUserId() {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/profile/get?id=' + this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({userRelatedData: response.data.body}, () => {
            let profileImage = null;
            if (this.state.userRelatedData.profileImage) {
              profileImage = this.getOptimizedImage(this.state.userRelatedData);
            }
            this.setState({
              showProfileImage: profileImage,
              showusername: this.state.userRelatedData.userName,
            });
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  handleUsernameSubmit = () => {
    this.getDeactivationConfig();
  };

  getDeactivationConfig = () => {
    this.setState({isSubmitted: true});
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/deactivation/get/config/' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res && res.status === '200 OK') {
          this.setState({config: res.body}, () => {
            if (this.state.config.adminRightRequired) {
              this.setState({displayNameModal: false}, () => {
                this.props.navigation.navigate('ProfileDeactivationAdmin', {
                  reason: this.state.reasonForDeleting,
                  description: this.state.description,
                  previousActivityOriginal: this.state.previousActivityOriginal,
                });
              });
            } else {
              this.handleDeactivationSubmit();
            }
          });
        } else {
          this.setState({isSubmitted: false});
        }
      })
      .catch((err) => {
        console.log(err);
        this.setState({isSubmitted: false});
      });
  };

  handleDeactivationSubmit = () => {
    let postBody = {
      userId: this.state.userId,
      reason: this.state.reasonForDeleting,
      description: this.state.description,
      previousActivityOriginal: this.state.previousActivityOriginal,
      adminRights: {},
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/deactivation/deactivate',
      headers: {'Content-Type': 'application/json'},
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          console.log('object success');
        }
      })
      .catch((err) => {
        if (err && err.response.data) {
          this.setState({
            isLoaded: true,
            error: {message: err.response.data.message, err: err.response},
          });
        }
      });
  };

  getCurrentDeactivationConfig() {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/deactivation/get/updation/config/' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({config: response.data.body});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  cancelDeactivationRequest = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/deactivation/cancel?userId=' +
        this.state.userId +
        '&adminRightChangeQueueId=' +
        this.state.config.adminRightChangeQueueId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.getCurrentDeactivationConfig();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    return (
      <SafeAreaView>
        {this.changePassModal()}
        {this.reasonModal()}
        {this.emailOtpModal()}
        {this.chooseNameModal()}
        {this.chooseOptionsModalView()}

        <ProfileEditHeader
          name="Account Settings"
          iconName="UserCircle"
          goback={this.goback}
          addButton={false}
        />

        <View style={styles.changePassView}>
          <Text
            style={[typography.H6, {color: COLORS.dark_700, marginLeft: 5}]}>
            Your Password
          </Text>
          <Text
            style={[
              typography.Note2,
              {color: COLORS.grey_400, marginTop: 1, marginLeft: 5},
            ]}>
            Last updated on{' '}
            {moment
              .unix(this.state.lastUpdatedPassword / 1000)
              .format('Do MMM, YYYY')}
          </Text>
          <TouchableOpacity
            onPress={() => this.setState({changePassModalOpen: true})}
            style={styles.changePassBtn}>
            <Text
              style={[typography.H5, {color: COLORS.dark_600, marginTop: 1}]}>
              Change Password
            </Text>
            <Icon name="Arrow_Right" size={16} color={COLORS.dark_600} />
          </TouchableOpacity>
        </View>

        <View style={styles.deleteProfileView}>
          <Text
            style={[typography.H6, {color: COLORS.dark_700, marginLeft: 5}]}>
            Move out of WeNaturalists
          </Text>
          <Text
            style={[
              typography.Note2,
              {
                color: COLORS.grey_400,
                marginTop: 1,
                marginLeft: 5,
                marginRight: 10,
              },
            ]}>
            Proceed here to start the process to delete your profile. You can
            reactivate your account within one year of deactivation.
          </Text>
          {!this.state.config.deactivationRequestSent ? (
            <TouchableOpacity
              style={styles.deleteProfileBtn}
              onPress={() => this.setState({deleteModalOpen: true})}>
              <Text
                style={[
                  typography.H5,
                  {color: COLORS.alert_red, marginTop: 1, marginRight: 10},
                ]}>
                Delete your Profile
              </Text>
              <Icon name="Caution" size={16} color={COLORS.alert_red} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={styles.deleteProfileBtn}
              onPress={this.cancelDeactivationRequest}>
              <Text
                style={[
                  typography.H5,
                  {color: COLORS.altgreen_400, marginTop: 1, marginRight: 10},
                ]}>
                Cancel Request
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  changePassView: {
    marginTop: 15,
    padding: 15,
    backgroundColor: COLORS.white,
  },
  changePassBtn: {
    width: '98%',
    backgroundColor: COLORS.altgreen_100,
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 5,
    padding: 10,
  },
  updateBtn: {
    width: '80%',
    backgroundColor: COLORS.dark_700,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
    paddingVertical: 10,
    alignSelf: 'center',
  },
  deleteProfileView: {
    marginTop: 25,
    paddingRight: 15,
    paddingLeft: 25,
  },
  borderStyleBase: {
    color: '#00394D',
    fontWeight: 'bold',
    fontSize: 24,
    width: 36,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 2,
    borderBottomColor: '#BFC52E',
  },
  borderStyleHighLighted: {
    borderColor: '#03DAC6',
  },
  resend: {
    fontSize: 14,
    textAlign: 'center',
    color: '#367681',
    marginTop: 10,
    marginBottom: 6,
    fontFamily: 'Montserrat-Medium',
  },
  deleteProfileBtn: {
    backgroundColor: COLORS.grey_200,
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-end',
    marginTop: 15,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
