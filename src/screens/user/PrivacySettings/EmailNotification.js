import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  StyleSheet,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import {Switch} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';

import icoMoonConfig from '../../../../assets/Icons/selection.json';
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import typography from '../../../Components/Shared/Typography';
import axios from 'axios';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class EmailNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      connectionRequest: true,
      reminderConnectionRequest: true,
      forumPoll: true,
      employeeRequest: true,
    };
  }

  goback = () => this.props.navigation.goBack();

  componentDidMount() {
    this.getNotificationPreferance();
  }

  getNotificationPreferance = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/notificationSetting/list?userId=' +
        this.props.route.params.userId +
        '&notificationType=EMAIL',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          connectionRequest:
            response.data.body.CONNECT_INVITATION.NETWORK === true ||
            response.data.body.CONNECT_INVITATION.NETWORK === false
              ? response.data.body.CONNECT_INVITATION.NETWORK
              : true,
          reminderConnectionRequest:
            response.data.body.CONNECT_INVITATION_RESEND.NETWORK === true ||
            response.data.body.CONNECT_INVITATION_RESEND.NETWORK === false
              ? response.data.body.CONNECT_INVITATION_RESEND.NETWORK
              : true,
          forumPoll:
            response.data.body.FORUM_POLL_CREATE.FORUM_POLL === true ||
            response.data.body.FORUM_POLL_CREATE.FORUM_POLL === false
              ? response.data.body.FORUM_POLL_CREATE.FORUM_POLL
              : true,
          employeeRequest:
            response.data.body.REQUEST_RECEIVED_ADMIN.EMPLOYEE === true ||
            response.data.body.REQUEST_RECEIVED_ADMIN.EMPLOYEE === false
              ? response.data.body.REQUEST_RECEIVED_ADMIN.EMPLOYEE
              : true,
        });
      })
      .catch((err) => console.log(err));
  };

  updateNotificationPreferance = (category, userEvent, isActive, stateType) => {
    let postBody = {
      userId: this.props.route.params.userId,
      notificationCategory: category,
      userEvent: userEvent,
      notificationType: 'EMAIL',
      active: isActive,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/notificationSetting/create',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        if (stateType === 'connectionRequest') {
          this.setState({connectionRequest: isActive});
        } else if (stateType === 'reminderConnectionRequest') {
          this.setState({reminderConnectionRequest: isActive});
        } else if (stateType === 'forumPoll') {
          this.setState({forumPoll: isActive});
        } else if (stateType === 'employeeRequest') {
          this.setState({employeeRequest: isActive});
        }
      })
      .catch((err) => console.log(err));
  };

  render() {
    return (
      <SafeAreaView>
        <ProfileEditHeader
          name="Email Notification"
          iconName="Envelope"
          goback={this.goback}
          addButton={false}
        />

        <View
          style={{
            marginVertical: 15,
            alignSelf: 'center',
            width: '80%',
            alignItems: 'center',
          }}>
          <Text
            style={[
              typography.Subtitle_1,
              {
                color: COLORS.altgreen_300,
                marginTop: 1,
                marginLeft: 5,
                marginRight: 10,
                textAlign: 'center',
              },
            ]}>
            Emails are a great way to keep you updated on activities of your
            connects and members you follow. You may change your settings as per
            your requirements.
          </Text>
        </View>

        {/********** Networking notification settings start ***********/}

        <View style={{maxWidth: '85%', marginLeft: 25, marginBottom: 10}}>
          <Text style={[typography.H6, {color: COLORS.dark_700}]}>
            Networking
          </Text>
          <Text
            style={[
              typography.Note2,
              {color: COLORS.altgreen_300, marginTop: 1},
            ]}>
            You will receive an email when someone sends you a connection
            request
          </Text>
        </View>

        <View activeOpacity={0.5} style={styles.notiView}>
          <View style={{maxWidth: '80%'}}>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_600, marginTop: 1},
              ]}>
              Someone sends a connection request
            </Text>
          </View>
          <Switch
            color={COLORS.green_400}
            value={this.state.connectionRequest}
            onValueChange={(value) =>
              this.updateNotificationPreferance(
                'NETWORK',
                'CONNECT_INVITATION',
                value,
                'connectionRequest',
              )
            }
          />
        </View>
        <View activeOpacity={0.5} style={styles.notiView}>
          <View style={{maxWidth: '80%'}}>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_600, marginTop: 1},
              ]}>
              Reminder for a connection request
            </Text>
          </View>
          <Switch
            color={COLORS.green_400}
            value={this.state.reminderConnectionRequest}
            onValueChange={(value) =>
              this.updateNotificationPreferance(
                'NETWORK',
                'CONNECT_INVITATION_RESEND',
                value,
                'reminderConnectionRequest',
              )
            }
          />
        </View>

        {/********** Networking notification settings ends ***********/}

        {/********** Forum/Poll notification settings start ***********/}

        <View
          style={{
            maxWidth: '85%',
            marginLeft: 25,
            marginBottom: 10,
            marginTop: 20,
          }}>
          <Text style={[typography.H6, {color: COLORS.dark_700}]}>
            Forum/Poll
          </Text>
          <Text
            style={[
              typography.Note2,
              {color: COLORS.altgreen_300, marginTop: 1},
            ]}>
            You will receive an email when your connect and member you follow
            posts a Poll, a Forum, or shares an opinion
          </Text>
        </View>

        <View activeOpacity={0.5} style={styles.notiView}>
          <View style={{maxWidth: '80%'}}>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_600, marginTop: 1},
              ]}>
              Someone posts a Forum or Poll
            </Text>
          </View>
          <Switch
            color={COLORS.green_400}
            value={this.state.forumPoll}
            onValueChange={(value) =>
              this.updateNotificationPreferance(
                'FORUM_POLL',
                'FORUM_POLL_CREATE',
                value,
                'forumPoll',
              )
            }
          />
        </View>

        {/********** Forum/Poll notification settings ends ***********/}

        {/********** Employee request notification settings start ***********/}

        <View
          style={{
            maxWidth: '85%',
            marginLeft: 25,
            marginBottom: 10,
            marginTop: 20,
          }}>
          <Text style={[typography.H6, {color: COLORS.dark_700}]}>
            Employee
          </Text>
          <Text
            style={[
              typography.Note2,
              {color: COLORS.altgreen_300, marginTop: 1},
            ]}>
            You will receive an email when someone sends you employee request
          </Text>
        </View>

        <View activeOpacity={0.5} style={styles.notiView}>
          <View style={{maxWidth: '80%'}}>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_600, marginTop: 1},
              ]}>
              Someone sends an Employee request
            </Text>
          </View>
          <Switch
            color={COLORS.green_400}
            value={this.state.employeeRequest}
            onValueChange={(value) =>
              this.updateNotificationPreferance(
                'EMPLOYEE',
                'REQUEST_RECEIVED_ADMIN',
                value,
                'employeeRequest',
              )
            }
          />
        </View>

        {/********** Employee request notification settings ends ***********/}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  notiView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 1.5,
    paddingRight: 15,
    paddingVertical: 10,
    paddingLeft: 25,
    backgroundColor: COLORS.white,
  },
});
