import React, {Component} from 'react';
import {
  FlatList,
  StyleSheet,
  Modal,
  ScrollView,
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';

import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {REACT_APP_userServiceURL} from '../../../../env.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class PrivacyVisibility extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibilityModalOpen: false,
      basicInfoModal: false,
      activityInfoModal: false,
      settingDetails: {},
      settings: {},
      profileBasicInfo: 'Loading ...',
      profile_picture: 'Loading ...',
      phone_number: 'Loading ...',
      email: 'Loading ...',
      address: 'Loading ...',
      experience: 'Loading ...',
      education: 'Loading ...',
      specialization: 'Loading ...',
      skills: 'Loading ...',
      interests: 'Loading ...',
      endorsement: 'Loading ...',
      causes: 'Loading ...',
      feeds: 'Loading ...',
      recentActivity: 'Loading ...',
      businessPages: 'Loading ...',
      circle: 'Loading ...',
      first_connection_tab: 'Loading ...',
      mutual_connection_tab: 'Loading ...',
      follower_tab: 'Loading ...',

      visibilityOptions: [
        'Everyone on WeNaturalists',
        'First Connects',
        'Connects & Connect of Connects',
        'Private',
      ],
      basicInfoOptions: [
        'Everyone on WeNaturalists',
        'First Connects',
        'Connects & Connect of Connects',
        'Private',
      ],
      activityModalOptions: [
        'Everyone on WeNaturalists',
        'First Connects',
        'Connects & Connect of Connects',
        'Private',
      ],
      modalHeader: '',
      currentPressed: '',
      currentScope: '',
    };
  }

  componentDidMount() {
    this.getSettingDetails();
  }

  convertSetting = (value) => {
    if (value === 'GLOBAL') return 'Everyone on WeNaturalists';
    if (value === 'FIRSTCONNECT') return 'First Connects';
    if (value === 'SECONDCONNECT') return 'Connects & Connect of Connects';
    if (value === 'PRIVATE') return 'Private';
  };

  convertSetting2 = (value) => {
    if (value === 'Everyone on WeNaturalists')
      this.setState({currentScope: 'GLOBAL'}, () => this.updateSetting());
    if (value === 'First Connects')
      this.setState({currentScope: 'FIRSTCONNECT'}, () => this.updateSetting());
    if (value === 'Connects & Connect of Connects')
      this.setState({currentScope: 'SECONDCONNECT'}, () =>
        this.updateSetting(),
      );
    if (value === 'Private')
      this.setState({currentScope: 'PRIVATE'}, () => this.updateSetting());
  };

  goback = () => this.props.navigation.goBack();

  updateData = () => {
    if (this.state.settings?.basicInfoStrength <= 1) {
      let updatedBasicOptions = [
        'Everyone on WeNaturalists',
        'First Connects',
        'Connects & Connect of Connects',
        'Private',
      ];
      this.setState({basicInfoOptions: updatedBasicOptions});
    } else if (this.state.settings?.basicInfoStrength <= 2) {
      let updatedBasicOptions = [
        'First Connects',
        'Connects & Connect of Connects',
        'Private',
      ];
      this.setState({basicInfoOptions: updatedBasicOptions});
    } else if (this.state.settings?.basicInfoStrength <= 3) {
      let updatedBasicOptions = ['Connects & Connect of Connects', 'Private'];
      this.setState({basicInfoOptions: updatedBasicOptions});
    } else if (this.state.settings?.basicInfoStrength <= 4) {
      let updatedBasicOptions = ['Private'];
      this.setState({basicInfoOptions: updatedBasicOptions});
    }
  };

  updateActivityData = () => {
    if (this.state.settings?.activitiesStrength <= 1) {
      let updateactivitiesStrength = [
        'Everyone on WeNaturalists',
        'First Connects',
        'Connects & Connect of Connects',
        'Private',
      ];
      this.setState({activityModalOptions: updateactivitiesStrength});
    } else if (this.state.settings?.activitiesStrength <= 2) {
      let updateactivitiesStrength = [
        'First Connects',
        'Connects & Connect of Connects',
        'Private',
      ];
      this.setState({activityModalOptions: updateactivitiesStrength});
    } else if (this.state.settings?.activitiesStrength <= 3) {
      let updatedBasicOptions = ['Connects & Connect of Connects', 'Private'];
      this.setState({activityModalOptions: updatedBasicOptions});
    } else if (this.state.settings?.activitiesStrength <= 4) {
      let updateactivitiesStrength = ['Private'];
      this.setState({activityModalOptions: updateactivitiesStrength});
    }
  };

  handleSelect = (item) => {
    this.convertSetting2(item);
    if (this.state.currentPressed === 'basic_info')
      this.setState({
        profileBasicInfo: item,
        profile_picture: item,
        phone_number: item,
        email: item,
        address: item,
      });
    if (this.state.currentPressed === 'profile_picture')
      this.setState({profile_picture: item});
    if (this.state.currentPressed === 'phone_number')
      this.setState({phone_number: item});
    if (this.state.currentPressed === 'email') this.setState({email: item});
    if (this.state.currentPressed === 'address') this.setState({address: item});
    if (this.state.currentPressed === 'experience')
      this.setState({experience: item});
    if (this.state.currentPressed === 'education')
      this.setState({education: item});
    if (this.state.currentPressed === 'speciality')
      this.setState({specialization: item});
    if (this.state.currentPressed === 'skills') this.setState({skills: item});
    if (this.state.currentPressed === 'interests')
      this.setState({interests: item});
    if (this.state.currentPressed === 'endorsement')
      this.setState({endorsement: item});
    if (this.state.currentPressed === 'causes') this.setState({causes: item});
    if (this.state.currentPressed === 'feeds')
      this.setState({feeds: item, recentActivity: item});
    if (this.state.currentPressed === 'recentActivity')
      this.setState({recentActivity: item});
    if (this.state.currentPressed === 'businessPages')
      this.setState({businessPages: item});
    if (this.state.currentPressed === 'circle') this.setState({circle: item});
    if (this.state.currentPressed === 'first_connection_tab')
      this.setState({first_connection_tab: item});
    if (this.state.currentPressed === 'mutual_connection_tab')
      this.setState({mutual_connection_tab: item});
    if (this.state.currentPressed === 'follower_tab')
      this.setState({follower_tab: item});

    this.setState({visibilityModalOpen: false, basicInfoModal: false});
    // this.state.currentScope !== '' && this.updateSetting()
  };

  updateSetting() {
    let data = {
      userId: this.props.route.params.id,
      category: this.state.currentPressed,
      scope: this.state.currentScope,
      type: 'INDIVIDUAL',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/privacy/updatePrivacySetting',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data.statusCode === 200 &&
          response.data.body !== null
        ) {
          this.setState({settings: response.data.body}, () => {
            this.updateData();
            this.updateActivityData();
            Snackbar.show({
              backgroundColor: COLORS.dark_900,
              text: 'Updated Successfully',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          });
          // console.log(response.data.statusCode)
        }
      })
      .catch((err) => console.log(err));
  }

  getSettingDetails() {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/privacy/getPrivacySettings?userId=' +
        this.props.route.params.id,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data.statusCode === 200 &&
          response.data.body !== null
        ) {
          this.setState(
            {
              settingDetails: response.data.body.settings,
              settings: response.data.body,
            },
            () => {
              this.updateData();
              this.setState({
                profileBasicInfo: this.convertSetting(
                  this.state.settingDetails.basic_info,
                ),
                profile_picture: this.convertSetting(
                  this.state.settingDetails.profile_picture,
                ),
                phone_number: this.convertSetting(
                  this.state.settingDetails.phone_number,
                ),
                email: this.convertSetting(this.state.settingDetails.email),
                address: this.convertSetting(this.state.settingDetails.address),
                experience: this.convertSetting(
                  this.state.settingDetails.experience,
                ),
                education: this.convertSetting(
                  this.state.settingDetails.education,
                ),
                specialization: this.convertSetting(
                  this.state.settingDetails.speciality,
                ),
                skills: this.convertSetting(this.state.settingDetails.skills),
                interests: this.convertSetting(
                  this.state.settingDetails.interests,
                ),
                causes: this.convertSetting(
                  this.state.settingDetails.causesSupported,
                ),
                endorsement: this.convertSetting(
                  this.state.settingDetails.endorsement,
                ),
                feeds: this.convertSetting(this.state.settingDetails.feeds),
                recentActivity: this.convertSetting(
                  this.state.settingDetails.recentActivity,
                ),
                businessPages: this.convertSetting(
                  this.state.settingDetails.businessPages,
                ),
                circle: this.convertSetting(this.state.settingDetails.circle),
                first_connection_tab: this.convertSetting(
                  this.state.settingDetails.first_connection_tab,
                ),
                mutual_connection_tab: this.convertSetting(
                  this.state.settingDetails.mutual_connection_tab,
                ),
                follower_tab: this.convertSetting(
                  this.state.settingDetails.follower_tab,
                ),
              });
            },
          );
        }
      })
      .catch((err) => console.log(err));
  }

  visibilityModal = () => {
    return (
      <Modal
        visible={this.state.visibilityModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 560,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({visibilityModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 280, backgroundColor: COLORS.bgfill, paddingBottom: 10},
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                {this.state.modalHeader}
              </Text>
            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingBottom: 0,
                  paddingTop: 46,
                  width: '100%',
                }}
                style={{height: '50%'}}
                keyExtractor={(item) => item}
                data={this.state.visibilityOptions}
                initialNumToRender={10}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() => this.handleSelect(item)}
                    style={styles.item}
                    activeOpacity={0.7}>
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.Button_Lead,
                        {color: COLORS.dark_600, textAlign: 'center'},
                      ]}>
                      {item}
                    </Text>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      {/* {this.state.selectedvisibility.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  basicInfoModal = () => {
    return (
      <Modal
        visible={this.state.basicInfoModal}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 560,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({basicInfoModal: false})}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 280, backgroundColor: COLORS.bgfill, paddingBottom: 10},
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                {this.state.modalHeader}
              </Text>
            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingBottom: 0,
                  paddingTop: 46,
                  width: '100%',
                }}
                style={{height: '50%'}}
                keyExtractor={(item) => item}
                data={this.state.basicInfoOptions}
                initialNumToRender={10}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() => this.handleSelect(item)}
                    style={styles.item}
                    activeOpacity={0.7}>
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.Button_Lead,
                        {color: COLORS.dark_600, textAlign: 'center'},
                      ]}>
                      {item}
                    </Text>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      {/* {this.state.selectedvisibility.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  activityInfoModal = () => {
    return (
      <Modal
        visible={this.state.activityInfoModal}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 560,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({activityInfoModal: false})}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 280, backgroundColor: COLORS.bgfill, paddingBottom: 10},
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                {this.state.modalHeader}
              </Text>
            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingBottom: 0,
                  paddingTop: 46,
                  width: '100%',
                }}
                style={{height: '50%'}}
                keyExtractor={(item) => item}
                data={this.state.activityModalOptions}
                initialNumToRender={10}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() => this.handleSelect(item)}
                    style={styles.item}
                    activeOpacity={0.7}>
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.Button_Lead,
                        {color: COLORS.dark_600, textAlign: 'center'},
                      ]}>
                      {item}
                    </Text>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      {/* {this.state.selectedvisibility.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    // console.log(this.state.currentScope)
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: COLORS.bgFill_200,
          paddingBottom: 10,
        }}>
        <ScrollView>
          {this.visibilityModal()}
          {this.basicInfoModal()}
          {this.activityInfoModal()}

          <ProfileEditHeader
            name="Privacy Settings"
            iconName="Shield_Tick"
            goback={this.goback}
            addButton={false}
          />

          <View
            style={{
              alignSelf: 'center',
              flexDirection: 'row',
              backgroundColor: COLORS.altgreen_t50,
              width: 320,
              marginTop: 20,
              justifyContent: 'center',
              borderRadius: 4,
            }}>
            <TouchableOpacity
              style={[
                defaultShape.InTab_Btn,
                {backgroundColor: '#fff', width: '50%', borderRadius: 4},
              ]}>
              <Text style={[defaultStyle.Caption, {color: COLORS.dark_800}]}>
                Visibility
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('PrivacySharing', {
                  id: this.props.route.params.id,
                })
              }
              style={[
                defaultShape.InTab_Btn,
                {
                  backgroundColor: COLORS.altgreen_t50,
                  width: '50%',
                  borderRadius: 4,
                },
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Sharing
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              backgroundColor: COLORS.white,
              width: '100%',
              marginTop: 20,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 8},
              ]}>
              Profile basic information
            </Text>
            <Text
              style={[
                defaultStyle.Note2,
                {color: COLORS.altgreen_300, marginTop: 6, marginLeft: 8},
              ]}>
              Set who all can view your basic profile information. This includes
              your name and About section
            </Text>

            <TouchableOpacity
              onPress={() =>
                this.setState({
                  visibilityModalOpen: true,
                  currentPressed: 'basic_info',
                  modalHeader: 'PROFILE BASIC INFO',
                })
              }
              activeOpacity={0.6}
              style={{
                height: 44,
                width: '98%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 10,
                borderWidth: 1,
                borderColor: COLORS.grey_300,
                borderRadius: 8,
                paddingHorizontal: 10,
              }}>
              <Text style={[defaultStyle.Subtitle_1, {color: COLORS.dark_700}]}>
                {this.state.profileBasicInfo}
              </Text>

              <View
                style={{
                  width: 16,
                  height: 16,
                  borderRadius: 8,
                  backgroundColor: '#698F8A',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 0,
                }}>
                <Icon
                  name="Arrow_Down"
                  size={9}
                  color="#FFF"
                  style={{marginTop: Platform.OS === 'android' ? 4 : 0}}
                />
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: '100%',
              marginTop: 20,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 8},
              ]}>
              Profile information
            </Text>
            <Text
              style={[
                defaultStyle.Note2,
                {color: COLORS.altgreen_300, marginTop: 6, marginLeft: 8},
              ]}>
              Set who all can view your profile information. This includes your
              picture and personal information shared by you on your profile
            </Text>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                basicInfoModal: true,
                currentPressed: 'profile_picture',
                modalHeader: 'PROFILE PICTURE',
              })
            }
            style={{
              borderBottomColor: COLORS.bgFill_200,
              borderBottomWidth: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Profile Picture
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4, maxWidth: 180},
                ]}>
                {this.state.profile_picture}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                basicInfoModal: true,
                currentPressed: 'phone_number',
                modalHeader: 'PHONE NUMBER',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
              marginTop: 1,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Phone Number
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4, maxWidth: 180},
                ]}>
                {this.state.phone_number}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                basicInfoModal: true,
                currentPressed: 'email',
                modalHeader: 'Email Address',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
              marginTop: 2,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Email Address
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4, maxWidth: 180},
                ]}>
                {this.state.email}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                basicInfoModal: true,
                currentPressed: 'address',
                modalHeader: 'ADDRESS',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
              marginTop: 2,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Address
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4, maxWidth: 180},
                ]}>
                {this.state.address}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <View
            style={{
              width: '100%',
              marginTop: 20,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 8},
              ]}>
              Additional information
            </Text>
            <Text
              style={[
                defaultStyle.Note2,
                {color: COLORS.altgreen_300, marginTop: 6, marginLeft: 8},
              ]}>
              Set preference on who all can view your professional information
              on your profile
            </Text>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'experience',
                modalHeader: 'EXPERIENCE',
              })
            }
            style={{
              borderBottomColor: COLORS.bgFill_200,
              borderBottomWidth: 2,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Experience
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4},
                ]}>
                {this.state.experience}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'education',
                modalHeader: 'EDUCATION',
              })
            }
            style={{
              borderBottomColor: COLORS.bgFill_200,
              borderBottomWidth: 2,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Education
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4},
                ]}>
                {this.state.education}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'speciality',
                modalHeader: 'SPECIALIZATION',
              })
            }
            style={{
              borderBottomColor: COLORS.bgFill_200,
              borderBottomWidth: 2,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Specialization
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4},
                ]}>
                {this.state.specialization}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'skills',
                modalHeader: 'SKILLS',
              })
            }
            style={{
              borderBottomColor: COLORS.bgFill_200,
              borderBottomWidth: 2,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Skills
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4},
                ]}>
                {this.state.skills}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'interests',
                modalHeader: 'INTERESTS',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Interests
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4},
                ]}>
                {this.state.interests}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <View
            style={{
              width: '100%',
              marginTop: 20,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 8},
              ]}>
              Other information
            </Text>
            <Text
              style={[
                defaultStyle.Note2,
                {color: COLORS.altgreen_300, marginTop: 6, marginLeft: 8},
              ]}>
              Set preference on who all can view your other profile information
            </Text>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'endorsement',
                modalHeader: 'ENDORSEMENT',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Endorsements
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4},
                ]}>
                {this.state.endorsement}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'causes',
                modalHeader: 'CAUSES',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
              marginTop: 2,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Causes
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4},
                ]}>
                {this.state.causes}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <View
            style={{
              width: '100%',
              marginTop: 20,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 8},
              ]}>
              Activity
            </Text>
            <Text
              style={[
                defaultStyle.Note2,
                {color: COLORS.altgreen_300, marginTop: 6, marginLeft: 8},
              ]}>
              Set your preference on who all can see your profile activity. Your
              activity includes your own posts and interaction with and reaction
              to these posts by others
            </Text>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'feeds',
                modalHeader: 'ACTIVITY',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Activity
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4, maxWidth: 180},
                ]}>
                {this.state.feeds}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                activityInfoModal: true,
                currentPressed: 'recentActivity',
                modalHeader: 'RECENT ACTIVITIES',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
              marginTop: 2,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Recent Activities
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4, maxWidth: 180},
                ]}>
                {this.state.recentActivity}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'businessPages',
                modalHeader: 'ORGANIZATION',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
              marginTop: 2,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Organization
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4, maxWidth: 180},
                ]}>
                {this.state.businessPages}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'circle',
                modalHeader: 'CIRCLE',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
              marginTop: 2,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Circle
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4, maxWidth: 180},
                ]}>
                {this.state.circle}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <View
            style={{
              width: '100%',
              marginTop: 20,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 8},
              ]}>
              Connections
            </Text>
            <Text
              style={[
                defaultStyle.Note2,
                {color: COLORS.altgreen_300, marginTop: 6, marginLeft: 8},
              ]}>
              Set your preference on who all can view your connections and
              followers
            </Text>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'first_connection_tab',
                modalHeader: 'FIRST CONNECTS',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              First Connects
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4, maxWidth: 180},
                ]}>
                {this.state.first_connection_tab}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'mutual_connection_tab',
                modalHeader: 'MUTUAL CONNECTS',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
              marginTop: 2,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Mutual Connects
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4, maxWidth: 180},
                ]}>
                {this.state.mutual_connection_tab}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.setState({
                visibilityModalOpen: true,
                currentPressed: 'follower_tab',
                modalHeader: 'FOLLOWERS',
              })
            }
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
              marginTop: 2,
              marginBottom: 60,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Followers
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4, maxWidth: 180},
                ]}>
                {this.state.follower_tab}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  item: {
    flexDirection: 'row',
    width: '83%',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'left',
    marginLeft: 30,
    // marginRight: 100,
    paddingHorizontal: '3%',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey_200,
    // backgroundColor: 'pink'
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '90%',
    height: 26,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    zIndex: 2,
  },
  updateButton: {
    flexDirection: 'row',
    width: 81,
    height: 28,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: COLORS.dark_600,
  },
  circleCard: {
    flexDirection: 'row',
    width: '90%',
    height: 80,
    borderRadius: 8,
    backgroundColor: COLORS.bgFill_200,
    marginHorizontal: 8,
    marginVertical: 4,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: COLORS.grey_200,
    borderBottomWidth: 1,
    alignSelf: 'center',
    // paddingLeft: 12,
    // paddingRight: 26
  },
  circleTitle: {
    color: '#00394D',
    fontSize: 12,
    fontFamily: 'Montserrat-Bold',
    // marginTop: 10,
    // maxWidth: '85%'
  },
  circleSubTitle: {
    color: '#607580',
    fontSize: 10,
    fontFamily: 'Montserrat-Medium',
    // maxWidth: '85%'
  },
});
