import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Platform,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
  Modal,
  KeyboardAvoidingView,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';

import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import defaultStyle from '../../../Components/Shared/Typography';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import projectDefault from '../../../../assets/project-default.jpg';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import defaultCircle from '../../../../assets/CirclesDefault.png';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
export default class ProfileDeactivationAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 'projects',
      config: {},
      userId: '',
      displayAssignuser: false,
      currentPressed: '',
      adminList: [],
      type: 'admin',
      forAll: false,
      reason: props.route.params.reason || '',
      description: props.route.params.description || '',
      previousActivityOriginal:
        props.route.params.previousActivityOriginal || '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      value &&
        this.setState({userId: value}, () => this.getDeactivationConfig());
    });
  }

  getDeactivationConfig = () => {
    this.setState({isSubmitted: true});
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/deactivation/get/config/' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res && res.status === '200 OK') {
          console.log('imran', res.body);
          this.setState({config: res.body, isSubmitted: false});
        } else {
          this.setState({isSubmitted: false});
        }
      })
      .catch((err) => {
        console.log(err.response);
        this.setState({isSubmitted: false});
      });
  };

  getOptimizedImage(entity) {
    if (
      entity.resizedEntityImage &&
      entity.resizedEntityImage['300x300'] != null
    ) {
      return entity.resizedEntityImage['300x300'];
    }
    return entity.entityImage;
  }

  getOptimizedBusinessPagesImage(entity) {
    if (
      entity.resizedEntityImage &&
      entity.resizedEntityImage['200x200'] != null
    ) {
      return entity.resizedEntityImage['200x200'];
    }
    return entity.entityImage;
  }

  assignAdminForAll = (user) => {
    if (this.state.type === 'project') {
      let projects = this.state.config.projects;
      Object.keys(projects).map((data, index) => {
        let value = projects[data];
        if (value) {
          value.assigned = true;
          value.username = user.username;
          value.userId = user.id;
        }
      });
      this.setState(
        {
          config: {...this.state.config, projects: projects},
          displayAssignuser: false,
        },
        () => {
          this.checkAllAdminAssigned();
        },
      );
    } else if (this.state.type === 'circle') {
      let circles = this.state.config.circles;
      Object.keys(circles).map((data, index) => {
        let value = circles[data];
        if (value) {
          value.assigned = true;
          value.username = user.username;
          value.userId = user.id;
        }
      });
      this.setState(
        {
          config: {...this.state.config, circles: circles},
          displayAssignuser: false,
        },
        () => {
          this.checkAllAdminAssigned();
        },
      );
    } else if (this.state.type === 'businessPage') {
      let businessPages = this.state.config.businessPages;
      Object.keys(businessPages).map((data, index) => {
        let value = businessPages[data];
        if (value) {
          value.assigned = true;
          value.username = user.username;
          value.userId = user.id;
        }
      });
      this.setState(
        {
          config: {...this.state.config, businessPages: businessPages},
          displayAssignuser: false,
        },
        () => {
          this.checkAllAdminAssigned();
        },
      );
    }
  };

  checkAllAdminAssigned = () => {
    let allAdminAssigned = true;
    let businessPages = this.state.config.businessPages;
    let circles = this.state.config.circles;
    let projects = this.state.config.projects;
    if (businessPages) {
      Object.keys(businessPages).map((data, index) => {
        if (!businessPages[data].assigned) {
          allAdminAssigned = false;
          return;
        }
      });
    }
    if (circles) {
      Object.keys(circles).map((data, index) => {
        if (!circles[data].assigned) {
          allAdminAssigned = false;
          return;
        }
      });
    }
    if (projects) {
      Object.keys(projects).map((data, index) => {
        if (!projects[data].assigned) {
          allAdminAssigned = false;
          return;
        }
      });
    }
    this.setState({allAdminAssigned: allAdminAssigned});
  };

  assignAdminForSingle = (user) => {
    if (this.state.type === 'project') {
      let projects = this.state.config.projects;
      let value = projects[this.state.currentPressed];
      if (value) {
        value.assigned = true;
        value.username = user.username;
        value.userId = user.id;
      }
      this.setState(
        {
          config: {...this.state.config, projects: projects},
          displayAssignuser: false,
        },
        () => {
          this.checkAllAdminAssigned();
        },
      );
    } else if (this.state.type === 'circle') {
      let circles = this.state.config.circles;
      let value = circles[this.state.currentPressed];
      if (value) {
        value.assigned = true;
        value.username = user.username;
        value.userId = user.id;
      }

      this.setState(
        {
          config: {...this.state.config, circles: circles},
          displayAssignuser: false,
        },
        () => {
          this.checkAllAdminAssigned();
        },
      );
    } else if (this.state.type === 'businessPage') {
      let businessPages = this.state.config.businessPages;
      let value = businessPages[this.state.currentPressed];
      if (value) {
        value.assigned = true;
        value.username = user.username;
        value.userId = user.id;
      }
      this.setState(
        {
          config: {...this.state.config, businessPages: businessPages},
          displayAssignuser: false,
        },
        () => {
          this.checkAllAdminAssigned();
        },
      );
    }
  };

  chooseAssignModal = () => {
    return (
      <Modal
        visible={this.state.displayAssignuser}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{flex: 1, width: '100%'}}>
          <View style={{marginTop: 'auto'}}>
            <View
              style={{
                width: '100%',
                height: 560,
                position: 'absolute',
                bottom: 0,
                alignSelf: 'center',
              }}>
              <LinearGradient
                colors={['#154A5900', '#154A59CC', '#154A59']}
                style={{
                  flex: 1,
                  paddingLeft: 15,
                  paddingRight: 15,
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6,
                }}></LinearGradient>
            </View>
            <TouchableOpacity
              activeOpacity={0.7}
              style={defaultShape.CloseBtn}
              onPress={() => this.setState({displayAssignuser: false})}>
              <Icon
                name="Cross"
                size={13}
                color="#367681"
                style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
              />
            </TouchableOpacity>
            <View
              style={[
                defaultShape.Modal_Categories_Container,
                {
                  height: 400,
                  backgroundColor: COLORS.bgfill,
                  paddingBottom: 10,
                  paddingTop: 0,
                  alignItems: 'flex-start',
                },
              ]}>
              <View
                style={{
                  width: '100%',
                  height: 58,
                  backgroundColor: COLORS.altgreen_100,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                }}>
                <Text
                  style={[
                    typography.Button_2,
                    {color: COLORS.dark_900, textAlign: 'center'},
                  ]}>
                  Select Connects
                </Text>
              </View>
              <View style={{paddingHorizontal: 15, marginTop: 10}}>
                <FlatList
                  data={this.state.config.connectionList}
                  keyExtractor={(item) => item.id}
                  renderItem={({item, index}) => (
                    <View
                      style={{
                        alignItems: 'flex-start',
                        alignSelf: 'flex-start',
                        width: '96%',
                        marginVertical: 6,
                      }}>
                      <TouchableOpacity
                        style={{
                          flexDirection: 'row',
                          height: 44,
                          alignItems: 'center',
                        }}
                        onPress={() =>
                          this.state.forAll
                            ? this.assignAdminForAll(item)
                            : this.assignAdminForSingle(item)
                        }>
                        <Image
                          source={
                            item.imageUrl
                              ? {uri: item.imageUrl}
                              : item.userType === 'COMPANY'
                              ? DefaultBusiness
                              : defaultProfile
                          }
                          style={{
                            marginLeft: '7%',
                            marginRight: 10,
                            width: 32,
                            height: 32,
                            borderRadius: 16,
                          }}
                        />
                        <View
                          style={{
                            alignItems: 'flex-start',
                            justifyContent: 'center',
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}>
                            <Text
                              numberOfLines={1}
                              style={[
                                typography.Button_Lead,
                                {
                                  color: COLORS.dark_600,
                                  textTransform:
                                    item.userType === 'COMPANY'
                                      ? 'none'
                                      : 'capitalize',
                                },
                              ]}>
                              {item.username}
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    </View>
                  )}
                />
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  };

  handleSubmit = () => {
    let adminRights = {};
    let projects = this.state.config.projects;
    let circles = this.state.config.circles;
    let businessPages = this.state.config.businessPages;
    if (projects) {
      Object.keys(projects).map((data, index) => {
        let value = projects[data];
        if (value) {
          let insertData = {};
          insertData.userId = value.userId;
          insertData.entityId = value.entityId;
          insertData.title = value.title;
          insertData.entityType = value.entityType;
          insertData.status = 'PENDING';
          adminRights[data] = insertData;
        }
      });
    }
    if (circles) {
      Object.keys(circles).map((data, index) => {
        let value = circles[data];
        if (value) {
          let insertData = {};
          insertData.userId = value.userId;
          insertData.entityId = value.entityId;
          insertData.title = value.title;
          insertData.entityType = value.entityType;
          insertData.status = 'PENDING';
          adminRights[data] = insertData;
        }
      });
    }
    if (businessPages) {
      Object.keys(businessPages).map((data, index) => {
        let value = businessPages[data];
        if (value) {
          let insertData = {};
          insertData.userId = value.userId;
          insertData.entityId = value.entityId;
          insertData.title = value.title;
          insertData.entityType = value.entityType;
          insertData.status = 'PENDING';
          adminRights[data] = insertData;
        }
      });
    }

    let postBody = {
      userId: this.state.userId,
      reason: this.state.reason,
      description: this.state.description,
      previousActivityOriginal: this.state.previousActivityOriginal,
      adminRights: adminRights,
    };

    axios({
      method: 'POST',
      url: REACT_APP_userServiceURL + '/deactivation/deactivate',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    const {selectedTab} = this.state;
    let projects = [];
    let businessPages = [];
    let circles = [];
    if (this.state.config.projects) {
      projects = Object.keys(this.state.config.projects);
    }
    if (this.state.config.businessPages) {
      businessPages = Object.keys(this.state.config.businessPages);
    }
    if (this.state.config.circles) {
      circles = Object.keys(this.state.config.circles);
    }
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.chooseAssignModal()}
        <View
          style={
            Platform.OS === 'ios'
              ? [styles.header, {paddingVertical: 15}]
              : styles.header
          }>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <TouchableOpacity
                style={{
                  width: 40,
                  height: 30,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() => this.props.navigation.goBack()}>
                <Icon
                  name="Arrow-Left"
                  size={15}
                  color="#91B3A2"
                  style={
                    Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}
                  }
                />
              </TouchableOpacity>
              <View>
                <Text
                  style={[
                    typography.Title_2,
                    {
                      color: COLORS.dark_500,
                      marginLeft: 5,
                      textTransform: 'capitalize',
                    },
                  ]}>
                  Transfer Admin Rights
                </Text>
              </View>
            </View>
            <TouchableOpacity
              style={{
                alignItems: 'center',
                paddingHorizontal: 15,
                paddingVertical: 5,
                backgroundColor: this.state.allAdminAssigned
                  ? COLORS.dark_800
                  : COLORS.grey_300,
                borderRadius: 20,
                marginVertical: 6,
                marginHorizontal: 10,
              }}
              disabled={!this.state.allAdminAssigned}
              onPress={this.handleSubmit}>
              <Text
                style={[
                  typography.Caption,
                  {
                    color: this.state.allAdminAssigned
                      ? COLORS.white
                      : COLORS.dark_600,
                  },
                ]}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{paddingBottom: 20}}>
          <Text
            style={[
              typography.Note2,
              {
                color: COLORS.dark_500,
                paddingVertical: 2,
                paddingHorizontal: 2,
                textTransform: 'capitalize',
              },
            ]}>
            Before deleting your account, please transfer your admin rights. The
            admin rights will be transferred once all your requests are
            accepted.
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              width: '100%',
              backgroundColor: COLORS.white,
            }}>
            <TouchableOpacity
              onPress={() => this.setState({selectedTab: 'projects'})}>
              <Text
                style={[
                  selectedTab === 'projects'
                    ? typography.Body_1_bold
                    : typography.Caption,
                  {
                    color:
                      selectedTab === 'projects'
                        ? COLORS.dark_800
                        : COLORS.altgreen_400,
                    textAlign: 'center',
                    marginBottom: 6,
                    paddingTop: 8,
                  },
                ]}>
                Projects ({this.state.config.totalProjectCount})
              </Text>
              <View
                style={{
                  height: 5,
                  backgroundColor:
                    selectedTab === 'projects'
                      ? COLORS.dark_800
                      : COLORS.NoColor,
                  borderTopLeftRadius: 4,
                  borderTopRightRadius: 4,
                }}></View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.setState({selectedTab: 'organization'})}>
              <Text
                style={[
                  selectedTab === 'organization'
                    ? typography.Body_1_bold
                    : typography.Caption,
                  {
                    color:
                      selectedTab === 'organization'
                        ? COLORS.dark_800
                        : COLORS.altgreen_400,
                    textAlign: 'center',
                    marginBottom: 6,
                    paddingTop: 8,
                  },
                ]}>
                Organization Pages ({this.state.config.totalBusinessPageCount})
              </Text>
              <View
                style={{
                  height: 5,
                  backgroundColor:
                    selectedTab === 'organization'
                      ? COLORS.dark_800
                      : COLORS.NoColor,
                  borderTopLeftRadius: 4,
                  borderTopRightRadius: 4,
                }}></View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.setState({selectedTab: 'circles'})}>
              <Text
                style={[
                  selectedTab === 'circles'
                    ? typography.Body_1_bold
                    : typography.Caption,
                  {
                    color:
                      selectedTab === 'circles'
                        ? COLORS.dark_800
                        : COLORS.altgreen_400,
                    textAlign: 'center',
                    marginBottom: 6,
                    paddingTop: 8,
                  },
                ]}>
                Circles ({this.state.config.totalCircleCount})
              </Text>
              <View
                style={{
                  height: 5,
                  backgroundColor:
                    selectedTab === 'circles'
                      ? COLORS.dark_800
                      : COLORS.NoColor,
                  borderTopLeftRadius: 4,
                  borderTopRightRadius: 4,
                }}></View>
            </TouchableOpacity>
          </View>
          {this.state.selectedTab === 'projects' && (
            <View>
              <TouchableOpacity
                style={{
                  alignSelf: 'flex-end',
                  width: 70,
                  alignItems: 'center',
                  paddingHorizontal: 5,
                  paddingVertical: 5,
                  backgroundColor: COLORS.dark_800,
                  borderRadius: 4,
                  marginVertical: 6,
                  marginHorizontal: 10,
                }}
                onPress={() =>
                  this.setState({
                    displayAssignuser: true,
                    type: 'project',
                    forAll: true,
                  })
                }>
                <Text style={[typography.Note2, {color: COLORS.white}]}>
                  Assign All
                </Text>
              </TouchableOpacity>
              <FlatList
                data={projects}
                keyExtractor={(item) => item}
                renderItem={(item) => (
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      backgroundColor: COLORS.white,
                      borderRadius: 8,
                      shadowColor: '#000',
                      shadowOffset: {
                        width: 0,
                        height: 1,
                      },
                      shadowOpacity: 0.22,
                      shadowRadius: 2.22,
                      elevation: 3,
                      width: '90%',
                      alignSelf: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={
                        this.state.config.projects[item.item]?.entityImage
                          ? {
                              uri: this.getOptimizedImage(
                                this.state.config.projects[item.item],
                              ),
                            }
                          : projectDefault
                      }
                      style={{
                        height: 90,
                        width: 90,
                        borderTopLeftRadius: 8,
                        borderBottomLeftRadius: 8,
                      }}
                    />
                    <View style={{marginLeft: 10}}>
                      <Text
                        style={[typography.Caption, {color: COLORS.dark_800}]}>
                        {this.state.config.projects[item.item]?.title}
                      </Text>
                      <Text
                        style={[
                          typography.Note2,
                          {color: COLORS.altgreen_400},
                        ]}>
                        {this.state.config?.projects[item.item]?.entityType}
                      </Text>
                      <Text
                        style={[
                          typography.Note2,
                          {color: COLORS.altgreen_400},
                        ]}>
                        {this.state.config?.projects[item.item]?.assigned
                          ? this.state.config?.projects[item.item]?.username
                          : 'Not Assigned'}
                      </Text>
                      <TouchableOpacity
                        style={{
                          width: 60,
                          alignItems: 'center',
                          paddingHorizontal: 5,
                          paddingVertical: 5,
                          backgroundColor: COLORS.dark_800,
                          borderRadius: 4,
                          marginVertical: 6,
                        }}
                        onPress={() =>
                          this.setState({
                            displayAssignuser: true,
                            currentPressed: item.item,
                            type: 'project',
                            forAll: false,
                          })
                        }>
                        <Text style={[typography.Note2, {color: COLORS.white}]}>
                          Assign
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
              />
            </View>
          )}
          {this.state.selectedTab === 'organization' && (
            <View style={{marginBottom: 220}}>
              <TouchableOpacity
                style={{
                  alignSelf: 'flex-end',
                  width: 70,
                  alignItems: 'center',
                  paddingHorizontal: 5,
                  paddingVertical: 5,
                  backgroundColor: COLORS.dark_800,
                  borderRadius: 4,
                  marginVertical: 6,
                  marginHorizontal: 10,
                }}
                onPress={() =>
                  this.setState({
                    displayAssignuser: true,
                    type: 'businessPage',
                    forAll: true,
                  })
                }>
                <Text style={[typography.Note2, {color: COLORS.white}]}>
                  Assign All
                </Text>
              </TouchableOpacity>
              <View>
                <FlatList
                  data={businessPages}
                  keyExtractor={(item) => item}
                  renderItem={(item) => (
                    <View
                      style={{
                        marginTop: 10,
                        flexDirection: 'row',
                        backgroundColor: COLORS.white,
                        borderRadius: 8,
                        shadowColor: '#000',
                        shadowOffset: {
                          width: 0,
                          height: 1,
                        },
                        shadowOpacity: 0.22,
                        shadowRadius: 2.22,
                        elevation: 3,
                        width: '90%',
                        alignSelf: 'center',
                        alignItems: 'center',
                      }}>
                      <Image
                        source={
                          this.state.config.businessPages[item.item].entityImage
                            ? {
                                uri: this.getOptimizedBusinessPagesImage(
                                  this.state.config.businessPages[item.item],
                                ),
                              }
                            : defaultBusiness
                        }
                        style={{
                          height: 90,
                          width: 90,
                          borderTopLeftRadius: 8,
                          borderBottomLeftRadius: 8,
                        }}
                      />
                      <View style={{marginLeft: 10}}>
                        <Text
                          style={[
                            typography.Caption,
                            {color: COLORS.dark_800},
                          ]}>
                          {this.state.config.businessPages[item.item].title}
                        </Text>
                        <Text
                          style={[
                            typography.Note2,
                            {color: COLORS.altgreen_400},
                          ]}>
                          {this.state.config.businessPages[item.item].assigned
                            ? this.state.config.businessPages[item.item]
                                .username
                            : 'Not Assigned'}
                        </Text>
                        <TouchableOpacity
                          style={{
                            width: 60,
                            alignItems: 'center',
                            paddingHorizontal: 5,
                            paddingVertical: 5,
                            backgroundColor: COLORS.dark_800,
                            borderRadius: 4,
                            marginVertical: 6,
                          }}
                          onPress={() =>
                            this.setState({
                              displayAssignuser: true,
                              currentPressed: item.item,
                              type: 'businessPage',
                              forAll: false,
                            })
                          }>
                          <Text
                            style={[typography.Note2, {color: COLORS.white}]}>
                            Assign
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  )}
                />
              </View>
            </View>
          )}
          {this.state.selectedTab === 'circles' && (
            <View style={{marginBottom: 220}}>
              <TouchableOpacity
                style={{
                  alignSelf: 'flex-end',
                  width: 70,
                  alignItems: 'center',
                  paddingHorizontal: 5,
                  paddingVertical: 5,
                  backgroundColor: COLORS.dark_800,
                  borderRadius: 4,
                  marginVertical: 6,
                  marginHorizontal: 10,
                }}
                onPress={() =>
                  this.setState({
                    displayAssignuser: true,
                    type: 'circle',
                    forAll: true,
                  })
                }>
                <Text style={[typography.Note2, {color: COLORS.white}]}>
                  Assign All
                </Text>
              </TouchableOpacity>
              <View>
                <FlatList
                  data={circles}
                  keyExtractor={(item) => item}
                  renderItem={(item) => (
                    <View
                      style={{
                        marginTop: 10,
                        flexDirection: 'row',
                        backgroundColor: COLORS.white,
                        borderRadius: 8,
                        shadowColor: '#000',
                        shadowOffset: {
                          width: 0,
                          height: 1,
                        },
                        shadowOpacity: 0.22,
                        shadowRadius: 2.22,
                        elevation: 3,
                        width: '90%',
                        alignSelf: 'center',
                        alignItems: 'center',
                      }}>
                      <Image
                        source={
                          this.state.config.circles[item.item].entityImage
                            ? {
                                uri: this.getOptimizedImage(
                                  this.state.config.circles[item.item],
                                ),
                              }
                            : defaultCircle
                        }
                        style={{
                          height: 90,
                          width: 90,
                          borderTopLeftRadius: 8,
                          borderBottomLeftRadius: 8,
                        }}
                      />
                      <View style={{marginLeft: 10}}>
                        <Text
                          style={[
                            typography.Caption,
                            {color: COLORS.dark_800},
                          ]}>
                          {this.state.config.circles[item.item].title}
                        </Text>
                        <Text
                          style={[
                            typography.Note2,
                            {color: COLORS.altgreen_400},
                          ]}>
                          {this.state.config.circles[item.item].country}
                        </Text>
                        <Text
                          style={[
                            typography.Note2,
                            {color: COLORS.altgreen_400},
                          ]}>
                          {this.state.config.circles[item.item].assigned
                            ? this.state.config.circles[item.item].username
                            : 'Not Assigned'}
                        </Text>
                        <TouchableOpacity
                          style={{
                            width: 60,
                            alignItems: 'center',
                            paddingHorizontal: 5,
                            paddingVertical: 5,
                            backgroundColor: COLORS.dark_800,
                            borderRadius: 4,
                            marginVertical: 6,
                          }}
                          onPress={() =>
                            this.setState({
                              displayAssignuser: true,
                              currentPressed: item.item,
                              type: 'circle',
                              forAll: false,
                            })
                          }>
                          <Text
                            style={[typography.Note2, {color: COLORS.white}]}>
                            Assign
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  )}
                />
              </View>
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  forumItemView: {
    width: '92%',
    marginTop: 15,
    backgroundColor: COLORS.white,
    borderRadius: 4,
    alignSelf: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
  profileImage: {
    height: 30,
    width: 30,
    borderRadius: 15,
    marginRight: 8,
  },
  title: {
    marginTop: 15,
    color: COLORS.dark_800,
    marginHorizontal: 15,
  },
  subtitle: {
    color: COLORS.dark_600,
    marginTop: 5,
  },
  body: {
    color: COLORS.altgreen_400,
    marginTop: 5,
  },
  commentCountBar: {
    backgroundColor: COLORS.altgreen_100,
    paddingVertical: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  commentItemView: {
    paddingLeft: 15,
    backgroundColor: COLORS.white,
    paddingTop: 12,
    borderBottomColor: COLORS.altgreen_400,
    borderBottomWidth: 0.2,
  },
  commentBody: {
    color: COLORS.altgreen_400,
    marginTop: -25,
    marginLeft: 38,
    maxWidth: '80%',
  },
  commentBoxView: {
    position: 'absolute',
    bottom: 0,
    paddingVertical: 6,
    backgroundColor: COLORS.altgreen_200,
    width: '100%',
    zIndex: 1,
  },
  inputBox: {
    marginLeft: 10,
    width: '80%',
    height: 40,
    backgroundColor: COLORS.white,
    borderRadius: 6,
    paddingHorizontal: 10,
  },
  submitBtn: {
    width: 90,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primarydark,
    borderRadius: 4,
    marginVertical: 10,
  },
  pollItem: {
    maxWidth: '100%',
    minHeight: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f2f3f4',
    borderRadius: 4,
    marginBottom: 10,
    padding: 8,
  },
  votedPollItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    maxWidth: '100%',
    minHeight: 30,
    alignItems: 'center',
    backgroundColor: '#f2f3f4',
    borderRadius: 4,
    marginBottom: 10,
    // paddingRight: 10,
  },
  progressView: {
    maxWidth: '100%',
    minHeight: 30,
    height: '100%',
    borderRadius: 4,
    marginBottom: 10,
    position: 'absolute',
    left: 0,
    zIndex: 1,
  },
});
