import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Modal,
  FlatList,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import {cloneDeep} from 'lodash';
import {Switch} from 'react-native-paper';

import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {COLORS} from '../../../Components/Shared/Colors';
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class NotificationSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibilityModalOpen: false,
      modalHeader: '',
      modalData: [],
      notiData: {},
    };
  }

  goback = () => this.props.navigation.goBack();

  componentDidMount() {
    this.getNotificationPreferance();
  }

  getNotificationPreferance = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/notificationSetting/list?userId=' +
        this.props.route.params.userId +
        '&notificationType=DASHBOARD',
      withCredentials: true,
    })
      .then((response) => {
        let tempNotiData = response.data.body;
        if (tempNotiData['LIKE']) {
          if (
            tempNotiData['LIKE']['POLL'] === false ||
            tempNotiData['LIKE']['FORUM'] === false ||
            tempNotiData['LIKE']['PROJECT'] === false ||
            tempNotiData['LIKE']['POST'] === false ||
            tempNotiData['LIKE']['COMMENT'] === false
          )
            tempNotiData['LIKE']['ALL'] = false;
        }
        if (tempNotiData['COMMENT']) {
          if (
            tempNotiData['COMMENT']['FORUM'] === false ||
            tempNotiData['COMMENT']['POST'] === false
          )
            tempNotiData['COMMENT']['ALL'] = false;
        }
        if (tempNotiData['POSTSHARE']) {
          if (
            tempNotiData['POSTSHARE']['POLL'] === false ||
            tempNotiData['POSTSHARE']['FORUM'] === false ||
            tempNotiData['POSTSHARE']['PROJECT'] === false ||
            tempNotiData['POSTSHARE']['POST'] === false
          )
            tempNotiData['POSTSHARE']['ALL'] = false;
        }
        this.setState({
          notiData: tempNotiData,
        });
      })
      .catch((err) => console.log(err));
  };

  convertedValueNotification = (userEvent, category) => {
    let tempNotiData = cloneDeep(this.state.notiData);
    if (tempNotiData[userEvent]) {
      if (
        tempNotiData[userEvent][category] === true ||
        tempNotiData[userEvent][category] === false
      ) {
        return tempNotiData[userEvent][category];
      } else {
        return true;
      }
    } else {
      return true;
    }
  };

  updateNotificationPreferance = (category, userEvent, isActive, index) => {
    let postBody = {
      userId: this.props.route.params.userId,
      notificationCategory: category,
      userEvent: userEvent,
      notificationType: 'DASHBOARD',
      active: isActive,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/notificationSetting/create',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let tempModalData = cloneDeep(this.state.modalData);
        let tempNotiData = cloneDeep(this.state.notiData);
        tempModalData[index].value = isActive;
        {
          tempNotiData[userEvent]
            ? tempNotiData[userEvent][category]
              ? (tempNotiData[userEvent][category] = isActive)
              : (tempNotiData[userEvent][category] = isActive)
            : (tempNotiData[userEvent] = {});
          tempNotiData[userEvent][category] = isActive;
        }
        if (
          category !== 'ALL' &&
          (userEvent === 'LIKE' ||
            userEvent === 'COMMENT' ||
            userEvent === 'POSTSHARE') &&
          isActive === false &&
          tempNotiData[userEvent]
        ) {
          tempNotiData[userEvent]['ALL'] = false;
          tempModalData[0].value = false;
        } else if (category === 'ALL' && tempNotiData[userEvent]) {
          for (let i = 1; i < tempModalData.length; i++) {
            tempModalData[i].value = isActive;
          }
        }
        this.setState({modalData: tempModalData, notiData: tempNotiData});
      })
      .catch((err) => console.log(err));
  };

  handleModal = (type) => {
    this.setState({visibilityModalOpen: true, modalHeader: type}, () => {
      if (type === 'LIKE') {
        this.setState({
          modalData: [
            {
              userEvent: 'LIKE',
              notificationCategory: 'ALL',
              text: 'All',
              value: this.convertedValueNotification('LIKE', 'ALL'),
            },
            {
              userEvent: 'LIKE',
              notificationCategory: 'POLL',
              text: 'Someone likes your posted Poll',
              value: this.convertedValueNotification('LIKE', 'POLL'),
            },
            {
              userEvent: 'LIKE',
              notificationCategory: 'FORUM',
              text: 'Someone likes your posted Forum',
              value: this.convertedValueNotification('LIKE', 'FORUM'),
            },
            {
              userEvent: 'LIKE',
              notificationCategory: 'PROJECT',
              text: 'Someone likes your posted Project',
              value: this.convertedValueNotification('LIKE', 'PROJECT'),
            },
            {
              userEvent: 'LIKE',
              notificationCategory: 'POST',
              text: 'Someone likes your post in feeds',
              value: this.convertedValueNotification('LIKE', 'POST'),
            },
            {
              userEvent: 'LIKE',
              notificationCategory: 'COMMENT',
              text: 'Someone likes your comment',
              value: this.convertedValueNotification('LIKE', 'COMMENT'),
            },
          ],
        });
      } else if (type === 'COMMENT') {
        this.setState({
          modalData: [
            {
              userEvent: 'COMMENT',
              notificationCategory: 'ALL',
              text: 'All',
              value: this.convertedValueNotification('COMMENT', 'ALL'),
            },
            {
              userEvent: 'COMMENT',
              notificationCategory: 'FORUM',
              text: 'Someone comments on your posted Forum',
              value: this.convertedValueNotification('COMMENT', 'FORUM'),
            },
            {
              userEvent: 'COMMENT',
              notificationCategory: 'POST',
              text: 'Someone comments on your posts in Feed',
              value: this.convertedValueNotification('COMMENT', 'POST'),
            },
          ],
        });
      } else if (type === 'SHARING') {
        this.setState({
          modalData: [
            {
              userEvent: 'POSTSHARE',
              notificationCategory: 'ALL',
              text: 'All',
              value: this.convertedValueNotification('POSTSHARE', 'ALL'),
            },
            {
              userEvent: 'POSTSHARE',
              notificationCategory: 'POLL',
              text: 'Someone shares your posted Poll',
              value: this.convertedValueNotification('POSTSHARE', 'POLL'),
            },
            {
              userEvent: 'POSTSHARE',
              notificationCategory: 'FORUM',
              text: 'Someone shares your posted Forum',
              value: this.convertedValueNotification('POSTSHARE', 'FORUM'),
            },
            {
              userEvent: 'POSTSHARE',
              notificationCategory: 'POST',
              text: 'Someone shares your posted Feed',
              value: this.convertedValueNotification('POSTSHARE', 'POST'),
            },
            {
              userEvent: 'POSTSHARE',
              notificationCategory: 'PROJECT',
              text: 'Someone shares your posted Project',
              value: this.convertedValueNotification('POSTSHARE', 'PROJECT'),
            },
          ],
        });
      } else if (type === 'PROJECT CREATE') {
        this.setState({
          modalData: [
            {
              userEvent: 'PROJECT_CREATE',
              notificationCategory: 'PROJECT',
              text: 'A Project is posted',
              value: this.convertedValueNotification(
                'PROJECT_CREATE',
                'PROJECT',
              ),
            },
          ],
        });
      } else if (type === 'OTHER') {
        this.setState({
          modalData: [
            {
              userEvent: 'LIKE_REPLY',
              notificationCategory: 'COMMENT',
              text: 'Someone likes your Reply',
              value: this.convertedValueNotification('LIKE_REPLY', 'COMMENT'),
            },
            {
              userEvent: 'COMMENT_REPLY',
              notificationCategory: 'COMMENT',
              text: 'Someone replies on your Comment',
              value: this.convertedValueNotification(
                'COMMENT_REPLY',
                'COMMENT',
              ),
            },
            {
              userEvent: 'POST_AS_CAUSE',
              notificationCategory: 'POST',
              text: 'Someone posts a feed in Cause you support',
              value: this.convertedValueNotification('POST_AS_CAUSE', 'POST'),
            },
            {
              userEvent: 'POST_AS_PROJECT',
              notificationCategory: 'POST',
              text: 'Someone posts a feed in Project you participated',
              value: this.convertedValueNotification('POST_AS_PROJECT', 'POST'),
            },
            {
              userEvent: 'MAKE_ADMIN',
              notificationCategory: 'MESSAGING',
              text: 'Someone adds you as an Admin for the chat group',
              value: this.convertedValueNotification('MAKE_ADMIN', 'MESSAGING'),
            },
            {
              userEvent: 'VOTE',
              notificationCategory: 'POLL',
              text: 'Someone votes',
              value: this.convertedValueNotification('VOTE', 'POLL'),
            },
            {
              userEvent: 'REQUEST_RECEIVED_ADMIN',
              notificationCategory: 'EMPLOYEE',
              text: 'Someone sends an employee request for your organization',
              value: this.convertedValueNotification(
                'REQUEST_RECEIVED_ADMIN',
                'EMPLOYEE',
              ),
            },
            {
              userEvent: 'REQUEST_ACCEPTED',
              notificationCategory: 'EMPLOYEE',
              text: 'Your employee request is accepted',
              value: this.convertedValueNotification(
                'REQUEST_ACCEPTED',
                'EMPLOYEE',
              ),
            },
            {
              userEvent: 'ADD',
              notificationCategory: 'ENDORSEMENT',
              text: 'Someone Endorses you',
              value: this.convertedValueNotification('ADD', 'ENDORSEMENT'),
            },
            {
              userEvent: 'FORUM_POLL_CREATE',
              notificationCategory: 'POLL',
              text: 'A poll is created',
              value: this.convertedValueNotification(
                'FORUM_POLL_CREATE',
                'POLL',
              ),
            },
            {
              userEvent: 'FORUM_POLL_CREATE',
              notificationCategory: 'FORUM',
              text: 'A forum is posted',
              value: this.convertedValueNotification(
                'FORUM_POLL_CREATE',
                'FORUM',
              ),
            },
            {
              userEvent: 'MENTION',
              notificationCategory: 'POST',
              text: 'Someone tags you in the post',
              value: this.convertedValueNotification('MENTION', 'POST'),
            },
            {
              userEvent: 'POSTCOMMENTMENTION',
              notificationCategory: 'POST',
              text: 'Someone commented the tagged post',
              value: this.convertedValueNotification(
                'POSTCOMMENTMENTION',
                'POST',
              ),
            },
            {
              userEvent: 'POSTLIKEMENTION',
              notificationCategory: 'POST',
              text: 'Someone likes the tagged post',
              value: this.convertedValueNotification('POSTLIKEMENTION', 'POST'),
            },
          ],
        });
      }
    });
  };

  visibilityModal = () => {
    return (
      <Modal
        visible={this.state.visibilityModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 560,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({visibilityModalOpen: false}, () =>
                this.getNotificationPreferance(),
              )
            }>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                height: 320,
                backgroundColor: COLORS.bgfill,
                paddingBottom: 10,
                paddingTop: 0,
                alignItems: 'flex-start',
              },
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                // position: 'absolute',
                // top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  typography.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                {this.state.modalHeader}
              </Text>
            </View>

            <View style={{marginBottom: 60}}>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                keyExtractor={(item) => item.text}
                data={this.state.modalData}
                renderItem={({item, index}) => (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginTop: 10,
                      paddingBottom: 5,
                      marginHorizontal: 15,
                      borderBottomColor: COLORS.grey_300,
                      borderBottomWidth: 1,
                      width: '90%',
                    }}
                    activeOpacity={0.7}>
                    <Text
                      numberOfLines={2}
                      style={[
                        typography.Caption,
                        {
                          color: COLORS.dark_600,
                          maxWidth: '80%',
                          marginLeft: 15,
                        },
                      ]}>
                      {item.text}
                    </Text>
                    <Switch
                      color={COLORS.green_400}
                      value={item.value}
                      onValueChange={(value) =>
                        this.updateNotificationPreferance(
                          item.notificationCategory,
                          item.userEvent,
                          value,
                          index,
                        )
                      }
                    />
                  </View>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView>
        {this.visibilityModal()}
        <ProfileEditHeader
          name="Notification Settings"
          iconName="Bell"
          goback={this.goback}
          addButton={false}
        />
        <View
          style={{
            marginVertical: 15,
            alignSelf: 'center',
            width: '80%',
            alignItems: 'center',
          }}>
          <Text
            style={[
              typography.Subtitle_1,
              {
                color: COLORS.altgreen_300,
                marginTop: 1,
                marginLeft: 5,
                marginRight: 10,
                textAlign: 'center',
              },
            ]}>
            Notifications makes it easy to keep a track of events that occur on
            WeNaturalists related to you, your connects and the members you
            follow. You may change settings as per your requirements.
          </Text>
        </View>

        <TouchableOpacity
          onPress={() => this.handleModal('LIKE')}
          activeOpacity={0.5}
          style={styles.notiView}>
          <View style={{maxWidth: '80%'}}>
            <Text style={[typography.H6, {color: COLORS.dark_700}]}>Like</Text>
            <Text
              style={[
                typography.Note2,
                {color: COLORS.altgreen_300, marginTop: 1},
              ]}>
              You will receive notifications when a member likes your activity
            </Text>
          </View>
          <Icon name="Arrow_Right" size={16} color={COLORS.dark_600} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.handleModal('COMMENT')}
          activeOpacity={0.5}
          style={styles.notiView}>
          <View style={{maxWidth: '80%'}}>
            <Text style={[typography.H6, {color: COLORS.dark_700}]}>
              Comment
            </Text>
            <Text
              style={[
                typography.Note2,
                {color: COLORS.altgreen_300, marginTop: 1},
              ]}>
              You will receive notifications when a member comments on your
              activity
            </Text>
          </View>
          <Icon name="Arrow_Right" size={16} color={COLORS.dark_600} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.handleModal('SHARING')}
          activeOpacity={0.5}
          style={styles.notiView}>
          <View style={{maxWidth: '80%'}}>
            <Text style={[typography.H6, {color: COLORS.dark_700}]}>
              Sharing
            </Text>
            <Text
              style={[
                typography.Note2,
                {color: COLORS.altgreen_300, marginTop: 1},
              ]}>
              You will receive notifications when a member shares your Post
            </Text>
          </View>
          <Icon name="Arrow_Right" size={16} color={COLORS.dark_600} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.handleModal('PROJECT CREATE')}
          activeOpacity={0.5}
          style={styles.notiView}>
          <View style={{maxWidth: '80%'}}>
            <Text style={[typography.H6, {color: COLORS.dark_700}]}>
              Project Create
            </Text>
            <Text
              style={[
                typography.Note2,
                {color: COLORS.altgreen_300, marginTop: 1},
              ]}>
              You will receive notifications when a member has posted a Project
            </Text>
          </View>
          <Icon name="Arrow_Right" size={16} color={COLORS.dark_600} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.handleModal('OTHER')}
          activeOpacity={0.5}
          style={styles.notiView}>
          <View style={{maxWidth: '80%'}}>
            <Text style={[typography.H6, {color: COLORS.dark_700}]}>
              Others
            </Text>
            <Text
              style={[
                typography.Note2,
                {color: COLORS.altgreen_300, marginTop: 1},
              ]}>
              You will receive notifications when members wish to engage with
              you
            </Text>
          </View>
          <Icon name="Arrow_Right" size={16} color={COLORS.dark_600} />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  notiView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 1.5,
    padding: 15,
    paddingLeft: 25,
    backgroundColor: COLORS.white,
  },
});
