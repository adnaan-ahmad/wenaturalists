import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  StyleSheet,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';

import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import typography from '../../../Components/Shared/Typography';
import AsyncStorage from '@react-native-community/async-storage';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class PrivacySettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        value && this.setState({userId: value});
      })
      .catch((err) => console.log(err));
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {/***** Header starts *****/}

        <View style={styles.headerView}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
          </TouchableOpacity>
          <Icon
            name="Setting"
            size={18}
            color={COLORS.dark_600}
            style={{marginLeft: 20}}
          />
          <Text
            style={[
              typography.Button_Lead,
              {
                color: COLORS.dark_600,
                marginLeft: 10,
                marginTop: Platform.OS === 'android' ? -8 : 0,
              },
            ]}>
            Privacy & Settings
          </Text>
        </View>

        {/***** Header ends *****/}

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('PrivacyVisibility', {
              id: this.state.userId,
            })
          }
          style={[styles.contentView, {marginTop: 12}]}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon
              name="Shield_Tick"
              size={20}
              color={COLORS.dark_600}
              style={{marginLeft: 20}}
            />
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_600,
                  marginLeft: 10,
                  marginTop: Platform.OS === 'android' ? -8 : 0,
                },
              ]}>
              Privacy Settings
            </Text>
          </View>
          <Icon
            name="Arrow_Right"
            size={20}
            color={COLORS.dark_600}
            style={{marginRight: 10}}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('AccountSettings')}
          style={styles.contentView}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon
              name="UserCircle"
              size={20}
              color={COLORS.dark_600}
              style={{marginLeft: 20}}
            />
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_600,
                  marginLeft: 10,
                  marginTop: Platform.OS === 'android' ? -8 : 0,
                },
              ]}>
              Account Settings
            </Text>
          </View>
          <Icon
            name="Arrow_Right"
            size={20}
            color={COLORS.dark_600}
            style={{marginRight: 10}}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('NotificationSettings', {
              userId: this.state.userId,
            })
          }
          style={styles.contentView}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon
              name="Bell"
              size={20}
              color={COLORS.dark_600}
              style={{marginLeft: 20}}
            />
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_600,
                  marginLeft: 10,
                  marginTop: Platform.OS === 'android' ? -8 : 0,
                },
              ]}>
              Notification Settings
            </Text>
          </View>
          <Icon
            name="Arrow_Right"
            size={20}
            color={COLORS.dark_600}
            style={{marginRight: 10}}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('EmailNotification', {
              userId: this.state.userId,
            })
          }
          style={styles.contentView}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon
              name="Envelope"
              size={20}
              color={COLORS.dark_600}
              style={{marginLeft: 20}}
            />
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_600,
                  marginLeft: 10,
                  marginTop: Platform.OS === 'android' ? -8 : 0,
                },
              ]}>
              Email Notification
            </Text>
          </View>
          <Icon
            name="Arrow_Right"
            size={20}
            color={COLORS.dark_600}
            style={{marginRight: 10}}
          />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headerView: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 54,
    marginBottom: 5,
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  contentView: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    marginTop: 4,
    justifyContent: 'space-between',
    height: 54,
    backgroundColor: COLORS.white,
  },
});
