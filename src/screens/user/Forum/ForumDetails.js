import React, {Component} from 'react';
import {
  Linking,
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
  ScrollView,
  Platform,
  PermissionsAndroid,
  Share,
  TextInput,
  Modal,
  Alert,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import Clipboard from '@react-native-community/clipboard';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {cloneDeep} from 'lodash';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import * as Progress from 'react-native-progress';
import DocumentPicker from 'react-native-document-picker';
import Snackbar from 'react-native-snackbar';
import {v4 as uuidv4} from 'uuid';
import LinearGradient from 'react-native-linear-gradient';

import Comment from '../../../Components/User/Common/Comment';
import Report from '../../../Components/User/Common/Report';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultShape from '../../../Components/Shared/Shape';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import ConnectDepth from '../../../Components/User/Common/ConnectDepth';
import LikedUserList from '../../../Components/User/Common/LikedUserList';
import SharedUserList from '../../../Components/User/Common/SharedUserList';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';

const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
  'i',
); // fragment locator
const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');

export default class ForumDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      forumData: {},
      commentData: [],
      commentAttachments: [],
      commentBody: '',
      page: 0,
      replyExpand: false,
      replyExpandId: '',
      reply: false,
      downloadStart: false,
      isDownloading: false,
      progressLimit: 0,
      shareModalOpen: false,
      likeModalOpen: false,
      userId: '',
      pressedActivityId: '',

      pressedId: '',
      peopleLiked: [],
      isReported: false,
      optionsModalOpen: false,
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      optionsModal2Open: false,
      edit: false,
      peopleShared: [],
      peopleSharedModalOpen: false,
      isCompany: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        value && this.setState({userId: value});
      })
      .catch((e) => {
        console.log(e);
      });

    AsyncStorage.getItem('userData').then((value) => {
      let objValue = JSON.parse(value);
      objValue.type === 'COMPANY' && this.setState({isCompany: true});
    });

    this.getForumDetails();

    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getForumDetails();
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  navigation = (value, params) => {
    this.setState({likeModalOpen: false, peopleSharedModalOpen: false}, () =>
      this.props.navigation.navigate(value, params),
    );
  };

  getUsersWhoShared = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/share/getUsers/' +
        this.state.forumData.id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        100,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          this.setState({peopleShared: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  peopleSharedModal = () => {
    return (
      <Modal
        visible={this.state.peopleSharedModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({peopleSharedModalOpen: false, peopleShared: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who Shared this forum
              </Text>
            </View>

            <SharedUserList
              id={this.state.forumData.id}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  trimDescription = (item) => {
    var trimmed = item.split('^^__').join(' ').indexOf('@@@__');

    var str = item
      .split('^^__')
      .join(' ')
      .indexOf(' ', item.split('^^__').join(' ').indexOf('@@@__'));

    var sub = item.substring(trimmed, str);

    item = item.replace(' ' + sub, '');
    item = item.replace('@@@^^^', ' ');
    item = item.replace('@@@__', ' ');
    item = item.replace('  ', '');

    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  documentPicker = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      for (const res of results) {
        // console.log(res);
        this.state.commentAttachments.push(res);
      }
      this.setState({...this.state});
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  downloadProgressWrapper = () => {
    return (
      <Modal
        isVisible={this.state.isDownloading}
        backdropColor={'#ffffff'}
        animationIn={'slideInUp'}
        animationInTiming={300}
        animationOut={'slideOutDown'}
        animationOutTiming={300}
        avoidKeyboard={true}>
        <View>
          {this.state.progressLimit == 1 ? null : (
            <Progress.Pie
              style={styles.downloadInProgress}
              progress={this.state.progressLimit}
              size={50}
              color={'green'}
            />
          )}
          {this.state.progressLimit == 1 ? (
            <Text style={styles.downloadInComplete}>
              File Download Successfully
            </Text>
          ) : null}
        </View>
      </Modal>
    );
  };

  readStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.writeStorage(data);
      } else {
        // Permission Denied
        alert('Read Storage Permission Denied');
      }
    } else {
      this.getDownloadFile(data); //work on ios
    }
  };

  writeStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.getDownloadFile(data);
      } else {
        // Permission Denied
        alert('Write Storage Permission Denied');
      }
    }
  };

  getDownloadFile = (urlDownload) => {
    if (this.state.downloadStart == true) {
      return;
    } else {
      this.setState({downloadStart: true});
    }

    let directoryFile;
    let dirs;

    let fileName;
    try {
      fileName = urlDownload.split('-').pop();
    } catch (e) {
      fileName = uuidv4();
    }
    if (Platform.OS === 'android') {
      directoryFile = RNFetchBlob.fs.dirs;
      dirs = directoryFile.DownloadDir + '/Download' + fileName;
    } else {
      directoryFile = RNFS.DocumentDirectoryPath;
      dirs = directoryFile + fileName;
    }

    try {
      RNFetchBlob.config({
        path: dirs,
      })
        .fetch('GET', urlDownload, {})
        .progress((received, total) => {
          let temp = parseFloat(received / total);

          if (temp > 0.9) {
            temp = 1;
            setTimeout(() => {
              this.setState({isDownloading: false});
            }, 1000);
          }
          this.setState(
            {
              isDownloading: true,
              progressLimit: temp,
            },
            () => this.downloadProgressWrapper(),
          );
        })
        .then((res) => {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'File downloaded successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });

          this.setState({
            ...this.state,
            downloadStart: false,
          });
          setTimeout(() => {
            this.setState({isDownloading: false});
          }, 1000);
        });
    } catch (error) {}
  };

  handleCommentSubmit = () => {
    const formData = new FormData();

    this.setState({commentBody: '', commentAttachments: []});

    let params = {
      userId: this.props.route.params.userId,
      activityId: this.state.forumData.id,
      userComment: this.state.commentBody.trim('\n'),
    };

    let replyParams = {
      userId: this.props.route.params.userId,
      activityId: this.state.replyExpandId,
      userComment: this.state.commentBody.trim('\n'),
      commentType: 'REPLY',
    };

    if (
      this.state.commentAttachments &&
      this.state.commentAttachments.length > 0
    ) {
      this.state.commentAttachments.forEach((file) => {
        formData.append('files', file);
      });
    }

    if (this.state.reply) {
      formData.append('data', JSON.stringify(replyParams));
    } else {
      formData.append('data', JSON.stringify(params));
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/comment/create',
      data: formData,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          this.setState({
            forumData: {
              ...this.state.forumData,
              commentCount: this.state.forumData.commentCount + 1,
            },
          });
          this.getComments(this.state.forumData.id);
        } else {
          console.log(response);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          REACT_APP_domainUrl + '/forumDetail/' + this.props.route.params.slug,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation.navigate('ShareForum', {
                    pressedActivityId: this.state.forumData.id,
                  }),
                );
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl +
                    '/forumDetail/' +
                    this.props.route.params.slug,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to forum
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                  <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Envelope"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  getForumDetails = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/forum/get?slug=' +
        this.props.route.params.slug +
        '&userId=' +
        this.props.route.params.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '200 OK') {
          this.setState({forumData: res.body});
          this.getComments(res.body.id);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getComments = (activityId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/comment/getCommentsByActivityId/' +
        activityId +
        '?userId=' +
        this.props.route.params.userId +
        '&page=' +
        this.state.page +
        '&size=1000',
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '200 OK') {
          this.setState({commentData: res.body.content});
        }
      })
      .catch((err) => {
        console.log('err in get comment : ', err);
      });
  };

  handleLike = (activityId, liked) => {
    const data = {
      userId: this.props.route.params.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.getForumDetails();
        } else {
          console.log(response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + 1 + ' s';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + ' m';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + ' h';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + ' d';
    }

    if (difference >= 864000) {
      return day + ' ' + month + ' ' + year;
    }
  };

  deleteComment = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/comment/delete?commentId=' +
        this.state.pressedActivityId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          console.log(res.status);
          this.getComments(this.state.forumData.id);
          // this.state.commentData.filter(item => this.setState({ commentData: item.id !== this.state.pressedActivityId }))
        }
      })
      .catch((err) => {
        console.log(err);
      });
    this.setState({optionsModal2Open: false, commentBody: ''});
  };

  handleCommentEdit = () => {
    let formData = {
      commentId: this.state.pressedActivityId,
      description: this.state.commentBody.trim(),
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/comment/edit',
      data: formData,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          // console.log(res.status)
          this.getComments(this.state.forumData.id);
        }
      })
      .catch((err) => {
        console.log(err);
      });
    this.setState({edit: false, commentBody: '', reply: false});
  };

  renderCommentItem = (item) => {
    let str = item.item.userComment.replace(/&nbsp;/g, ' ');
    return (
      <>
        <View style={styles.commentItemView}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                item.item.userType === 'INDIVIDUAL'
                  ? this.state.userId === item.item.userId
                    ? this.props.navigation.navigate('ProfileStack', {
                        screen: 'ProfileScreen',
                        // params: { userId: item.item.userId },
                      })
                    : this.props.navigation.navigate('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.item.userId},
                      })
                  : this.props.navigation.navigate('ProfileStack', {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.item.userId},
                    })
              }
              style={{flexDirection: 'row'}}>
              <Image
                source={
                  item.item.profileImage
                    ? {uri: item.item.profileImage}
                    : defaultProfile
                }
                style={defaultShape.Media_Round}
              />
              <Text
                numberOfLines={1}
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 6,
                    maxWidth: 120,
                    textTransform:
                      item.item.userType === 'COMPANY' ? 'none' : 'capitalize',
                  },
                ]}>
                {item.item.userName}
              </Text>
            </TouchableOpacity>

            <View style={{flexDirection: 'row'}}>
              <Text
                style={[
                  typography.Note,
                  {
                    color: COLORS.altgreen_300,
                    fontFamily: 'Montserrat-Medium',
                    marginTop: 5,
                  },
                ]}>
                {/* {moment.unix(item.item.time / 1000).format('Do MMM, YYYY')} */}
                {this.unixTime2(item.item.time)}
              </Text>
              <TouchableOpacity
                style={defaultShape.Nav_Gylph_Btn}
                onPress={() => {
                  this.setState(
                    {
                      optionsModal2Open: true,
                      pressedUserId: item.item.userId,
                      pressedActivityId: item.item.id,
                    },
                    () => this.verifyReported2(),
                  );
                  setTimeout(() => {
                    this.setState({
                      commentBody: str.replace(/<br\s*[\/]?>/gi, '\n'),
                    });
                  }, 500);
                }}>
                <Icon
                  name="Kebab"
                  color={COLORS.altgreen_400}
                  size={14}
                  style={{marginTop: -8}}
                />
              </TouchableOpacity>
            </View>
          </View>

          <Text style={[typography.Body_2, styles.commentBody]}>
            {str.replace(/<br\s*[\/]?>/gi, '\n')}
          </Text>

          {item.item.attachmentIds.length > 0 ? (
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{marginTop: 10, paddingLeft: 32, paddingRight: 30}}
              contentContainerStyle={{paddingRight: 40}}
              data={item.item.attachmentIds}
              keyExtractor={(attachmentItem) => attachmentItem.id}
              renderItem={(attachmentItem) => (
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {
                    this.readStorage(attachmentItem.item.attachmentUrl);
                  }}
                  style={[
                    defaultShape.ContextBtn_FL,
                    {paddingHorizontal: 5, marginRight: 10},
                  ]}>
                  {/* <Text style={[typography.Caption, { color: COLORS.dark_500 }]}>{attachmentItem.item.attachmentUrl.split("-").pop()}</Text> */}
                  <Image
                    source={{uri: attachmentItem.item.attachmentUrl}}
                    style={{width: 250, height: 160, borderRadius: 10}}
                  />
                </TouchableOpacity>
              )}
            />
          ) : (
            <></>
          )}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: 5,
            }}>
            {item.item.replies.length > 0 ? (
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    replyExpandId: item.item.id,
                    replyExpand: !this.state.replyExpand,
                  })
                }
                style={{flexDirection: 'row', marginLeft: 38}}>
                <View
                  style={{
                    backgroundColor: COLORS.altgreen_300,
                    height: 14,
                    width: 14,
                    borderRadius: 7,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginRight: 5,
                  }}>
                  <Icon
                    name="Arrow_Down"
                    color={COLORS.white}
                    size={10}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                </View>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-Medium',
                    },
                  ]}>
                  {item.item.replies.length}{' '}
                  {item.item.replies.length > 1 ? 'replies' : 'reply'}
                </Text>
              </TouchableOpacity>
            ) : (
              <View></View>
            )}

            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    reply: true,
                    replyExpand: true,
                    replyExpandId: item.item.id,
                  });
                  this.textInputField.focus();
                }}
                style={[
                  defaultShape.Nav_Gylph_Btn,
                  {flexDirection: 'row', marginRight: -10, minWidth: 70},
                ]}>
                <Icon name="Reply" color={COLORS.altgreen_300} size={14} />
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.altgreen_300,
                      marginLeft: 5,
                      marginTop: Platform.OS === 'ios' ? 0 : -8,
                    },
                  ]}>
                  Reply
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleLike(item.item.id, item.item.liked)}
                style={defaultShape.Nav_Gylph_Btn}>
                <Icon
                  name={item.item.liked ? 'Like_FL' : 'Like'}
                  color={COLORS.green_500}
                  size={14}
                  style={{marginTop: 3}}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        {this.state.replyExpandId === item.item.id &&
        this.state.replyExpand === true ? (
          <View>
            <FlatList
              // style={{ marginTop: this.state.setreadmore ? 0 : 10 }}
              // horizontal
              // showsHorizontalScrollIndicator={false}
              data={item.item.replies}
              keyExtractor={(rep) => rep.id}
              renderItem={(rep) => this.renderReplyItem(rep)}
            />
          </View>
        ) : (
          <></>
        )}
      </>
    );
  };

  renderReplyItem = (item) => {
    let str = item.item.userComment.replace(/&nbsp;/g, ' ');
    return (
      <View style={{backgroundColor: COLORS.grey_300}}>
        <View
          style={[
            styles.commentItemView,
            {
              backgroundColor: COLORS.bgfill,
              width: '95%',
              alignSelf: 'flex-end',
            },
          ]}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                item.item.userType === 'INDIVIDUAL'
                  ? this.state.userId === item.item.userId
                    ? this.props.navigation.navigate('ProfileStack', {
                        screen: 'ProfileScreen',
                        // params: { userId: item.item.userId },
                      })
                    : this.props.navigation.navigate('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.item.userId},
                      })
                  : this.props.navigation.navigate('ProfileStack', {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.item.userId},
                    })
              }
              style={{flexDirection: 'row'}}>
              <Image
                source={
                  item.item.profileImage
                    ? {uri: item.item.profileImage}
                    : defaultProfile
                }
                style={defaultShape.Media_Round}
              />
              <Text
                numberOfLines={1}
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 6,
                    maxWidth: 120,
                    textTransform:
                      item.item.userType === 'COMPANY' ? 'none' : 'capitalize',
                  },
                ]}>
                {item.item.userName}
              </Text>
            </TouchableOpacity>

            <View style={{flexDirection: 'row'}}>
              <Text
                style={[
                  typography.Note,
                  {
                    color: COLORS.altgreen_300,
                    fontFamily: 'Montserrat-Medium',
                    marginTop: 5,
                  },
                ]}>
                {/* {moment.unix(item.item.time / 1000).format('Do MMM, YYYY')} */}
                {this.unixTime2(item.item.time)}
              </Text>
              <TouchableOpacity
                style={defaultShape.Nav_Gylph_Btn}
                onPress={() => {
                  this.setState(
                    {
                      optionsModal2Open: true,
                      pressedUserId: item.item.userId,
                      pressedActivityId: item.item.id,
                    },
                    () => this.verifyReported2(),
                  );
                  setTimeout(() => {
                    this.setState({
                      commentBody: str.replace(/<br\s*[\/]?>/gi, '\n'),
                    });
                  }, 500);
                }}>
                <Icon
                  name="Kebab"
                  color={COLORS.altgreen_400}
                  size={14}
                  style={{marginTop: -8}}
                />
              </TouchableOpacity>
            </View>
          </View>

          <Text style={[typography.Body_2, styles.commentBody]}>
            {str.replace(/<br\s*[\/]?>/gi, '\n')}
          </Text>

          {item.item.attachmentIds.length > 0 ? (
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{marginTop: 10, paddingLeft: 32, paddingRight: 30}}
              contentContainerStyle={{paddingRight: 40}}
              data={item.item.attachmentIds}
              keyExtractor={(attachmentItem) => attachmentItem.id}
              renderItem={(attachmentItem) => (
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {
                    this.readStorage(attachmentItem.item.attachmentUrl);
                  }}
                  style={[
                    defaultShape.ContextBtn_FL,
                    {paddingHorizontal: 5, marginRight: 10},
                  ]}>
                  {/* <Text style={[typography.Caption, { color: COLORS.dark_500 }]}>{attachmentItem.item.attachmentUrl.split("-").pop()}</Text> */}
                  <Image
                    source={{uri: attachmentItem.item.attachmentUrl}}
                    style={{width: 250, height: 160, borderRadius: 10}}
                  />
                </TouchableOpacity>
              )}
            />
          ) : (
            <></>
          )}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: 5,
            }}>
            <View></View>

            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({reply: true, replyExpand: true});
                  this.textInputField.focus();
                }}
                style={[
                  defaultShape.Nav_Gylph_Btn,
                  {flexDirection: 'row', marginRight: -10, minWidth: 70},
                ]}>
                <Icon name="Reply" color={COLORS.altgreen_300} size={14} />
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.altgreen_300,
                      marginLeft: 5,
                      marginTop: Platform.OS === 'ios' ? 0 : -8,
                    },
                  ]}>
                  Reply
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleLike(item.item.id, item.item.liked)}
                style={defaultShape.Nav_Gylph_Btn}>
                <Icon
                  name={item.item.liked ? 'Like_FL' : 'Like'}
                  color={COLORS.green_500}
                  size={14}
                  style={{marginTop: 3}}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.forumData.id,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  verifyReported2 = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedActivityId,
      // entityType: this.state.entityType,
      entityType: 'FORUM',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    setTimeout(() => {
      this.setState({reasonForReporting: '', description: ''});
    }, 1000);
  };

  changeState = (value) => {
    this.setState(value);
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <Report
          changeState={this.changeState}
          entityId={this.state.pressedActivityId}
          entityType="FORUM"
        />
      </Modal>
    );
  };

  handlePinnedForumAndPoll = () => {
    let tempForumData = cloneDeep(this.state.forumData);
    let pinnedBody = {
      userId: this.state.userId,
      entityId: this.state.forumData.id,
      entityType: 'FORUM',
      pinned: !this.state.forumData.pinned,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/pinned/create',
      headers: {'Content-Type': 'application/json'},
      data: pinnedBody,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          console.log(res.message);
          tempForumData.pinned = !this.state.forumData.pinned;
          this.setState({forumData: tempForumData});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleHideModal = () => {
    let data = {
      userId: this.state.userId,
      activityId: this.state.forumData.id,
      entityType: 'FORUM',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          console.log('response.status', response.status);
          this.props.navigation.goBack();
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          console.log('response.status', err.response.status);
          this.props.navigation.goBack();
        }
      });
    // this.setState({ optionsModalOpen: false, forumData: this.state.forumData.filter((item) => item.id !== id) })
  };

  deleteForum = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/forum/delete?forumId=' +
        this.state.forumData.id,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          console.log('response.status', res.status);
          this.props.navigation.goBack();
        }
      })
      .catch((err) => {
        console.log('response.status', err.response.data.message);
        this.props.navigation.goBack();
      });
    // this.setState({ optionsModalOpen: false, forumData: this.state.forumData.filter((item) => item.id !== this.state.pressedActivityId) })
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({optionsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          {this.state.userId !== this.state.forumData.userId ? (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
                ]}>
                <Text
                  style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                  Forum
                </Text>
              </View>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {optionsModalOpen: false, likeModalOpen: true},
                    () => this.getUsersWhoLiked(),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Like_FL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who liked it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {optionsModalOpen: false, peopleSharedModalOpen: true},
                    () => this.getUsersWhoShared(),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Share"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who shared it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              {!this.state.forumData.pinned && (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {paddingVertical: 15},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.handleHideModal();
                  }}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    Hide
                  </Text>
                  <Icon
                    name="Hide"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                </TouchableOpacity>
              )}

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    reasonForReportingModalOpen: !this.state.isReported
                      ? true
                      : false,
                    optionsModalOpen: false,
                  });
                  this.state.isReported
                    ? setTimeout(() => {
                        Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Your report request was already taken',
                          duration: Snackbar.LENGTH_LONG,
                        });
                      }, 500)
                    : null;
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Report
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({optionsModalOpen: false}, () =>
                    this.handlePinnedForumAndPoll(),
                  )
                }
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  {this.state.forumData && this.state.forumData.pinned
                    ? 'Unpin'
                    : 'Pin'}
                </Text>
                <Icon
                  name="Pin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
                ]}>
                <Text
                  style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                  Forum
                </Text>
              </View>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {optionsModalOpen: false, likeModalOpen: true},
                    () => this.getUsersWhoLiked(),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Like_FL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who liked it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {optionsModalOpen: false, peopleSharedModalOpen: true},
                    () => this.getUsersWhoShared(),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Share"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who shared it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              {!this.state.forumData.pinned && (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {paddingVertical: 15},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.handleHideModal();
                  }}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    Hide
                  </Text>
                  <Icon
                    name="Hide"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                </TouchableOpacity>
              )}

              <TouchableOpacity
                onPress={this.deleteForumAlert}
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Delete
                </Text>
                <Icon
                  name="TrashBin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({optionsModalOpen: false}, () =>
                    this.handlePinnedForumAndPoll(),
                  )
                }
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  {this.state.forumData && this.state.forumData.pinned
                    ? 'Unpin'
                    : 'Pin'}
                </Text>
                <Icon
                  name="Pin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  };

  getUsersWhoLiked = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/like/getUsers/' +
        this.state.forumData.id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          // console.log('%%%%%%%%%%%%%%%% response.data.body %%%%%%%%%%%%%%%%%%%', response.data.body.content[0])
          this.setState({peopleLiked: response.data.body.content});
        }
      })
      .catch((err) => {
        // console.log(err)
      });
  };

  likeModal = () => {
    return (
      <Modal
        visible={this.state.likeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({likeModalOpen: false, peopleLiked: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who liked this forum
              </Text>
            </View>

            <LikedUserList
              id={this.state.forumData.id}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  optionsModal2 = () => {
    return (
      <Modal
        visible={this.state.optionsModal2Open}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({optionsModal2Open: false, commentBody: ''})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          {this.state.userId !== this.state.pressedUserId ? (
            <View style={defaultShape.Modal_Categories_Container}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    reasonForReportingModalOpen: !this.state.isReported
                      ? true
                      : false,
                    optionsModal2Open: false,
                    commentBody: '',
                  });
                  this.state.isReported
                    ? setTimeout(() => {
                        Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Your report request was already taken',
                          duration: Snackbar.LENGTH_LONG,
                        });
                      }, 500)
                    : null;
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Report
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={defaultShape.Modal_Categories_Container}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({optionsModal2Open: false, edit: true});
                  this.textInputField.focus();
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Edit
                </Text>
                <Icon
                  name="EditBox"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.setState({commentBody: ''}), this.createTwoButtonAlert();
                }}
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Delete
                </Text>
                <Icon
                  name="TrashBin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  };

  createTwoButtonAlert = () => {
    this.setState({optionsModal2Open: false});
    Alert.alert('', 'Are you sure you want to delete this comment?', [
      {
        text: 'YES',
        onPress: () => this.deleteComment(),
        style: 'cancel',
      },
      {
        text: 'NO',
        onPress: () => this.setState({commentBody: ''}),
      },
    ]);
  };

  deleteForumAlert = () => {
    this.setState({optionsModalOpen: false});
    Alert.alert('', 'Are you sure you want to delete this forum?', [
      {
        text: 'YES',
        onPress: () => this.deleteForum(),
        style: 'cancel',
      },
      {
        text: 'NO',
        onPress: () => console.log('user cancelled deletion'),
      },
    ]);
  };

  openWebsite = (website) => {
    if (website && website.includes('http')) return Linking.openURL(website);
    else if (website && !website.includes('http'))
      return Linking.openURL('https://' + website);
  };

  handleFollowUnfollow = (isFollowed, userId, item) => {
    // let tempLikeList = cloneDeep(this.state.peopleLiked);
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        console.log(response.status);
        if (response && response.status === 202) {
          this.setState({}, () => this.getForumDetails());
          // tempLikeList[index].followed = !item.followed;
          // this.setState({ peopleLiked: tempLikeList });
        } else {
          console.log(response);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  navigateToProfile = (item) => {
    return item.userType === 'INDIVIDUAL'
      ? this.state.userId === item.userId
        ? this.props.navigation.navigate('ProfileStack', {
            screen: 'ProfileScreen',
            // params: { userId: item.userId },
          })
        : this.props.navigation.navigate('ProfileStack', {
            screen: 'OtherProfileScreen',
            params: {userId: item.userId},
          })
      : this.props.navigation.navigate('ProfileStack', {
          screen: 'CompanyProfileScreen',
          params: {userId: item.userId},
        });
  };

  render() {
    let {forumData} = this.state;
    console.log('forumData', this.state.forumData);
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.shareModal()}
        {this.optionsModal()}
        {this.reasonForReportingModal()}
        {this.likeModal()}
        {this.optionsModal2()}
        {this.peopleSharedModal()}

        {/*********** Header starts ***********/}
        <View
          style={
            Platform.OS === 'ios'
              ? [styles.header, {paddingVertical: 15}]
              : styles.header
          }>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              style={{
                width: 40,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="Arrow-Left"
                size={15}
                color="#91B3A2"
                style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
              />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                this.props.route.params.userId === forumData.userId
                  ? this.props.navigation.navigate('ProfileStack', {
                      screen: 'ProfileScreen',
                      // params: { userId: forumData.userId },
                    })
                  : this.props.navigation.navigate('ProfileStack', {
                      screen: 'OtherProfileScreen',
                      params: {userId: forumData.userId},
                    })
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                source={
                  forumData.profileImage
                    ? {uri: forumData.profileImage}
                    : forumData.userType === 'COMPANY'
                    ? defaultBusiness
                    : defaultProfile
                }
                style={styles.profileImage}
              />

              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text
                    numberOfLines={1}
                    style={[
                      typography.Subtitle_1,
                      {
                        color: COLORS.dark_500,
                        marginLeft: 5,
                        maxWidth: 220,
                        textTransform:
                          forumData.userType === 'COMPANY'
                            ? 'none'
                            : 'capitalize',
                      },
                    ]}>
                    {forumData.userName}
                  </Text>

                  {!forumData.deactivated &&
                  forumData.userId !== this.state.userId &&
                  !this.state.isCompany ? (
                    <TouchableOpacity
                      onPress={() =>
                        this.handleFollowUnfollow(
                          forumData.followed,
                          forumData.userId,
                          // index,
                          forumData,
                        )
                      }
                      style={{
                        height: 30,
                        width: 30,
                        alignItems: 'center',
                        marginLeft: 4,
                      }}>
                      <Icon
                        name={forumData.followed ? 'TickRSS' : 'RSS'}
                        size={13}
                        color={forumData.followed ? COLORS.dark_600 : COLORS.green_600}
                        style={{marginTop: Platform.OS === 'android' ? 4 : 14}}
                      />
                    </TouchableOpacity>
                  ) : null}
                </View>
                <Text
                  style={[
                    typography.Note2,
                    {
                      color: '#888',
                      marginLeft: 5,
                    },
                  ]}>
                  Published on{' '}
                  {moment
                    .unix(forumData.createTime / 1000)
                    .format('Do MMM, YYYY')}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        {/*********** Header ends ***********/}

        <ScrollView keyboardShouldPersistTaps="handled">
          <Text style={[typography.Title_2, styles.title]}>
            {forumData.title}
          </Text>
          <View style={styles.forumItemView}>
            <TouchableOpacity
              style={{
                width: 26,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'flex-end',
              }}
              onPress={() =>
                this.setState(
                  {
                    optionsModalOpen: true,
                    pressedActivityId: forumData.id,
                    pressedUserId: forumData.userId,
                  },
                  () =>
                    forumData.userId !== this.state.userId &&
                    this.verifyReported(),
                )
              }>
              <Icon
                name="Kebab"
                size={14}
                color="#91B3A2"
                style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
              />
            </TouchableOpacity>

            <Text style={[typography.Caption, styles.subtitle]}>
              BASIC DESCRIPTION
            </Text>

            {forumData.comment && regexp.test(forumData.comment) ? (
              <Text
                numberOfLines={3}
                onPress={() => this.openWebsite(forumData.comment)}
                style={[
                  typography.Title_2,
                  styles.title,
                  {
                    color: '#4068eb',
                    marginTop: 2,
                    fontSize: 13,
                    marginHorizontal: 0,
                  },
                ]}>
                {forumData.comment}
              </Text>
            ) : (
              <Text style={[typography.Body_1, styles.body]}>
                {forumData.comment && this.trimDescription(forumData.comment)}
              </Text>
            )}

            <FlatList
              style={{marginTop: 10}}
              horizontal
              showsHorizontalScrollIndicator={false}
              data={forumData.hashTags}
              keyExtractor={(item) => item}
              renderItem={(item) => (
                <Text
                  style={[
                    typography.Subtitle_2,
                    {color: '#4068eb', marginRight: 8},
                  ]}>
                  #{item.item}
                </Text>
              )}
            />

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
                marginBottom: 5,
                alignItems: 'center',
              }}>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() => this.handleLike(forumData.id, forumData.liked)}
                  activeOpacity={0.5}
                  style={{alignItems: 'center', flexDirection: 'row'}}>
                  <Icon
                    name={forumData.liked ? 'Like_FL' : 'Like'}
                    color={COLORS.green_500}
                    size={14}
                    style={{
                      marginTop: Platform.OS === 'android' ? 10 : 0,
                      marginHorizontal: 6,
                    }}
                  />
                </TouchableOpacity>

                <Text
                  onPress={() => this.handleLike(forumData.id, forumData.liked)}
                  style={[
                    {
                      color: COLORS.altgreen_300,
                      marginTop: Platform.OS === 'ios' ? 0 : 12,
                    },
                    typography.Caption,
                  ]}>
                  {forumData.likesCount}{' '}
                  {forumData.likesCount === 0 || forumData.likesCount === 1
                    ? 'Like'
                    : 'Likes'}
                </Text>
              </View>
              <TouchableOpacity
                activeOpacity={0.5}
                style={{flexDirection: 'row', marginTop: 10}}>
                <Text
                  onPress={() => this.setState({shareModalOpen: true})}
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.grey_350,
                      marginRight: 5,
                      marginTop: Platform.OS === 'ios' ? -2 : 2,
                    },
                  ]}>
                  {forumData.sharesCount ? forumData.sharesCount : null} Share
                </Text>
                <Icon
                  onPress={() => {
                    this.setState({shareModalOpen: true});
                  }}
                  name="Share"
                  color={COLORS.grey_350}
                  size={14}
                />
              </TouchableOpacity>
            </View>
          </View>

          {!this.props.route.params.hidden && (
            <TouchableOpacity
              activeOpacity={0.8}
              style={[
                defaultShape.ContextBtn_FL_Drk,
                {
                  borderRadius: 6,
                  paddingHorizontal: 10,
                  alignSelf: 'center',
                  marginTop: 10,
                  marginBottom: 16,
                },
              ]}
              onPress={() => this.props.navigation.goBack()}>
              <Text
                style={[
                  typography.Caption,
                  {color: COLORS.altgreen_200, paddingVertical: 2},
                ]}>
                View Source
              </Text>
            </TouchableOpacity>
          )}

          {/****** Comments start ******/}

          <Comment
            id={this.state.forumData.id}
            commentData={this.state.commentData}
            commentCount={this.state.commentData.length}
            navigateToProfile={this.navigateToProfile}
          />

          {/****** Comments ends ******/}
        </ScrollView>

        {/****** Comment Box start ******/}

        {/****** Comment Box ends ******/}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  forumItemView: {
    width: '94%',
    marginTop: 15,
    backgroundColor: COLORS.white,
    borderRadius: 4,
    alignSelf: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
    marginBottom: 12,
  },
  profileImage: {
    height: 30,
    width: 30,
    borderRadius: 15,
    marginRight: 8,
  },
  title: {
    marginTop: 15,
    color: COLORS.dark_800,
    marginHorizontal: 15,
  },
  subtitle: {
    color: COLORS.dark_600,
    marginTop: 5,
  },
  body: {
    color: COLORS.altgreen_400,
    marginTop: 5,
  },
  commentCountBar: {
    backgroundColor: COLORS.altgreen_100,
    paddingVertical: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  commentItemView: {
    paddingLeft: 15,
    backgroundColor: COLORS.white,
    paddingTop: 12,
    borderBottomColor: COLORS.altgreen_400,
    borderBottomWidth: 0.2,
  },
  commentBody: {
    color: COLORS.altgreen_400,
    marginTop: -25,
    marginLeft: 38,
    maxWidth: '80%',
  },
  commentBoxView: {
    position: 'absolute',
    bottom: 0,
    paddingVertical: 6,
    backgroundColor: COLORS.altgreen_200,
    width: '100%',
    zIndex: 1,
  },
  inputBox: {
    marginLeft: 10,
    width: '80%',
    height: 40,
    backgroundColor: COLORS.white,
    borderRadius: 6,
    paddingHorizontal: 10,
  },
});
