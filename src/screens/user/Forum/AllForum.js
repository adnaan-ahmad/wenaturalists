import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
  Modal,
  ScrollView,
  Platform,
  Linking,
  RefreshControl,
  Share,
  Alert,
  KeyboardAvoidingView,
} from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {cloneDeep, set} from 'lodash';
import {TextInput} from 'react-native-paper';
import DatePicker from 'react-native-datepicker';
import {connect} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import Snackbar from 'react-native-snackbar';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-crop-picker';
import {
  actions,
  getContentCSS,
  RichEditor,
  RichToolbar,
} from 'react-native-pell-rich-editor';

import SuggestedHashTags from '../../../Components/User/Common/SuggestedHashTags';
import Report from '../../../Components/User/Common/Report';
import defaultStyle from '../../../Components/Shared/Typography';
import {personalProfileRequest} from '../../../services/Redux/Actions/User/PersonalProfileActions';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultProfile from '../../../../assets/defaultProfile.png';
import DefaultBusiness from '../../../../assets/DefaultBusiness.png';
import defaultShape from '../../../Components/Shared/Shape';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import ForumHeader from '../../../Components/User/Forum/ForumHeader';
import SkeletonLoader from '../../../Components/Shared/SkeletonLoader';
import AddHashTags from '../../../Components/User/Common/AddHashTags';


const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');
const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
  'i',
); // fragment locator
class AllForum extends Component {
  richText = React.createRef();
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      selectedTab: '',
      searchParam: '',
      page: 0,
      forumData: [],
      createForumModalOpen: false,
      optionsModalOpen: false,
      startDate: 0,
      endDate: 0,
      hashTags: [],
      keyPressed: '',
      forumTitle: '',
      writeSomething: '',
      imageContent: '',
      optionOne: '',
      optionTwo: '',
      infoModal: false,
      selectedTrending: false,
      draftId: '',
      publishButton: true,
      pressedActivityId: '',
      pressedUserId: '',
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      isReported: false,
      seconds: 59,
      addHashTagsModalOpen: false,
      pressedItem: {},
      shareModalOpen: false,
      isLoading: false,
      userData: {},
      suggestedHashTagsModalOpen: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      value &&
        this.setState({userId: value}, () => {
          this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.onRefresh();
          });
        });
      value && this.getForumList(value, '', '');
    });

    AsyncStorage.getItem('userData').then((value) => {
      console.log('userData', JSON.parse(value));
      this.setState({userData: JSON.parse(value)});
    });
  }

  editorInitializedCallback() {
    this.richText.current?.registerToolbar(function (items) {
      // console.log('Toolbar click, selected items (insert end callback):', items);
    });
  }

  componentDidUpdate() {
    if (this.state.seconds === 0 && this.state.createForumModalOpen) {
      this.saveAsDraft();
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    this._unsubscribe();
  }

  changeHashTagsState = (value) => {
    this.setState(value);
  };

  addHashTagsModal = () => {
    return (
      <Modal
        visible={this.state.addHashTagsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <AddHashTags
          changeHashTagsState={this.changeHashTagsState}
          hashTags={this.state.hashTags}
          currentOpen="FORUM"
        />
      </Modal>
    );
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  changeState = (item) => {
    this.setState(item, () =>
      this.getForumList(
        this.state.userId,
        this.state.selectedTab,
        this.state.searchParam,
      ),
    );
  };

  handleLoadMore = () => {
    this.setState({page: this.state.page + 1}, () =>
      this.getForumList(
        this.state.userId,
        this.state.selectedTab,
        this.state.searchParam,
        true,
      ),
    );
  };

  trimDescription = (item) => {
    var trimmed = item.split('^^__').join(' ').indexOf('@@@__');

    var str = item
      .split('^^__')
      .join(' ')
      .indexOf(' ', item.split('^^__').join(' ').indexOf('@@@__'));

    var sub = item.substring(trimmed, str);

    item = item.replace(' ' + sub, '');
    item = item.replace('@@@^^^', ' ');
    item = item.replace('@@@__', ' ');
    item = item.replace('  ', '');

    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  getForumList = (userId, showType, searchParam, shouldShowLoader) => {
    !shouldShowLoader && this.setState({isLoading: true});
    axios({
      method: 'get',
      url: this.state.selectedTrending
        ? REACT_APP_userServiceURL +
          '/forum/trending?userId=' +
          userId +
          '&page=' +
          this.state.page +
          '&size=10'
        : REACT_APP_userServiceURL +
          '/forum/list?showType=' +
          showType +
          '&filterType=Forum&userId=' +
          userId +
          '&searchParam=' +
          searchParam +
          '&page=' +
          this.state.page +
          '&size=10',
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          this.setState({
            forumData: this.state.forumData.concat(res.body.content),
            isLoading: false,
          });
        }
      })
      .catch((err) => {
        this.setState({isLoading: false});
        console.log(err);
      });
  };

  handlePinnedForumAndPoll = (item) => {
    let tempForumData = cloneDeep(this.state.forumData);
    let pinnedBody = {
      userId: this.state.userId,
      entityId: item.item.id,
      entityType: 'FORUM',
      pinned: !item.item.pinned,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/pinned/create',
      headers: {'Content-Type': 'application/json'},
      data: pinnedBody,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          tempForumData[item.index].pinned = !item.item.pinned;
          this.setState({forumData: tempForumData});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleFollowUnfollow = (isFollowed, userId, index) => {
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          console.log(response.status);
          let tempForumData = cloneDeep(this.state.forumData);
          for (let i of tempForumData) {
            if (i.userId === userId) {
              console.log(i.followed, !isFollowed);
              i.followed = !isFollowed;
            }
          }
          this.setState({forumData: tempForumData});
        } else {
          console.log(response);
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }

  openWebsite = (website) => {
    if (website && website.includes('http')) return Linking.openURL(website)
    else if (website && !website.includes('http'))
      return Linking.openURL('https://' + website)
  }

  renderForumItem = (item) => {
    return (
      <TouchableOpacity
        onPress={() => 
          // console.log(item.item)
          this.props.navigation.navigate('ForumDetails', {
            slug: item.item.slug,
            userId: this.state.userId,
          })
        }
        activeOpacity={0.5}
        style={styles.forumItemView}>
        <Text style={[typography.Note2, {color: COLORS.altgreen_400}]}>
          Published on{' '}
          {moment.unix(item.item.createTime / 1000).format('Do MMM, YYYY')}
        </Text>
        <Text
          style={[typography.Title_2, {color: COLORS.dark_800, marginTop: 10}]}
          numberOfLines={1}>
          {item.item.title}
        </Text>

        {item.item.comment && regexp.test(item.item.comment) ? (
          <Text
            numberOfLines={3}
            onPress={() => this.openWebsite(item.item.comment)}
            style={[
              typography.Title_2,
              styles.title,
              {color: '#4068eb', marginTop: 2, fontSize: 13},
            ]}>
            {this.trimDescription(item.item.comment)}
            {/* dcdcdcccc */}
          </Text>
        ) : (
          <Text
            style={[typography.Body_2, {color: COLORS.dark_500, marginTop: 5}]}
            numberOfLines={3}>
            {this.trimDescription(item.item.comment)}
            {/* dcdcdcccc */}
          </Text>
        )}

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 10
          }}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              this.state.userId === item.item.userId
                ? this.props.navigation.navigate('ProfileStack', {
                    screen: 'ProfileScreen',
                    // params: { userId: item.item.userId },
                  })
                : this.props.navigation.navigate('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: {userId: item.item.userId},
                  })
            }
            style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              source={
                item.item.originalProfileImage
                  ? {uri: item.item.originalProfileImage}
                  : item.item.userType === 'COMPANY'
                  ? DefaultBusiness
                  : defaultProfile
              }
              style={styles.profileImage}
            />
            <Text
              numberOfLines={1}
              style={[
                typography.Caption,
                {
                  color: COLORS.dark_600,
                  marginTop: 2,
                  maxWidth: 120,
                  textTransform:
                    item.item.userType === 'COMPANY' ? 'none' : 'capitalize',
                },
              ]}>
              {item.item.userName}
            </Text>

            {!item.item.deactivated && (
              <Text
                onPress={() =>
                  item.item.connectDepth === -1 ||
                  (item.item.connectDepth === 0 &&
                    this.state.userData.type &&
                    this.state.userData.type !== 'COMPANY')
                    ? this.handleFollowUnfollow(
                        item.item.followed,
                        item.item.userId,
                        item.index,
                      )
                    : null
                }
                style={[
                  {
                    color:
                      (item.item.connectDepth === -1 ||
                        item.item.connectDepth === 0) &&
                      !item.item.followed
                        ? '#97a600'
                        : '#888',
                    fontSize: 14,
                    marginLeft: 4,
                    marginTop: 4,
                  },
                  typography.Note2,
                ]}>
                {item.userConnectStatus &&
                item.userConnectStatus.connectStatus === 'PENDING_CONNECT'
                  ? 'Pending'
                  : item.item.connectDepth === 1
                  ? '• 1st'
                  : item.item.connectDepth === 2
                  ? '• 2nd'
                  : ''}
              </Text>
            )}

            {this.state.userId !== item.item.userId &&
            this.state.userData.type &&
            this.state.userData.type !== 'COMPANY' &&
            !item.item.deactivated ? (
              <TouchableOpacity
                style={{height: 30, width: 30, alignItems: 'center'}}
                onPress={() =>
                  this.handleFollowUnfollow(
                    item.item.followed,
                    item.item.userId,
                    item.index,
                  )
                }>
                <Icon
                  name={item.item.followed ? 'TickRSS' : 'RSS'}
                  size={12}
                  color={
                    item.item.followed ? COLORS.dark_600 : COLORS.green_600
                  }
                  style={{
                    marginTop: Platform.OS === 'android' ? 6 : 10,
                    marginLeft: 4,
                  }}
                />
              </TouchableOpacity>
            ) : (
              <></>
            )}

            {item.item.userConnectStatus &&
            item.item.userConnectStatus.connectStatus === 'PENDING_CONNECT' ? (
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  onPress={() =>
                    this.props.navigation.navigate('NetworkInvitationStack')
                  }
                  name="FollowTick"
                  size={12}
                  color="#888"
                  style={{
                    marginTop: Platform.OS === 'android' ? 12 : 3,
                    marginLeft: 4,
                  }}
                />
                <Text
                  onPress={() =>
                    this.props.navigation.navigate('NetworkInvitationStack')
                  }
                  style={[
                    {
                      color: '#888',
                      fontSize: 14,
                      marginLeft: 4,
                      marginTop: 4,
                    },
                    typography.Note2,
                  ]}>
                  {item.item.userConnectStatus &&
                  item.item.userConnectStatus.connectStatus ===
                    'PENDING_CONNECT'
                    ? 'Pending'
                    : ''}
                </Text>
              </View>
            ) : (
              <></>
            )}
          </TouchableOpacity>

          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              style={defaultShape.Nav_Gylph_Btn}
              onPress={() =>
                this.setState(
                  {
                    pressedItem: item,
                    optionsModalOpen: true,
                    pressedActivityId: item.item.id,
                    pressedUserId: item.item.userId,
                  },
                  () =>
                    this.state.pressedUserId !== this.state.userId &&
                    this.verifyReported(),
                )
              }>
              <Icon
                name="Meatballs"
                size={15}
                color="#91B3A2"
                style={
                  (Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8},
                  {marginRight: -15})
                }
              />
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  handleHideModal = (id) => {
    let data = {
      userId: this.state.userId,
      activityId: id,
      entityType: 'FORUM',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          console.log('response.status', response.status);
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          console.log('response.status', err.response.status);
        }
      });
    this.setState({
      optionsModalOpen: false,
      forumData: this.state.forumData.filter((item) => item.id !== id),
    });
  };

  deleteForum = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/forum/delete?forumId=' +
        this.state.pressedActivityId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          console.log('response.status', res.status);
        }
      })
      .catch((err) => {
        console.log('response.status', err.response.data.message);
      });
    this.setState({
      optionsModalOpen: false,
      forumData: this.state.forumData.filter(
        (item) => item.id !== this.state.pressedActivityId,
      ),
    });
  };

  changeState2 = (value) => {
    this.setState(value);
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <Report
          changeState={this.changeState2}
          entityId={this.state.pressedActivityId}
          entityType="FORUM"
        />
      </Modal>
    );
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedActivityId,
      // entityType: this.state.entityType,
      entityType: 'FORUM',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    setTimeout(() => {
      this.setState({
        reasonForReporting: 'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT',
        description: '',
      });
    }, 1000);
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          REACT_APP_domainUrl +
          '/forumDetail/' +
          this.state.pressedItem.item.slug,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation.navigate('ShareForum', {
                    pressedActivityId: this.state.pressedActivityId,
                  }),
                );
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl +
                    '/forumDetail/' +
                    this.state.pressedItem.item.slug,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
                setTimeout(() => {
                  this.setState({shareModalOpen: false});
                }, 2000);
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to forum
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                  <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Envelope"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({optionsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          {this.state.userId !== this.state.pressedUserId ? (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
                ]}>
                <Text
                  style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                  Forum
                </Text>
              </View>

              {this.state.selectedTab !== 'PINNED' &&
              this.state.pressedItem &&
              this.state.pressedItem.item &&
              !this.state.pressedItem.item.pinned ? (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {paddingVertical: 15},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.handleHideModal(this.state.pressedActivityId);
                  }}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    Hide
                  </Text>
                  <Icon
                    name="Hide"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                </TouchableOpacity>
              ) : null}

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    reasonForReportingModalOpen: !this.state.isReported
                      ? true
                      : false,
                    optionsModalOpen: false,
                  });
                  this.state.isReported
                    ? setTimeout(() => {
                        Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Your report request was already taken',
                          duration: Snackbar.LENGTH_LONG,
                        });
                      }, 500)
                    : null;
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Report
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({optionsModalOpen: false}, () =>
                    this.handlePinnedForumAndPoll(this.state.pressedItem),
                  )
                }
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  {this.state.pressedItem &&
                  this.state.pressedItem.item &&
                  this.state.pressedItem.item.pinned
                    ? 'Unpin'
                    : 'Pin'}
                </Text>
                <Icon
                  name="Pin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({optionsModalOpen: false, shareModalOpen: true})
                }
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  Share
                </Text>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
                ]}>
                <Text
                  style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                  Forum
                </Text>
              </View>

              {this.state.selectedTab !== 'PINNED' &&
              this.state.pressedItem &&
              this.state.pressedItem.item &&
              !this.state.pressedItem.item.pinned ? (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {paddingVertical: 15},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.handleHideModal(this.state.pressedActivityId);
                  }}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    Hide
                  </Text>
                  <Icon
                    name="Hide"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                </TouchableOpacity>
              ) : null}

              <TouchableOpacity
                onPress={this.deleteForumAlert}
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  Delete
                </Text>
                <Icon
                  name="TrashBin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({optionsModalOpen: false}, () =>
                    this.handlePinnedForumAndPoll(this.state.pressedItem),
                  )
                }
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  {this.state.pressedItem &&
                  this.state.pressedItem.item &&
                  this.state.pressedItem.item.pinned
                    ? 'Unpin'
                    : 'Pin'}
                </Text>
                <Icon
                  name="Pin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({optionsModalOpen: false, shareModalOpen: true})
                }
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  Share
                </Text>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  };

  infoModal = () => {
    return (
      <Modal
        visible={this.state.infoModal}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            alignSelf: 'center',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {height: 700, bottom: 0},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({infoModal: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.white,
              paddingHorizontal: 15,
              paddingTop: 15,
              paddingBottom: 10,
              width: '80%',
            }}>
            <Text
              style={[
                typography.Title_2,
                {color: COLORS.dark_600, alignSelf: 'center'},
              ]}>
              Supporting media file type
            </Text>
            <Text
              style={[
                typography.Caption,
                {
                  color: COLORS.altgreen_400,
                  alignSelf: 'center',
                  marginTop: 10,
                },
              ]}>
              You can enhance your WeNaturalists
            </Text>
            <Text
              style={[
                typography.Caption,
                {color: COLORS.altgreen_400, alignSelf: 'center'},
              ]}>
              experience by adding and sharing
            </Text>
            <Text
              style={[
                typography.Caption,
                {color: COLORS.altgreen_400, alignSelf: 'center'},
              ]}>
              media samples
            </Text>

            <Text
              style={[
                typography.Note,
                {
                  color: COLORS.dark_600,
                  fontFamily: 'Montserrat-SemiBold',
                  marginTop: 10,
                },
              ]}>
              The following file formats of media samples are supported:
            </Text>
            <View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <Text style={{color: COLORS.altgreen_300}}>{'\u2022'}</Text>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-SemiBold',
                      marginLeft: 5,
                    },
                  ]}>
                  Adobe pdf (.pdf)
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 2}}>
                <Text style={{color: COLORS.altgreen_300}}>{'\u2022'}</Text>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-SemiBold',
                      marginLeft: 5,
                    },
                  ]}>
                  Microsoft Powerpoint (.ppt/.pptx)
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 2}}>
                <Text style={{color: COLORS.altgreen_300}}>{'\u2022'}</Text>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-SemiBold',
                      marginLeft: 5,
                    },
                  ]}>
                  Microsoft Excel (.xls/.xlsx)
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 2}}>
                <Text style={{color: COLORS.altgreen_300}}>{'\u2022'}</Text>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-SemiBold',
                      marginLeft: 5,
                    },
                  ]}>
                  Microsoft Word (.doc/.docx)
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 2}}>
                <Text style={{color: COLORS.altgreen_300}}>{'\u2022'}</Text>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-SemiBold',
                      marginLeft: 5,
                    },
                  ]}>
                  Other Documents (.csv/.txt)
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 2}}>
                <Text style={{color: COLORS.altgreen_300}}>{'\u2022'}</Text>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-SemiBold',
                      marginLeft: 5,
                    },
                  ]}>
                  Image formats (.jpg/.jpeg/.png)
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 2}}>
                <Text style={{color: COLORS.altgreen_300}}>{'\u2022'}</Text>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-SemiBold',
                      marginLeft: 5,
                    },
                  ]}>
                  Audio formats (.mp3/.ogg/.mpeg/.aac/.wav)
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 2}}>
                <Text style={{color: COLORS.altgreen_300}}>{'\u2022'}</Text>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-SemiBold',
                      marginLeft: 5,
                    },
                  ]}>
                  Video formats (.mp4/.m4a/.webm/.mkv/.flv)
                </Text>
              </View>
            </View>

            <Text
              style={[
                typography.Note,
                {
                  color: COLORS.dark_600,
                  fontFamily: 'Montserrat-SemiBold',
                  marginTop: 10,
                },
              ]}>
              Important
            </Text>
            <View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <Text style={{color: COLORS.altgreen_300}}>{'\u2022'}</Text>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-SemiBold',
                      marginLeft: 5,
                    },
                  ]}>
                  Individual file size cannot exceed 100 MB
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 2}}>
                <Text style={{color: COLORS.altgreen_300}}>{'\u2022'}</Text>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-SemiBold',
                      marginLeft: 5,
                    },
                  ]}>
                  Blog cover image size cannot exceed 15 MB
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  deleteForumAlert = () => {
    this.setState({optionsModalOpen: false});
    Alert.alert('', 'Are you sure you want to delete this forum?', [
      {
        text: 'YES',
        onPress: () => this.deleteForum(),
        style: 'cancel',
      },
      {
        text: 'NO',
        onPress: () => console.log('user cancelled deletion'),
      },
    ]);
  };

  handleSubmit = () => {
    if (
      this.state.forumTitle.trim() !== '' &&
      this.state.writeSomething.trim() !== '' &&
      this.state.hashTags.length > 0
    ) {
      let postBody = {
        userId: this.state.userId,
        title: this.state.forumTitle.trim(),
        content: this.state.writeSomething.trim(),
        hashTags: this.state.hashTags
          ? this.state.hashTags.map((item) => {
              return item.replace(/#/g, '');
            })
          : [],
        status: 'ENABLE',
        userType: 'WENAT',
        createdBy:
          this.state.userData && this.state.userData.firstName
            ? this.state.userData.firstName
            : this.state.userData.companyName,
      };
      if (this.state.draftId) {
        postBody.draftId = this.state.draftId;
      }

      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/backend/public/forum/create',
        headers: {'Content-Type': 'application/json'},
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (res.status === '201 CREATED') {
            Snackbar.show({
              backgroundColor: COLORS.dark_900,
              text: 'Forum created successfully',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
            setTimeout(() => {
              this.setState({
                createForumModalOpen: false,
                forumTitle: '',
                writeSomething: '',
                hashTags: [],
                publishButton: true,
              });
              this.getForumList(
                this.state.userId,
                this.state.selectedTab,
                this.state.searchParam,
              );
            }, 1000);
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            console.log(err.response.data);
            err.response.data.status === 500
              ? Snackbar.show({
                  backgroundColor: '#B22222',
                  text: 'Internal server error. Please try after some time',
                  duration: Snackbar.LENGTH_LONG,
                })
              : Snackbar.show({
                  backgroundColor: '#B22222',
                  text: err.response.data.message,
                  duration: Snackbar.LENGTH_LONG,
                });
          }
        });
    } else {
      if (this.state.forumTitle.trim() === '')
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter the title',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.writeSomething.trim() === '')
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter the description',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.hashTags.length === 0)
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Add at least 1 hashtag',
          duration: Snackbar.LENGTH_LONG,
        });
    }
  };

  saveAsDraft = () => {
    if (this.state.forumTitle.trim() !== '') {
      let parameterDetails = {};

      parameterDetails.title = this.state.forumTitle;
      if (this.state.writeSomething !== '')
        parameterDetails.content = this.state.writeSomething;
      if (this.state.hashTags.length > 0)
        parameterDetails.hashTags = this.state.hashTags.map((item) => {
          return item.replace(/#/g, '');
        });
      parameterDetails.createdBy =
        this.state.userData && this.state.userData.firstName
          ? this.state.userData.firstName
          : this.state.userData.companyName;

      let postBody = {
        userId: this.state.userId,
        parameterDetails: parameterDetails,
        type: 'FORUM',
      };
      if (this.state.draftId) {
        postBody.draftId = this.state.draftId;
      }

      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/backend/drafts/create',
        headers: {'Content-Type': 'application/json'},
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          console.log(res.status);
          if (res.status === '201 CREATED') {
            Snackbar.show({
              backgroundColor: COLORS.dark_900,
              text: 'Forum Saved as draft',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
            setTimeout(() => {
              this.setState({
                createForumModalOpen: false,
                forumTitle: '',
                writeSomething: '',
                hashTags: [],
                addHashTagsModalOpen: false,
                publishButton: true,
              });
              this.getForumList(
                this.state.userId,
                this.state.selectedTab,
                this.state.searchParam,
              );
            }, 1000);
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            err.response.data.status === 500
              ? Snackbar.show({
                  backgroundColor: '#B22222',
                  text:
                    'Can not save as draft due to internal server error. Please try after some time',
                  duration: Snackbar.LENGTH_LONG,
                })
              : Snackbar.show({
                  backgroundColor: '#B22222',
                  text: err.response.data.message,
                  duration: Snackbar.LENGTH_LONG,
                });
          }
        });
    } else {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please enter the Title',
        duration: Snackbar.LENGTH_LONG,
      });
    }
  };

  insertVideo = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.video],
      });
      this.richText.current?.insertVideo({src: res.uri});
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
      } else {
        throw err;
      }
    }
  };

  createForumModal = () => {
    return (
      <Modal
        onRequestClose={() => this.setState({createForumModalOpen: false})}
        visible={this.state.createForumModalOpen}
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <SafeAreaView style={{flex: 1, backgroundColor: COLORS.grey_100}}>
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null}>
            <ScrollView
              style={{marginTop: '-6%'}}
              keyboardShouldPersistTaps="handled">
              <View
                style={[
                  defaultShape.Modal_Categories_Container,
                  {backgroundColor: COLORS.grey_100},
                ]}>
                <View
                  style={
                    Platform.OS === 'ios'
                      ? [
                          styles.header,
                          {paddingVertical: 12, backgroundColor: COLORS.white},
                        ]
                      : [styles.header, {backgroundColor: COLORS.grey_100}]
                  }>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      width: '100%',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        maxWidth: '60%',
                        // backgroundColor: 'pink'
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.state.forumTitle.trim() !== ''
                            ? this.saveAsDraft()
                            : this.setState({
                                createForumModalOpen: false,
                                forumTitle: '',
                                writeSomething: '',
                                hashTags: [],
                              })
                        }>
                        <Icon
                          name="Cross"
                          size={14}
                          color={COLORS.dark_500}
                          style={{
                            paddingTop: Platform.OS === 'ios' ? 0 : 8,
                            marginRight: 8,
                          }}
                        />
                      </TouchableOpacity>

                      <Image
                        source={
                          this.state.userData &&
                          this.state.userData.profileImage
                            ? {uri: this.state.userData.profileImage}
                            : this.state.userData &&
                              this.state.userData.type === 'COMPANY'
                            ? DefaultBusiness
                            : defaultProfile
                        }
                        style={{width: 30, height: 30, borderRadius: 15}}
                      />
                      <Text
                        numberOfLines={1}
                        style={[
                          typography.Button_Lead,
                          {
                            color: COLORS.dark_800,
                            marginLeft: 8,
                            maxWidth: 140,
                          },
                        ]}>
                        Post as{' '}
                        <Text
                          style={[
                            typography.Button_1,
                            {
                              color: COLORS.dark_800,
                              marginLeft: 8,
                              fontSize: 15,
                            },
                          ]}>
                          {this.state.userData && this.state.userData.firstName
                            ? this.state.userData.firstName.split(' ').slice(0, 1).join(' ')
                            : this.state.userData.companyName ? 
                            this.state.userData.companyName.split(' ').slice(0, 1).join(' ') : null}
                        </Text>
                      </Text>
                    </View>

                    {this.state.publishButton ? (
                      <View
                        style={{flexDirection: 'row', alignItems: 'center'}}>
                        <TouchableOpacity
                          onPress={() => this.handleSubmit()}
                          activeOpacity={0.9}
                          style={[
                            {
                              paddingVertical: 5,
                              paddingHorizontal: 10,
                              alignSelf: 'flex-end',
                              marginRight: 0,
                              borderRadius: 0,
                              borderTopLeftRadius: 4,
                              borderBottomLeftRadius: 4,
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                              backgroundColor: COLORS.dark_700,
                            },
                          ]}>
                          <Text
                            style={[
                              typography.Caption,
                              {color: COLORS.altgreen_300},
                            ]}>
                            PUBLISH
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => this.setState({publishButton: false})}
                          activeOpacity={0.9}
                          style={[
                            {
                              paddingRight: 10,
                              paddingVertical: Platform.OS === 'ios' ? 6 : 0,
                              alignSelf: 'flex-end',
                              marginRight: 0,
                              borderRadius: 0,
                              borderTopRightRadius: 4,
                              borderBottomRightRadius: 4,
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                              marginLeft: Platform.OS === 'ios' ? 0 : -4,
                              backgroundColor: COLORS.dark_600,
                              paddingHorizontal: 0,
                              height: 28.2,
                            },
                          ]}>
                          <Icon
                            name="Arrow2_Down"
                            color={COLORS.white}
                            size={16}
                            style={{
                              marginTop: Platform.OS === 'android' ? 9 : 0,
                              marginLeft: 6,
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                    ) : (
                      <View style={{flexDirection: 'column'}}>
                        <View style={{flexDirection: 'row'}}>
                          <TouchableOpacity
                            onPress={() => this.handleSubmit()}
                            activeOpacity={0.5}
                            style={[
                              defaultShape.ContextBtn_FL_Drk,
                              {
                                paddingVertical: 5,
                                paddingHorizontal: 10,
                                alignSelf: 'flex-end',
                                marginRight: 0,
                                borderRadius: 0,
                                borderTopLeftRadius: 4,
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: COLORS.dark_700,
                              },
                            ]}>
                            <Text
                              style={[
                                typography.Caption,
                                {color: COLORS.altgreen_300},
                              ]}>
                              PUBLISH
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            onPress={() => this.setState({publishButton: true})}
                            activeOpacity={0.5}
                            style={[
                              {
                                paddingRight: 10,
                                paddingVertical: Platform.OS === 'ios' ? 6 : 0,
                                alignSelf: 'flex-end',
                                marginRight: 0,
                                borderRadius: 0,
                                borderTopRightRadius: 4,
                                borderBottomRightRadius: 0,
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginLeft: Platform.OS === 'ios' ? 0 : -4,
                                backgroundColor: COLORS.dark_600,
                                paddingHorizontal: 0,
                                height: 28.2,
                              },
                            ]}>
                            <Icon
                              name="Arrow2_Down"
                              color={COLORS.white}
                              size={16}
                              style={{
                                marginTop: Platform.OS === 'android' ? 9 : 0,
                                marginLeft: 6,
                              }}
                            />
                          </TouchableOpacity>
                        </View>
                        <TouchableOpacity
                          onPress={() => this.saveAsDraft()}
                          activeOpacity={0.9}
                          style={[
                            defaultShape.ContextBtn_FL_Drk,
                            {
                              width: Platform.OS === 'android' ? 104 : 108.09,
                              paddingVertical: 6,
                              alignSelf: 'flex-start',
                              alignItems: 'flex-start',
                              marginRight: 0,
                              borderRadius: 0,
                              borderBottomLeftRadius: 4,
                              borderBottomRightRadius: 4,
                              backgroundColor: COLORS.dark_700,
                            },
                          ]}>
                          <Text
                            style={[
                              typography.Caption,
                              {color: COLORS.altgreen_300, marginLeft: 1.5},
                            ]}>
                            DRAFT
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                  </View>
                </View>

                <View
                  style={{
                    backgroundColor: COLORS.grey_100,
                    alignItems: 'center',
                    width: '100%',
                    paddingBottom: 16,
                  }}>
                  <View
                    style={{
                      width: '100%',
                      height: 90,
                      backgroundColor: '#00394d',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={[typography.H3, {color: '#dadd21'}]}>
                      CREATE A FORUM
                    </Text>
                  </View>

                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        paddingLeft: 6,
                        paddingRight: 12,
                        marginTop: 8,
                      }}>
                      <TextInput
                        theme={{
                          colors: {
                            text: COLORS.dark_700,
                            primary: COLORS.altgreen_300,
                            placeholder: COLORS.altgreen_300,
                          },
                        }}
                        label="Add forum title"
                        multiline
                        selectionColor="#C8DB6E"
                        style={[
                          typography.H3,
                          {
                            width: '90%',
                            color: COLORS.dark_700,
                            borderRadius: 50,
                            backgroundColor: COLORS.grey_100,
                          },
                        ]}
                        onChangeText={(value) =>
                          this.setState({forumTitle: value, seconds: 59})
                        }
                        value={this.state.forumTitle}
                      />
                    </View>
                    <View>
                      <RichToolbar
                        style={[styles.richBar]}
                        flatContainerStyle={styles.flatStyle}
                        editor={this.richText}
                        disabled={false}
                        selectedIconTint={'#2095F2'}
                        disabledIconTint={'#bfbfbf'}
                      />
                      <RichEditor
                        ref={this.richText}
                        useContainer={true}
                        style={styles.rich}
                        initialHeight={300}
                        placeholder={'please write here ...'}
                        onChange={(value) =>
                          this.setState({writeSomething: value})
                        }
                        editorInitializedCallback={() =>
                          this.editorInitializedCallback()
                        }
                      />
                      <RichToolbar
                        style={[styles.richBar]}
                        flatContainerStyle={styles.flatStyle}
                        editor={this.richText}
                        insertVideo={this.insertVideo}
                        onPressAddImage={() => {
                          ImagePicker.openPicker({
                            width: 300,
                            height: 400,
                            mediaType: 'photo',
                          }).then((image) => {
                            ImagePicker.openCropper({
                              path: image.path,
                              width: 300,
                              height: 200,
                              includeBase64: true,
                            })
                              .then((cropImage) => {
                                console.log('PressAddImage', cropImage);
                                this.richText.current?.insertImage(
                                  `data:${cropImage.mime};base64,${cropImage.data}`,
                                );
                              })
                              .catch((err) => console.log(err));
                          });
                        }}
                        disabled={false}
                        actions={[
                          // actions.undo,
                          // actions.redo,
                          // actions.insertVideo,
                          actions.insertImage,
                          actions.setStrikethrough,
                          actions.checkboxList,
                          actions.insertOrderedList,
                          actions.blockquote,
                          actions.alignLeft,
                          actions.alignCenter,
                          actions.alignRight,
                          actions.code,
                          actions.line,

                          actions.foreColor,
                          actions.hiliteColor,
                          actions.heading1,
                          actions.heading4,
                          'fontSize',
                        ]}
                        iconMap={{
                          [actions.heading1]: ({tintColor}) => (
                            <Text style={[styles.tib, {color: tintColor}]}>
                              H1
                            </Text>
                          ),
                          [actions.heading4]: ({tintColor}) => (
                            <Text style={[styles.tib, {color: tintColor}]}>
                              H3
                            </Text>
                          ),
                        }}
                        fontSize={this.fontSize}
                        selectedIconTint={'#2095F2'}
                        disabledIconTint={'#bfbfbf'}
                      />
                    </View>
                  </View>

                  <View
                    style={{
                      // backgroundColor: COLORS.white,
                      alignItems: 'flex-start',
                      // alignSelf: 'flex-start',
                      paddingVertical: 20,
                      paddingLeft: 30,
                      width: '100%',
                      marginTop: 10,
                      // position: 'absolute',
                      // bottom: 0
                    }}>
                    <FlatList
                      keyboardShouldPersistTaps="handled"
                      scrollEventThrottle={0}
                      ref={(ref) => (this.scrollView = ref)}
                      onContentSizeChange={() => {
                        this.scrollView.scrollToEnd({animated: false});
                        this.setState({hashTagModalOpen: true});
                      }}
                      columnWrapperStyle={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                      }}
                      numColumns={3}
                      showsVerticalScrollIndicator={false}
                      data={[...this.state.hashTags, 'lastData']}
                      keyExtractor={(item, index) => item + index}
                      renderItem={({item, index}) =>
                        index < this.state.hashTags.length ? (
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({
                                hashTags: this.state.hashTags.filter(
                                  (value, index2) =>
                                    value + index2 !== item + index,
                                ),
                                seconds: 59,
                              })
                            }
                            activeOpacity={0.6}
                            style={{
                              flexDirection: 'row',
                              backgroundColor: COLORS.altgreen_t50 + '80',
                              paddingHorizontal: 10,
                              marginVertical: 6,
                              height: 28,
                              borderRadius: 17,
                              justifyContent: 'center',
                              alignItems: 'center',
                              marginRight: 6,
                            }}>
                            <Text
                              style={[
                                typography.Caption,
                                {
                                  color: COLORS.dark_500,
                                  marginRight: 4,
                                  marginTop: Platform.OS === 'ios' ? -2 : 0,
                                },
                              ]}>
                              {item}
                            </Text>
                            <Icon
                              name="Cross_Rounded"
                              color={COLORS.dark_500}
                              size={15}
                              style={{
                                marginTop: Platform.OS === 'android' ? 10 : 0,
                              }}
                            />
                          </TouchableOpacity>
                        ) : (
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({
                                addHashTagsModalOpen: true,
                                createForumModalOpen: false,
                                seconds: 59,
                              })
                            }
                            activeOpacity={0.6}
                            style={{
                              flexDirection: 'row',
                              backgroundColor: COLORS.grey_200,
                              width: 137,
                              height: 28,
                              borderRadius: 17,
                              justifyContent: 'center',
                              alignItems: 'center',
                              marginTop: 7,
                            }}>
                            <Icon
                              name="Hashtag"
                              color={COLORS.dark_500}
                              size={14}
                              style={{
                                marginTop: Platform.OS === 'android' ? 7 : 0,
                              }}
                            />
                            <Text
                              style={[
                                typography.Caption,
                                {
                                  color: COLORS.dark_500,
                                  marginLeft: 5,
                                  marginTop: Platform.OS === 'ios' ? -2 : 0,
                                },
                              ]}>
                              Add Hashtag
                            </Text>
                          </TouchableOpacity>
                        )
                      }
                    />
                  </View>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </SafeAreaView>
      </Modal>
    );
  };

  changeHashTagsState = (value) => {
    this.setState(value);
  };

  suggestedHashTagsModal = () => {
    return (
      <Modal
        visible={this.state.suggestedHashTagsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <SuggestedHashTags
          changeHashTagsState={this.changeHashTagsState}
          hashTags={this.state.hashTags}
          writeSomething={this.state.writeSomething}
        />
      </Modal>
    );
  };

  listHeaderComponent = () => {
    if (this.state.isLoading) {
      return <SkeletonLoader />;
    } else {
      return !this.state.forumData.length &&
        this.state.selectedTab === 'MYFORUM' ? (
        <View>
          <Text
            style={[
              typography.Title_2,
              {
                color: COLORS.dark_800,
                fontSize: 16,
                alignSelf: 'center',
                marginTop: 60,
                textAlign: 'center',
                marginHorizontal: 8,
              },
            ]}>
            Forums are great way to understand, discuss and collaborate on
            thoughts, ideas or opinions.
          </Text>

          <Text
            style={[
              typography.Subtitle_2,
              {
                color: '#698f8a',
                marginTop: 20,
                alignSelf: 'center',
                fontSize: 14,
              },
            ]}>
            Start by{' '}
            <Text
              onPress={() => this.setState({createForumModalOpen: true})}
              style={[
                typography.Body_1_bold,
                {
                  textDecorationLine: 'underline',
                  color: '#97a600',
                  fontSize: 14,
                },
              ]}>
              creating
            </Text>{' '}
            or{' '}
            <Text
              onPress={() => this.changeState({selectedTab: ''})}
              style={[
                typography.Body_1_bold,
                {
                  textDecorationLine: 'underline',
                  color: '#97a600',
                  fontSize: 14,
                },
              ]}>
              participating
            </Text>{' '}
            in a forum
          </Text>
        </View>
      ) : !this.state.forumData.length &&
        this.state.selectedTab === 'PINNED' ? (
        <View>
          <Text
            style={[
              typography.Title_2,
              {
                color: COLORS.dark_800,
                fontSize: 16,
                alignSelf: 'center',
                marginTop: 60,
                textAlign: 'center',
                marginHorizontal: 8,
              },
            ]}>
            Pining your favourite forums is a great way to revisit them later.
          </Text>

          <Text
            style={[
              typography.Subtitle_2,
              {
                color: '#698f8a',
                marginTop: 20,
                alignSelf: 'center',
                fontSize: 14,
              },
            ]}>
            Pin{' '}
            <Text
              onPress={() => this.changeState({selectedTab: ''})}
              style={[
                typography.Body_1_bold,
                {
                  textDecorationLine: 'underline',
                  color: '#97a600',
                  fontSize: 14,
                },
              ]}>
              forums
            </Text>{' '}
            you like!
          </Text>
        </View>
      ) : (
        <></>
      );
    }
  };

  onRefresh = () => {
    this.setState({page: 0, forumData: []}, () =>
      this.getForumList(
        this.state.userId,
        this.state.selectedTab,
        this.state.searchParam,
      ),
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {/*********** Header starts ***********/}
        {this.createForumModal()}
        {this.infoModal()}
        {this.optionsModal()}
        {this.reasonForReportingModal()}
        {this.addHashTagsModal()}
        {this.shareModal()}
        {this.suggestedHashTagsModal()}

        <View
          style={
            Platform.OS === 'ios'
              ? [styles.header, {paddingVertical: 15}]
              : styles.header
          }>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              style={{
                width: 40,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="Arrow-Left"
                size={15}
                color="#91B3A2"
                style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
              />
            </TouchableOpacity>
            <Text
              style={[
                typography.H3,
                {
                  color: COLORS.dark_800,
                  fontSize: 18,
                  marginLeft: 10,
                },
              ]}>
              FORUM
            </Text>
          </View>
          <TouchableOpacity
            style={[defaultShape.ContextBtn_FL_Drk]}
            onPress={() =>
              this.setState(
                {createForumModalOpen: true, seconds: 59},
                () =>
                  (this.interval = setInterval(
                    () =>
                      this.setState((prevState) => ({
                        seconds: prevState.seconds - 1,
                      })),
                    1000,
                  )),
              )
            }>
            <Text
              style={[
                typography.Caption,
                {color: COLORS.altgreen_200, paddingVertical: 2},
              ]}>
              Add FORUM
            </Text>
          </TouchableOpacity>
        </View>

        <ForumHeader
          changeState={this.changeState}
          selectedTab={this.state.selectedTab}
        />

        {/*********** Header ends ***********/}

        {/*********** Forum ItemList starts ***********/}

        <FlatList
          showsVerticalScrollIndicator={false}
          style={{paddingTop: 10}}
          contentContainerStyle={{paddingBottom: 150}}
          data={this.state.forumData}
          ListHeaderComponent={this.listHeaderComponent()}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={1}
          keyExtractor={(item) => item.id}
          renderItem={(item) => !item.item.hidden && this.renderForumItem(item)}
          refreshControl={
            <RefreshControl refreshing={false} onRefresh={this.onRefresh} />
          }
        />

        {/*********** Forum ItemList ends ***********/}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  rich: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: '#e3e3e3',
    minHeight: 300,
  },
  flatStyle: {
    paddingHorizontal: 12,
  },
  richBar: {
    borderColor: '#efefef',
    borderTopWidth: StyleSheet.hairlineWidth,
  },
  richBarDark: {
    backgroundColor: '#191d20',
    borderColor: '#696969',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  forumItemView: {
    width: '90%',
    marginTop: 15,
    backgroundColor: COLORS.white,
    borderRadius: 4,
    alignSelf: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
  profileImage: {
    height: 30,
    width: 30,
    borderRadius: 15,
    marginRight: 8,
  },
});

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AllForum);
