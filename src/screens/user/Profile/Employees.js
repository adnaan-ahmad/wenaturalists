import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    Platform,
    ActivityIndicator,
    FlatList,
    Image,
    Modal,
    SafeAreaView,
    Linking,
    Share,
    Clipboard
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import axios from 'axios'
import Snackbar from 'react-native-snackbar'

import EmployeeList from '../../../Components/User/Profile/EmployeeList'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultStyle from '../../../Components/Shared/Typography'
import { REACT_APP_userServiceURL, REACT_APP_domainUrl } from '../../../../env.json'
import httpService from '../../../services/AxiosInterceptors'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import {
    personalProfileRequest,
    personalSkillsSpecializationRequest,
    personalRecentActivityRequest,
    personalExperienceRequest,
    personalEducationRequest,
    personalBusinessPageRequest,
    personalHashTagsRequest,
    personalConnectionInfoRequest
} from '../../../services/Redux/Actions/User/PersonalProfileActions'
import { userCircleRequest } from '../../../services/Redux/Actions/User/CircleActions'
import { endorsementRequest } from '../../../services/Redux/Actions/User/EndorsementActions'
import { causesRequest } from '../../../services/Redux/Actions/User/CausesActions'
import { connectsRequest } from '../../../services/Redux/Actions/User/ConnectsActions'

import projectDefault from '../../../../assets/project-default.jpg'
import defaultCover from '../../../../assets/defaultCover.png'
import InstitutionLogo from '../../../../assets/InstitutionLogo.png'
import ProfileSvg from '../../../Components/User/Profile/ProfileSvg'
import RequestEndorsement from '../../../Components/User/Profile/RequestEndorsement'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

httpService.setupInterceptors()

class ConnectionDetails extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            requestEndorsementModalOpen: false,
            connected: false,
            followers: [],
            mutualConnects: [],
            isConnected: '',
            currentTab: 'followers',
            totalMutualConnects: 0,
            navigate: false,
            navigateId: ''
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("userId").then((value) => {

            if (this.props.route.params && this.props.route.params.id !== '') {
                this.props.personalProfileRequest({ userId: this.props.route.params.id, otherUserId: value })
                this.props.personalConnectionInfoRequest({ userId: this.props.route.params.id })
                this.props.userConnectionInfo.body ? this.props.connectsRequest({ userId: this.props.route.params.id, size: this.props.userConnectionInfo.body.connections }) : null
                this.props.userConnectionInfo.body ?
                    this.getFollowers(this.props.route.params.id, 'followers', this.props.userConnectionInfo.body.connections) : null
                this.getMutualConnects()
            }
            else {
                this.props.personalProfileRequest({ userId: value, otherUserId: '' })
                this.props.personalConnectionInfoRequest({ userId: value })
                this.props.userConnectionInfo.body ? this.props.connectsRequest({ userId: value, size: this.props.userConnectionInfo.body.connections }) : null
                this.props.userConnectionInfo.body ?
                    this.getFollowers(value, 'followers', this.props.userConnectionInfo.body.connections) : null
            }

            this.setState({ userId: value })
        }).catch((e) => {
            console.log(e)
        })
    }

    getFollowers = (id, type, total) => {
        axios({
            method: "get",
            url:
                REACT_APP_userServiceURL +
                "/network/" +
                id +
                '/' + type +
                "?page=" +
                0 +
                "&size=" +
                total + 10,
            headers: { "Content-Type": "application/json" },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.data && response.data.message === "Success!") {
                    console.log('```````````````````` Followers ````````````````````', response.data.body.content[0])
                    this.setState({ followers: response.data.body.content })

                }
            })
            .catch((err) => {
                console.log(err)
            })
    }

    changeState = (value) => {
        this.setState(value)
    }

    componentDidUpdate(prevProps, prevState) {
    
        if (this.state.navigate) {
    
            this.setState({ navigate: false }, () => this.props.navigation.navigate('ProfileStack', {
                screen: 'ProfileScreen',
                params: { userId: this.state.navigateId },
              }))
    
        }
      }

    getMutualConnects = () => {
        axios({
            method: "get",
            url:
                REACT_APP_userServiceURL +
                "/network/" +
                this.state.userId +
                '/mutualConnects/' + this.props.route.params.id +
                "?page=" +
                0 +
                "&size=" +
                100,
            headers: { "Content-Type": "application/json" },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.data && response.data.message === "Success!") {
                    // console.log('```````````````````` Mutual ````````````````````', response.data.body.content[0])
                    this.setState({ mutualConnects: response.data.body.content, totalMutualConnects: response.data.body.content.length })

                }
            })
            .catch((err) => {
                console.log(err)
            })
    }

    render() {
        const { currentTab, followers, mutualConnects, totalMutualConnects } = this.state
        return (
            <SafeAreaView>
                <View style={{ backgroundColor: COLORS.white, position: 'absolute', top: 0, zIndex: 2 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: COLORS.white }}>

                        <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', padding: 10 }}>

                            <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', justifyContent: 'space-between' }}>
                                
                                <TouchableOpacity activeOpacity={0.5}
                                    onPress={() => {
                                        this.props.route.params && this.props.route.params.id !== '' ?
                                            this.props.navigation.navigate('CompanyProfileScreen', { userId: this.props.route.params.id })
                                            : this.props.navigation.navigate('CompanyProfileScreen')

                                    }}
                                    style={{ backgroundColor: COLORS.white, alignItems: 'center', justifyContent: 'center', height: 42, width: 42, borderRadius: 21 }}>
                                    <Icon name="Arrow-Left" size={16} color="#00394D" style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }} />
                                </TouchableOpacity>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                                <View style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: COLORS.green_500, justifyContent: 'center', alignItems: 'center' }}> 
                                    <Icon name="Network_F" size={15} color={COLORS.dark_700} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                                </View>     

                                    <View style={{ alignSelf: 'center' }}>
                                        <Text style={[defaultStyle.Title_1, { color: COLORS.dark_800, fontSize: 14 }]}>Network</Text>
                                    </View>
                                </View>

                                <View style={{ width: 30, height: 30 }}></View>

                            </View>

                        </View>

                    </View>

                    

                </View>

                <EmployeeList
                    changeState={this.changeState}
                    connectsData={this.props.userConnects.body ? this.props.userConnects.body.content.sort((a, b) => a.firstName.toUpperCase() > b.firstName.toUpperCase()) : null}
                    currentTab={currentTab}
                    followers={followers.sort((a, b) => a.firstName.toUpperCase() > b.firstName.toUpperCase())}
                    mutualConnects={mutualConnects.sort((a, b) => a.firstName.toUpperCase() > b.firstName.toUpperCase())}
                    totalMutualConnects={totalMutualConnects}
                    employees={this.props.route.params ? this.props.route.params.employees : null}
                    companyName={this.props.user.body ? this.props.user.body.userName : null}
                    originalEmployees={this.props.route.params ? this.props.route.params.employees : null}
                />

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    selectedTabText: {
        color: COLORS.dark_900, textAlign: 'center', fontWeight: 'bold'
    },
    selectedTabNo: {
        color: COLORS.altgreen_400, textAlign: 'center', fontWeight: '700'
    },
    unSelectedTabText: {
        color: COLORS.altgreen_300, marginBottom: 5, textAlign: 'center'
    },
    unSelectedTabNo: {
        color: COLORS.grey_400, marginTop: -5, textAlign: 'center', marginBottom: 8
    },
    borderBottom: {
        width: 100, height: 3, backgroundColor: COLORS.dark_800, borderTopLeftRadius: 4, borderTopRightRadius: 4, position: 'absolute', bottom: 0, alignSelf: 'center'
    },
    borderBottomUnselected: {
        width: 100, height: 3, backgroundColor: COLORS.white, borderTopLeftRadius: 4, borderTopRightRadius: 4, position: 'absolute', bottom: 0, alignSelf: 'center'
    }
})

const mapStateToProps = (state) => {
    return {
        userDataProgress: state.personalProfileReducer.userDataProgress,
        user: state.personalProfileReducer.user,
        error: state.personalProfileReducer.error,

        userConnectionInfoProgress: state.personalProfileReducer.userConnectionInfoProgress,
        userConnectionInfo: state.personalProfileReducer.userConnectionInfo,
        errorConnectionInfo: state.personalProfileReducer.errorConnectionInfo,

        userConnectsProgress: state.connectsReducer.userConnectsProgress,
        userConnects: state.connectsReducer.userConnects,
        errorConnects: state.connectsReducer.errorConnects
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
        personalConnectionInfoRequest: (data) => dispatch(personalConnectionInfoRequest(data)),
        connectsRequest: (data) => dispatch(connectsRequest(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConnectionDetails)
