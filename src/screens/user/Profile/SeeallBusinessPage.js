import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, ActivityIndicator, SafeAreaView, Platform } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'

import defaultBusiness from '../../../../assets/DefaultBusiness.png'
import { personalBusinessPageRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultCover from '../../../../assets/defaultCover.png'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultStyle from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

class SeeallBusinessPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {

    AsyncStorage.getItem("refreshToken").then((value) => {
      if (value === null) {
        this.props.navigation.replace("Login")
      }
    })

    AsyncStorage.getItem("userId").then((value) => {

      this.props.route.params && this.props.route.params.id !== '' ?
        this.props.personalBusinessPageRequest({ userId: this.props.route.params.id, otherUserId: value })
        : this.props.personalBusinessPageRequest({ userId: value, otherUserId: '' })

    }).catch((e) => {
      // console.log(e)
    })
  }

  renderItem = (item) => {
    return (

      <TouchableOpacity 
      activeOpacity={0.8}
      onPress={() => this.props.navigation.navigate('CompanyProfileScreen', { userId: item.companyId })}
      style={styles.businessCradView}>
        <View style={styles.businessProfileCard}>
          {item.profileImageUrl ?
            <Image style={{ width: 68, height: 68, borderRadius: 6 }} source={{ uri: item.profileImageUrl }} />
            :
            <Image style={{ width: 68, height: 68, borderRadius: 6 }} source={defaultBusiness} />
          }
        </View>
        <View style={styles.businessDetailsCard}>
          <Text numberOfLines={1} style={styles.businessName}>{item.companyName.charAt(0).toUpperCase() + item.companyName.slice(1)}</Text>
          <Text numberOfLines={1} style={styles.businessLocation}>{item.country}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    // this.props.userBusinessPage.body ? 
    // console.log('________________________this.props.userBusinessPage.body__________________________', this.props.userBusinessPage.body)
    // : null
    
    return (
      <SafeAreaView>

        <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 12 }] : styles.header}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack()}>
              <Icon name="Arrow-Left" size={14} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
            </TouchableOpacity>
            <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_800, marginLeft: 5 }]}>
              ORGANIZATION PAGES
            </Text>
          </View>
        </View>

        { this.props.userBusinessPage.body ?
          <FlatList
            initialNumToRender={10}
            contentContainerStyle={{ flexDirection: "row", flexWrap: "wrap", paddingBottom: 40, paddingTop: 10, justifyContent: 'center' }}
            // columnWrapperStyle={{ flexDirection: "row", flexWrap: "wrap", paddingVertical: 10, justifyContent: 'center' }}
            // numColumns={4000}
            keyExtractor={(item) => item.companyId}
            data={this.props.userBusinessPage.body.businessPage.sort((a, b) => a.companyName.toUpperCase(0) > b.companyName.toUpperCase(0))}
            renderItem={({ item }) => (
              this.renderItem(item)
            )}
          />
          :
          <ActivityIndicator size='small' color="#00394D" />
        }

      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
    backgroundColor: '#fff',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  businessCradView: {
    height: 124,
    width: 140,
    alignItems: 'center',
    marginVertical: 11,
    marginHorizontal: 8
  },
  businessProfileCard: {
    height: 68,
    width: 68,
    borderRadius: 6,
    position: 'absolute',
    top: 0,
    zIndex: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2,
  },
  businessDetailsCard: {
    height: 84,
    width: 140,
    borderRadius: 4,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.00,

    elevation: 1,
  },
  businessName: {
    color: COLORS.primarydark,
    fontSize: 12,
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
    maxWidth: '90%',
  },
  businessLocation: {
    color: COLORS.altgreen_400,
    fontSize: 12,
    fontFamily: 'Montserrat-Medium',
    maxWidth: '90%',
  }
})

const mapStateToProps = (state) => {
  return {
    userBusinessPageProgress: state.personalProfileReducer.userBusinessPageProgress,
    userBusinessPage: state.personalProfileReducer.userBusinessPage,
    errorBusinessPage: state.personalProfileReducer.errorBusinessPage,
  }
}

const mapDispatchToProps = (dispatch) => {

  return {
    personalBusinessPageRequest: (data) => dispatch(personalBusinessPageRequest(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeeallBusinessPage)