import React, {Component} from 'react';
import {
  Linking,
  Clipboard,
  Share,
  Dimensions,
  Modal,
  ImageBackground,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  TouchableHighlight,
  FlatList,
  Image,
  SafeAreaView,
  Platform,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {connect} from 'react-redux';

import {otherProfileRequest} from '../../../services/Redux/Actions/User/OtherProfileActions';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import httpService from '../../../services/AxiosInterceptors';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultStyle from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import typography from '../../../Components/Shared/Typography';
import HashTagItem from '../../../Components/User/HashTag/HashTagItem';

httpService.setupInterceptors();
const Icon = createIconSetFromIcoMoon(icoMoonConfig);
class OtherHashTags extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      hashtagList: [],
      filterModalOpen: false,
      filter: 'popular',
      filterData: 'POPULAR',
      currentFilterPressed: '',
      page: 0,
      size: 10,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        this.props.route.params && this.props.route.params.id !== ''
          ? this.props.otherProfileRequest({
              userId: this.props.route.params.id,
              otherUserId: value,
            })
          : this.props.otherProfileRequest({userId: value, otherUserId: ''});

        this.setState({userId: value}, () => {
          this.getPopularHashtags('POPULAR');
        });
      })
      .catch((e) => {
        console.log(e);
      });
    try {
      messaging().onNotificationOpenedApp((remoteMessage) => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from background state:',
            remoteMessage.data.url,
            remoteMessage.data.userId,
          );
        }
      });
    } catch (error) {
      console.log(error);
    }
    try {
      messaging()
        .getInitialNotification()
        .then((remoteMessage) => {
          if (remoteMessage) {
            // console.log('remoteMessage :', remoteMessage)
            console.log(
              'Notification caused app to open from quit state:',
              remoteMessage.data.url,
              remoteMessage.data.userId,
            );
          }
        });
    } catch (error) {
      console.log(error);
    }
  }

  stickyHeader = () => {
    return (
      <View
        style={{
          backgroundColor: COLORS.white,
          position: 'absolute',
          top: 0,
          zIndex: 2,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: COLORS.white,
          }}>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => this.props.navigation.goBack()}
                style={{
                  backgroundColor: COLORS.white,
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 42,
                  width: 42,
                  borderRadius: 21,
                  marginTop: 6,
                }}>
                <Icon
                  name="Arrow-Left"
                  size={16}
                  color="#00394D"
                  style={{marginTop: 5}}
                />
              </TouchableOpacity>

              {this.props.user.body &&
              this.props.user.body.originalProfileImage ? (
                <Image
                  source={{uri: this.props.user.body.originalProfileImage}}
                  style={{width: 30, height: 30, borderRadius: 15}}
                />
              ) : null}

              <View style={{alignSelf: 'center', marginLeft: 8}}>
                {this.props.user.body !== undefined ? (
                  <Text
                    style={[
                      defaultStyle.Title_1,
                      {color: COLORS.dark_800, fontSize: 14},
                    ]}>
                    {this.props.user.body.userName}{' '}
                  </Text>
                ) : (
                  <></>
                )}

                {this.props.user.body ? (
                  <Text
                    style={[
                      defaultStyle.Subtitle_1,
                      {color: COLORS.altgreen_400, fontSize: 12, marginTop: -6},
                    ]}>
                    {this.props.user.body.persona}
                  </Text>
                ) : (
                  <></>
                )}
              </View>
            </View>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            justifyContent: 'space-evenly',
            width: 240,
          }}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={{paddingTop: 8, width: 120}}
            onPress={() => this.props.navigation.navigate('OtherProfileScreen', { userId: this.props.route.params.id })}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {
                  color: COLORS.altgreen_400,
                  marginBottom: 6,
                  textAlign: 'center',
                },
              ]}>
              ABOUT
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.8}
            style={{paddingTop: 8, width: 120}}
            onPress={() => this.props.navigation.navigate('OtherActivitiesScreen', { id: this.props.route.params.id })}>
            <Text
              style={[
                defaultStyle.Button_2,
                {
                    color: COLORS.altgreen_400,
                    marginBottom: 6,
                    textAlign: 'center',
                },
              ]}>
              ACTIVITY
            </Text>
          </TouchableOpacity>

          <TouchableOpacity activeOpacity={0.8} style={{width: 120}}>
            <Text
              style={[
                defaultStyle.Button_2,
                {
                  color: COLORS.dark_800,
                  textAlign: 'center',
                  marginBottom: 6,
                  paddingTop: 8,
                },
              ]}>
              HASHTAGS
            </Text>
            <View
              style={{
                width: 120,
                height: 5,
                backgroundColor: COLORS.dark_800,
                borderTopLeftRadius: 4,
                borderTopRightRadius: 4,
              }}></View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  /** ---- start fetch popular hashtag ------ **/
  getPopularHashtags = (filterData) => {
    axios({
      method: 'get',
      url: `${REACT_APP_userServiceURL}/tags/getHashtagByUserId/?userId=${this.props.route.params.id}&page=${this.state.page}&size=24&filter=${filterData}`,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            hashtagList: this.state.hashtagList.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  };
  /** ----- end fetch popular hashtags ------- **/

  handleLoadMore = () => {
    this.setState({page: this.state.page + 1}, () => this.getPopularHashtags());
  };

  /** -------- start handle filter dropdown and functions -------**/
  currentFilterPressed = (filterprop) => {
    if (filterprop === 'popular') {
      this.setState({page: 0}, () => {
        this.getPopularHashtags('POPULAR');
      });
    } else if (filterprop === 'recent') {
      this.setState({page: 0}, () => {
        this.getPopularHashtags('RECENT');
      });
    } else if (filterprop === 'trending') {
      this.setState({page: 0}, () => {
        this.getPopularHashtags('TRENDING');
      });
    }
  };

  filterModal = () => {
    return (
      <Modal
        visible={this.state.filterModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}></View>
          <LinearGradient
            colors={[
              COLORS.dark_800 + '00',
              COLORS.dark_800 + 'CC',
              COLORS.dark_800,
            ]}
            style={defaultShape.Linear_Gradient}></LinearGradient>
        </View>
        <TouchableOpacity
          activeOpacity={0.7}
          style={defaultShape.CloseBtn}
          onPress={() => this.setState({filterModalOpen: false})}>
          <Icon
            name="Cross"
            size={13}
            color={COLORS.dark_600}
            style={styles.crossIcon}
          />
        </TouchableOpacity>
        <Text
          style={[
            typography.Caption,
            {
              fontSize: 12,
              color: COLORS.dark_500,
              textAlign: 'center',
              alignSelf: 'center',
              position: 'absolute',
              bottom: 110,
              zIndex: 2,
            },
          ]}>
          Sort By
        </Text>
        <View
          style={[
            defaultShape.Modal_Categories_Container,
            {
              paddingTop: 10,
              height: 140,
              flexDirection: 'row',
            },
          ]}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <TouchableOpacity
              style={
                this.state.filter === 'popular'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() =>
                this.setState(
                  {
                    filterModalOpen: false,
                    filter: 'popular',
                    hashtagList: [],
                  },
                  () => this.currentFilterPressed('popular'),
                )
              }>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.filter === 'popular'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Popular
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.filter === 'recent'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() =>
                this.setState(
                  {
                    filterModalOpen: false,
                    filter: 'recent',
                    hashtagList: [],
                  },
                  () => this.currentFilterPressed('recent'),
                )
              }>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.filter === 'recent'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Recent
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.filter === 'trending'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() =>
                this.setState(
                  {
                    filterModalOpen: false,
                    filter: 'trending',
                    hashtagList: [],
                  },
                  () => this.currentFilterPressed('trending'),
                )
              }>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.filter === 'trending'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Trending
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </Modal>
    );
  };

  sortByComponent = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
          marginTop: 15,
          paddingBottom: 10,
          paddingHorizontal: 15,
          alignItems: 'center',
        }}>
        <Text
          style={[
            typography.Title_2,
            {color: COLORS.altgreen_400, marginRight: 10},
          ]}>
          Sort by
        </Text>
        <TouchableOpacity
          onPress={() => this.setState({filterModalOpen: true})}
          style={[styles.sortbyfilter]}>
          <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
            {this.state.filter}
          </Text>
          <Icon
            name="Arrow_Down"
            color={COLORS.altgreen_400}
            size={12}
            style={{marginTop: Platform.OS === 'ios' ? 0 : 8, marginLeft: 5}}
          />
        </TouchableOpacity>
      </View>
    );
  };
  /** -------- end handle filter dropdown and functions -------**/

  hashTagDetailNavigation = (value, slug) => {
    this.props.navigation.navigate(value, {slug: slug});
  };

  render() {
    const {hashtagList} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.stickyHeader()}
        {/***** Header starts *****/}
        {/* <View style={styles.headerView}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
          </TouchableOpacity>
          <Text
            style={[
              typography.Button_Lead,
              {
                color: COLORS.dark_600,
                marginLeft: 10,
                marginTop: Platform.OS === 'android' ? -8 : 0,
              },
            ]}>
            Hashtag
          </Text>
        </View> */}
        {/***** Header ends *****/}
        {/* {this.sortByComponent()} */}
        {/* ------ Start hashtag item on looping ------- */}
        {/* <View>
          <FlatList
            keyboardShouldPersistTaps="handled"
            keyExtractor={(item) => item.id}
            data={hashtagList}
            onEndReached={this.handleLoadMore}
            renderItem={(item) => (
              <HashTagItem
                item={item.item}
                hashTagDetailNavigation={this.hashTagDetailNavigation}
              />
            )}
          />
        </View> */}
        {/*-------  End hashtag item on looping ---------  */}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: '#00394D',
    flex: 1,
  },
  modalText: {
    fontSize: 14,
    color: '#154A59',
    fontFamily: 'Montserrat-SemiBold',
    marginLeft: 14,
  },
  termsmodal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '85%',
    paddingVertical: 5.5,
    paddingLeft: 30,
  },
  renderSharedPost: {
    width: '94%',
    marginTop: 5,
    marginLeft: 6,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: COLORS.grey_300,
    padding: 10,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  renderItemStyle: {
    backgroundColor: COLORS.white,
    paddingLeft: 10,
    paddingVertical: 10,
    marginHorizontal: 10,
    marginVertical: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: COLORS.grey_300,
  },
  unreadNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderRadius: 8,
    // borderBottomColor: COLORS.grey_400,
    // borderBottomWidth: 0.3,
    // height: 44
  },
  scrollViewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editIcon: {
    marginTop: 8,
    marginRight: 8,
  },
  editIcon2: {
    marginTop: Platform.OS === 'android' ? 8 : 0,
    // marginRight: 8
  },
  floatingIconText: {
    color: '#00394D',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 2,
  },
  floatingIcon: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 113,
    height: 44,
    borderRadius: 40,
    backgroundColor: '#D8DE21',
    position: 'absolute',
    bottom: 16,
    right: 12,
  },
  floatingIcon2: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 28,
    height: 42,
    borderRadius: 40,
    backgroundColor: '#1A4D5F80',
    position: 'absolute',
    bottom: 66,
    right: 12,
  },
  feedDetails: {
    marginTop: 20,
    marginLeft: 20,
    paddingBottom: 16,
  },
  selected: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#367681',
    marginRight: 15,
    height: 27,
    // width: 66,
    borderRadius: 16,
    textAlign: 'center',
  },
  notSelected: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    height: 27,
    // width: 66,
    borderWidth: 1,
    borderColor: '#698F8A',
    borderRadius: 16,
  },
  selectedText: {
    color: '#F7F7F5',
    fontSize: 12,
    paddingHorizontal: 14,
    fontFamily: 'Montserrat-Medium',
  },
  notSelectedText: {
    color: '#698F8A',
    fontSize: 12,
    paddingHorizontal: 14,
    fontFamily: 'Montserrat-Medium',
  },
  searchBar: {
    // marginTop: 8
  },
  explore: {
    flexDirection: 'row',
    paddingTop: 8,
    marginLeft: 15,
  },
  feed: {
    flexDirection: 'row',
    paddingTop: 8,
    marginLeft: 20,
    borderBottomColor: '#BFC52E',
    borderBottomWidth: 4,
    width: 100,
  },
  header: {
    flexDirection: 'row',
    borderBottomColor: '#154A59',
    borderBottomWidth: 1,
    paddingTop: 15,
    height: 65,
  },
  feedText: {
    color: '#FFFFFF',
    fontSize: 22,
    marginLeft: 5,
    fontFamily: 'Montserrat-SemiBold',
  },
  exploreText: {
    color: '#91B3A2',
    fontSize: 22,
    marginLeft: 5,
    fontFamily: 'Montserrat-Regular',
  },
  projItem: {
    marginHorizontal: 15,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    marginVertical: 7.5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
    paddingBottom: 20,
  },
});

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.otherProfileReducer.userDataProgress,
    user: state.otherProfileReducer.user,
    error: state.otherProfileReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    otherProfileRequest: (data) => dispatch(otherProfileRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OtherHashTags);
