import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, ActivityIndicator, SafeAreaView } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'

import { personalEducationRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import InstitutionLogo from '../../../../assets/InstitutionLogo.png'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultStyle from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

class SeeallEducation extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {

            this.props.route.params && this.props.route.params.id !== '' ?
                this.props.personalEducationRequest({ userId: this.props.route.params.id, otherUserId: value })
                : this.props.personalEducationRequest({ userId: value, otherUserId: '' })

            // this.props.personalEducationRequest({ userId: value, otherUserId: '' })
        }).catch((e) => {
            console.log(e)
        })

    }

    unixTime = (UNIX_timestamp) => {
        var date = new Date(UNIX_timestamp)
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        var year = date.getFullYear()
        var month = months[date.getMonth()]
        var day = date.getDate()
        return day + ' ' + month + ' ' + year
    }

    renderItem = (item, index) => {
        return (
            <View style={[styles.educationList, { borderBottomWidth: this.props.userEducation.body && this.props.userEducation.body.list && this.props.userEducation.body.list.length - 1 === index ? 0 : 0.2 }]}>
                <Image source={InstitutionLogo} style={{ height: 36, width: 36, borderRadius: 18, marginRight: 15 }} />
                <View>
                    <Text style={styles.eduTitle}>{item.institution}</Text>
                    <Text style={styles.eduSubTitle}>{item.specialisations}</Text>
                    {this.unixTime(item.endTime).split(' ').splice(1, 2).join(' ') !== 'Jan 1970' ?
                        <Text style={[defaultStyle.Note, { marginHorizontal: 0, color: COLORS.grey_400 }]}>{this.unixTime(item.startTime).split(' ').splice(1).join(' ')} to {this.unixTime(item.endTime).split(' ').splice(1, 2).join(' ')}</Text>
                        :
                        <Text style={[defaultStyle.Note, { marginHorizontal: 0, color: COLORS.grey_400 }]}>{this.unixTime(item.startTime).split(' ').splice(1).join(' ')} to Present</Text>
                    }
                </View>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView>

                <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 12 }] : styles.header}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.navigate("ProfileScreen")}>
                            <Icon name="Arrow-Left" size={14} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                        </TouchableOpacity>
                        <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_800, marginLeft: 5 }]}>
                            EDUCATION
                        </Text>
                    </View>
                </View>

                {this.props.userEducation.body ?
                    <FlatList
                        style={{ paddingTop: 15 }}
                        contentContainerStyle={{ paddingBottom: 70 }}
                        keyExtractor={(item) => item.id}
                        data={this.props.userEducation.body.list}
                        renderItem={({ item, index }) => (
                            this.renderItem(item, index)
                        )}
                    />
                    :
                    <ActivityIndicator size='small' color="#00394D" />
                }

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    educationList: {
        width: '85%',
        flexDirection: 'row',
        borderBottomColor: '#90949C',
        borderBottomWidth: 0.2,
        alignSelf: 'center',
        paddingVertical: 10,
        marginVertical: 5
    },
    eduTitle: {
        color: '#00394D',
        fontFamily: 'Montserrat-Bold',
        fontSize: 12,
        maxWidth: 220
    },
    eduSubTitle: {
        color: '#698F8A',
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        maxWidth: 220
    },
    eduDate: {
        color: '#90949C',
        fontStyle: 'italic',
        fontSize: 10
    }
})

const mapStateToProps = (state) => {
    return {
        userEducationProgress: state.personalProfileReducer.userEducationProgress,
        userEducation: state.personalProfileReducer.userEducation,
        errorEducation: state.personalProfileReducer.errorEducation
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        personalEducationRequest: (data) => dispatch(personalEducationRequest(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeeallEducation)
