import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    ImageBackground,
    Platform,
    ActivityIndicator,
    FlatList,
    Image,
    Modal,
    SafeAreaView,
    Linking,
    Share,
    Clipboard,
    TextInput,
    Pressable
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import axios from 'axios'
import Snackbar from 'react-native-snackbar'

import typography from '../../../Components/Shared/Typography'
import defaultProfile from '../../../../assets/defaultProfile.png'
import ConnectionList from '../../../Components/User/Profile/ConnectionList'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultStyle from '../../../Components/Shared/Typography'
import { REACT_APP_userServiceURL, REACT_APP_domainUrl } from '../../../../env.json'
import httpService from '../../../services/AxiosInterceptors'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import {
    personalProfileRequest,
    personalSkillsSpecializationRequest,
    personalRecentActivityRequest,
    personalExperienceRequest,
    personalEducationRequest,
    personalBusinessPageRequest,
    personalHashTagsRequest,
    personalConnectionInfoRequest
} from '../../../services/Redux/Actions/User/PersonalProfileActions'
import { userCircleRequest } from '../../../services/Redux/Actions/User/CircleActions'
import { endorsementRequest } from '../../../services/Redux/Actions/User/EndorsementActions'
import { causesRequest } from '../../../services/Redux/Actions/User/CausesActions'
import { connectsRequest } from '../../../services/Redux/Actions/User/ConnectsActions'

import { cloneDeep } from 'lodash'
import projectDefault from '../../../../assets/project-default.jpg'
import defaultCover from '../../../../assets/defaultCover.png'
import InstitutionLogo from '../../../../assets/InstitutionLogo.png'
import ProfileSvg from '../../../Components/User/Profile/ProfileSvg'
import RequestEndorsement from '../../../Components/User/Profile/RequestEndorsement'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

httpService.setupInterceptors()

class ConnectionDetails extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            requestEndorsementModalOpen: false,
            connected: false,
            followers: [],
            mutualConnects: [],
            isConnected: '',
            currentTab: this.props.route.params.currentTab,
            totalMutualConnects: 0,
            navigateToUserId: '',
            navigate: false,
            connects: [],
            searchText: '',
            currentSelected: {},
            optionsModalOpen: false,
            currentIndex: 0,
            otherUserId: '',
            username: '',
            otherUserProfile: ''
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value }, () => {
                this._unsubscribe = this.props.navigation.addListener('focus', () => {
                    if (this.props.route.params && this.props.route.params.id !== value) {
                        this.state.currentTab === 'connects' ? this.fetchConnects(this.props.route.params.id)
                            : this.state.currentTab === 'followers' ? this.fetchFollowers(this.props.route.params.id)
                                : this.state.currentTab === 'mutualConnects' ? this.getMutualConnects(value, this.props.route.params.id)
                                    : null
                        this.props.personalProfileRequest({ userId: this.props.route.params.id, otherUserId: value })
                        this.props.personalConnectionInfoRequest({ userId: this.props.route.params.id })
                        this.props.userConnectionInfo.body ? this.props.connectsRequest({ userId: this.props.route.params.id, size: this.props.userConnectionInfo.body.connections }) : null
                        this.props.userConnectionInfo.body ?
                            this.getFollowers(this.props.route.params.id, 'connects', this.props.userConnectionInfo.body.connections) : null

                    }
                    else {
                        this.state.currentTab === 'connects' ? this.fetchConnects(value) : this.fetchFollowers(value)
                        this.props.personalProfileRequest({ userId: value, otherUserId: '' })
                        this.props.personalConnectionInfoRequest({ userId: value })
                        this.props.userConnectionInfo.body ? this.props.connectsRequest({ userId: value, size: this.props.userConnectionInfo.body.connections }) : null
                        this.props.userConnectionInfo.body ?
                            this.getFollowers(value, 'connects', this.props.userConnectionInfo.body.connections) : null
                    }
                })
            })

            if (this.props.route.params && this.props.route.params.id !== value) {
                this.state.currentTab === 'connects' ? this.fetchConnects(this.props.route.params.id)
                    : this.state.currentTab === 'followers' ? this.fetchFollowers(this.props.route.params.id)
                        : this.state.currentTab === 'mutualConnects' ? this.getMutualConnects(value, this.props.route.params.id)
                            : null
                this.props.personalProfileRequest({ userId: this.props.route.params.id, otherUserId: value })
                this.props.personalConnectionInfoRequest({ userId: this.props.route.params.id })
                this.props.userConnectionInfo.body ? this.props.connectsRequest({ userId: this.props.route.params.id, size: this.props.userConnectionInfo.body.connections }) : null
                this.props.userConnectionInfo.body ?
                    this.getFollowers(this.props.route.params.id, 'connects', this.props.userConnectionInfo.body.connections) : null

            }
            else {
                this.state.currentTab === 'connects' ? this.fetchConnects(value) : this.fetchFollowers(value)
                this.props.personalProfileRequest({ userId: value, otherUserId: '' })
                this.props.personalConnectionInfoRequest({ userId: value })
                this.props.userConnectionInfo.body ? this.props.connectsRequest({ userId: value, size: this.props.userConnectionInfo.body.connections }) : null
                this.props.userConnectionInfo.body ?
                    this.getFollowers(value, 'connects', this.props.userConnectionInfo.body.connections) : null
            }

            // this.setState({ userId: value })
        }).catch((e) => {
            console.log(e)
        })
    }

    componentWillUnmount() {
        this._unsubscribe()
    }

    fetchConnects = (userId) => {
        axios({
            // url: `${REACT_APP_userServiceURL}/network/${userId}/connects?page=${0}&size=${1000}`,
            url: REACT_APP_userServiceURL + "/network/" +
                userId +
                "/connections" + "?requestingUserId=" + this.state.userId +
                "&page=" +
                0 +
                "&size=" +
                1000,
            method: "GET",
            headers: { "Content-Type": "application/json" },
            withCredentials: true,
        }).then((response) => {
            if (
                response &&
                response.data &&
                response.data.body &&
                response.data.status === "200 OK"
            ) {
                // console.log('connects', response.data.body.content[0])
                this.setState({ connects: response.data.body.content })
            }
        })
    }

    getFollowers = (id, type, total) => {
        axios({
            method: "get",
            url:
                REACT_APP_userServiceURL +
                "/network/" +
                id +
                '/' + type +
                "?page=" +
                0 +
                "&size=" +
                total + 10,
            headers: { "Content-Type": "application/json" },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.data && response.data.message === "Success!") {
                    console.log('```````````````````` Followers ````````````````````', response.data.body.content[0])
                    this.setState({ followers: response.data.body.content })

                }
            })
            .catch((err) => {
                console.log(err)
            })
    }

    fetchFollowers = (userId) => {
        axios({
            url: `${REACT_APP_userServiceURL}/network/${userId}/followers/?requestingUserId=${this.state.userId}&page=${0}&size=${1000}`,
            method: "GET",
            headers: { "Content-Type": "application/json" },
            withCredentials: true,
        }).then((response) => {
            if (
                response &&
                response.data &&
                response.data.body &&
                response.data.status === "200 OK"
            ) {
                // console.log('followers', response.data.body.content[0])
                this.setState({ connects: response.data.body.content })
            }
        })
    }

    changeState = (value) => {
        this.setState(value)
    }

    getMutualConnects = (userId, id) => {
        axios({
            method: "get",
            url:
                REACT_APP_userServiceURL +
                "/network/" +
                userId +
                '/mutualConnects/' + id +
                "?page=" +
                0 +
                "&size=" +
                500,
            headers: { "Content-Type": "application/json" },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.data && response.data.message === "Success!") {
                    console.log('getMutualConnects', response.data.body.content[0])
                    this.setState({ connects: response.data.body.content, totalMutualConnects: response.data.body.content.length })

                }
            })
            .catch((err) => {
                console.log('getMutualConnects', err)
            })
    }

    searchNames = (query) => {
        if (query === '') {
            return this.state.connects
        }

        const regex = new RegExp(`${query.trim()}`, 'i')

        return this.state.connects.filter(item => (item.firstName + ' ' + item.lastName).search(regex) >= 0)
    }

    handleConnectStatusChange = (userId) => {
        axios({
            method: 'post',
            url:
                REACT_APP_userServiceURL +
                '/graph/users/' +
                this.state.userId +
                '/invite/' +
                userId,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.status === 202) {
                    console.log(response.status)
                    this.setState({ connects: [] }, () =>
                        this.props.route.params && this.props.route.params.id !== '' ?
                            this.fetchFollowers(this.props.route.params.id)
                            : this.fetchFollowers(this.state.userId)
                    )
                }
            })
            .catch((err) => {
                // console.log(err)
                if (err.message === 'Request failed with status code 409') {
                    Snackbar.show({
                        backgroundColor: '#B22222',
                        text: 'You can send connection request after 3 days to this member',
                        duration: Snackbar.LENGTH_LONG,
                    });
                }
                if (err.message === 'Request failed with status code 400') {
                    Snackbar.show({
                        backgroundColor: '#B22222',
                        text: 'Cannot send request to an organization',
                        duration: Snackbar.LENGTH_LONG,
                    });
                }
            });
    };

    optionsModal = () => {
        return (

            <Modal visible={this.state.optionsModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={[defaultShape.Linear_Gradient_View, { bottom: 0 }]}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ optionsModalOpen: false })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
                    </TouchableOpacity>


                    <View style={defaultShape.Modal_Categories_Container}>

                        <TouchableOpacity style={[[defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, justifyContent: 'center', borderBottomWidth: 0 }]]} activeOpacity={0.6} onPress={() => {
                            this.setState({ optionsModalOpen: false }, () => this.props.navigation.navigate('Chats', {
                                userId: this.state.userId,
                                otherUserId: this.state.otherUserId,
                                lastActive: null,
                                grpType: 'Private',
                                name: this.state.username,
                                otherUserProfile: this.state.otherUserProfile,
                            }))
                        }}>
                            <Text style={[typography.H5, { color: COLORS.dark_900 }]}>{this.state.currentSelected.connectDepth === 1 ? 'Chat' : 'Send Message'}</Text>
                        </TouchableOpacity>


                        {this.state.currentSelected.connectDepth !== 1 && this.state.currentSelected.connectStatus && this.state.currentSelected.connectStatus.connectStatus !== 'DEADLOCK_PENDING_STATUS' &&

                            <TouchableOpacity style={[[defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, justifyContent: 'center', borderBottomWidth: 0 }]]} activeOpacity={0.6} onPress={() => {
                                this.setState({ optionsModalOpen: false }
                                    , () => {
                                        this.state.currentSelected.connectStatus.connectStatus === 'PENDING_CONNECT' ? this.props.navigation.navigate('MyNetworkStack', {
                                            screen: 'YourRequests',
                                        }) :
                                            this.state.currentSelected.connectStatus.connectStatus === 'REQUEST_RECEIVED' ? this.props.navigation.navigate('NetworkInvitationStack', {
                                                screen: 'InvitationsNetwork',
                                            }) : this.handleConnectStatusChange(this.state.otherUserId)
                                    })
                            }}>
                                <Text style={[typography.H5, { color: COLORS.dark_900 }]}>
                                    {this.state.currentSelected.connectStatus.connectStatus === 'REQUEST_RECEIVED' ||
                                        this.state.currentSelected.connectStatus.connectStatus === 'PENDING_CONNECT' ? 'Pending' :
                                        this.state.currentSelected.connectStatus.connectStatus === 'NOT_CONNECTED' || 'IGNORE_PENDING_CONNECT' ? 'Connect' : null
                                    }
                                </Text>

                            </TouchableOpacity>

                        }


                        <TouchableOpacity
                            style={[defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, justifyContent: 'center', borderBottomWidth: 0 }]} activeOpacity={0.6} onPress={() => {
                                this.setState({ optionsModalOpen: false }, () =>
                                    this.handleFollowUnfollow(
                                        this.state.currentSelected.followed,
                                        this.state.currentSelected.id,
                                        this.state.currentIndex,
                                        this.state.currentSelected,
                                    ))
                            }} >
                            <Text style={[typography.H5, { color: COLORS.dark_900 }]}>{this.state.currentSelected.followed ? 'Following' : 'Follow'}</Text>

                        </TouchableOpacity>

                    </View>

                </View>

            </Modal>

        )
    }

    handleFollowUnfollow = (isFollowed, userId, index, item) => {
        let tempConnectsList = cloneDeep(this.state.connects)
        let url;
        if (!isFollowed) {
            url =
                REACT_APP_userServiceURL +
                '/graph/users/' +
                this.state.userId +
                '/follows/' +
                userId;
        } else {
            url =
                REACT_APP_userServiceURL +
                '/graph/users/' +
                this.state.userId +
                '/unfollows/' +
                userId;
        }
        axios({
            method: 'post',
            url: url,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
        })
            .then((response) => {
                // console.log(response.status)
                if (response && response.status === 202) {
                    tempConnectsList[index].followed = !item.followed
                    this.setState({ connects: tempConnectsList })
                } else {
                    // console.log(response)
                }
            })
            .catch((err) => {
                console.log(err)
            })
    }

    render() {
        console.log('this.state.connects', this.state.connects[0])
        const { currentTab, followers, mutualConnects, totalMutualConnects } = this.state
        return (
            <SafeAreaView>
                {this.optionsModal()}
                <View style={{ backgroundColor: COLORS.white, position: 'absolute', top: 0, zIndex: 2 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: COLORS.white }}>

                        <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', padding: 10 }}>

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity activeOpacity={0.5}
                                    onPress={() => {
                                        this.props.navigation.goBack()
                                        // this.props.route.params && this.props.route.params.id !== '' ?
                                        //     this.props.navigation.navigate('ProfileScreen', { userId: this.props.route.params.id })
                                        //     : this.props.navigation.navigate('ProfileScreen')

                                    }}
                                    style={{ backgroundColor: COLORS.white, alignItems: 'center', justifyContent: 'center', height: 42, width: 42, borderRadius: 21, marginTop: 6 }}>
                                    <Icon name="Arrow-Left" size={16} color="#00394D" style={{ marginTop: 5 }} />
                                </TouchableOpacity>

                                {(this.props.user.body && this.props.user.body.originalProfileImage) ?
                                    <Image source={{ uri: this.props.user.body.originalProfileImage }} style={{ width: 30, height: 30, borderRadius: 15 }} />
                                    : null}

                                <View style={{ alignSelf: 'center', marginLeft: 8 }}>{this.props.user.body !== undefined ? <Text style={[defaultStyle.Title_1, { color: COLORS.dark_800, fontSize: 14, textTransform: 'capitalize' }]}>{this.props.user.body.userName} </Text> : <></>}

                                    {this.props.user.body ? <Text style={[defaultStyle.Subtitle_1, { color: COLORS.altgreen_400, fontSize: 12, marginTop: -6, textTransform: 'capitalize' }]}>{this.props.user.body.persona}</Text> : <></>}
                                </View>

                            </View>

                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-evenly', width: '100%', marginTop: 8 }}>

                        <TouchableOpacity onPress={() => {
                            this.setState({ currentTab: 'connects', connects: [], mutualConnects: [], totalMutualConnects: totalMutualConnects },
                                () => this.props.route.params && this.props.route.params.id !== '' ?
                                    this.fetchConnects(this.props.route.params.id)
                                    : this.fetchConnects(this.state.userId)
                            )
                        }} activeOpacity={0.8} style={{ width: '30%' }}>

                            <Text style={currentTab === 'connects' ? [defaultStyle.Body_1, styles.selectedTabText]
                                : [defaultStyle.Caption, styles.unSelectedTabText]}>{this.props.userConnectionInfo.body && this.props.userConnectionInfo.body.connections > 1 ? 'Connections' : 'Connection'}</Text>
                            <Text style={currentTab === 'connects' ? [defaultStyle.Caption, styles.selectedTabNo]
                                : [defaultStyle.Caption_Rescaled, styles.unSelectedTabNo]}>{this.props.userConnectionInfo.body ? this.props.userConnectionInfo.body.connections : "0"}</Text>
                            <View style={currentTab === 'connects' ? styles.borderBottom : styles.borderBottomUnselected}></View>
                        </TouchableOpacity>



                        {this.props.route.params.id !== this.state.userId &&
                            <TouchableOpacity onPress={() => {
                                this.setState({ currentTab: 'mutualConnects', connects: [], mutualConnects: [], totalMutualConnects: totalMutualConnects },
                                    () => this.getMutualConnects(this.state.userId, this.props.route.params.id))
                            }} activeOpacity={0.8} style={{ width: '30%' }}>

                                <Text style={currentTab === 'mutualConnects' ? [defaultStyle.Body_1, styles.selectedTabText]
                                    : [defaultStyle.Caption, styles.unSelectedTabText]}>{this.props.route.params.totalMutualConnects && this.props.route.params.totalMutualConnects > 1 ? 'Mutual Connects' : 'Mutual Connect'}</Text>
                                <Text style={currentTab === 'mutualConnects' ? [defaultStyle.Caption, styles.selectedTabNo]
                                    : [defaultStyle.Caption_Rescaled, styles.unSelectedTabNo]}>{this.props.route.params.totalMutualConnects}</Text>
                                <View style={currentTab === 'mutualConnects' ? styles.borderBottom : styles.borderBottomUnselected}></View>
                            </TouchableOpacity>}





                        <TouchableOpacity onPress={() => {
                            this.setState({ currentTab: 'followers', connects: [], mutualConnects: [], totalMutualConnects: totalMutualConnects },
                                () => this.props.route.params && this.props.route.params.id !== '' ?
                                    this.fetchFollowers(this.props.route.params.id)
                                    : this.fetchFollowers(this.state.userId)
                            )
                        }} activeOpacity={0.8} style={{ width: '30%' }}>

                            <Text style={currentTab === 'followers' ? [defaultStyle.Body_1, styles.selectedTabText]
                                : [defaultStyle.Caption, styles.unSelectedTabText]}>{this.props.userConnectionInfo.body && this.props.userConnectionInfo.body.followers > 1 ? 'Followers' : 'Follower'}</Text>
                            <Text style={currentTab === 'followers' ? [defaultStyle.Caption, styles.selectedTabNo]
                                : [defaultStyle.Caption_Rescaled, styles.unSelectedTabNo]}>{this.props.userConnectionInfo.body ? this.props.userConnectionInfo.body.followers : "0"}</Text>
                            <View style={currentTab === 'followers' ? styles.borderBottom : styles.borderBottomUnselected}></View>
                        </TouchableOpacity>


                    </View>

                </View>

                {/* <ConnectionList
                    changeState={this.changeState}
                    connectsData={this.props.userConnects.body ? this.props.userConnects.body.content.sort((a, b) => a.firstName.toUpperCase() > b.firstName.toUpperCase()) : null}
                    currentTab={currentTab}
                    followers={followers.sort((a, b) => a.firstName.toUpperCase() > b.firstName.toUpperCase())}
                    mutualConnects={mutualConnects.sort((a, b) => a.firstName.toUpperCase() > b.firstName.toUpperCase())}
                    totalMutualConnects={totalMutualConnects}
                /> */}

                <View style={{ height: '100%', paddingTop: '33%' }}>

                    {/* <View style={{
                        height: 60, backgroundColor: '#F7F7F5', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: -1
                    }}>

                        <TextInput
                            style={styles.textInput}
                            placeholderTextColor="#D9E1E4"
                            onChangeText={(value) => { this.setState({ searchText: value }) }}
                            color='#154A59'
                            placeholder='Search'
                            onFocus={() => this.setState({ searchIcon: false })}
                            onBlur={() => this.setState({ searchIcon: true })}
                            underlineColorAndroid="transparent"
                            ref={input => { this.textInput = input }}
                        />


                    </View> */}


                    <FlatList
                        keyboardShouldPersistTaps='handled'
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ paddingBottom: 80, paddingTop: 20 }}
                        style={{ height: '50%' }}
                        keyExtractor={(item) => item.id}
                        // data={this.searchNames(this.state.searchText)}
                        data={this.state.connects}
                        initialNumToRender={10}
                        renderItem={({ item, index }) => (

                            <TouchableOpacity activeOpacity={0.6} >
                                <View style={styles.item}>

                                    <Pressable style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}
                                        onPress={() =>
                                            item.id === this.state.userId ?
                                                this.props.navigation.navigate('ProfileScreen') :
                                                this.props.navigation.navigate('OtherProfileScreen', {
                                                    // screen: 'OtherProfileScreen',
                                                    userId: item.id,
                                                    // unsubscribe: 'connectionDetail' 
                                                })
                                        }
                                    >
                                        <Image source={item.personalInfo.profileImage ? { uri: item.personalInfo.profileImage } : defaultProfile} style={[styles.image, { marginLeft: '7%' }]} />

                                        <TouchableOpacity
                                            activeOpacity={0.6}
                                            onPress={() =>
                                                item.id === this.state.userId ?
                                                    this.props.navigation.navigate('ProfileScreen') :
                                                    this.props.navigation.navigate('OtherProfileScreen', {
                                                        // screen: 'OtherProfileScreen',
                                                        userId: item.id,
                                                        // unsubscribe: true 
                                                    })}
                                            style={styles.nameMsg}>

                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Text numberOfLines={1} style={styles.name}>{item.id === this.state.userId ? 'You' : item.username} </Text>
                                                <Text style={[{ color: '#888', fontSize: 14 }, typography.Note2]}>
                                                    {
                                                        item.id === this.state.userId ? '' :
                                                            item.connectDepth === 1 ? '• 1st' :
                                                                item.connectDepth === 2 ? '• 2nd' :
                                                                    item.connectDepth === -1 || item.connectDepth === 0 ? '' : ""
                                                    }
                                                </Text>
                                            </View>
                                            {item.personalInfo && item.personalInfo.persona && item.id !== this.state.userId ?

                                                <Text numberOfLines={1} style={[typography.Caption, { color: '#698f8a', maxWidth: 200 }]}>{item.personalInfo.persona.charAt(0).toUpperCase() + item.personalInfo.persona.slice(1)}</Text> :
                                                <></>}
                                            {item.addressDetail && item.addressDetail.country && item.id !== this.state.userId ?

                                                <Text numberOfLines={1} style={[typography.Caption, { color: '#99b2bf', maxWidth: 200 }]}>{item.addressDetail.country}</Text> :
                                                <></>}

                                        </TouchableOpacity>

                                    </Pressable>

                                    {item.id !== this.state.userId &&
                                        <TouchableOpacity
                                            onPress={() => this.setState({
                                                optionsModalOpen: true, pressedUserId: item.id, currentSelected: item, currentIndex: index,
                                                otherUserId: item.id,
                                                username: item.username,
                                                otherUserProfile:
                                                    item.personalInfo && item.personalInfo.profileImage
                                                        ? item.personalInfo.profileImage
                                                        : null,
                                            }, () => console.log(item))}
                                            style={{ width: 34, height: 34, justifyContent: 'center', alignItems: 'center', position: 'absolute', right: 16 }} activeOpacity={0.5} >
                                            <Icon name='Kebab' size={14} color={COLORS.grey_350} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                                        </TouchableOpacity>}

                                </View>

                            </TouchableOpacity>


                        )}
                    />
                </View>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    selectedTabText: {
        color: COLORS.dark_900, textAlign: 'center', fontWeight: 'bold'
    },
    item: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'space-around',
        backgroundColor: '#F7F7F500',
        width: '100%',
        height: 48,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',
        marginBottom: 18
    },
    image: {
        height: 32,
        width: 32,
        borderRadius: 16
    },
    name: {
        fontWeight: '700',
        color: '#4B4F56',
        fontSize: 14,
        textAlign: 'left',
        maxWidth: 200,
        textTransform: 'capitalize'
    },
    message: {
        color: '#698F8A',
        fontSize: 10.5,
    },
    nameMsg: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: '6%'
    },
    selectedTabNo: {
        color: COLORS.altgreen_400, textAlign: 'center', fontWeight: '700'
    },
    unSelectedTabText: {
        color: COLORS.altgreen_300, marginBottom: 5, textAlign: 'center'
    },
    unSelectedTabNo: {
        color: COLORS.grey_400, marginTop: -5, textAlign: 'center', marginBottom: 8
    },
    borderBottom: {
        width: 100, height: 3, backgroundColor: COLORS.dark_800, borderTopLeftRadius: 4, borderTopRightRadius: 4, position: 'absolute', bottom: 0, alignSelf: 'center'
    },
    borderBottomUnselected: {
        width: 100, height: 3, backgroundColor: COLORS.white, borderTopLeftRadius: 4, borderTopRightRadius: 4, position: 'absolute', bottom: 0, alignSelf: 'center'
    },
    textInput: {
        fontSize: 17,
        fontFamily: 'Montserrat-Medium',
        padding: 10,
        backgroundColor: '#FFF',
        width: '90%',
        zIndex: 1,
        textAlign: 'center',
        borderRadius: 8,
        borderColor: '#D9E1E4',
        borderWidth: 1,
        shadowColor: '#36768140',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
})

const mapStateToProps = (state) => {
    return {
        userDataProgress: state.personalProfileReducer.userDataProgress,
        user: state.personalProfileReducer.user,
        error: state.personalProfileReducer.error,

        userConnectionInfoProgress: state.personalProfileReducer.userConnectionInfoProgress,
        userConnectionInfo: state.personalProfileReducer.userConnectionInfo,
        errorConnectionInfo: state.personalProfileReducer.errorConnectionInfo,

        userConnectsProgress: state.connectsReducer.userConnectsProgress,
        userConnects: state.connectsReducer.userConnects,
        errorConnects: state.connectsReducer.errorConnects
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
        personalConnectionInfoRequest: (data) => dispatch(personalConnectionInfoRequest(data)),
        connectsRequest: (data) => dispatch(connectsRequest(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConnectionDetails)
