import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Platform,
  ActivityIndicator,
  FlatList,
  Image,
  Modal,
  SafeAreaView,
  Linking,
  Share,
  Clipboard
} from 'react-native'
import { TextInput } from 'react-native-paper'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import axios from 'axios'
import Snackbar from 'react-native-snackbar'

import defaultBusiness from '../../../../assets/DefaultBusiness.png'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultStyle from '../../../Components/Shared/Typography'
import { REACT_APP_userServiceURL, REACT_APP_domainUrl } from '../../../../env.json'
import httpService from '../../../services/AxiosInterceptors'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import {
  companyProfileRequest,
  companySkillsSpecializationRequest,
  companyRecentActivityRequest,
  companyExperienceRequest,
  companyEducationRequest,
  companyBusinessPageRequest,
  companyHashTagsRequest,
  companyConnectionInfoRequest,
  companyAddressRequest
} from '../../../services/Redux/Actions/User/CompanyProfileActions'
import { userCircleRequest } from '../../../services/Redux/Actions/User/CircleActions'
import { endorsementRequest } from '../../../services/Redux/Actions/User/EndorsementActions'
import { causesRequest } from '../../../services/Redux/Actions/User/CausesActions'
import { connectsRequest } from '../../../services/Redux/Actions/User/ConnectsActions'

import projectDefault from '../../../../assets/project-default.jpg'
import defaultCover from '../../../../assets/defaultCover.png'
import InstitutionLogo from '../../../../assets/InstitutionLogo.png'
import ProfileSvg from '../../../Components/User/Profile/ProfileSvg'
import RequestEndorsement from '../../../Components/User/Profile/RequestEndorsement'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

httpService.setupInterceptors()

class Profile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userData: {},
      coverImage: '',
      userId: '',
      skills: [],
      connectionInfo: {},
      specializationExpand: false,
      skillExpand: false,
      interestExpand: false,
      optionsModalOpen: false,
      requestEndorsementModalOpen: false,
      modalOpen: false,
      selectedExperienceType: 'ALL',
      skillsModalOpen: false,
      specializationModalOpen: false,
      interestsModalOpen: false,
      readMore: false,
      readMoreExperienceId: '',
      currentScrollPosition: 0,
      paramsId: '',
      connected: false,
      following: 0,
      hashTagModalOpen: false,
      shareModalOpen: false,
      contactInfoModalOpen: false,
      isConnected: '',
      mutualConnects: [],
      employees: [],
      followers: [],
      reasonForReportingModalOpen: false,
      reasonForReporting: 'FAKE_SPAM_OR_SCAM',
      description: '',
      skillsSuggestions: [],
      endorseOrganizationModalOpen: false,
      hashTags: [],
      query2: '',
    }
  }

  componentDidMount() {

    // console.log('================================ this.props.route.params =======================',this.props.route.params)
    this.props.route.params ? this.setState({ paramsId: this.props.route.params.userId }) : null

    AsyncStorage.getItem("refreshToken").then((value) => {
      if (value === null) {
        this.props.navigation.replace("Login")
      }
    })

    AsyncStorage.getItem("userId").then((value) => {

      this.setState({ userId: value })
      // console.log('================================ value =======================', value)

      if (this.props.route.params) {
        this.props.companyProfileRequest({ userId: this.props.route.params.userId, otherUserId: value })
        this.props.companyAddressRequest({ userId: this.props.route.params.userId, otherUserId: value })
        this.props.companySkillsSpecializationRequest({ userId: this.props.route.params.userId, otherUserId: value })
        this.props.companyRecentActivityRequest({ userId: this.props.route.params.userId, otherUserId: value })
        this.props.companyExperienceRequest({ userId: this.props.route.params.userId, otherUserId: value })
        this.props.companyEducationRequest({ userId: this.props.route.params.userId, otherUserId: value })
        this.props.userCircleRequest({ userId: this.props.route.params.userId, otherUserId: value })
        this.props.endorsementRequest({ userId: this.props.route.params.userId, otherUserId: value })
        this.props.companyBusinessPageRequest({ userId: this.props.route.params.userId, otherUserId: value })
        this.props.causesRequest({ userId: this.props.route.params.userId, otherUserId: value })
        this.props.companyHashTagsRequest({ userId: this.props.route.params.userId, otherUserId: value })
        this.props.companyConnectionInfoRequest({ userId: this.state.paramsId })
        this.props.userConnectionInfo.body ? this.props.connectsRequest({ userId: this.props.route.params.userId, size: this.props.userConnectionInfo.body.connections }) : null
        this.getConnectStatus()
        this.getMutualConnects()
        this.getEmployees()
      }
      else {
        this.props.companyProfileRequest({ userId: value, otherUserId: '' })
        this.props.companyAddressRequest({ userId: value, otherUserId: '' })
        this.props.companySkillsSpecializationRequest({ userId: value, otherUserId: '' })
        this.props.companyRecentActivityRequest({ userId: value, otherUserId: '' })
        this.props.companyExperienceRequest({ userId: value, otherUserId: '' })
        this.props.companyEducationRequest({ userId: value, otherUserId: '' })
        this.props.userCircleRequest({ userId: value, otherUserId: '' })
        this.props.endorsementRequest({ userId: value, otherUserId: '' })
        this.props.companyBusinessPageRequest({ userId: value, otherUserId: '' })
        this.props.causesRequest({ userId: value, otherUserId: '' })
        this.props.companyHashTagsRequest({ userId: value, otherUserId: '' })
        this.props.companyConnectionInfoRequest({ userId: value })
        this.getEmployees2()
      }

    }).catch((e) => {
      // console.log(e)
    })

    this.props.route.params && this.props.userConnectionInfo.body ? this.getFollowers() : null
    this.getMasterConfig()
  }

  changeState = (value) => {
    this.setState(value)
  }

  skillExpand = () => {
    this.setState({ skillExpand: !this.state.skillExpand })
  }

  specializationExpand = () => {
    this.setState({ specializationExpand: !this.state.specializationExpand })
  }

  interestExpand = () => {
    this.setState({ interestExpand: !this.state.interestExpand })
  }

  shouldComponentUpdate(nextProps, nextState) {

    if (nextProps.user !== this.props.user) {
      return true
    }
    if (nextProps.userSkillsSpecialization !== this.props.userSkillsSpecialization) {
      return true
    }
    if (nextProps.userRecentActivity !== this.props.userRecentActivity) {
      return true
    }
    if (nextProps.userExperience !== this.props.userExperience) {
      return true
    }
    if (nextProps.userEducation !== this.props.userEducation) {
      return true
    }
    if (nextProps.userCircle !== this.props.userCircle) {
      return true
    }
    if (nextProps.userEndorsement !== this.props.userEndorsement) {
      return true
    }
    if (nextProps.userBusinessPage !== this.props.userBusinessPage) {
      return true
    }
    if (nextProps.userCauses !== this.props.userCauses) {
      return true
    }
    if (nextProps.userHashTags !== this.props.userHashTags) {
      return true
    }
    if (nextProps.userConnectionInfo !== this.props.userConnectionInfo) {
      return true
    }
    if (nextProps.userConnects !== this.props.userConnects) {
      return true
    }

    if (nextState.query2 !== this.state.query2) {
      return true
    }
    if (nextState.hashTags !== this.state.hashTags) {
      return true
    }
    if (nextState.endorseOrganizationModalOpen !== this.state.endorseOrganizationModalOpen) {
      return true
    }
    if (nextState.skillsSuggestions !== this.state.skillsSuggestions) {
      return true
    }
    if (nextState.description !== this.state.description) {
      return true
    }
    if (nextState.reasonForReporting !== this.state.reasonForReporting) {
      return true
    }
    if (nextState.reasonForReportingModalOpen !== this.state.reasonForReportingModalOpen) {
      return true
    }
    if (nextState.followers !== this.state.followers) {
      return true
    }
    if (nextState.employees !== this.state.employees) {
      return true
    }
    if (nextState.mutualConnects !== this.state.mutualConnects) {
      return true
    }
    if (nextState.isConnected !== this.state.isConnected) {
      return true
    }
    if (nextState.readMoreExperienceId !== this.state.readMoreExperienceId) {
      return true
    }
    if (nextState.contactInfoModalOpen !== this.state.contactInfoModalOpen) {
      return true
    }
    if (nextState.shareModalOpen !== this.state.shareModalOpen) {
      return true
    }
    if (nextState.hashTagModalOpen !== this.state.hashTagModalOpen) {
      return true
    }
    if (nextState.paramsId !== this.state.paramsId) {
      return true
    }
    if (nextState.userData !== this.state.userData) {
      return true
    }
    if (nextState.userId !== this.state.userId) {
      return true
    }
    if (nextState.coverImage !== this.state.coverImage) {
      return true
    }
    if (nextState.skills !== this.state.skills) {
      return true
    }
    if (nextState.connectionInfo !== this.state.connectionInfo) {
      return true
    }
    if (nextState.specializationExpand !== this.state.specializationExpand) {
      return true
    }
    if (nextState.skillExpand !== this.state.skillExpand) {
      return true
    }
    if (nextState.interestExpand !== this.state.interestExpand) {
      return true
    }
    if (nextState.optionsModalOpen !== this.state.optionsModalOpen) {
      return true
    }
    if (nextState.requestEndorsementModalOpen !== this.state.requestEndorsementModalOpen) {
      return true
    }
    if (nextState.modalOpen !== this.state.modalOpen) {
      return true
    }
    if (nextState.skillsModalOpen !== this.state.skillsModalOpen) {
      return true
    }
    if (nextState.specializationModalOpen !== this.state.specializationModalOpen) {
      return true
    }
    if (nextState.interestsModalOpen !== this.state.interestsModalOpen) {
      return true
    }
    if (nextState.readMore !== this.state.readMore) {
      return true
    }
    if (nextState.currentScrollPosition !== this.state.currentScrollPosition) {
      return true
    }
    if (nextState.connected !== this.state.connected) {
      return true
    }
    if (nextState.following !== this.state.following) {
      return true
    }

    return false
  }

  unixTime = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp)
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    var year = date.getFullYear()
    var month = months[date.getMonth()]
    var day = date.getDate()
    return day + ' ' + month + ' ' + year
  }

  renderItemBusinessPage = (item) => {
    return (

      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => this.props.navigation.navigate('CompanyProfileScreen', { userId: item.companyId })}
        style={{ marginLeft: 10 }}
      >

        <View style={styles.businessPageList}>

          {item.profileImageUrl ?
            <Image source={{ uri: item.profileImageUrl }} style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }} />
            :
            <Image source={defaultBusiness} style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }} />
          }

          <View>
            <Text style={{ color: '#00394D', fontFamily: 'Montserrat-Bold', fontSize: 12 }}>
              {item.companyName.charAt(0).toUpperCase() + item.companyName.slice(1)}
            </Text>
            <Text style={{ color: '#91B3A2', fontFamily: 'Montserrat-SemiBold', fontSize: 12 }}>
              {item.country}
            </Text>
          </View>

        </View>

        <View style={styles.borderView}></View>

      </TouchableOpacity>
    )
  }

  modal = () => {
    return (

      <Modal visible={this.state.modalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <View style={styles.modalCountry}>

          <View style={{
            width: '100%',
            height: 500,
            position: 'absolute',
            bottom: 0,
            alignSelf: 'center'
          }}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={{
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6
            }}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={styles.crossButtonContainer}
            onPress={() => this.setState({ modalOpen: false })} >
            <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon} />
          </TouchableOpacity>

          <View style={[styles.modalCategoriesContainer, { height: 250 }]}>

            <TouchableOpacity style={styles.termsModalNew} activeOpacity={0.6} onPress={() => { this.setState({ modalOpen: false, selectedExperienceType: 'ALL' }) }} >
              <Text style={[styles.modalText, { marginLeft: 0 }]}>All</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.termsModalNew} activeOpacity={0.6} onPress={() => { this.setState({ modalOpen: false, selectedExperienceType: 'JOB' }) }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>Jobs</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.termsModalNew} activeOpacity={0.6} onPress={() => { this.setState({ modalOpen: false, selectedExperienceType: 'ASSIGNMENT' }) }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>Assignments</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.termsModalNew} activeOpacity={0.6} onPress={() => { this.setState({ modalOpen: false, selectedExperienceType: 'EVENT' }) }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>Events</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.termsModalNew, { borderBottomWidth: 0 }]} activeOpacity={0.6} onPress={() => { this.setState({ modalOpen: false, selectedExperienceType: 'TRAINING' }) }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>Trainings</Text>
            </TouchableOpacity>

          </View>

        </View>

      </Modal>

    )
  }


  requestEndorsementModal = () => {
    return (
      <Modal visible={this.state.requestEndorsementModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <RequestEndorsement changeState={this.changeState} connectsData={this.state.followers ? this.state.followers.sort((a, b) => a.firstName.toUpperCase() > b.firstName.toUpperCase()) : null} />
      </Modal>
    )
  }


  skillsModal = () => {
    return (
      <Modal visible={this.state.skillsModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

        <View style={{ marginTop: 'auto' }}>

          <View style={{
            width: '100%',
            height: 500,
            position: 'absolute',
            bottom: 0,
            alignSelf: 'center'
          }}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={{
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6
            }}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={styles.crossButtonContainer}
            onPress={() => this.setState({ skillsModalOpen: false })} >
            <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon} />
          </TouchableOpacity>

          <View style={[styles.modalCategoriesContainer, { height: 350 }]}>

            {this.props.user.body ?
              <FlatList
                columnWrapperStyle={{ flexDirection: "row", flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', paddingVertical: 6 }}
                numColumns={3}
                showsVerticalScrollIndicator={false}
                data={this.props.user.body.skills}
                keyExtractor={(item) => item}
                renderItem={({ item }) => (
                  <View style={styles.itemBox}>
                    <Text numberOfLines={1} style={{ fontSize: 12, color: '#367681', fontFamily: 'Montserrat-SemiBold' }}>
                      {item}
                    </Text>
                  </View>
                )}
              /> : <></>}

          </View>

        </View>

      </Modal>
    )
  }


  specializationModal = () => {
    return (
      <Modal visible={this.state.specializationModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

        <View style={{ marginTop: 'auto' }}>

          <View style={{
            width: '100%',
            height: 500,
            position: 'absolute',
            bottom: 0,
            alignSelf: 'center'
          }}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={{
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6
            }}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={styles.crossButtonContainer}
            onPress={() => this.setState({ specializationModalOpen: false })} >
            <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon} />
          </TouchableOpacity>

          <View style={[styles.modalCategoriesContainer, { height: 350 }]}>

            {this.props.user.body ?
              <FlatList
                columnWrapperStyle={{ flexDirection: "row", flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', paddingVertical: 6 }}
                numColumns={3}
                showsVerticalScrollIndicator={false}
                data={this.props.user.body.specialities}
                keyExtractor={(item) => item}
                renderItem={({ item }) => (
                  <View style={styles.itemBox}>
                    <Text numberOfLines={1} style={{ fontSize: 12, color: '#367681', fontFamily: 'Montserrat-SemiBold' }}>
                      {item}
                    </Text>
                  </View>
                )}
              /> : <ActivityIndicator size='small' color="#fff" />}

          </View>

        </View>

      </Modal>
    )
  }


  interestsModal = () => {
    return (
      <Modal visible={this.state.interestsModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

        <View style={{ marginTop: 'auto' }}>

          <View style={{
            width: '100%',
            height: 500,
            position: 'absolute',
            bottom: 0,
            alignSelf: 'center'
          }}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={{
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6
            }}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={styles.crossButtonContainer}
            onPress={() => this.setState({ interestsModalOpen: false })} >
            <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon} />
          </TouchableOpacity>

          <View style={[styles.modalCategoriesContainer, { height: 350 }]}>

            {this.props.user.body ? <FlatList
              columnWrapperStyle={{ flexDirection: "row", flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', paddingVertical: 6 }}
              numColumns={3}
              showsVerticalScrollIndicator={false}
              data={this.props.user.body.interests}
              keyExtractor={(item) => item}
              renderItem={({ item }) => (
                <View style={styles.itemBox}>
                  <Text numberOfLines={1} style={{ fontSize: 12, color: '#367681', fontFamily: 'Montserrat-SemiBold' }}>
                    {item}
                  </Text>
                </View>
              )}
            /> : <ActivityIndicator size='small' color="#fff" />}

          </View>

        </View>

      </Modal>
    )
  }

  verifyReported = () => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/verifyAlreadyReported?reporterId='
        + this.state.userId + '&entityId=' + this.props.route.params.userId,
      withCredentials: true
    }).then(response => {
      if (response && response.status === 200 && response.data && response.data.body) {
        // console.log(response.data.body.reported)
        !response.data.body.reported ?
          this.setState({ isReported: response.data.body.reported, reasonForReportingModalOpen: true })
          :
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: "Your report request was already taken",
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          })

      } else {
        console.log(response)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  sendRequest = () => {
    let postBody = {
      "userId": this.state.userId,
      "companyId": this.props.route.params.userId,
      "status": 'REQUEST_SENT'
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/employee/save',
      headers: { 'Content-Type': 'application/json' },
      data: postBody,
      withCredentials: true
    }).then((response) => {
      let res = response.data;
      if (res.status === '201 CREATED') {
        this.getEmployeeDetail();
      }
    }).catch((err) => {
      if (err && err.response && err.response.data) {
        this.setState({
          isLoaded: true,
          error: { message: err.response.data.message, err: err.response }
        });
      }
    });


  }

  optionsModal = () => {
    return (

      <Modal visible={this.state.optionsModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <View style={styles.modalCountry}>

          <View style={styles.linearGradientView2}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={styles.linearGradient2}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={styles.crossButtonContainer}
            onPress={() => this.setState({ optionsModalOpen: false })} >
            <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon} />
          </TouchableOpacity>

          {this.props.route.params ?
            <View style={styles.modalCategoriesContainer}>

              <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400, textAlign: 'center' }]}>Organization page</Text>

              {/* <TouchableOpacity style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal, { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' }]} activeOpacity={0.6} onPress={() => this.setState({ optionsModalOpen: false }, () => this.sendRequest())} >
                <Icon name="Administrator" size={16} color="#154A59" style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                <Text style={styles.modalText}>Employee Request</Text>
              </TouchableOpacity> */}

              <TouchableOpacity style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal, { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' }]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false, shareModalOpen: true }) }}>
                <Icon name='Share' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 10 } : {}} />
                <Text style={styles.modalText}>Share Organization Page</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingTop: 25, paddingBottom: 15 }] : styles.termsmodal, { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' }]} activeOpacity={0.6} onPress={() => this.setState({ optionsModalOpen: false }, () => this.props.navigation.navigate('Chats', {
                userId: this.state.userId,
                otherUserId: this.props.route.params.userId,
                lastActive: null,
                grpType: 'Private',
                name: this.props.user.body && this.props.user.body.userName,
                otherUserProfile:
                  this.props.user.body && this.props.user.body.originalProfileImage
                    ? this.props.user.body.originalProfileImage
                    : 'https://cdn.dscovr.com/images/DefaultBusiness.png'
              }))}>
                <Icon name='Messeges_F' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                <Text style={styles.modalText}>Send Message</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal, { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' }]}
                activeOpacity={0.6}
                onPress={() => this.setState({ optionsModalOpen: false, endorseOrganizationModalOpen: true })} >
                <Icon name="Feedback" size={16} color="#154A59" style={Platform.OS === 'android' ? { marginTop: 12 } : {}} />
                <Text style={styles.modalText}>Endorse Organization</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal, { borderBottomWidth: 0, borderBottomColor: '#E8ECEB' }]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false, contactInfoModalOpen: true }) }}>
                <Icon name='Email_At' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 10 } : {}} />
                <Text style={styles.modalText}>View Contact Info</Text>
              </TouchableOpacity>

              {/* <TouchableOpacity
                style={Platform.OS === 'ios' ? [styles.termsmodal, { paddingTop: 25, paddingBottom: 15 }] : styles.termsmodal}
                activeOpacity={0.6}
                onPress={() => { this.setState({ optionsModalOpen: false }, () => this.verifyReported()) }}>
                <Icon name='Caution' size={16} color='#913838' style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                <Text style={styles.modalText}>Report Organization</Text>
              </TouchableOpacity> */}

            </View>
            :
            <View style={styles.modalCategoriesContainer}>

              <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400, textAlign: 'center', paddingVertical: 6 }]}>Organization page</Text>

              <TouchableOpacity style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal, { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' }]} activeOpacity={0.6} onPress={() => this.setState({ optionsModalOpen: false, shareModalOpen: true })} >
                <Icon name="Share" size={16} color="#154A59" style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                <Text style={styles.modalText}>Share Organization Page</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal, { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' }]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false, contactInfoModalOpen: true }) }}>
                <Icon name='Email_At' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                <Text style={styles.modalText}>View Contact Info</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal, { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' }]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false }) }}>
                <Icon name='Setting' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                <Text style={styles.modalText}>Page Settings</Text>
              </TouchableOpacity>

              <TouchableOpacity style={Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false }) }}>
                <Icon name='Help' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                <Text style={styles.modalText}>Get Support</Text>
              </TouchableOpacity>

            </View>
          }

        </View>

      </Modal>

    )
  }

  renderItem = (experience, index) => {

    if (index + 1 >= 6) return <TouchableOpacity onPress={() => this.props.navigation.navigate("SeeallExperienceScreen", { id: this.state.paramsId })}
      key={experience.id} activeOpacity={0.5} style={{ marginHorizontal: 16, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', height: 26, width: 70, borderWidth: 1, borderColor: '#698F8A', borderRadius: 13, paddingHorizontal: 6, paddingVertical: 2 }}>
      <Text style={{ color: '#698F8A', fontSize: 11, fontFamily: 'Montserrat-Medium' }}>See All</Text>
    </TouchableOpacity>

    // else if (this.props.userExperience.body.experience.content.length < 6 && index + 1 === this.props.userExperience.body.experience.content.length ) return <TouchableOpacity onPress={() => this.props.navigation.navigate("SeeallExperienceScreen")}
    //   key={experience.id} activeOpacity={0.5} style={{ marginHorizontal: 16, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', height: 26, width: 70, borderWidth: 1, borderColor: '#698F8A', borderRadius: 13, paddingHorizontal: 6, paddingVertical: 2 }}>
    //   <Text style={{ color: '#698F8A', fontSize: 11, fontFamily: 'Montserrat-Medium' }}>See All</Text>
    // </TouchableOpacity>

    else return <View style={{ backgroundColor: '#FFFFFF', marginHorizontal: 5, width: 244, borderRadius: 6, marginVertical: 8, shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5, }} key={experience.id}>
      <View style={{ paddingBottom: 14, marginRight: 10, padding: 6, width: 244, borderRadius: 6 }} >

        <View style={{ flexDirection: 'row', marginRight: 10, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>

          {experience.coverImageUrl !== null ? <Image source={{ uri: experience.coverImageUrl }} style={{ width: 50, height: 50, borderRadius: 5 }} />
            :
            <Image source={projectDefault} style={{ width: 50, height: 50, borderRadius: 5 }} />
          }
          <View style={{ width: 150, backgroundColor: '#FFF', justifyContent: 'center', borderTopRightRadius: 5, borderBottomRightRadius: 6 }}>
            <Text style={[defaultStyle.Title_2, { marginHorizontal: 12, color: COLORS.primarydark }]}>{experience.title}</Text>
            <Text style={[defaultStyle.Caption, { marginLeft: 12, color: COLORS.altgreen_400 }]}>{experience.companyName}</Text>
            <Text style={[defaultStyle.Body_1, { marginHorizontal: 12, color: COLORS.grey }]}>{experience.location.city}, {experience.location.country}</Text>
          </View>
        </View>

        <View style={{ borderColor: '#E8ECEB', borderWidth: 0.45, width: '105%', marginVertical: 10, marginLeft: -6 }}></View>
        <Text numberOfLines={this.state.readMoreExperienceId !== experience.id ? 3 : 20} style={[defaultStyle.Body_2, { marginHorizontal: 11, paddingBottom: 36, color: COLORS.grey_500 }]}>{experience.description}</Text>
        <TouchableOpacity style={{ height: 30, width: 80, marginLeft: 10, marginTop: -38, marginBottom: 16 }}
          onPress={() => this.setState({ readMoreExperienceId: this.state.readMoreExperienceId.length > 0 ? '' : experience.id })}>
          <Text style={{ color: COLORS.green_500, fontWeight: '700' }}>
            {experience.description.length > 105 && this.state.readMoreExperienceId !== experience.id ? 'Read more'
              : this.state.readMoreExperienceId === experience.id ? 'Read less'
                : null}
          </Text>
        </TouchableOpacity>

      </View>
      <View style={{ flexDirection: 'row', marginRight: 10, backgroundColor: '#E7F3E3', borderBottomRightRadius: 6, borderBottomLeftRadius: 6, width: 244, height: 40, justifyContent: 'space-around', alignItems: 'center', position: 'absolute', bottom: -6 }}>
        {this.unixTime(experience.startTime).split(' ').splice(1).join(' ') !== this.unixTime(experience.endTime).split(' ').splice(1).join(' ') && this.unixTime(experience.endTime).split(' ').splice(1).join(' ') !== 'Jan 1970' ?
          <Text style={[defaultStyle.Body_2_italic, { color: COLORS.altgreen_400 }]}>{this.unixTime(experience.startTime).split(' ').splice(1).join(' ')} to {this.unixTime(experience.endTime).split(' ').splice(1, 2).join(' ')}</Text>
          : this.unixTime(experience.endTime).split(' ').splice(1).join(' ') === 'Jan 1970' ?
            <Text style={[defaultStyle.Body_2_italic, { color: COLORS.altgreen_400 }]}>{this.unixTime(experience.startTime).split(' ').splice(1).join(' ')} - Running</Text>
            : <Text style={[defaultStyle.Body_2_italic, { color: COLORS.altgreen_400 }]}>{this.unixTime(experience.startTime).split(' ').splice(1).join(' ')}</Text>
        }
        <Text style={[defaultStyle.Note, { color: COLORS.altgreen_400, borderWidth: 1, borderColor: '#91B3A2', borderRadius: 4, paddingHorizontal: 6, paddingVertical: 2 }]}>{experience.type}</Text>
      </View>
    </View>
  }

  handleScroll = (event) => {
    // console.log(event.nativeEvent.contentOffset.y)
    this.setState({ currentScrollPosition: event.nativeEvent.contentOffset.y })
  }

  hashTagModal = () => {
    return (
      <Modal visible={this.state.hashTagModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

        <View style={{ marginTop: 'auto' }}>

          <View style={{
            width: '100%',
            height: 500,
            position: 'absolute',
            bottom: 0,
            alignSelf: 'center'
          }}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={{
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6
            }}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={styles.crossButtonContainer}
            onPress={() => this.setState({ hashTagModalOpen: false })} >
            <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon} />
          </TouchableOpacity>

          <View style={[styles.modalCategoriesContainer, { height: 350 }]}>

            {this.props.userHashTags.body ?
              <FlatList
                columnWrapperStyle={{ flexDirection: "row", flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', paddingVertical: 6 }}
                numColumns={3}
                showsVerticalScrollIndicator={false}
                data={this.props.userHashTags.body.hashTags}
                keyExtractor={(item) => item}
                renderItem={({ item }) => (
                  <View style={[styles.itemBox, { marginHorizontal: 10 }]}>
                    <Text numberOfLines={1} style={{ fontSize: 12, color: '#367681', fontFamily: 'Montserrat-SemiBold' }}>
                      {item}
                    </Text>
                  </View>
                )}
              /> : <></>}

          </View>

        </View>

      </Modal>
    )
  }

  openWebsite = (website) => {

    if (website && website.includes('http')) return Linking.openURL(website)

    else if (website && !website.includes('http')) return Linking.openURL('https://' + website)

  }

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          REACT_APP_domainUrl + '/profile/' + this.props.user.body.customUrl,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // console.log('shared with activity type of result.activityType')
        } else {
          // console.log('shared')
        }
      } else if (result.action === Share.dismissedAction) {
        // console.log('dismissed')
      }
    } catch (error) {
      // console.log(error.message)
    }
  }

  shareModal = () => {
    return (

      <Modal visible={this.state.shareModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>

          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
            onPress={() => this.setState({ shareModalOpen: false })} >
            <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>

            <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
              <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>Share</Text>
            </View>

            <TouchableOpacity onPress={() => {
              Clipboard.setString(REACT_APP_domainUrl + '/profile/' + this.props.user.body.customUrl)
              Snackbar.show({
                backgroundColor: '#97A600',
                text: "Link Copied",
                textColor: "#00394D",
                duration: Snackbar.LENGTH_LONG,
              })
            }}
              style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt]}
              activeOpacity={0.6}>
              <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>Copy link to profile</Text>
              <Icon name='TxEdi_AddLink' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

            </TouchableOpacity>

            <TouchableOpacity onPress={() => { this.setState({ shareModalOpen: false }, () => this.onShare()) }}
              style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]}
              activeOpacity={0.6}>
              <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>Share via others</Text>

              <View style={[{ flexDirection: 'row', justifyContent: 'space-evenly', width: 100, marginRight: -6 }]}>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name="Social_FB" size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                  <Icon name='Social_Twitter' size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                  <Icon name='Social_LinkedIn' size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                  <Icon name="Meatballs" size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                </View>

              </View>

            </TouchableOpacity>


          </View>



        </View>

      </Modal>

    )
  }

  contactInfoModal = () => {
    return (

      <Modal visible={this.state.contactInfoModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>

          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
            onPress={() => this.setState({ contactInfoModalOpen: false })} >
            <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
          </TouchableOpacity>

          {this.props.user.body ? <View style={defaultShape.Modal_Categories_Container}>

            <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0, borderBottomWidth: 0 }]}>
              <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>CONTACT INFO</Text>
            </View>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15, borderBottomWidth: 0, flexDirection: 'column', alignItems: 'flex-start' }] : [defaultShape.ActList_Cell_Gylph_Alt, { flexDirection: 'column', alignItems: 'flex-start', borderBottomWidth: 0, paddingVertical: 12 }]} activeOpacity={0.6} onPress={() => { this.setState({ contactInfoModalOpen: false }) }}>
              <Text style={[defaultStyle.Button_2, { color: COLORS.dark_600 }]}>Phone Number :</Text>
              <Text style={[defaultStyle.Caption, { color: COLORS.dark_600, fontSize: 12.5 }]}>+{this.props.user.body.countryCode} {this.props.user.body.mobile}</Text>

            </TouchableOpacity>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15, borderBottomWidth: 0, flexDirection: 'column', alignItems: 'flex-start' }] : [defaultShape.ActList_Cell_Gylph_Alt, { flexDirection: 'column', alignItems: 'flex-start', borderBottomWidth: 0, paddingVertical: 12 }]} activeOpacity={0.6} onPress={() => { this.setState({ contactInfoModalOpen: false }) }}>
              <Text style={[defaultStyle.Button_2, { color: COLORS.dark_600 }]}>Email :</Text>
              <Text style={[defaultStyle.Caption, { color: COLORS.dark_600, fontSize: 12.5 }]}>{this.props.user.body.email}</Text>

            </TouchableOpacity>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15, borderBottomWidth: 0, flexDirection: 'column', alignItems: 'flex-start' }] : [defaultShape.ActList_Cell_Gylph_Alt, { flexDirection: 'column', alignItems: 'flex-start', borderBottomWidth: 0, paddingVertical: 12 }]} activeOpacity={0.6} onPress={() => { this.setState({ contactInfoModalOpen: false }) }}>
              <Text style={[defaultStyle.Button_2, { color: COLORS.dark_600 }]}>Address :</Text>
              {this.props.userAddress.body ?
                <Text style={[defaultStyle.Caption, { color: COLORS.dark_600, fontSize: 12.5 }]}>{this.props.userAddress.body.city}, {this.props.userAddress.body.state}, {this.props.userAddress.body.country}</Text>
                : <></>}
            </TouchableOpacity>

          </View> : <></>}

        </View>

      </Modal>

    )
  }

  thumbnail = (item) => {
    if (item.coverImage) {
      return <Image source={{ uri: item.coverImage }} style={{ width: 60, height: 60, borderTopLeftRadius: 5, borderBottomLeftRadius: 5 }} />
    }
    if (item.type === 'COMMENT' || item.type === 'POST' || item.type === 'POLL' || item.type === 'LINK') {
      return (
        <View style={{ width: 60, height: 60, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, backgroundColor: COLORS.grey_350, alignItems: 'center', justifyContent: 'center' }}>
          {item.type === 'COMMENT' || item.type === 'POST' ?
            <Icon name='Img' size={26} color={COLORS.grey_400} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
            : item.type === 'POLL' ?
              <Icon name='Polls' size={26} color={COLORS.grey_400} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
              : <Icon name='Link' size={26} color={COLORS.grey_400} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
          }
        </View>
      )
    }
    else return <Image source={projectDefault} style={{ width: 60, height: 60, borderTopLeftRadius: 5, borderBottomLeftRadius: 5 }} />
  }

  handleFollowUnfollow = () => {

    let url
    if (this.state.following === 0) {
      url = REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/follows/' + this.props.route.params.userId
    } else {
      url = REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/unfollows/' + this.props.route.params.userId
    }
    axios({
      method: 'post',
      url: url,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.status === 202) {
        // console.log(response.status)
        this.getFollowers()
        this.props.companyConnectionInfoRequest({ userId: this.props.route.params.userId })
      } else {
        // console.log(response)
      }
    }).catch((err) => {
      // console.log(err)
    })
  }

  getFollowers = () => {
    axios({
      method: "get",
      url:
        REACT_APP_userServiceURL +
        "/network/" +
        this.props.route.params.userId +
        "/followers" +
        "?page=" +
        0 +
        "&size=" +
        this.props.userConnectionInfo.body.followers,
      headers: { "Content-Type": "application/json" },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === "Success!") {
          // console.log('```````````````````` Followers ````````````````````', response.data.body.page.content.filter((item) => item.profileId === this.state.userId).length)
          this.setState({
            following: response.data.body.page.content.filter((item) => item.profileId === this.state.userId).length
          })

        }
      })
      .catch((err) => {
        // console.log(err)
      })
  }

  getFollowers2 = () => {
    axios({
      method: "get",
      url:
        REACT_APP_userServiceURL +
        "/network/" +
        this.state.userId +
        "/followers" +
        "?page=" +
        0 +
        "&size=" +
        this.props.userConnectionInfo.body.followers,
      headers: { "Content-Type": "application/json" },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === "Success!") {
          // console.log('```````````````````` Followers ````````````````````', response.data.body.content)
          this.setState({
            followers: response.data.body.content
          })

        }
      })
      .catch((err) => {
        // console.log(err)
      })
  }

  getConnectStatus = () => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + "/graph/users/" + this.state.userId + "/connectionStatus/" + this.props.route.params.userId,
      withCredentials: true,
      headers: { "Content-Type": "application/json" },
    }).then((response) => {
      if (response && response.status === 200) {
        // console.log(response.data.body.connectStatus)
        this.setState({ isConnected: response.data.body.connectStatus })
      }
    }).catch((e) => {
      // console.log(e)
    })
  }


  handleConnectStatusChange = () => {

    let url
    if (this.state.isConnected === 'NOT_CONNECTED') {
      url = REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/invite/' + this.props.route.params.userId;
    } else if (this.state.isConnected === 'PENDING_CONNECT') {
      url = REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/removeInvite/' + this.props.route.params.userId
    } else if (this.state.isConnected === 'CONNECTED') {
      url = REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/unconnect/' + this.props.route.params.userId
    }
    else if (this.state.isConnected === 'DEADLOCK_PENDING_STATUS' || this.state.isConnected === 'IGNORE_PENDING_CONNECT') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: "Error sending request to this member",
        duration: Snackbar.LENGTH_LONG,
      })
    }

    if (url && url !== '') {
      axios({
        method: 'post',
        url: url,
        headers: { 'Content-Type': 'application/json' },
        withCredentials: true
      }).then(response => {
        if (response && response.status === 202) {
          // console.log(response.status)
          this.getConnectStatus()
          this.props.companyConnectionInfoRequest({ userId: this.props.route.params.userId })
        }
      }).catch((err) => {
        // console.log(err)
        if (err.message === 'Request failed with status code 409') {
          Snackbar.show({
            backgroundColor: '#B22222',
            text: "You can send connection request after 3 days to this member",
            duration: Snackbar.LENGTH_LONG,
          })
        }
        if (err.message === 'Request failed with status code 400') {
          Snackbar.show({
            backgroundColor: '#B22222',
            text: "Cannot send request to an organization",
            duration: Snackbar.LENGTH_LONG,
          })
        }
      })
    }
  }

  getMutualConnects = () => {
    axios({
      method: "get",
      url:
        REACT_APP_userServiceURL +
        "/network/" +
        this.state.userId +
        '/mutualConnects/' + this.props.route.params.userId +
        "?page=" +
        0 +
        "&size=" +
        100,
      headers: { "Content-Type": "application/json" },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === "Success!") {
          // console.log('```````````````````` Mutual ````````````````````', response.data.body.content[0])
          this.setState({ mutualConnects: response.data.body.content, totalMutualConnects: response.data.body.content.length })

        }
      })
      .catch((err) => {
        // console.log(err)
      })
  }

  getEmployees() {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/employee/joined?companyId=' + this.props.route.params.userId + "&userId=" + this.props.route.params.userId + "&page=" + 0 + "&size=" + 1000,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.body && response.data.message === 'Success!') {
        // console.log('response.data.body.content', response.data.body.content)
        this.setState({ employees: response.data.body.content })
        // this.setState({ 'pagination': response.data.body.last })
        // console.log('response.data.body.content ..............', response.data.body.content, response.data.body.content.length)
      }
    }).catch((err) => {
      // console.log(err)
    })
  }

  getEmployees2() {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/employee/joined?companyId=' + this.state.userId + "&userId=" + this.state.userId + "&page=" + 0 + "&size=" + 1000,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.body && response.data.message === 'Success!') {
        // console.log('response.data.body.content', response.data.body.content)
        this.setState({ employees: response.data.body.content })
        // this.setState({ 'pagination': response.data.body.last })
        // console.log('response.data.body.content ..............', response.data.body.content, response.data.body.content.length)
      }
    }).catch((err) => {
      // console.log(err)
    })
  }

  reasonForReportingModal = () => {
    return (

      <Modal visible={this.state.reasonForReportingModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>

          <View style={[defaultShape.Linear_Gradient_View, { height: 700, bottom: 0 }]}>
            <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={[defaultShape.CloseBtn, { marginBottom: 0 }]}
            onPress={() => this.setState({ reasonForReportingModalOpen: false })} >
            <Icon name='Cross' size={13} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }} />
          </TouchableOpacity>


          <View style={{
            borderRadius: 20,
            backgroundColor: COLORS.altgreen_100,
            alignItems: 'center',
            paddingTop: 15,
            paddingBottom: 10,
            // paddingLeft: 20,
            width: '90%',
            alignSelf: 'center',
            marginBottom: 30,
            marginTop: 15
          }}>

            <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
              <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>Reason for reporting</Text>
            </View>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, borderBottomWidth: 0 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'FAKE_SPAM_OR_SCAM' }) }} >

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                  {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                </View>
                <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Fake, spam or scam</Text>
              </View>


            </TouchableOpacity>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'ACCOUNT_MAY_BE_HACKED' }) }} >

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                  {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                </View>
                <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Account may be hacked</Text>
              </View>

            </TouchableOpacity>
            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'IMPERSONATING_SOMEONE' }) }} >

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                  {this.state.reasonForReporting === 'IMPERSONATING_SOMEONE' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                </View>
                <Text numberOfLines={2} style={[defaultStyle.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Impersonating someone</Text>
              </View>

            </TouchableOpacity>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'VIOLATES_TERMS_OF_USE' }) }} >

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                  {this.state.reasonForReporting === 'VIOLATES_TERMS_OF_USE' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                </View>
                <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Violates Terms Of Use</Text>
              </View>

            </TouchableOpacity>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'OTHERS', description: 'OTHERS' }) }} >

              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                  {this.state.reasonForReporting === 'OTHERS' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                </View>
                <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Others</Text>
              </View>

            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.5}
              onPress={() => { this.handleReportAbuseSubmit(), this.setState({ reasonForReportingModalOpen: false }) }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 15,
                height: 27,
                marginVertical: 10,
                borderRadius: 16,
                textAlign: 'center',
                borderWidth: 1,
                borderColor: '#698F8A'
              }}>
              <Text style={{
                color: '#698F8A',
                fontSize: 14,
                paddingHorizontal: 14,
                paddingVertical: 20,
                fontFamily: 'Montserrat-Medium',
                fontWeight: 'bold'
              }}>Submit</Text>
            </TouchableOpacity>

          </View>
        </View>

      </Modal>

    )
  }

  handleReportAbuseSubmit = () => {

    let data = {
      reporterId: this.state.userId,
      entityId: this.props.route.params.userId,
      // entityType: this.state.entityType,
      entityType: 'USER',
      reason: this.state.reasonForReporting,
      description: this.state.description
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true
    }).then(response => {
      this.setState({ reasonForReporting: this.state.reasonForReporting })
      if (response && response.status === 201) {
        Snackbar.show({
          backgroundColor: COLORS.primarydark,
          text: "Your request has been taken and appropriate action will be taken as per our report abuse policy",
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        })
        // console.log(response)
      } else {
        // console.log(response)

      }
    }).catch((err) => {
      this.setState({ reasonForReporting: this.state.reasonForReporting })
      if (err && err.response && err.response.status === 409) {
        // console.log(err.response)
        Snackbar.show({
          backgroundColor: COLORS.primarydark,
          text: "Your report request was already taken",
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        })

      } else {
        // console.log(err)
        Snackbar.show({
          backgroundColor: '#B22222',
          text: "Please check your network or try again later",
          duration: Snackbar.LENGTH_LONG,
        })
      }
    })
  }

  getMasterConfig() {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/master/config/get',
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then((response) => {
      let res = response.data
      if (res.message === 'Success!') {
        // console.log('res.body.fields.skills', res.body.fields.skills)
        this.setState({ skillsSuggestions: res.body.fields.skills })
      }
    }).catch((err) => {
      if (err && err.response && err.response.data) {
        // console.log(err.response.data.message, err.response)

      }
    })
  }

  handleSubmit = () => {

    let postBody = {
      "userId": this.props.route.params.userId,
      "endorseBy": this.state.userId,
      "description": 'Description',
      "skills": this.state.hashTags
    }
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/endorsement/save',
      headers: { 'Content-Type': 'application/json' },
      data: postBody,
      withCredentials: true
    }).
      then((response) => {
        // console.log('response.data.status')
        if (response.data.status === '201 CREATED') {
          // console.log(response.data.status)
          Snackbar.show({
            backgroundColor: '#97A600',
            text: "Organization endorsed successfully",
            textColor: "#00394D",
            duration: Snackbar.LENGTH_LONG,
          })
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          // console.log(err.response.data.message)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: err.response.data.message,
            duration: Snackbar.LENGTH_LONG,
          })
        }
      })
  }

  endorseOrganizationModal = () => {
    return (
      <Modal visible={this.state.endorseOrganizationModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

        <View style={{ marginTop: 'auto' }}>

          <View style={{
            width: '100%',
            height: 700,
            position: 'absolute',
            bottom: 0,
            alignSelf: 'center'
          }}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={{
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6
            }}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
            onPress={() => this.setState({ endorseOrganizationModalOpen: false, hashTags: [] })} >
            <Icon name='Cross' size={13} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
          </TouchableOpacity>

          <View style={[defaultShape.Modal_Categories_Container, { height: 450, backgroundColor: COLORS.bgfill }]}>
            <View style={{ width: '100%', height: 58, backgroundColor: COLORS.altgreen_100, justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
              <Text style={[defaultStyle.Button_2, { color: COLORS.dark_900, textAlign: 'center' }]}>
                ENDORSE ORGANIZATION
              </Text>
            </View>

            <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400, textAlign: 'center', marginTop: 60 }]}>
              Type a speciality and then add space to {`\n`} automatically start a new speciality.
            </Text>

            <FlatList
              // keyboardDismissMode="on-drag"
              keyboardShouldPersistTaps='handled'
              scrollEventThrottle={0}
              ref={ref => this.scrollView = ref}
              onContentSizeChange={() => {
                this.scrollView.scrollToEnd({ animated: false });
                this.setState({ endorseOrganizationModalOpen: true })
              }}
              style={{ marginTop: 10 }}
              columnWrapperStyle={{ flexDirection: "row", flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center' }}
              numColumns={3}
              showsVerticalScrollIndicator={false}
              data={[...this.state.hashTags, 'lastData']}
              keyExtractor={(item, index) => item + index}
              renderItem={({ item, index }) => (
                <View>
                  {index < this.state.hashTags.length ?
                    <TouchableOpacity onPress={() => this.setState({ hashTags: this.state.hashTags.filter((value, index2) => value + index2 !== item + index) })}
                      activeOpacity={0.6} style={{ flexDirection: 'row', backgroundColor: COLORS.altgreen_t50 + '80', paddingHorizontal: 10, marginVertical: 6, height: 28, borderRadius: 17, justifyContent: 'center', alignItems: 'center', marginRight: 6 }}>

                      <Text style={[defaultStyle.Caption, { color: COLORS.dark_500, marginRight: 4, marginTop: Platform.OS === 'ios' ? -2 : 0 }]}>{item}</Text>
                      <Icon name="Cross_Rounded" color={COLORS.dark_500} size={15} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                    </TouchableOpacity>

                    :
                    <TextInput
                      theme={{ colors: { text: COLORS.dark_600, primary: COLORS.altgreen_300, placeholder: COLORS.altgreen_300 } }}
                      // label="Write something interesting"
                      placeholder="Add Speciality"
                      autoFocus={true}
                      selectionColor='#C8DB6E'
                      style={[defaultStyle.H6, { height: 28, backgroundColor: COLORS.bgfill, color: COLORS.dark_600, textAlign: 'center', marginVertical: 6, }]}
                      onChangeText={(value) => {
                        this.setState({ query2: value.trim() })
                        value[value.length - 1] === ' ' && value.trim() ? this.setState({ hashTags: [...this.state.hashTags, value.trim()], query2: '' }) : null
                      }
                      }
                      value={this.state.query2}
                    />}


                </View>
              )
              }
            />

            <View style={{ width: '80%', alignSelf: 'center', backgroundColor: this.state.hashTags.length ? COLORS.green_300 : COLORS.grey_200, height: 2 }}></View>

            <TouchableOpacity onPress={() => {
              this.state.hashTags.length > 0 ? (this.handleSubmit(), this.setState({ endorseOrganizationModalOpen: false, hashTags: [] }))
                :
                Snackbar.show({
                  backgroundColor: '#B22222',
                  text: "Please enter atleast 1 speciality",
                  duration: Snackbar.LENGTH_LONG,
                })
            }}
              activeOpacity={0.7} style={[{ width: 120, height: 38, borderRadius: 20, alignItems: 'center', justifyContent: 'center', marginVertical: 10, backgroundColor: COLORS.dark_600, flexDirection: 'row' }]}>
              <Icon name="Feedback" size={15} color={COLORS.altgreen_100} style={Platform.OS === 'android' ? { marginTop: 12 } : {}} />
              <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_100, marginLeft: 6 }]}>Endorse</Text>
            </TouchableOpacity>

          </View>
        </View>

      </Modal>
    )
  }

  render() {
    // this.props.user.body ?
    //   console.log('################### this.props.user.body ######################', this.props.user.body)
    //   : null

    return (
      <SafeAreaView>

        {this.state.currentScrollPosition > 344 ?
          <View style={{ backgroundColor: COLORS.white, position: 'absolute', top: 0, zIndex: 2 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: COLORS.white }}>

              <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', padding: 10 }}>

                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <TouchableOpacity activeOpacity={0.5}
                    onPress={() => this.props.navigation.goBack()}
                    style={{ backgroundColor: COLORS.white, alignItems: 'center', justifyContent: 'center', height: 42, width: 42, borderRadius: 21, marginTop: 6 }}>
                    <Icon name="Arrow-Left" size={16} color="#00394D" style={{ marginTop: 5 }} />
                  </TouchableOpacity>

                  {(this.props.user.body && this.props.user.body.originalProfileImage) ?
                    <Image source={{ uri: this.props.user.body.originalProfileImage }} style={{ width: 30, height: 30, borderRadius: 15 }} />
                    : null}

                  <View style={{ alignSelf: 'center', marginLeft: 8 }}>{this.props.user.body !== undefined ? <Text style={[defaultStyle.Title_1, { color: COLORS.dark_800, fontSize: 14, }]}>{this.props.user.body.userName} </Text> : <></>}

                    {this.props.user.body ? <Text style={[defaultStyle.Subtitle_1, { color: COLORS.altgreen_400, fontSize: 12, marginTop: -6 }]}>{this.props.user.body.persona}</Text> : <></>}
                  </View>

                </View>

                <TouchableOpacity activeOpacity={0.5} onPress={() => this.setState({ optionsModalOpen: true })}
                  style={{ backgroundColor: COLORS.white, alignItems: 'center', justifyContent: 'center', height: 42, width: 42, borderRadius: 21, marginTop: 6 }}>
                  <Icon name="Kebab" size={16} color="#00394D" style={{ marginTop: 5 }} />
                </TouchableOpacity>
              </View>

            </View>

            <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-evenly', width: 240 }}>
              <TouchableOpacity activeOpacity={0.8} style={{ width: 120 }}>
                <Text style={[defaultStyle.Button_2, { color: COLORS.dark_800, textAlign: 'center', marginBottom: 6, paddingTop: 8 }]}>ABOUT</Text>
                <View style={{ width: 120, height: 5, backgroundColor: COLORS.dark_800, borderTopLeftRadius: 4, borderTopRightRadius: 4 }}></View>
              </TouchableOpacity>


              <TouchableOpacity activeOpacity={0.8} style={{ paddingTop: 8, width: 120 }} onPress={() => this.props.navigation.navigate('CompanyActivitiesScreen', { id: this.state.paramsId, state: this.props.userAddress.body ? this.props.userAddress.body.state : null })}>
                <Text style={[defaultStyle.Button_Lead, { color: COLORS.altgreen_400, marginBottom: 6, textAlign: 'center' }]}>ACTIVITY</Text>
              </TouchableOpacity>


            </View>

          </View> : <></>}


        <ScrollView
          onScroll={this.handleScroll}
          // stickyHeaderIndices={[0]}
          scrollEventThrottle={0}
          // ref={ref => this.scrollView = ref}
          // onContentSizeChange={() => {
          //   this.scrollView.scrollToEnd({ animated: true });
          // }}
          keyboardDismissMode="on-drag" keyboardShouldPersistTaps='handled' showsVerticalScrollIndicator={false} style={styles.container}>


          <View style={styles.imageContainer}
            // source={{ uri: this.props.user.body.originalCoverImage }}
          >

            <View style={{ width: '100%', height: '30%', position: 'absolute', bottom: 0 }}>
              <LinearGradient colors={['#FFFFFF00', '#FFFFFFBA', '#FFFFFFE3', '#FFFFFF']} style={styles.linearGradient}>
              </LinearGradient>
            </View>

            <View style={{ flexDirection: 'row', width: '95%', marginHorizontal: '5%', justifyContent: 'space-between' }}>
              <TouchableOpacity activeOpacity={0.5}
                onPress={() => this.props.navigation.goBack()}
                style={{ backgroundColor: '#CFE7C733', alignItems: 'center', justifyContent: 'center', height: 42, width: 42, borderRadius: 21, marginLeft: -10, marginTop: 12 }}>
                <Icon name="Arrow-Left" size={16} color="#00394D" style={{ marginTop: 5 }} />
              </TouchableOpacity>

              <TouchableOpacity activeOpacity={0.5} onPress={() => this.setState({ optionsModalOpen: true })}
                style={{ backgroundColor: '#CFE7C733', alignItems: 'center', justifyContent: 'center', height: 42, width: 42, borderRadius: 21, marginRight: 10, marginTop: 12 }}>
                <Icon name="Kebab" size={16} color="#00394D" style={{ marginTop: 5 }} />
              </TouchableOpacity>
            </View>

          </View>
  

          {this.shareModal()}
          {this.optionsModal()}
          {this.requestEndorsementModal()}
          {this.modal()}
          {this.specializationModal()}
          {this.skillsModal()}
          {this.interestsModal()}
          {this.hashTagModal()}
          {this.contactInfoModal()}
          {this.reasonForReportingModal()}
          {this.endorseOrganizationModal()}

          <View style={styles.mainSection}>

            {/* <View style={[styles.imageUsername, { backgroundColor: 'white' }]}> */}

            {/* <View style={styles.header}> */}

            <View style={styles.userProfile}>

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>

                <View style={{ width: 40, height: 31 }}>

                </View>

                <Image source={this.props.user.body &&
                  this.props.user.body.originalProfileImage ?
                  { uri: this.props.user.body.originalProfileImage } :
                  defaultBusiness}
                  style={{ width: 90, height: 90, borderRadius: 10, marginTop: -40 }} />

                <TouchableOpacity onPress={() => this.openWebsite(this.props.user.body.website)}
                  activeOpacity={0.5} style={{ width: 31, height: 31, borderRadius: 17, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 6, marginRight: 6 }}>
                  <Icon name='Export' size={14} color='#367681' style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 6 }} />
                </TouchableOpacity>

              </View>

              {this.props.user.body !== undefined ? <Text style={[defaultStyle.Title_1, { color: COLORS.dark_800, marginTop: 14 }]}>{this.props.user.body.userName} </Text> : <></>}
              <View>
                {this.props.user.body ? <Text style={[defaultStyle.Subtitle_1, { color: COLORS.altgreen_400, marginTop: 3 }]}>{this.props.user.body.organizationType}</Text> : <></>}
              </View>

              {this.props.user.body ?
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignSelf: 'center', marginTop: -3 }}>
                  <Icon name='Location' size={12} color='#367681' style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 6 }} />
                  {this.props.user.body ?
                    <Text numberOfLines={1}
                      style={[defaultStyle.Subtitle_2, { color: COLORS.grey_400, marginLeft: 4 }]}>
                      {this.props.user.body.country}, {this.props.user.body.city}
                    </Text>
                    : <ActivityIndicator size='small' color="#fff" />}

                </View>
                : <></>}

              {this.props.route.params ? <TouchableOpacity onPress={() => this.handleFollowUnfollow()}
                style={{ flexDirection: 'row', paddingHorizontal: 14, height: 38, borderRadius: 4, backgroundColor: this.state.following === 1 ? '#91b3a2' : COLORS.green_500, justifyContent: 'center', alignItems: 'center', marginVertical: 6 }} activeOpacity={0.8}>
                {this.state.following === 1 ? <Icon name='FollowTick' size={14} color={COLORS.white} style={{ marginTop: Platform.OS === 'android' ? 8 : 0, marginRight: 6 }} /> : <></>}
                <Text style={[defaultStyle.Button_Lead, { color: COLORS.white }]}>
                  {this.state.following === 0 ? 'Follow' : 'Following'}
                </Text>
              </TouchableOpacity> : <></>}

              <View style={{ borderColor: '#D9E1E4', borderWidth: 0.45, width: '76%', marginTop: 8 }}></View>

              {/* ******connection info start******* */}

              <TouchableOpacity onPress={() => this.props.navigation.navigate('EmployeesScreen', { id: this.state.paramsId, totalMutualConnects: this.props.route.params ? this.state.mutualConnects.length : null, employees: this.state.employees })}
                activeOpacity={0.7} style={{ flexDirection: 'row', justifyContent: 'space-evenly', width: '100%', alignItems: 'center' }}>

                <View style={{ marginRight: 12, marginTop: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={[defaultStyle.Body_1_bold, { color: COLORS.dark_800 }]}>
                    {this.props.userConnectionInfo.body ? this.props.userConnectionInfo.body.followers : "0"}
                  </Text>
                  <Text style={[defaultStyle.Body_1, { color: COLORS.altgreen_300, marginLeft: 6 }]}>Followers</Text>
                </View>

                <View style={{ marginRight: 12, marginTop: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={[defaultStyle.Body_1_bold, { color: COLORS.dark_800 }]}>
                    {this.state.employees.length}
                  </Text>
                  <Text style={[defaultStyle.Body_1, { color: COLORS.altgreen_300, marginLeft: 6 }]}>Employee</Text>
                </View>



              </TouchableOpacity>

              {this.props.user.body && this.props.user.body.bio ?
                <View style={{ paddingHorizontal: 20, marginTop: 16 }}>
                  <Text onPress={() => this.setState({ readMore: !this.state.readMore })}
                    numberOfLines={this.state.readMore ? 500 : 5}
                    style={[defaultStyle.Body_1, { color: COLORS.altgreen_300, marginHorizontal: 10, lineHeight: 16, textAlign: 'center' }]}>
                    {this.props.user.body.bio}
                  </Text>
                  <TouchableOpacity style={{ height: 30, width: 80, marginLeft: 10, alignSelf: 'center' }}
                    onPress={() => this.setState({ readMore: !this.state.readMore })}>
                    <Text style={{ color: COLORS.green_500, fontWeight: '700', textAlign: 'center' }}>
                      {this.state.readMore ? 'Read less' : 'Read more'}
                    </Text>
                  </TouchableOpacity>
                </View>
                : <View style={{ paddingHorizontal: 20, marginTop: 20 }} />}

              {/* ******connection info end******* */}

            </View>

            {/* </View> */}

            {/* </View> */}



            {/* -------- About Starts-------------- */}

            <View style={{}}>

              <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-evenly', width: 240 }}>
                <TouchableOpacity activeOpacity={0.8} style={{ width: 120 }}>
                  <Text style={[defaultStyle.Button_2, { color: COLORS.dark_800, textAlign: 'center', marginBottom: 6, paddingTop: 8 }]}>ABOUT</Text>
                  <View style={{ width: 120, height: 5, backgroundColor: COLORS.dark_800, borderTopLeftRadius: 4, borderTopRightRadius: 4 }}></View>
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={0.8} style={{ paddingTop: 8, width: 120 }} onPress={() => this.props.navigation.navigate('CompanyActivitiesScreen', { id: this.state.paramsId, state: this.props.userAddress.body ? this.props.userAddress.body.state : null })}>
                  <Text style={[defaultStyle.Button_Lead, { color: COLORS.altgreen_400, marginBottom: 6, textAlign: 'center' }]}>ACTIVITY</Text>
                </TouchableOpacity>
              </View>


              {/* -------- Recent Activity -------------- */}

              {this.props.userRecentActivity.body && this.props.userRecentActivity.body.content.length ?
                <View style={{ backgroundColor: '#D9E1E4', paddingVertical: 20, paddingLeft: 16 }}>
                  <Text style={[defaultStyle.OVERLINE, { color: COLORS.altgreen_400 }]}>RECENT ACTIVITY</Text>

                  {this.props.userRecentActivity.body ?
                    <FlatList
                      style={{ paddingVertical: 20 }}
                      showsHorizontalScrollIndicator={false}
                      horizontal={true}
                      alwaysBounceHorizontal={false}
                      keyExtractor={(item) => item.activityId}
                      data={this.props.userRecentActivity.body.content}
                      renderItem={({ item }) => (
                        <TouchableOpacity activeOpacity={0.5}
                          onPress={() => this.props.navigation.navigate('IndividualFeedsPost', { id: item.parentId })}
                          style={{ flexDirection: 'row', marginRight: 10 }}>
                          {this.thumbnail(item)}

                          <View style={{ width: 150, height: 60, backgroundColor: '#FFF', justifyContent: 'center', borderTopRightRadius: 5, borderBottomRightRadius: 5 }}>
                            {item.type === 'ASSIGNMENT' ?
                              <Text style={[defaultStyle.Caption, { marginHorizontal: 12, color: COLORS.primarydark }]}>Posted an ASSIGNMENT</Text> :
                              <Text style={[defaultStyle.Caption, { marginHorizontal: 12, color: COLORS.primarydark }]}>Posted a {item.type}</Text>}
                            <Text style={[defaultStyle.Body_1, { marginHorizontal: 12, color: COLORS.grey_400 }]}>on {this.unixTime(item.createTime)}</Text>
                          </View>


                        </TouchableOpacity>
                      )}
                    /> : <ActivityIndicator size='small' color="#fff" />}

                </View> : <></>}

              {/* -------- Recent Activity -------------- */}



              {/* -------- Experience Starts-------------- */}
              {this.props.userExperience.body && this.props.userExperience.body.experience.content.length ?
                <View style={{ paddingTop: 20, paddingBottom: 5, paddingLeft: 16, backgroundColor: '#F7F7F5' }}>

                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingRight: 18 }}>
                    <Text style={[defaultStyle.H6, { color: COLORS.dark_800 }]}>EXPERIENCE</Text>
                    <TouchableOpacity onPress={() => this.setState({ modalOpen: true })} activeOpacity={0.5} style={{ flexDirection: 'row', width: 'auto', height: 31, borderRadius: 20, backgroundColor: '#D9E1E4', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 10 }} >
                      {this.state.selectedExperienceType === 'ALL' ? <Text style={[defaultStyle.Caption, { color: COLORS.dark_600 }]}>{this.state.selectedExperienceType}</Text> : <Text style={[defaultStyle.Caption, { color: COLORS.dark_600 }]}>{this.state.selectedExperienceType}S</Text>}
                      <Icon name='Arrow2_Down' size={14} color="#367681" style={Platform.OS === 'android' ? { marginTop: 8 } : null} />
                    </TouchableOpacity>
                  </View>


                  {this.props.userExperience.body ?
                    <FlatList
                      style={{ paddingVertical: 20, marginRight: 10 }}
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                      alwaysBounceHorizontal={false}
                      keyExtractor={(item) => item.id}
                      data={this.state.selectedExperienceType !== 'ALL' ? this.props.userExperience.body.experience.content.filter((content) => content.type === this.state.selectedExperienceType).slice(0, 6) : this.props.userExperience.body.experience.content.slice(0, 6)}
                      renderItem={({ item, index }) => (
                        this.renderItem(item, index)
                      )}
                    />
                    :
                    <ActivityIndicator size='small' color="#00394D" />
                  }


                </View> : <></>}

              {/* -------- Experience Ends-------------- */}




              {/* -------- Education Starts ------------ */}

              {this.props.userEducation.body && this.props.userEducation.body.list.length ?
                <View style={{ paddingVertical: 20, backgroundColor: '#F7F7F5' }}>

                  <View style={{ backgroundColor: '#FFFFFF', marginHorizontal: 10, alignSelf: 'center', width: '90%', borderRadius: 6, shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5, }}>

                    <Text style={[defaultStyle.H6, { color: COLORS.dark_800, marginTop: 14, marginLeft: 14 }]}>EDUCATION</Text>

                    {this.props.userEducation.body ?
                      <FlatList
                        style={{ paddingVertical: 20 }}
                        // horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        alwaysBounceHorizontal={false}
                        keyExtractor={(item) => item.id}
                        data={this.props.userEducation.body.list.slice(0, 5)}
                        renderItem={({ item, index }) => {
                          if (index + 1 === 5 || index + 1 === this.props.userEducation.body.list.length) return <View>
                            <View style={{ width: '100%', flexDirection: 'row', marginRight: 10, borderRadius: 5, justifyContent: 'flex-start', alignItems: 'center' }}>

                              <Image source={InstitutionLogo} style={{ width: 36, height: 36, borderRadius: 18, marginLeft: 30, marginRight: 10 }} />
                              <View style={{ height: 60, justifyContent: 'center', borderTopRightRadius: 5, borderBottomRightRadius: 6 }}>
                                <Text style={[defaultStyle.Body_1, { marginHorizontal: 12, color: COLORS.dark_900 }]}>{item.institution}</Text>
                                <Text style={[defaultStyle.Caption, { marginLeft: 12, color: COLORS.altgreen_400 }]}>{item.specialisations}</Text>
                                {this.unixTime(item.endTime).split(' ').splice(1, 2).join(' ') !== 'Jan 1970' ?
                                  <Text style={[defaultStyle.Note, { marginHorizontal: 12, color: COLORS.grey_400 }]}>{this.unixTime(item.startTime).split(' ').splice(1).join(' ')} to {this.unixTime(item.endTime).split(' ').splice(1, 2).join(' ')}</Text>
                                  :
                                  <Text style={[defaultStyle.Note, { marginHorizontal: 12, color: COLORS.grey_400 }]}>{this.unixTime(item.startTime).split(' ').splice(1).join(' ')} to Present</Text>
                                }
                              </View>

                            </View>


                          </View>

                          else return <View>
                            <View style={{ width: '100%', flexDirection: 'row', marginRight: 10, borderRadius: 5, justifyContent: 'flex-start', alignItems: 'center' }}>

                              <Image source={InstitutionLogo} style={{ width: 36, height: 36, borderRadius: 18, marginLeft: 30, marginRight: 10 }} />
                              <View style={{ height: 60, justifyContent: 'center', borderTopRightRadius: 5, borderBottomRightRadius: 6 }}>
                                <Text style={{ marginHorizontal: 12, color: '#00394D', fontSize: 12, fontFamily: 'Montserrat-Bold' }}>{item.institution}</Text>
                                <Text style={{ marginLeft: 12, color: '#91B3A2', fontSize: 12, fontFamily: 'Montserrat-SemiBold' }}>{item.specialisations}</Text>
                                {this.unixTime(item.endTime).split(' ').splice(1, 2).join(' ') !== 'Jan 1970' ?
                                  <Text style={[defaultStyle.Note, { marginHorizontal: 12, color: COLORS.grey_400 }]}>{this.unixTime(item.startTime).split(' ').splice(1).join(' ')} to {this.unixTime(item.endTime).split(' ').splice(1, 2).join(' ')}</Text>
                                  :
                                  <Text style={[defaultStyle.Note, { marginHorizontal: 12, color: COLORS.grey_400 }]}>{this.unixTime(item.startTime).split(' ').splice(1).join(' ')} to Present</Text>
                                }
                              </View>

                            </View>
                            <View style={{ borderColor: '#E8ECEB', borderWidth: 0.45, width: '92%', marginVertical: 10, alignSelf: 'center' }}></View>

                          </View>
                        }}
                      />
                      :
                      <ActivityIndicator size='small' color="#00394D" />
                    }



                    <TouchableOpacity onPress={() => this.props.navigation.navigate("SeeallEducationScreen", { id: this.state.paramsId })}
                      activeOpacity={0.6} style={{ flexDirection: 'row', marginRight: 10, backgroundColor: '#F1F5EE', borderBottomRightRadius: 8, borderBottomLeftRadius: 8, width: '100%', height: 40, justifyContent: 'space-around', alignItems: 'center' }}>

                      <Text style={{ color: '#698F8A', fontSize: 12, fontFamily: 'Montserrat-Medium', paddingHorizontal: 6, paddingVertical: 2 }}>See all</Text>
                    </TouchableOpacity>

                  </View>




                </View> : <></>}

              {/* ---------- Education Ends ----------- */}




              {/* --------- Circle Starts ------------- */}


              {this.props.userCircle.body && this.props.userCircle.body.content.length ? <View style={{ backgroundColor: '#FFFFFF', paddingVertical: 20, paddingLeft: 16 }}>
                <Text style={{ color: '#154A59', fontSize: 14, fontFamily: 'Montserrat-Bold', marginLeft: 8 }}>CIRCLES</Text>

                {this.props.userCircle.body ?
                  <FlatList
                    style={{ paddingVertical: 20 }}
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    alwaysBounceHorizontal={false}
                    keyExtractor={(item) => item.id}
                    data={this.props.userCircle.body.content.slice(0, 6)}
                    renderItem={({ item, index }) => {

                      if (index + 1 === 6) return <TouchableOpacity onPress={() => this.props.navigation.navigate("SeeallCircleScreen", { id: this.state.paramsId })}
                        activeOpacity={0.5} style={{ marginHorizontal: 16, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', height: 26, width: 70, borderWidth: 1, borderColor: '#698F8A', borderRadius: 13, paddingHorizontal: 6, paddingVertical: 2 }}>
                        <Text style={{ color: '#698F8A', fontSize: 11, fontFamily: 'Montserrat-Medium' }}>See All</Text>
                      </TouchableOpacity>

                      else return <View style={{ marginRight: 16 }}>
                        {item.coverImage ?
                          <Image source={{ uri: item.coverImage }} style={{ width: 120, height: 52, borderRadius: 6 }} />
                          : <Image source={defaultCover} style={{ width: 120, height: 52, borderRadius: 6 }} />}

                        {item.profileImage ?
                          <Image source={{ uri: item.profileImage }} style={{ position: 'absolute', top: 26, zIndex: 2, alignSelf: 'center', width: 42, height: 42, borderRadius: 21 }} />
                          : <Image source={defaultCover} style={{ position: 'absolute', top: 26, zIndex: 2, alignSelf: 'center', width: 42, height: 42, borderRadius: 21 }} />}

                        <View style={{ marginTop: 16, paddingLeft: 2, width: 120, height: 46, backgroundColor: '#FFF', justifyContent: 'center', borderRadius: 5 }}>
                          <Text style={{ marginHorizontal: 0, color: '#00394D', fontSize: 12, fontFamily: 'Montserrat-Bold', textAlign: 'center' }}>{item.title}</Text>
                          <Text style={{ marginHorizontal: 0, color: '#607580', fontSize: 10, fontFamily: 'Montserrat-Medium', textAlign: 'center' }}>{item.memberType}</Text>
                        </View>
                      </View>

                    }}
                  /> : <ActivityIndicator size='small' color="#fff" />}

              </View> : <></>}

              {/* --------- Circle Ends ------------- */}



              {/* -------- Endorsement Starts ------------ */}
              {this.props.userEndorsement.body && this.props.userEndorsement.body.length ?
                <View style={{ paddingVertical: 20, backgroundColor: '#F7F7F5' }}>

                  <View style={{ backgroundColor: '#FFFFFF', marginHorizontal: 10, alignSelf: 'center', width: '90%', borderRadius: 6, shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5, }}>

                    <Text style={{ color: '#154A59', fontSize: 14, fontFamily: 'Montserrat-Bold', marginTop: 14, marginLeft: 14 }}>ENDORSEMENTS</Text>


                    {this.props.userEndorsement.body ?
                      <FlatList
                        style={{ paddingVertical: 20 }}

                        showsHorizontalScrollIndicator={false}
                        alwaysBounceHorizontal={false}
                        keyExtractor={(item) => item.topic}
                        data={this.props.userEndorsement.body.slice(0, 5)}
                        renderItem={({ item, index }) => {
                          if (index + 1 === 5 || index + 1 === this.props.userEndorsement.body.length) return <View>
                            <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigation.navigate("SeeallEndorsementScreen", { id: this.state.paramsId, topic: item.topic })}
                              style={{ width: '100%', flexDirection: 'row', marginRight: 10, borderRadius: 5, justifyContent: 'space-between', alignItems: 'center', paddingRight: 20, paddingLeft: 30 }}>


                              <View style={{ height: 60, justifyContent: 'center', borderTopRightRadius: 5, borderBottomRightRadius: 6 }}>

                                <Text style={{ color: '#00394D', fontSize: 14, fontFamily: 'Montserrat-SemiBold' }}>{item.topic}</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                  <Image source={defaultCover} style={{ width: 14, height: 14, borderRadius: 7, marginRight: 6 }} />
                                  {/* <Text style={{ color: '#607580', fontSize: 12, fontFamily: 'Montserrat-SemiBold' }}>Derek James</Text> */}
                                  <Text style={{ color: '#607580', fontSize: 12, fontFamily: 'Montserrat-Medium' }}>{item.count} endorsements</Text>

                                </View>
                                {/* <Text style={{ marginHorizontal: 12, color: '#90949C', fontSize: 10, fontFamily: 'Montserrat-Medium', fontStyle: 'italic' }}>{this.unixTime(endorsement.startTime).split(' ').splice(1).join(' ')} to {this.unixTime(endorsement.endTime).split(' ').splice(1, 2).join(' ')}</Text> */}
                              </View>

                              <View activeOpacity={0.5} style={{ width: 16, height: 16, borderRadius: 8, backgroundColor: '#00000033', justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name="Arrow_Right" size={12} color="#00394D" style={Platform.OS === 'android' ? { marginTop: 6 } : {}} />
                              </View>

                            </TouchableOpacity>

                          </View>

                          return <View>
                            <TouchableOpacity activeOpacity={0.5}
                              onPress={() => this.props.navigation.navigate("SeeallEndorsementScreen", { id: this.state.paramsId, topic: item.topic })}
                              style={{ width: '100%', flexDirection: 'row', marginRight: 10, borderRadius: 5, justifyContent: 'space-between', alignItems: 'center', paddingRight: 20, paddingLeft: 30 }}>


                              <View style={{ height: 60, justifyContent: 'center', borderTopRightRadius: 5, borderBottomRightRadius: 6 }}>

                                <Text style={{ color: '#00394D', fontSize: 14, fontFamily: 'Montserrat-SemiBold' }}>{item.topic}</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                  <Image source={defaultCover} style={{ width: 14, height: 14, borderRadius: 7, marginRight: 6 }} />
                                  {/* <Text style={{ color: '#607580', fontSize: 12, fontFamily: 'Montserrat-SemiBold' }}>Derek James</Text> */}
                                  <Text style={{ color: '#607580', fontSize: 12, fontFamily: 'Montserrat-Medium' }}>{item.count} endorsements</Text>

                                </View>
                                {/* <Text style={{ marginHorizontal: 12, color: '#90949C', fontSize: 10, fontFamily: 'Montserrat-Medium', fontStyle: 'italic' }}>{this.unixTime(endorsement.startTime).split(' ').splice(1).join(' ')} to {this.unixTime(endorsement.endTime).split(' ').splice(1, 2).join(' ')}</Text> */}
                              </View>

                              <View
                                activeOpacity={0.5} style={{ width: 16, height: 16, borderRadius: 8, backgroundColor: '#00000033', justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name="Arrow_Right" size={12} color="#00394D" style={Platform.OS === 'android' ? { marginTop: 6 } : {}} />
                              </View>

                            </TouchableOpacity>
                            <View style={{ borderColor: '#E8ECEB', borderWidth: 0.45, width: '92%', marginVertical: 10, alignSelf: 'center' }}></View>

                          </View>
                        }}
                      />
                      :
                      <ActivityIndicator size='small' color="#00394D" />
                    }



                    <TouchableOpacity onPress={() => this.props.navigation.navigate("SeeallEndorsementScreen", { id: this.state.paramsId })} activeOpacity={0.6} style={{ flexDirection: 'row', marginRight: 10, backgroundColor: '#F1F5EE', borderBottomRightRadius: 8, borderBottomLeftRadius: 8, width: '100%', height: 40, justifyContent: 'space-around', alignItems: 'center' }}>

                      <Text style={{ color: '#698F8A', fontSize: 12, fontFamily: 'Montserrat-Medium', paddingHorizontal: 6, paddingVertical: 2 }}>See all</Text>
                    </TouchableOpacity>

                  </View>


                  {!this.props.route.params ?
                    <TouchableOpacity
                      style={styles.requestEndorsement} activeOpacity={0.8} onPress={() => this.setState({ requestEndorsementModalOpen: true }, () => this.getFollowers2())}>
                      <Icon name="Feedback" size={12} color="#E7F3E3" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                      <Text style={{ fontSize: 12, color: '#E7F3E3', fontFamily: 'Montserrat-SemiBold', marginLeft: 5 }}>
                        Request Endorsement
                      </Text>
                    </TouchableOpacity>
                    : <></>}

                </View> : <></>}

              {/* ---------- Endorsement Ends ----------- */}


              {/* ---------- Business pages start ----------- */}

              {this.props.userBusinessPage.body && this.props.userBusinessPage.body.businessPage.length ?

                <View style={{ backgroundColor: '#F7F7F5' }}>
                  <View style={[styles.cardView, { marginTop: 16 }]}>

                    <Text style={{ color: '#154A59', fontSize: 14, fontFamily: 'Montserrat-Bold', marginVertical: 15, marginLeft: 15 }}>
                      ORGANIZATION PAGES
                    </Text>

                    {this.props.userBusinessPage.body ?
                      <FlatList
                        // style={{ paddingVertical: 20 }}
                        keyExtractor={(item) => item.companyId}
                        data={this.props.userBusinessPage.body.businessPage.sort((a, b) => a.companyName.toUpperCase(0) > b.companyName.toUpperCase(0)).slice(0, 5)}
                        renderItem={({ item }) => (
                          this.renderItemBusinessPage(item)
                        )}
                      />
                      :
                      <ActivityIndicator size='small' color="#00394D" />}

                    <TouchableOpacity onPress={() => this.props.navigation.navigate("SeeallBusinessPageScreen", { id: this.state.paramsId })} activeOpacity={0.6} style={{ flexDirection: 'row', marginRight: 10, backgroundColor: '#F1F5EE', borderBottomRightRadius: 8, borderBottomLeftRadius: 8, width: '100%', height: 40, justifyContent: 'space-around', alignItems: 'center' }}>

                      <Text style={{ color: '#698F8A', fontSize: 12, fontFamily: 'Montserrat-Medium', paddingHorizontal: 6, paddingVertical: 2 }}>See all</Text>
                    </TouchableOpacity>

                  </View>
                </View>
                :
                <></>
              }

              {/* ---------- Business pages Ends ----------- */}



              {/* --------- Causes Supported Starts ------------- */}


              {this.props.userCauses.body && this.props.userCauses.body.causeList.content.length ? <View style={styles.causesSupported}>
                <Text style={styles.causesSupportedText}>CAUSES SUPPORTED</Text>

                {this.props.userCauses.body ?
                  <FlatList
                    style={styles.causesFlatList}
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    alwaysBounceHorizontal={false}
                    keyExtractor={(item) => item.id}
                    data={this.props.userCauses.body.causeList.content.slice(0, 6)}
                    renderItem={({ item, index }) => {

                      if (index + 1 === 6) return <TouchableOpacity onPress={() => this.props.navigation.navigate("SeeallCausesScreen", { id: this.state.paramsId })}
                        activeOpacity={0.5} style={styles.seeAll}>
                        <Text style={styles.seeAllText}>See All</Text>
                      </TouchableOpacity>

                      else return <TouchableOpacity onPress={() => this.props.navigation.navigate('CausesStack', {
                        screen: 'CausesDetail',
                        params: { circleData: JSON.stringify(item), id: item.id },
                      })}
                        style={styles.individualCause}
                        activeOpacity={0.8}
                      >
                        {item.imageUrl ?
                          <Image source={{ uri: item.imageUrl }} style={styles.causesImage} />
                          : <Image source={defaultCover} style={styles.causesImage} />}

                        <View style={styles.linearGradientView}>
                          <LinearGradient colors={['#154A59', '#154A59CC', '#154A5900']} style={styles.linearGradient}>
                          </LinearGradient>
                        </View>

                        <Text style={styles.causesText}>{item.name}</Text>

                      </TouchableOpacity>

                    }}
                  /> : <ActivityIndicator size='small' color="#fff" />}

              </View> : <></>}

              {/* --------- Causes Supported Ends ------------- */}



              <View style={{ backgroundColor: '#F7F7F5', paddingBottom: 15 }}>


                {/* --------- Specialization Starts ------------- */}

                {this.props.user.body && this.props.user.body.specialities && this.props.user.body.specialities.length ?

                  <TouchableOpacity activeOpacity={0.9}
                    onPress={this.specializationExpand}
                    style={this.state.specializationExpand ? styles.expandedStrip : styles.expandToViewStrip}>
                    {
                      this.state.specializationExpand ?
                        <View style={{ width: '100%', marginTop: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                          <Text style={{ color: '#154A59', fontFamily: 'Montserrat-Bold', fontSize: 14 }}>SPECIALIZATION</Text>
                          <View style={{ height: 16, width: 16, borderRadius: 8, backgroundColor: '#E8ECEB', alignItems: 'center', justifyContent: 'center', }}>
                            <Icon name="Arrow_Up" color="#698F8A" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 5 }} />
                          </View>
                        </View>
                        :
                        <><Text style={{ color: '#154A59', fontFamily: 'Montserrat-Bold', fontSize: 14 }}>SPECIALIZATION</Text>
                          <View style={{ height: 16, width: 16, borderRadius: 8, backgroundColor: '#E8ECEB', alignItems: 'center', justifyContent: 'center', }}>
                            <Icon name="Arrow_Down" color="#698F8A" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 5 }} />
                          </View></>
                    }

                    {
                      this.state.specializationExpand && this.props.user.body ?
                        <>
                          <FlatList
                            columnWrapperStyle={{ flexDirection: "row", flexWrap: "wrap", paddingVertical: 15 }}
                            numColumns={9}
                            data={this.props.user.body.specialities.slice(0, 5)}
                            keyExtractor={(item) => item}
                            renderItem={({ item }) => (
                              <View style={styles.itemBox}>
                                <Text numberOfLines={1} style={{ fontSize: 12, color: '#367681', fontFamily: 'Montserrat-SemiBold' }}>
                                  {item}
                                </Text>
                              </View>
                            )}
                          />

                          {this.props.user.body.specialities.length > 5 ? <View style={{ width: '100%', justifyContent: 'center', alignItems: 'flex-end', paddingBottom: 15 }}>
                            <TouchableOpacity onPress={() => this.setState({ specializationModalOpen: true })}>
                              <Text style={{ color: '#91B3A2', fontFamily: 'Montserrat-Medium', fontSize: 14 }}>See All</Text>
                            </TouchableOpacity>
                          </View> : <></>}
                        </>
                        :
                        <></>
                    }
                  </TouchableOpacity> : <></>}

                {/* --------- Specialization Ends ------------- */}


                {/* --------- SKILLS Starts ------------- */}

                {/* --------- SKILLS Ends ------------- */}


                {/* --------- INTERESTS Starts ------------- */}

                {/* --------- INTERESTS Ends ------------- */}

              </View>



              {/* --------- Hashtag Start ------------- */}

              {this.props.userHashTags.body && this.props.userHashTags.body.hashTags.length ?
                <View style={{ backgroundColor: '#F7F7F5', paddingVertical: 15 }}>
                  <View style={styles.hashtagview}>
                    <Text style={{ color: '#154A59', fontFamily: 'Montserrat-Bold', fontSize: 14 }}>HASHTAG</Text>

                    {
                      this.props.userHashTags.body ?

                        <FlatList
                          columnWrapperStyle={{ flexDirection: "row", flexWrap: "wrap", paddingVertical: 15, alignItems: 'center', justifyContent: 'center' }}
                          numColumns={9}
                          data={this.props.userHashTags.body.hashTags.slice(0, 6)}
                          keyExtractor={(item) => item}
                          renderItem={({ item, index }) => (
                            index === 5 ?
                              <TouchableOpacity onPress={() => this.setState({ hashTagModalOpen: true })}
                                activeOpacity={0.5} style={{ height: 30, width: 30, borderRadius: 15, borderColor: '#91B3A2', borderWidth: 1, marginHorizontal: 5, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 12, fontFamily: 'Montserrat-SemiBold', color: '#367681' }}>...</Text>
                              </TouchableOpacity>
                              :
                              <View style={styles.hashtagItem}>
                                <Text numberOfLines={1}
                                  style={{ color: '#367681', fontFamily: 'Montserrat-SemiBold', fontSize: 12 }}>
                                  {item}
                                </Text>
                              </View>
                          )}
                        />
                        :
                        <></>
                    }

                  </View>
                </View> : <></>}

              {/* --------- Hashtag  Ends ------------- */}



            </View>

            {/* -------- About Ends-------------- */}


          </View>

        </ScrollView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  linearGradient2: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6
  },

  linearGradientView2: {
    width: '100%',
    height: 500,
    position: 'absolute',
    bottom: 150,
    alignSelf: 'center'
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6
  },

  linearGradientView: {
    width: 120,
    height: 80,
    position: 'absolute',
    top: 0
  },
  termsmodal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '85%',
    paddingVertical: 5.5,
    paddingLeft: 30
  },
  termsModalNew: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '85%',
    paddingVertical: 11,
    borderBottomWidth: 0.55,
    borderBottomColor: '#E8ECEB'
  },
  modalText: {
    fontSize: 14,
    color: '#154A59',
    fontFamily: 'Montserrat-SemiBold',
    marginLeft: 14
  },
  modalCategoriesContainer: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    paddingVertical: 20,
    // height: 250
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 6 : 0
  },
  crossButtonContainer: {
    alignSelf: 'center',
    width: 42,
    height: 42,
    borderRadius: 21,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E7F3E3',
    marginBottom: 10
  },
  modalCountry: {
    marginTop: 'auto',
  },

  causesText: {
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
    position: 'absolute',
    top: 6,
    left: 8,
    zIndex: 2,
    width: 110
  },
  causesImage: {
    width: 120,
    height: 150,
    borderRadius: 6
  },
  individualCause: {
    marginRight: 16
  },
  seeAllText: {
    color: '#698F8A',
    fontSize: 11,
    fontFamily: 'Montserrat-Medium'
  },
  seeAll: {
    marginHorizontal: 16,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 26,
    width: 70,
    borderWidth: 1,
    borderColor: '#698F8A',
    borderRadius: 13,
    paddingHorizontal: 6,
    paddingVertical: 2
  },
  causesFlatList: {
    paddingTop: 20,
    paddingBottom: 10
  },
  causesSupportedText: {
    color: '#154A59',
    fontSize: 14,
    fontFamily: 'Montserrat-Bold',
    marginLeft: 8
  },
  causesSupported: {
    backgroundColor: '#E7F3E3',
    paddingVertical: 20,
    paddingLeft: 16,
  },

  container: {
    backgroundColor: '#F7F7F5',
  },
  imageContainer: {
    width: '100%',
    height: screenHeight * .16,
    backgroundColor: COLORS.white
  },
  imageUsername: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingLeft: 20
  },
  header: {
    flexDirection: 'row',
    marginBottom: 38,
    marginTop: -10
  },
  userProfile: {
    marginBottom: 24,
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    alignSelf: 'center',
    borderColor: COLORS.grey_300,
    borderWidth: 0.6,
    borderRadius: 4,
    shadowColor: COLORS.dark_800_t50,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.00,
    elevation: 1,
  },
  userName: {
    color: '#154A59',
    fontSize: 20,
    fontFamily: 'Montserrat-SemiBold'
  },
  viewProfile: {
    fontSize: 14,
    color: '#698F8A',
    fontFamily: 'Montserrat-Medium'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  border: {
    borderBottomWidth: .6,
    borderBottomColor: '#91B3A2',
    width: '84%',
    marginLeft: 30,
  },
  image: {
    height: screenHeight * .25,
    width: '190',
  },
  mainSection: {
    backgroundColor: '#FFFFFF',
    width: '100%',
    borderTopWidth: 0,
  },

  requestEndorsement: {
    width: 201,
    height: 35,
    borderRadius: 20,
    backgroundColor: '#367681',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: 25
  },
  cardView: {
    width: '90%',
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    marginBottom: 23,
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  businessPageList: {
    height: 56,
    width: '90%',
    backgroundColor: '#fff',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 23,
    paddingBottom: 10,
    marginTop: 15,
  },
  borderView: {
    borderColor: '#E8ECEB',
    borderWidth: 0.5,
    width: '92%',
    alignSelf: 'center'
  },
  expandToViewStrip: {
    width: '90%',
    height: 45,
    borderRadius: 6,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    alignItems: 'center',
    marginTop: 15,
    alignSelf: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  expandedStrip: {
    width: '90%',
    // height: 202,
    borderRadius: 6,
    backgroundColor: '#fff',
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    paddingHorizontal: 15,
    alignItems: 'center',
    marginTop: 15,
    alignSelf: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  itemBox: {
    // width: 90,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomColor: '#D9E1E4',
    borderBottomWidth: 3,
    marginHorizontal: 5,
    marginVertical: 8
  },
  hashtagview: {
    width: '70%',
    alignItems: 'center',
    alignSelf: 'center'
  },
  hashtagItem: {
    height: 31,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: '#91B3A2',
    backgroundColor: '#CFE7C780',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    marginHorizontal: 5,
    marginVertical: 6
  }

})

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.companyProfileReducer.userDataProgress,
    user: state.companyProfileReducer.user,
    error: state.companyProfileReducer.error,

    userAddressProgress: state.companyProfileReducer.userAddressProgress,
    userAddress: state.companyProfileReducer.userAddress,
    errorAddress: state.companyProfileReducer.errorAddress,

    userSkillsSpecializationProgress: state.companyProfileReducer.userSkillsSpecializationProgress,
    userSkillsSpecialization: state.companyProfileReducer.userSkillsSpecialization,
    errorSkillsSpecialization: state.companyProfileReducer.errorSkillsSpecialization,

    userRecentActivityProgress: state.companyProfileReducer.userRecentActivityProgress,
    userRecentActivity: state.companyProfileReducer.userRecentActivity,
    errorRecentActivity: state.companyProfileReducer.errorRecentActivity,

    userExperienceProgress: state.companyProfileReducer.userExperienceProgress,
    userExperience: state.companyProfileReducer.userExperience,
    errorExperience: state.companyProfileReducer.errorExperience,

    userEducationProgress: state.companyProfileReducer.userEducationProgress,
    userEducation: state.companyProfileReducer.userEducation,
    errorEducation: state.companyProfileReducer.errorEducation,

    userCircleProgress: state.circleReducer.userCircleProgress,
    userCircle: state.circleReducer.userCircle,
    errorCircle: state.circleReducer.errorCircle,

    userEndorsementProgress: state.endorsementReducer.userEndorsementProgress,
    userEndorsement: state.endorsementReducer.userEndorsement,
    errorEndorsement: state.endorsementReducer.errorEndorsement,

    userBusinessPageProgress: state.companyProfileReducer.userBusinessPageProgress,
    userBusinessPage: state.companyProfileReducer.userBusinessPage,
    errorBusinessPage: state.companyProfileReducer.errorBusinessPage,

    userCausesProgress: state.causesReducer.userCausesProgress,
    userCauses: state.causesReducer.userCauses,
    errorCauses: state.causesReducer.errorCauses,

    userHashTagsProgress: state.companyProfileReducer.userHashTagsProgress,
    userHashTags: state.companyProfileReducer.userHashTags,
    errorHashTags: state.companyProfileReducer.errorHashTags,

    userConnectionInfoProgress: state.companyProfileReducer.userConnectionInfoProgress,
    userConnectionInfo: state.companyProfileReducer.userConnectionInfo,
    errorConnectionInfo: state.companyProfileReducer.errorConnectionInfo,

    userConnectsProgress: state.connectsReducer.userConnectsProgress,
    userConnects: state.connectsReducer.userConnects,
    errorConnects: state.connectsReducer.errorConnects
  }
}

const mapDispatchToProps = (dispatch) => {

  return {
    companyProfileRequest: (data) => dispatch(companyProfileRequest(data)),
    companyAddressRequest: (data) => dispatch(companyAddressRequest(data)),
    companySkillsSpecializationRequest: (data) => dispatch(companySkillsSpecializationRequest(data)),
    companyRecentActivityRequest: (data) => dispatch(companyRecentActivityRequest(data)),
    companyExperienceRequest: (data) => dispatch(companyExperienceRequest(data)),
    companyEducationRequest: (data) => dispatch(companyEducationRequest(data)),
    userCircleRequest: (data) => dispatch(userCircleRequest(data)),
    endorsementRequest: (data) => dispatch(endorsementRequest(data)),
    companyBusinessPageRequest: (data) => dispatch(companyBusinessPageRequest(data)),
    causesRequest: (data) => dispatch(causesRequest(data)),
    companyHashTagsRequest: (data) => dispatch(companyHashTagsRequest(data)),
    companyConnectionInfoRequest: (data) => dispatch(companyConnectionInfoRequest(data)),
    connectsRequest: (data) => dispatch(connectsRequest(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
