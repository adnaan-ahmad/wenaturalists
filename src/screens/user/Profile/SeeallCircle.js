import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, ActivityIndicator, SafeAreaView } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'

import circleDefault from '../../../../assets/CirclesDefault.png'
import { userCircleRequest } from '../../../services/Redux/Actions/User/CircleActions'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultCover from '../../../../assets/defaultCover.png'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultStyle from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

class SeeallCircle extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {

            this.props.route.params && this.props.route.params.id !== '' ?
                this.props.userCircleRequest({ userId: this.props.route.params.id, otherUserId: value })
                : this.props.userCircleRequest({ userId: value, otherUserId: '' })

        }).catch((e) => {
            console.log(e)
        })

    }

    renderItem = (item) => {
        return (

            <TouchableOpacity
                activeOpacity={0.9}
                onPress={() => this.props.navigation.navigate("CircleProfileStack", {
                    screen: 'CircleProfile',
                    params: { slug: item.slug },
                })} style={styles.circleCard}>
                {item.coverImage ?
                    <Image style={{ width: 150, height: 80, borderTopRightRadius: 8, borderTopLeftRadius: 8 }} source={{ uri: item.coverImage }} />
                    :
                    <Image style={{ width: 150, height: 80, borderTopRightRadius: 8, borderTopLeftRadius: 8 }} source={{ uri: 'https://cdn.dscovr.com/images/prof-banner.webp' }} />
                }

                {item.profileImage ?
                    <Image style={{ width: 60, height: 60, borderRadius: 30, marginTop: -50 }} source={{ uri: item.profileImage }} />
                    :
                    <Image style={{ width: 60, height: 60, borderRadius: 30, marginTop: -50 }} source={circleDefault} />
                }

                <Text numberOfLines={1} style={styles.circleTitle}>{item.title}</Text>
                <Text numberOfLines={1} style={styles.circleSubTitle}>{item.memberType}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <SafeAreaView>

                <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 12 }] : styles.header}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.navigate("ProfileScreen")}>
                            <Icon name="Arrow-Left" size={14} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                        </TouchableOpacity>
                        <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_800, marginLeft: 5 }]}>
                            CIRCLES
                        </Text>
                    </View>
                </View>

                {this.props.userCircle.body ?
                    <FlatList
                        initialNumToRender={10}
                        contentContainerStyle={{ flexDirection: "row", flexWrap: "wrap", paddingTop: 10, paddingBottom: 46, justifyContent: 'center' }}
                        // columnWrapperStyle={{flexDirection : "row", flexWrap : "wrap", paddingVertical:10, justifyContent:'center'}}
                        // numColumns={4000}
                        keyExtractor={(item) => item.id}
                        data={this.props.userCircle.body.content}
                        renderItem={({ item }) => (
                            this.renderItem(item)
                        )}
                    />
                    :
                    <ActivityIndicator size='small' color="#00394D" />
                }
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    circleCard: {
        width: 150,
        height: 146,
        borderRadius: 8,
        backgroundColor: '#fff',
        marginHorizontal: 8,
        marginVertical: 11,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    circleTitle: {
        color: '#00394D',
        fontSize: 12,
        fontFamily: 'Montserrat-Bold',
        marginTop: 10,
        maxWidth: '85%'
    },
    circleSubTitle: {
        color: '#607580',
        fontSize: 10,
        fontFamily: 'Montserrat-Medium',
        maxWidth: '85%'
    }
})

const mapStateToProps = (state) => {
    return {
        userCircleProgress: state.circleReducer.userCircleProgress,
        userCircle: state.circleReducer.userCircle,
        errorCircle: state.circleReducer.errorCircle,
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        userCircleRequest: (data) => dispatch(userCircleRequest(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeeallCircle)