import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Platform,
  ActivityIndicator,
  FlatList,
  Image,
  Modal,
  SafeAreaView,
  Linking,
  Share,
  Clipboard,
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import Autolink from 'react-native-autolink'

import typography from '../../../Components/Shared/Typography'
import { tagDescription } from '../../../Components/Shared/commonFunction'
import RecentActivityItem from '../../../Components/User/Common/RecentActivityItem'
import circleDefault from '../../../../assets/CirclesDefault.png';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import { COLORS } from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import httpService from '../../../services/AxiosInterceptors';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {
  personalProfileRequest,
  personalSkillsSpecializationRequest,
  personalRecentActivityRequest,
  personalExperienceRequest,
  personalEducationRequest,
  personalBusinessPageRequest,
  personalHashTagsRequest,
  personalConnectionInfoRequest,
  personalAddressRequest,
} from '../../../services/Redux/Actions/User/PersonalProfileActions';
import { userCircleRequest } from '../../../services/Redux/Actions/User/CircleActions';
import { endorsementRequest } from '../../../services/Redux/Actions/User/EndorsementActions';
import { causesRequest } from '../../../services/Redux/Actions/User/CausesActions';
import { connectsRequest } from '../../../services/Redux/Actions/User/ConnectsActions';

import projectDefault from '../../../../assets/project-default.jpg';
import defaultCover from '../../../../assets/defaultCover.png';
import InstitutionLogo from '../../../../assets/InstitutionLogo.png';
import ProfileSvg from '../../../Components/User/Profile/ProfileSvg';
import RequestEndorsement from '../../../Components/User/Profile/RequestEndorsement';

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

httpService.setupInterceptors()

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {},
      coverImage: '',
      userId: '',
      skills: [],
      connectionInfo: {},
      specializationExpand: false,
      skillExpand: false,
      interestExpand: false,
      optionsModalOpen: false,
      requestEndorsementModalOpen: false,
      modalOpen: false,
      selectedExperienceType: 'ASSIGNMENTEVENTTRAINING',
      skillsModalOpen: false,
      specializationModalOpen: false,
      interestsModalOpen: false,
      readMore: false,
      readMoreExperienceId: '',
      currentScrollPosition: 0,
      paramsId: '',
      connected: false,
      following: 0,
      hashTagModalOpen: false,
      shareModalOpen: false,
      contactInfoModalOpen: false,
      isConnected: '',
      mutualConnects: [],
      experienceList: [],
      followingCount: 0,
      awardList: [],
      hashtagList: []
    }
  }

  componentDidMount() {
    // console.log('================================ this.props.route.params =======================',this.props.route.params)
    this.props.route.params
      ? this.setState({ paramsId: this.props.route.params.userId })
      : null;

    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    })

    AsyncStorage.getItem('userId').then((value) => {
      this.setState({ userId: value }, () => {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
          // this.setState({ selectedExperienceType: 'ASSIGNMENTEVENTTRAINING' })
          this.getAllAwardList(value)
          this.getPopularHashtags(value)
          this.getConnectionInfo(value)
          this.props.personalProfileRequest({ userId: value, otherUserId: '' })
          this.props.personalAddressRequest({ userId: value, otherUserId: '' })
          this.props.personalSkillsSpecializationRequest({
            userId: value,
            otherUserId: '',
          });
          this.props.personalRecentActivityRequest({
            userId: value,
            otherUserId: '',
          });
          this.props.personalExperienceRequest({ userId: value, otherUserId: '' })
          this.getAllEvents(value)
          this.props.personalEducationRequest({ userId: value, otherUserId: '' })
          this.props.userCircleRequest({ userId: value, otherUserId: '' })
          this.props.endorsementRequest({ userId: value, otherUserId: '' })
          this.props.personalBusinessPageRequest({
            userId: value,
            otherUserId: '',
          });
          this.props.causesRequest({ userId: value, otherUserId: '' });
          this.props.personalHashTagsRequest({ userId: value, otherUserId: '' })
          this.props.personalConnectionInfoRequest({ userId: value })
        })
      })

      this.getAllAwardList(value)
      this.getPopularHashtags(value)
      this.getConnectionInfo(value)
      this.props.personalProfileRequest({ userId: value, otherUserId: '' })
      this.props.personalAddressRequest({ userId: value, otherUserId: '' })
      this.props.personalSkillsSpecializationRequest({
        userId: value,
        otherUserId: '',
      });
      this.props.personalRecentActivityRequest({
        userId: value,
        otherUserId: '',
      });
      this.props.personalExperienceRequest({ userId: value, otherUserId: '' })
      this.getAllEvents(value)
      this.props.personalEducationRequest({ userId: value, otherUserId: '' })
      this.props.userCircleRequest({ userId: value, otherUserId: '' })
      this.props.endorsementRequest({ userId: value, otherUserId: '' })
      this.props.personalBusinessPageRequest({
        userId: value,
        otherUserId: '',
      });
      this.props.causesRequest({ userId: value, otherUserId: '' });
      this.props.personalHashTagsRequest({ userId: value, otherUserId: '' })
      this.props.personalConnectionInfoRequest({ userId: value })

    })
      .catch((e) => {
        // console.log(e)
      });

    this.props.route.params && this.props.userConnectionInfo.body
      ? this.getFollowers()
      : null
  }

  componentWillUnmount() {
    this._unsubscribe()
  }

  getPopularHashtags = (userId) => {
    axios({
      method: 'get',
      url: `${REACT_APP_userServiceURL}/tags/getHashtagByUserId/?userId=${userId}&page=${0}&size=24&filter=POPULAR`,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            hashtagList: this.state.hashtagList.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  getAllAwardList(userId) {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/award/list?entityId=' + userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.message === 'Success!') {
        console.log('getAllAwardList', response.data.body)
        this.setState({
          awardList: response.data.body
        })
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  getJobs = (userId) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/profile/get/experience?id=' + userId + '&page=0&size=6',
      cache: true,
      withCredentials: true,
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => {
      if (response && response.status === 200 && response.data.body) {
        console.log('getJobs', response.data.body.experience.content[1])
        this.setState({ experienceList: response.data.body.experience.content })
      }
    }).catch((e) => {
      console.log(e)
    })
  }

  getAllEvents = (userId) => {

    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + "/backend/participation/find-project-published-applicants-details-by-userId-and-custom-project-type/" + userId + "/" + this.state.selectedExperienceType + "?page=" + 0 + "&size=" + 6,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.message === 'Success!') {
        console.log('getAllEvents', response.data.body.content[1])

        this.setState({ experienceList: response.data.body.content })
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  getJoinedList = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/cause/joined/list?userId=' +
        userId +
        '&page=' +
        0 +
        '&size=' +
        100,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          console.log('getJoinedList', response.data.body.content[0]);
          this.setState({ personalJoinedCauses: response.data.body.content });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  changeState = (value) => {
    this.setState(value);
  };

  skillExpand = () => {
    this.setState({ skillExpand: !this.state.skillExpand });
  };

  specializationExpand = () => {
    this.setState({ specializationExpand: !this.state.specializationExpand });
  };

  interestExpand = () => {
    this.setState({ interestExpand: !this.state.interestExpand });
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.user !== this.props.user) {
      return true;
    }
    if (
      nextProps.userSkillsSpecialization !== this.props.userSkillsSpecialization
    ) {
      return true;
    }
    if (nextProps.userRecentActivity !== this.props.userRecentActivity) {
      return true;
    }
    if (nextProps.userExperience !== this.props.userExperience) {
      return true;
    }
    if (nextProps.userEducation !== this.props.userEducation) {
      return true;
    }
    if (nextProps.userCircle !== this.props.userCircle) {
      return true;
    }
    if (nextProps.userEndorsement !== this.props.userEndorsement) {
      return true;
    }
    if (nextProps.userBusinessPage !== this.props.userBusinessPage) {
      return true;
    }
    if (nextProps.userCauses !== this.props.userCauses) {
      return true;
    }
    if (nextProps.userHashTags !== this.props.userHashTags) {
      return true;
    }
    if (nextProps.userConnectionInfo !== this.props.userConnectionInfo) {
      return true;
    }
    if (nextProps.userConnects !== this.props.userConnects) {
      return true;
    }

    if (nextState.hashtagList !== this.state.hashtagList) {
      return true;
    }
    if (nextState.awardList !== this.state.awardList) {
      return true;
    }
    if (nextState.followingCount !== this.state.followingCount) {
      return true;
    }
    if (nextState.mutualConnects !== this.state.mutualConnects) {
      return true;
    }
    if (nextState.isConnected !== this.state.isConnected) {
      return true;
    }
    if (nextState.readMoreExperienceId !== this.state.readMoreExperienceId) {
      return true;
    }
    if (nextState.contactInfoModalOpen !== this.state.contactInfoModalOpen) {
      return true;
    }
    if (nextState.shareModalOpen !== this.state.shareModalOpen) {
      return true;
    }
    if (nextState.hashTagModalOpen !== this.state.hashTagModalOpen) {
      return true;
    }
    if (nextState.paramsId !== this.state.paramsId) {
      return true;
    }
    if (nextState.userData !== this.state.userData) {
      return true;
    }
    if (nextState.userId !== this.state.userId) {
      return true;
    }
    if (nextState.coverImage !== this.state.coverImage) {
      return true;
    }
    if (nextState.skills !== this.state.skills) {
      return true;
    }
    if (nextState.connectionInfo !== this.state.connectionInfo) {
      return true;
    }
    if (nextState.specializationExpand !== this.state.specializationExpand) {
      return true;
    }
    if (nextState.skillExpand !== this.state.skillExpand) {
      return true;
    }
    if (nextState.interestExpand !== this.state.interestExpand) {
      return true;
    }
    if (nextState.optionsModalOpen !== this.state.optionsModalOpen) {
      return true;
    }
    if (
      nextState.requestEndorsementModalOpen !==
      this.state.requestEndorsementModalOpen
    ) {
      return true;
    }
    if (nextState.modalOpen !== this.state.modalOpen) {
      return true;
    }
    if (nextState.skillsModalOpen !== this.state.skillsModalOpen) {
      return true;
    }
    if (
      nextState.specializationModalOpen !== this.state.specializationModalOpen
    ) {
      return true;
    }
    if (nextState.interestsModalOpen !== this.state.interestsModalOpen) {
      return true;
    }
    if (nextState.readMore !== this.state.readMore) {
      return true;
    }
    if (nextState.currentScrollPosition !== this.state.currentScrollPosition) {
      return true;
    }
    if (nextState.connected !== this.state.connected) {
      return true;
    }
    if (nextState.following !== this.state.following) {
      return true;
    }
    if (nextState.experienceList !== this.state.experienceList) {
      return true;
    }
    if (nextState.selectedExperienceType !== this.state.selectedExperienceType) {
      return true;
    }

    return false;
  }

  unixTime = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    return day + ' ' + month + ' ' + year;
  };

  renderItemBusinessPage = (item) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() =>
          this.props.navigation.navigate('CompanyProfileScreen', {
            userId: item.companyId,
          })
        }
        style={{ marginLeft: 10 }}>
        <View style={styles.businessPageList}>
          {item.profileImageUrl ? (
            <Image
              source={{ uri: item.profileImageUrl }}
              style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
            />
          ) : (
            <Image
              source={defaultBusiness}
              style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }}
            />
          )}

          <View>
            <Text
              style={{
                color: '#00394D',
                fontFamily: 'Montserrat-Bold',
                fontSize: 12,
              }}>
              {item.companyName.charAt(0).toUpperCase() +
                item.companyName.slice(1)}
            </Text>
            <Text
              style={{
                color: '#91B3A2',
                fontFamily: 'Montserrat-SemiBold',
                fontSize: 12,
              }}>
              {item.country}
            </Text>
          </View>
        </View>

        <View style={styles.borderView}></View>
      </TouchableOpacity>
    )
  }

  modal = () => {
    return (
      <Modal
        visible={this.state.modalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={styles.modalCountry}>
          <View
            style={{
              width: '100%',
              height: 500,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.crossButtonContainer}
            onPress={() => this.setState({ modalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={[styles.modalCategoriesContainer, { height: 360 }]}>
            <TouchableOpacity
              style={styles.termsModalNew}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  modalOpen: false,
                  selectedExperienceType: 'ASSIGNMENTEVENTTRAINING',
                  experienceList: []
                }, () => this.getAllEvents(this.state.userId))
              }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>All</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.termsModalNew}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  modalOpen: false,
                  selectedExperienceType: 'JOB',
                  experienceList: []
                }, () => this.getJobs(this.state.userId))
              }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>Job</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.termsModalNew}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  modalOpen: false,
                  selectedExperienceType: 'ASSIGNMENT',
                  experienceList: []
                }, () => this.getAllEvents(this.state.userId))
              }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>
                Assignment
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.termsModalNew}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  modalOpen: false,
                  selectedExperienceType: 'EVENT',
                  experienceList: []
                }, () => this.getAllEvents(this.state.userId))
              }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>Event</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[styles.termsModalNew]}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  modalOpen: false,
                  selectedExperienceType: 'TRAINING',
                  experienceList: []
                }, () => this.getAllEvents(this.state.userId))
              }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>Training</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[styles.termsModalNew]}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  modalOpen: false,
                  selectedExperienceType: 'EXPEDITION',
                  experienceList: []
                }, () => this.getAllEvents(this.state.userId))
              }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>Expedition</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[styles.termsModalNew]}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  modalOpen: false,
                  selectedExperienceType: 'STORYBOOK',
                  experienceList: []
                }, () => this.getAllEvents(this.state.userId))
              }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>Story Book</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[styles.termsModalNew, { borderBottomWidth: 0 }]}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  modalOpen: false,
                  selectedExperienceType: 'FUNDRAISE',
                  experienceList: []
                }, () => this.getAllEvents(this.state.userId))
              }}>
              <Text style={[styles.modalText, { marginLeft: 0 }]}>Fund Raise</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }

  requestEndorsementModal = () => {
    return (
      <Modal
        visible={this.state.requestEndorsementModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <RequestEndorsement
          changeState={this.changeState}
          connectsData={
            this.props.userConnects.body
              ? this.props.userConnects.body.content.sort(
                (a, b) =>
                  a.firstName.toUpperCase() > b.firstName.toUpperCase(),
              )
              : null
          }
        />
      </Modal>
    );
  }

  skillsModal = () => {
    return (
      <Modal
        visible={this.state.skillsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View
            style={{
              width: '100%',
              height: 500,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.crossButtonContainer}
            onPress={() => this.setState({ skillsModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={[styles.modalCategoriesContainer, { height: 350 }]}>
            {this.props.user.body ? (
              <FlatList
                columnWrapperStyle={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 6,
                }}
                numColumns={3}
                showsVerticalScrollIndicator={false}
                data={this.props.user.body.skills}
                keyExtractor={(item) => item}
                renderItem={({ item }) => (
                  <View style={styles.itemBox}>
                    <Text
                      onPress={() => this.setState({ skillsModalOpen: false }, () => {
                        this.props.navigation.navigate('PeopleWithSimilar', {
                          id: this.state.userId,
                          totalMutualConnects: this.props.route.params
                            ? this.state.mutualConnects.length
                            : null,
                          currentTab: 'connects',
                          peopleWithSimilar: 'Skills',
                          text: item
                        })
                      })}
                      numberOfLines={1}
                      style={{
                        fontSize: 12,
                        color: '#367681',
                        fontFamily: 'Montserrat-SemiBold',
                      }}>
                      {item}
                    </Text>
                  </View>
                )}
              />
            ) : (
              <></>
            )}
          </View>
        </View>
      </Modal>
    );
  };

  specializationModal = () => {
    return (
      <Modal
        visible={this.state.specializationModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View
            style={{
              width: '100%',
              height: 500,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.crossButtonContainer}
            onPress={() => this.setState({ specializationModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={[styles.modalCategoriesContainer, { height: 350 }]}>
            {this.props.user.body ? (
              <FlatList
                columnWrapperStyle={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 6,
                }}
                numColumns={3}
                showsVerticalScrollIndicator={false}
                data={this.props.user.body.specialities}
                keyExtractor={(item) => item}
                renderItem={({ item }) => (
                  <View style={styles.itemBox}>
                    <Text
                      onPress={() => this.setState({ specializationModalOpen: false }, () => {
                        this.props.navigation.navigate('PeopleWithSimilar', {
                          id: this.state.userId,
                          totalMutualConnects: this.props.route.params
                            ? this.state.mutualConnects.length
                            : null,
                          currentTab: 'connects',
                          peopleWithSimilar: 'Specialization',
                          text: item
                        })
                      })}
                      numberOfLines={1}
                      style={{
                        fontSize: 12,
                        color: '#367681',
                        fontFamily: 'Montserrat-SemiBold',
                      }}>
                      {item}
                    </Text>
                  </View>
                )}
              />
            ) : (
              <ActivityIndicator size="small" color="#fff" />
            )}
          </View>
        </View>
      </Modal>
    );
  };

  interestsModal = () => {
    return (
      <Modal
        visible={this.state.interestsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View
            style={{
              width: '100%',
              height: 500,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.crossButtonContainer}
            onPress={() => this.setState({ interestsModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={[styles.modalCategoriesContainer, { height: 350 }]}>
            {this.props.user.body ? (
              <FlatList
                columnWrapperStyle={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 6,
                }}
                numColumns={3}
                showsVerticalScrollIndicator={false}
                data={this.props.user.body.interests}
                keyExtractor={(item) => item}
                renderItem={({ item }) => (
                  <View style={styles.itemBox}>
                    <Text
                      onPress={() => this.setState({ interestsModalOpen: false }, () => {
                        this.props.navigation.navigate('PeopleWithSimilar', {
                          id: this.state.userId,
                          totalMutualConnects: this.props.route.params
                            ? this.state.mutualConnects.length
                            : null,
                          currentTab: 'connects',
                          peopleWithSimilar: 'Interest',
                          text: item
                        })
                      })}
                      numberOfLines={1}
                      style={{
                        fontSize: 12,
                        color: '#367681',
                        fontFamily: 'Montserrat-SemiBold',
                      }}>
                      {item}
                    </Text>
                  </View>
                )}
              />
            ) : (
              <ActivityIndicator size="small" color="#fff" />
            )}
          </View>
        </View>
      </Modal>
    );
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={styles.modalCountry}>
          <View style={styles.linearGradientView2}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={styles.linearGradient2}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.crossButtonContainer}
            onPress={() => this.setState({ optionsModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          {this.props.route.params ? (
            <View style={styles.modalCategoriesContainer}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, { paddingVertical: 15 }]
                    : styles.termsmodal
                }
                activeOpacity={0.6}
                onPress={() =>
                  this.setState({
                    optionsModalOpen: false,
                    contactInfoModalOpen: true,
                  })
                }>
                <Icon
                  name="Email_At"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
                <Text style={styles.modalText}>View Contact Info</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={[
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, { paddingVertical: 15 }]
                    : styles.termsmodal,
                  { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' },
                ]}
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({ optionsModalOpen: false });
                }}>
                <Icon
                  name="Feedback"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? { marginTop: 10 } : {}}
                />
                <Text style={styles.modalText}>Endorse</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, { paddingTop: 25, paddingBottom: 15 }]
                    : styles.termsmodal
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({ optionsModalOpen: false });
                }}>
                <Icon
                  name="Caution"
                  size={16}
                  color="#913838"
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
                <Text style={styles.modalText}>Report User</Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.modalCategoriesContainer}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, { paddingVertical: 15 }]
                    : styles.termsmodal
                }
                activeOpacity={0.6}
                onPress={() =>
                  this.setState({ optionsModalOpen: false, shareModalOpen: true })
                }>
                <Icon
                  name="Share"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
                <Text style={styles.modalText}>Share Profile Page</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={[
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, { paddingVertical: 15 }]
                    : styles.termsmodal,
                  { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' },
                ]}
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    optionsModalOpen: false,
                    contactInfoModalOpen: true,
                  });
                }}>
                <Icon
                  name="Email_At"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
                <Text style={styles.modalText}>View Contact Info</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, { paddingTop: 25, paddingBottom: 15 }]
                    : styles.termsmodal
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({ optionsModalOpen: false }),
                    this.props.navigation.navigate('ProfileEditStack');
                }}>
                <Icon
                  name="EditBox"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
                <Text style={styles.modalText}>Edit Profile</Text>
              </TouchableOpacity>

              {(this.props.userEducation.body && this.props.userEducation.body.list && this.props.userEducation.body.list.length) ||
                (this.props.userCircle.body && this.props.userCircle.body.content && this.props.userCircle.body.content.length) ||

                (this.props.user.body && this.props.user.body.specialities && this.props.user.body.specialities.length) ||
                (this.props.user.body && this.props.user.body.skills && this.props.user.body.skills.length) ||
                (this.props.user.body && this.props.user.body.interests && this.props.user.body.interests.length) ||
                (this.props.userBusinessPage.body && this.props.userBusinessPage.body.businessPage.length)
                ?
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [styles.termsmodal, { paddingVertical: 15 }]
                      : styles.termsmodal
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({ optionsModalOpen: false }),
                      this.props.navigation.navigate('ReorderProfileScreen');
                  }}>
                  <Icon
                    name="TxEdi_Bullet"
                    size={16}
                    color="#154A59"
                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                  />
                  <Text style={styles.modalText}>Reorder Profile page</Text>
                </TouchableOpacity> : <></>}

              <TouchableOpacity
                onPress={() =>
                  this.setState({ optionsModalOpen: false }, () =>
                    this.props.navigation.navigate('PrivacySettingStack'),
                  )
                }
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, { paddingVertical: 15 }]
                    : styles.termsmodal
                }
                activeOpacity={0.6}>
                <Icon
                  name="Setting"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
                <Text style={styles.modalText}>Privacy & Settings</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    )
  }

  renderDescription = (description, id) => {

    return (
      <Autolink
        text={description && this.trimDescription(description)}
        email
        hashtag="instagram"
        mention="twitter"
        phone="sms"
        numberOfLines={
          this.state.readMoreExperienceId !== id ? 3 : 20
        }
        style={[
          typography.Body_1,
          {
            fontSize: 11,
            color: COLORS.dark_700,
            marginLeft: 6,
            marginTop: 8,
            marginBottom: 20
          }
        ]}
        url
      />
    )
  }

  renderItem = (experience, index) => {
    if (index + 1 >= 6)
      return (
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('SeeallExperienceScreen', {
              id: this.state.paramsId,
              selectedExperienceType: this.state.selectedExperienceType
            })
          }
          key={experience.project.id}
          activeOpacity={0.5}
          style={{
            marginHorizontal: 16,
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            height: 26,
            width: 70,
            borderWidth: 1,
            borderColor: '#698F8A',
            borderRadius: 13,
            paddingHorizontal: 6,
            paddingVertical: 2,
          }}>
          <Text
            style={{
              color: '#698F8A',
              fontSize: 11,
              fontFamily: 'Montserrat-Medium',
            }}>
            See All
          </Text>
        </TouchableOpacity>
      );
    // else if (this.props.userExperience.body.experience.content.length < 6 && index + 1 === this.props.userExperience.body.experience.content.length ) return <TouchableOpacity onPress={() => this.props.navigation.navigate("SeeallExperienceScreen")}
    //   key={experience.id} activeOpacity={0.5} style={{ marginHorizontal: 16, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', height: 26, width: 70, borderWidth: 1, borderColor: '#698F8A', borderRadius: 13, paddingHorizontal: 6, paddingVertical: 2 }}>
    //   <Text style={{ color: '#698F8A', fontSize: 11, fontFamily: 'Montserrat-Medium' }}>See All</Text>
    // </TouchableOpacity>
    else
      return (
        <View
          style={{
            backgroundColor: '#FFFFFF',
            marginHorizontal: 5,
            width: 244,
            borderRadius: 6,
            marginVertical: 8,
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,
          }}
          key={experience.project.id}>
          <View
            style={{
              paddingBottom: 14,
              marginRight: 10,
              padding: 6,
              width: 244,
              borderRadius: 6,
            }}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => this.props.navigation.navigate('ProjectDetailView', {
                slug: experience.project.slug,
              })}
              style={{
                flexDirection: 'row',
                marginRight: 10,
                borderRadius: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {experience.project.coverImage !== null ? (
                <Image
                  source={{ uri: experience.project.coverImage }}
                  style={{ width: 50, height: 50, borderRadius: 5 }}
                />
              ) : (
                <Image
                  source={projectDefault}
                  style={{ width: 50, height: 50, borderRadius: 5 }}
                />
              )}
              <View
                style={{
                  width: 150,
                  backgroundColor: '#FFF',
                  justifyContent: 'center',
                  borderTopRightRadius: 5,
                  borderBottomRightRadius: 6,
                }}>
                <Text numberOfLines={2}
                  style={[
                    defaultStyle.Title_2,
                    { marginHorizontal: 12, color: COLORS.primarydark, textTransform: 'capitalize' },
                  ]}>
                  {experience.project.title}
                </Text>
                <Text
                  style={[
                    defaultStyle.Caption,
                    { marginLeft: 12, color: COLORS.altgreen_400, textTransform: 'capitalize' },
                  ]}>
                  {experience.project.companyName}
                </Text>
                <Text
                  style={[
                    defaultStyle.Body_1,
                    { marginHorizontal: 12, color: COLORS.grey, textTransform: 'capitalize' },
                  ]}>
                  {experience.project.location.city}, {experience.project.location.state}, {experience.project.location.country}
                </Text>
              </View>
            </TouchableOpacity>

            <View
              style={{
                borderColor: '#E8ECEB',
                borderWidth: 0.45,
                width: '105%',
                marginVertical: 10,
                marginLeft: -6,
              }}></View>

            {this.renderDescription(experience.project.shortBrief, experience.project.id)}

            {/* <Text
              numberOfLines={
                this.state.readMoreExperienceId !== experience.project.id ? 3 : 20
              }
              style={[
                defaultStyle.Body_2,
                {
                  marginHorizontal: 11,
                  paddingBottom: 36,
                  color: COLORS.grey_500,
                },
              ]}>
              {experience.project.shortBrief}
            </Text> */}

            <TouchableOpacity
              style={{
                height: 30,
                width: 80,
                marginLeft: 6,
                marginTop: -20,
                marginBottom: 16,
              }}
              onPress={() =>
                this.setState({
                  readMoreExperienceId:
                    this.state.readMoreExperienceId.length > 0
                      ? ''
                      : experience.project.id,
                })
              }>

              <Text style={{ color: COLORS.green_500, fontWeight: '700', fontSize: 12 }}>
                {experience.project.shortBrief.length > 105 &&
                  this.state.readMoreExperienceId !== experience.project.id
                  ? 'Read more'
                  : this.state.readMoreExperienceId === experience.project.id
                    ? 'Read less'
                    : null}
              </Text>

            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginRight: 10,
              backgroundColor: '#E7F3E3',
              borderBottomRightRadius: 6,
              borderBottomLeftRadius: 6,
              width: 244,
              height: 40,
              justifyContent: 'space-around',
              alignItems: 'center',
              position: 'absolute',
              bottom: -6,
            }}>
            {this.unixTime(experience.project.createTime)
              .split(' ')
              .splice(1)
              .join(' ') !==
              this.unixTime(experience.project.endingTime)
                .split(' ')
                .splice(1)
                .join(' ') &&
              this.unixTime(experience.project.endingTime).split(' ').splice(1).join(' ') !==
              'Jan 1970' && new Date().getTime() >= experience.project.endingTime ? (
              <Text
                style={[
                  defaultStyle.Body_2_italic,
                  { color: COLORS.altgreen_400 },
                ]}>
                {this.unixTime(experience.project.createTime)
                  .split(' ')
                  .splice(1)
                  .join(' ')}{' '}
                to{' '}
                {this.unixTime(experience.project.endingTime)
                  .split(' ')
                  .splice(1, 2)
                  .join(' ')}
              </Text>
            ) : this.unixTime(experience.project.endingTime)
              .split(' ')
              .splice(1)
              .join(' ') === 'Jan 1970' || new Date().getTime() < experience.project.endingTime ? (
              <Text
                style={[
                  defaultStyle.Body_2_italic,
                  { color: COLORS.altgreen_400 },
                ]}>
                {this.unixTime(experience.project.createTime)
                  .split(' ')
                  .splice(1)
                  .join(' ')}{' '}
                - Present
              </Text>
            ) : (
              <Text
                style={[
                  defaultStyle.Body_2_italic,
                  { color: COLORS.altgreen_400 },
                ]}>
                {this.unixTime(experience.project.createTime)
                  .split(' ')
                  .splice(1)
                  .join(' ')}
              </Text>
            )}
            <Text
              onPress={() => console.log(new Date().getTime(), experience.project.endingTime)}
              style={[
                defaultStyle.Note,
                {
                  color: COLORS.altgreen_400,
                  borderWidth: 1,
                  borderColor: '#91B3A2',
                  borderRadius: 4,
                  paddingHorizontal: 6,
                  paddingVertical: 2,
                },
              ]}>
              {experience.projectType}
            </Text>
          </View>
        </View>
      )
  }

  employmentType = (value) => {
    if (value === 'FULL_TIME') return 'Full-time'
    if (value === 'PART_TIME') return 'Part-time'
    if (value === 'SELF_EMPLOYED') return 'Self-employed'
    if (value === 'FREELANCE') return 'Freelance'
    if (value === 'INTERNSHIP') return 'Internship'
    if (value === 'TRAINEE') return 'Trainee'
  }

  renderJobItem = (experience, index) => {
    if (index + 1 >= 6)
      return (
        <TouchableOpacity
          onPress={() => {
            // this.setState({ selectedExperienceType: 'ASSIGNMENTEVENTTRAINING' }, () => {
            this.props.navigation.navigate('SeeallExperienceScreen', {
              id: this.state.paramsId,
              selectedExperienceType: this.state.selectedExperienceType
            })
            // })
          }
          }
          key={experience.id}
          activeOpacity={0.5}
          style={{
            marginHorizontal: 16,
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            height: 26,
            width: 70,
            borderWidth: 1,
            borderColor: '#698F8A',
            borderRadius: 13,
            paddingHorizontal: 6,
            paddingVertical: 2,
          }}>
          <Text
            style={{
              color: '#698F8A',
              fontSize: 11,
              fontFamily: 'Montserrat-Medium',
            }}>
            See All
          </Text>
        </TouchableOpacity>
      );
    // else if (this.props.userExperience.body.experience.content.length < 6 && index + 1 === this.props.userExperience.body.experience.content.length ) return <TouchableOpacity onPress={() => this.props.navigation.navigate("SeeallExperienceScreen")}
    //   key={experience.id} activeOpacity={0.5} style={{ marginHorizontal: 16, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', height: 26, width: 70, borderWidth: 1, borderColor: '#698F8A', borderRadius: 13, paddingHorizontal: 6, paddingVertical: 2 }}>
    //   <Text style={{ color: '#698F8A', fontSize: 11, fontFamily: 'Montserrat-Medium' }}>See All</Text>
    // </TouchableOpacity>
    else
      return (
        <View
          style={{
            backgroundColor: '#FFFFFF',
            marginHorizontal: 5,
            width: 244,
            borderRadius: 6,
            marginVertical: 8,
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,
          }}
          key={experience.id}>
          <View
            style={{
              paddingBottom: 14,
              marginRight: 10,
              padding: 6,
              width: 244,
              borderRadius: 6,
            }}>
            <View
              style={{
                flexDirection: 'row',
                marginRight: 10,
                borderRadius: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {experience.coverImageUrl !== null ? (
                <Image
                  source={{ uri: experience.coverImageUrl }}
                  style={{ width: 50, height: 50, borderRadius: 5 }}
                />
              ) : (
                <Image
                  source={projectDefault}
                  style={{ width: 50, height: 50, borderRadius: 5 }}
                />
              )}
              <View
                style={{
                  width: 150,
                  backgroundColor: '#FFF',
                  justifyContent: 'center',
                  borderTopRightRadius: 5,
                  borderBottomRightRadius: 6,
                }}>
                <Text
                  onPress={() => console.log(experience)}
                  numberOfLines={2}
                  style={[
                    defaultStyle.Title_2,
                    { marginHorizontal: 12, color: COLORS.primarydark, textTransform: 'capitalize' },
                  ]}>
                  {experience.title}
                </Text>

                <Text
                  style={[
                    defaultStyle.Caption,
                    { marginLeft: 12, color: COLORS.altgreen_400, textTransform: 'capitalize' },
                  ]}>
                  {experience.companyName}
                </Text>

                {experience.employmentType && <Text
                  style={[
                    defaultStyle.Caption,
                    { marginLeft: 12, color: '#888', textTransform: 'capitalize', fontSize: 11 },
                  ]}>
                  {this.employmentType(experience.employmentType)}
                </Text>}

                <Text
                  style={[
                    defaultStyle.Body_1,
                    { marginHorizontal: 12, color: COLORS.grey, textTransform: 'capitalize' },
                  ]}>
                  {experience.location && experience.location.city && experience.location.city}, {experience.location && experience.location.state && experience.location.state}, {experience.location && experience.location.country && experience.location.country}
                </Text>

                {experience.website &&
                  <TouchableOpacity
                    onPress={() =>
                      this.openWebsite(experience.website)
                    }
                    activeOpacity={0.5}
                    style={{
                      // backgroundColor: '#154A59',
                      // width: 131,
                      // height: 31,
                      // borderRadius: 17,
                      flexDirection: 'row',
                      justifyContent: 'flex-start',
                      alignItems: 'center',
                      marginLeft: 12,
                    }}>
                    <Icon
                      name="Export"
                      size={10}
                      color="#367681"
                      style={
                        Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 6 }
                      }
                    />
                    <Text
                      style={[
                        defaultStyle.Subtitle_2,
                        { color: COLORS.altgreen_300, marginLeft: 5 },
                      ]}>
                      Website
                    </Text>
                  </TouchableOpacity>}

              </View>
            </View>

            <View
              style={{
                borderColor: '#E8ECEB',
                borderWidth: 0.45,
                width: '105%',
                marginVertical: 10,
                marginLeft: -6,
              }}></View>

            {this.renderDescription(experience.description, experience.id)}

            {/* <Text
              numberOfLines={
                this.state.readMoreExperienceId !== experience.id ? 3 : 20
              }
              style={[
                defaultStyle.Body_2,
                {
                  marginHorizontal: 11,
                  paddingBottom: 36,
                  color: COLORS.grey_500,
                },
              ]}>
              {experience.description}
            </Text> */}

            <TouchableOpacity
              style={{
                height: 30,
                width: 80,
                marginLeft: 6,
                marginTop: -20,
                marginBottom: 16
              }}
              onPress={() =>
                this.setState({
                  readMoreExperienceId:
                    this.state.readMoreExperienceId.length > 0
                      ? ''
                      : experience.id,
                })
              }>
              <Text style={{ color: COLORS.green_500, fontWeight: '700', fontSize: 12 }}>
                {experience.description && experience.description.length > 105 &&
                  this.state.readMoreExperienceId !== experience.id
                  ? 'Read more'
                  : this.state.readMoreExperienceId === experience.id
                    ? 'Read less'
                    : null}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginRight: 10,
              backgroundColor: '#E7F3E3',
              borderBottomRightRadius: 6,
              borderBottomLeftRadius: 6,
              width: 244,
              height: 40,
              justifyContent: 'space-around',
              alignItems: 'center',
              position: 'absolute',
              bottom: -6,
            }}>
            {this.unixTime(experience.createTime)
              .split(' ')
              .splice(1)
              .join(' ') !==
              this.unixTime(experience.endTime)
                .split(' ')
                .splice(1)
                .join(' ') &&
              this.unixTime(experience.endTime).split(' ').splice(1).join(' ') !==
              'Jan 1970' && new Date().getTime() >= experience.endTime ? (
              <Text
                style={[
                  defaultStyle.Body_2_italic,
                  { color: COLORS.altgreen_400 },
                ]}>
                {this.unixTime(experience.createTime)
                  .split(' ')
                  .splice(1)
                  .join(' ')}{' '}
                to{' '}
                {this.unixTime(experience.endTime)
                  .split(' ')
                  .splice(1, 2)
                  .join(' ')}
              </Text>
            ) : this.unixTime(experience.endTime)
              .split(' ')
              .splice(1)
              .join(' ') === 'Jan 1970' || new Date().getTime() < experience.endTime ? (
              <Text
                style={[
                  defaultStyle.Body_2_italic,
                  { color: COLORS.altgreen_400 },
                ]}>
                {this.unixTime(experience.createTime)
                  .split(' ')
                  .splice(1)
                  .join(' ')}{' '}
                - Present
              </Text>
            ) : (
              <Text
                style={[
                  defaultStyle.Body_2_italic,
                  { color: COLORS.altgreen_400 },
                ]}>
                {this.unixTime(experience.createTime)
                  .split(' ')
                  .splice(1)
                  .join(' ')}
              </Text>
            )}
            <Text
              style={[
                defaultStyle.Note,
                {
                  color: COLORS.altgreen_400,
                  borderWidth: 1,
                  borderColor: '#91B3A2',
                  borderRadius: 4,
                  paddingHorizontal: 6,
                  paddingVertical: 2,
                },
              ]}>
              {experience.type}
            </Text>
          </View>
        </View>
      )
  }

  handleScroll = (event) => {
    // console.log(event.nativeEvent.contentOffset.y)
    this.setState({ currentScrollPosition: event.nativeEvent.contentOffset.y });
  };

  hashTagModal = () => {
    return (
      <Modal
        visible={this.state.hashTagModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View
            style={{
              width: '100%',
              height: 500,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.crossButtonContainer}
            onPress={() => this.setState({ hashTagModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={[styles.modalCategoriesContainer, { height: 350 }]}>
            {this.props.userHashTags.body ? (
              <FlatList
                columnWrapperStyle={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 6,
                }}
                numColumns={3}
                showsVerticalScrollIndicator={false}
                data={this.props.userHashTags.body.hashTags}
                keyExtractor={(item) => item}
                renderItem={({ item }) => (
                  <View style={[styles.itemBox, { marginHorizontal: 10 }]}>
                    <Text
                      numberOfLines={1}
                      style={{
                        fontSize: 12,
                        color: '#367681',
                        fontFamily: 'Montserrat-SemiBold',
                      }}>
                      {item}
                    </Text>
                  </View>
                )}
              />
            ) : (
              <></>
            )}
          </View>
        </View>
      </Modal>
    );
  };

  openWebsite = (website) => {
    if (website && website.includes('http')) return Linking.openURL(website);
    else if (website && !website.includes('http'))
      return Linking.openURL('https://' + website);
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          REACT_APP_domainUrl + '/profile/' + this.props.user.body.customUrl,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // console.log('shared with activity type of result.activityType')
        } else {
          // console.log('shared')
        }
      } else if (result.action === Share.dismissedAction) {
        // console.log('dismissed')
      }
    } catch (error) {
      // console.log(error.message)
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ shareModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text
                style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>
                Share
              </Text>
            </View>

            {/* <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15 }] : defaultShape.ActList_Cell_Gylph_Alt} activeOpacity={0.6} onPress={() => { this.setState({ shareModalOpen: false }) }}>
              <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>Repost on WeNaturalists</Text>
              <Icon name='Forward' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

            </TouchableOpacity> */}

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl +
                  '/profile/' +
                  this.props.user.body.customUrl,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text
                style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                Copy link to profile
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({ shareModalOpen: false }, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { borderBottomWidth: 0 },
                  ]
              }
              activeOpacity={0.6}>
              <Text
                style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  contactInfoModal = () => {
    return (
      <Modal
        visible={this.state.contactInfoModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ contactInfoModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          {this.props.user.body ? (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  {
                    justifyContent: 'center',
                    paddingBottom: 10,
                    paddingTop: 0,
                    borderBottomWidth: 0,
                  },
                ]}>
                <Text
                  style={[defaultStyle.Caption, { color: 'rgb(0, 57, 77)', fontFamily: 'Montserrat-Bold' }]}>
                  CONTACT INFO
                </Text>
              </View>

              <View
                style={
                  Platform.OS === 'ios'
                    ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {
                        paddingTop: 25,
                        paddingBottom: 15,
                        borderBottomWidth: 0,
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                      },
                    ]
                    : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        borderBottomWidth: 0,
                        paddingVertical: 12,
                      },
                    ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({ contactInfoModalOpen: false });
                }}>
                <Text style={[defaultStyle.Button_2, { color: COLORS.dark_600 }]}>
                  Phone Number :
                </Text>
                <Text
                  style={[
                    defaultStyle.Caption,
                    { color: COLORS.dark_600, fontSize: 12.5 },
                  ]}>
                  +{this.props.user.body.countryCode}{' '}
                  {this.props.user.body.mobile}
                </Text>
              </View>

              <View
                style={
                  Platform.OS === 'ios'
                    ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {
                        paddingTop: 25,
                        paddingBottom: 15,
                        borderBottomWidth: 0,
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                      },
                    ]
                    : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        borderBottomWidth: 0,
                        paddingVertical: 12,
                      },
                    ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({ contactInfoModalOpen: false });
                }}>
                <Text style={[defaultStyle.Button_2, { color: COLORS.dark_600 }]}>
                  Email :
                </Text>
                <Text
                  style={[
                    defaultStyle.Caption,
                    { color: COLORS.dark_600, fontSize: 12.5 },
                  ]}>
                  {this.props.user.body.email}
                </Text>
              </View>

              <View
                style={
                  Platform.OS === 'ios'
                    ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {
                        paddingTop: 25,
                        paddingBottom: 15,
                        borderBottomWidth: 0,
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                      },
                    ]
                    : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        borderBottomWidth: 0,
                        paddingVertical: 12,
                      },
                    ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({ contactInfoModalOpen: false });
                }}>
                <Text style={[defaultStyle.Button_2, { color: COLORS.dark_600 }]}>
                  Address :
                </Text>
                {this.props.userAddress.body ? (
                  <Text
                    style={[
                      defaultStyle.Caption,
                      { color: COLORS.dark_600, fontSize: 12.5 },
                    ]}>
                    {this.props.userAddress.body.city},{' '}
                    {this.props.userAddress.body.state},{' '}
                    {this.props.userAddress.body.country}
                  </Text>
                ) : (
                  <></>
                )}
              </View>
            </View>
          ) : (
            <></>
          )}
        </View>
      </Modal>
    );
  };

  thumbnail = (item) => {
    if (item.coverImage) {
      return (
        <Image
          source={{ uri: item.coverImage }}
          style={{
            width: 60,
            height: 60,
            borderTopLeftRadius: 5,
            borderBottomLeftRadius: 5,
          }}
        />
      );
    }
    if (
      item.type === 'COMMENT' ||
      item.type === 'POST' ||
      item.type === 'POLL' ||
      item.type === 'LINK'
    ) {
      return (
        <View
          style={{
            width: 60,
            height: 60,
            borderTopLeftRadius: 5,
            borderBottomLeftRadius: 5,
            backgroundColor: COLORS.grey_350,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          {item.type === 'COMMENT' || item.type === 'POST' ? (
            <Icon
              name="Img"
              size={26}
              color={COLORS.grey_400}
              style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
            />
          ) : item.type === 'POLL' ? (
            <Icon
              name="Polls"
              size={26}
              color={COLORS.grey_400}
              style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
            />
          ) : (
            <Icon
              name="Link"
              size={26}
              color={COLORS.grey_400}
              style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
            />
          )}
        </View>
      );
    } else
      return (
        <Image
          source={projectDefault}
          style={{
            width: 60,
            height: 60,
            borderTopLeftRadius: 5,
            borderBottomLeftRadius: 5,
          }}
        />
      );
  };

  handleFollowUnfollow = () => {
    let url;
    if (this.state.following === 0) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        this.props.route.params.userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        this.props.route.params.userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          // console.log(response.status)
          this.getFollowers();
          this.props.personalConnectionInfoRequest({
            userId: this.props.route.params.userId,
          });
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        // console.log(err)
      });
  };

  getFollowers = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        this.props.route.params.userId +
        '/followers' +
        '?page=' +
        0 +
        '&size=' +
        this.props.userConnectionInfo.body.followers,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log('```````````````````` Followers ````````````````````', response.data.body.page.content.filter((item) => item.profileId === this.state.userId).length)
          this.setState({
            following: response.data.body.page.content.filter(
              (item) => item.profileId === this.state.userId,
            ).length,
          });
        }
      })
      .catch((err) => {
        // console.log(err)
      });
  };

  getConnectStatus = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/connectionStatus/' +
        this.props.route.params.userId,
      withCredentials: true,
      headers: { 'Content-Type': 'application/json' },
    })
      .then((response) => {
        if (response && response.status === 200) {
          // console.log(response.data.body.connectStatus)
          this.setState({ isConnected: response.data.body.connectStatus });
        }
      })
      .catch((e) => {
        // console.log(e)
      });
  };

  handleConnectStatusChange = () => {
    let url;
    if (this.state.isConnected === 'NOT_CONNECTED') {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/invite/' +
        this.props.route.params.userId;
    } else if (this.state.isConnected === 'PENDING_CONNECT') {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/removeInvite/' +
        this.props.route.params.userId;
    } else if (this.state.isConnected === 'CONNECTED') {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unconnect/' +
        this.props.route.params.userId;
    } else if (
      this.state.isConnected === 'DEADLOCK_PENDING_STATUS' ||
      this.state.isConnected === 'IGNORE_PENDING_CONNECT'
    ) {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Error sending request to this member',
        duration: Snackbar.LENGTH_LONG,
      });
    }

    if (url && url !== '') {
      axios({
        method: 'post',
        url: url,
        headers: { 'Content-Type': 'application/json' },
        withCredentials: true,
      })
        .then((response) => {
          if (response && response.status === 202) {
            // console.log(response.status)
            this.getConnectStatus();
            this.props.personalConnectionInfoRequest({
              userId: this.props.route.params.userId,
            });
          }
        })
        .catch((err) => {
          // console.log(err)
          if (err.message === 'Request failed with status code 409') {
            Snackbar.show({
              backgroundColor: '#B22222',
              text:
                'You can send connection request after 3 days to this member',
              duration: Snackbar.LENGTH_LONG,
            });
          }
          if (err.message === 'Request failed with status code 400') {
            Snackbar.show({
              backgroundColor: '#B22222',
              text: 'Cannot send request to an organization',
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    }
  };

  getMutualConnects = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        this.state.userId +
        '/mutualConnects/' +
        this.props.route.params.userId +
        '?page=' +
        0 +
        '&size=' +
        100,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log('```````````````````` Mutual ````````````````````', response.data.body.content[0])
          this.setState({
            mutualConnects: response.data.body.content,
            totalMutualConnects: response.data.body.content.length,
          });
        }
      })
      .catch((err) => {
        // console.log(err)
      });
  };

  trimDescription = (item) => {
    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  renderTaggedUsers = (text) => {
    if (!text || text.length === 0) {
      return text;
    }
    // text = text.split("@@@__").join("<a href=\"/profile/");
    text = text.split('@@@__').join('#');
    text = text.split('^^__').join('">');
    text = text.split('&&__').join('">');
    text = text.split('&amp;&amp;__').join('">');
    text = text.split('&amp;&amp;__').join('">');
    text = text.split('@@@^^^').join('');
    // text = text.split("###__").join("<a class=\"hashLinkPost\" style=\"cursor:text\" href=\"javascript:void(0)/");
    text = text.split('###__').join('#');
    text = text.split('###^^^').join('');
    return text;
  };

  extractBio = () => {
    let tempBio = '';
    this.props.user.body &&
      this.props.user.body.bio.split(' ').map((item) => {
        tempBio = tempBio + ' ' + this.renderTaggedUsers(item)
      })
    return tempBio
  }

  navigation = (value, params) => {
    this.props.navigation.navigate(value, params)
  }

  getUserDetailsByCustomUrl = (customurl) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get_by_custom_url?customUrl=' +
        customurl +
        '&otherUserId=' +
        this.state.userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          response.data.body &&
            response.data.body.type === 'INDIVIDUAL' &&
            response.data.body.userId === this.state.userId
            ? this.props.navigation.navigate('ProfileStack')
            : response.data.body.type === 'INDIVIDUAL' &&
              response.data.body.userId !== this.state.userId
              ? this.props.navigation.navigate('ProfileStack', {
                screen: 'OtherProfileScreen',
                params: { userId: response.data.body.userId },
              })
              : response.data.body.type === 'COMPANY'
                ? this.props.navigation.navigate('ProfileStack', {
                  screen: 'CompanyProfileScreen',
                  params: { userId: response.data.body.userId },
                })
                : this.props.navigation.navigate('CircleProfileStack', {
                  screen: 'CircleProfile',
                  params: { slug: customurl },
                });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderBio = (item) => {
    return (
      <Autolink
        text={item && this.trimDescription(tagDescription(item))}
        email
        hashtag="instagram"
        mention="twitter"
        phone="sms"
        numberOfLines={5}
        matchers={[
          {
            pattern: /@\[([^[]*)]\(([^(^)]*)\)/g,
            style: { color: COLORS.mention_color, fontWeight: 'bold' },
            getLinkText: (replacerArgs) => `${replacerArgs[2]}`,
            onPress: (match) => {
              this.getUserDetailsByCustomUrl(match.getReplacerArgs()[1]);
            },
          },
          {
            pattern: /#\[([^[]*)]\(([^(^)]*)\)/g,
            style: { color: COLORS.mention_color, fontWeight: 'bold' },
            getLinkText: (replacerArgs) => `#${replacerArgs[2]}`,
            onPress: (match) => {
              this.props.navigation.navigate('HashTagDetail', {
                slug: match.getReplacerArgs()[1],
              });
            },
          },
        ]}
        style={[
          typography.Body_1,
          {
            color: COLORS.dark_500,
            marginLeft: 6,
            marginRight: 17,
            marginVertical: 8
          }
        ]}
        url
      />
    )
  }

  getConnectionInfo = (userId) => {
    axios({
      method: "get",
      url:
        REACT_APP_userServiceURL +
        "/profile/connection/info?userId=" +
        userId,
      headers: { "Content-Type": "application/json" },
      withCredentials: true
    })
      .then((response) => {
        if (response && response.data && response.data.message === "Success!" && response.data.body) {
          this.setState({ followingCount: response.data.body.following })
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }

  render() {
    // this.props.userConnectionInfo.body ?
    //   console.log('################### this.props.userConnectionInfo.body ######################', this.props.userConnectionInfo.body)
    //   : null

    return (
      <SafeAreaView style={{}}>
        {/* {this.state.currentScrollPosition > 344 ? (
          <View
            style={{
              backgroundColor: COLORS.white,
              position: 'absolute',
              top: 0,
              zIndex: 2,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                backgroundColor: COLORS.white,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  padding: 10,
                }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => this.props.navigation.goBack()}
                    style={{
                      backgroundColor: COLORS.white,
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 42,
                      width: 42,
                      borderRadius: 21,
                      marginTop: 6,
                    }}>
                    <Icon
                      name="Arrow-Left"
                      size={16}
                      color="#00394D"
                      style={{ marginTop: 5 }}
                    />
                  </TouchableOpacity>

                  {this.props.user.body &&
                    this.props.user.body.originalProfileImage ? (
                    <Image
                      source={{ uri: this.props.user.body.originalProfileImage }}
                      style={{ width: 30, height: 30, borderRadius: 15 }}
                    />
                  ) : null}

                  <View style={{ alignSelf: 'center', marginLeft: 8 }}>
                    {this.props.user.body !== undefined ? (
                      <Text
                        style={[
                          defaultStyle.Title_1,
                          { color: COLORS.dark_800, fontSize: 14 },
                        ]}>
                        {this.props.user.body.userName}{' '}
                      </Text>
                    ) : (
                      <></>
                    )}

                    {this.props.user.body ? (
                      <Text
                        style={[
                          defaultStyle.Subtitle_1,
                          {
                            color: COLORS.altgreen_400,
                            fontSize: 12,
                            marginTop: -6,
                          },
                        ]}>
                        {this.props.user.body.persona}
                      </Text>
                    ) : (
                      <></>
                    )}
                  </View>
                </View>

                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => this.setState({ optionsModalOpen: true })}
                  style={{
                    backgroundColor: COLORS.white,
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 42,
                    width: 42,
                    borderRadius: 21,
                    marginTop: 6,
                  }}>
                  <Icon
                    name="Kebab"
                    size={16}
                    color="#00394D"
                    style={{ marginTop: 5 }}
                  />
                </TouchableOpacity>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                justifyContent: 'space-evenly',
                width: 300,
              }}>
              <TouchableOpacity activeOpacity={0.8} style={{ width: 100 }}>
                <Text
                  style={[
                    defaultStyle.Button_2,
                    {
                      color: COLORS.dark_800,
                      textAlign: 'center',
                      marginBottom: 6,
                      paddingTop: 8,
                    },
                  ]}>
                  ABOUT
                </Text>
                <View
                  style={{
                    width: 100,
                    height: 5,
                    backgroundColor: COLORS.dark_800,
                    borderTopLeftRadius: 4,
                    borderTopRightRadius: 4,
                  }}></View>
              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={0.8}
                style={{ paddingTop: 8, width: 100 }}
                onPress={() =>
                  this.props.navigation.navigate('ActivitiesProfileScreen', {
                    id: this.state.paramsId,
                    state: this.props.userAddress.body
                      ? this.props.userAddress.body.state
                      : null,
                  })
                }>
                <Text
                  style={[
                    defaultStyle.Button_Lead,
                    {
                      color: COLORS.altgreen_400,
                      marginBottom: 6,
                      textAlign: 'center',
                    },
                  ]}>
                  ACTIVITY
                </Text>
              </TouchableOpacity>

              {this.state.hashtagList.length ?
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={{ paddingTop: 8, width: 100, marginLeft: 8 }}
                  onPress={() =>
                    this.props.navigation.navigate('HashTags', {
                      id: this.state.paramsId,
                      state: this.props.userAddress.body
                        ? this.props.userAddress.body.state
                        : null,
                    })
                  }>
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {
                        color: COLORS.altgreen_400,
                        marginBottom: 6,
                        textAlign: 'center',
                      },
                    ]}>
                    HASHTAG
                  </Text>
                </TouchableOpacity> : <></>}

              {this.state.awardList.length ?
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={{ paddingTop: 8, width: 100 }}
                  onPress={() =>
                    this.props.navigation.navigate('Awards', {
                      id: this.state.paramsId,
                      state: this.props.userAddress.body
                        ? this.props.userAddress.body.state
                        : null,
                    })
                  }>
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {
                        color: COLORS.altgreen_400,
                        marginBottom: 6,
                        textAlign: 'center',
                      },
                    ]}>
                    AWARDS
                  </Text>
                </TouchableOpacity> : <></>}


            </View>
          </View>
        ) : (
          <></>
        )} */}

        <ScrollView
          onScroll={this.handleScroll}
          // stickyHeaderIndices={[0]}
          scrollEventThrottle={0}
          // ref={ref => this.scrollView = ref}
          // onContentSizeChange={() => {
          //   this.scrollView.scrollToEnd({ animated: true });
          // }}
          contentContainerStyle={{ paddingBottom: 20 }}
          keyboardDismissMode="on-drag"
          keyboardShouldPersistTaps="handled"
          showsVerticalScrollIndicator={false}
          style={styles.container}>

          <View
            style={styles.imageContainer}
          // source={{ uri: this.props.user.body.originalCoverImage }}
          >
            <View
              style={{
                width: '100%',
                height: '30%',
                position: 'absolute',
                bottom: 0,
              }}>
              <LinearGradient
                colors={['#FFFFFF00', '#FFFFFFBA', '#FFFFFFE3', '#FFFFFF']}
                style={styles.linearGradient}></LinearGradient>
            </View>

            <View
              style={{
                flexDirection: 'row',
                width: '95%',
                marginHorizontal: '5%',
                justifyContent: 'space-between',
              }}>
              <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => this.props.navigation.goBack()}
                style={{
                  backgroundColor: '#CFE7C733',
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 42,
                  width: 42,
                  borderRadius: 21,
                  marginLeft: -10,
                  marginTop: 12,
                }}>
                <Icon
                  name="Arrow-Left"
                  size={16}
                  color="#00394D"
                  style={{ marginTop: 5 }}
                />
              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => this.setState({ optionsModalOpen: true })}
                style={{
                  backgroundColor: '#CFE7C733',
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 42,
                  width: 42,
                  borderRadius: 21,
                  marginRight: 10,
                  marginTop: 12,
                }}>
                <Icon
                  name="Kebab"
                  size={16}
                  color="#00394D"
                  style={{ marginTop: 5 }}
                />
              </TouchableOpacity>
            </View>
          </View>


          {this.shareModal()}
          {this.optionsModal()}
          {this.requestEndorsementModal()}
          {this.modal()}
          {this.specializationModal()}
          {this.skillsModal()}
          {this.interestsModal()}
          {this.hashTagModal()}
          {this.contactInfoModal()}

          <View style={styles.mainSection}>
            <View style={[styles.imageUsername, { backgroundColor: 'white' }]}>
              <View style={styles.header}>
                {/* <ProfileSvg /> */}
                <ProfileSvg
                  profileImg={
                    this.props.user.body &&
                      this.props.user.body.originalProfileImage
                      ? this.props.user.body.originalProfileImage
                      : null
                  }
                />

                <View style={styles.userProfile}>
                  {this.props.user.body !== undefined ? (
                    <Text
                      style={[defaultStyle.Title_1, { color: COLORS.dark_800 }]}>
                      {this.props.user.body.userName}{' '}
                    </Text>
                  ) : (
                    <></>
                  )}
                  <View>
                    {this.props.user.body ? (
                      <Text
                        style={[
                          defaultStyle.Subtitle_1,
                          { color: COLORS.altgreen_400 },
                        ]}>
                        {this.props.user.body.persona}
                      </Text>
                    ) : (
                      <></>
                    )}
                  </View>

                  <View
                    style={{
                      borderColor: '#D9E1E4',
                      borderWidth: 0.45,
                      width: '104%',
                      marginTop: 8,
                    }}></View>

                  {/* ******connection info start******* */}

                  <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate(
                          'ConnectionDetailsScreen',
                          {
                            id: this.state.userId,
                            totalMutualConnects: this.props.route.params
                              ? this.state.mutualConnects.length
                              : null,
                            currentTab: 'connects',
                          },
                        )
                      }
                      activeOpacity={0.7}
                      style={{ marginRight: 12, marginTop: 10 }}>
                      <Text
                        style={[
                          defaultStyle.Body_1_bold,
                          { color: COLORS.dark_800 },
                        ]}>
                        {this.props.userConnectionInfo.body
                          ? this.props.userConnectionInfo.body.connections
                          : '0'}
                      </Text>
                      <Text
                        style={[
                          defaultStyle.Body_1,
                          { color: COLORS.altgreen_300 },
                        ]}>
                        {this.props.userConnectionInfo.body &&
                          this.props.userConnectionInfo.body.connections <= 1
                          ? 'Connection'
                          : 'Connections'}
                      </Text>
                    </TouchableOpacity>

                    {this.props.route.params ? (
                      <View style={{ marginRight: 12, marginTop: 10 }}>
                        <Text
                          style={[
                            defaultStyle.Body_1_bold,
                            { color: COLORS.dark_800 },
                          ]}>
                          {this.state.mutualConnects
                            ? this.state.mutualConnects.length
                            : '0'}
                        </Text>
                        <Text
                          style={[
                            defaultStyle.Body_1,
                            { color: COLORS.altgreen_300 },
                          ]}>
                          Mutual Connects
                        </Text>
                      </View>
                    ) : (
                      <View style={{ marginRight: 12, marginTop: 10 }}>
                        <Text
                          style={[
                            defaultStyle.Body_1_bold,
                            { color: COLORS.dark_800 },
                          ]}>
                          {this.state.followingCount}
                        </Text>
                        <Text
                          style={[
                            defaultStyle.Body_1,
                            { color: COLORS.altgreen_300 },
                          ]}>
                          Following
                        </Text>
                      </View>
                    )}

                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate(
                          'ConnectionDetailsScreen',
                          {
                            id: this.state.userId,
                            totalMutualConnects: this.props.route.params
                              ? this.state.mutualConnects.length
                              : null,
                            currentTab: 'followers',
                          },
                        )
                      }
                      activeOpacity={0.7}
                      style={{ marginRight: 12, marginTop: 10 }}>
                      <Text
                        style={[
                          defaultStyle.Body_1_bold,
                          { color: COLORS.dark_800 },
                        ]}>
                        {this.props.userConnectionInfo.body
                          ? this.props.userConnectionInfo.body.followers
                          : '0'}
                      </Text>
                      <Text
                        style={[
                          defaultStyle.Body_1,
                          { color: COLORS.altgreen_300 },
                        ]}>
                        {this.props.userConnectionInfo.body
                          && this.props.userConnectionInfo.body.followers > 1 ? 'Followers' : 'Follower'}
                      </Text>
                    </TouchableOpacity>
                  </View>

                  {/* ******connection info end******* */}
                </View>
              </View>
            </View>

            {this.props.route.params ? (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                  marginBottom: 30,
                }}>
                {/* <TouchableOpacity onPress={() => this.setState({ connected: !this.state.connected })} */}
                {this.state.isConnected !== 'DEADLOCK_PENDING_STATUS' ||
                  this.state.isConnected !== 'IGNORE_PENDING_CONNECT' ? (
                  <TouchableOpacity
                    onPress={() => this.handleConnectStatusChange()}
                    style={{
                      flexDirection: 'row',
                      paddingHorizontal: 14,
                      height: 38,
                      borderRadius: 4,
                      backgroundColor: COLORS.green_500,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    activeOpacity={0.8}>
                    {this.state.isConnected === 'CONNECTED' ||
                      this.state.isConnected === 'PENDING_CONNECT' ? (
                      <Icon
                        name="FollowTick"
                        size={14}
                        color={COLORS.white}
                        style={{
                          marginTop: Platform.OS === 'android' ? 8 : 0,
                          marginRight: 6,
                        }}
                      />
                    ) : (
                      <></>
                    )}
                    <Text
                      style={{
                        fontSize: 13,
                        color: '#FFFFFF',
                        fontFamily: 'Montserrat-Bold',
                      }}>
                      {this.state.isConnected === 'NOT_CONNECTED'
                        ? 'CONNECT'
                        : this.state.isConnected === 'CONNECTED'
                          ? 'CONNECTED'
                          : this.state.isConnected === 'PENDING_CONNECT'
                            ? 'REQUEST SENT'
                            : 'CONNECT'}
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <></>
                )}

                <TouchableOpacity
                  onPress={() => this.handleFollowUnfollow()}
                  activeOpacity={0.5}
                  style={{ marginLeft: -26 }}>
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {
                        color: this.state.following
                          ? COLORS.grey_350
                          : COLORS.green_500,
                      },
                    ]}>
                    {this.state.following === 0 ? 'Follow' : 'Following'}
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={{
                    width: 31,
                    height: 31,
                    borderRadius: 15.5,
                    backgroundColor: COLORS.altgreen_100,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  activeOpacity={0.6}>
                  <Icon
                    name="Messeges_F"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                  />
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.setState({ shareModalOpen: true })}
                  style={{
                    marginLeft: -30,
                    width: 31,
                    height: 31,
                    borderRadius: 15.5,
                    backgroundColor: COLORS.altgreen_100,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  activeOpacity={0.6}>
                  <Icon
                    name="Share"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                  />
                </TouchableOpacity>
              </View>
            ) : (
              <></>
            )}

            {/* -------- About Starts-------------- */}

            <View style={{}}>
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  justifyContent: 'space-evenly',
                  width: 300,
                }}>
                <TouchableOpacity activeOpacity={0.8} style={{ width: 100 }}>
                  <Text
                    style={[
                      defaultStyle.Button_2,
                      {
                        color: COLORS.dark_800,
                        textAlign: 'center',
                        marginBottom: 6,
                        paddingTop: 8,
                      },
                    ]}>
                    ABOUT
                  </Text>
                  <View
                    style={{
                      width: 100,
                      height: 5,
                      backgroundColor: COLORS.dark_800,
                      borderTopLeftRadius: 4,
                      borderTopRightRadius: 4,
                    }}></View>
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={0.8}
                  style={{ paddingTop: 8, width: 100 }}
                  onPress={() =>
                    this.props.navigation.navigate('ActivitiesProfileScreen', {
                      id: this.state.paramsId,
                      state: this.props.userAddress.body
                        ? this.props.userAddress.body.state
                        : null,
                    })
                  }>
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {
                        color: COLORS.altgreen_400,
                        marginBottom: 6,
                        textAlign: 'center',
                      },
                    ]}>
                    ACTIVITY
                  </Text>
                </TouchableOpacity>

                {this.state.hashtagList.length ?
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={{ paddingTop: 8, width: 100, marginLeft: 8 }}
                    onPress={() =>
                      this.props.navigation.navigate('HashTags', {
                        id: this.state.paramsId,
                        state: this.props.userAddress.body
                          ? this.props.userAddress.body.state
                          : null,
                      })
                    }>
                    <Text
                      style={[
                        defaultStyle.Button_Lead,
                        {
                          color: COLORS.altgreen_400,
                          marginBottom: 6,
                          textAlign: 'center',
                        },
                      ]}>
                      HASHTAG
                    </Text>
                  </TouchableOpacity> : <></>}

                {this.state.awardList.length ?
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={{ paddingTop: 8, width: 100 }}
                    onPress={() =>
                      this.props.navigation.navigate('Awards', {
                        id: this.state.paramsId,
                        state: this.props.userAddress.body
                          ? this.props.userAddress.body.state
                          : null,
                      })
                    }>
                    <Text
                      style={[
                        defaultStyle.Button_Lead,
                        {
                          color: COLORS.altgreen_400,
                          marginBottom: 6,
                          textAlign: 'center',
                        },
                      ]}>
                      AWARDS
                    </Text>
                  </TouchableOpacity> : <></>}


              </View>

              <View
                style={{
                  backgroundColor: '#154A59',
                  paddingHorizontal: 10,
                  paddingVertical: 28,
                }}>
                {this.props.userSkillsSpecialization.body &&
                  this.props.userSkillsSpecialization.body.skillsAndSpecialities
                    .length ? (
                  <FlatList
                    style={{ paddingBottom: 20 }}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    alwaysBounceHorizontal={false}
                    keyExtractor={(item) => item}
                    data={this.props.userSkillsSpecialization.body.skillsAndSpecialities.slice(
                      0,
                      10,
                    )}
                    renderItem={({ item, index }) => {

                      return (
                        <View style={{ flexDirection: 'row' }}>
                          <Text
                            onPress={() => console.log(this.props.userSkillsSpecialization.body.skillsAndSpecialities.length)}
                            style={[
                              defaultStyle.Body_1,
                              {
                                color: COLORS.altgreen_50,
                                paddingHorizontal: 10,
                              },
                            ]}>
                            {item}
                          </Text>

                          <View
                            style={{
                              height: '100%',
                              width: this.props.userSkillsSpecialization.body.skillsAndSpecialities.length - 1 === index ? 0 : 2,
                              backgroundColor: '#BFC52E',
                            }}>
                          </View>
                        </View>
                      );

                    }}
                  />
                ) : (
                  <></>
                )}

                {this.props.user.body ? (
                  <View>
                    <Text
                      // onPress={() =>
                      //   this.setState({ readMore: !this.state.readMore })
                      // }
                      numberOfLines={this.state.readMore ? 500 : 5}
                      style={[
                        defaultStyle.Body_1,
                        {
                          color: COLORS.altgreen_300,
                          marginHorizontal: 10,
                          lineHeight: 16,
                        },
                      ]}>
                      {this.props.user.body && this.props.user.body.bio && this.renderBio(this.props.user.body.bio)}
                    </Text>
                    {this.props.user.body &&
                      this.props.user.body.bio &&
                      this.props.user.body.bio.split(' ').length > 50 ? (
                      <TouchableOpacity
                        style={{ height: 30, width: 80, marginLeft: 10 }}
                        onPress={() =>
                          this.setState({ readMore: !this.state.readMore })
                        }>
                        <Text
                          style={{ color: COLORS.green_500, fontWeight: '700' }}>
                          {this.state.readMore ? 'Read less' : 'Read more'}
                        </Text>
                      </TouchableOpacity>
                    ) : null}
                  </View>
                ) : (
                  <ActivityIndicator size="small" color="#fff" />
                )}
              </View>

              <View
                style={{
                  backgroundColor: '#0F4251',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingVertical: 18,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginHorizontal: 20,
                  }}>
                  <Icon
                    name="Location"
                    size={12}
                    color="#367681"
                    style={
                      Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 6 }
                    }
                  />
                  {this.props.user.body ? (
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.Subtitle_2,
                        { color: COLORS.grey_400, marginLeft: 4, width: 130 },
                      ]}>
                      {this.props.user.body.country}
                    </Text>
                  ) : (
                    <ActivityIndicator size="small" color="#fff" />
                  )}
                </View>
                {this.props.user.body && this.props.user.body.website ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.openWebsite(this.props.user.body.website)
                    }
                    activeOpacity={0.5}
                    style={{
                      backgroundColor: '#154A59',
                      width: 131,
                      height: 31,
                      borderRadius: 17,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginHorizontal: 20,
                    }}>
                    <Icon
                      name="Export"
                      size={10}
                      color="#367681"
                      style={
                        Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 6 }
                      }
                    />
                    <Text
                      style={[
                        defaultStyle.Subtitle_2,
                        { color: COLORS.altgreen_300, marginLeft: 5 },
                      ]}>
                      Visit Website
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <></>
                )}
              </View>

              {/* -------- Recent Activity -------------- */}
              {this.props.userRecentActivity.body &&
                this.props.userRecentActivity.body.content.length ? (
                <View
                  style={{
                    backgroundColor: '#D9E1E4',
                    paddingVertical: 20,
                    paddingLeft: 16,
                  }}>
                  <Text
                    style={[
                      defaultStyle.OVERLINE,
                      { color: 'rgb(21, 74, 89)', fontSize: 14, fontFamily: 'Montserrat-Bold' },
                    ]}>
                    Recent Activity
                  </Text>

                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('ViewAllActivity', { userId: this.state.userId })}
                    style={{ borderRadius: 17, borderColor: '#00394d', borderWidth: 1, justifyContent: 'center', alignItems: 'center', position: 'absolute', right: 20, top: 16, alignSelf: 'center', paddingHorizontal: 10, height: 32 }}
                    activeOpacity={0.6} >
                    <Icon
                      name="Dashboard"
                      size={14}
                      color="#00394d"
                      style={{ marginTop: Platform.OS === 'android' && 6 }}
                    />
                  </TouchableOpacity>

                  {this.props.userRecentActivity.body ? (
                    <FlatList
                      style={{ paddingVertical: 20 }}
                      showsHorizontalScrollIndicator={false}
                      horizontal={true}
                      alwaysBounceHorizontal={false}
                      keyExtractor={(item) => item.activityId}
                      data={this.props.userRecentActivity.body.content}
                      renderItem={(item) => <RecentActivityItem data={item.item} navigation={this.navigation} userId={this.state.userId} />}
                    />
                  ) : (
                    <ActivityIndicator size="small" color="#fff" />
                  )}
                </View>
              ) : (
                <></>
              )}

              {/* -------- Recent Activity -------------- */}

              {/* -------- Experience Starts-------------- */}

              <View
                style={{
                  paddingTop: 20,
                  paddingBottom: 5,
                  paddingLeft: 16,
                  backgroundColor: '#F7F7F5',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingRight: 18,
                  }}>
                  <Text style={[defaultStyle.H6, { color: COLORS.dark_800 }]}>
                    EXPERIENCE
                  </Text>
                  <TouchableOpacity
                    onPress={() => this.setState({ modalOpen: true })}
                    activeOpacity={0.5}
                    style={{
                      flexDirection: 'row',
                      width: 'auto',
                      height: 31,
                      borderRadius: 20,
                      backgroundColor: '#D9E1E4',
                      justifyContent: 'center',
                      alignItems: 'center',
                      paddingHorizontal: 10,
                    }}>
                    {this.state.selectedExperienceType === 'ASSIGNMENTEVENTTRAINING' ? (
                      <Text
                        style={[
                          defaultStyle.Caption,
                          { color: COLORS.dark_600 },
                        ]}>
                        ALL
                      </Text>
                    ) : (
                      <Text
                        style={[
                          defaultStyle.Caption,
                          { color: COLORS.dark_600 },
                        ]}>
                        {this.state.selectedExperienceType}
                      </Text>
                    )}
                    <Icon
                      name="Arrow2_Down"
                      size={14}
                      color="#367681"
                      style={
                        Platform.OS === 'android' ? { marginTop: 8 } : null
                      }
                    />
                  </TouchableOpacity>
                </View>

                {this.state.experienceList.length ? (
                  <FlatList
                    style={{ paddingVertical: 20, marginRight: 10 }}
                    horizontal={true}
                    // extraData={this.state.refresh}
                    showsHorizontalScrollIndicator={false}
                    alwaysBounceHorizontal={false}
                    keyExtractor={(item) => this.state.selectedExperienceType === 'JOB' ? item.id : item.project.id}
                    data={this.state.experienceList.slice(0, 6)}
                    renderItem={({ item, index }) =>
                      this.state.selectedExperienceType === 'JOB' ? this.renderJobItem(item, index) : this.renderItem(item, index)
                    }
                  />
                ) : (
                  <Text
                    onPress={() => {
                      this.props.navigation.navigate('ProfileEditStack', { screen: 'ExperienceEdit' })
                    }}
                    style={{ textAlign: 'center', color: '#97a600', marginLeft: 10, marginRight: 16, marginVertical: 30 }}>Update your experience. It will help you explore relevant opportunities, enable discovery by other users, and to know you better.</Text>
                )}
              </View>

              {/* -------- Experience Ends-------------- */}

              {/* -------- Education Starts ------------ */}

              {this.props.userEducation.body &&
                this.props.userEducation.body.list.length ? (
                <View style={{ paddingVertical: 20, backgroundColor: '#F7F7F5' }}>
                  <View
                    style={{
                      backgroundColor: '#FFFFFF',
                      marginHorizontal: 10,
                      alignSelf: 'center',
                      width: '90%',
                      borderRadius: 6,
                      shadowColor: '#000',
                      shadowOffset: { width: 0, height: 2 },
                      shadowOpacity: 0.25,
                      shadowRadius: 3.84,
                      elevation: 5,
                    }}>
                    <Text
                      style={[
                        defaultStyle.H6,
                        { color: COLORS.dark_800, marginTop: 14, marginLeft: 14 },
                      ]}>
                      EDUCATION
                    </Text>

                    {this.props.userEducation.body ? (
                      <FlatList
                        style={{ paddingVertical: 20 }}
                        // horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        alwaysBounceHorizontal={false}
                        keyExtractor={(item) => item.id}
                        data={this.props.userEducation.body.list.slice(0, 5)}
                        renderItem={({ item, index }) => {
                          if (
                            index + 1 === 5 ||
                            index + 1 ===
                            this.props.userEducation.body.list.length
                          )
                            return (
                              <View>
                                <View
                                  style={{
                                    width: '100%',
                                    flexDirection: 'row',
                                    marginRight: 10,
                                    borderRadius: 5,
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                  }}>
                                  <Image
                                    source={InstitutionLogo}
                                    style={{
                                      width: 36,
                                      height: 36,
                                      borderRadius: 18,
                                      marginLeft: 30,
                                      marginRight: 10,
                                    }}
                                  />
                                  <View
                                    style={{
                                      height: 60,
                                      justifyContent: 'center',
                                      borderTopRightRadius: 5,
                                      borderBottomRightRadius: 6,
                                    }}>
                                    <Text
                                      numberOfLines={1}
                                      style={[
                                        defaultStyle.Body_1,
                                        {
                                          marginHorizontal: 12,
                                          color: COLORS.dark_900,
                                          maxWidth: 200
                                        },
                                      ]}>
                                      {item.institution}
                                    </Text>
                                    <Text
                                      style={[
                                        defaultStyle.Caption,
                                        {
                                          marginLeft: 12,
                                          color: COLORS.altgreen_400,
                                        },
                                      ]}>
                                      {item.specialisations}
                                    </Text>
                                    {this.unixTime(item.endTime)
                                      .split(' ')
                                      .splice(1, 2)
                                      .join(' ') !== 'Jan 1970' ? (
                                      <Text
                                        style={[
                                          defaultStyle.Note,
                                          {
                                            marginHorizontal: 12,
                                            color: COLORS.grey_400,
                                          },
                                        ]}>
                                        {this.unixTime(item.startTime)
                                          .split(' ')
                                          .splice(1)
                                          .join(' ')}{' '}
                                        to{' '}
                                        {this.unixTime(item.endTime)
                                          .split(' ')
                                          .splice(1, 2)
                                          .join(' ')}
                                      </Text>
                                    ) : (
                                      <Text
                                        style={[
                                          defaultStyle.Note,
                                          {
                                            marginHorizontal: 12,
                                            color: COLORS.grey_400,
                                          },
                                        ]}>
                                        {this.unixTime(item.startTime)
                                          .split(' ')
                                          .splice(1)
                                          .join(' ')}{' '}
                                        to Present
                                      </Text>
                                    )}
                                  </View>
                                </View>
                              </View>
                            );
                          else
                            return (
                              <View>
                                <View
                                  style={{
                                    width: '100%',
                                    flexDirection: 'row',
                                    marginRight: 10,
                                    borderRadius: 5,
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                  }}>
                                  <Image
                                    source={InstitutionLogo}
                                    style={{
                                      width: 36,
                                      height: 36,
                                      borderRadius: 18,
                                      marginLeft: 30,
                                      marginRight: 10,
                                    }}
                                  />
                                  <View
                                    style={{
                                      height: 60,
                                      justifyContent: 'center',
                                      borderTopRightRadius: 5,
                                      borderBottomRightRadius: 6,
                                    }}>
                                    <Text
                                      style={{
                                        marginHorizontal: 12,
                                        color: '#00394D',
                                        fontSize: 12,
                                        fontFamily: 'Montserrat-Bold',
                                      }}>
                                      {item.institution}
                                    </Text>
                                    <Text
                                      style={{
                                        marginLeft: 12,
                                        color: '#91B3A2',
                                        fontSize: 12,
                                        fontFamily: 'Montserrat-SemiBold',
                                      }}>
                                      {item.specialisations}
                                    </Text>
                                    {this.unixTime(item.endTime)
                                      .split(' ')
                                      .splice(1, 2)
                                      .join(' ') !== 'Jan 1970' ? (
                                      <Text
                                        style={[
                                          defaultStyle.Note,
                                          {
                                            marginHorizontal: 12,
                                            color: COLORS.grey_400,
                                          },
                                        ]}>
                                        {this.unixTime(item.startTime)
                                          .split(' ')
                                          .splice(1)
                                          .join(' ')}{' '}
                                        to{' '}
                                        {this.unixTime(item.endTime)
                                          .split(' ')
                                          .splice(1, 2)
                                          .join(' ')}
                                      </Text>
                                    ) : (
                                      <Text
                                        style={[
                                          defaultStyle.Note,
                                          {
                                            marginHorizontal: 12,
                                            color: COLORS.grey_400,
                                          },
                                        ]}>
                                        {this.unixTime(item.startTime)
                                          .split(' ')
                                          .splice(1)
                                          .join(' ')}{' '}
                                        to Present
                                      </Text>
                                    )}
                                  </View>
                                </View>
                                <View
                                  style={{
                                    borderColor: '#E8ECEB',
                                    borderWidth: 0.45,
                                    width: '92%',
                                    marginVertical: 10,
                                    alignSelf: 'center',
                                  }}></View>
                              </View>
                            );
                        }}
                      />
                    ) : (
                      <ActivityIndicator size="small" color="#00394D" />
                    )}

                    {this.props.userEducation.body && this.props.userEducation.body.list &&
                      this.props.userEducation.body.list.length ?
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate(
                            'SeeallEducationScreen',
                            { id: this.state.paramsId },
                          )
                        }
                        activeOpacity={0.6}
                        style={{
                          flexDirection: 'row',
                          marginRight: 10,
                          backgroundColor: '#F1F5EE',
                          borderBottomRightRadius: 8,
                          borderBottomLeftRadius: 8,
                          width: '100%',
                          height: 40,
                          justifyContent: 'space-around',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            color: '#698F8A',
                            fontSize: 12,
                            fontFamily: 'Montserrat-Medium',
                            paddingHorizontal: 6,
                            paddingVertical: 2,
                          }}>
                          See all
                        </Text>
                      </TouchableOpacity> : <></>}
                  </View>
                </View>
              ) : (
                <></>
              )}

              {/* ---------- Education Ends ----------- */}

              {/* --------- Circle Starts ------------- */}

              {this.props.userCircle.body &&
                this.props.userCircle.body.content.length ? (
                <View
                  style={{
                    backgroundColor: '#FFFFFF',
                    paddingVertical: 20,
                    paddingLeft: 16,
                  }}>
                  <Text
                    style={{
                      color: '#154A59',
                      fontSize: 14,
                      fontFamily: 'Montserrat-Bold',
                      marginLeft: 8,
                    }}>
                    CIRCLES
                  </Text>

                  {this.props.userCircle.body ? (
                    <FlatList
                      style={{ paddingVertical: 20 }}
                      showsHorizontalScrollIndicator={false}
                      horizontal={true}
                      alwaysBounceHorizontal={false}
                      keyExtractor={(item) => item.id}
                      data={this.props.userCircle.body.content.slice(0, 6)}
                      renderItem={({ item, index }) => {
                        if (index + 1 === 6)
                          return (
                            <TouchableOpacity
                              onPress={() =>
                                this.props.navigation.navigate(
                                  'SeeallCircleScreen',
                                  { id: this.state.paramsId },
                                )
                              }
                              activeOpacity={0.5}
                              style={{
                                marginHorizontal: 16,
                                alignSelf: 'center',
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: 26,
                                width: 70,
                                borderWidth: 1,
                                borderColor: '#698F8A',
                                borderRadius: 13,
                                paddingHorizontal: 6,
                                paddingVertical: 2,
                              }}>
                              <Text
                                style={{
                                  color: '#698F8A',
                                  fontSize: 11,
                                  fontFamily: 'Montserrat-Medium',
                                }}>
                                See All
                              </Text>
                            </TouchableOpacity>
                          );
                        else
                          return (
                            <TouchableOpacity
                              activeOpacity={0.9}
                              onPress={() =>
                                this.props.navigation.navigate(
                                  'CircleProfileStack',
                                  {
                                    screen: 'CircleProfile',
                                    params: { slug: item.slug },
                                  },
                                )
                              }
                              style={{ marginRight: 16 }}>
                              {item.coverImage ? (
                                <Image
                                  source={{ uri: item.coverImage }}
                                  style={{
                                    width: 120,
                                    height: 52,
                                    borderRadius: 6,
                                  }}
                                />
                              ) : (
                                <Image
                                  source={{
                                    uri:
                                      'https://cdn.dscovr.com/images/prof-banner.webp',
                                  }}
                                  style={{
                                    width: 120,
                                    height: 52,
                                    borderRadius: 6,
                                  }}
                                />
                              )}

                              {item.profileImage ? (
                                <Image
                                  source={{ uri: item.profileImage }}
                                  style={{
                                    position: 'absolute',
                                    top: 26,
                                    zIndex: 2,
                                    alignSelf: 'center',
                                    width: 42,
                                    height: 42,
                                    borderRadius: 21,
                                  }}
                                />
                              ) : (
                                <Image
                                  source={circleDefault}
                                  style={{
                                    position: 'absolute',
                                    top: 26,
                                    zIndex: 2,
                                    alignSelf: 'center',
                                    width: 42,
                                    height: 42,
                                    borderRadius: 21,
                                  }}
                                />
                              )}

                              <View
                                style={{
                                  marginTop: 16,
                                  paddingLeft: 2,
                                  width: 120,
                                  height: 46,
                                  backgroundColor: '#FFF',
                                  justifyContent: 'center',
                                  borderRadius: 5,
                                }}>
                                <Text
                                  style={{
                                    marginHorizontal: 0,
                                    color: '#00394D',
                                    fontSize: 12,
                                    fontFamily: 'Montserrat-Bold',
                                    textAlign: 'center',
                                  }}>
                                  {item.title}
                                </Text>
                                <Text
                                  style={{
                                    marginHorizontal: 0,
                                    color: '#607580',
                                    fontSize: 10,
                                    fontFamily: 'Montserrat-Medium',
                                    textAlign: 'center',
                                  }}>
                                  {item.memberType}
                                </Text>
                              </View>
                            </TouchableOpacity>
                          );
                      }}
                    />
                  ) : (
                    <ActivityIndicator size="small" color="#fff" />
                  )}
                </View>
              ) : (
                <></>
              )}

              {/* --------- Circle Ends ------------- */}

              {/* -------- Endorsement Starts ------------ */}
              {this.props.userEndorsement.body &&
                this.props.userEndorsement.body.length ? (
                <View style={{ paddingVertical: 20, backgroundColor: '#F7F7F5' }}>
                  <View
                    style={{
                      backgroundColor: '#FFFFFF',
                      marginHorizontal: 10,
                      alignSelf: 'center',
                      width: '90%',
                      borderRadius: 6,
                      shadowColor: '#000',
                      shadowOffset: { width: 0, height: 2 },
                      shadowOpacity: 0.25,
                      shadowRadius: 3.84,
                      elevation: 5,
                    }}>
                    <Text
                      style={{
                        color: '#154A59',
                        fontSize: 14,
                        fontFamily: 'Montserrat-Bold',
                        marginTop: 14,
                        marginLeft: 14,
                      }}>
                      ENDORSEMENTS
                    </Text>

                    {this.props.userEndorsement.body ? (
                      <FlatList
                        style={{ paddingVertical: 20 }}
                        showsHorizontalScrollIndicator={false}
                        alwaysBounceHorizontal={false}
                        keyExtractor={(item) => item.topic}
                        data={this.props.userEndorsement.body.slice(0, 5)}
                        renderItem={({ item, index }) => {
                          if (
                            index + 1 === 5 ||
                            index + 1 === this.props.userEndorsement.body.length
                          )
                            return (
                              <View>
                                <View
                                  style={{
                                    width: '100%',
                                    flexDirection: 'row',
                                    marginRight: 10,
                                    borderRadius: 5,
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    paddingRight: 20,
                                    paddingLeft: 30,
                                  }}>
                                  <View
                                    style={{
                                      height: 60,
                                      justifyContent: 'center',
                                      borderTopRightRadius: 5,
                                      borderBottomRightRadius: 6,
                                    }}>
                                    <Text
                                      style={{
                                        color: '#00394D',
                                        fontSize: 14,
                                        fontFamily: 'Montserrat-SemiBold',
                                      }}>
                                      {item.topic}
                                    </Text>
                                    <View
                                      style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                      }}>
                                      <Image
                                        source={defaultCover}
                                        style={{
                                          width: 14,
                                          height: 14,
                                          borderRadius: 7,
                                          marginRight: 6,
                                        }}
                                      />
                                      {/* <Text style={{ color: '#607580', fontSize: 12, fontFamily: 'Montserrat-SemiBold' }}>Derek James</Text> */}
                                      <Text
                                        style={{
                                          color: '#607580',
                                          fontSize: 12,
                                          fontFamily: 'Montserrat-Medium',
                                        }}>
                                        {item.count} endorsements
                                      </Text>
                                    </View>
                                    {/* <Text style={{ marginHorizontal: 12, color: '#90949C', fontSize: 10, fontFamily: 'Montserrat-Medium', fontStyle: 'italic' }}>{this.unixTime(endorsement.startTime).split(' ').splice(1).join(' ')} to {this.unixTime(endorsement.endTime).split(' ').splice(1, 2).join(' ')}</Text> */}
                                  </View>

                                  <TouchableOpacity
                                    onPress={() =>
                                      this.props.navigation.navigate(
                                        'SeeallEndorsementScreen',
                                        {
                                          id: this.state.paramsId,
                                          topic: item.topic,
                                        },
                                      )
                                    }
                                    activeOpacity={0.5}
                                    style={{
                                      width: 16,
                                      height: 16,
                                      borderRadius: 8,
                                      backgroundColor: '#00000033',
                                      justifyContent: 'center',
                                      alignItems: 'center',
                                    }}>
                                    <Icon
                                      name="Arrow_Right"
                                      size={12}
                                      color="#00394D"
                                      style={
                                        Platform.OS === 'android'
                                          ? { marginTop: 6 }
                                          : {}
                                      }
                                    />
                                  </TouchableOpacity>
                                </View>
                              </View>
                            );

                          return (
                            <View>
                              <View
                                style={{
                                  width: '100%',
                                  flexDirection: 'row',
                                  marginRight: 10,
                                  borderRadius: 5,
                                  justifyContent: 'space-between',
                                  alignItems: 'center',
                                  paddingRight: 20,
                                  paddingLeft: 30,
                                }}>
                                <View
                                  style={{
                                    height: 60,
                                    justifyContent: 'center',
                                    borderTopRightRadius: 5,
                                    borderBottomRightRadius: 6,
                                  }}>
                                  <Text
                                    style={{
                                      color: '#00394D',
                                      fontSize: 14,
                                      fontFamily: 'Montserrat-SemiBold',
                                    }}>
                                    {item.topic}
                                  </Text>
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      alignItems: 'center',
                                    }}>
                                    <Image
                                      source={defaultCover}
                                      style={{
                                        width: 14,
                                        height: 14,
                                        borderRadius: 7,
                                        marginRight: 6,
                                      }}
                                    />
                                    {/* <Text style={{ color: '#607580', fontSize: 12, fontFamily: 'Montserrat-SemiBold' }}>Derek James</Text> */}
                                    <Text
                                      style={{
                                        color: '#607580',
                                        fontSize: 12,
                                        fontFamily: 'Montserrat-Medium',
                                      }}>
                                      {item.count} endorsements
                                    </Text>
                                  </View>
                                  {/* <Text style={{ marginHorizontal: 12, color: '#90949C', fontSize: 10, fontFamily: 'Montserrat-Medium', fontStyle: 'italic' }}>{this.unixTime(endorsement.startTime).split(' ').splice(1).join(' ')} to {this.unixTime(endorsement.endTime).split(' ').splice(1, 2).join(' ')}</Text> */}
                                </View>

                                <TouchableOpacity
                                  onPress={() =>
                                    this.props.navigation.navigate(
                                      'SeeallEndorsementScreen',
                                      {
                                        id: this.state.paramsId,
                                        topic: item.topic,
                                      },
                                    )
                                  }
                                  activeOpacity={0.5}
                                  style={{
                                    width: 16,
                                    height: 16,
                                    borderRadius: 8,
                                    backgroundColor: '#00000033',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                  }}>
                                  <Icon
                                    name="Arrow_Right"
                                    size={12}
                                    color="#00394D"
                                    style={
                                      Platform.OS === 'android'
                                        ? { marginTop: 6 }
                                        : {}
                                    }
                                  />
                                </TouchableOpacity>
                              </View>
                              <View
                                style={{
                                  borderColor: '#E8ECEB',
                                  borderWidth: 0.45,
                                  width: '92%',
                                  marginVertical: 10,
                                  alignSelf: 'center',
                                }}></View>
                            </View>
                          );
                        }}
                      />
                    ) : (
                      <ActivityIndicator size="small" color="#00394D" />
                    )}

                    {this.props.userEndorsement.body &&
                      this.props.userEndorsement.body &&
                      this.props.userEndorsement.body.length >
                      5 && (
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate(
                              'SeeallEndorsementScreen',
                              { id: this.state.paramsId },
                            )
                          }
                          activeOpacity={0.6}
                          style={{
                            flexDirection: 'row',
                            marginRight: 10,
                            backgroundColor: '#F1F5EE',
                            borderBottomRightRadius: 8,
                            borderBottomLeftRadius: 8,
                            width: '100%',
                            height: 40,
                            justifyContent: 'space-around',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              color: '#698F8A',
                              fontSize: 12,
                              fontFamily: 'Montserrat-Medium',
                              paddingHorizontal: 6,
                              paddingVertical: 2,
                            }}>
                            See all
                          </Text>
                        </TouchableOpacity>)}
                  </View>

                  {!this.props.route.params ? (
                    <TouchableOpacity
                      style={styles.requestEndorsement}
                      activeOpacity={0.8}
                      onPress={() =>
                        this.setState({ requestEndorsementModalOpen: true }, () =>
                          this.props.userConnectionInfo.body
                            ? this.props.connectsRequest({
                              userId: this.props.route.params
                                ? this.props.route.params.userId
                                : this.state.userId,
                              size: this.props.userConnectionInfo.body
                                .connections,
                            })
                            : null,
                        )
                      }>
                      <Icon
                        name="Feedback"
                        size={12}
                        color="#E7F3E3"
                        style={
                          Platform.OS === 'ios'
                            ? { marginTop: 0 }
                            : { marginTop: 8 }
                        }
                      />
                      <Text
                        style={{
                          fontSize: 12,
                          color: '#E7F3E3',
                          fontFamily: 'Montserrat-SemiBold',
                          marginLeft: 5,
                        }}>
                        Request Endorsement
                      </Text>
                    </TouchableOpacity>
                  ) : (
                    <></>
                  )}
                </View>
              ) : (
                <></>
              )}

              {/* ---------- Endorsement Ends ----------- */}

              {/* ---------- Business pages start ----------- */}

              {this.props.userBusinessPage.body &&
                this.props.userBusinessPage.body.businessPage &&
                this.props.userBusinessPage.body.businessPage.length ? (
                <View style={{ backgroundColor: '#F7F7F5' }}>
                  <View style={[styles.cardView, { marginTop: 16 }]}>
                    <Text
                      style={{
                        color: '#154A59',
                        fontSize: 14,
                        fontFamily: 'Montserrat-Bold',
                        marginVertical: 15,
                        marginLeft: 15,
                      }}>
                      ORGANIZATION PAGES
                    </Text>

                    {this.props.userBusinessPage.body ? (
                      <FlatList
                        // style={{ paddingVertical: 20 }}
                        keyExtractor={(item) => item.companyId}
                        data={this.props.userBusinessPage.body.businessPage
                          .sort(
                            (a, b) =>
                              a.companyName.toUpperCase(0) >
                              b.companyName.toUpperCase(0),
                          )
                          .slice(0, 5)}
                        renderItem={({ item }) =>
                          this.renderItemBusinessPage(item)
                        }
                      />
                    ) : (
                      <ActivityIndicator size="small" color="#00394D" />
                    )}

                    {this.props.userBusinessPage.body &&
                      this.props.userBusinessPage.body.businessPage &&
                      this.props.userBusinessPage.body.businessPage.length >
                      5 && (
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate(
                              'SeeallBusinessPageScreen',
                              { id: this.state.paramsId },
                            )
                          }
                          activeOpacity={0.6}
                          style={{
                            flexDirection: 'row',
                            marginRight: 10,
                            backgroundColor: '#F1F5EE',
                            borderBottomRightRadius: 8,
                            borderBottomLeftRadius: 8,
                            width: '100%',
                            height: 40,
                            justifyContent: 'space-around',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              color: '#698F8A',
                              fontSize: 12,
                              fontFamily: 'Montserrat-Medium',
                              paddingHorizontal: 6,
                              paddingVertical: 2,
                            }}>
                            See all
                          </Text>
                        </TouchableOpacity>
                      )}
                  </View>
                </View>
              ) : (
                <></>
              )}

              {/* ---------- Business pages Ends ----------- */}

              {/* --------- Causes Supported Starts ------------- */}

              {this.props.userCauses.body &&
                this.props.userCauses.body.causeList.content.length ? (
                <View style={styles.causesSupported}>
                  <Text style={styles.causesSupportedText}>
                    CAUSES SUPPORTED
                  </Text>

                  {this.props.userCauses.body ? (
                    <FlatList
                      style={styles.causesFlatList}
                      showsHorizontalScrollIndicator={false}
                      horizontal={true}
                      alwaysBounceHorizontal={false}
                      keyExtractor={(item) => item.id}
                      data={this.props.userCauses.body.causeList.content.slice(
                        0,
                        6,
                      )}
                      renderItem={({ item, index }) => {
                        if (index + 1 === 6)
                          return (
                            <TouchableOpacity
                              onPress={() =>
                                this.props.navigation.navigate(
                                  'SeeallCausesScreen',
                                  { id: this.state.paramsId },
                                )
                              }
                              activeOpacity={0.5}
                              style={styles.seeAll}>
                              <Text style={styles.seeAllText}>See All</Text>
                            </TouchableOpacity>
                          );
                        else
                          return (
                            <TouchableOpacity
                              onPress={() =>
                                this.props.navigation.navigate('CausesStack', {
                                  screen: 'CausesDetail',
                                  params: {
                                    circleData: JSON.stringify(item),
                                    id: item.id,
                                  },
                                })
                              }
                              style={styles.individualCause}
                              activeOpacity={0.8}>
                              {item.imageUrl ? (
                                <Image
                                  source={{ uri: item.imageUrl }}
                                  style={styles.causesImage}
                                />
                              ) : (
                                <Image
                                  source={defaultCover}
                                  style={styles.causesImage}
                                />
                              )}

                              <View style={styles.linearGradientView}>
                                <LinearGradient
                                  colors={['#154A59', '#154A59CC', '#154A5900']}
                                  style={
                                    styles.linearGradient
                                  }></LinearGradient>
                              </View>

                              <Text style={styles.causesText}>{item.name}</Text>
                            </TouchableOpacity>
                          );
                      }}
                    />
                  ) : (
                    <ActivityIndicator size="small" color="#fff" />
                  )}
                </View>
              ) : (
                <></>
              )}

              {/* --------- Causes Supported Ends ------------- */}

              <View style={{ backgroundColor: '#F7F7F5', paddingBottom: 15 }}>
                {/* --------- Specialization Starts ------------- */}

                {this.props.user.body &&
                  this.props.user.body.specialities &&
                  this.props.user.body.specialities.length ? (
                  <TouchableOpacity
                    activeOpacity={1}
                    // onPress={this.specializationExpand}
                    style={
                      this.state.specializationExpand
                        ? styles.expandedStrip
                        : styles.expandToViewStrip
                    }>
                    {this.state.specializationExpand ? (
                      <View
                        style={{
                          width: '100%',
                          marginTop: 15,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text
                          style={{
                            color: '#154A59',
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 14,
                          }}>
                          SPECIALIZATION
                        </Text>
                        <TouchableOpacity
                          onPress={this.specializationExpand}
                          style={{
                            height: 16,
                            width: 16,
                            borderRadius: 8,
                            backgroundColor: '#E8ECEB',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Icon
                            name="Arrow_Up"
                            color="#698F8A"
                            style={
                              Platform.OS === 'ios'
                                ? { marginTop: 0 }
                                : { marginTop: 5 }
                            }
                          />
                        </TouchableOpacity>
                      </View>
                    ) : (
                      <>
                        <Text
                          style={{
                            color: '#154A59',
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 14,
                          }}>
                          SPECIALIZATION
                        </Text>
                        <TouchableOpacity
                          onPress={this.specializationExpand}
                          style={{
                            height: 16,
                            width: 16,
                            borderRadius: 8,
                            backgroundColor: '#E8ECEB',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Icon
                            name="Arrow_Down"
                            color="#698F8A"
                            style={
                              Platform.OS === 'ios'
                                ? { marginTop: 0 }
                                : { marginTop: 5 }
                            }
                          />
                        </TouchableOpacity>
                      </>
                    )}

                    {this.state.specializationExpand && this.props.user.body ? (
                      <>
                        <FlatList
                          columnWrapperStyle={{
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            paddingVertical: 15,
                          }}
                          numColumns={9}
                          data={this.props.user.body.specialities.slice(0, 5)}
                          keyExtractor={(item) => item}
                          renderItem={({ item }) => (
                            <View style={styles.itemBox}>
                              <Text
                                onPress={() => this.props.navigation.navigate('PeopleWithSimilar', {
                                  id: this.state.userId,
                                  totalMutualConnects: this.props.route.params
                                    ? this.state.mutualConnects.length
                                    : null,
                                  currentTab: 'connects',
                                  peopleWithSimilar: 'Specialization',
                                  text: item
                                })}
                                numberOfLines={1}
                                style={{
                                  fontSize: 12,
                                  color: '#367681',
                                  fontFamily: 'Montserrat-SemiBold',
                                }}>
                                {item}
                              </Text>
                            </View>
                          )}
                        />

                        {this.props.user.body.specialities.length > 5 ? (
                          <View
                            style={{
                              width: '100%',
                              justifyContent: 'center',
                              alignItems: 'flex-end',
                              paddingBottom: 15,
                            }}>
                            <TouchableOpacity
                              onPress={() =>
                                this.setState({ specializationModalOpen: true })
                              }>
                              <Text
                                style={{
                                  color: '#91B3A2',
                                  fontFamily: 'Montserrat-Medium',
                                  fontSize: 14,
                                }}>
                                See All
                              </Text>
                            </TouchableOpacity>
                          </View>
                        ) : (
                          <></>
                        )}
                      </>
                    ) : (
                      <></>
                    )}
                  </TouchableOpacity>
                ) : (
                  <></>
                )}

                {/* --------- Specialization Ends ------------- */}

                {/* --------- SKILLS Starts ------------- */}

                {this.props.user.body &&
                  this.props.user.body.skills &&
                  this.props.user.body.skills.length ? (
                  <TouchableOpacity

                    activeOpacity={1}
                    style={
                      this.state.skillExpand
                        ? styles.expandedStrip
                        : styles.expandToViewStrip
                    }>
                    {this.state.skillExpand ? (
                      <View
                        activeOpacity={0.7}
                        style={{
                          width: '100%',
                          marginTop: 15,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text
                          style={{
                            color: '#154A59',
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 14,
                          }}>
                          SKILLS
                        </Text>
                        <TouchableOpacity
                          onPress={() => this.skillExpand()}
                          style={{
                            height: 16,
                            width: 16,
                            borderRadius: 8,
                            backgroundColor: '#E8ECEB',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Icon
                            name="Arrow_Up"
                            color="#698F8A"
                            style={
                              Platform.OS === 'ios'
                                ? { marginTop: 0 }
                                : { marginTop: 5 }
                            }
                          />
                        </TouchableOpacity>
                      </View>
                    ) : (
                      <>
                        <Text
                          style={{
                            color: '#154A59',
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 14,
                          }}>
                          SKILLS
                        </Text>
                        <TouchableOpacity
                          onPress={() => this.skillExpand()}
                          style={{
                            height: 16,
                            width: 16,
                            borderRadius: 8,
                            backgroundColor: '#E8ECEB',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Icon
                            name="Arrow_Down"
                            color="#698F8A"
                            style={
                              Platform.OS === 'ios'
                                ? { marginTop: 0 }
                                : { marginTop: 5 }
                            }
                          />
                        </TouchableOpacity>
                      </>
                    )}

                    {this.state.skillExpand && this.props.user.body.skills ? (
                      <>
                        <FlatList
                          columnWrapperStyle={{
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            paddingVertical: 15,
                          }}
                          numColumns={9}
                          data={this.props.user.body.skills.slice(0, 5)}
                          keyExtractor={(item) => item}
                          renderItem={({ item }) => (
                            <View style={styles.itemBox}>
                              <Text
                                onPress={() => this.props.navigation.navigate('PeopleWithSimilar', {
                                  id: this.state.userId,
                                  totalMutualConnects: this.props.route.params
                                    ? this.state.mutualConnects.length
                                    : null,
                                  currentTab: 'connects',
                                  peopleWithSimilar: 'Skills',
                                  text: item
                                })}
                                numberOfLines={1}
                                style={{
                                  fontSize: 12,
                                  color: '#367681',
                                  fontFamily: 'Montserrat-SemiBold',
                                }}>
                                {item}
                              </Text>
                            </View>
                          )}
                        />

                        {this.props.user.body.skills.length > 5 ? (
                          <View
                            style={{
                              width: '100%',
                              justifyContent: 'center',
                              alignItems: 'flex-end',
                              paddingBottom: 15,
                            }}>
                            <TouchableOpacity
                              activeOpacity={0.6}
                              onPress={() =>
                                this.setState({ skillsModalOpen: true })
                              }>
                              <Text
                                style={{
                                  color: '#91B3A2',
                                  fontFamily: 'Montserrat-Medium',
                                  fontSize: 14,
                                }}>
                                See All
                              </Text>
                            </TouchableOpacity>
                          </View>
                        ) : (
                          <></>
                        )}
                      </>
                    ) : (
                      <></>
                    )}
                  </TouchableOpacity>
                ) : (
                  <></>
                )}

                {/* --------- SKILLS Ends ------------- */}

                {/* --------- INTERESTS Starts ------------- */}

                {this.props.user.body &&
                  this.props.user.body.interests &&
                  this.props.user.body.interests.length > 0 && (
                    <TouchableOpacity
                      activeOpacity={1}

                      style={
                        this.state.interestExpand
                          ? styles.expandedStrip
                          : styles.expandToViewStrip
                      }>
                      {this.state.interestExpand ? (
                        <View
                          style={{
                            width: '100%',
                            marginTop: 15,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              color: '#154A59',
                              fontFamily: 'Montserrat-Bold',
                              fontSize: 14,
                            }}>
                            INTERESTS
                          </Text>
                          <TouchableOpacity
                            onPress={this.interestExpand}
                            style={{
                              height: 16,
                              width: 16,
                              borderRadius: 8,
                              backgroundColor: '#E8ECEB',
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}>
                            <Icon
                              name="Arrow_Up"
                              color="#698F8A"
                              style={
                                Platform.OS === 'ios'
                                  ? { marginTop: 0 }
                                  : { marginTop: 5 }
                              }
                            />
                          </TouchableOpacity>
                        </View>
                      ) : (
                        <View
                          style={{
                            width: '100%',
                            marginTop: 15,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingBottom: 10,
                          }}>
                          <Text
                            style={{
                              color: '#154A59',
                              fontFamily: 'Montserrat-Bold',
                              fontSize: 14,
                            }}>
                            INTERESTS
                          </Text>
                          <TouchableOpacity
                            onPress={this.interestExpand}
                            style={{
                              height: 16,
                              width: 16,
                              borderRadius: 8,
                              backgroundColor: '#E8ECEB',
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}>
                            <Icon
                              name="Arrow_Down"
                              color="#698F8A"
                              style={
                                Platform.OS === 'ios'
                                  ? { marginTop: 0 }
                                  : { marginTop: 5 }
                              }
                            />
                          </TouchableOpacity>
                        </View>
                      )}

                      {this.state.interestExpand &&
                        this.props.user.body &&
                        this.props.user.body.interests &&
                        this.props.user.body.interests.length > 0 ? (
                        <>
                          <FlatList
                            columnWrapperStyle={{
                              flexDirection: 'row',
                              flexWrap: 'wrap',
                              paddingVertical: 15,
                            }}
                            numColumns={9}
                            data={this.props.user.body.interests.slice(0, 5)}
                            keyExtractor={(item) => item}
                            renderItem={({ item }) => (
                              <View style={styles.itemBox}>
                                <Text
                                  onPress={() => this.props.navigation.navigate('PeopleWithSimilar', {
                                    id: this.state.userId,
                                    totalMutualConnects: this.props.route.params
                                      ? this.state.mutualConnects.length
                                      : null,
                                    currentTab: 'connects',
                                    peopleWithSimilar: 'Interest',
                                    text: item
                                  })}
                                  numberOfLines={1}
                                  style={{
                                    fontSize: 12,
                                    color: '#367681',
                                    fontFamily: 'Montserrat-SemiBold',
                                  }}>
                                  {item}
                                </Text>
                              </View>
                            )}
                          />

                          {this.props.user.body.interests.length > 5 ? (
                            <View
                              style={{
                                width: '100%',
                                justifyContent: 'center',
                                alignItems: 'flex-end',
                                paddingBottom: 15,
                              }}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.setState({ interestsModalOpen: true })
                                }>
                                <Text
                                  style={{
                                    color: '#91B3A2',
                                    fontFamily: 'Montserrat-Medium',
                                    fontSize: 14,
                                  }}>
                                  See All
                                </Text>
                              </TouchableOpacity>
                            </View>
                          ) : (
                            <></>
                          )}
                        </>
                      ) : (
                        <></>
                      )}
                    </TouchableOpacity>
                  )}

                {/* --------- INTERESTS Ends ------------- */}
              </View>

              {/* --------- Hashtag Start ------------- */}

              {/* {this.props.userHashTags.body &&
                this.props.userHashTags.body.hashTags.length ? (
                <View style={{ backgroundColor: '#F7F7F5', paddingVertical: 15 }}>
                  <View style={styles.hashtagview}>
                    <Text
                      style={{
                        color: '#154A59',
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 14,
                      }}>
                      HASHTAG
                    </Text>

                    {this.props.userHashTags.body ? (
                      <FlatList
                        columnWrapperStyle={{
                          flexDirection: 'row',
                          flexWrap: 'wrap',
                          paddingVertical: 15,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                        numColumns={9}
                        data={this.props.userHashTags.body.hashTags.slice(0, 6)}
                        keyExtractor={(item) => item}
                        renderItem={({ item, index }) =>
                          index === 5 ? (
                            <TouchableOpacity
                              onPress={() =>
                                this.setState({ hashTagModalOpen: true })
                              }
                              activeOpacity={0.5}
                              style={{
                                height: 30,
                                width: 30,
                                borderRadius: 15,
                                borderColor: '#91B3A2',
                                borderWidth: 1,
                                marginHorizontal: 5,
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}>
                              <Text
                                style={{
                                  fontSize: 12,
                                  fontFamily: 'Montserrat-SemiBold',
                                  color: '#367681',
                                }}>
                                ...
                              </Text>
                            </TouchableOpacity>
                          ) : (
                            <View style={styles.hashtagItem}>
                              <Text
                                numberOfLines={1}
                                style={{
                                  color: '#367681',
                                  fontFamily: 'Montserrat-SemiBold',
                                  fontSize: 12,
                                }}>
                                {item}
                              </Text>
                            </View>
                          )
                        }
                      />
                    ) : (
                      <></>
                    )}
                  </View>
                </View>
              ) : (
                <></>
              )} */}

              {/* --------- Hashtag  Ends ------------- */}
            </View>

            {/* -------- About Ends-------------- */}
          </View>
          <Text
            style={{ textAlign: 'center', color: '#698f8a', marginHorizontal: 10 }}>Update your personal details, education, skills, specializations, interests, etc. It will help you explore relevant opportunities, enable discovery by other users, and to know you better. <Text onPress={() => {
              this.props.navigation.navigate('ProfileEditStack')
            }} style={{ color: '#97a600' }}>Update your profile</Text></Text>
        </ScrollView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  linearGradient2: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  },

  linearGradientView2: {
    width: '100%',
    height: 500,
    position: 'absolute',
    bottom: 150,
    alignSelf: 'center',
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },

  linearGradientView: {
    width: 120,
    height: 80,
    position: 'absolute',
    top: 0,
  },
  termsmodal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '85%',
    paddingVertical: 5.5,
    paddingLeft: 30,
  },
  termsModalNew: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '85%',
    paddingVertical: 11,
    borderBottomWidth: 0.55,
    borderBottomColor: '#E8ECEB',
  },
  modalText: {
    fontSize: 14,
    color: '#154A59',
    fontFamily: 'Montserrat-SemiBold',
    marginLeft: 14,
  },
  modalCategoriesContainer: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    paddingVertical: 20,
    // height: 250
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 6 : 0,
  },
  crossButtonContainer: {
    alignSelf: 'center',
    width: 42,
    height: 42,
    borderRadius: 21,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E7F3E3',
    marginBottom: 10,
  },
  modalCountry: {
    marginTop: 'auto',
  },

  causesText: {
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
    position: 'absolute',
    top: 6,
    left: 8,
    zIndex: 2,
    width: 110,
  },
  causesImage: {
    width: 120,
    height: 150,
    borderRadius: 6,
  },
  individualCause: {
    marginRight: 16,
  },
  seeAllText: {
    color: '#698F8A',
    fontSize: 11,
    fontFamily: 'Montserrat-Medium',
  },
  seeAll: {
    marginHorizontal: 16,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 26,
    width: 70,
    borderWidth: 1,
    borderColor: '#698F8A',
    borderRadius: 13,
    paddingHorizontal: 6,
    paddingVertical: 2,
  },
  causesFlatList: {
    paddingTop: 20,
    paddingBottom: 10,
  },
  causesSupportedText: {
    color: '#154A59',
    fontSize: 14,
    fontFamily: 'Montserrat-Bold',
    marginLeft: 8,
  },
  causesSupported: {
    backgroundColor: '#E7F3E3',
    paddingVertical: 20,
    paddingLeft: 16,
  },

  container: {
    backgroundColor: '#F7F7F5',
  },
  imageContainer: {
    width: '100%',
    height: screenHeight * 0.1,
    backgroundColor: COLORS.white
  },
  imageUsername: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingLeft: 20,
  },
  header: {
    flexDirection: 'row',
    marginBottom: 38,
    marginTop: -10,
  },
  userProfile: {
    marginLeft: 2,
    marginTop: 22,
  },
  userName: {
    color: '#154A59',
    fontSize: 20,
    fontFamily: 'Montserrat-SemiBold',
  },
  viewProfile: {
    fontSize: 14,
    color: '#698F8A',
    fontFamily: 'Montserrat-Medium',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  border: {
    borderBottomWidth: 0.6,
    borderBottomColor: '#91B3A2',
    width: '84%',
    marginLeft: 30,
  },
  image: {
    height: screenHeight * 0.25,
    width: '190',
  },
  mainSection: {
    backgroundColor: '#FFFFFF',
    width: '100%',
    borderTopWidth: 0,
  },

  requestEndorsement: {
    width: 201,
    height: 35,
    borderRadius: 20,
    backgroundColor: '#367681',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: 25,
  },
  cardView: {
    width: '90%',
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    marginBottom: 23,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  businessPageList: {
    height: 56,
    width: '90%',
    backgroundColor: '#fff',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 23,
    paddingBottom: 10,
    marginTop: 15,
  },
  borderView: {
    borderColor: '#E8ECEB',
    borderWidth: 0.5,
    width: '92%',
    alignSelf: 'center',
  },
  expandToViewStrip: {
    width: '90%',
    height: 45,
    borderRadius: 6,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    alignItems: 'center',
    marginTop: 15,
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  expandedStrip: {
    width: '90%',
    // height: 202,
    borderRadius: 6,
    backgroundColor: '#fff',
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    paddingHorizontal: 15,
    alignItems: 'center',
    marginTop: 15,
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  itemBox: {
    // width: 90,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomColor: '#D9E1E4',
    borderBottomWidth: 3,
    marginHorizontal: 5,
    marginVertical: 8,
  },
  hashtagview: {
    width: '70%',
    alignItems: 'center',
    alignSelf: 'center',
  },
  hashtagItem: {
    height: 31,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: '#91B3A2',
    backgroundColor: '#CFE7C780',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    marginHorizontal: 5,
    marginVertical: 6,
  },
});

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,

    userAddressProgress: state.personalProfileReducer.userAddressProgress,
    userAddress: state.personalProfileReducer.userAddress,
    errorAddress: state.personalProfileReducer.errorAddress,

    userSkillsSpecializationProgress:
      state.personalProfileReducer.userSkillsSpecializationProgress,
    userSkillsSpecialization:
      state.personalProfileReducer.userSkillsSpecialization,
    errorSkillsSpecialization:
      state.personalProfileReducer.errorSkillsSpecialization,

    userRecentActivityProgress:
      state.personalProfileReducer.userRecentActivityProgress,
    userRecentActivity: state.personalProfileReducer.userRecentActivity,
    errorRecentActivity: state.personalProfileReducer.errorRecentActivity,

    userExperienceProgress: state.personalProfileReducer.userExperienceProgress,
    userExperience: state.personalProfileReducer.userExperience,
    errorExperience: state.personalProfileReducer.errorExperience,

    userEducationProgress: state.personalProfileReducer.userEducationProgress,
    userEducation: state.personalProfileReducer.userEducation,
    errorEducation: state.personalProfileReducer.errorEducation,

    userCircleProgress: state.circleReducer.userCircleProgress,
    userCircle: state.circleReducer.userCircle,
    errorCircle: state.circleReducer.errorCircle,

    userEndorsementProgress: state.endorsementReducer.userEndorsementProgress,
    userEndorsement: state.endorsementReducer.userEndorsement,
    errorEndorsement: state.endorsementReducer.errorEndorsement,

    userBusinessPageProgress:
      state.personalProfileReducer.userBusinessPageProgress,
    userBusinessPage: state.personalProfileReducer.userBusinessPage,
    errorBusinessPage: state.personalProfileReducer.errorBusinessPage,

    userCausesProgress: state.causesReducer.userCausesProgress,
    userCauses: state.causesReducer.userCauses,
    errorCauses: state.causesReducer.errorCauses,

    userHashTagsProgress: state.personalProfileReducer.userHashTagsProgress,
    userHashTags: state.personalProfileReducer.userHashTags,
    errorHashTags: state.personalProfileReducer.errorHashTags,

    userConnectionInfoProgress:
      state.personalProfileReducer.userConnectionInfoProgress,
    userConnectionInfo: state.personalProfileReducer.userConnectionInfo,
    errorConnectionInfo: state.personalProfileReducer.errorConnectionInfo,

    userConnectsProgress: state.connectsReducer.userConnectsProgress,
    userConnects: state.connectsReducer.userConnects,
    errorConnects: state.connectsReducer.errorConnects,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
    personalAddressRequest: (data) => dispatch(personalAddressRequest(data)),
    personalSkillsSpecializationRequest: (data) =>
      dispatch(personalSkillsSpecializationRequest(data)),
    personalRecentActivityRequest: (data) =>
      dispatch(personalRecentActivityRequest(data)),
    personalExperienceRequest: (data) =>
      dispatch(personalExperienceRequest(data)),
    personalEducationRequest: (data) =>
      dispatch(personalEducationRequest(data)),
    userCircleRequest: (data) => dispatch(userCircleRequest(data)),
    endorsementRequest: (data) => dispatch(endorsementRequest(data)),
    personalBusinessPageRequest: (data) =>
      dispatch(personalBusinessPageRequest(data)),
    causesRequest: (data) => dispatch(causesRequest(data)),
    personalHashTagsRequest: (data) => dispatch(personalHashTagsRequest(data)),
    personalConnectionInfoRequest: (data) =>
      dispatch(personalConnectionInfoRequest(data)),
    connectsRequest: (data) => dispatch(connectsRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
