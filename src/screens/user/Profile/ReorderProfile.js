import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, SafeAreaView, TouchableOpacity, Platform } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { AutoDragSortableView } from 'react-native-drag-sort'

import AsyncStorage from '@react-native-community/async-storage'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultStyle from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

const AUTO_TEST_DATA = [
    { icon: 'Projects_F1', title: 'Experiences', txt: 1, hide: false },
    { icon: 'MortarBoard', title: 'Education', txt: 2, hide: false },
    { icon: 'Circles_Fl', title: 'Circles', txt: 3, hide: false },
    { icon: 'Feedback', title: 'Endorsements', txt: 4, hide: false },
    { icon: 'Company', title: 'Organization Pages', txt: 5, hide: false },
    { icon: 'Causes_F', title: 'Causes', txt: 6, hide: false },
    { icon: 'Star_OL', title: 'Specialization', txt: 7, hide: false },
    { icon: 'Skill', title: 'Skills', txt: 8, hide: false },
    { icon: 'Like', title: 'Interests', txt: 9, hide: false },
    { icon: 'Hashtag', title: 'Hashtag', txt: 10, hide: false },
]


const { width } = Dimensions.get('window')

const parentWidth = width
const childrenWidth = width - 20
const childrenHeight = 48

export default class ReorderProfile extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: AUTO_TEST_DATA,
            userId: '',
            profileItems: [],
            profileItemsModal: []
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('userId')
            .then((value) => {
                this.setState({ userId: value })
                this.getItems(value)
            })
    }

    getItems = (userId) => {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profileOrder/get/' + userId,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data.body !== null) {
                console.log("response.data.body ", response.data.body)
                let profileItemsMenu = [];
                this.state.profileItemsModal = [];
                var obj = response.data.body.profileOrder;
                for (let i = 0; i < Object.keys(obj).length; i++) {
                    profileItemsMenu.push({ 'name': Object.keys(obj)[i], 'value': Object.values(obj)[i] });
                    //this.state.profileItemsModal.push({'name': Object.keys(obj)[i], 'value' : Object.values(obj)[i]});
                }
                profileItemsMenu.sort((a, b) => (a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0))
                //this.state.profileItemsModal.sort((a,b) => (a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0))
                this.setState({
                    profileItems: profileItemsMenu,
                    profileItemsModal: profileItemsMenu
                })
            }
        }).catch((err) => {
            console.log(err);
        });
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 15 }] : styles.header}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity style={{ width: 15 }} onPress={() => this.props.navigation.goBack()}>
                            <Icon name="Arrow-Left" size={14} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                        </TouchableOpacity>
                        <Text style={[defaultStyle.Button3, { color: COLORS.dark_800, marginLeft: 15 }]}>
                            REORDER PROFILE PAGE
                        </Text>
                    </View>
                </View>

                <Text style={styles.instructions}>Press & Hold to Reorder Sections</Text>

                <AutoDragSortableView
                    dataSource={this.state.data}
                    delayLongPress={100}
                    autoThrottle={25}
                    autoThrottleDuration={100}
                    parentWidth={parentWidth}
                    childrenWidth={childrenWidth}
                    marginChildrenBottom={10}
                    marginChildrenRight={10}
                    marginChildrenLeft={10}
                    childrenHeight={childrenHeight}

                    onDataChange={(data) => {

                        this.setState({
                            data: data
                        }, () => console.log("dragable data : ", this.state.data))
                    }}
                    keyExtractor={(item, index) => item.txt}
                    renderItem={(item, index) => {
                        return this.renderItem(item, index)
                    }}
                />
            </SafeAreaView>
        )
    }

    renderItem(item, index) {
        return (
            <View style={styles.item}>
                <View style={styles.item_icon_swipe}>
                    {/* <Icon name="Hamburger" size={18} color={COLORS.dark_800} /> */}
                    <Icon name={item.icon} size={18} color={COLORS.dark_800} style={{ marginLeft: 12, marginRight: 5, marginTop: Platform.OS === 'android' && 10 }} />
                    <Text>{item.title}</Text>
                </View>
                <View style={{ height: 40, width: 40, alignItems: 'center', justifyContent: 'center', marginRight: 6 }}
                // onPress={() => { item.hide = !item.hide, this.setState({ ...this.state }) }}
                >
                    <Icon name="Hamburger" size={18} color={COLORS.dark_800} />
                </View>
            </View>
        )
    }



}

const styles = StyleSheet.create({

    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    instructions: {
        color: COLORS.altgreen_400,
        fontSize: 12,
        fontStyle: 'italic',
        alignSelf: 'center',
        marginVertical: 15
    },
    item: {
        width: childrenWidth,
        height: childrenHeight,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 4,
    },
    item_icon_swipe: {
        flexDirection: 'row',
        marginLeft: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
})
