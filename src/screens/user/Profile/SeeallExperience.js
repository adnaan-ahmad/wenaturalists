import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Platform,
    FlatList,
    ActivityIndicator,
    Image,
    Modal,
    SafeAreaView,
    Linking
} from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'
import LinearGradient from 'react-native-linear-gradient'
import axios from 'axios'
import Autolink from 'react-native-autolink'

import typography from '../../../Components/Shared/Typography'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import projectDefault from '../../../../assets/project-default.jpg'
import { COLORS } from '../../../Components/Shared/Colors';
import defaultStyle from '../../../Components/Shared/Typography';
import { personalExperienceRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions'
import icoMoonConfig from '../../../../assets/Icons/selection.json'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

class SeeallExperience extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            modalOpen: false,
            selectedExperienceType: this.props.route.params.selectedExperienceType ? this.props.route.params.selectedExperienceType : 'ASSIGNMENTEVENTTRAINING',
            readMoreExperienceId: '',
            experienceList: []
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value })
            this.props.route.params.selectedExperienceType === 'JOB' ? this.getJobs(value) : this.getAllEvents(value)

            this.props.route.params && this.props.route.params.id !== '' ?
                this.props.personalExperienceRequest({ userId: this.props.route.params.id, otherUserId: value })
                : this.props.personalExperienceRequest({ userId: value, otherUserId: '' })


            // this.props.personalExperienceRequest({ userId: value, otherUserId: '' })
        }).catch((e) => {
            console.log(e)
        })

    }

    getJobs = (userId) => {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/experience?id=' + userId + '&page=0&size=500',
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            },
        }).then((response) => {
            if (response && response.status === 200 && response.data.body) {
                console.log('getJobs', response.data.body.experience.content[1])
                this.setState({ experienceList: response.data.body.experience.content })
            }
        }).catch((e) => {
            console.log(e)
        })
    }

    getAllEvents = (userId) => {

        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + "/backend/participation/find-project-published-applicants-details-by-userId-and-custom-project-type/" + userId + "/" + this.state.selectedExperienceType + "?page=" + 0 + "&size=" + 500,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.message === 'Success!') {
                console.log('getAllEvents', response.data.body.content[1])

                this.setState({ experienceList: response.data.body.content })
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    renderItem = (experience, index) => {
        return (
            <View
                style={{
                    backgroundColor: '#FFFFFF',
                    marginHorizontal: 5,
                    width: '95%',
                    borderRadius: 6,
                    marginVertical: 8,
                    shadowColor: '#000',
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 5,
                }}
                key={experience.project.id}>
                <View
                    style={{
                        paddingBottom: 14,
                        marginRight: 10,
                        padding: 6,
                        width: '95%',
                        borderRadius: 6,
                        paddingLeft: 20
                    }}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => this.props.navigation.navigate('ProjectDetailView', {
                            slug: experience.project.slug,
                        })}
                        style={{
                            flexDirection: 'row',
                            // marginLeft: 20,
                            borderRadius: 5,
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            alignSelf: 'flex-start'
                        }}>
                        {experience.project.coverImage !== null ? (
                            <Image
                                source={{ uri: experience.project.coverImage }}
                                style={{ width: 50, height: 50, borderRadius: 5 }}
                            />
                        ) : (
                            <Image
                                source={projectDefault}
                                style={{ width: 50, height: 50, borderRadius: 5 }}
                            />
                        )}
                        <View
                            style={{
                                maxWidth: 280,
                                backgroundColor: '#FFF',
                                justifyContent: 'center',
                                borderTopRightRadius: 5,
                                borderBottomRightRadius: 6,
                            }}>
                            <Text numberOfLines={2}
                                style={[
                                    defaultStyle.Title_2,
                                    { marginHorizontal: 12, color: COLORS.primarydark, textTransform: 'capitalize' },
                                ]}>
                                {experience.project.title}
                            </Text>
                            <Text
                                style={[
                                    defaultStyle.Caption,
                                    { marginLeft: 12, color: COLORS.altgreen_400, textTransform: 'capitalize' },
                                ]}>
                                {experience.project.companyName}
                            </Text>
                            <Text
                                style={[
                                    defaultStyle.Body_1,
                                    { marginHorizontal: 12, color: COLORS.grey, textTransform: 'capitalize' },
                                ]}>
                                {experience.project.location && experience.project.location.city}, {experience.project.location && experience.project.location.state}, {experience.project.location && experience.project.location.country}
                            </Text>
                        </View>
                    </TouchableOpacity>

                    <View
                        style={{
                            borderColor: '#E8ECEB',
                            borderWidth: 0.45,
                            width: '105%',
                            marginVertical: 10,
                            marginLeft: -6,
                        }}></View>

                    {this.renderDescription(experience.project.shortBrief, experience.project.id)}

                    {/* <Text
                        numberOfLines={
                            this.state.readMoreExperienceId !== experience.project.id ? 3 : 20
                        }
                        style={[
                            defaultStyle.Body_2,
                            {
                                marginHorizontal: 11,
                                paddingBottom: 36,
                                color: COLORS.grey_500,
                            },
                        ]}>
                        {experience.project.shortBrief}
                    </Text> */}

                    <TouchableOpacity
                        style={{
                            height: 30,
                            width: 80,
                            marginLeft: 6,
                            marginTop: -20,
                            marginBottom: 16,
                        }}
                        onPress={() =>
                            this.setState({
                                readMoreExperienceId:
                                    this.state.readMoreExperienceId.length > 0
                                        ? ''
                                        : experience.project.id,
                            })
                        }>
                        <Text style={{ color: COLORS.green_500, fontWeight: '700', fontSize: 12 }}>
                            {experience.project.shortBrief && experience.project.shortBrief.length > 105 &&
                                this.state.readMoreExperienceId !== experience.project.id
                                ? 'Read more'
                                : this.state.readMoreExperienceId === experience.project.id
                                    ? 'Read less'
                                    : null}
                        </Text>
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        marginRight: 10,
                        backgroundColor: '#E7F3E3',
                        borderBottomRightRadius: 6,
                        borderBottomLeftRadius: 6,
                        width: '100%',
                        height: 40,
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        position: 'absolute',
                        bottom: -6,
                        paddingLeft: 26,
                        paddingRight: 16
                    }}>
                    {this.unixTime(experience.project.createTime)
                        .split(' ')
                        .splice(1)
                        .join(' ') !==
                        this.unixTime(experience.project.endingTime)
                            .split(' ')
                            .splice(1)
                            .join(' ') &&
                        this.unixTime(experience.project.endingTime).split(' ').splice(1).join(' ') !==
                        'Jan 1970' && new Date().getTime() >= experience.project.endingTime ? (
                        <Text
                            style={[
                                defaultStyle.Body_2_italic,
                                { color: COLORS.altgreen_400 },
                            ]}>
                            {this.unixTime(experience.project.createTime)
                                .split(' ')
                                .splice(1)
                                .join(' ')}{' '}
                            to{' '}
                            {this.unixTime(experience.project.endingTime)
                                .split(' ')
                                .splice(1, 2)
                                .join(' ')}
                        </Text>
                    ) : this.unixTime(experience.project.endingTime)
                        .split(' ')
                        .splice(1)
                        .join(' ') === 'Jan 1970' || new Date().getTime() < experience.project.endingTime ? (
                        <Text
                            style={[
                                defaultStyle.Body_2_italic,
                                { color: COLORS.altgreen_400 },
                            ]}>
                            {this.unixTime(experience.project.createTime)
                                .split(' ')
                                .splice(1)
                                .join(' ')}{' '}
                            - Present
                        </Text>
                    ) : (
                        <Text
                            style={[
                                defaultStyle.Body_2_italic,
                                { color: COLORS.altgreen_400 },
                            ]}>
                            {this.unixTime(experience.project.createTime)
                                .split(' ')
                                .splice(1)
                                .join(' ')}
                        </Text>
                    )}
                    <Text
                        style={[
                            defaultStyle.Note,
                            {
                                color: COLORS.altgreen_400,
                                borderWidth: 1,
                                borderColor: '#91B3A2',
                                borderRadius: 4,
                                paddingHorizontal: 6,
                                paddingVertical: 2,
                            },
                        ]}>
                        {experience.projectType}
                    </Text>
                </View>
            </View>
        )
    }

    employmentType = (value) => {
        if (value === 'FULL_TIME') return 'Full-time'
        if (value === 'PART_TIME') return 'Part-time'
        if (value === 'SELF_EMPLOYED') return 'Self-employed'
        if (value === 'FREELANCE') return 'Freelance'
        if (value === 'INTERNSHIP') return 'Internship'
        if (value === 'TRAINEE') return 'Trainee'
    }

    openWebsite = (website) => {
        if (website && website.includes('http')) return Linking.openURL(website)
        else if (website && !website.includes('http'))
            return Linking.openURL('https://' + website)
    }

    renderJobItem = (experience, index) => {
        return (
            <View
                style={{
                    backgroundColor: '#FFFFFF',
                    marginHorizontal: 5,
                    width: '95%',
                    borderRadius: 6,
                    marginVertical: 8,
                    shadowColor: '#000',
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 5,
                }}
                key={experience.id}>
                <View
                    style={{
                        paddingBottom: 14,
                        marginRight: 10,
                        padding: 6,
                        width: '95%',
                        borderRadius: 6,
                        paddingLeft: 20
                    }}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => console.log(experience)
                            // this.props.navigation.navigate('ProjectDetailView', {
                            // slug: experience.slug,
                            // })
                        }
                        style={{
                            flexDirection: 'row',
                            // marginLeft: 20,
                            borderRadius: 5,
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            alignSelf: 'flex-start'
                        }}>
                        {experience.coverImageUrl !== null ? (
                            <Image
                                source={{ uri: experience.coverImageUrl }}
                                style={{ width: 50, height: 50, borderRadius: 5 }}
                            />
                        ) : (
                            <Image
                                source={projectDefault}
                                style={{ width: 50, height: 50, borderRadius: 5 }}
                            />
                        )}
                        <View
                            style={{
                                maxWidth: 280,
                                backgroundColor: '#FFF',
                                justifyContent: 'center',
                                borderTopRightRadius: 5,
                                borderBottomRightRadius: 6,
                            }}>
                            <Text numberOfLines={2}
                                style={[
                                    defaultStyle.Title_2,
                                    { marginHorizontal: 12, color: COLORS.primarydark, textTransform: 'capitalize' },
                                ]}>
                                {experience.title}
                            </Text>
                            <Text
                                style={[
                                    defaultStyle.Caption,
                                    { marginLeft: 12, color: COLORS.altgreen_400, textTransform: 'capitalize' },
                                ]}>
                                {experience.companyName}
                            </Text>

                            {experience.employmentType && <Text
                                style={[
                                    defaultStyle.Caption,
                                    { marginLeft: 12, color: '#888', textTransform: 'capitalize', fontSize: 11 },
                                ]}>
                                {this.employmentType(experience.employmentType)}
                            </Text>}

                            <Text
                                style={[
                                    defaultStyle.Body_1,
                                    { marginHorizontal: 12, color: COLORS.grey, textTransform: 'capitalize' },
                                ]}>
                                {experience.location && experience.location.city && experience.location.city}, {experience.location && experience.location.state && experience.location.state}, {experience.location && experience.location.country && experience.location.country}
                            </Text>

                            {experience.website ?
                                <TouchableOpacity
                                    onPress={() =>
                                        this.openWebsite(experience.website)
                                    }
                                    activeOpacity={0.5}
                                    style={{
                                        // backgroundColor: '#154A59',
                                        // width: 131,
                                        // height: 31,
                                        // borderRadius: 17,
                                        flexDirection: 'row',
                                        justifyContent: 'flex-start',
                                        alignItems: 'center',
                                        marginLeft: 12,
                                    }}>
                                    <Icon
                                        name="Export"
                                        size={10}
                                        color="#367681"
                                        style={
                                            Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 6 }
                                        }
                                    />
                                    <Text
                                        style={[
                                            defaultStyle.Subtitle_2,
                                            { color: COLORS.altgreen_300, marginLeft: 5 },
                                        ]}>
                                        Website
                                    </Text>
                                </TouchableOpacity> : <></>}

                        </View>
                    </TouchableOpacity>

                    <View
                        style={{
                            borderColor: '#E8ECEB',
                            borderWidth: 0.45,
                            width: '105%',
                            marginVertical: 10,
                            marginLeft: -6,
                        }}></View>

                    {this.renderDescription(experience.description, experience.id)}

                    {/* <Text
                        numberOfLines={
                            this.state.readMoreExperienceId !== experience.id ? 3 : 20
                        }
                        style={[
                            defaultStyle.Body_2,
                            {
                                marginHorizontal: 11,
                                paddingBottom: 36,
                                color: COLORS.grey_500,
                            },
                        ]}>
                        {experience.description}
                    </Text> */}

                    <TouchableOpacity
                        style={{
                            height: 30,
                            width: 80,
                            marginLeft: 6,
                            marginTop: -20,
                            marginBottom: 16
                        }}
                        onPress={() =>
                            this.setState({
                                readMoreExperienceId:
                                    this.state.readMoreExperienceId.length > 0
                                        ? ''
                                        : experience.id,
                            })
                        }>
                        <Text style={{ color: COLORS.green_500, fontWeight: '700', fontSize: 12 }}>
                            {experience.description.length > 105 &&
                                this.state.readMoreExperienceId !== experience.id
                                ? 'Read more'
                                : this.state.readMoreExperienceId === experience.id
                                    ? 'Read less'
                                    : null}
                        </Text>
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        marginRight: 10,
                        backgroundColor: '#E7F3E3',
                        borderBottomRightRadius: 6,
                        borderBottomLeftRadius: 6,
                        width: '100%',
                        height: 40,
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        position: 'absolute',
                        bottom: -6,
                        paddingLeft: 26,
                        paddingRight: 16
                    }}>
                    {this.unixTime(experience.createTime)
                        .split(' ')
                        .splice(1)
                        .join(' ') !==
                        this.unixTime(experience.endTime)
                            .split(' ')
                            .splice(1)
                            .join(' ') &&
                        this.unixTime(experience.endTime).split(' ').splice(1).join(' ') !==
                        'Jan 1970' && new Date().getTime() >= experience.endTime ? (
                        <Text
                            style={[
                                defaultStyle.Body_2_italic,
                                { color: COLORS.altgreen_400 },
                            ]}>
                            {this.unixTime(experience.createTime)
                                .split(' ')
                                .splice(1)
                                .join(' ')}{' '}
                            to{' '}
                            {this.unixTime(experience.endTime)
                                .split(' ')
                                .splice(1, 2)
                                .join(' ')}
                        </Text>
                    ) : this.unixTime(experience.endTime)
                        .split(' ')
                        .splice(1)
                        .join(' ') === 'Jan 1970' || new Date().getTime() < experience.endTime ? (
                        <Text
                            style={[
                                defaultStyle.Body_2_italic,
                                { color: COLORS.altgreen_400 },
                            ]}>
                            {this.unixTime(experience.createTime)
                                .split(' ')
                                .splice(1)
                                .join(' ')}{' '}
                            - Present
                        </Text>
                    ) : (
                        <Text
                            style={[
                                defaultStyle.Body_2_italic,
                                { color: COLORS.altgreen_400 },
                            ]}>
                            {this.unixTime(experience.createTime)
                                .split(' ')
                                .splice(1)
                                .join(' ')}
                        </Text>
                    )}
                    <Text
                        style={[
                            defaultStyle.Note,
                            {
                                color: COLORS.altgreen_400,
                                borderWidth: 1,
                                borderColor: '#91B3A2',
                                borderRadius: 4,
                                paddingHorizontal: 6,
                                paddingVertical: 2,
                            },
                        ]}>
                        {experience.type}
                    </Text>
                </View>
            </View>
        )
    }

    trimDescription = (item) => {
        item = item.replace(/&nbsp;/g, ' ');
        item = item.replace(/<br\s*[\/]?>/gi, '\n');

        const regex = /(<([^>]+)>)/gi;
        item = item.replace(regex, '');

        return item.split('^^__').join(' ');
    };

    renderDescription = (description, id) => {

        return (
            <Autolink
                text={this.trimDescription(description)}
                email
                hashtag="instagram"
                mention="twitter"
                phone="sms"
                numberOfLines={
                    this.state.readMoreExperienceId !== id ? 3 : 20
                }
                style={[
                    typography.Body_1,
                    {
                        fontSize: 11,
                        color: COLORS.dark_700,
                        marginLeft: 6,
                        marginTop: 8,
                        marginBottom: 20
                    }
                ]}
                url
            />
        )
    }

    unixTime = (UNIX_timestamp) => {
        var date = new Date(UNIX_timestamp)
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        var year = date.getFullYear()
        var month = months[date.getMonth()]
        var day = date.getDate()
        return day + ' ' + month + ' ' + year
    }

    // renderItem = (item) => {
    //     return (
    //         <View style={styles.experienceCard}>
    //             <View style={styles.expCardHeader}>

    //                 {item.coverImageUrl !== null ?
    //                     <Image source={{ uri: item.coverImageUrl }} style={{ width: 50, height: 50, borderRadius: 6 }} />
    //                     :
    //                     <Image source={projectDefault} style={{ width: 50, height: 50, borderRadius: 6 }} />
    //                 }
    //                 <View style={{ height: 50, backgroundColor: '#FFF', justifyContent: 'center', }}>
    //                     <Text numberOfLines={1} style={styles.expTitle}>{item.title}</Text>
    //                     <Text numberOfLines={1} style={styles.expCompanyName}>{item.companyName}</Text>
    //                     <Text numberOfLines={1} style={styles.expLocation}>{item.location.city}, {item.location.country}</Text>
    //                 </View>
    //             </View>

    //             <Text
    //                 numberOfLines={this.state.readMoreExperienceId !== item.id ? 3 : 20}
    //                 style={styles.expDescription}>
    //                 {item.description}
    //             </Text>
    //             <TouchableOpacity style={{ height: 30, width: 80, marginLeft: 14, marginTop: -16 }}
    //                 onPress={() => this.setState({ readMoreExperienceId: this.state.readMoreExperienceId === item.id ? '' : item.id })}>
    //                 <Text style={{ color: COLORS.green_500, fontWeight: '700' }}>
    //                     {item.description.length > 150 && this.state.readMoreExperienceId !== item.id ? 'Read more'
    //                         : this.state.readMoreExperienceId === item.id ? 'Read less'
    //                             : null}
    //                 </Text>
    //             </TouchableOpacity>

    //             <View style={{ flexDirection: 'row', paddingHorizontal: 10, backgroundColor: '#E7F3E3', borderBottomRightRadius: 6, borderBottomLeftRadius: 6, width: '100%', height: 40, justifyContent: 'space-between', alignItems: 'center' }}>
    //                 {this.unixTime(item.startTime).split(' ').splice(1, 2).join(' ') !== this.unixTime(item.endTime).split(' ').splice(1).join(' ') && this.unixTime(item.endTime).split(' ').splice(1).join(' ') !== 'Jan 1970' ?
    //                     <Text style={{ color: '#698F8A', fontSize: 12, fontFamily: 'Montserrat-Medium', fontStyle: 'italic' }}>{this.unixTime(item.startTime).split(' ').splice(1).join(' ')} to {this.unixTime(item.endTime).split(' ').splice(1, 2).join(' ')}</Text>
    //                     : this.unixTime(item.endTime).split(' ').splice(1).join(' ') === 'Jan 1970' ?
    //                         <Text style={{ color: '#698F8A', fontSize: 12, fontFamily: 'Montserrat-Medium', fontStyle: 'italic' }}>{this.unixTime(item.startTime).split(' ').splice(1).join(' ')} - Running</Text>
    //                         :
    //                         <Text style={{ color: '#698F8A', fontSize: 12, fontFamily: 'Montserrat-Medium', fontStyle: 'italic' }}>{this.unixTime(item.startTime).split(' ').splice(1).join(' ')}</Text>
    //                 }
    //                 <Text style={{ color: '#698F8A', fontSize: 10, fontFamily: 'Montserrat-Medium', fontStyle: 'italic', borderWidth: 1, borderColor: '#91B3A2', borderRadius: 4, paddingHorizontal: 6, paddingVertical: 2 }}>{item.type}</Text>
    //             </View>
    //         </View>
    //     )
    // }

    modal = () => {
        return (
            <Modal
                visible={this.state.modalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={styles.modalCountry}>
                    <View
                        style={{
                            width: '100%',
                            height: 500,
                            position: 'absolute',
                            bottom: 0,
                            alignSelf: 'center',
                        }}>
                        <LinearGradient
                            colors={['#154A5900', '#154A59CC', '#154A59']}
                            style={{
                                flex: 1,
                                paddingLeft: 15,
                                paddingRight: 15,
                                borderBottomLeftRadius: 6,
                                borderBottomRightRadius: 6,
                            }}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={styles.crossButtonContainer}
                        onPress={() => this.setState({ modalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color="#367681"
                            style={styles.crossIcon}
                        />
                    </TouchableOpacity>

                    <View style={[styles.modalCategoriesContainer, { height: 360 }]}>
                        <TouchableOpacity
                            style={styles.termsModalNew}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({
                                    modalOpen: false,
                                    selectedExperienceType: 'ASSIGNMENTEVENTTRAINING',
                                    experienceList: []
                                }, () => this.getAllEvents(this.state.userId))
                            }}>
                            <Text style={[styles.modalText, { marginLeft: 0 }]}>All</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.termsModalNew}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({
                                    modalOpen: false,
                                    selectedExperienceType: 'JOB',
                                    experienceList: []
                                }, () => this.getJobs(this.state.userId))
                            }}>
                            <Text style={[styles.modalText, { marginLeft: 0 }]}>Job</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.termsModalNew}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({
                                    modalOpen: false,
                                    selectedExperienceType: 'ASSIGNMENT',
                                    experienceList: []
                                }, () => this.getAllEvents(this.state.userId))
                            }}>
                            <Text style={[styles.modalText, { marginLeft: 0 }]}>
                                Assignment
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.termsModalNew}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({
                                    modalOpen: false,
                                    selectedExperienceType: 'EVENT',
                                    experienceList: []
                                }, () => this.getAllEvents(this.state.userId))
                            }}>
                            <Text style={[styles.modalText, { marginLeft: 0 }]}>Event</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[styles.termsModalNew]}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({
                                    modalOpen: false,
                                    selectedExperienceType: 'TRAINING',
                                    experienceList: []
                                }, () => this.getAllEvents(this.state.userId))
                            }}>
                            <Text style={[styles.modalText, { marginLeft: 0 }]}>Training</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[styles.termsModalNew]}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({
                                    modalOpen: false,
                                    selectedExperienceType: 'EXPEDITION',
                                    experienceList: []
                                }, () => this.getAllEvents(this.state.userId))
                            }}>
                            <Text style={[styles.modalText, { marginLeft: 0 }]}>Expedition</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[styles.termsModalNew]}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({
                                    modalOpen: false,
                                    selectedExperienceType: 'STORYBOOK',
                                    experienceList: []
                                }, () => this.getAllEvents(this.state.userId))
                            }}>
                            <Text style={[styles.modalText, { marginLeft: 0 }]}>Story Book</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[styles.termsModalNew, { borderBottomWidth: 0 }]}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({
                                    modalOpen: false,
                                    selectedExperienceType: 'FUNDRAISE',
                                    experienceList: []
                                }, () => this.getAllEvents(this.state.userId))
                            }}>
                            <Text style={[styles.modalText, { marginLeft: 0 }]}>Fund Raise</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ paddingBottom: 60 }}>
                <View style={styles.header}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity style={{ width: 15 }} onPress={() => this.props.navigation.navigate("ProfileScreen")}>
                            <Icon name="Arrow-Left" size={14} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                        </TouchableOpacity>
                        <Text style={{ color: '#154A59', fontFamily: 'Montserrat-SemiBold', fontSize: 14, marginLeft: 15 }}>
                            EXPERIENCE
                        </Text>
                    </View>

                    <TouchableOpacity onPress={() => this.setState({ modalOpen: true })} activeOpacity={0.5} style={{ flexDirection: 'row', width: 'auto', height: 31, borderRadius: 20, backgroundColor: '#D9E1E4', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 10 }} >
                        {this.state.selectedExperienceType === 'ASSIGNMENTEVENTTRAINING' ? <Text style={{ color: '#367681', fontSize: 12, fontFamily: 'Montserrat-SemiBold' }}>ALL</Text> : <Text style={{ color: '#367681', fontSize: 12, fontFamily: 'Montserrat-SemiBold' }}>{this.state.selectedExperienceType}</Text>}
                        <Icon name='Arrow2_Down' size={14} color="#367681" style={Platform.OS === 'android' ? { marginTop: 8 } : { marginTop: 0 }} />
                    </TouchableOpacity>
                </View>

                {this.modal()}

                {this.state.experienceList && this.state.experienceList.length === 0 ?
                    <Text
                        onPress={() => {
                            this.props.navigation.navigate('ProfileEditStack', { screen: 'ExperienceEdit' })
                        }}
                        style={{ textAlign: 'center', color: '#97a600', marginHorizontal: 10, marginTop: '60%' }}>Update your experience. It will help you explore relevant opportunities, enable discovery by other users, and to know you better.</Text>
                    :
                    this.state.experienceList ?
                        <FlatList
                            // onEndReached={() => this.props.personalExperienceRequest({ userId: this.state.userId, otherUserId: '' })}
                            // onEndReachedThreshold={5}
                            contentContainerStyle={{ paddingTop: 6, marginLeft: '2%' }}
                            keyExtractor={(item) => this.state.selectedExperienceType === 'JOB' ? item.id : item.project.id}
                            data={this.state.experienceList}
                            renderItem={({ item, index }) =>
                                this.state.selectedExperienceType === 'JOB' ? this.renderJobItem(item, index) : this.renderItem(item, index)
                            }
                        />
                        :
                        <ActivityIndicator size='small' color="#00394D" />
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6
    },
    termsModalNew: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '85%',
        paddingVertical: 11,
        borderBottomWidth: 0.55,
        borderBottomColor: '#E8ECEB',
    },
    linearGradientView: {
        width: '100%',
        height: 500,
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center'
    },
    termsmodal: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '85%',
        paddingVertical: 11,
        borderBottomWidth: 0.55,
        borderBottomColor: '#E8ECEB'
    },
    modalText: {
        fontSize: 14,
        color: '#154A59',
        fontFamily: 'Montserrat-SemiBold'
    },
    modalCategoriesContainer: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        paddingVertical: 20,
        height: 250
    },
    crossIcon: {
        marginTop: 5
    },
    crossButtonContainer: {
        alignSelf: 'center',
        width: 42,
        height: 42,
        borderRadius: 21,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E7F3E3',
        marginBottom: 10
    },
    modalCountry: {
        marginTop: 'auto'
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    experienceCard: {
        width: '90%',
        marginVertical: 10,
        backgroundColor: '#fff',
        borderRadius: 8,
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    expCardHeader: {
        flexDirection: 'row',
        marginTop: 5,
        height: 70,
        alignItems: 'center',
        borderBottomColor: '#90949C',
        borderBottomWidth: 0.5,
        marginHorizontal: 15
    },
    expTitle: {
        marginLeft: 12,
        color: '#00394D',
        fontSize: 14,
        fontFamily: 'Montserrat-Bold'
    },
    expCompanyName: {
        width: 200,
        marginLeft: 12,
        color: '#698F8A',
        fontSize: 12,
        fontFamily: 'Montserrat-SemiBold'
    },
    expLocation: {
        marginLeft: 12,
        color: '#90949C',
        fontSize: 12,
        fontFamily: 'Montserrat-Medium'
    },
    expDescription: {
        flexWrap: 'wrap',
        color: '#607580',
        fontSize: 12,
        margin: 15
    }
})

const mapStateToProps = (state) => {
    return {
        userExperienceProgress: state.personalProfileReducer.userExperienceProgress,
        userExperience: state.personalProfileReducer.userExperience,
        errorExperience: state.personalProfileReducer.errorExperience,
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        personalExperienceRequest: (data) => dispatch(personalExperienceRequest(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeeallExperience)