import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, ActivityIndicator, SafeAreaView } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'
import LinearGradient from 'react-native-linear-gradient'

import { causesRequest } from '../../../services/Redux/Actions/User/CausesActions'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultCover from '../../../../assets/defaultCover.png'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultStyle from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

class SeeallCauses extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {

            this.props.route.params && this.props.route.params.id !== '' ?
                this.props.causesRequest({ userId: this.props.route.params.id, otherUserId: value })
                : this.props.causesRequest({ userId: value, otherUserId: '' })

        }).catch((e) => {
            console.log(e)
        })

    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.userCauses !== this.props.userCauses) {
            return true
        }
        return false
    }

    render() {
        return (
            <SafeAreaView>

                <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 12 }] : styles.header}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.navigate("ProfileScreen")}>
                            <Icon name="Arrow-Left" size={14} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                        </TouchableOpacity>
                        <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_800, marginLeft: 5 }]}>
                            CAUSES SUPPORTED
                        </Text>
                    </View>
                </View>

                {this.props.userCauses.body ?
                    <FlatList
                        contentContainerStyle={styles.causesFlatList}
                        showsHorizontalScrollIndicator={false}
                        alwaysBounceHorizontal={false}
                        keyExtractor={(item) => item.id}
                        data={this.props.userCauses.body.causeList.content}
                        renderItem={({ item, index }) => {

                            return <TouchableOpacity onPress={() => this.props.navigation.navigate('CausesStack', {
                                screen: 'CausesDetail',
                                params: { circleData: JSON.stringify(item), id: item.id },
                            })}
                                style={styles.individualCause}
                                activeOpacity={0.8}>
                                {item.imageUrl ?
                                    <Image source={{ uri: item.imageUrl }} style={styles.causesImage} />
                                    : <Image source={defaultCover} style={styles.causesImage} />}

                                <View style={styles.linearGradientView}>
                                    <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={styles.linearGradient}>
                                    </LinearGradient>
                                </View>

                                <Text style={styles.causesText}>{item.name}</Text>

                            </TouchableOpacity>

                        }}
                    />
                    :
                    <ActivityIndicator size='small' color="#00394D" />
                }
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6
    },

    linearGradientView: {
        width: '90%',
        height: 80,
        position: 'absolute',
        bottom: 12
    },
    causesText: {
        color: '#FFFFFF',
        fontSize: 18,
        fontFamily: 'Montserrat-SemiBold',
        position: 'absolute',
        bottom: 20,
        zIndex: 2
    },
    causesImage: {
        width: '90%',
        height: 160,
        borderRadius: 6,
        marginVertical: 12
    },
    individualCause: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    seeAllText: {
        color: '#698F8A',
        fontSize: 11,
        fontFamily: 'Montserrat-Medium'
    },
    seeAll: {
        marginHorizontal: 16,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        height: 26,
        width: 70,
        borderWidth: 1,
        borderColor: '#698F8A',
        borderRadius: 13,
        paddingHorizontal: 6,
        paddingVertical: 2
    },
    causesFlatList: {
        paddingTop: 6,
        paddingBottom: 46
    },
    causesSupportedText: {
        color: '#154A59',
        fontSize: 14,
        fontFamily: 'Montserrat-Bold',
        marginLeft: 8
    },
    causesSupported: {
        backgroundColor: '#E7F3E3',
        paddingVertical: 20,
        paddingLeft: 16
    },


    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }
})

const mapStateToProps = (state) => {
    return {
        userCausesProgress: state.causesReducer.userCausesProgress,
        userCauses: state.causesReducer.userCauses,
        errorCauses: state.causesReducer.errorCauses
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        causesRequest: (data) => dispatch(causesRequest(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeeallCauses)
