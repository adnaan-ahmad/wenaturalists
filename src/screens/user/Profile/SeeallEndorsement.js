import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Platform,
    FlatList,
    ActivityIndicator,
    Image,
    ScrollView,
    SafeAreaView
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import Autolink from 'react-native-autolink'

import BusinessDefault from '../../../../assets/BusinessDefault.png'
import typography from '../../../Components/Shared/Typography'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultProfile from '../../../../assets/defaultProfile.png'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultCover from '../../../../assets/defaultCover.png'
import { endorsementRequest, endorsementDetailRequest } from '../../../services/Redux/Actions/User/EndorsementActions'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

class SeeallEndorsement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            readMoreEndorsementId: '',
            // selectedEndorsementType: this.props.userEndorsement.body ? this.props.userEndorsement.body[0].topic : ''
            selectedEndorsementType: this.props.route.params.topic ? this.props.route.params.topic :
                this.props.userEndorsement.body ? this.props.userEndorsement.body[0].topic : ''
        };
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value }, () => {
                this._unsubscribe = this.props.navigation.addListener('focus', () => {

                    if (this.props.route.params && this.props.route.params.id !== '') {
                        this.props.endorsementRequest({ userId: this.props.route.params.id, otherUserId: value })
                        this.props.endorsementDetailRequest({ userId: this.props.route.params.id, topic: this.state.selectedEndorsementType })
                    }
                    else {
                        this.props.endorsementRequest({ userId: value, otherUserId: '' })
                        this.props.endorsementDetailRequest({ userId: value, topic: this.state.selectedEndorsementType })
                    }
                })
            })
            if (this.props.route.params && this.props.route.params.id !== '') {
                this.props.endorsementRequest({ userId: this.props.route.params.id, otherUserId: value })
                this.props.endorsementDetailRequest({ userId: this.props.route.params.id, topic: this.state.selectedEndorsementType })
            }
            else {
                this.props.endorsementRequest({ userId: value, otherUserId: '' })
                this.props.endorsementDetailRequest({ userId: value, topic: this.state.selectedEndorsementType })
            }

        }).catch((e) => {
            console.log(e)
        })

    }

    unixTime = (UNIX_timestamp) => {
        var date = new Date(UNIX_timestamp)
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        var year = date.getFullYear()
        var month = months[date.getMonth()]
        var day = date.getDate()
        return day + ' ' + month + ' ' + year
    }

    hideEndorsement(data) {
        let postBody = {
            "endorsementId": data.endorsementId
        };
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/endorsement/hide',
            headers: { 'Content-Type': 'application/json' },
            data: postBody,
            withCredentials: true
        }).then((response) => {
            let res = response.data
            // console.log(res.status)
            if (res.status === '201 CREATED') {

                if (this.props.route.params && this.props.route.params.id !== '') {
                    this.props.endorsementRequest({ userId: this.props.route.params.id, otherUserId: this.state.userId })
                    this.props.endorsementDetailRequest({ userId: this.props.route.params.id, topic: this.state.selectedEndorsementType })
                }
                else {
                    this.props.endorsementRequest({ userId: this.state.userId, otherUserId: '' })
                    this.props.endorsementDetailRequest({ userId: this.state.userId, topic: this.state.selectedEndorsementType })
                }

            }

        }).catch((err) => {

            if (err && err.response && err.response.data) {
                // console.log(err.response.data.message)
            }
        })

    }

    trimDescription = (item) => {
        item = item.replace(/&nbsp;/g, ' ');
        item = item.replace(/<br\s*[\/]?>/gi, '\n');

        const regex = /(<([^>]+)>)/gi;
        item = item.replace(regex, '');

        return item.split('^^__').join(' ');
    }

    renderDescription = (description, id) => {

        return (
            <Autolink
                text={description && this.trimDescription(description)}
                email
                hashtag="instagram"
                mention="twitter"
                phone="sms"
                numberOfLines={
                    this.state.readMoreEndorsementId !== id ? 3 : 20
                }
                style={[
                    typography.Body_1,
                    {
                        fontSize: 11,
                        color: COLORS.dark_700,
                        marginLeft: 6,
                        marginTop: 8,
                        marginBottom: 20
                    }
                ]}
                url
            />
        )
    }

    renderItem = (item) => {
        return (
            <View style={styles.experienceCard}>

                <TouchableOpacity
                    onPress={() =>
                        // console.log(item)
                        item.userType === 'COMPANY' ?
                            this.props.navigation.navigate('CompanyProfileScreen', { userId: item.endorseBy }) :
                            item.endorseBy !== this.state.userId ?
                                this.props.navigation.navigate('OtherProfileScreen', { userId: item.endorseBy }) :
                                this.props.navigation.navigate('ProfileScreen')
                    }
                    style={{ paddingHorizontal: 10, paddingTop: 5, backgroundColor: '#E7F3E3', borderTopLeftRadius: 6, borderBottomLeftRadius: 6, width: 20, justifyContent: 'space-between', alignItems: 'center' }}>
                    {item.imageUrl !== null ?
                        <Image source={{ uri: item.imageUrl }} style={{ width: 36, height: 36, borderRadius: 18 }} />
                        :
                        <Image source={item.userType === 'COMPANY' ? BusinessDefault : defaultProfile} style={{ width: 36, height: 36, borderRadius: 18 }} />
                    }
                </TouchableOpacity>

                <View style={styles.expCardHeader}>

                    <View style={{ marginRight: 10, paddingTop: 10, backgroundColor: '#FFF', alignSelf: 'flex-start' }}>
                        <Text onPress={() =>
                            item.userType === 'COMPANY' ?
                                this.props.navigation.navigate('CompanyProfileScreen', { userId: item.endorseBy }) :
                                item.endorseBy !== this.state.userId ?
                                    this.props.navigation.navigate('OtherProfileScreen', { userId: item.endorseBy }) :
                                    this.props.navigation.navigate('ProfileScreen')
                        } numberOfLines={1} style={styles.expTitle}>{item.username}</Text>
                        {this.renderDescription(item.description, item.endorsementId)}

                        <TouchableOpacity
                            style={{
                                height: 30,
                                width: 80,
                                marginLeft: 6,
                                marginTop: this.state.readMoreEndorsementId === item.endorsementId ? -50 : -18,
                                marginBottom: 16,
                            }}
                            onPress={() =>
                                this.setState({
                                    readMoreEndorsementId:
                                        this.state.readMoreEndorsementId.length > 0
                                            ? ''
                                            : item.endorsementId,
                                })
                            }>

                            <Text style={{ color: COLORS.green_500, fontWeight: '700', fontSize: 12 }}>
                                {item.description.length > 105 &&
                                    this.state.readMoreEndorsementId !== item.endorsementId
                                    ? 'Read more'
                                    : this.state.readMoreEndorsementId === item.endorsementId
                                        ? 'Read less'
                                        : null}
                            </Text>

                        </TouchableOpacity>
                    </View>

                </View>

                {!this.props.route.params.id &&
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={[defaultShape.Nav_Gylph_Btn, { position: 'absolute', right: 6 }]}
                        onPress={() => this.hideEndorsement(item)}>
                        <Icon
                            name="Hide"
                            color={COLORS.altgreen_300}
                            size={14}
                            style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                        />
                    </TouchableOpacity>}


            </View>
        )
    }

    render() {
        return (
            <SafeAreaView>
                <ScrollView style={{ paddingBottom: 30 }}>
                    <View style={styles.header}>

                        <View style={Platform.OS === 'ios' ? { flexDirection: 'row', alignItems: 'center', alignSelf: 'flex-start', paddingVertical: 12 } : { flexDirection: 'row', alignItems: 'center', alignSelf: 'flex-start' }}>
                            <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack()}>
                                <Icon name="Arrow-Left" size={14} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                            </TouchableOpacity>
                            <Text style={{ color: '#154A59', fontFamily: 'Montserrat-SemiBold', fontSize: 14, marginLeft: 15 }}>
                                ENDORSEMENT
                            </Text>
                        </View>

                        <View style={{ borderColor: '#E8ECEB', borderWidth: 0.45, width: '108%', marginVertical: 4, alignSelf: 'center' }}></View>

                        {this.props.userEndorsement.body ?
                            <FlatList
                                style={{ paddingTop: 15, alignSelf: 'flex-start' }}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                keyExtractor={(item) => item.topic}
                                data={this.props.userEndorsement.body}
                                renderItem={({ item, index }) => (
                                    <TouchableOpacity activeOpacity={0.6} onPress={() => this.setState({ selectedEndorsementType: item.topic },
                                        () => this.props.endorsementDetailRequest({ userId: this.props.route.params && this.props.route.params.id !== '' ? this.props.route.params.id : this.state.userId, topic: this.state.selectedEndorsementType }))}
                                        style={styles.endorsementTypes}>
                                        {item.topic && item.count ? <Text style={this.state.selectedEndorsementType === item.topic ? styles.endorsementSelectedType : styles.endorsementUnSelectedType}>{item.topic}</Text> : <></>}
                                        {item.topic && item.count ? <Text style={this.state.selectedEndorsementType === item.topic ? styles.endorsementSelectedCount : styles.endorsementUnSelectedCount}>{item.count} Endorsement</Text> : <></>}
                                        {this.state.selectedEndorsementType === item.topic ? <View style={styles.borderBottom}></View> : <></>}
                                    </TouchableOpacity>
                                )}
                            />
                            :
                            <ActivityIndicator size='small' color="#00394D" />
                        }

                    </View>

                    {this.props.userEndorsementDetail.body ?
                        <FlatList
                            style={{ paddingTop: 15 }}
                            keyExtractor={(item) => item.endorsementId}
                            data={this.props.userEndorsementDetail.body.content}
                            renderItem={({ item }) => (
                                this.renderItem(item)
                            )}
                        />
                        :
                        <ActivityIndicator size='small' color="#00394D" style={{ marginTop: 50 }} />
                    }
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        // flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingTop: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    borderBottom: {
        width: 120,
        height: 3,
        backgroundColor: '#154A59',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4
    },
    endorsementTypes: {
        marginHorizontal: 10,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    endorsementSelectedType: {
        color: '#154A59',
        fontSize: 12,
        fontFamily: 'Montserrat-Bold'
    },
    endorsementUnSelectedType: {
        color: '#91B3A2',
        fontSize: 12,
        fontFamily: 'Montserrat-SemiBold'
    },
    endorsementUnSelectedCount: {
        color: '#99B2BF',
        fontSize: 10,
        fontFamily: 'Montserrat-Medium',
        textAlign: 'center',
        marginBottom: 10
    },
    endorsementSelectedCount: {
        color: '#698F8A',
        fontSize: 10,
        fontFamily: 'Montserrat-Medium',
        textAlign: 'center',
        marginBottom: 8
    },
    experienceCard: {
        flexDirection: 'row',
        width: '90%',
        // height: 'auto',
        marginVertical: 10,
        backgroundColor: '#fff',
        borderRadius: 8,
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    expCardHeader: {
        flexDirection: 'row',
        // height: 70,
        alignItems: 'center',
        // backgroundColor: 'orange',
        marginHorizontal: 20,
        paddingBottom: 26
    },
    expTitle: {
        marginLeft: 12,
        color: '#00394D',
        fontSize: 14,
        fontFamily: 'Montserrat-SemiBold',
        maxWidth: 200
    },
    expCompanyName: {
        width: 200,
        marginLeft: 12,
        color: '#698F8A',
        fontSize: 12,
        fontFamily: 'Montserrat-SemiBold'
    },
    expLocation: {
        marginLeft: 12,
        marginTop: 6,
        color: '#698F8A',
        fontSize: 11,
        fontFamily: 'Montserrat-Medium'
    },
    expDescription: {
        flexWrap: 'wrap',
        color: '#99B2BF',
        fontSize: 10,
        fontFamily: 'Montserrat-Medium',
        position: 'absolute',
        right: 14,
        top: 12
    }
})

const mapStateToProps = (state) => {
    return {
        userExperienceProgress: state.personalProfileReducer.userExperienceProgress,
        userExperience: state.personalProfileReducer.userExperience,
        errorExperience: state.personalProfileReducer.errorExperience,

        userEndorsementProgress: state.endorsementReducer.userEndorsementProgress,
        userEndorsement: state.endorsementReducer.userEndorsement,
        errorEndorsement: state.endorsementReducer.errorEndorsement,

        userEndorsementDetailProgress: state.endorsementReducer.userEndorsementDetailProgress,
        userEndorsementDetail: state.endorsementReducer.userEndorsementDetail,
        errorEndorsementDetail: state.endorsementReducer.errorEndorsementDetail,
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        personalExperienceRequest: (data) => dispatch(personalExperienceRequest(data)),
        endorsementRequest: (data) => dispatch(endorsementRequest(data)),
        endorsementDetailRequest: (data) => dispatch(endorsementDetailRequest(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeeallEndorsement)
