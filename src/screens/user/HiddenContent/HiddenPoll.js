import React, {Component} from 'react';
import {
  Switch,
  FlatList,
  ScrollView,
  Image,
  TouchableOpacity,
  Platform,
  Text,
  View,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import hiddenContent from '../../../../assets/hiddenContent.webp';
import typography from '../../../Components/Shared/Typography';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class HiddenPoll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      showOnlyMyPost: false,
      pageNo: 0,
      pageSize: 6,
      pollList: [],
      isCompany: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      value &&
        this.setState({userId: value}, () => {
          this.getHiddenPollList();
          this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getHiddenPollList();
          });
        });
    });

    AsyncStorage.getItem('userData').then((value) => {
      let objValue = JSON.parse(value);
      objValue.type === 'COMPANY' && this.setState({isCompany: true});
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  getHiddenPollList = () => {
    let url;
    url =
      '/hidden/poll/list' +
      '?userId=' +
      this.state.userId +
      '&showOnlyMyPost=' +
      this.state.showOnlyMyPost +
      '&page=' +
      this.state.pageNo +
      '&size=' +
      this.state.pageSize;
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    }).then((response) => {
      let res = response.data;
      if (res.message === 'Success!') {
        this.setState({
          pollList: this.state.pollList.concat(response.data.body.page.content),
        });
      }
    });
  };

  handleLoadMore = () => {
    this.setState({pageNo: this.state.pageNo + 1}, () =>
      this.getHiddenPollList(),
    );
  };

  handleUnHideModal = (id) => {
    let data = {
      userId: this.state.userId,
      activityId: id,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/unhide',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          console.log(response.status);
          this.getHiddenPollList();
        } else {
          console.log(response.status);
          // this.setState({'isSubmitted': false})
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          console.log(err.response.status);
          // this.setState({'error': 'Entity has already been unhidden', 'isSubmitted': false})
        } else {
          console.log(err.response.status);
          // this.setState({'isSubmitted': false})
        }
      });
  };

  handleFollowUnfollow = (isFollowed, userId, item) => {
    // let tempLikeList = cloneDeep(this.state.peopleLiked);
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        console.log(response.status);
        if (response && response.status === 202) {
          this.setState({pollList: [], pageNo: 0}, () =>
            this.getHiddenPollList(),
          );
          // tempLikeList[index].followed = !item.followed;
          // this.setState({ peopleLiked: tempLikeList });
        } else {
          console.log(response);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderPollItem = (item) => {
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate('PollStack', {
            screen: 'PollDetails',
            params: {
              slug: item.item.slug,
              userId: this.state.userId,
              hidden: true,
            },
          })
        }
        activeOpacity={0.7}
        style={styles.forumItemView}>
        <Text
          style={[
            typography.Title_2,
            {color: COLORS.dark_800, fontSize: 16, maxWidth: '90%'},
          ]}
          numberOfLines={1}>
          {item.item.question}
          {/* This is a very very big project title This is a very very big project title  */}
        </Text>

        <View
          style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
          <Image
            source={
              item.item.profileImage
                ? {uri: item.item.profileImage}
                : item.item.userType === 'COMPANY'
                ? defaultBusiness
                : defaultProfile
            }
            style={styles.profileImage}
          />
          <Text
            numberOfLines={1}
            style={[
              typography.Caption,
              {color: COLORS.dark_600, maxWidth: '70%'},
            ]}>
            {item.item.userName}
            {/* This is a very very big project title This is a very very big project title  */}
          </Text>

          {item.item.userId !== this.state.userId && !this.state.isCompany && (
            <TouchableOpacity
              onPress={() =>
                this.handleFollowUnfollow(
                  item.item.followed,
                  item.item.userId,
                  // index,
                  item.item,
                )
              }
              style={{
                height: 30,
                width: 30,
                alignItems: 'center',
                marginLeft: 0,
              }}>
              <Icon
                name={item.item.followed ? 'TickRSS' : 'RSS'}
                size={13}
                color={COLORS.dark_600}
                style={{marginTop: Platform.OS === 'android' ? 5 : 8}}
              />
            </TouchableOpacity>
          )}
        </View>

        <TouchableOpacity
          activeOpacity={0.5}
          style={[
            defaultShape.Nav_Gylph_Btn,
            {position: 'absolute', right: 0, top: -4},
          ]}
          onPress={() => this.handleUnHideModal(item.item.id)}>
          <Icon
            name="Eye_OL"
            color={COLORS.altgreen_300}
            size={14}
            style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
          />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  listHeaderComponent = () => {
    return (
      <>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginLeft: 20,
            marginVertical: 10,
          }}>
          <Text
            style={[
              typography.Caption,
              {color: COLORS.dark_600, fontSize: 13, marginRight: 4},
            ]}>
            Show only my posts
          </Text>

          <Switch
            trackColor={{false: '#767577', true: '#dadd21'}}
            thumbColor={this.state.showOnlyMyPost ? '#00394d' : '#f4f3f4'}
            ios_backgroundColor="#3e3e3e"
            onValueChange={() => {
              this.setState(
                {
                  showOnlyMyPost: !this.state.showOnlyMyPost,
                  pollList: [],
                  pageNo: 0,
                },
                () => this.getHiddenPollList(),
              );
            }}
            value={this.state.showOnlyMyPost}
          />
        </View>
        {!this.state.pollList.length ? (
          <Text
            style={[
              typography.Title_2,
              {
                color: COLORS.dark_800,
                fontSize: 16,
                alignSelf: 'center',
                marginTop: 20,
              },
            ]}>
            You have no Hidden Activities
          </Text>
        ) : (
          <></>
        )}
      </>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View
          style={
            Platform.OS === 'ios'
              ? [styles.header, {paddingVertical: 20}]
              : styles.header
          }>
          <TouchableOpacity
            style={{
              width: 40,
              height: 30,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => this.props.navigation.goBack()}>
            <Icon
              name="Arrow-Left"
              size={15}
              color="#91B3A2"
              style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
            />
          </TouchableOpacity>
          <Text style={[typography.H3, {color: COLORS.dark_800, fontSize: 22}]}>
            Poll
          </Text>
          <View
            style={{
              width: 40,
              height: 30,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Icon
              name="Arrow-Left"
              size={15}
              color="#FFF"
              style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
            />
          </View>
        </View>

        <FlatList
          showsVerticalScrollIndicator={false}
          style={{paddingTop: 10}}
          contentContainerStyle={{paddingBottom: 30}}
          data={this.state.pollList}
          ListHeaderComponent={this.listHeaderComponent()}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={1}
          keyExtractor={(item) => item.id}
          renderItem={(item) => this.renderPollItem(item)}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#FFF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  forumItemView: {
    width: '90%',
    marginTop: 15,
    backgroundColor: COLORS.white,
    borderRadius: 4.5,
    alignSelf: 'center',
    paddingHorizontal: 15,
    paddingVertical: 14,
  },
  profileImage: {
    height: 24,
    width: 24,
    borderRadius: 12,
    marginRight: 8,
  },
});
