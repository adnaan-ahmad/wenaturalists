import React, { Component } from 'react'
import { ScrollView, Image, TouchableOpacity, Platform, Text, View, SafeAreaView, StyleSheet } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultProfile from '../../../../assets/defaultProfile.png'
import hiddenContent from '../../../../assets/hiddenContent.webp'
import typography from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class HiddenContent extends Component {
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView>

          <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 20 }] : styles.header}>

            <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack()}>
              <Icon name="Arrow-Left" size={15} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
            </TouchableOpacity>
            <Text style={[typography.H3, { color: COLORS.dark_800, fontSize: 22 }]}>
              Hidden Activities
            </Text>
            <View style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="Arrow-Left" size={15} color="#FFF" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
            </View>

          </View>

          <View
            style={{ flexDirection: 'row', width: '90%', alignSelf: 'center', backgroundColor: '#eee', flexWrap: 'wrap', justifyContent: 'space-evenly', marginTop: 20, borderRadius: 8, paddingVertical: 16 }}>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('HiddenFeeds')}
              style={{ width: '40%', height: 120, backgroundColor: '#FFF', alignItems: 'center', justifyContent: 'center', borderRadius: 8, marginBottom: 16 }}>
              <Icon name="WN_Feeds_OL" size={30} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 0 : 0 }} />
              <Text style={[typography.Body_1_bold, { color: COLORS.dark_800, fontSize: 13, marginTop: Platform.OS === 'ios' ? 0 : -24 }]}>Feeds</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('HiddenProjects')}
              style={{ width: '40%', height: 120, backgroundColor: '#FFF', alignItems: 'center', justifyContent: 'center', borderRadius: 8, marginBottom: 16 }}>
              <Icon name="Projects_OL" size={30} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 0 : 0 }} />
              <Text style={[typography.Body_1_bold, { color: COLORS.dark_800, fontSize: 13, marginTop: Platform.OS === 'ios' ? 0 : -24 }]}>Projects</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('HiddenForum')}
              style={{ width: '40%', height: 120, backgroundColor: '#FFF', alignItems: 'center', justifyContent: 'center', borderRadius: 8 }}>
              <Icon name="WN_Forum_OL" size={30} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 0 : 0 }} />
              <Text style={[typography.Body_1_bold, { color: COLORS.dark_800, fontSize: 13, marginTop: Platform.OS === 'ios' ? 0 : -24 }]}>Forum</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('HiddenPoll')}
              style={{ width: '40%', height: 120, backgroundColor: '#FFF', alignItems: 'center', justifyContent: 'center', borderRadius: 8 }}>
              <Icon name="Polls" size={30} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 0 : 0 }} />
              <Text style={[typography.Body_1_bold, { color: COLORS.dark_800, fontSize: 13, marginTop: Platform.OS === 'ios' ? 0 : -24 }]}>Poll</Text>
            </TouchableOpacity>
          </View>

          <View
            style={{ width: '90%', alignSelf: 'center', backgroundColor: 'rgb(208, 232, 200)', marginTop: 20, borderRadius: 8, paddingVertical: 16, paddingLeft: 20, paddingRight: 26 }}>

            <Text style={[typography.H3, { color: COLORS.dark_800, fontSize: 20 }]}>
              What are Hidden Activities?
            </Text>

            <Text style={[typography.Subtitle_2, { color: COLORS.dark_800, marginTop: 6 }]}>
              You can hide activities that you may not want to see or be visible in your profile to others . You may unhide whenever you want to.
            </Text>

          </View>

          <Image source={hiddenContent} style={{ height: 200, width: '60%', alignSelf: 'center', marginTop: 10 }} />
        </ScrollView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingVertical: 20,
    backgroundColor: '#FFF',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  }
})
