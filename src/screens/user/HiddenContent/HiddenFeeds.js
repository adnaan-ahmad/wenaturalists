import React, { Component } from 'react'
import { Switch, FlatList, Image, TouchableOpacity, Platform, Text, View, SafeAreaView, StyleSheet } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

import { REACT_APP_userServiceURL } from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import typography from '../../../Components/Shared/Typography'
import projectDefault from '../../../../assets/project-default.jpg'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const moment = require('moment')

export default class HiddenFeeds extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            showOnlyMyPost: false,
            pageNo: 0,
            pageSize: 500,
            forumList: [],
            selected: 'ARTICLES'
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('userId').then((value) => {
            value && this.setState({ userId: value })
            value && this.getHiddenFeedsList()
        })
    }

    getHiddenFeedsList() {
        let url
        url = '/hidden/post/list?userId=' + this.state.userId + '&newsFeedType=' + this.state.selected + '&showOnlyMyPost=' + this.state.showOnlyMyPost + '&page=' + this.state.pageNo + '&size=' + this.state.pageSize,
            axios({
                method: 'get',
                url: REACT_APP_userServiceURL + url,
                headers: { 'Content-Type': 'application/json' },
                withCredentials: true
            }).then((response) => {
                let res = response.data;
                if (response.data.status === '200 OK') {
                    console.log('getHiddenFeedsList', response.data.body.content[0])
                    this.setState({ forumList: response.data.body.content })
                }
            })

    }

    renderThumbNail = (item) => {

        if (this.state.selected === 'ARTICLES') return (
            <Image
                source={
                    item.attachmentIds[0] && item.attachmentIds[0].attachmentUrl
                        ? { uri: item.attachmentIds[0].attachmentUrl }
                        : { uri: 'https://cdn.dscovr.com/images/banner-explore-blog-small.webp' }
                }
                style={styles.profileImage}
            />
        )

        if (this.state.selected === 'PHOTOS') return (
            <Image
                source={
                    item.attachmentIds[0] && item.attachmentIds[0].attachmentUrl
                        ? { uri: item.attachmentIds[0].attachmentUrl }
                        : projectDefault
                }
                style={styles.profileImage}
            />
        )

        if (this.state.selected === 'VIDEOS') return (
            <Image
                source={
                    item.attachmentIds[0] && item.attachmentIds[0].thumbnails
                        ? { uri: item.attachmentIds[0].thumbnails[0] }
                        : projectDefault
                }
                style={styles.profileImage}
            />
        )
        if (this.state.selected === 'AUDIO') return (
            <View style={[styles.profileImage, { justifyContent: 'center', alignItems: 'center', backgroundColor: '#d4dde0' }]}>
                <Icon name="Audio" size={23} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
            </View>
        )
        if (this.state.selected === 'TOP') {
            return (
                <View style={[styles.profileImage, { justifyContent: 'center', alignItems: 'center', backgroundColor: '#d4dde0' }]}>
                    <Icon name={this.iconNames(item)} size={23} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                </View>
            )
        }

    }

    iconNames = (item) => {
        if (item.postType === 'LINK') return 'Link'
        if (item.postType === 'POST' && !item.sharedEntityId) return 'Img'
        if (item.postType === 'POST' && item.sharedEntityId) return 'Share'
        if (item.postType === 'POLL') return 'Polls'
        if (item.postType === 'FORUM') return 'WN_Forum_OL'
    }

    handleUnHideModal = (id) => {
        let data = {
            userId: this.state.userId,
            activityId: id
        }

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/hidden/unhide',
            data: data,
            withCredentials: true
        }).then(response => {
            if (response && response.status === 202) {
                console.log(response.status)
                this.getHiddenFeedsList()
            } else {
                console.log(response.status)
                // this.setState({'isSubmitted': false})
            }
        }).catch((err) => {
            if (err && err.response && err.response.status === 409) {
                console.log(err.response.status)
                // this.setState({'error': 'Entity has already been unhidden', 'isSubmitted': false})
            } else {
                console.log(err.response.status)
                // this.setState({'isSubmitted': false})
            }
        })
    }

    navigateWindowControl = (item) =>{
        if(this.state.selected === "ARTICLES"){
            this.props.navigation.navigate('EditorsDesk',{
                id:item.item.id,
            })
        }else{
            this.props.navigation.navigate('IndividualFeedsPost',{
                id:item.item.id,
            })
        }
    }

    renderFeedsItem = (item) => {
        return (
            <TouchableOpacity
                onPress={() =>this.navigateWindowControl(item)}
                activeOpacity={0.7}
                style={styles.forumItemView}>



                <View style={{ flexDirection: 'row' }}>

                    {this.renderThumbNail(item.item)}

                    <View style={{ justifyContent: 'space-evenly', paddingTop: 6 }}>
                        <Text
                            style={[typography.Title_2, { color: COLORS.dark_800, fontSize: 15, maxWidth: '81%' }]}
                            numberOfLines={1}>
                            {item.item.userName ? item.item.userName :
                                item.item.params && item.item.params.circleTitle ? item.item.params.circleTitle : null
                            }
                            {/* This is a very very big project title This is a very very big project title */}
                        </Text>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            {item.item.country && <Icon name="Location" size={13} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />}
                            {item.item.country && <Text numberOfLines={1}
                                style={[
                                    typography.Caption,
                                    { color: COLORS.dark_600, marginLeft: 3, maxWidth: '46%' },
                                ]}>
                                {item.item.country}
                                {/* Republic Of Cairo Egypt */}
                            </Text>}

                            {item.item.country && <Icon
                                name="Bullet_Fill"
                                color={COLORS.altgreen_300}
                                size={10}
                                style={{ marginTop: Platform.OS === 'android' ? 8 : 0, marginLeft: 4 }}
                            />}
                            <Text
                                style={[
                                    typography.Body_2,
                                    { color: '#000', marginLeft: 3 },
                                ]}>
                                {moment.unix(item.item.createTime / 1000).format('Do MMM, YYYY')}
                            </Text>
                        </View>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={[defaultShape.Nav_Gylph_Btn, { position: 'absolute', right: -16, top: -4 }]}
                        onPress={() => this.handleUnHideModal(item.item.id) }>
                        <Icon
                            name="Eye_OL"
                            color={COLORS.altgreen_300}
                            size={14}
                            style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                        />
                    </TouchableOpacity>

                </View>

            </TouchableOpacity>
        )
    }

    listHeaderComponent = () => {
        return (
            <>

                <View style={{ alignSelf: 'center', flexDirection: 'row', backgroundColor: COLORS.altgreen_t50, width: 350, marginTop: 20, marginBottom: 10, justifyContent: 'center', borderRadius: 4 }}>
                    <TouchableOpacity onPress={() => this.setState({ selected: 'ARTICLES' }, () => this.getHiddenFeedsList())}
                        style={[defaultShape.InTab_Btn, { backgroundColor: this.state.selected === 'ARTICLES' ? '#fff' : COLORS.altgreen_t50, width: 70, borderTopLeftRadius: 4, borderBottomLeftRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: this.state.selected === 'ARTICLES' ? COLORS.dark_800 : COLORS.altgreen_400 }]}>Blogs</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ selected: 'PHOTOS' }, () => this.getHiddenFeedsList())}
                        style={[defaultShape.InTab_Btn, { backgroundColor: this.state.selected === 'PHOTOS' ? '#fff' : COLORS.altgreen_t50, width: 70 }]}>
                        <Text style={[typography.Caption, { color: this.state.selected === 'PHOTOS' ? COLORS.dark_800 : COLORS.altgreen_400 }]}>Photos</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ selected: 'VIDEOS' }, () => this.getHiddenFeedsList())}
                        style={[defaultShape.InTab_Btn, { backgroundColor: this.state.selected === 'VIDEOS' ? '#fff' : COLORS.altgreen_t50, width: 70 }]}>
                        <Text style={[typography.Caption, { color: this.state.selected === 'VIDEOS' ? COLORS.dark_800 : COLORS.altgreen_400 }]}>Videos</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ selected: 'AUDIO' }, () => this.getHiddenFeedsList())}
                        style={[defaultShape.InTab_Btn, { backgroundColor: this.state.selected === 'AUDIO' ? '#fff' : COLORS.altgreen_t50, width: 70 }]}>
                        <Text style={[typography.Caption, { color: this.state.selected === 'AUDIO' ? COLORS.dark_800 : COLORS.altgreen_400 }]}>Audios</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ selected: 'TOP' }, () => this.getHiddenFeedsList())}
                        style={[defaultShape.InTab_Btn, { backgroundColor: this.state.selected === 'TOP' ? '#fff' : COLORS.altgreen_t50, width: 70, borderTopRightRadius: 4, borderBottomRightRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: this.state.selected === 'TOP' ? COLORS.dark_800 : COLORS.altgreen_400 }]}>Others</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20, marginVertical: 10 }}>
                    <Text
                        style={[
                            typography.Caption,
                            { color: COLORS.dark_600, fontSize: 13, marginRight: 4 }
                        ]}>
                        Show only my posts
                    </Text>

                    <Switch
                        trackColor={{ false: "#767577", true: "#dadd21" }}
                        thumbColor={this.state.showOnlyMyPost ? "#00394d" : "#f4f3f4"}
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={() => { this.setState({ showOnlyMyPost: !this.state.showOnlyMyPost }, () => this.getHiddenFeedsList()) }}
                        value={this.state.showOnlyMyPost}
                    />
                </View>

                {!this.state.forumList.length ? 
                <Text
                    style={[typography.Title_2, { color: COLORS.dark_800, fontSize: 16, alignSelf: 'center', marginTop: 20 }]}
                    >
                    You have no Hidden Activities
                </Text> 
                :
                <></>}
            </>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 20 }] : styles.header}>

                    <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack()}>
                        <Icon name="Arrow-Left" size={15} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                    </TouchableOpacity>
                    <Text style={[typography.H3, { color: COLORS.dark_800, fontSize: 22 }]}>
                        Feeds
                    </Text>
                    <View style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }}>
                        <Icon name="Arrow-Left" size={15} color="#FFF" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                    </View>

                </View>

                <FlatList
                    showsVerticalScrollIndicator={false}
                    style={{ paddingTop: 10 }}
                    data={this.state.forumList}
                    ListHeaderComponent={this.listHeaderComponent()}
                    contentContainerStyle={{ paddingBottom: 30 }}
                    // onEndReached={this.handleLoadMore}
                    // onEndReachedThreshold={1}
                    keyExtractor={(item) => item.id}
                    renderItem={(item) => this.renderFeedsItem(item)}
                />

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 20,
        backgroundColor: '#FFF',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    forumItemView: {
        width: '90%',
        marginTop: 15,
        backgroundColor: COLORS.white,
        borderRadius: 4.5,
        alignSelf: 'center',
        paddingRight: 15
    },
    profileImage: {
        height: 70,
        width: '22%',
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        marginRight: 14,
    },
})
