import React, { Component } from 'react'
import { Switch, FlatList, ScrollView, Image, TouchableOpacity, Platform, Text, View, SafeAreaView, StyleSheet } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'

import { REACT_APP_userServiceURL } from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import projectDefault from '../../../../assets/project-default.jpg'
import hiddenContent from '../../../../assets/hiddenContent.webp'
import typography from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class HiddenProjects extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            showOnlyMyPost: false,
            pageNo: 0,
            pageSize: 500,
            projectsList: [],
            completedProjectsList: [],
            selected: 'upcoming'
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('userId').then((value) => {
            value && this.setState({ userId: value })
            value && this.getOngoingProjectsList()
        })
    }

    getOngoingProjectsList() {
        let url
        url = this.state.selected === 'running' ?
            '/hidden/running-project/' + this.state.userId + '/' + this.state.userId + '/?showOnlyMyPost=' + this.state.showOnlyMyPost + "&filterType=timeline&isPinned=false&userActivityType=ALL&projectFilterType=ALL" + "&page=" + this.state.pageNo + "&size=" + this.state.pageSize
            : this.state.selected === 'completed' ?
                '/hidden/completed-projects/' + this.state.userId + '/' + this.state.userId + '/?showOnlyMyPost=' + this.state.showOnlyMyPost + "&filterType=timeline&isPinned=false&userActivityType=ALL&projectFilterType=ALL" + "&page=" + this.state.pageNo + "&size=" + this.state.pageSize
                : '/hidden/upcoming-projects/' + this.state.userId + '/' + this.state.userId + '/?showOnlyMyPost=' + this.state.showOnlyMyPost + "&filterType=timeline&isPinned=false&userActivityType=ALL&projectFilterType=ALL" + "&page=" + this.state.pageNo + "&size=" + this.state.pageSize
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + url,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then((response) => {
            let res = response.data;
            if (res.message === 'Success!') {
                // console.log('getOngoingProjectsList', response.data.body.content[1])
                this.setState({ projectsList: response.data.body.content })
            }
        })

    }

    // getCompletedProjectsList() {
    //     let url
    //     url = '/hidden/' + this.state.selected + '-project/' + this.state.userId + '/' + this.state.userId + '/?showOnlyMyPost=' + this.state.showOnlyMyPost + "&filterType=timeline&isPinned=false&userActivityType=ALL&projectFilterType=ALL" + "&page=" + this.state.pageNo + "&size=" + this.state.pageSize
    //     axios({
    //         method: 'get',
    //         url: REACT_APP_userServiceURL + url,
    //         headers: { 'Content-Type': 'application/json' },
    //         withCredentials: true
    //     }).then((response) => {
    //         let res = response.data;
    //         if (res.message === 'Success!') {
    //             this.setState({ projectsList: response.data.body.content })
    //         }
    //     })

    // }

    handleUnHideModal = (id) => {
        let data = {
            userId: this.state.userId,
            activityId: id
        }

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/hidden/unhide',
            data: data,
            withCredentials: true
        }).then(response => {
            if (response && response.status === 202) {
                console.log(response.status)
                this.getOngoingProjectsList()
                // this.getCompletedProjectsList()
            } else {
                console.log(response.status)
                // this.setState({'isSubmitted': false})
            }
        }).catch((err) => {
            if (err && err.response && err.response.status === 409) {
                console.log(err.response.status)
                // this.setState({'error': 'Entity has already been unhidden', 'isSubmitted': false})
            } else {
                console.log(err.response.status)
                // this.setState({'isSubmitted': false})
            }
        })
    }

    renderProjectItem = (item) => {
        return (

            <TouchableOpacity onPress={() =>
                this.props.navigation.navigate('ProjectDetailView', {
                    slug: item.item.project.slug,
                })
            }
                activeOpacity={0.7}
                style={styles.forumItemView}>



                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                        source={
                            item.item.project.coverImage
                                ? { uri: item.item.project.coverImage }
                                : projectDefault
                        }
                        style={styles.profileImage}
                    />

                    <View>
                        <Text
                            style={[typography.Title_2, { color: COLORS.dark_800, fontSize: 16, maxWidth: '66%' }]}
                            numberOfLines={1}
                        >
                            {item.item.projectType}
                        </Text>

                        <Text numberOfLines={2}
                            style={[
                                typography.Caption,
                                { color: COLORS.dark_600, marginRight: 100, marginTop: 6 },
                            ]}>
                            {item.item.project.title}
                        </Text>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={[defaultShape.Nav_Gylph_Btn, { position: 'absolute', right: -12, top: -4 }]}
                        onPress={() => this.handleUnHideModal(item.item.projectId)}>
                        <Icon
                            name="Eye_OL"
                            color={COLORS.altgreen_300}
                            size={14}
                            style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                        />
                    </TouchableOpacity>

                </View>

            </TouchableOpacity>
        )
    }

    listHeaderComponent = () => {
        return (
            <>

                <View style={{ alignSelf: 'center', flexDirection: 'row', backgroundColor: COLORS.altgreen_t50, width: 330, marginTop: 20, marginBottom: 10, justifyContent: 'center', borderRadius: 4 }}>
                    <TouchableOpacity onPress={() => { this.setState({ selected: 'upcoming' }, () => this.getOngoingProjectsList()) }}
                        style={[defaultShape.InTab_Btn, { backgroundColor: this.state.selected === 'upcoming' ? '#fff' : COLORS.altgreen_t50, width: 110, borderTopLeftRadius: 4, borderBottomLeftRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: this.state.selected === 'upcoming' ? COLORS.dark_800 : COLORS.altgreen_400 }]}>Upcoming</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.setState({ selected: 'running' }, () => this.getOngoingProjectsList()) }}
                        style={[defaultShape.InTab_Btn, { backgroundColor: this.state.selected === 'running' ? '#fff' : COLORS.altgreen_t50, width: 110 }]}>
                        <Text style={[typography.Caption, { color: this.state.selected === 'running' ? COLORS.dark_800 : COLORS.altgreen_400 }]}>Ongoing</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.setState({ selected: 'completed' }, () => this.getOngoingProjectsList()) }}
                        style={[defaultShape.InTab_Btn, { backgroundColor: this.state.selected === 'completed' ? '#fff' : COLORS.altgreen_t50, width: 110, borderTopRightRadius: 4, borderBottomRightRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: this.state.selected === 'completed' ? COLORS.dark_800 : COLORS.altgreen_400 }]}>Completed</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20, marginVertical: 10 }}>
                    <Text
                        style={[
                            typography.Caption,
                            { color: COLORS.dark_600, fontSize: 13, marginRight: 4 }
                        ]}>
                        Show only my posts
                    </Text>

                    <Switch
                        trackColor={{ false: "#767577", true: "#dadd21" }}
                        thumbColor={this.state.showOnlyMyPost ? "#00394d" : "#f4f3f4"}
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={() => { this.setState({ showOnlyMyPost: !this.state.showOnlyMyPost }, () => { this.getOngoingProjectsList() }) }}
                        value={this.state.showOnlyMyPost}
                    />
                </View>

                {!this.state.projectsList.length ?
                    <Text
                        style={[typography.Title_2, { color: COLORS.dark_800, fontSize: 16, alignSelf: 'center', marginTop: 20 }]}
                    >
                        You have no Hidden Activities
                    </Text>
                    :
                    <></>}
            </>
        )
    }

    // listFooterComponent = () => {
    //     return (
    //         this.state.projectsList.length && this.state.completedProjectsList.length ?
    //             <FlatList
    //                 showsVerticalScrollIndicator={false}
    //                 style={{ paddingTop: 10, marginTop: 30 }}
    //                 data={this.state.completedProjectsList}
    //                 ListHeaderComponent={this.listHeaderComponent2()}
    //                 contentContainerStyle={{ paddingBottom: 30, marginTop: 30 }}
    //                 // onEndReached={this.handleLoadMore}
    //                 // onEndReachedThreshold={1}
    //                 keyExtractor={(item) => item.id}
    //                 renderItem={(item) => this.renderProjectItem(item)}
    //             />
    //             :
    //             <></>
    //     )
    // }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 20 }] : styles.header}>

                    <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack()}>
                        <Icon name="Arrow-Left" size={15} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                    </TouchableOpacity>
                    <Text style={[typography.H3, { color: COLORS.dark_800, fontSize: 22 }]}>
                        Projects
                    </Text>
                    <View style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }}>
                        <Icon name="Arrow-Left" size={15} color="#FFF" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                    </View>

                </View>


                <FlatList
                    showsVerticalScrollIndicator={false}
                    style={{ paddingTop: 10 }}
                    data={this.state.projectsList}
                    ListHeaderComponent={this.listHeaderComponent()}
                    // ListFooterComponent={this.listFooterComponent()}
                    contentContainerStyle={{ paddingBottom: 30 }}
                    // onEndReached={this.handleLoadMore}
                    // onEndReachedThreshold={1}
                    keyExtractor={(item) => item.id}
                    renderItem={(item) => this.renderProjectItem(item)}
                />



            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 20,
        backgroundColor: '#FFF',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    forumItemView: {
        width: '90%',
        marginTop: 15,
        backgroundColor: COLORS.white,
        borderRadius: 4.5,
        alignSelf: 'center',
        paddingRight: 15,
        // paddingVertical: 16,
    },
    profileImage: {
        height: 70,
        width: '22%',
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        marginRight: 14,
    },
})
