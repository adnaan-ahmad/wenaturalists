import React, {Component} from 'react';
import {
  ScrollView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  RefreshControl,
  FlatList,
  ActivityIndicator,
  SafeAreaView,
  ImageBackground,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';

import defaultShape from '../../../Components/Shared/Shape';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultCover from '../../../../assets/defaultCover.png';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultStyle from '../../../Components/Shared/Typography';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

class SupportedCauses extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      allCausesList: [],
      pressed: false,
      page: 0,
      refreshing: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({userId: value}, () => {
          this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.onRefresh();
          });
          this.getAllCauses();
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  onRefresh = () => {
    this.setState({page: 0, allCausesList: []}, () => this.getAllCauses());
  };

  handleSupportStatusChange(causeId) {
    let postBody = {
      userId: this.state.userId,
      causeId: [causeId],
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/public/cause/join',
      headers: {'Content-Type': 'application/json'},
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '201 CREATED') {
          let allCauseListTemp = cloneDeep(this.state.allCausesList);
          let tempindex = this.state.allCausesList.findIndex(
            (x) => x.id === causeId,
          );
          let joinString = this.state.allCausesList[tempindex].joined;
          if (joinString) {
            allCauseListTemp[tempindex].joined = false;
          } else {
            allCauseListTemp[tempindex].joined = true;
          }
          this.setState({allCausesList: allCauseListTemp});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  removeCause(causeId) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/cause/delete?id=' +
        causeId +
        '&userId=' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          let allCauseListTemp = cloneDeep(this.state.allCausesList);
          let tempindex = this.state.allCausesList.findIndex(
            (x) => x.id === causeId,
          );
          let joinString = this.state.allCausesList[tempindex].joined;
          if (joinString) {
            allCauseListTemp[tempindex].joined = false;
          } else {
            allCauseListTemp[tempindex].joined = true;
          }
          this.setState({allCausesList: allCauseListTemp});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  onRefresh = () => {
    this.setState({page: 0, allCausesList: []}, () => this.getAllCauses());
  };

  handleLoadMore = () => {
    this.setState({page: this.state.page + 1}, () => this.getAllCauses());
  };

  getAllCauses() {
    let url;
    url =
      '/backend/public/cause/unSupportedList/list?userId=' +
      this.state.userId +
      '&page=' +
      this.state.page +
      '&size=' +
      10;
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.data.message === 'Success!'
        ) {
          this.setState({
            allCausesList: this.state.allCausesList.concat(
              response.data.body.list,
            ),
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    return (
      <SafeAreaView>
        <View
          style={
            Platform.OS === 'ios'
              ? [styles.header, {paddingVertical: 20}]
              : styles.header
          }>
          <TouchableOpacity
            style={{
              width: 40,
              height: 30,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => this.props.navigation.goBack()}>
            <Icon
              name="Arrow-Left"
              size={15}
              color="#91B3A2"
              style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
            />
          </TouchableOpacity>
          <Text
            style={[defaultStyle.H3, {color: COLORS.dark_800, fontSize: 22}]}>
            CAUSES
          </Text>
          <TouchableOpacity
            style={{
              width: 40,
              height: 30,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => this.props.navigation.goBack()}>
            <Icon
              name="Arrow-Left"
              size={15}
              color="#FFF"
              style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            alignSelf: 'center',
            flexDirection: 'row',
            backgroundColor: COLORS.altgreen_t50,
            width: 320,
            marginTop: 20,
            justifyContent: 'center',
            borderRadius: 4,
          }}>
          <TouchableOpacity
            style={[
              defaultShape.InTab_Btn,
              {backgroundColor: '#fff', width: '50%', borderRadius: 4},
            ]}>
            <Text style={[defaultStyle.Caption, {color: COLORS.dark_800}]}>
              Recommended
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('SupportedCauses')}
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor: COLORS.altgreen_t50,
                width: '50%',
                borderRadius: 4,
              },
            ]}>
            <Text style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
              Supported
            </Text>
          </TouchableOpacity>
        </View>

        {this.state.allCausesList ? (
          <FlatList
            contentContainerStyle={styles.causesFlatList}
            showsVerticalScrollIndicator={false}
            alwaysBounceHorizontal={false}
            keyExtractor={(item) => item.id}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
            onEndReached={this.handleLoadMore}
            data={this.state.allCausesList}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('CausesStack', {
                      screen: 'CausesDetail',
                      params: {circleData: JSON.stringify(item), id: item.id},
                    })
                  }
                  style={styles.individualCause}
                  activeOpacity={0.96}>
                  <ImageBackground
                    imageStyle={{borderRadius: 6}}
                    source={item.imageUrl ? {uri: item.imageUrl} : defaultCover}
                    style={styles.causesImage}>
                    <TouchableOpacity
                      onPress={() =>
                        item.joined
                          ? this.removeCause(item.id)
                          : this.handleSupportStatusChange(item.id)
                      }
                      style={{
                        flexDirection: 'row',
                        paddingHorizontal: 10,
                        height: 30,
                        borderRadius: 4,
                        backgroundColor: item.joined
                          ? COLORS.altgreen_300
                          : COLORS.green_500,
                        justifyContent: 'center',
                        alignItems: 'center',
                        minWidth: 100,
                        maxWidth: 150,
                        marginLeft: 8,
                        marginTop: 30,
                      }}
                      activeOpacity={0.9}>
                      <Icon
                        name={item.joined ? 'FollowTick' : 'Causes_F'}
                        size={14}
                        color={COLORS.white}
                        style={{
                          marginTop: Platform.OS === 'android' ? 8 : 0,
                          marginRight: 6,
                        }}
                      />
                      <Text
                        style={[
                          defaultStyle.Button_Lead,
                          {
                            color: item.joined ? COLORS.white : COLORS.white,
                            fontSize: 12,
                          },
                        ]}>
                        {item && item.joined ? 'SUPPORTING' : 'SUPPORT CAUSE'}
                      </Text>
                    </TouchableOpacity>
                  </ImageBackground>

                  <View style={styles.linearGradientView}>
                    <View style={styles.linearGradient}>
                      <Text
                        style={[
                          defaultStyle.Title_2,
                          {color: COLORS.primarydark, marginTop: 10},
                        ]}>
                        {item.name}
                      </Text>

                      <ScrollView
                        contentContainerStyle={{
                          flexDirection: 'row',
                          marginTop: 4,
                        }}>
                        {item.hashtag &&
                          item.hashtag.slice(0, 3).map((hashTag, index) => (
                            <View key={index}>
                              <Text
                                style={[
                                  defaultStyle.Subtitle_2,
                                  {color: COLORS.link_blue, marginRight: 8},
                                ]}>
                                #{hashTag}
                              </Text>
                            </View>
                          ))}
                      </ScrollView>

                      {/* <HTML source={{ html: item.content }} ignoredStyles={['font-family']} /> */}

                      {/* {item.content ? <Text style={[defaultStyle.Subtitle_2, { color: '#43454a', marginTop: 4, fontSize: 11 }]} numberOfLines={2}><HTML source={{ html: item.content }} tagsStyles={{
                                            span: { fontSize: 11 }
                                        }} />... <Text style={[defaultStyle.Subtitle_2, { color: COLORS.green_500, fontSize: 11, marginTop: item.content ? 0 : 4 }]}>Know more</Text></Text> : <></>}
                                        {!item.content ?  */}
                      <Text
                        style={[
                          defaultStyle.Subtitle_2,
                          {color: COLORS.link_blue, fontSize: 11, marginTop: 4},
                        ]}>
                        Know more
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            }}
          />
        ) : (
          <ActivityIndicator size="small" color="#00394D" />
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
    backgroundColor: COLORS.white,
    paddingBottom: 10,
  },

  linearGradientView: {
    width: '90%',
    // height: 70,
    position: 'absolute',
    bottom: 0,
  },
  causesText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    position: 'absolute',
    bottom: 20,
    zIndex: 2,
  },
  causesImage: {
    width: '94.8%',
    height: 210,
    borderRadius: 6,
    marginVertical: 12,
    marginLeft: 19,
    justifyContent: 'center',
  },
  individualCause: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 2,
  },
  seeAllText: {
    color: '#698F8A',
    fontSize: 11,
    fontFamily: 'Montserrat-Medium',
  },
  seeAll: {
    marginHorizontal: 16,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 26,
    width: 70,
    borderWidth: 1,
    borderColor: '#698F8A',
    borderRadius: 13,
    paddingHorizontal: 6,
    paddingVertical: 2,
  },
  causesFlatList: {
    paddingTop: 6,
    paddingBottom: 130,
  },
  causesSupportedText: {
    color: '#154A59',
    fontSize: 14,
    fontFamily: 'Montserrat-Bold',
    marginLeft: 8,
  },
  causesSupported: {
    backgroundColor: '#E7F3E3',
    paddingVertical: 20,
    paddingLeft: 16,
  },

  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});

export default SupportedCauses;
