import React, {Component} from 'react';
import {
  Linking,
  Clipboard,
  Share,
  Dimensions,
  Modal,
  ImageBackground,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  FlatList,
  Image,
  SafeAreaView,
  Platform,
  PermissionsAndroid,
  Alert,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {connect} from 'react-redux';
import Video from 'react-native-video';
import VideoPlayer from 'react-native-video-controls';
import LinearGradient from 'react-native-linear-gradient';
import Snackbar from 'react-native-snackbar';
import RNUrlPreview from 'react-native-url-preview';
import {TextInput} from 'react-native-paper';
import RNFS from 'react-native-fs';
import * as Progress from 'react-native-progress';
import RNFetchBlob from 'rn-fetch-blob';
import {v4 as uuidv4} from 'uuid';
import TimeAgo from 'react-native-timeago';

import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import circleDefault from '../../../../assets/CirclesDefault.png';
import typography from '../../../Components/Shared/Typography';
import {personalProfileRequest} from '../../../services/Redux/Actions/User/PersonalProfileActions';
import {
  feedsPhotosRequest,
  feedsVideosRequest,
} from '../../../services/Redux/Actions/User/FeedsActions';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import SearchBar from '../../../Components/User/SearchBar';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import httpService from '../../../services/AxiosInterceptors';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import goToTop from '../../../../assets/GoToTop.svg';
import defaultStyle from '../../../Components/Shared/Typography';
import defaultCover from '../../../../assets/defaultCover.png';
import BlogDefault from '../../../../assets/BlogDefault.jpg';

import {times} from 'lodash';
import postIcon from '../../../../assets/Post_Icon.png';
import blogIcon from '../../../../assets/Blog_Icon.png';
import linkIcon from '../../../../assets/Link_Icon.png';
import ConnectDepth from '../../../Components/User/Common/ConnectDepth';
import Follow from '../../../Components/User/Common/Follow';
import Autolink from 'react-native-autolink';
import SharedUserList from '../../../Components/User/Common/SharedUserList';
import LikedUserList from '../../../Components/User/Common/LikedUserList';

httpService.setupInterceptors();
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
  'i',
); // fragment locator

class CausesDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 'LATEST',
      userId: '',
      notification: false,
      redirectToProfile: false,
      pageNumber: 0,
      pageSize: 30,
      postModalOpen: false,
      attachmentModalopen: false,
      optionsModalOpen: false,
      headerOptionsModalOpen: false,
      shareModalOpen: false,
      likeModalOpen: false,
      readMore: false,
      currentScrollPosition: 0,
      hideControls: false,
      pressedActivityId: '',
      peopleLiked: [],
      activitiesData: [],
      currentUserId: '',
      canReport: '',
      videoHeight: 190,
      videoWidth: '97%',
      shareModal2Open: false,
      contactInfoModalOpen: false,
      circleData: {},
      supporting: true,
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      isReported: false,
      causeItem: {},
      downloadStart: false,
      isDownloading: false,
      progressLimit: 0,
      customUserDetails: {},
      peopleShared: [],
      peopleSharedModalOpen: false,
      isCompany: false,
      currentPressed: {},
    };
  }

  componentDidMount() {
    let obj = JSON.parse(this.props.route.params.circleData);
    // this.setState({circleData: obj, supporting: obj.joined});

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.props.route.params && this.props.route.params.id !== ''
          ? this.props.personalProfileRequest({
              userId: this.props.route.params.id,
              otherUserId: value,
            })
          : this.props.personalProfileRequest({userId: value, otherUserId: ''});

        this.setState({userId: value}, () => {
          this.getUserActivitiesPosts();
          this.getCausesBySlug(obj.slug);
        });
      })
      .catch((e) => {
        console.log(e);
      });

    AsyncStorage.getItem('userData')
      .then((value) => {
        let objValue = JSON.parse(value);
        objValue.type === 'COMPANY' && this.setState({isCompany: true});
      })
      .catch((e) => {
        console.log(e);
      });

    // this.props.userFeedsPhotos.body.content ?
    // this.setState({ likedPosts: this.props.userFeedsPhotos.body.content.filter((item) => item.liked) })
    // : null

    try {
      messaging().onNotificationOpenedApp((remoteMessage) => {
        if (remoteMessage) {
          // console.log('remoteMessage :', remoteMessage)
          console.log(
            'Notification caused app to open from background state:',
            remoteMessage.data.url,
            remoteMessage.data.userId,
          );
        }
      });
    } catch (error) {
      console.log(error);
    }

    try {
      messaging()
        .getInitialNotification()
        .then((remoteMessage) => {
          if (remoteMessage) {
            // console.log('remoteMessage :', remoteMessage)
            console.log(
              'Notification caused app to open from quit state:',
              remoteMessage.data.url,
              remoteMessage.data.userId,
            );
          }
        });
    } catch (error) {
      console.log(error);
    }
  }

  getCausesBySlug = (slug) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/cause/getBySlug?slug=' +
        slug +
        '&userId=' +
        this.state.userId,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          circleData: response.data.body,
          supporting: response.data.body.joined,
        });
      })
      .catch((err) => console.log('get causes by slug error : ', err));
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message: REACT_APP_domainUrl + '/post/' + this.state.pressedActivityId,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  handleLoadMore = () => {
    console.log('handle load more called');
    this.setState({pageNumber: this.state.pageNumber + 1}, () => {
      this.getUserActivitiesPosts();
    });
  };

  // shouldComponentUpdate(nextProps, nextState) {
  //     if (nextProps.user !== this.props.user) {
  //         return true
  //     }
  //     if (nextProps.userFeedsPhotos !== this.props.userFeedsPhotos) {
  //         return true
  //     }
  //     if (nextProps.userFeedsVideos !== this.props.userFeedsVideos) {
  //         return true
  //     }

  //     if (nextState.description !== this.state.description) {
  //         return true
  //     }
  //     if (nextState.reasonForReporting !== this.state.reasonForReporting) {
  //         return true
  //     }
  //     if (nextState.reasonForReportingModalOpen !== this.state.reasonForReportingModalOpen) {
  //         return true
  //     }
  //     if (nextState.supporting !== this.state.supporting) {
  //         return true
  //     }
  //     if (nextState.circleActivities !== this.state.circleActivities) {
  //         return true
  //     }
  //     if (nextState.circleData !== this.state.circleData) {
  //         return true
  //     }
  //     if (nextState.contactInfoModalOpen !== this.state.contactInfoModalOpen) {
  //         return true
  //     }
  //     if (nextState.shareModal2Open !== this.state.shareModal2Open) {
  //         return true
  //     }
  //     if (nextState.pageNumber !== this.state.pageNumber) {
  //         return true
  //     }
  //     if (nextState.pageSize !== this.state.pageSize) {
  //         return true
  //     }
  //     if (nextState.userId !== this.state.userId) {
  //         return true
  //     }
  //     if (nextState.notification !== this.state.notification) {
  //         return true
  //     }
  //     if (nextState.redirectToProfile !== this.state.redirectToProfile) {
  //         return true
  //     }
  //     if (nextState.selected !== this.state.selected) {
  //         return true
  //     }
  //     if (nextState.postModalOpen !== this.state.postModalOpen) {
  //         return true
  //     }
  //     if (nextState.attachmentModalopen !== this.state.attachmentModalopen) {
  //         return true
  //     }
  //     if (nextState.readMore !== this.state.readMore) {
  //         return true
  //     }
  //     if (nextState.currentScrollPosition !== this.state.currentScrollPosition) {
  //         return true
  //     }
  //     if (nextState.hideControls !== this.state.hideControls) {
  //         return true
  //     }
  //     if (nextState.pressedActivityId !== this.state.pressedActivityId) {
  //         return true
  //     }
  //     if (nextState.optionsModalOpen !== this.state.optionsModalOpen) {
  //         return true
  //     }
  //     if (nextState.shareModalOpen !== this.state.shareModalOpen) {
  //         return true
  //     }
  //     if (nextState.likeModalOpen !== this.state.likeModalOpen) {
  //         return true
  //     }
  //     if (nextState.peopleLiked !== this.state.peopleLiked) {
  //         return true
  //     }
  //     if (nextState.videoHeight !== this.state.videoHeight) {
  //         return true
  //     }
  //     if (nextState.videoWidth !== this.state.videoWidth) {
  //         return true
  //     }
  //     if (nextState.headerOptionsModalOpen !== this.state.headerOptionsModalOpen) {
  //         return true
  //     }
  //     if (nextState.downloadStart !== this.state.downloadStart) {
  //         return true
  //     }
  //     if (nextState.isDownloading !== this.state.isDownloading) {
  //         return true
  //     }
  //     if (nextState.progressLimit !== this.state.progressLimit) {
  //         return true
  //     }
  //     // if (nextState.likedPosts !== this.state.likedPosts) {
  //     //   return true
  //     // }
  //     if (nextState.activitiesData !== this.state.activitiesData) {
  //         return true
  //     }
  //     if (nextState.selected !== this.state.selected) {
  //         return true
  //     }
  //     return false
  // }

  componentDidUpdate(prevProps, prevState) {
    // if (prevState.likedPosts !== this.state.likedPosts) {

    // }

    if (this.state.notification) {
      this.setState({notification: false}, () =>
        this.props.navigation.navigate('Notification'),
      );
    }
    if (this.state.redirectToProfile) {
      this.setState({redirectToProfile: false}, () =>
        this.props.navigation.navigate('ProfileStack'),
      );
    }
  }

  changeState = (value) => {
    this.setState(value);
  };

  renderHeader = (item, index) => {
    return (
      <View style={styles.unreadNotiItem}>
        <View
          style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              item.userType === 'COMPANY'
                ? this.props.navigation.navigate('ProfileStack', {
                    screen: 'CompanyProfileScreen',
                    params: {userId: item.userId},
                  })
                : item.userType === 'INDIVIDUAL' &&
                  item.userId !== this.state.userId
                ? this.props.navigation.navigate('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: {userId: item.userId},
                  })
                : item.userType === 'INDIVIDUAL' &&
                  item.userId === this.state.userId
                ? this.props.navigation.navigate('ProfileStack', {
                    screen: 'ProfileScreen',
                    // params: {userId: item.userId},
                  })
                : null;
            }}>
            {item.originalProfileImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={{uri: item.originalProfileImage}}
              />
            ) : item.params && item.params.circleImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={{uri: item.params.circleImage}}
              />
            ) : item.userType === 'INDIVIDUAL' && !item.originalProfileImage ? (
              <Image
                style={[defaultShape.Media_Round, {backgroundColor: 'orange'}]}
                source={defaultProfile}
              />
            ) : item.userType === 'COMPANY' && !item.originalProfileImage ? (
              <Image
                style={[defaultShape.Media_Round, {backgroundColor: 'orange'}]}
                source={defaultBusiness}
              />
            ) : item.params &&
              item.params.circleSlug &&
              !item.params.circleImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={circleDefault}
              />
            ) : (
              <></>
            )}
          </TouchableOpacity>
          <View
            style={{
              justifyContent: 'flex-start',
              marginLeft: 6,
            }}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Text
                numberOfLines={1}
                onPress={() => {
                  item.userType === 'COMPANY'
                    ? this.props.navigation.navigate('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                        params: {userId: item.userId},
                      })
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId !== this.state.userId
                    ? this.props.navigation.navigate('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.userId},
                      })
                    : item.userType === 'INDIVIDUAL' &&
                      item.userId === this.state.userId
                    ? this.props.navigation.navigate('ProfileStack', {
                        screen: 'ProfileScreen',
                        // params: {userId: item.userId},
                      })
                    : null;
                }}
                style={[
                  typography.Title_2,
                  {
                    color: COLORS.dark_800,
                    maxWidth: 160,
                  },
                ]}>
                {item.userName ? item.userName : item.params.circleTitle}
              </Text>
              {item.connectDepth && item.userId != this.state.userId ? (
                <ConnectDepth
                  connectDepth={item.connectDepth}
                  key={item.connectDepth}
                />
              ) : (
                <Text></Text>
              )}
              {item.userEntityType !== 'CIRCLE' &&
                !this.state.isCompany &&
                item &&
                (!item.connectDepth ||
                  (item.connectDepth && item.connectDepth < 1)) && (
                  <Follow
                    item={item}
                    followed={item.followed}
                    userId={item.userId}
                    index={index}
                  />
                )}
            </View>

            <View style={{flexDirection: 'row'}}>
              {item && item.country ? (
                <Icon
                  name="Location"
                  color={COLORS.altgreen_300}
                  size={10}
                  style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                />
              ) : (
                <></>
              )}
              <Text
                numberOfLines={1}
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigation.navigate('EditorsDesk', {
                        id: item.id,
                        userId: this.state.userId,
                      })
                    : this.props.navigation.navigate('IndividualFeedsPost', {
                        id: item.id,
                        commentCount: item.commentCount,
                      })
                }
                style={[
                  {
                    color: COLORS.altgreen_300,
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                    maxWidth: 100,
                  },
                ]}>
                {item && item.country ? item.country : null}
                {/* {+ new Date() - item.createTime} */}
              </Text>
              {item && item.country ? (
                <Icon
                  name="Bullet_Fill"
                  color={COLORS.altgreen_300}
                  size={8}
                  style={{marginTop: Platform.OS === 'android' ? 1 : 2}}
                />
              ) : (
                <></>
              )}

              <Text
                onPress={() =>
                  item && item.postType === 'ARTICLE'
                    ? this.props.navigation.navigate('EditorsDesk', {
                        id: item.id,
                        userId: this.state.userId,
                      })
                    : this.props.navigation.navigate('IndividualFeedsPost', {
                        id: item.id,
                        commentCount: item.commentCount,
                      })
                }
                style={[
                  {
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                  },
                ]}>
                <TimeAgo time={item.createTime} />
                {/* {+ new Date() - item.createTime} */}
              </Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          activeOpacity={0.5}
          style={[defaultShape.Nav_Gylph_Btn, {}]}
          onPress={() =>
            this.setState(
              {
                optionsModalOpen: true,
                pressedActivityId: item.id,
                currentUserId: item.userId,
                canReport: item.canReport,
                currentPressed: item,
              },
              () => this.verifyReported(),
            )
          }>
          <Icon
            name="Kebab"
            color={COLORS.altgreen_300}
            size={14}
            style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
          />
        </TouchableOpacity>
      </View>
    );
  };

  renderSharedHeader = (item) => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          item.userType === 'INDIVIDUAL' && item.userId === this.state.userId
            ? this.props.navigation.navigate('ProfileStack', {
                screen: 'ProfileScreen',
                // params: {userId: item.userId},
              })
            : item.userType === 'INDIVIDUAL' &&
              item.userId !== this.state.userId
            ? this.props.navigation.navigate('ProfileStack', {
                screen: 'OtherProfileScreen',
                params: {userId: item.userId},
              })
            : item.userType === 'COMPANY'
            ? this.props.navigation.navigate('ProfileStack', {
                screen: 'CompanyProfileScreen',
                params: {userId: item.userId},
              })
            : this.props.navigation.navigate('CircleProfileStack', {
                screen: 'CircleProfile',
                params: {slug: item.params.circleSlug},
              });
          // console.log(item)
        }}
        style={styles.unreadNotiItem}>
        {item.username || (item.params && item.params.circleTitle) ? (
          <View style={{width: '40%', flexDirection: 'row'}}>
            {item.profileImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={{uri: item.profileImage}}
              />
            ) : item.params && item.params.circleImage ? (
              <Image
                style={[defaultShape.Media_Round, {}]}
                source={{uri: item.params.circleImage}}
              />
            ) : (
              <Image
                style={[defaultShape.Media_Round]}
                source={
                  item.userType === 'INDIVIDUAL'
                    ? defaultProfile
                    : item.userType === 'INDIVIDUAL'
                    ? defaultProfile
                    : item.userType === 'COMPANY'
                    ? defaultBusiness
                    : circleDefault
                }
              />
            )}

            <View style={{}}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text
                  // onPress={() => console.log(item)}
                  numberOfLines={1}
                  style={[
                    defaultStyle.Title_2,
                    {color: COLORS.dark_800, marginLeft: 8},
                  ]}>
                  {item.username
                    ? item.username.split(' ').slice(0, 2).join(' ')
                    : item.params && item.params.circleTitle
                    ? item.params.circleTitle
                    : null}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  marginLeft: 5,
                }}>
                {item && item.addressDetail && item.addressDetail.country ? (
                  <Icon
                    name="Location"
                    color={COLORS.altgreen_300}
                    size={10}
                    style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                  />
                ) : (
                  <></>
                )}
                {item?.addressDetail?.country && (
                  <Text
                    onPress={() =>
                      item && item.postType === 'ARTICLE'
                        ? this.props.navigation.navigate('EditorsDesk', {
                            id: item.id,
                            userId: this.state.userId,
                          })
                        : this.props.navigation.navigate(
                            'IndividualFeedsPost',
                            {
                              id: item.id,
                              commentCount: item.commentCount,
                            },
                          )
                    }
                    style={[
                      {
                        color: COLORS.altgreen_300,
                        marginLeft: 2,
                        fontSize: 10,
                        fontFamily: 'Montserrat-Medium',
                      },
                    ]}>
                    {item && item.addressDetail && item.addressDetail.country
                      ? item.addressDetail &&
                        item.addressDetail.country
                          .split(' ')
                          .splice(0, 2)
                          .join(' ')
                      : null}
                    {/* {+ new Date() - item.createTime} */}
                  </Text>
                )}
                {item?.addressDetail?.country && (
                  <Icon
                    name="Bullet_Fill"
                    color={COLORS.altgreen_300}
                    size={8}
                    style={{marginTop: Platform.OS === 'android' ? 0 : 0}}
                  />
                )}

                <Text
                  onPress={() =>
                    item && item.postType === 'ARTICLE'
                      ? this.props.navigation.navigate('EditorsDesk', {
                          id: item.id,
                          userId: this.state.userId,
                        })
                      : this.props.navigation.navigate('IndividualFeedsPost', {
                          id: item.id,
                          commentCount: item.commentCount,
                        })
                  }
                  style={[
                    {
                      marginLeft: 2,
                      fontSize: 10,
                      fontFamily: 'Montserrat-Medium',
                    },
                  ]}>
                  <TimeAgo time={item.createTime} />
                  {/* {+ new Date() - item.createTime} */}
                </Text>
              </View>
            </View>
          </View>
        ) : (
          <View style={{}}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 2},
              ]}>
              {item.title}
            </Text>
            {item.location ? (
              <Text
                style={[
                  {
                    color: COLORS.altgreen_300,
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                  },
                ]}>
                {item.location.city}, {item.location.state},{' '}
                {item.location.country}
              </Text>
            ) : (
              <></>
            )}
          </View>
        )}

        {/* <TouchableOpacity activeOpacity={0.5} style={[defaultShape.Nav_Gylph_Btn, {}]}
          onPress={() => this.setState({ optionsModalOpen: true, pressedActivityId: item.id })}>
          <Icon name="Kebab" color={COLORS.altgreen_300} size={14} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
        </TouchableOpacity> */}
      </TouchableOpacity>
    );
  };

  renderFooter = (item) => {
    return (
      <View style={[styles.unreadNotiItem, {paddingRight: 26}]}>
        <View>
          {item.secondaryAttachmentIds &&
          item.secondaryAttachmentIds.length > 0 ? (
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}
              onPress={() =>
                this.setState({attachmentModalopen: true, causeItem: item})
              }>
              <Icon
                name="Clip"
                color={COLORS.green_500}
                size={14}
                style={{
                  marginTop: Platform.OS === 'android' ? 10 : 0,
                  marginRight: 6,
                }}
              />
              <Text style={[{color: COLORS.altgreen_300}, defaultStyle.Note2]}>
                {item.secondaryAttachmentIds.length === 1
                  ? item.secondaryAttachmentIds.length + ' File '
                  : item.secondaryAttachmentIds.length + ' Files '}{' '}
                Attached
              </Text>
            </TouchableOpacity>
          ) : (
            <Text></Text>
          )}
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() =>
              this.setState(
                {pressedActivityId: item.id},
                this.handleLike(item.id, item.liked),
              )
            }
            activeOpacity={0.5}
            style={{alignItems: 'center', flexDirection: 'row'}}>
            <Icon
              name={item.liked ? 'Like_FL' : 'Like'}
              color={COLORS.green_500}
              size={14}
              style={{
                marginTop: Platform.OS === 'android' ? 10 : 0,
                marginHorizontal: 6,
              }}
            />
            <Text style={[{color: COLORS.altgreen_300}, defaultStyle.Caption]}>
              {item.likesCount}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }
            activeOpacity={0.5}
            style={{
              alignItems: 'center',
              flexDirection: 'row',
              marginLeft: 12,
            }}>
            <Icon
              name="Comment"
              color={COLORS.green_500}
              size={14}
              style={{
                marginTop: Platform.OS === 'android' ? 10 : 0,
                marginRight: 6,
              }}
            />
            <Text style={[{color: COLORS.altgreen_300}, defaultStyle.Caption]}>
              {item.commentCount}
            </Text>
          </TouchableOpacity>
        </View>

        {/* <TouchableOpacity onPress={() => this.setState({ shareModalOpen: true })} activeOpacity={0.5} style={[defaultShape.Nav_Gylph_Btn, { flexDirection: 'row' }]}>
          <Text style={[defaultStyle.Caption, { color: COLORS.grey_350, marginLeft: 8 }]}>Share</Text>
          <Icon name="Share" color={COLORS.altgreen_300} size={14} style={{ marginTop: Platform.OS === 'android' ? 10 : 0, marginLeft: 6 }} />
        </TouchableOpacity> */}
      </View>
    );
  };

  getUsersWhoLiked = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/like/getUsers/' +
        this.state.pressedActivityId +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          // console.log('%%%%%%%%%%%%%%%% response.data.body %%%%%%%%%%%%%%%%%%%', response.data.body)
          this.setState({peopleLiked: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleLike = (activityId, liked) => {
    // let newState = !this.state.liked;
    // let likesCount = this.state.likesCount;

    // this.setState({
    //     'liked': newState,
    //     'likesCount': newState ? likesCount + 1 : Math.max(0, likesCount - 1),
    //     'likeSuccess': false
    // })

    const data = {
      userId: this.state.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.setState(
            {activitiesData: [], pageNumber: 0, pageSize: 5},
            () => {
              this.getUserActivitiesPosts();
            },
          );
          // this.setState({ ...this.state })
          // this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
          // this.setState({
          //   'likeSuccess': true,
          //   'likesCount': newState ? likesCount + 1 : Math.max(0, likesCount - 1)
          // })
        } else {
          // this.setState({ 'liked': !newState })
        }
      })
      .catch((err) => {
        // this.setState({ 'liked': !newState, 'likeSuccess': true })
        console.log(err);
      });
  };

  unixTime = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    return day + ' ' + month + ' ' + year;
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + ' s';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + ' m';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + ' h';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + ' d';
    }

    if (difference >= 864000) {
      return day + ' ' + month + ' ' + year;
    }
  };

  renderImage = (item) => {
    if (item.attachmentIds) {
      if (item.attachmentIds.length > 5) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[3].attachmentUrl}}
              />

              <ImageBackground
                source={{uri: item.attachmentIds[4].attachmentUrl}}
                imageStyle={{}}
                style={{height: 120, width: '57.8%'}}>
                <View
                  style={{
                    height: 120,
                    width: '57.8%',
                    backgroundColor: COLORS.dark_900 + 'BF',
                    opacity: 0.95,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat-Bold',
                      fontSize: 28,
                      color: COLORS.white,
                    }}>
                    +{item.attachmentIds.length - 4}
                  </Text>
                </View>
              </ImageBackground>
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 5) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[3].attachmentUrl}}
              />
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[4].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 4) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[3].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 3) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '100%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 2) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '100%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '100%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 1) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            {/* {this.state.selected === 'AUDIO' && item.attachmentIds[0].attachmentUrl || this.state.selected === 'VIDEOS' ? */}
            {item.attachmentIds[0].attachmentType === 'AUDIO' &&
            this.state.selected !== 'VIDEOS' ? (
              <VideoPlayer
                style={{width: '97%', height: 80}}
                tapAnywhereToPause={true}
                disableFullscreen={true}
                disableSeekbar={true}
                disableVolume={true}
                disableTimer={false}
                disableBack={true}
                poster="https://www.agbiz.co.za/Content/images/audio.jpg"
                paused={true}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
                navigator={this.props.navigator}
              />
            ) : item.attachmentIds[0].attachmentType === 'VIDEO' &&
              this.state.selected !== 'AUDIO' ? (
              <VideoPlayer
                style={{
                  width: this.state.videoWidth,
                  height: this.state.videoHeight,
                }}
                tapAnywhereToPause={true}
                disableSeekbar={true}
                disableVolume={true}
                disableTimer={true}
                poster={
                  item.attachmentIds[0] &&
                  item.attachmentIds[0].thumbnails &&
                  item.attachmentIds[0] &&
                  item.attachmentIds[0].thumbnails[0]
                    ? item.attachmentIds[0].thumbnails[0]
                    : 'https://cdn.dscovr.com/images/video-blank2.webp'
                }
                disableBack={true}
                paused={true}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
                navigator={this.props.navigator}
              />
            ) : (
              <Image
                style={{width: '100%', height: 150}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
            )}
          </TouchableOpacity>
        );
      }
      // }

      // else if(this.state.selected === 'VIDEOS' && this.props.userFeedsPhotos.body) {
      //   return (
      //     <Video style={{ width: 300, height: 200 }}
      //     paused={true}
      //     source={{ uri: item.attachmentIds[0].attachmentUrl }} />
      //   )
      // }
    } else return <></>;
  };

  downloadProgressWrapper = () => {
    return (
      <Modal
        isVisible={this.state.isDownloading}
        backdropColor={'#ffffff'}
        animationIn={'slideInUp'}
        animationInTiming={300}
        animationOut={'slideOutDown'}
        animationOutTiming={300}
        avoidKeyboard={true}>
        <View>
          {this.state.progressLimit == 1 ? null : (
            <Progress.Pie
              style={styles.downloadInProgress}
              progress={this.state.progressLimit}
              size={50}
              color={'green'}
            />
          )}
          {this.state.progressLimit == 1 ? (
            <Text style={styles.downloadInComplete}>
              File Download Successfully{' '}
              {Platform.OS === 'android' ? 'in Download Folder' : ''}
            </Text>
          ) : null}
        </View>
      </Modal>
    );
  };

  readStorage = async (data) => {
    console.log('storage');
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.writeStorage(data);
      } else {
        // Permission Denied
        alert('Read Storage Permission Denied');
      }
    } else {
      this.getDownloadFile(data); //work on ios
    }
  };

  writeStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.getDownloadFile(data);
      } else {
        // Permission Denied
        alert('Write Storage Permission Denied');
      }
    }
  };

  getDownloadFile = (urlDownload) => {
    if (this.state.downloadStart == true) {
      return;
    } else {
      this.setState({downloadStart: true});
    }

    let directoryFile;
    let dirs;

    let fileName;
    try {
      fileName = urlDownload.split('-').pop();
    } catch (e) {
      fileName = uuidv4();
    }
    if (Platform.OS === 'android') {
      directoryFile = RNFetchBlob.fs.dirs;
      dirs = directoryFile.DownloadDir + '/Download/' + fileName;
    } else {
      directoryFile = RNFS.DocumentDirectoryPath;
      dirs = directoryFile + fileName;
    }

    try {
      RNFetchBlob.config({
        path: dirs,
      })
        .fetch('GET', urlDownload, {})
        .progress((received, total) => {
          let temp = parseFloat(received / total);

          if (temp > 0.9) {
            temp = 1;
            setTimeout(() => {
              this.setState({isDownloading: false});
            }, 1000);
          }
          this.setState(
            {
              isDownloading: true,
              progressLimit: temp,
            },
            () => this.downloadProgressWrapper(),
          );
        })
        .then((res) => {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: `File downloaded successfully ${
              Platform.OS === 'android' ? 'in Download Folder' : ''
            }`,
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });

          this.setState({
            ...this.state,
            downloadStart: false,
          });
          setTimeout(() => {
            this.setState({isDownloading: false});
          }, 1000);
        });
    } catch (error) {}
  };

  navigation = (value, params) => {
    this.setState({likeModalOpen: false, peopleSharedModalOpen: false}, () =>
      this.props.navigation.navigate(value, params),
    );
  };

  peopleSharedModal = () => {
    return (
      <Modal
        visible={this.state.peopleSharedModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({peopleSharedModalOpen: false, peopleShared: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who Shared this post
              </Text>
            </View>

            <SharedUserList
              id={this.state.pressedActivityId}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  attachmentModal = () => {
    return (
      <Modal
        visible={this.state.attachmentModalopen}
        transparent
        animationType="slide"
        supportedOrientation={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({attachmentModalopen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>
          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
              },
            ]}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Attachments{' '}
              </Text>
            </View>
            <FlatList
              keyboardShouldPersistTaps="handled"
              showsVerticalScrollIndicator={false}
              // contentContainerStyle={{ justifyContent: 'flex-start' }}
              style={{height: '60%', width: '100%', marginTop: 6}}
              keyExtractor={(item) => item.id}
              data={this.state.causeItem.secondaryAttachmentIds}
              // initialNumToRender={10}
              renderItem={({item, index}) => (
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Icon
                    name="Clip"
                    color={COLORS.green_500}
                    size={14}
                    style={{
                      marginTop: Platform.OS === 'android' ? 10 : 0,
                      marginRight: 6,
                    }}
                  />
                  <TouchableOpacity
                    onPress={() => {
                      this.readStorage(item.attachmentUrl);
                    }}>
                    <Text>
                      {item.attachmentUrl
                        .substring(item.attachmentUrl.lastIndexOf('/') + 1)
                        .substring(
                          item.attachmentUrl
                            .substring(item.attachmentUrl.lastIndexOf('/') + 1)
                            .indexOf('-') + 1,
                        )}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>
        </View>
      </Modal>
    );
  };

  postModal = () => {
    return (
      <Modal
        visible={this.state.postModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({postModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              },
            ]}>
            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewFeedsPost', {link: false}),
                );
              }}>
              {/* <Icon name="Add_Post" size={44} color={COLORS.primarygreen} style={{ alignSelf: 'center' }} /> */}
              <Image style={{alignSelf: 'center'}} source={postIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    alignSelf: 'center',
                    position: 'absolute',
                    top: 50,
                  },
                ]}>
                Post
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewBlogPost'),
                );
              }}>
              {/* <Icon name='Blog' size={44} color={COLORS.primarygreen} /> */}
              <Image style={{alignSelf: 'center'}} source={blogIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    position: 'absolute',
                    top: 47,
                  },
                ]}>
                Blog
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewFeedsPost', {link: true}),
                );
              }}>
              {/* <Icon name='Link_Post' size={44} color={COLORS.primarygreen} /> */}
              <Image style={{alignSelf: 'center'}} source={linkIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    position: 'absolute',
                    top: 50,
                  },
                ]}>
                Link
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  headerOptionsModal = () => {
    return (
      <Modal
        visible={this.state.headerOptionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({headerOptionsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          {this.props.route.params && this.props.route.params.id !== '' ? (
            <View style={defaultShape.Modal_Categories_Container}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, {paddingVertical: 15}]
                    : styles.termsmodal
                }
                activeOpacity={0.6}
                onPress={() =>
                  this.setState({
                    headerOptionsModalOpen: false,
                    contactInfoModalOpen: true,
                  })
                }>
                <Icon
                  name="Email_At"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text style={styles.modalText}>View Contact Info</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={[
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, {paddingVertical: 15}]
                    : styles.termsmodal,
                  {borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB'},
                ]}
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({headerOptionsModalOpen: false});
                }}>
                <Icon
                  name="Feedback"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? {marginTop: 10} : {}}
                />
                <Text style={styles.modalText}>Endorse</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, {paddingTop: 25, paddingBottom: 15}]
                    : styles.termsmodal
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({headerOptionsModalOpen: false});
                }}>
                <Icon
                  name="Caution"
                  size={16}
                  color="#913838"
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text style={styles.modalText}>Report User</Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View
              style={[
                defaultShape.Modal_Categories_Container,
                {justifyContent: 'space-evenly'},
              ]}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, {paddingVertical: 15}]
                    : styles.termsmodal
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    headerOptionsModalOpen: false,
                    shareModal2Open: true,
                  });
                }}>
                <Icon
                  name="Share"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text style={styles.modalText}>Share Profile Page</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={[
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, {paddingVertical: 15}]
                    : styles.termsmodal,
                  {borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB'},
                ]}
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    headerOptionsModalOpen: false,
                    contactInfoModalOpen: true,
                  });
                }}>
                <Icon
                  name="Email_At"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text style={styles.modalText}>View Contact Info</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, {paddingTop: 25, paddingBottom: 15}]
                    : styles.termsmodal
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({headerOptionsModalOpen: false});
                }}>
                <Icon
                  name="EditBox"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text style={styles.modalText}>Edit Profile</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, {paddingVertical: 15}]
                    : styles.termsmodal
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({headerOptionsModalOpen: false}),
                    this.props.navigation.navigate('ReorderProfileScreen');
                }}>
                <Icon
                  name="TxEdi_Bullet"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text style={styles.modalText}>Reorder Profile page</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, {paddingVertical: 15}]
                    : styles.termsmodal
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({headerOptionsModalOpen: false});
                }}>
                <Icon
                  name="Setting"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text style={styles.modalText}>Privacy & Settings</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [styles.termsmodal, {paddingVertical: 15}]
                    : styles.termsmodal
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({headerOptionsModalOpen: false});
                }}>
                <Icon
                  name="Help"
                  size={16}
                  color="#154A59"
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text style={styles.modalText}>Get Supports</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  };

  handleHideModal = (id) => {
    let data = {
      userId: this.state.userId,
      activityId: id,
      entityType: 'POST',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          console.log('response.status', response.status);
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          console.log('response.status', err.response.status);
        }
      });
    this.setState({
      optionsModalOpen: false,
      activitiesData: this.state.activitiesData.filter(
        (item) => item.id !== id,
      ),
    });
  };

  deletePostAlert = () => {
    this.setState({optionsModalOpen: false});
    Alert.alert('', 'Are you sure you want to delete this post?', [
      {
        text: 'YES',
        onPress: () => this.handleDeleteSubmit(),
        style: 'cancel',
      },
      {
        text: 'NO',
        // onPress: () => console.log('user cancelled deletion'),
      },
    ]);
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({optionsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          {this.props.route.params && this.props.route.params.id !== '' ? (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
                ]}>
                <Text
                  style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                  Post
                </Text>
              </View>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {optionsModalOpen: false, likeModalOpen: true},
                    () => this.getUsersWhoLiked(),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Like_FL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who liked it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    optionsModalOpen: false,
                    peopleSharedModalOpen: true,
                  });
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Share"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who shared it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    optionsModalOpen: false,
                    shareModalOpen: true,
                  });
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Share"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    Share
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.handleHideModal(this.state.pressedActivityId);
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Hide
                </Text>
                <Icon
                  name="Hide"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              {this.state.currentPressed.creatorId === this.state.userId && (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {paddingVertical: 15},
                        ]
                      : [defaultShape.ActList_Cell_Gylph_Alt]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({optionsModalOpen: false}, () =>
                      this.props.navigation.navigate('EditPost', {
                        pressedActivityId: this.state.currentPressed.id,
                        entityType: this.state.currentPressed.postType,
                        description: this.state.currentPressed.description,
                        hashTags: this.state.currentPressed.hashTags,
                      }),
                    );
                  }}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    Edit Post
                  </Text>
                  <Icon
                    name="EditBox"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                </TouchableOpacity>
              )}

              {this.state.canReport ? (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {paddingVertical: 15},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({
                      reasonForReportingModalOpen: !this.state.isReported
                        ? true
                        : false,
                      optionsModalOpen: false,
                    });
                    this.state.isReported
                      ? setTimeout(() => {
                          Snackbar.show({
                            backgroundColor: '#B22222',
                            text: 'Your report request was already taken',
                            duration: Snackbar.LENGTH_LONG,
                          });
                        }, 500)
                      : null;
                  }}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    Report
                  </Text>
                  <Icon
                    name="ReportComment_OL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {paddingVertical: 15},
                        ]
                      : [defaultShape.ActList_Cell_Gylph_Alt]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.deletePostAlert();
                  }}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    Delete Post
                  </Text>
                  <Icon
                    name="Hide"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                </TouchableOpacity>
              )}
            </View>
          ) : (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
                ]}>
                <Text
                  style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                  Post
                </Text>
              </View>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {optionsModalOpen: false, likeModalOpen: true},
                    () => this.getUsersWhoLiked(),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Like_FL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who liked it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    optionsModalOpen: false,
                    shareModalOpen: true,
                  });
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Share"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    Share
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({optionsModalOpen: false});
                }}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  Edit Post
                </Text>
                <Icon
                  name="EditBox"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.handleDeleteSubmit()}
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  Delete Post
                </Text>
                <Icon
                  name="TrashBin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  };

  likeModal = () => {
    return (
      <Modal
        visible={this.state.likeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({likeModalOpen: false, peopleLiked: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Who liked this post
              </Text>
            </View>

            <LikedUserList
              id={this.state.pressedActivityId}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  onShare2 = async () => {
    try {
      const result = await Share.share({
        message: REACT_APP_domainUrl + '/cause/' + this.state.circleData.slug,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  shareModal2 = () => {
    return (
      <Modal
        visible={this.state.shareModal2Open}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModal2Open: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl + '/cause/' + this.state.circleData.slug,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to cause
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModal2Open: false}, () => this.onShare2());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Envelope"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleScroll = (event) => {
    // console.log(event.nativeEvent.contentOffset.y)
    this.setState({currentScrollPosition: event.nativeEvent.contentOffset.y});
  };

  goToTop = () => {
    this.flatListRef.scrollToOffset({animated: true, offset: 0});
  };

  listHeaderComponent = () => {
    return (
      <>
        {this.stickyHeader()}
        <View style={styles.container}>
          <View style={styles.feedDetails}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              alwaysBounceHorizontal={false}>
              <TouchableOpacity
                onPress={() =>
                  this.setState(
                    {
                      activitiesData: [],
                      selected: 'LATEST',
                      pageNumber: 0,
                      pageSize: 5,
                    },
                    // () => this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                    () => this.getUserActivitiesPosts(),
                  )
                }
                style={
                  this.state.selected === 'LATEST'
                    ? styles.selected
                    : styles.notSelected
                }>
                <Text
                  style={
                    this.state.selected === 'LATEST'
                      ? styles.selectedText
                      : styles.notSelectedText
                  }>
                  Latest
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState(
                    {
                      activitiesData: [],
                      selected: 'PHOTOS',
                      pageNumber: 0,
                      pageSize: 5,
                    },
                    // () => this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                    () => this.getUserActivitiesPosts(),
                  )
                }
                style={
                  this.state.selected === 'PHOTOS'
                    ? styles.selected
                    : styles.notSelected
                }>
                <Text
                  style={
                    this.state.selected === 'PHOTOS'
                      ? styles.selectedText
                      : styles.notSelectedText
                  }>
                  Photos
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState(
                    {
                      activitiesData: [],
                      selected: 'VIDEOS',
                      pageNumber: 0,
                      pageSize: 5,
                    },
                    // () => this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                    () => this.getUserActivitiesPosts(),
                  )
                }
                style={
                  this.state.selected === 'VIDEOS'
                    ? styles.selected
                    : styles.notSelected
                }>
                <Text
                  style={
                    this.state.selected === 'VIDEOS'
                      ? styles.selectedText
                      : styles.notSelectedText
                  }>
                  Videos
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState(
                    {
                      activitiesData: [],
                      selected: 'ARTICLES',
                      pageNumber: 0,
                      pageSize: 5,
                    },
                    // () => this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                    () => this.getUserActivitiesPosts(),
                  )
                }
                style={
                  this.state.selected === 'ARTICLES'
                    ? styles.selected
                    : styles.notSelected
                }>
                <Text
                  style={
                    this.state.selected === 'ARTICLES'
                      ? styles.selectedText
                      : styles.notSelectedText
                  }>
                  Blogs
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState(
                    {
                      activitiesData: [],
                      selected: 'AUDIO',
                      pageNumber: 0,
                      pageSize: 5,
                    },
                    // () => this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                    () => this.getUserActivitiesPosts(),
                  )
                }
                style={
                  this.state.selected === 'AUDIO'
                    ? styles.selected
                    : styles.notSelected
                }>
                <Text
                  style={
                    this.state.selected === 'AUDIO'
                      ? styles.selectedText
                      : styles.notSelectedText
                  }>
                  Audio
                </Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
      </>
    );
  };

  trimDescription = (item) => {
    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  // mention auto link
  tagDescription = (item) => {
    item = item.split('@@@__').join('@[');
    item = item.split('^^__').join('](');
    item = item.split('@@@^^^').join(')');
    item = item.split('###__').join('#[');
    item = item.split('&&__').join('](');
    item = item.split('###^^^').join(')');
    return item;
  };

  getUserDetailsByCustomUrl = (customurl) => {
    console.log('initial');
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get_by_custom_url?customUrl=' +
        customurl +
        '&otherUserId=' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        console.log('initial res', response.data.body);
        if (response && response.data && response.data.message === 'Success!') {
          console.log('end res', response.data.body);
          response.data.body &&
          response.data.body.type === 'INDIVIDUAL' &&
          response.data.body.userId === this.state.userId
            ? this.props.navigation.navigate('ProfileStack')
            : response.data.body.type === 'INDIVIDUAL' &&
              response.data.body.userId !== this.state.userId
            ? this.props.navigation.navigate('ProfileStack', {
                screen: 'OtherProfileScreen',
                params: {userId: response.data.body.userId},
              })
            : response.data.body.type === 'COMPANY'
            ? this.props.navigation.navigate('ProfileStack', {
                screen: 'CompanyProfileScreen',
                params: {userId: response.data.body.userId},
              })
            : this.props.navigation.navigate('CircleProfileStack', {
                screen: 'CircleProfile',
                params: {slug: customurl},
              });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  openWebsite = (website) => {
    if (website && website.includes('http')) return Linking.openURL(website);
    else if (website && !website.includes('http'))
      return Linking.openURL('https://' + website);
  };

  renderDescription = (item) => {
    return item.postType === 'ARTICLE' ? (
      <View
        style={{
          backgroundColor:
            item.postType === 'ARTICLE' ? COLORS.altgreen_100 : COLORS.white,
        }}>
        {item.postType === 'ARTICLE' && (
          <Text
            numberOfLines={1}
            style={[
              typography.H6,
              {
                color: COLORS.dark_800,
                marginLeft: 6,
                marginRight: 17,
                marginTop: 5,
              },
            ]}>
            {item.title}
          </Text>
        )}
        <Text
          onPress={() =>
            item.postType === 'ARTICLE'
              ? this.props.navigation.navigate('EditorsDesk', {
                  id: item.id,
                  userId: this.state.userId,
                })
              : this.props.navigation.navigate('IndividualFeedsPost', {
                  id: item.id,
                })
          }
          // onPress={() => this.props.profileRedirectionNavigation('IndividualFeedsPost', { id: item.id })}
          numberOfLines={item.postType === 'ARTICLE' ? 2 : 5}
          style={[
            typography.Body_1,
            {
              color:
                item.postType === 'ARTICLE' ? COLORS.dark_500 : COLORS.dark_700,
              marginLeft: 6,
              marginRight: 17,
              marginVertical: 8,
            },
          ]}>
          {this.trimDescription(item.description)}
        </Text>
      </View>
    ) : (
      <>
        <Autolink
          text={this.trimDescription(this.tagDescription(item.description))}
          email
          hashtag="instagram"
          mention="twitter"
          phone="sms"
          numberOfLines={5}
          matchers={[
            {
              pattern: /@\[([^[]*)]\(([^(^)]*)\)/g,
              style: {color: COLORS.mention_color, fontWeight: 'bold'},
              getLinkText: (replacerArgs) => `${replacerArgs[2]}`,
              onPress: (match) => {
                this.getUserDetailsByCustomUrl(match.getReplacerArgs()[1]);
              },
            },
            {
              pattern: /#\[([^[]*)]\(([^(^)]*)\)/g,
              style: {color: COLORS.mention_color, fontWeight: 'bold'},
              getLinkText: (replacerArgs) => `#${replacerArgs[2]}`,
              onPress: (match) => {
                this.props.navigation.navigate('HashTagDetail', {
                  slug: match.getReplacerArgs()[1],
                });
              },
            },
          ]}
          style={[
            typography.Body_1,
            {
              color:
                item.postType === 'ARTICLE' ? COLORS.dark_500 : COLORS.dark_700,
              marginLeft: 6,
              marginRight: 17,
              marginVertical: 8,
            },
          ]}
          url
        />
      </>
    );
  };

  renderSharedPost = (item, sharedEntityType) => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          this.setState({selectedVideoId: ''});
          sharedEntityType && sharedEntityType === 'FORUM'
            ? this.props.navigation.navigate('ForumStack', {
                screen: 'ForumDetails',
                params: {slug: item.slug, userId: this.state.userId},
              })
            : sharedEntityType && sharedEntityType === 'POLL'
            ? this.props.navigation.navigate('PollStack', {
                screen: 'PollDetails',
                params: {slug: item.slug, userId: this.state.userId},
              })
            : item && item.postType === 'ARTICLE'
            ? this.props.navigation.navigate('EditorsDesk', {
                id: item.id,
                userId: this.state.userId,
              })
            : sharedEntityType === 'JOB' ||
              sharedEntityType === 'EVENT' ||
              sharedEntityType === 'TRAINING' ||
              sharedEntityType === 'ASSIGNMENT' ||
              sharedEntityType === 'EXPEDITION' ||
              sharedEntityType === 'FUNDRAISE' ||
              sharedEntityType === 'STORYBOOK'
            ? this.props.navigation.navigate('ProjectDetailView', {
                slug: item.slug,
              })
            : this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
                commentCount: item.commentCount,
              });
        }}
        style={styles.renderSharedPost}>
        {item.userId !== this.state.userId && this.renderSharedHeader(item)}

        {item.description && item.postType !== 'ARTICLE' ? (
          this.renderDescription(item)
        ) : (
          <View style={{backgroundColor: 'transparent', height: 10}}></View>
        )}

        {item && item.postType === 'LINK' ? (
          <RNUrlPreview
            text={item.postLinkTypeUrl}
            titleNumberOfLines={1}
            titleStyle={[typography.Title_2, {color: COLORS.dark_800}]}
            descriptionStyle={[
              typography.Note,
              {
                fontFamily: 'Montserrat-Medium',
                color: COLORS.altgreen_300,
                marginTop: 8,
              },
            ]}
            containerStyle={{
              marginTop: 15,
              width: '100%',
              flexDirection: 'column',
              backgroundColor: COLORS.altgreen_100,
            }}
            imageStyle={{width: '100%', height: 150}}
            imageProps={{width: '100%', height: 150}}
          />
        ) : (
          <></>
        )}

        {(sharedEntityType === 'EVENT' ||
          sharedEntityType === 'ASSIGNMENT' ||
          sharedEntityType === 'JOB' ||
          sharedEntityType === 'TRAINING' ||
          sharedEntityType === 'EXPEDITION' ||
          sharedEntityType === 'FUNDRAISE' ||
          sharedEntityType === 'STORYBOOK') && (
          <Image
            style={{height: 150, width: '100%'}}
            resizeMode={'contain'}
            source={
              item.coverImageUrl ? {uri: item.coverImageUrl} : BlogDefault
            }
          />
        )}

        <View style={{width: '103%'}}>
          {item && item.attachmentIds && item.attachmentIds.length ? (
            this.renderImage(item)
          ) : (
            <></>
          )}
          {item.question ? (
            <Text style={[typography.Button_Lead, {color: COLORS.dark_800}]}>
              {item.question}
            </Text>
          ) : (
            <Text></Text>
          )}
          {item.title ? (
            <Text style={[typography.Button_Lead, {color: COLORS.dark_800}]}>
              {item.title}
            </Text>
          ) : (
            <Text></Text>
          )}

          {item.addressDetail && item.addressDetail.country ? (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignSelf: 'flex-start',
                marginTop: 10,
              }}>
              <Icon
                name="Location"
                color={COLORS.altgreen_300}
                size={10}
                style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
              />

              <Text
                style={[
                  {
                    color: COLORS.altgreen_300,
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                    maxWidth: 200,
                  },
                ]}>
                {item.addressDetail.country}
                {/* {+ new Date() - item.createTime} */}
              </Text>
            </View>
          ) : null}

          {item.shortBrief && (
            <Autolink
              text={this.trimDescription(item.shortBrief)}
              email
              hashtag="instagram"
              mention="twitter"
              style={[
                typography.Body_1,
                {
                  color:
                    item.postType === 'ARTICLE'
                      ? COLORS.dark_500
                      : COLORS.dark_700,
                  marginRight: 17,
                  marginVertical: 8,
                },
              ]}
              phone="sms"
              url
            />
          )}

          {item.description && item.postType === 'ARTICLE' ? (
            this.renderDescription(item)
          ) : (
            <Text></Text>
          )}
        </View>

        <View style={{marginLeft: 8}}>
          {item.secondaryAttachmentIds &&
          item.secondaryAttachmentIds.length > 0 ? (
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}
              // onPress={() => this.setState({ attachmentModalopen: true, causeItem: item })}
              onPress={() => {
                this.setState({selectedVideoId: ''});
                item && item.postType === 'ARTICLE'
                  ? this.props.navigation.navigate('EditorsDesk', {
                      id: item.id,
                      userId: this.state.userId,
                    })
                  : this.props.navigation.navigate('IndividualFeedsPost', {
                      id: item.id,
                      commentCount: item.commentCount,
                    });
              }}>
              <Icon
                name="Clip"
                color={COLORS.green_500}
                size={14}
                style={{
                  marginTop: Platform.OS === 'android' ? 10 : 0,
                  marginRight: 6,
                }}
              />
              <Text style={[{color: COLORS.altgreen_300}, defaultStyle.Note2]}>
                {item.secondaryAttachmentIds.length === 1
                  ? item.secondaryAttachmentIds.length + ' File '
                  : item.secondaryAttachmentIds.length + ' Files '}{' '}
                Attached
              </Text>
            </TouchableOpacity>
          ) : (
            <Text></Text>
          )}
        </View>

        {item &&
        item.hashTags &&
        item.hashTags.length &&
        item.postType !== 'ARTICLE' ? (
          this.renderHashTags(item)
        ) : (
          <></>
        )}
      </TouchableOpacity>
    );
  };

  renderHashTags = (item) => {
    return (
      <ScrollView
        contentContainerStyle={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          marginLeft: 6,
          marginTop: 8,
        }}>
        {item.hashTags.map((hashTag, index) => (
          <View key={index}>
            <Text
              style={[
                defaultStyle.Subtitle_2,
                {color: COLORS.link_blue, marginRight: 8},
              ]}>
              #{hashTag}
            </Text>
          </View>
        ))}
      </ScrollView>
    );
  };

  handleSupportStatusChange() {
    let postBody = {
      userId: this.state.userId,
      causeId: [this.props.route.params.id],
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/public/cause/join',
      headers: {'Content-Type': 'application/json'},
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        // console.log('SSSSSSSSSSSSSS', res.status)
        if (res.status === '201 CREATED') {
          this.setState({supporting: true});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  removeCause() {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/cause/delete?id=' +
        this.props.route.params.id +
        '&userId=' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        // console.log('DDDDDDDDDDDDD', response.data.message)
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({supporting: false});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  stickyHeader = () => {
    return (
      <View style={{backgroundColor: COLORS.white, zIndex: 2}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: COLORS.white,
            alignSelf: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
              alignSelf: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                borderRadius: 50,
                alignSelf: 'center',
              }}>
              {/* {(this.props.user.body && this.props.user.body.originalProfileImage) ?
                                <Image source={{ uri: this.props.user.body.originalProfileImage }} style={{ width: 30, height: 30, borderRadius: 15 }} />
                                : null} */}
              <ImageBackground
                imageStyle={{borderRadius: 10}}
                source={
                  this.state.circleData && this.state.circleData.imageUrl
                    ? {uri: this.state.circleData.imageUrl}
                    : defaultCover
                }
                style={{
                  width: '98%',
                  height: 200,
                  borderRadius: 50,
                  alignSelf: 'center',
                  marginLeft: '2%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'flex-end',
                }}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => this.props.navigation.goBack()}
                  style={{
                    height: 50,
                    width: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    position: 'absolute',
                    bottom: 156,
                    left: -8,
                  }}>
                  <Icon
                    name="Arrow-Left"
                    size={17}
                    color={COLORS.white}
                    style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
                  />
                </TouchableOpacity>

                <View
                  style={{
                    marginTop: '20%',
                    width: '90%',
                    alignSelf: 'flex-end',
                    paddingLeft: 10,
                    paddingBottom: 8,
                  }}>
                  <Text
                    style={[
                      defaultStyle.H3,
                      {color: COLORS.white, fontSize: 23},
                    ]}>
                    {this.state.circleData && this.state.circleData.name
                      ? this.state.circleData.name.toUpperCase()
                      : null}
                  </Text>

                  <View
                    contentContainerStyle={{
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      marginTop: 4,
                    }}>
                    {this.state.circleData && this.state.circleData.hashtag
                      ? this.state.circleData.hashtag
                          .slice(0, 4)
                          .map((hashTag, index) => (
                            <View key={index}>
                              <Text
                                numberOfLines={1}
                                style={[
                                  defaultStyle.Subtitle_2,
                                  {color: COLORS.link_blue, marginRight: 8},
                                ]}>
                                #{hashTag}
                              </Text>
                            </View>
                          ))
                      : null}
                    {this.state.circleData &&
                    this.state.circleData.hashtag &&
                    this.state.circleData.hashtag.length > 4 ? (
                      <Text>....</Text>
                    ) : (
                      <></>
                    )}
                  </View>

                  <TouchableOpacity
                    onPress={() =>
                      this.state.supporting
                        ? this.removeCause()
                        : this.handleSupportStatusChange()
                    }
                    style={{
                      flexDirection: 'row',
                      paddingHorizontal: 10,
                      height: 30,
                      borderRadius: 4,
                      backgroundColor: this.state.supporting
                        ? COLORS.altgreen_300
                        : COLORS.green_500,
                      justifyContent: 'center',
                      alignItems: 'center',
                      minWidth: 100,
                      maxWidth: 150,
                    }}
                    activeOpacity={0.9}>
                    {this.state.supporting && (
                      <Icon
                        name="FollowTick"
                        size={14}
                        color={COLORS.white}
                        style={{
                          marginTop: Platform.OS === 'android' ? 8 : 0,
                          marginRight: 6,
                        }}
                      />
                    )}
                    <Text
                      style={[
                        defaultStyle.Button_Lead,
                        {
                          color: this.state.supporting
                            ? COLORS.white
                            : COLORS.white,
                          fontSize: 12,
                        },
                      ]}>
                      {this.state.supporting ? 'SUPPORTING' : 'SUPPORT CAUSE'}
                    </Text>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => this.setState({shareModal2Open: true})}>
                  <Icon
                    name="Share"
                    size={17}
                    color={COLORS.green_500}
                    style={{
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                      marginRight: 26,
                    }}
                  />
                </TouchableOpacity>
              </ImageBackground>
            </View>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            justifyContent: 'space-evenly',
            width: 240,
          }}></View>
      </View>
    );
  };

  getUserActivitiesPosts() {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/post/cause/getNewsFeed?causeId=' +
        this.props.route.params.id +
        '&userId=' +
        this.state.userId +
        '&newsFeedType=' +
        this.state.selected +
        '&page=' +
        this.state.pageNumber +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        console.log('about data', response);
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          this.setState({
            activitiesData: this.state.activitiesData.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation.navigate('RepostOnWenat', {
                    link: false,
                    pressedActivityId: this.state.pressedActivityId,
                    entityType: 'POST',
                  }),
                );
              }}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl + '/post/' + this.state.pressedActivityId,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            {/* <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false});
              }}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Share through Mail
              </Text>
              <Icon
                name="Mail_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity> */}

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Envelope"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  contactInfoModal = () => {
    return (
      <Modal
        visible={this.state.contactInfoModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({contactInfoModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          {this.props.user.body && (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  {
                    justifyContent: 'center',
                    paddingBottom: 10,
                    paddingTop: 0,
                    borderBottomWidth: 0,
                  },
                ]}>
                <Text
                  style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                  CONTACT INFO
                </Text>
              </View>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {
                          paddingTop: 25,
                          paddingBottom: 15,
                          borderBottomWidth: 0,
                          flexDirection: 'column',
                          alignItems: 'flex-start',
                        },
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {
                          flexDirection: 'column',
                          alignItems: 'flex-start',
                          borderBottomWidth: 0,
                          paddingVertical: 12,
                        },
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({contactInfoModalOpen: false});
                }}>
                <Text style={[defaultStyle.Button_2, {color: COLORS.dark_600}]}>
                  Phone Number :
                </Text>
                <Text
                  style={[
                    defaultStyle.Caption,
                    {color: COLORS.dark_600, fontSize: 12.5},
                  ]}>
                  +{this.props.user.body.countryCode}{' '}
                  {this.props.user.body.mobile}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {
                          paddingTop: 25,
                          paddingBottom: 15,
                          borderBottomWidth: 0,
                          flexDirection: 'column',
                          alignItems: 'flex-start',
                        },
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {
                          flexDirection: 'column',
                          alignItems: 'flex-start',
                          borderBottomWidth: 0,
                          paddingVertical: 12,
                        },
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({contactInfoModalOpen: false});
                }}>
                <Text style={[defaultStyle.Button_2, {color: COLORS.dark_600}]}>
                  Email :
                </Text>
                <Text
                  style={[
                    defaultStyle.Caption,
                    {color: COLORS.dark_600, fontSize: 12.5},
                  ]}>
                  {this.props.user.body.email}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {
                          paddingTop: 25,
                          paddingBottom: 15,
                          borderBottomWidth: 0,
                          flexDirection: 'column',
                          alignItems: 'flex-start',
                        },
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {
                          flexDirection: 'column',
                          alignItems: 'flex-start',
                          borderBottomWidth: 0,
                          paddingVertical: 12,
                        },
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({contactInfoModalOpen: false});
                }}>
                <Text style={[defaultStyle.Button_2, {color: COLORS.dark_600}]}>
                  Address :
                </Text>
                <Text
                  style={[
                    defaultStyle.Caption,
                    {color: COLORS.dark_600, fontSize: 12.5},
                  ]}>
                  {this.props.user.body.city},{' '}
                  {this.props.route.params
                    ? this.props.route.params.state
                    : null}
                  , {this.props.user.body.country}
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  };

  handleDeleteSubmit = () => {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/backend/post/delete/' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          // console.log(response.status)
        }
      })
      .catch((err) => {
        // console.log(err)
      });
    this.setState({
      optionsModalOpen: false,
      feedsData: this.state.activitiesData.filter(
        (item) => item.id !== this.state.pressedActivityId,
      ),
    });
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {height: 700, bottom: 0},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={[defaultShape.CloseBtn, {marginBottom: 0}]}
            onPress={() =>
              this.setState({
                reasonForReportingModalOpen: false,
                reasonForReporting: '',
              })
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.altgreen_100,
              alignItems: 'center',
              paddingTop: 15,
              paddingBottom: 10,
              // paddingLeft: 20,
              width: '90%',
              alignSelf: 'center',
              marginBottom: 30,
              marginTop: 15,
            }}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Reason for reporting
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '94%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting:
                    'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting ===
                  'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Inappropriate, abusive or offensive content
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting: 'CONTENT_PROMOTING_VIOLENCE_OR_TERRORISM',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting ===
                  'CONTENT_PROMOTING_VIOLENCE_OR_TERRORISM' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Content promoting violence or terrorism
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingVertical: 15, borderBottomWidth: 0},
                    ]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'FAKE_SPAM_OR_SCAM'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Fake, spam or scam
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'ACCOUNT_MAY_BE_HACKED'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Account may be hacked
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '90.5%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting:
                    'DEFAMATION_TRADEMARK_OR_COPYRIGHT_VIOLATION',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting ===
                  'DEFAMATION_TRADEMARK_OR_COPYRIGHT_VIOLATION' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  numberOfLines={2}
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Defamation, trademark or copyright violation
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'HARASSMENT_OR_THREAT'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'HARASSMENT_OR_THREAT' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Harassment or threat
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'OTHERS'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'OTHERS' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Others
                </Text>
              </View>
            </TouchableOpacity>

            {this.state.reasonForReporting === 'OTHERS' && (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label="Write the details"
                  multiline
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Subtitle_1,
                    {
                      width: '90%',
                      height: 56,
                      backgroundColor: COLORS.altgreen_100,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) => this.setState({description: value})}
                  value={this.state.description}
                />
              </View>
            )}

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                this.state.reasonForReporting === 'OTHERS' &&
                this.state.description === ''
                  ? Snackbar.show({
                      backgroundColor: '#B22222',
                      text: 'Please enter the detail',
                      duration: Snackbar.LENGTH_LONG,
                    })
                  : this.state.reasonForReporting === ''
                  ? Snackbar.show({
                      backgroundColor: '#B22222',
                      text: 'Please select an option',
                      duration: Snackbar.LENGTH_LONG,
                    })
                  : (this.handleReportAbuseSubmit(),
                    this.setState({reasonForReportingModalOpen: false}));
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 15,
                height: 27,
                marginVertical: 10,
                borderRadius: 16,
                textAlign: 'center',
                borderWidth: 1,
                borderColor: '#698F8A',
              }}>
              <Text
                style={{
                  color: '#698F8A',
                  fontSize: 14,
                  paddingHorizontal: 14,
                  paddingVertical: 20,
                  fontFamily: 'Montserrat-Medium',
                  fontWeight: 'bold',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedActivityId,
      // entityType: this.state.entityType,
      entityType: 'POST',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    setTimeout(() => {
      this.setState({
        reasonForReporting: 'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT',
        description: '',
      });
    }, 1000);
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.postModal()}
        {this.optionsModal()}
        {this.shareModal()}
        {this.likeModal()}
        {this.headerOptionsModal()}
        {this.shareModal2()}
        {this.reasonForReportingModal()}
        {this.contactInfoModal()}
        {this.attachmentModal()}
        {this.peopleSharedModal()}

        <View style={styles.container}>
          {this.state.activitiesData ? (
            <FlatList
              ref={(ref) => {
                this.flatListRef = ref;
              }}
              ListHeaderComponent={this.listHeaderComponent()}
              // onScroll={this.handleScroll}
              style={{backgroundColor: COLORS.bgFill_200, marginTop: 0}}
              showsVerticalScrollIndicator={false}
              alwaysBounceVertical={false}
              keyExtractor={(item) => item.id}
              // data={this.props.userFeedsPhotos.body.content.filter((item) => item.description.indexOf('@@@__') > -1)}
              data={
                this.state.activitiesData.length
                  ? this.state.activitiesData
                  : [0]
              }
              onEndReached={this.handleLoadMore}
              onEndReachedThreshold={5}
              renderItem={({item, index}) => {
                return this.state.activitiesData.length ? (
                  <View style={styles.renderItemStyle}>
                    {this.renderHeader(item, index)}
                    {item && item.description && item.postType !== 'ARTICLE' ? (
                      this.renderDescription(item)
                    ) : (
                      <View
                        style={{
                          backgroundColor: 'transparent',
                          height: 10,
                        }}></View>
                    )}
                    {item.postType === 'LINK' ? (
                      <RNUrlPreview
                        text={item.postLinkTypeUrl}
                        titleNumberOfLines={1}
                        titleStyle={[
                          typography.Title_2,
                          {color: COLORS.dark_800},
                        ]}
                        descriptionStyle={[
                          typography.Note,
                          {
                            fontFamily: 'Montserrat-Medium',
                            color: COLORS.altgreen_300,
                            marginTop: 8,
                          },
                        ]}
                        containerStyle={{
                          marginTop: 15,
                          width: '100%',
                          flexDirection: 'column',
                          backgroundColor: COLORS.altgreen_100,
                        }}
                        imageStyle={{width: '100%', height: 150}}
                        imageProps={{width: '100%', height: 150}}
                      />
                    ) : (
                      <></>
                    )}

                    {item && item.sharedEntityId ? (
                      this.renderSharedPost(
                        item.sharedEntityParams,
                        item.sharedEntityType,
                      )
                    ) : item && item.attachmentIds.length ? (
                      this.renderImage(item)
                    ) : item.postType === 'ARTICLE' ? (
                      <Image
                        source={BlogDefault}
                        style={{width: '100%', height: 150}}
                      />
                    ) : (
                      <></>
                    )}

                    {item && item.description && item.postType === 'ARTICLE' ? (
                      this.renderDescription(item)
                    ) : (
                      <View
                        style={{
                          backgroundColor: 'transparent',
                          height: 10,
                        }}></View>
                    )}

                    {item.postLocation ? (
                      <Text
                        style={[
                          typography.Note2,
                          {
                            color: COLORS.grey_350,
                            marginLeft: 6,
                            marginRight: 17,
                            marginTop: 5,
                          },
                        ]}>
                        <Icon
                          name="Location"
                          color={COLORS.grey_350}
                          size={10}
                          style={{
                            marginTop: Platform.OS === 'android' ? -2 : 0,
                          }}
                        />
                        {item.postLocation}
                      </Text>
                    ) : (
                      <></>
                    )}

                    {item.hashTags && item.hashTags.length ? (
                      this.renderHashTags(item)
                    ) : (
                      <></>
                    )}

                    {this.renderFooter(item)}
                  </View>
                ) : this.state.supporting ? (
                  <Text
                    style={[
                      typography.Subtitle_2,
                      {
                        color: '#698f8a',
                        marginTop: 100,
                        alignSelf: 'center',
                        fontSize: 14,
                      },
                    ]}>
                    <Text
                      onPress={() => this.setState({postModalOpen: true})}
                      style={[
                        typography.Body_1_bold,
                        {
                          textDecorationLine: 'underline',
                          color: '#97a600',
                          fontSize: 14,
                        },
                      ]}>
                      Share
                    </Text>{' '}
                    your thoughts
                  </Text>
                ) : (
                  <></>
                );
              }}
            />
          ) : (
            <></>
          )}

          <TouchableOpacity
            onPress={this.goToTop}
            activeOpacity={0.7}
            style={styles.floatingIcon2}>
            <Icon
              name="Arrow-Up"
              size={18}
              color="#00394D"
              style={styles.editIcon2}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.setState({postModalOpen: true})}
            activeOpacity={0.7}
            style={styles.floatingIcon}>
            <Icon
              name="EditBox"
              size={18}
              color="#00394D"
              style={styles.editIcon}
            />
            <Text style={styles.floatingIconText}>POST</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#00394D',
    flex: 1,
  },
  modalText: {
    fontSize: 14,
    color: '#154A59',
    fontFamily: 'Montserrat-SemiBold',
    marginLeft: 14,
  },
  termsmodal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '85%',
    paddingVertical: 5.5,
    paddingLeft: 30,
  },
  renderSharedPost: {
    width: '94%',
    marginTop: 5,
    marginLeft: 6,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: COLORS.grey_300,
    padding: 10,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  renderItemStyle: {
    backgroundColor: COLORS.white,
    paddingLeft: 10,
    paddingVertical: 10,
    marginHorizontal: 10,
    marginVertical: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: COLORS.grey_300,
  },
  unreadNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderRadius: 8,
    // marginTop: 50
    // borderBottomColor: COLORS.grey_400,
    // borderBottomWidth: 0.3,
    // height: 44
  },
  scrollViewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editIcon: {
    marginTop: 8,
    marginRight: 8,
  },
  editIcon2: {
    marginTop: Platform.OS === 'android' ? 8 : 0,
    // marginRight: 8
  },
  floatingIconText: {
    color: '#00394D',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 2,
  },
  floatingIcon: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 113,
    height: 44,
    borderRadius: 40,
    backgroundColor: '#D8DE21',
    position: 'absolute',
    bottom: 16,
    right: 12,
  },
  floatingIcon2: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 28,
    height: 42,
    borderRadius: 40,
    backgroundColor: '#1A4D5F80',
    position: 'absolute',
    bottom: 66,
    right: 12,
  },
  feedDetails: {
    marginTop: 20,
    marginLeft: 20,
    paddingBottom: 16,
  },
  selected2: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.white,
    marginRight: 15,
    height: 224,
    width: 204,
    borderRadius: 8,
    textAlign: 'center',
  },
  notSelected2: {
    flex: 1,
    // paddingBottom: 6,
    marginRight: 15,
    // height: 224,
    width: 204,
    borderWidth: 1,
    borderColor: '#698F8A',
    borderRadius: 8,
    backgroundColor: COLORS.white,
  },
  selected: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#367681',
    marginRight: 15,
    height: 27,
    // width: 66,
    borderRadius: 16,
    textAlign: 'center',
  },
  notSelected: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    height: 27,
    // width: 66,
    borderWidth: 1,
    borderColor: '#698F8A',
    borderRadius: 16,
  },
  selectedText: {
    color: '#F7F7F5',
    fontSize: 12,
    paddingHorizontal: 14,
    fontFamily: 'Montserrat-Medium',
  },
  notSelectedText: {
    color: '#698F8A',
    fontSize: 12,
    paddingHorizontal: 14,
    fontFamily: 'Montserrat-Medium',
  },
  searchBar: {
    // marginTop: 8
  },
  explore: {
    flexDirection: 'row',
    paddingTop: 8,
    marginLeft: 15,
  },
  feed: {
    flexDirection: 'row',
    paddingTop: 8,
    marginLeft: 20,
    borderBottomColor: '#BFC52E',
    borderBottomWidth: 4,
    width: 100,
  },
  header: {
    flexDirection: 'row',
    borderBottomColor: '#154A59',
    borderBottomWidth: 1,
    paddingTop: 15,
    height: 65,
  },
  feedText: {
    color: '#FFFFFF',
    fontSize: 22,
    marginLeft: 5,
    fontFamily: 'Montserrat-SemiBold',
  },
  exploreText: {
    color: '#91B3A2',
    fontSize: 22,
    marginLeft: 5,
    fontFamily: 'Montserrat-Regular',
  },
});

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,

    userFeedsPhotosProgress: state.feedsReducer.userFeedsPhotosProgress,
    userFeedsPhotos: state.feedsReducer.userFeedsPhotos,
    errorFeedsPhotos: state.feedsReducer.errorFeedsPhotos,

    userFeedsVideosProgress: state.feedsReducer.userFeedsVideosProgress,
    userFeedsVideos: state.feedsReducer.userFeedsVideos,
    errorFeedsVideos: state.feedsReducer.errorFeedsVideos,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
    feedsPhotosRequest: (data) => dispatch(feedsPhotosRequest(data)),
    feedsVideosRequest: (data) => dispatch(feedsVideosRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CausesDetail);
