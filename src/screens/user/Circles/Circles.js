import React, { Component } from 'react'
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, FlatList, Image, ActivityIndicator, Platform } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'

import { REACT_APP_userServiceURL } from '../../../../env.json'
import circleDefault from '../../../../assets/CirclesDefault.png'
import defaultCover from '../../../../assets/defaultCover.png'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import typography from '../../../Components/Shared/Typography'
import icoMoonConfig from '../../../../assets/Icons/selection.json'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class Circles extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            userId: '',
            suggestedCircleData: [],
            pageSize: 10,
            followbtnid: ''
        };
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {

            this.setState({ userId: value })

            this.getSuggestedCircle(value)
            // this.props.personalProfileRequest({ userId: value, otherUserId: '' })
            // this.props.personalAddressRequest({ userId: value, otherUserId: '' })

        }).catch((e) => {
            console.log(e)
        })
    }

    getSuggestedCircle = (userId) => {
        let postBody = {
            "userId": userId
        }
        axios({
            method: "post",
            url: REACT_APP_userServiceURL + "/circle/unrelated-recommended?page=0" + "&size=" + this.state.pageSize,
            headers: { "Content-Type": "application/json" },
            data: postBody,
            withCredentials: true
        }).then((response) => {
            if (response && response.data && response.data.message === "Success!") {
                this.setState({ isLoading: false, suggestedCircleData: response.data.body.content })
                // console.log("circle suggesstion: ", response.data.body.content)
            }
        }).catch((err) => {
            this.setState({ isLoading: false })
            console.log(err)
        });
    }

    handleFollowCircle = (id) => {
        this.setState({ isLoading: true, followbtnid: id })
        let postBody = {
            userId: this.state.userId,
            entityId: id,
            entityType: 'CIRCLE'
        }
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/entity/follow',
            data: postBody,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.status === 202) {
                this.getSuggestedCircle(this.state.userId)
                this.setState({ isLoading: false })
            } else {
                this.setState({ isLoading: false })
            }
        }).catch((err) => {
            this.setState({ isLoading: false })
            console.log(err);
        })
    }

    handleLoadmore = () => {
        console.log('object')
        this.setState({ pageSize: this.state.pageSize + 10 }, () => this.getSuggestedCircle(this.state.userId))
    }

    renderCircleItem = (item) => {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate("CircleProfileStack", {
                screen: 'CircleProfile',
                params: { slug: item.item.slug },
            })}
                activeOpacity={0.8} style={styles.circleItem}>
                <Image source={item.item.resizedCoverImages ? { uri: item.item.resizedCoverImages.compressed } : defaultCover}
                    style={{ width: '100%', height: 80, borderTopLeftRadius: 8, borderTopRightRadius: 8 }} />
                <Image source={item.item.profileImage ? { uri: item.item.profileImage } : circleDefault}
                    style={{ width: 60, height: 60, borderRadius: 30, position: 'absolute', top: 33 }} />

                <Text numberOfLines={1} style={[typography.Body_1, { marginTop: 20, color: COLORS.dark_900, maxWidth: '90%' }]}>
                    {item.item.title}
                </Text>

                <TouchableOpacity onPress={() => this.handleFollowCircle(item.item.id)}
                    style={styles.followBtn}>
                    {this.state.isLoading && item.item.id === this.state.followbtnid ?
                        <ActivityIndicator color={COLORS.white} size="small" />
                        :
                        <Text style={[typography.Caption, { color: COLORS.white }]}>Follow</Text>}
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }

    headerFlatlist = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("MyCircle")} activeOpacity={0.5} style={styles.mycircle}>
                    <View style={{ marginLeft: 15, flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name="Circles_Fl" color={COLORS.altgreen_200} size={18} style={{ marginTop: Platform.OS === 'ios' ? 0 : 10 }} />
                        <Text style={[typography.Button_Lead, { color: COLORS.white, marginLeft: 10 }]}>My Circles</Text>
                    </View>
                    <View style={defaultShape.Nav_Gylph_Btn}>
                        <View style={{ height: 16, width: 16, borderRadius: 8, backgroundColor: COLORS.dark_500, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name="Arrow_Right" color={COLORS.dark_700} size={10} style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }} />
                        </View>
                    </View>
                </TouchableOpacity>

                {/* <View style={{ width: '100%', marginTop: 15, flexDirection: 'row', alignSelf: 'center' }}>
                    <TouchableOpacity style={styles.searchbar}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Icon name="Search" color={COLORS.grey_350} size={16} style={{ marginTop: Platform.OS === 'ios' ? 0 : 8 }} />
                            <Text style={[typography.Button_Lead, { color: COLORS.grey_350, marginLeft: 8 }]}>Search</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={defaultShape.Nav_Gylph_Btn}>
                        <Icon name="FilterSlide" color={COLORS.altgreen_400} size={18} />
                    </TouchableOpacity>
                </View> */}

                <Text style={[typography.Button_2, { color: COLORS.dark_800, marginTop: 20 }]}>CIRCLES FOR YOU</Text>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                {/******** Header starts ********/}

                <View style={styles.header}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                        style={defaultShape.Nav_Gylph_Btn}>
                        <Icon name="Arrow-Left" color={COLORS.altgreen_400} size={16} style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }} />
                    </TouchableOpacity>

                    <View style={{ alignItems: 'center', marginRight: 30 }}>
                        <View style={[defaultShape.Media_Round, { backgroundColor: COLORS.dark_600, alignItems: 'center', justifyContent: 'center' }]}>
                            <Icon name="Circle_Ol" color={COLORS.altgreen_200} size={16} style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }} />
                        </View>
                        <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Circles</Text>
                    </View>

                    <View></View>
                </View>

                {/******** Header ends ********/}

                <View style={{ width: '90%', alignSelf: 'center' }}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.suggestedCircleData}
                        keyExtractor={(item) => item.id}
                        contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap', paddingBottom: 100, justifyContent: 'center' }}
                        ListHeaderComponent={this.headerFlatlist}
                        ListHeaderComponentStyle={{ width: '100%' }}
                        onEndReached={this.handleLoadmore}
                        onEndReachedThreshold={2}
                        renderItem={(item) => this.renderCircleItem(item)}
                    />
                </View>

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        height: 64,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: COLORS.white,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    mycircle: {
        width: '100%',
        height: 44,
        backgroundColor: COLORS.dark_700,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'center',
        marginTop: 15,
        alignItems: 'center',
        borderRadius: 8
    },
    searchbar: {
        width: '90%',
        height: 40,
        borderRadius: 8,
        backgroundColor: COLORS.white,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.8,
        borderColor: COLORS.grey_300
    },
    circleItem: {
        height: 163,
        width: 148,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        alignItems: 'center',
        marginTop: 15,
        marginHorizontal: 7
    },
    followBtn: {
        width: 78,
        height: 28,
        borderRadius: 4,
        marginTop: 8,
        backgroundColor: COLORS.green_400,
        justifyContent: 'center',
        alignItems: 'center'
    }
})