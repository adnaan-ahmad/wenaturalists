import React, { Component } from 'react'
import { Clipboard, Share, Modal, ScrollView, TextInput, Dimensions, FlatList, View, Text, TouchableOpacity, Image, StyleSheet, Platform, SafeAreaView } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'
import axios from 'axios'
import Snackbar from 'react-native-snackbar'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'

import circleDefault from '../../../../assets/CirclesDefault.png'
import { userCircleRequest } from '../../../services/Redux/Actions/User/CircleActions'
import { REACT_APP_userServiceURL, REACT_APP_domainUrl } from '../../../../env.json'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultCover from '../../../../assets/defaultCover.png'
import defaultShape from '../../../Components/Shared/Shape'
import typography from '../../../Components/Shared/Typography'
import { COLORS } from '../../../Components/Shared/Colors'
import postIcon from '../../../../assets/Post_Icon.png'
import blogIcon from '../../../../assets/Blog_Icon.png'
import linkIcon from '../../../../assets/Link_Icon.png'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

const screenHeight = Dimensions.get('window').height

class MyCircles extends Component {

  constructor(props) {
    super(props)

    this.state = {
      users: [],
      selectedPage: '',
      name: '',
      searchText: '',
      selectedContactsState: [],
      searchIcon: true,
      userId: '',
      optionsModalOpen: false,
      pressedUserId: '',
      following: 0,
      isConnected: '',
      reasonForReportingModalOpen: false,
      reasonForReporting: 'FAKE_SPAM_OR_SCAM',
      description: '',
      postModalOpen: false,
      sortBy: 'firstName',
      originalEmployees: [],
      shareModalOpen: false
    }
  }

  componentDidMount() {

    this.setState({ originalEmployees: this.props.originalEmployees })

    AsyncStorage.getItem("refreshToken").then((value) => {
      if (value === null) {
        this.props.navigation.replace("Login")
      }
    })

    AsyncStorage.getItem("userId").then((value) => {
      this.setState({ userId: value })
      this.props.userCircleRequest({ userId: value, otherUserId: '' })

    }).catch((e) => {
      console.log(e)
    })

    // this.iterateCountry()

  }

  searchNames = (query) => {

    const regex = new RegExp(`${query.trim()}`, 'i')

    if (this.state.sortBy === 'firstName' && this.props.userCircle.body) {
      return this.props.userCircle.body.content.sort((a, b) => a.title.toUpperCase() > b.title.toUpperCase()).filter(item => (item.title).search(regex) >= 0)
    }
    else if (this.state.sortBy === 'lastName' && this.props.userCircle.body) {
      return this.props.userCircle.body.content.sort((a, b) => a.title.toUpperCase().split(' ')[1] > b.title.toUpperCase().split(' ')[1]).filter(item => (item.title).search(regex) >= 0)
    }
    else if (this.state.sortBy === 'recentlyAdded' && this.props.userCircle.body) {
      return this.props.userCircle.body.content.sort((a, b) => a.createTime < b.createTime).filter(item => (item.title).search(regex) >= 0)
    }
    else if (this.state.sortBy === 'country' && this.props.userCircle.body) {
      let array = this.props.userCircle.body.content.sort((a, b) => {

        return (a.location === null) - (b.location === null)
      })
      let array2 = array.sort((a, b) => {

        return a.location && b.location ? a.location.toUpperCase() > b.location.toUpperCase() : a
      })
      return array2.filter(item => (item.title).search(regex) >= 0)
    }
  }

  postModal = () => {

    return (

      <Modal visible={this.state.postModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>

          <View style={[defaultShape.Linear_Gradient_View, { bottom: 100 }]}>
            <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
            onPress={() => this.setState({ postModalOpen: false })} >
            <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
          </TouchableOpacity>

          <Text style={[typography.Caption, { fontSize: 12, color: COLORS.dark_500, textAlign: 'center', alignSelf: 'center', position: 'absolute', bottom: 110, zIndex: 2 }]}>Sort By</Text>

          <View style={[defaultShape.Modal_Categories_Container, { paddingTop: 10, height: 140, flexDirection: 'row', justifyContent: 'space-evenly' }]}>

            <TouchableOpacity style={this.state.sortBy === 'firstName' ? styles.selectedText : styles.unselectedText} activeOpacity={0.6} onPress={() => { this.setState({ postModalOpen: false, sortBy: 'firstName' }) }} >

              <Text style={[typography.Caption, { fontSize: 11, color: this.state.sortBy === 'firstName' ? COLORS.altgreen_200 : COLORS.dark_500, textAlign: 'center', alignSelf: 'center' }]}>First Name</Text>
            </TouchableOpacity>

            <TouchableOpacity style={this.state.sortBy === 'lastName' ? styles.selectedText : styles.unselectedText} activeOpacity={0.6} onPress={() => { this.setState({ postModalOpen: false, sortBy: 'lastName' }) }}>

              <Text style={[typography.Caption, { fontSize: 11, color: this.state.sortBy === 'lastName' ? COLORS.altgreen_200 : COLORS.dark_500, textAlign: 'center', alignSelf: 'center' }]}>Last Name</Text>
            </TouchableOpacity>

            <TouchableOpacity style={this.state.sortBy === 'recentlyAdded' ? styles.selectedText : styles.unselectedText} activeOpacity={0.6} onPress={() => { this.setState({ postModalOpen: false, sortBy: 'recentlyAdded' }) }}>

              <Text style={[typography.Caption, { fontSize: 11, color: this.state.sortBy === 'recentlyAdded' ? COLORS.altgreen_200 : COLORS.dark_500, textAlign: 'center', alignSelf: 'center' }]}>Recently Added</Text>
            </TouchableOpacity>

            <TouchableOpacity style={this.state.sortBy === 'country' ? styles.selectedText : styles.unselectedText} activeOpacity={0.6} onPress={() => { this.setState({ postModalOpen: false, sortBy: 'country' }) }}>

              <Text style={[typography.Caption, { fontSize: 11, color: this.state.sortBy === 'country' ? COLORS.altgreen_200 : COLORS.dark_500, textAlign: 'center', alignSelf: 'center' }]}>Country</Text>
            </TouchableOpacity>

          </View>

        </View>

      </Modal>

    )
  }

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          REACT_APP_domainUrl + '/profile/' + this.state.customUrl,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType')
        } else {
          console.log('shared')
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed')
      }
    } catch (error) {
      console.log(error.message)
    }
  }

  render() {
    // console.log('------------------------------ this.props.userCircle.body[10] -------------------------------', this.props.userCircle.body && this.props.userCircle.body.content[10])

    return (

      <SafeAreaView style={{ height: '100%' }}>

        {this.postModal()}


        {/******** Header starts ********/}

        <View style={styles.header}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_400} size={16} />
          </TouchableOpacity>

          <View style={{ alignItems: 'center', marginRight: 30 }}>
            <View style={[defaultShape.Media_Round, { backgroundColor: COLORS.green_300, alignItems: 'center', justifyContent: 'center' }]}>
              <Icon name="Circle_Ol" color={COLORS.dark_600} size={16} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
            </View>
            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Circles</Text>
          </View>

          <View></View>
        </View>

        {/******** Header ends ********/}

        <View style={{
          height: 56, backgroundColor: '#F7F7F5', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', marginBottom: 6, marginTop: 6
        }}>

          <TextInput
            style={styles.textInput}
            placeholderTextColor="#D9E1E4"
            onChangeText={(value) => { this.setState({ searchText: value }) }}
            color='#154A59'
            placeholder='Search'
            onFocus={() => this.setState({ searchIcon: false })}
            onBlur={() => this.setState({ searchIcon: true })}
            underlineColorAndroid="transparent"
            ref={input => { this.textInput = input }}
          />

          <TouchableOpacity onPress={() => this.setState({ postModalOpen: true })}>
            <Icon name='FilterSlide' size={18} color={COLORS.altgreen_400} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
          </TouchableOpacity>

        </View>

        <View style={{ width: '100%', height: 24, backgroundColor: COLORS.altgreen_200, justifyContent: 'center', alignItems: 'center', marginBottom: 6 }}>
          <Text style={[typography.Caption, { color: COLORS.altgreen_400, textAlign: 'center' }]}>{this.props.userCircle.body && this.props.userCircle.body.content.length} CIRCLES</Text>
        </View>


        <FlatList
          keyboardShouldPersistTaps='handled'
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 80 }}
          style={{ height: '50%' }}
          keyExtractor={(item) => item.id}
          data={this.searchNames(this.state.searchText)}
          initialNumToRender={10}
          renderItem={({ item }) => (

            <TouchableOpacity activeOpacity={0.6} onPress={() => this.props.navigation.navigate("CircleProfileStack", {
              screen: 'CircleProfile',
              params: { slug: item.slug },
            })}>
              <View style={styles.item}>

                <Image source={item.profileImage ? { uri: item.profileImage } : circleDefault} style={[styles.image, { marginLeft: '7%' }]} />

                <View style={styles.nameMsg}>
                  <Text style={styles.name}>{item.title.split(' ').slice(0, 2).join(' ').charAt(0).toUpperCase() + item.title.split(' ').slice(0, 2).join(' ').slice(1)}</Text>
                  {item && item.memberType ?
                    <Text style={[typography.Note2, { color: COLORS.altgreen_300 }]}>{item.memberType.charAt(0).toUpperCase() + item.memberType.slice(1)}</Text> :
                    <></>}
                  {item && item.type ?
                    <Text style={[typography.Note2, { color: COLORS.altgreen_300 }]}>{item.type.charAt(0).toUpperCase() + item.type.slice(1)}</Text> :
                    <></>}
                </View>

              </View>

            </TouchableOpacity>


          )}
        />

      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({

  container: {
    flexDirection: 'column',
    textAlign: 'left',
    fontSize: 15,
    position: "absolute",
    top: 30,
    height: screenHeight,
    backgroundColor: '#fff'
  },
  header: {
    flexDirection: 'row',
    height: 64,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  unselectedText: {
    justifyContent: 'center', alignItems: 'center', borderColor: COLORS.altgreen_300, borderWidth: 1, paddingVertical: 4, paddingHorizontal: 10, borderRadius: 20
  },
  selectedText: {
    backgroundColor: COLORS.dark_700,
    justifyContent: 'center', alignItems: 'center', paddingVertical: 5, paddingHorizontal: 10, borderRadius: 20
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0
  },
  requestEndorsementText: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 14,
    color: '#154A59',
    textAlign: 'center'
  },
  connectsSelectedText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: '#698F8A',
    textAlign: 'center',
    marginVertical: 10
  },
  crossButtonContainer: {
    alignSelf: 'center',
    width: 42,
    height: 42,
    borderRadius: 21,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E7F3E3',
    marginBottom: 10
  },
  linearGradientView2: {
    width: '100%',
    height: 200,
    position: 'absolute',
    top: -50,
    alignSelf: 'center'
  },
  linearGradient2: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6
  },
  sendRequest: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 15,
    color: '#E7F3E3',
    marginLeft: 6
  },
  floatingIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 175,
    height: 39,
    borderRadius: 28,
    backgroundColor: '#367681',
    position: 'absolute',
    bottom: 66,
    right: 12,
  },
  editIcon: {
    alignSelf: 'center'
  },
  selectedName: {
    fontSize: 11,
    color: '#154A59',
    textAlign: 'center',
    fontFamily: 'Montserrat-Medium'
  },
  searchIcon: {
    position: 'absolute',
    left: 136,
    top: 15,
    zIndex: 2
  },
  border: {
    borderWidth: 1,
    marginRight: '6%',
    width: '95%',
    marginTop: '-1%',
    borderColor: '#91B3A2',
    alignSelf: 'center',
    borderRadius: 1,
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '80%',
    height: 46,
    zIndex: 1,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5
  },
  tabNavigator: {
    marginTop: '-8%',
  },


  // --- Assignment ---

  item: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F7F7F500',
    width: '100%',
    height: 52,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'center',
    marginBottom: 8
  },
  image: {
    height: 34,
    width: 34,
    borderRadius: 17
  },
  name: {
    fontWeight: '700',
    color: '#4B4F56',
    fontSize: 14,
    textAlign: 'left'
  },
  message: {
    color: '#698F8A',
    fontSize: 10.5,
  },

  // --- New ---

  time: {
    color: '#AABCC3',
    fontSize: 10,
    marginRight: '10%'
  },
  nameMsg: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: '6%'
  },
  imageGroup: {
    height: 26,
    width: 26,
    borderRadius: 13,
  },
})

const mapStateToProps = (state) => {
  return {
    userCircleProgress: state.circleReducer.userCircleProgress,
    userCircle: state.circleReducer.userCircle,
    errorCircle: state.circleReducer.errorCircle,
  }
}

const mapDispatchToProps = (dispatch) => {

  return {
    userCircleRequest: (data) => dispatch(userCircleRequest(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyCircles)
