import React, { Component } from 'react'
import { Clipboard, Share, View, Text, SafeAreaView, StyleSheet, ImageBackground, TouchableOpacity, Image, FlatList, ScrollView, ActivityIndicator, Platform, Modal } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import LinearGradient from 'react-native-linear-gradient'
import Snackbar from 'react-native-snackbar'

import { REACT_APP_domainUrl, REACT_APP_userServiceURL } from '../../../../env.json'
import circleDefault from '../../../../assets/CirclesDefault.png'
import defaultProfile from '../../../../assets/defaultProfile.png'
import defaultCover from '../../../../assets/defaultCover.png'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import typography from '../../../Components/Shared/Typography'
import icoMoonConfig from '../../../../assets/Icons/selection.json'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class CircleProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userId: '',
      isLoading: false,

      interestsModalOpen: false,
      hashTagModalOpen: false,
      optionsModalOpen: false,
      shareModal2Open: false,

      circleData: {},
      circlePermissionAndCount: {},
      circleLocation: {},
      keyMembersList: [],
      circleJoinedCauses: [],
      circleInterest: [],
      circleHashtag: []
    };
  }

  componentDidMount() {

    AsyncStorage.getItem('userId').then((value) => {

      value && this.setState({ userId: value })
      this.getCircleDetails(this.props.route.params.slug, value)
      // this.getDataByCircleId((this.props.route.params.id, value))

    }).catch((err) => console.log(err))

  }

  hashTagModal = () => {
    return (
      <Modal visible={this.state.hashTagModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

        <View style={{ marginTop: 'auto' }}>

          <View style={{
            width: '100%',
            height: 500,
            position: 'absolute',
            bottom: 0,
            alignSelf: 'center'
          }}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={{
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6
            }}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={styles.crossButtonContainer}
            onPress={() => this.setState({ hashTagModalOpen: false })} >
            <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon} />
          </TouchableOpacity>

          <View style={[defaultShape.Modal_Categories_Container, { height: 350 }]}>

            {this.state.circleHashtag ?
              <FlatList
                columnWrapperStyle={{ flexDirection: "row", flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', paddingVertical: 6 }}
                numColumns={3}
                showsVerticalScrollIndicator={false}
                data={this.state.circleHashtag}
                keyExtractor={(item) => item}
                renderItem={({ item }) => (
                  <View style={[styles.itemBox, { marginHorizontal: 10 }]}>
                    <Text numberOfLines={1} style={{ fontSize: 12, color: '#367681', fontFamily: 'Montserrat-SemiBold' }}>
                      {item}
                    </Text>
                  </View>
                )}
              /> : <></>}

          </View>

        </View>

      </Modal>
    )
  }

  interestsModal = () => {
    return (
      <Modal visible={this.state.interestsModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

        <View style={{ marginTop: 'auto' }}>

          <View style={{
            width: '100%',
            height: 500,
            position: 'absolute',
            bottom: 0,
            alignSelf: 'center'
          }}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={{
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6
            }}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={styles.crossButtonContainer}
            onPress={() => this.setState({ interestsModalOpen: false })} >
            <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon} />
          </TouchableOpacity>

          <View style={[defaultShape.Modal_Categories_Container, { height: 350 }]}>

            {this.state.circleInterest ? <FlatList
              columnWrapperStyle={{ flexDirection: "row", flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', paddingVertical: 6 }}
              numColumns={3}
              showsVerticalScrollIndicator={false}
              data={this.state.circleInterest}
              keyExtractor={(item) => item}
              renderItem={({ item }) => (
                <View style={styles.itemBox}>
                  <Text numberOfLines={1} style={{ fontSize: 12, color: '#367681', fontFamily: 'Montserrat-SemiBold' }}>
                    {item}
                  </Text>
                </View>
              )}
            /> : <ActivityIndicator size='small' color="#fff" />}

          </View>

        </View>

      </Modal>
    )
  }

  getUserConfig = (circleId, userId) => {
    axios({
      method: "get",
      url: REACT_APP_userServiceURL + "/backend/circle/get/user/config/" + circleId + "/" + userId,
      headers: { "Content-Type": "application/json" },
      withCredentials: true
    }).then((response) => {
      if (response && response.data && response.data.message === "Success!") {
        this.setState({ isLoading: false, circlePermissionAndCount: response.data.body })
        // console.log("counts : ", response.data.body)
      }
    }).catch((err) => {
      this.setState({ isLoading: false })
      console.log(err)
    })
  }

  getCircleDetails = (slug, userId) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/circle/get-by-slug/' + slug + '?userId=' + userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then((response) => {
      let res = response.data;
      if (res && res.status === '200 OK') {
        this.setState({
          circleData: res.body
        }, () => {
          this.getUserConfig(this.state.circleData.id, userId)
          this.getKeyMembersList(this.state.circleData.id, userId)
          this.getCircleJoinedCauses(this.state.circleData.id)
          this.getCircleInterest(this.state.circleData.id)
          this.getHashTags(this.state.circleData.id)
          this.getLocation(this.state.circleData.id)
        })
        // console.log('circle data by slug: ', res.body)
      }
    }).catch((err) => {
      console.log(err)
    });
  }

  getLocation = (circleId) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/circle/get/location/' + circleId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then((response) => {
      let res = response.data;
      if (res && res.status === '200 OK') {
        // console.log('key members: ', res.body.content)
        this.setState({ circleLocation: res.body })
      }
    }).catch((err) => {
      console.log('circle Location error: ', err.response)
    })
  }

  getKeyMembersList = (circleId, userId) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/circle/get/key/member/list/' + circleId + '/' + userId + '/' + '?page=0&size=5',
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then((response) => {
      let res = response.data;
      if (res && res.status === '200 OK') {
        // console.log('key members: ', res.body.content)
        this.setState({ keyMembersList: res.body.content })
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  getCircleJoinedCauses = (circleId) => {
    return axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/public/cause/joined/list?userId=' + circleId + "&page=" + 0 + "&size=" + 20,
      cache: true,
      withCredentials: true,
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => {
      if (response && response.status === 200) {
        this.setState({ circleJoinedCauses: response.data.body.content })
      }
    }).catch((e) => {
      console.log(e);
    })
  }

  getCircleInterest = (circleId) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/circle/get/interests?circleId=' + circleId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then((response) => {
      let res = response.data;
      if (res && res.status === '200 OK') {
        this.setState({ circleInterest: res.body })
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  getHashTags = (circleId) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/post/getCircleHashtags?circleId=' + circleId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then((response) => {
      let res = response.data;
      if (res && res.status === '200 OK') {
        this.setState({
          circleHashtag: res.body,
          // isHashTagLoad: res.message

        })
      }
    }).catch((err) => console.log(err))
  }

  handleFollowCircle = (circleId) => {
    this.setState({ isLoading: true })
    let postBody = {
      userId: this.state.userId,
      entityId: circleId,
      entityType: 'CIRCLE'
    }
    axios({
      method: 'post',
      url: this.state.circlePermissionAndCount.followed ? REACT_APP_userServiceURL + '/backend/entity/unfollow' : REACT_APP_userServiceURL + '/backend/entity/follow',
      data: postBody,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.status === 202) {
        this.getUserConfig(circleId, this.state.userId)
        this.setState({ isLoading: false })
      } else {
        this.setState({ isLoading: false })
      }
    }).catch((err) => {
      this.setState({ isLoading: false })
      console.log(err);
    })
  }

  requestMembership = (circleId) => {
    this.setState({ isLoading: true })

    let postData = {
      userId: this.state.userId,
      circleId: circleId
    }
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/circle/send/member/request/',
      headers: { 'Content-Type': 'application/json' },
      data: postData,
      withCredentials: true
    }).then((response) => {
      let res = response.data
      if (res && res.status === '201 CREATED') {
        this.getUserConfig(circleId, this.state.userId)
        this.setState({ isLoading: false })
        Snackbar.show({
          backgroundColor: COLORS.dark_900,
          text: "You have successfully sent member request please wait for confirmation!",
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        })
      }
    }).catch((err) => {
      this.setState({ isLoading: false })
      console.log(err)
    })
  }

  getDataByCircleId = (circleId, userId) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL +
        "/circle/get/config/" + userId + "/" + "?circleId=" + circleId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.message === 'Success!') {
        // this.setState({
        //     userList: response.data.body.inviteeDetails,
        //     privacySetting:response.data.body.privacySettingConfigDto
        // }, () => {
        //     this.getKeyMembersList()
        // })
        console.log('circle data by id: ', response.data.body)
      }
    }).catch((err) => {
      console.log(err);
    });
  }

  unixTime = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp)
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    var year = date.getFullYear()
    var month = months[date.getMonth()]
    var day = date.getDate()

    return day + ' ' + month + ' ' + year
  }

  renderKeyMemberItem = (item) => {
    return (
      item.item.type === 'Super Admin' || item.item.type === 'Admin' ?

        <View style={styles.admin}>
          <Image source={item.item.originalProfileImage ? { uri: item.item.originalProfileImage } : defaultProfile}
            style={{ height: 54, width: 54, borderRadius: 27 }} />

          <View style={{ marginLeft: 10 }}>
            <Text numberOfLines={1} style={[typography.Button_Lead, { color: COLORS.dark_800, maxWidth: 117 }]}>{item.item.username}</Text>
            <Text numberOfLines={1} style={[typography.Caption, { color: COLORS.altgreen_300 }]}>{item.item.type}</Text>
          </View>
        </View>
        :
        <></>
    )
  }

  renderCausesItem = (item) => {
    return (
      <View style={[styles.causes, { marginTop: 10, marginHorizontal: 8 }]}>
        <Image source={{ uri: item.item.imageUrl }} style={styles.causes} />
        <Text numberOfLines={1} style={[typography.Title_2, { position: 'absolute', top: 80, left: 15, color: COLORS.white, zIndex: 1, maxWidth: 170 }]}>
          {item.item.name}
        </Text>
        {/* <TouchableOpacity style={[defaultShape.ActionBtn_Gylph, { position: 'absolute', bottom: 5, right: 5, zIndex: 1 }]}>
          <Icon name="Causes_Plus" size={16} color={COLORS.dark_800} style={{ marginTop: 5,alignSelf:'center' }} />
        </TouchableOpacity> */}
        <LinearGradient colors={[COLORS.darkershade + '00', COLORS.darkershade + '99', COLORS.primarydark]} style={[styles.gradient, { height: 110, borderRadius: 16 }]}></LinearGradient>
      </View>
    )
  }

  renderInterestItem = (item) => {
    return (
      <View style={styles.itemBox}>
        <Text numberOfLines={1} style={{ fontSize: 12, color: '#367681', fontFamily: 'Montserrat-SemiBold' }}>
          {item.item}
        </Text>
      </View>
    )
  }

  renderHashtagItem = (item) => {
    return (
      <View style={styles.hashtagItem}>
        <Text numberOfLines={1}
          style={{ color: '#367681', fontFamily: 'Montserrat-SemiBold', fontSize: 12 }}>
          {item.item}
        </Text>
      </View>
    )
  }

  optionsModal = () => {
    return (

      <Modal visible={this.state.optionsModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>

          <View style={[defaultShape.Linear_Gradient_View, { bottom: 0 }]}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={defaultShape.Linear_Gradient}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={styles.crossButtonContainer}
            onPress={() => this.setState({ optionsModalOpen: false })} >
            <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon} />
          </TouchableOpacity>


          <View style={defaultShape.Modal_Categories_Container}>

            <Text style={[typography.Caption, { color: COLORS.altgreen_400, textAlign: 'center' }]}>Circle page</Text>

            {this.state.circlePermissionAndCount.circleUserPermissionMetadata && this.state.circlePermissionAndCount.circleUserPermissionMetadata.canEdit &&
              <TouchableOpacity style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal, { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' }]} activeOpacity={0.6}
                onPress={() => this.setState({ optionsModalOpen: false }, () => {
                  this.props.navigation.navigate("CircleProfileEdit", { id: this.state.circleData.id, slug: this.state.circleData.slug })
                })} >
                <Icon name="Setting" size={16} color="#154A59" style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                <Text style={styles.modalText}>Page Settings</Text>
              </TouchableOpacity>}

            <TouchableOpacity style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal, { borderBottomWidth: 0, borderBottomColor: '#E8ECEB' }]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false, shareModal2Open: true }) }}>
              <Icon name='Share' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 10 } : {}} />
              <Text style={styles.modalText}>Share Circle Page</Text>
            </TouchableOpacity>

          </View>

        </View>

      </Modal>

    )
  }

  onShare2 = async () => {
    try {
      const result = await Share.share({
        message:
          REACT_APP_domainUrl + '/circle/' + this.props.route.params.slug,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType')
        } else {
          console.log('shared')
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed')
      }
    } catch (error) {
      console.log(error.message)
    }
  }

  shareModal2 = () => {
    return (

      <Modal visible={this.state.shareModal2Open} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>

          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
            onPress={() => this.setState({ shareModal2Open: false })} >
            <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>

            <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Share</Text>
            </View>

            <TouchableOpacity onPress={() => {

              Clipboard.setString(REACT_APP_domainUrl + '/circle/' + this.props.route.params.slug)
              Snackbar.show({
                backgroundColor: '#97A600',
                text: "Link Copied",
                textColor: "#00394D",
                duration: Snackbar.LENGTH_LONG,
              })
              setTimeout(() => {
                this.setState({ shareModal2Open: false })
              }, 2000)
            }}
              style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt]}
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Copy link to circle</Text>
              <Icon name='TxEdi_AddLink' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

            </TouchableOpacity>

            <TouchableOpacity onPress={() => { this.setState({ shareModal2Open: false }, () => this.onShare2()) }}
              style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]}
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Share via others</Text>

              <View style={[{ flexDirection: 'row', justifyContent: 'space-evenly', width: 100, marginRight: -6 }]}>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon name="Social_FB" size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                  <Icon name='Social_Twitter' size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                  <Icon name='Social_LinkedIn' size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                  <Icon name="Meatballs" size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                </View>

              </View>

            </TouchableOpacity>


          </View>



        </View>

      </Modal>

    )
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>

        {this.optionsModal()}
        {this.interestsModal()}
        {this.hashTagModal()}
        {this.shareModal2()}

        <ScrollView alwaysBounceVertical={false} showsVerticalScrollIndicator={false}>

          <View style={styles.banner}
            // source={this.state.circleData.resizedCoverImages ? { uri: this.state.circleData.resizedCoverImages.compressed } : defaultCover}
          >

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10, marginTop: 12 }}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                style={defaultShape.AdjunctBtn_Blurred}>
                <Icon name="Arrow-Left" size={18} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'ios' ? 0 : 6 }} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.setState({ optionsModalOpen: true })}
                style={defaultShape.AdjunctBtn_Blurred}>
                <Icon name="Kebab" size={18} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'ios' ? 0 : 6 }} />
              </TouchableOpacity>
            </View>

            <Image style={{ height: 140, width: 140, borderRadius: 10, position: 'absolute', bottom: 60, alignSelf: 'center', zIndex: 1 }}
              source={this.state.circleData.profileImage ? { uri: this.state.circleData.profileImage } : circleDefault} />

            <LinearGradient colors={[COLORS.white + '00', COLORS.white + 'BF', COLORS.white]} style={styles.gradient}>
              <View style={{ alignItems: 'center' }}>
                <Text style={[typography.Title_1, { color: COLORS.dark_800 }]}>{this.state.circleData.title}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: 245, marginTop: 10 }}>
                  <Text style={[typography.Note2, { color: COLORS.dark_600 }]}>
                    {this.unixTime(this.state.circleData.createTime)}
                  </Text>
                  <Text style={[typography.Note2, { color: COLORS.dark_600 }]}>
                    {this.state.circleLocation ? this.state.circleLocation.city + ', ' + this.state.circleLocation.state + ', ' + this.state.circleLocation.country : ''}
                  </Text>
                </View>
              </View>
            </LinearGradient>

          </View>

          <View style={styles.descriptionView}>

            <Text style={[typography.Body_1, { color: COLORS.altgreen_400 }]}>{this.state.circleData.description}</Text>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, width: '90%' }}>
              <View style={{ alignItems: 'center' }}>
                <Text style={[typography.Title_1, { color: COLORS.altgreen_400 }]}>{this.state.circlePermissionAndCount.membersCount}</Text>
                <Text style={[typography.Caption, { color: COLORS.grey_400 }]}>MEMBERS</Text>
              </View>
              <View style={{ alignItems: 'center' }}>
                <Text style={[typography.Title_1, { color: COLORS.altgreen_400 }]}>{this.state.circlePermissionAndCount.postCount}</Text>
                <Text style={[typography.Caption, { color: COLORS.grey_400 }]}>POSTS</Text>
              </View>
              <View style={{ alignItems: 'center' }}>
                <Text style={[typography.Title_1, { color: COLORS.altgreen_400 }]}>{this.state.circlePermissionAndCount.followerCount}</Text>
                <Text style={[typography.Caption, { color: COLORS.grey_400 }]}>FOLLOWERS</Text>
              </View>
            </View>

            <TouchableOpacity onPress={() => this.handleFollowCircle(this.state.circleData.id)}
              style={[defaultShape.ActionBtn_Prim, { marginTop: 20 }]}>
              {this.state.isLoading ?
                <ActivityIndicator size="small" color={COLORS.white} />
                :
                <Text style={[typography.Button_1, { color: COLORS.white }]}>
                  {this.state.circlePermissionAndCount.followed ? 'FOLLOWING' : 'FOLLOW'}
                </Text>}
            </TouchableOpacity>

          </View>

          {/********* Admin panel starts *********/}

          <View style={{ marginVertical: 15, paddingHorizontal: 22 }}>
            <Text style={[typography.Button_2, { color: COLORS.dark_800 }]}>ADMINS</Text>
            <FlatList
              horizontal
              alwaysBounceHorizontal={false}
              showsHorizontalScrollIndicator={false}
              data={this.state.keyMembersList}
              keyExtractor={(item) => item.id}
              renderItem={(item) => this.renderKeyMemberItem(item)}
            />
          </View>

          {/********* Admin panel ends *********/}



          {/********* Activity, projects and members area starts *********/}

          <View style={styles.activityArea}>

            {this.state.circlePermissionAndCount && this.state.circlePermissionAndCount.membersCount >= 6 ?
              <TouchableOpacity onPress={() => this.props.navigation.navigate('CircleActivities',
                {
                  id: this.state.circleData.id,
                  circleData: JSON.stringify(this.state.circleData),
                  membersCount: this.state.circlePermissionAndCount.membersCount
                })}
                style={styles.activityBtn}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Icon name="Photos" size={18} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }} />
                  <Text style={[typography.Button_1, { color: COLORS.dark_600, marginLeft: 10 }]}>ACTIVITIES</Text>
                </View>
                <Icon name="Arrow-Right" size={16} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }} />
              </TouchableOpacity> : <></>}

            <TouchableOpacity style={styles.activityBtn} onPress={() => this.props.navigation.navigate('OngoingProjects', {
              id: this.state.circleData.id,
              circleData: JSON.stringify(this.state.circleData)
            })}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon name="Projects_OL" size={18} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }} />
                <Text style={[typography.Button_1, { color: COLORS.dark_600, marginLeft: 10 }]}>PROJECTS</Text>
              </View>
              <Icon name="Arrow-Right" size={16} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.activityBtn}
              onPress={() => this.props.navigation.navigate('CircleMembers',
                {
                  id: this.state.circleData.id,
                  circleData: JSON.stringify(this.state.circleData)
                })}
            >
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon name="Community" size={18} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }} />
                <Text style={[typography.Button_1, { color: COLORS.dark_600, marginLeft: 10 }]}>MEMBERS</Text>
              </View>
              <Icon name="Arrow-Right" size={16} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }} />
            </TouchableOpacity>
          </View>

          {/********* Activity, projects and members area ends *********/}

          {this.state.circlePermissionAndCount && this.state.circlePermissionAndCount.membersCount < 6 ?
            <Text style={[typography.Button_1, { color: COLORS.dark_600, marginHorizontal: 14, marginVertical: 30, textAlign: 'center', fontSize: 14 }]}>This Circle will be active once it meets the minimum members criteria {'\n'}{'\n'} Circle is a great way to create a community with people
              who share the same interest, cause or activity.
              Share your ideas, work and stories with everyone and create a larger impact together.</Text>
            : <></>}

          {/********* Causes support starts *********/}

          {this.state.circlePermissionAndCount && this.state.circlePermissionAndCount.membersCount >= 6 && this.state.circleJoinedCauses.length > 0 &&
            <View style={{ height: 182, backgroundColor: COLORS.altgreen_400 }}>
              <Text style={[typography.H6, { color: COLORS.white, alignSelf: 'center', marginTop: 20 }]}>CAUSES WE SUPPORT</Text>
              <FlatList
                horizontal
                contentContainerStyle={{ paddingHorizontal: 10 }}
                alwaysBounceHorizontal={false}
                showsHorizontalScrollIndicator={false}
                data={this.state.circleJoinedCauses}
                keyExtractor={(item) => item.id}
                renderItem={(item) => this.renderCausesItem(item)}
              />
            </View>}

          {/********* Causes support ends *********/}



          {/********* Interests starts *********/}

          {this.state.circlePermissionAndCount && this.state.circlePermissionAndCount.membersCount >= 6 && this.state.circleInterest.length > 0 &&
            <View style={{ height: this.state.circleInterest.length > 8 ? Platform.OS === 'ios' ? 280 : 330 : 230 }}>
              <View style={{ backgroundColor: COLORS.altgreen_300, height: '50%' }}></View>

              <View style={styles.interestsBox}>
                <Text style={[typography.Button_2, { color: COLORS.dark_800, marginTop: 15 }]}>Our Interests</Text>
                <FlatList
                  contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  alwaysBounceHorizontal={false}
                  showsVerticalScrollIndicator={false}
                  data={this.state.circleInterest.slice(0, 9)}
                  keyExtractor={(item) => item}
                  renderItem={(item) => this.renderInterestItem(item)}
                />
                {
                  this.state.circleInterest.length > 9 ?
                    <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', paddingBottom: 15, marginTop: 5 }}>
                      <TouchableOpacity onPress={() => this.setState({ interestsModalOpen: true })}>
                        <Text style={[typography.Button_Lead, { color: COLORS.altgreen_300 }]}>See All</Text>
                      </TouchableOpacity>
                    </View>
                    :
                    <></>
                }
              </View>

            </View>}

          {/********* Interests ends *********/}



          {/********* Hashtags starts *********/}

          {this.state.circlePermissionAndCount && this.state.circlePermissionAndCount.membersCount >= 6 && this.state.circleHashtag.length > 0 &&
            <View style={styles.hashtagView}>
              <Text style={[typography.Button_2, { color: COLORS.dark_800, marginTop: 15, marginBottom: 8 }]}>HASHTAG</Text>
              <FlatList
                contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center' }}
                alwaysBounceHorizontal={false}
                showsHorizontalScrollIndicator={false}
                ListFooterComponent={
                  this.state.circleHashtag.length > 8 ?
                    <TouchableOpacity onPress={() => this.setState({ hashTagModalOpen: true })}
                      activeOpacity={0.5} style={{ height: 30, width: 30, borderRadius: 15, borderColor: '#91B3A2', borderWidth: 1, marginHorizontal: 5, alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontSize: 12, fontFamily: 'Montserrat-SemiBold', color: '#367681' }}>...</Text>
                    </TouchableOpacity>
                    :
                    <></>
                }
                ListFooterComponentStyle={{ marginTop: 5 }}
                data={this.state.circleHashtag.slice(0, 8)}
                keyExtractor={(item) => item}
                renderItem={(item) => this.renderHashtagItem(item)}
              />
            </View>}

          {/********* Hashtags ends *********/}



          {/********* Request membership starts *********/}

          {this.state.circlePermissionAndCount.circleUserPermissionMetadata &&
            this.state.circlePermissionAndCount.circleUserPermissionMetadata.canMembershipRequest === true &&
            <View style={styles.requestMembership}>
              <Text style={[typography.Subtitle_1, { color: COLORS.dark_700 }]}>Become a Member</Text>
              {this.state.isLoading ?
                <ActivityIndicator size="small" color={COLORS.white} />
                :
                <TouchableOpacity onPress={() => this.requestMembership(this.state.circleData.id)}
                  style={styles.reqBtn} activeOpacity={0.8}>
                  <Text style={[typography.Button_1, { color: COLORS.white }]}>REQUEST MEMBERSHIP</Text>
                </TouchableOpacity>}
            </View>}

          {/********* Request membership ends *********/}

        </ScrollView>

      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  banner: {
    height: 270,
    width: '100%'
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 6 : 0
  },
  crossButtonContainer: {
    alignSelf: 'center',
    width: 42,
    height: 42,
    borderRadius: 21,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E7F3E3',
    marginBottom: 10
  },
  modalText: {
    fontSize: 14,
    color: '#154A59',
    fontFamily: 'Montserrat-SemiBold',
    marginLeft: 14
  },
  termsmodal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '85%',
    paddingVertical: 5.5,
    paddingLeft: 30
  },
  gradient: {
    position: 'absolute',
    bottom: 0,
    height: 145,
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  descriptionView: {
    backgroundColor: COLORS.white,
    paddingVertical: 20,
    paddingHorizontal: 25,
    justifyContent: 'center',
    alignItems: 'center'
  },
  admin: {
    flexDirection: 'row',
    width: 191,
    height: 56,
    borderRadius: 28,
    backgroundColor: COLORS.white,
    marginTop: 10,
    marginBottom: 2,
    marginHorizontal: 5,
    alignItems: 'center'
  },
  activityArea: {
    backgroundColor: COLORS.altgreen_200,
    height: 206,
    alignItems: 'center',
    justifyContent: 'center'
  },
  activityBtn: {
    flexDirection: 'row',
    width: '85%',
    height: 50,
    borderRadius: 27,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: COLORS.altgreen_100,
    marginVertical: 4
  },
  causes: {
    width: 200,
    height: 110,
    borderRadius: 16
  },
  interestsBox: {
    position: 'absolute',
    top: 20,
    minHeight: 200,
    maxHeight: 300,
    width: '80%',
    alignSelf: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    borderRadius: 8,
    backgroundColor: COLORS.white,
    zIndex: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,

    elevation: 8,
  },
  itemBox: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomColor: '#D9E1E4',
    borderBottomWidth: 3,
    marginHorizontal: 5,
    marginVertical: 8
  },
  hashtagView: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: '80%',
    marginBottom: 20
  },
  hashtagItem: {
    height: 31,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: '#91B3A2',
    backgroundColor: '#CFE7C780',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    marginHorizontal: 5,
    marginVertical: 6
  },
  requestMembership: {
    height: 80,
    width: '90%',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    marginVertical: 35
  },
  reqBtn: {
    height: 50,
    width: '100%',
    borderRadius: 27,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.dark_800
  }
})