import React, { Component } from 'react'
import { FlatList, Modal, View, Text, SafeAreaView, TouchableOpacity, Platform, StyleSheet, ScrollView, ImageBackground, Image, Dimensions } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import LinearGradient from 'react-native-linear-gradient'
import { TextInput } from 'react-native-paper'
import Snackbar from 'react-native-snackbar'
import * as _ from "lodash"
import DatePicker from 'react-native-datepicker'
import ImagePicker from 'react-native-image-crop-picker'

import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import circleDefault from '../../../../assets/CirclesDefault.png'
import defaultCover from '../../../../assets/defaultCover.png'
import defaultStyle from '../../../Components/Shared/Typography'
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader'
import EditUserAddress from '../../../Components/User/EditProfile/EditUserAddress'
import { REACT_APP_userServiceURL } from '../../../../env.json'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const windowWidth = Dimensions.get('window').width

export default class CircleInfoEdit extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userId: this.props.route.params.id,
      slug: this.props.route.params.slug,
      isLoading: false,
      query: '',
      dob: '',
      coverImage: '',
      profileImage: '',
      bio: '',

      editBioModalOpen: false,
      editPersonalInfoModalOpen: false,
      editUserAddressModalOpen: false,
      whatDefinesYouModalOpen: false,

      editedBio: '',
      circleName: '',
      persona: '',
      dateOfBirth: '',
      circleLocation: {},
      address: '',
      country: '',
      state: '',
      city: '',

      whatDefinesYouSuggestions: [],
      searchWhatDefinesYou: '',
    }
  }

  componentDidMount() {

    AsyncStorage.getItem("refreshToken").then((value) => {
      if (value === null) {
        this.props.navigation.replace("Login")
      }
    })

    AsyncStorage.getItem("userId").then((value) => {
      value &&
        // this.setState({ userId: value })
        this.getCircleDetails(this.state.slug, value)
      this.getDataByCircleId(value)

    }).catch((e) => {
      console.log(e)
    })
  }

  goback = () => { this.props.navigation.goBack() }

  unixTime = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp)
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    var year = date.getFullYear()
    var month = months[date.getMonth()]
    var day = date.getDate()

    return day + ' ' + month + ' ' + year
  }

  unixTimeDatePicker = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp)
    var year = date.getFullYear()
    var month = date.getMonth() + 1
    var day = date.getDate()

    return day + '-' + month + '-' + year
  }

  currentDate = () => {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = dd + '-' + mm + '-' + yyyy

    return today
  }

  getCircleDetails = (slug, userId) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/circle/get-by-slug/' + slug + '?userId=' + userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then((response) => {
      let res = response.data;
      if (res && res.status === '200 OK') {
        this.setState({
          circleName: res.body.title,
          dateOfBirth: res.body.incorporationDate,
          dob: res.body.incorporationDate,
          bio: res.body.description,
          editedBio: res.body.description,
          profileImage: res.body.profileImage,
          coverImage: res.body.resizedCoverImages && res.body.resizedCoverImages.compressed,
          persona: res.body.type

        }, () => {
          this.getLocation(this.state.userId)
        })
        // console.log('circle data by slug: ', res.body)
      }
    }).catch((err) => {
      console.log(err)
    });
  }

  getLocation = (circleId) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/circle/get/location/' + circleId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then((response) => {
      let res = response.data;
      if (res && res.status === '200 OK') {
        // console.log('key members: ', res.body.content)
        this.setState({ circleLocation: res.body })
      }
    }).catch((err) => {
      console.log('circle Location error: ', err.response)
    })
  }

  //**************** persona suggesstions api call starts ******************//

  getDataByCircleId = (userId) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL +
        "/circle/get/config/" + userId + "/" + "?circleId=" + this.state.userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.message === 'Success!') {
        this.setState({
          whatDefinesYouSuggestions: response.data.body.persona,
        })
      }
    }).catch((err) => {
      console.log(err);
    });
  }

  //**************** persona suggesstions api call ends ******************//



  //**************** personal info validation and api call starts ******************//

  validateForm() {
    let formIsValid = true
    if (this.state.circleName === '') {
      if (_.isUndefined(this.state.circleName) ||
        _.isEmpty((this.state.circleName || "").toString()) ||
        _.isNull(this.state.circleName)) {

        Snackbar.show({
          backgroundColor: COLORS.alert_red,
          text: 'Please enter circle name',
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        })
        formIsValid = false
      }
    } else {
      const fn = /^[a-zA-Z\s]+$/;
      if (this.state.circleName === '' || fn.test(this.state.circleName)) {
        this.setState({ circleName: this.state.circleName });
      }

      if (this.state.circleName.trim().replace(" ", "").length > 25 ||
        this.state.circleName.trim().replace(" ", "").length < 2
      ) {
        Snackbar.show({
          backgroundColor: COLORS.alert_red,
          text: 'Circle name must have minimum 2 characters',
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        })
        formIsValid = false
      }
    }

    if (_.isUndefined(this.state.editedBio.trim()) ||
      _.isEmpty(this.state.editedBio.trim()) ||
      _.isNull(this.state.editedBio.trim())) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Description cannot be empty',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      })
      this.setState({ isLoading: false })
      formIsValid = false
    }

    return formIsValid;

  }

  submitProfileDetail = () => {

    this.setState({ isLoading: true })

    let tempdob = typeof (this.state.dob) == "string" && this.state.dob.split("-").reverse().join("-")
    let convertedUnixTime = new Date(tempdob).getTime()

    if (this.validateForm()) {
      let postBody = {
        "circleId": this.state.userId,
        "title": this.state.circleName,
        "description": this.state.editedBio,
        "type": this.state.persona,
        "incorporationDate": typeof (this.state.dob) == "string" ? convertedUnixTime : this.state.dateOfBirth,
      }
      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/backend/circle/update/info',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true
      }).then((response) => {
        let res = response.data
        if (res.status === '202 ACCEPTED') {
          this.setState({ isLoading: false, editPersonalInfoModalOpen: false, editBioModalOpen: false })
          typeof (this.state.dob) == "string" && this.setState({ dateOfBirth: convertedUnixTime })
          this.getCircleDetails(this.state.slug, this.state.userId)
        }
      }).catch((err) => {
        if (err && err.response && err.response.data) {
          this.setState({ isLoading: false })
          Snackbar.show({
            backgroundColor: COLORS.alert_red,
            text: err.response.data.message,
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          })
        }
      })
    }
  }

  //**************** personal info validation and api call ends ******************//



  //**************** profile & cover image update api call starts ******************//

  openPicker = () => {
    ImagePicker.openPicker({
      width: windowWidth * 0.9,
      height: 217,
      cropping: true,
      mediaType: 'photo'
    }).then(image => {
      console.log(image)
      this.onCropComplete(image, 'cover')
    })
  }

  openPickerProfile = () => {
    ImagePicker.openPicker({
      width: 110,
      height: 145,
      cropping: true,
      mediaType: 'photo'
    }).then(image => {
      console.log(image)
      this.onCropComplete(image, 'profile')
    })
  }

  onCropComplete = (crop, value) => {
    value === 'cover' ? this.setState({ coverImage: crop.path }) : this.setState({ profileImage: crop.path })

    let photo = {
      uri: crop.path,
      type: crop.mime,
      name: crop.path.split("-").pop()
    }

    this.setState({ isLoading: true })
    const formData = new FormData()
    value === 'cover' ? formData.append('coverImage', photo) : formData.append('profileImage', photo)
    formData.append('circleId', this.state.userId)
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/circle/update/image',
      headers: { 'Content-Type': 'multipart/form-data' },
      data: formData,
      withCredentials: true
    }).then((response) => {
      let res = response.data
      console.log('update cover: ', res)
      if (res.message === 'Success!') {
        this.setState({ isLoading: false })
      }
    }).catch((err) => {
      this.setState({ isLoading: false })
      console.log('error in update cover: ', err)
    })
  }

  //**************** profile & cover image update api call ends ******************//



  // **************** delete profile api call starts ******************//

  deleteProfilePic = () => {

    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/circle/delete/profileImage?circleId=' + this.state.userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then((response) => {
      let res = response.data;
      if (res.status === '200 OK') {
        console.log(res.status)
        this.setState({ profileImage: '' })
      }
    }).catch((err) => {
      if (err && err.response && err.response.data) {
        this.setState({ isLoading: false })
        console.log('error in delete profile pic: ', err)
      }
    })
  }

  // **************** Delete profile api call ends ****************** //



  // **************** Delete cover image api call starts ****************** //

  deleteCoverPic = () => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/circle/delete/coverImage?circleId=' + this.state.userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then((response) => {
      let res = response.data;
      if (res.status === '200 OK') {
        this.setState({ coverImage: '' })
      }
    }).catch((err) => {
      if (err && err.response && err.response.data) {
        this.setState({ isLoading: false })
        // console.log('error in delete cover pic: ', err)
      }
    })
  }

  // **************** Delete cover image api call ends ****************** //


  editBioModal = () => {
    return (
      <Modal visible={this.state.editBioModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

        <View style={{ marginTop: 'auto' }}>

          <View style={styles.linearGradientView}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={styles.linearGradient}>
            </LinearGradient>
          </View>

          <ScrollView keyboardShouldPersistTaps='handled'>

            <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
              onPress={() => this.setState({ editBioModalOpen: false })} >
              <Icon name='Cross' size={13} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
            </TouchableOpacity>

            <View style={[defaultShape.Modal_Categories_Container, { height: 450, backgroundColor: COLORS.bgfill }]}>
              <View style={styles.modalHeaderView}>
                <Text style={[defaultStyle.Button_2, { color: COLORS.dark_900, textAlign: 'center' }]}>
                  EDIT BIO
                </Text>
              </View>


              <TextInput
                theme={{ colors: { text: COLORS.dark_600, primary: COLORS.bgfill, placeholder: COLORS.altgreen_300 } }}
                // label="Write something interesting"
                placeholder="Add Bio"
                // autoFocus={true}
                multiline
                selectionColor='#C8DB6E'
                style={[defaultStyle.H6, { height: '76%', backgroundColor: COLORS.bgfill, color: COLORS.dark_600, marginTop: '12%', marginHorizontal: '2%', alignSelf: 'flex-start', textAlign: 'left' }]}
                onChangeText={(value) => this.setState({ editedBio: value })}
                value={this.state.editedBio}
              />


              <View style={styles.border}></View>

              <TouchableOpacity onPress={this.submitProfileDetail}
                activeOpacity={0.7} style={styles.updateButton}>
                <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_100 }]}>Update</Text>
              </TouchableOpacity>

            </View>
          </ScrollView>
        </View>

      </Modal>
    )
  }

  editPersonalInfoModal = () => {
    return (
      <Modal visible={this.state.editPersonalInfoModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>


        <View style={{ marginTop: 'auto' }}>


          <View style={styles.linearGradientView}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={styles.linearGradient}></LinearGradient>
          </View>

          <ScrollView keyboardShouldPersistTaps='handled'>

            <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
              onPress={() => this.setState({ editPersonalInfoModalOpen: false })} >
              <Icon name='Cross' size={13} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
            </TouchableOpacity>

            <View style={[defaultShape.Modal_Categories_Container, { height: 373, backgroundColor: COLORS.bgfill }]}>
              <View style={styles.modalHeaderView}>
                <Text style={[defaultStyle.Button_2, { color: COLORS.dark_900, textAlign: 'center' }]}>
                  EDIT PERSONAL INFO
                </Text>
              </View>


              <View style={{
                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 56, width: '90%'
              }}>

                <TextInput
                  theme={{ colors: { text: COLORS.dark_700, primary: COLORS.altgreen_300, placeholder: COLORS.altgreen_300 } }}
                  label="Circle Name"
                  // mode='outlined'
                  selectionColor='#C8DB6E'
                  style={[defaultStyle.Subtitle_1, { width: '100%', height: 56, backgroundColor: COLORS.bgfill, color: COLORS.dark_700 }]}
                  onChangeText={(value) => this.setState({ circleName: value })}
                  value={this.state.circleName}
                />

              </View>

              <TouchableOpacity activeOpacity={0.6}
                style={{
                  height: 56, width: '90%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 20, paddingLeft: 6, paddingRight: 12
                }}>

                <View style={{ width: '100%' }}>
                  <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>Incorporation Date</Text>

                  <DatePicker
                    style={{ width: '100%', borderWidth: 0 }}
                    date={typeof (this.state.dob) == "string" && this.state.dob.includes('-') ? this.state.dob : this.unixTimeDatePicker(this.state.dob)}
                    mode="date"
                    placeholder="select date"
                    format="DD-MM-YYYY"
                    minDate="01-01-1900"
                    maxDate={this.currentDate()}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        top: -10,
                        marginRight: 0
                      },
                      dateInput: {
                        alignItems: 'flex-start',
                        borderWidth: 0
                      },
                      dateText: {
                        color: COLORS.dark_700,
                        marginTop: -20
                      }
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { this.setState({ dob: date }) }}
                  />
                </View>
              </TouchableOpacity>
              <View style={styles.border2}></View>

              <TouchableOpacity onPress={() => this.setState({ whatDefinesYouModalOpen: true, editPersonalInfoModalOpen: false })}
                activeOpacity={0.6}
                style={{
                  height: 56, width: '90%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 10, paddingLeft: 6, paddingRight: 6
                }}>

                <View>
                  <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>What defines you best</Text>
                  <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>{this.state.persona}</Text>
                </View>

                <View style={{ width: 26, height: 26, borderRadius: 13, backgroundColor: '#E7F3E3', justifyContent: 'center', alignItems: 'center', marginTop: 6 }}>
                  <Icon name='Arrow_Down' size={12} color='#91B3A2' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
                </View>
              </TouchableOpacity>
              <View style={styles.border2}></View>

              <TouchableOpacity onPress={this.submitProfileDetail}
                activeOpacity={0.7} style={[styles.updateButton, { marginTop: 30 }]}>
                <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_100 }]}>Update</Text>
              </TouchableOpacity>

            </View>
          </ScrollView>
        </View>


      </Modal>
    )
  }

  submitAddressDetail = () => {

    let postBody = {
      "circleId": this.state.userId,
      "country": this.state.country,
      "state": this.state.state,
      "city": this.state.city
    }
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/circle/update/location',
      headers: { 'Content-Type': 'application/json' },
      data: postBody,
      withCredentials: true
    }).then((response) => {
      let res = response.data
      if (res.status === '202 ACCEPTED') {
        console.log(res.status)
        this.getLocation(this.state.userId)
      }
    }).catch((err) => {
      if (err && err.response && err.response.data) {
        console.log(err.response.data)
      }
    })
  }

  changeState = (value) => {
    this.setState(value)
  }

  editUserAddressModal = () => {
    return (
      <Modal visible={this.state.editUserAddressModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

        <View style={{ marginTop: 'auto' }}>

          <View style={styles.linearGradientView}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={styles.linearGradient}></LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
            onPress={() => this.setState({ editUserAddressModalOpen: false })} >
            <Icon name='Cross' size={13} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
          </TouchableOpacity>

          <View style={[defaultShape.Modal_Categories_Container, { height: 400, backgroundColor: COLORS.bgfill }]}>
            <View style={[styles.modalHeaderView]}>
              <Text style={[defaultStyle.Button_2, { color: COLORS.dark_900, textAlign: 'center' }]}>
                EDIT ADDRESS
              </Text>
            </View>

            <EditUserAddress country={this.state.circleLocation.country ? this.state.circleLocation.country : ''} state={this.state.circleLocation.country ? this.state.circleLocation.state : ''} city={this.state.circleLocation.country ? this.state.circleLocation.city : ''} changeState={this.changeState} />
            {/* <CreateCircle country={this.state.country} state={this.state.state} city={this.state.city} changeState={this.changeState} marginTop={30} /> */}

            {/* <View style={styles.border2}></View>
            <View style={[styles.border, { position: 'absolute', bottom: 80 }]}></View> */}

            <TouchableOpacity onPress={() => { this.setState({ editUserAddressModalOpen: false }), this.submitAddressDetail() }}
              activeOpacity={0.7} style={[styles.updateButton, { marginTop: 0 }]}>
              <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_100 }]}>Update</Text>
            </TouchableOpacity>

          </View>
        </View>

      </Modal>
    )
  }

  searchWhatDefinesYou = (query) => {
    if (query === '') {
      return this.state.whatDefinesYouSuggestions
    }

    const regex = new RegExp(`${query.trim()}`, 'i')

    return this.state.whatDefinesYouSuggestions.filter(item => (item).search(regex) >= 0)
  }

  whatDefinesYouModal = () => {
    return (
      <Modal visible={this.state.whatDefinesYouModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

        <View style={{ marginTop: 'auto' }}>

          <View style={{
            width: '100%',
            height: 700,
            position: 'absolute',
            bottom: 0,
            alignSelf: 'center'
          }}>
            <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={{
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6
            }}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
            onPress={() => this.setState({ whatDefinesYouModalOpen: false })} >
            <Icon name='Cross' size={13} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
          </TouchableOpacity>

          <View style={[defaultShape.Modal_Categories_Container, { height: 450, backgroundColor: COLORS.bgfill, paddingBottom: 10 }]}>
            <View style={{ width: '100%', height: 58, backgroundColor: COLORS.altgreen_100, justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
              <Text style={[defaultStyle.Button_2, { color: COLORS.dark_900, textAlign: 'center' }]}>
                WHAT DEFINES YOU BEST
              </Text>
            </View>


            <View style={{
              height: 40, backgroundColor: '#F7F7F5', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: '13%', zIndex: 2
            }}>

              <TextInput
                style={styles.textInput}
                theme={{ colors: { text: COLORS.dark_700, primary: '#F7F7F5', placeholder: COLORS.altgreen_300 } }}
                // label="Type in address"
                selectionColor='#C8DB6E'
                onChangeText={(value) => { this.setState({ searchWhatDefinesYou: value }) }}
                placeholder='Search'
                onFocus={() => this.setState({ searchIcon: false })}
                onBlur={() => this.setState({ searchIcon: true })}
                underlineColorAndroid="transparent"
                ref={input => { this.textInput = input }}
              />


            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps='handled'
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 0, paddingTop: 6, width: '100%' }}
                style={{ height: '50%' }}
                keyExtractor={(item) => item}
                data={this.searchWhatDefinesYou(this.state.searchWhatDefinesYou)}
                initialNumToRender={10}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() => { this.setState({ persona: item, searchWhatDefinesYou: '', whatDefinesYouModalOpen: false }) }}
                    style={styles.item} activeOpacity={0.7}>

                    <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600, textAlign: 'center' }]}>{item}</Text>
                    <View style={{ width: 17, height: 17, borderRadius: 8.5, alignItems: 'center', justifyContent: 'center' }}>
                      {/* {this.state.selectedWhatDefinesYou.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>

          </View>
        </View>

      </Modal>
    )
  }

  render() {

    return (
      <SafeAreaView style={{ flex: 1 }}>

        {/***** Header starts *****/}

        <ProfileEditHeader name="Circle Info" iconName="Circle_Ol" goback={this.goback} />

        {/***** Header ends *****/}

        {this.editBioModal()}
        {this.editPersonalInfoModal()}
        {this.editUserAddressModal()}
        {this.whatDefinesYouModal()}

        <ScrollView>

          {/***** Profile & Cover image starts *****/}

          <View style={styles.profileCoverImgView}>
            <ImageBackground source={this.state.coverImage ? { uri: this.state.coverImage } : defaultCover} style={{ height: 138, width: '100%', zIndex: 1 }}>
              <View style={{ alignSelf: 'flex-end', flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, marginRight: 10, width: 70 }}>
                <TouchableOpacity onPress={this.openPicker}
                  activeOpacity={0.5} style={defaultShape.AdjunctBtn_Small_Sec}>
                  <Icon name="UploadPhoto" color={COLORS.dark_600} size={14} style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }} />
                </TouchableOpacity>
                {this.state.coverImage ?
                <TouchableOpacity onPress={() => this.deleteCoverPic()}
                  activeOpacity={0.5} style={defaultShape.AdjunctBtn_Small_Sec}>
                  <Icon name="TrashBin" color={COLORS.alertred_200} size={14} style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }} />
                </TouchableOpacity> : null}
              </View>
            </ImageBackground>

            <View style={{ position: 'absolute', bottom: 20, flexDirection: 'row', justifyContent: 'space-between', width: '92%', zIndex: 2 }}>
              <Image source={this.state.profileImage ? { uri: this.state.profileImage } : circleDefault}
                style={{ height: 110, width: 110, borderRadius: 55, zIndex: 2, marginLeft: 25 }} />

              <View style={{ alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-around', position: 'absolute', top: 72, left: 45, width: 70, zIndex: 3 }}>
                <TouchableOpacity onPress={this.openPickerProfile}
                  activeOpacity={0.5} style={defaultShape.AdjunctBtn_Small_Sec}>
                  <Icon name="UploadPhoto" color={COLORS.dark_600} size={14} style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }} />
                </TouchableOpacity>
                {this.state.profileImage ? 
                <TouchableOpacity onPress={this.deleteProfilePic}
                  activeOpacity={0.5} style={defaultShape.AdjunctBtn_Small_Sec}>
                  <Icon name="TrashBin" color={COLORS.alertred_200} size={14} style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }} />
                </TouchableOpacity> : null}
              </View>

              {/* <TouchableOpacity style={[defaultShape.ContextBtn_FL_Drk, { zIndex: 2, alignSelf: 'center', marginTop: 40, width: 76 }]}>
                <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_200 }]}>Update</Text>
              </TouchableOpacity> */}
            </View>

          </View>

          {/***** Profile & Cover image ends *****/}



          {/***** Name dob persona starts *****/}

          <View style={{ height: 162, backgroundColor: COLORS.white, marginTop: 15 }}>
            <TouchableOpacity onPress={() => this.setState({ editPersonalInfoModalOpen: true })}
              style={[defaultShape.Nav_Gylph_Btn, { position: 'absolute', top: 5, right: 5, zIndex: 1 }]}>
              <Icon name="EditBox" size={16} color={COLORS.grey_400} />
            </TouchableOpacity>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Circle Name
              </Text>
              <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                {this.state.circleName}
              </Text>
            </View>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Incorporation Date
              </Text>
              <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                {this.unixTime(this.state.dateOfBirth)}
              </Text>
            </View>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                What defines you best
              </Text>
              <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                {this.state.persona}
              </Text>
            </View>
          </View>

          {/***** Name dob persona ends *****/}



          {/***** Bio starts *****/}

          <View style={{ maxHeight: 150, backgroundColor: COLORS.white, marginTop: 15, paddingBottom: 10 }}>
            <TouchableOpacity
              onPress={() => this.setState({ editBioModalOpen: true })}
              style={[defaultShape.Nav_Gylph_Btn, { position: 'absolute', top: 5, right: 5, zIndex: 1 }]}>
              <Icon name="EditBox" size={16} color={COLORS.grey_400} />
            </TouchableOpacity>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Description
              </Text>
              <Text numberOfLines={5} style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700, maxWidth: '85%' }]}>
                {this.state.bio}
              </Text>
            </View>
          </View>

          {/***** Bio ends *****/}



          {/***** Address starts *****/}

          <View style={{ height: 68, backgroundColor: COLORS.white, marginTop: 15, marginBottom: 40 }}>
            <TouchableOpacity onPress={() => this.setState({ editUserAddressModalOpen: true })}
              style={[defaultShape.Nav_Gylph_Btn, { position: 'absolute', top: 5, right: 5, zIndex: 1 }]}>
              <Icon name="EditBox" size={16} color={COLORS.grey_400} />
            </TouchableOpacity>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Address
              </Text>
              <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                {this.state.circleLocation.country
                  ?
                  this.state.circleLocation.city + ', ' + this.state.circleLocation.state + ', ' + this.state.circleLocation.country
                  : ''}
              </Text>
            </View>
          </View>

          {/***** Address ends *****/}

        </ScrollView>

      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headerView: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 54,
    marginBottom: 5,
    backgroundColor: COLORS.white,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  borderStyleBase: {
    color: '#00394D',
    fontWeight: 'bold',
    fontSize: 24,
    width: 36,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 2,
    borderBottomColor: '#BFC52E'
  },
  borderStyleHighLighted: {
    borderColor: "#03DAC6",
  },
  inputFocus: {
    padding: 10,
    fontSize: 14,
    backgroundColor: '#FFFFFF',
    height: 43,
  },
  item: {
    flexDirection: 'row',
    width: '83%',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'left',
    marginLeft: 30,
    // marginRight: 100,
    paddingHorizontal: '3%',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey_200,
    // backgroundColor: 'pink'
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '90%',
    height: 26,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    zIndex: 2
  },
  linearGradientView: {
    width: '100%',
    height: 700,
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center'
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6
  },
  profileCoverImgView: {
    height: 217,
    width: '90%',
    alignSelf: 'center'
  },
  urlView: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    backgroundColor: COLORS.altgreen_t50,
    borderRadius: 4,
    marginTop: 5,
    width: '90%',
    borderWidth: 1,
    borderColor: COLORS.altgreen_300,
    borderStyle: 'dashed'
  },
  updateButton: {
    width: 86,
    height: 28,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: COLORS.dark_600
  },
  border: {
    width: '80%',
    alignSelf: 'center',
    backgroundColor: COLORS.grey_200,
    height: 2
  },
  border2: {
    width: '100%',
    alignSelf: 'center',
    backgroundColor: COLORS.bgfill,
    height: 10,
    zIndex: 2,
    marginTop: -6
  },
  modalHeaderView: {
    zIndex: 2,
    width: '100%',
    height: 58,
    backgroundColor: COLORS.altgreen_100,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
})
