import React, {Component} from 'react';
import {
  FlatList,
  StyleSheet,
  Modal,
  ScrollView,
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import Snackbar from 'react-native-snackbar';

import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class CirclePrivacySharing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      profileSharing: 'Loading ...',
      informationSharing: 'Loading ...',
      profileSharingLocation: 'Loading ...',
      project: 'Private',
      experience: 'Private',

      profileSharingModalOpen: false,
      informationSharingModalOpen: false,
      profileSharingLocationModalOpen: false,
      sharingProjectModalOpen: false,
      sharingExperienceModalOpen: false,

      profileSharingOptions: [
        'Everyone on WeNaturalists',
        'Members Only',
        'Members & Followers',
        'Private',
      ],
      informationSharingOptions: [
        'Basic Information',
        'Basic + Additional Information',
        'Private',
      ],
      profileSharingLocationOptions: ['Outside WeNaturalists', 'Private'],
      sharingProjectOptions: [
        'Outside WeNaturalists',
        'Everyone on WeNaturalists',
        'Private',
      ],
    };
  }

  componentDidMount() {
    // console.log(this.props.route.params.id)
    // AsyncStorage.getItem('userId').then((value) => {

    //     value && this.setState({ userId: value })

    //   }).catch((err) => console.log(err))

    this.getSharingDetails(this.props.route.params.id);
  }

  goback = () => {
    this.props.navigation.navigate('CircleProfileEdit');
  };

  displayProfileSharing = (value) => {
    if (value === 'GLOBAL') {
      this.setState({profileSharing: 'Everyone on WeNaturalists'});
    } else if (value === 'MEMBER') {
      this.setState({profileSharing: 'Members only'});
    } else if (value === 'MEMBERANDFOLLOWER') {
      this.setState({profileSharing: 'Members & Followers'});
    } else {
      this.setState({profileSharing: 'Private'});
    }
  };
  displayProfileSharingScope = (index) => {
    if (index === 0) {
      return 'GLOBAL';
    } else if (index === 1) {
      return 'MEMBER';
    } else if (index === 2) {
      return 'MEMBERANDFOLLOWER';
    } else {
      return 'PRIVATE';
    }
  };

  displayInfoSharing = (value) => {
    if (value === 'BASIC') {
      this.setState({informationSharing: 'Basic Information'});
    } else if (value === 'BASICANDADDITIONAL') {
      this.setState({informationSharing: 'Basic + Additional Information'});
    } else {
      this.setState({informationSharing: 'Private'});
    }
  };
  displayInfoSharingScope = (index) => {
    if (index === 0) {
      return 'BASIC';
    } else if (index === 1) {
      return 'BASICANDADDITIONAL';
    } else {
      return 'PRIVATE';
    }
  };

  displayProfileSharingLocation = (value) => {
    if (value === 'OUTSIDEWN') {
      this.setState({profileSharingLocation: 'Outside WeNaturalists'});
    } else {
      this.setState({profileSharingLocation: 'Private'});
    }
  };
  displayProfileSharingLocationScope = (index) => {
    if (index === 0) {
      return 'OUTSIDEWN';
    } else {
      return 'PRIVATE';
    }
  };

  displayProjectSharing = (value) => {
    if (value === 'GLOBAL') {
      this.setState({project: 'Everyone on WeNaturalists'});
    } else if (value === 'OUTSIDEWN') {
      this.setState({project: 'Outside WeNaturalists'});
    } else {
      this.setState({project: 'Private'});
    }
  };
  displayProjectSharingScope = (index) => {
    if (index === 0) {
      return 'OUTSIDEWN';
    } else if (index === 1) {
      return 'GLOBAL';
    } else {
      return 'PRIVATE';
    }
  };

  displayExperienceSharing = (value) => {
    if (value === 'GLOBAL') {
      this.setState({experience: 'Everyone on WeNaturalists'});
    } else if (value === 'OUTSIDEWN') {
      this.setState({experience: 'Outside WeNaturalists'});
    } else {
      this.setState({experience: 'Private'});
    }
  };
  displayExperienceSharingScope = (index) => {
    if (index === 0) {
      return 'OUTSIDEWN';
    } else if (index === 1) {
      return 'GLOBAL';
    } else {
      return 'PRIVATE';
    }
  };

  getSharingDetails(userId) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/sharing/getSharingSettings?userId=' +
        userId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data.statusCode === 200 &&
          response.data.body !== null
        ) {
          this.displayProfileSharing(response.data.body.settings.profile);
          this.displayInfoSharing(response.data.body.settings.information);
          this.displayProfileSharingLocation(
            response.data.body.settings.profile_location,
          );
          this.displayProjectSharing(response.data.body.settings.project);
          this.displayExperienceSharing(response.data.body.settings.experience);
        }
      })
      .catch((err) => console.log(err));
  }

  updateSharingSetting = (category, scope) => {
    let postdata = {
      userId: this.props.route.params.id,
      category: category,
      scope: scope,
      type: 'CIRCLE',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/sharing/updateSharingSetting',
      data: postdata,
      withCredentials: true,
    }).then((response) => {
      if (
        response &&
        response.data.statusCode === 200 &&
        response.data.body !== null
      ) {
        // this.setState({ sharingDetails: response.data.body })
        Snackbar.show({
          backgroundColor: COLORS.dark_900,
          text: 'Updated Successfully',
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        });
      }
    });
  };

  profileSharingModal = () => {
    return (
      <Modal
        visible={this.state.profileSharingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 560,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({profileSharingModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 280, backgroundColor: COLORS.bgfill, paddingBottom: 10},
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                PROFILE SHARING
              </Text>
            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingBottom: 0,
                  paddingTop: 46,
                  width: '100%',
                }}
                style={{height: '50%'}}
                keyExtractor={(item) => item}
                data={this.state.profileSharingOptions}
                initialNumToRender={10}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        profileSharing: item,
                        profileSharingModalOpen: false,
                      });
                      this.updateSharingSetting(
                        'profile',
                        this.displayProfileSharingScope(index),
                      );
                    }}
                    style={styles.item}
                    activeOpacity={0.7}>
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.Button_Lead,
                        {color: COLORS.dark_600, textAlign: 'center'},
                      ]}>
                      {item}
                    </Text>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      {/* {this.state.selectedprofileSharing.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  informationSharingModal = () => {
    return (
      <Modal
        visible={this.state.informationSharingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 560,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({informationSharingModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 250, backgroundColor: COLORS.bgfill, paddingBottom: 10},
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                INFORMATION SHARING
              </Text>
            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingBottom: 0,
                  paddingTop: 46,
                  width: '100%',
                }}
                style={{height: '50%'}}
                keyExtractor={(item) => item}
                data={this.state.informationSharingOptions}
                initialNumToRender={10}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        informationSharing: item,
                        informationSharingModalOpen: false,
                      });
                      this.updateSharingSetting(
                        'information',
                        this.displayInfoSharingScope(index),
                      );
                    }}
                    style={styles.item}
                    activeOpacity={0.7}>
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.Button_Lead,
                        {color: COLORS.dark_600, textAlign: 'center'},
                      ]}>
                      {item}
                    </Text>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      {/* {this.state.selectedinformationSharing.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  profileSharingLocationModal = () => {
    return (
      <Modal
        visible={this.state.profileSharingLocationModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 560,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({profileSharingLocationModalOpen: false})
            }>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 210, backgroundColor: COLORS.bgfill, paddingBottom: 10},
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                PROFILE SHARING LOCATION
              </Text>
            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingBottom: 0,
                  paddingTop: 46,
                  width: '100%',
                }}
                style={{height: '50%'}}
                keyExtractor={(item) => item}
                data={this.state.profileSharingLocationOptions}
                initialNumToRender={10}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        profileSharingLocation: item,
                        profileSharingLocationModalOpen: false,
                      });
                      this.updateSharingSetting(
                        'profile_location',
                        this.displayProfileSharingLocationScope(index),
                      );
                    }}
                    style={styles.item}
                    activeOpacity={0.7}>
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.Button_Lead,
                        {color: COLORS.dark_600, textAlign: 'center'},
                      ]}>
                      {item}
                    </Text>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      {/* {this.state.selectedprofileSharingLocation.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  sharingProjectModal = () => {
    return (
      <Modal
        visible={this.state.sharingProjectModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 560,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({sharingProjectModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 260, backgroundColor: COLORS.bgfill, paddingBottom: 10},
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                PROJECT SHARING
              </Text>
            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingBottom: 0,
                  paddingTop: 46,
                  width: '100%',
                }}
                style={{height: '50%'}}
                keyExtractor={(item) => item}
                data={this.state.sharingProjectOptions}
                initialNumToRender={10}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        project: item,
                        sharingProjectModalOpen: false,
                      });
                      this.updateSharingSetting(
                        'project',
                        this.displayProjectSharingScope(index),
                      );
                    }}
                    style={styles.item}
                    activeOpacity={0.7}>
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.Button_Lead,
                        {color: COLORS.dark_600, textAlign: 'center'},
                      ]}>
                      {item}
                    </Text>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      {/* {this.state.selectedsharingProject.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  sharingExperienceModal = () => {
    return (
      <Modal
        visible={this.state.sharingExperienceModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 560,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({sharingExperienceModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 260, backgroundColor: COLORS.bgfill, paddingBottom: 10},
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                EXPERIENCE SHARING
              </Text>
            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingBottom: 0,
                  paddingTop: 46,
                  width: '100%',
                }}
                style={{height: '50%'}}
                keyExtractor={(item) => item}
                data={this.state.sharingProjectOptions}
                initialNumToRender={10}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        experience: item,
                        sharingExperienceModalOpen: false,
                      });
                      this.updateSharingSetting(
                        'experience',
                        this.displayExperienceSharingScope(index),
                      );
                    }}
                    style={styles.item}
                    activeOpacity={0.7}>
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.Button_Lead,
                        {color: COLORS.dark_600, textAlign: 'center'},
                      ]}>
                      {item}
                    </Text>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      {/* {this.state.selectedsharingExperience.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: COLORS.bgFill_200,
          paddingBottom: 10,
        }}>
        <ScrollView>
          {this.profileSharingModal()}
          {this.informationSharingModal()}
          {this.profileSharingLocationModal()}
          {this.sharingProjectModal()}
          {this.sharingExperienceModal()}

          <ProfileEditHeader
            name="Privacy Settings"
            iconName="Shield_Tick"
            goback={this.goback}
            addButton={false}
          />

          <View
            style={{
              alignSelf: 'center',
              flexDirection: 'row',
              backgroundColor: COLORS.altgreen_t50,
              width: 320,
              marginTop: 20,
              justifyContent: 'center',
              borderRadius: 4,
            }}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('CirclePrivacyVisibility')
              }
              style={[
                defaultShape.InTab_Btn,
                {
                  backgroundColor: COLORS.altgreen_t50,
                  width: '50%',
                  borderRadius: 4,
                },
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Visibility
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                defaultShape.InTab_Btn,
                {backgroundColor: '#fff', width: '50%', borderRadius: 4},
              ]}>
              <Text style={[defaultStyle.Caption, {color: COLORS.dark_800}]}>
                Sharing
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              backgroundColor: COLORS.white,
              width: '100%',
              marginTop: 20,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 8},
              ]}>
              Profile sharing
            </Text>
            <Text
              style={[
                defaultStyle.Note2,
                {color: COLORS.altgreen_300, marginTop: 6, marginLeft: 8},
              ]}>
              Set preference on who all can share your profile. You may further
              customise it by selecting individual preference below
            </Text>

            <TouchableOpacity
              onPress={() => this.setState({profileSharingModalOpen: true})}
              activeOpacity={0.6}
              style={{
                height: 44,
                width: '98%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 10,
                borderWidth: 1,
                borderColor: COLORS.grey_300,
                borderRadius: 8,
                paddingHorizontal: 10,
              }}>
              <Text style={[defaultStyle.Subtitle_1, {color: COLORS.dark_700}]}>
                {this.state.profileSharing}
              </Text>

              <View
                style={{
                  width: 16,
                  height: 16,
                  borderRadius: 8,
                  backgroundColor: '#698F8A',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 0,
                }}>
                <Icon
                  name="Arrow_Down"
                  size={9}
                  color="#FFF"
                  style={{marginTop: Platform.OS === 'android' ? 4 : 0}}
                />
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{
              backgroundColor: COLORS.white,
              width: '100%',
              marginTop: 30,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 8},
              ]}>
              Information sharing
            </Text>
            <Text
              style={[
                defaultStyle.Note2,
                {color: COLORS.altgreen_300, marginTop: 6, marginLeft: 8},
              ]}>
              Set preference on what can be shared in your profile. Selecting
              additional information will mean that you wish to share Basic
              information as well
            </Text>

            <TouchableOpacity
              onPress={() => this.setState({informationSharingModalOpen: true})}
              activeOpacity={0.6}
              style={{
                height: 44,
                width: '98%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 10,
                borderWidth: 1,
                borderColor: COLORS.grey_300,
                borderRadius: 8,
                paddingHorizontal: 10,
              }}>
              <Text style={[defaultStyle.Subtitle_1, {color: COLORS.dark_700}]}>
                {this.state.informationSharing}
              </Text>

              <View
                style={{
                  width: 16,
                  height: 16,
                  borderRadius: 8,
                  backgroundColor: '#698F8A',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 0,
                }}>
                <Icon
                  name="Arrow_Down"
                  size={9}
                  color="#FFF"
                  style={{marginTop: Platform.OS === 'android' ? 4 : 0}}
                />
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{
              backgroundColor: COLORS.white,
              width: '100%',
              marginTop: 30,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 8},
              ]}>
              Profile sharing location
            </Text>
            <Text
              style={[
                defaultStyle.Note2,
                {color: COLORS.altgreen_300, marginTop: 6, marginLeft: 8},
              ]}>
              Set preference on where can someone share your profile. On
              selecting “Outside WeNaturalists”, your profile will be visible to
              people who are not the members of WeNaturalists
            </Text>

            <TouchableOpacity
              onPress={() =>
                this.setState({profileSharingLocationModalOpen: true})
              }
              activeOpacity={0.6}
              style={{
                height: 44,
                width: '98%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 10,
                borderWidth: 1,
                borderColor: COLORS.grey_300,
                borderRadius: 8,
                paddingHorizontal: 10,
              }}>
              <Text style={[defaultStyle.Subtitle_1, {color: COLORS.dark_700}]}>
                {this.state.profileSharingLocation}
              </Text>

              <View
                style={{
                  width: 16,
                  height: 16,
                  borderRadius: 8,
                  backgroundColor: '#698F8A',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 0,
                }}>
                <Icon
                  name="Arrow_Down"
                  size={9}
                  color="#FFF"
                  style={{marginTop: Platform.OS === 'android' ? 4 : 0}}
                />
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: '100%',
              marginTop: 26,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {color: COLORS.dark_800, marginLeft: 8},
              ]}>
              Sharing of Project and Experience
            </Text>
            <Text
              style={[
                defaultStyle.Note2,
                {color: COLORS.altgreen_300, marginTop: 6, marginLeft: 8},
              ]}>
              Set preference on where can someone share your projects and
              experiences. On selecting “Outside WeNaturalists”, your project
              will be visible to people who are not the members of WeNaturalists
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => this.setState({sharingProjectModalOpen: true})}
            activeOpacity={0.7}
            style={{
              borderBottomColor: COLORS.bgFill_200,
              borderBottomWidth: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Project
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4},
                ]}>
                {this.state.project}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => this.setState({sharingExperienceModalOpen: true})}
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: COLORS.white,
              width: '100%',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 8},
              ]}>
              Experience
            </Text>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 4},
                ]}>
                {this.state.experience}
              </Text>
              <Icon
                name="Arrow_Right"
                size={14}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'android' ? 7.5 : 0}}
              />
            </View>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  item: {
    flexDirection: 'row',
    width: '83%',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'left',
    marginLeft: 30,
    // marginRight: 100,
    paddingHorizontal: '3%',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey_200,
    // backgroundColor: 'pink'
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '90%',
    height: 26,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    zIndex: 2,
  },
  updateButton: {
    flexDirection: 'row',
    width: 81,
    height: 28,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: COLORS.dark_600,
  },
  circleCard: {
    flexDirection: 'row',
    width: '90%',
    height: 80,
    borderRadius: 8,
    backgroundColor: COLORS.bgFill_200,
    marginHorizontal: 8,
    marginVertical: 4,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: COLORS.grey_200,
    borderBottomWidth: 1,
    alignSelf: 'center',
    // paddingLeft: 12,
    // paddingRight: 26
  },
  circleTitle: {
    color: '#00394D',
    fontSize: 12,
    fontFamily: 'Montserrat-Bold',
    // marginTop: 10,
    // maxWidth: '85%'
  },
  circleSubTitle: {
    color: '#607580',
    fontSize: 10,
    fontFamily: 'Montserrat-Medium',
    // maxWidth: '85%'
  },
});
