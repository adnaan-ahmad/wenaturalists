import React, { Component } from 'react'
import { Image, StyleSheet, FlatList, TouchableOpacity, View, Text, SafeAreaView } from 'react-native'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import defaultProfile from '../../../../assets/defaultProfile.png'
import typography from '../../../Components/Shared/Typography'
import defaultCover from '../../../../assets/defaultCover.png'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultStyle from '../../../Components/Shared/Typography'
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const moment = require('moment')

export default class ReceivedInvitations extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            receivedInvitations: []
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value })
            this.getRecivedMemberList(value)

        }).catch((e) => {
            console.log(e)
        })

    }

    getRecivedMemberList = (userId) => {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/circle/get/received/member/' + userId + '/' + this.props.route.params.id + '/' + "?page=" + 0 + "&size=" + 1000,
            header: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then((response) => {
            let res = response.data
            if (res && res.status === '200 OK') {
                // console.log('getRecivedMemberList', res.body.content[0])
                this.setState({
                    receivedInvitations: res.body.content
                })
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    goback = () => this.props.navigation.goBack()

    acceptRequest = (userId) => {
        let postData = {
            circleId: this.props.route.params.id,
            userId: userId,
            adminId: this.state.userId
        }
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/circle/accept/request/',
            data: postData,
            header: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then((response) => {
            if (response && response.data && response.data.status === '202 ACCEPTED') {
                // console.log(response.data.status)
                this.getRecivedMemberList(this.state.userId)
            }
        }).catch((err) => {
            console.log(err)
        })

    }

    declineRequest = (userId) => {
        let postData = {
            circleId: this.props.route.params.id,
            userId: userId,
            adminId: this.state.userId
        }
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/circle/decline/request/admin/',
            data: postData,
            header: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then((response) => {
            if (response && response.data && response.data.status === '202 ACCEPTED') {
                // console.log(response.data.status)
                this.getRecivedMemberList(this.state.userId)
            }
        }).catch((err) => {
            console.log(err)
        })

    }

    stickyHeader = () => {
        return (
            <View style={{ backgroundColor: COLORS.white, position: 'absolute', top: 0, zIndex: 2 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: COLORS.white }}>

                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', padding: 10 }}>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TouchableOpacity activeOpacity={0.5}
                                onPress={() => this.props.navigation.navigate('CircleMembers')}
                                style={{ backgroundColor: COLORS.white, alignItems: 'center', justifyContent: 'center', height: 42, width: 42, borderRadius: 21, marginTop: 6 }}>
                                <Icon name="Arrow-Left" size={16} color="#00394D" style={{ marginTop: 5 }} />
                            </TouchableOpacity>


                            <View style={{ alignSelf: 'center', marginLeft: 8 }}>

                                <Text numberOfLines={1} style={[typography.Title_1, { color: COLORS.dark_800, fontSize: 14, maxWidth: 150 }]}>
                                    INVITATIONS
                                </Text>

                            </View>

                        </View>

                    </View>

                </View>

            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                {/***** Header starts *****/}

                {this.stickyHeader()}

                {/***** Header ends *****/}

                <View style={{ alignSelf: 'center', flexDirection: 'row', backgroundColor: COLORS.altgreen_t50, width: 300, marginTop: 90, marginBottom: 10, justifyContent: 'center', borderRadius: 4 }}>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('SentInvitations')}
                        style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 150, borderTopLeftRadius: 4, borderBottomLeftRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Sent</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={[defaultShape.InTab_Btn, { backgroundColor: '#fff', width: 150, borderRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: COLORS.dark_800 }]}>Received</Text>
                    </TouchableOpacity>

                </View>

                <View style={{ width: '100%', height: 24, backgroundColor: COLORS.altgreen_200, justifyContent: 'center', alignItems: 'center', marginTop: 18 }}>
                    <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400, textAlign: 'center' }]}>{this.state.receivedInvitations.length} PENDING</Text>
                </View>

                <FlatList
                    keyboardShouldPersistTaps='handled'
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 26, paddingTop: 16 }}
                    style={{ height: '50%' }}
                    keyExtractor={(item) => item.id}
                    data={this.state.receivedInvitations}
                    initialNumToRender={10}
                    renderItem={({ item }) => (

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfileStack', {
                            screen: 'OtherProfileScreen',
                            params: { userId: item.id },
                        })}
                            activeOpacity={0.7} style={{ marginBottom: 6 }}>
                            <View style={styles.item}>

                                <Image source={item.originalProfileImage ? { uri: item.originalProfileImage } : defaultProfile} style={[styles.image, { marginLeft: '7%' }]} />

                                <View style={styles.nameMsg}>
                                    <Text numberOfLines={1} style={styles.name}>{item.username.split(' ').slice(0, 2).join(' ').charAt(0).toUpperCase() + item.username.split(' ').slice(0, 2).join(' ').slice(1)}</Text>

                                    {item && item.country ?
                                        <Text style={[defaultStyle.Note2, { color: 'rgb(136, 136, 136)' }]}>{item.country.charAt(0).toUpperCase() + item.country.slice(1)}</Text> :
                                        <></>}
                                    {item && item.persona ?
                                        <Text style={[defaultStyle.Note2, { color: 'rgb(105, 143, 138)' }]}>{moment.unix(item.createTime / 1000).format('Do MMM, YYYY')}</Text> :
                                        <></>}
                                </View>

                                <TouchableOpacity
                                    onPress={() => this.acceptRequest(item.userId)}
                                    style={{ width: 70, height: 28, borderRadius: 17, backgroundColor: COLORS.dark_700, justifyContent: 'center', alignItems: 'center', marginRight: 6 }}
                                    activeOpacity={0.8} >
                                    <Text style={[defaultStyle.OVERLINE, { color: COLORS.altgreen_200 }]}>Accept</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.declineRequest(item.userId)}
                                    style={{ width: 30, height: 30, borderRadius: 15, justifyContent: 'center', alignItems: 'center', marginRight: 16, backgroundColor: COLORS.grey_200 }} activeOpacity={0.5} >
                                    <Icon name='Cross' size={12} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                                </TouchableOpacity>

                            </View>

                        </TouchableOpacity>


                    )}
                />

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    item: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#F7F7F500',
        width: '100%',
        height: 52,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',
        marginBottom: 8
    },
    image: {
        height: 36,
        width: 36,
        borderRadius: 18
    },
    nameMsg: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: '6%'
    },
    name: {
        fontWeight: '700',
        color: '#4B4F56',
        fontSize: 14,
        textAlign: 'left',
        maxWidth: 170
    },
})
