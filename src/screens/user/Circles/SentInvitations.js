import React, { Component } from 'react'
import { Modal, Image, StyleSheet, FlatList, TouchableOpacity, View, Text, SafeAreaView, Pressable } from 'react-native'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import defaultProfile from '../../../../assets/defaultProfile.png'
import typography from '../../../Components/Shared/Typography'
import defaultCover from '../../../../assets/defaultCover.png'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultStyle from '../../../Components/Shared/Typography'
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader'
import NewInvite from '../../../Components/User/Circle/NewInvite'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class ReceivedInvitations extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            receivedInvitations: [],
            usersSuggestion: [],
            newInviteModalOpen: false
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value })
            this.getUsersSuggestion()
            this.getSentMemberList(value)
        }).catch((e) => {
            console.log(e)
        })

    }

    getUsersSuggestion = () => {
        let url = REACT_APP_userServiceURL +
            "/circle/get/config/" + this.state.userId + "/" + "?circleId=" + this.props.route.params.id
        axios({
            method: 'get',
            url: url,
            headers: {'Content-Type': 'application/json'},
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.message === 'Success!') {
                // if(response.data.body.privacySettingConfigDto && !mappingUtils.getPrivacySettingData(response.data.body.privacySettingConfigDto, 'member_tab')){
                //     window.location.href="/circle/" + this.state.circleData.slug;
                // }
                // let userInfos = (response.data.body.inviteeDetails);
                // let users = [];
                // userInfos.map((userInfo) => {
                //     let user = {
                //         value: userInfo.username,
                //         img: userInfo.imageUrl && !_.isNull(userInfo.imageUrl) ? userInfo.imageUrl :  userInfo.type == "INDIVIDUAL" ? "https://cdn.dscovr.com/images/users1.png" : "https://cdn.dscovr.com/images/DefaultBusiness.png",
                //         id: userInfo.id,
                //     };
                //     users.push(user);
                // });
                // console.log('inviteeDetails', response.data.body.inviteeDetails[2])
                this.setState({usersSuggestion: response.data.body.inviteeDetails})
            }
        }).catch((err) => {
            console.log(err)
        })

    }

    changeState = (value) => {
        this.setState(value)
    }

    newInviteModal = () => {
        return (
          <Modal visible={this.state.newInviteModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
            <NewInvite getUsersSuggestion={this.getUsersSuggestion} getSentMemberList={this.getSentMemberList} changeState={this.changeState} connectsData={this.state.usersSuggestion} circleId={this.props.route.params.id} />
          </Modal>
        )
    }

    goback = () => this.props.navigation.goBack()


    getSentMemberList = (userId) => {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/circle/get/sent/member/' + userId + '/' + this.props.route.params.id + '/' + "?page=" + 0 + "&size=" + 1000,
            header: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then((response) => {
            let res = response.data
            if (res && res.status === '200 OK') {
                console.log('getSentMemberList', res.body.content[0])
                this.setState({
                    receivedInvitations: res.body.content
                })
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    withdrawRequest = (userId) => {
        let postData = {
            circleId: this.props.route.params.id,
            userId: userId,
            adminId: this.state.userId
        }
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/circle/withdraw/member/request/',
            data: postData,
            header: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then((response) => {
            if (response && response.data && response.data.status === '202 ACCEPTED') {
                console.log(response.data.status)
                this.getSentMemberList(this.state.userId)
                this.getUsersSuggestion()
            }
        }).catch((err) => {
            console.log(err)
        })

    }

    acceptConnectInvitation = (id) => {
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/graph/users/' + id + '/connectTo/' + this.state.userId,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.status === 202) {
                this.getInvitations(this.state.userId)
                this.getUsersSuggestion()
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    stickyHeader = () => {
        return (
            <View style={{ backgroundColor: COLORS.white, position: 'absolute', top: 0, zIndex: 2 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: COLORS.white }}>

                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', padding: 10 }}>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TouchableOpacity activeOpacity={0.5}
                                onPress={() => this.props.navigation.goBack()}
                                style={{ backgroundColor: COLORS.white, alignItems: 'center', justifyContent: 'center', height: 42, width: 42, borderRadius: 21, marginTop: 6 }}>
                                <Icon name="Arrow-Left" size={16} color="#00394D" style={{ marginTop: 5 }} />
                            </TouchableOpacity>


                            <View style={{ alignSelf: 'center', marginLeft: 8 }}>

                                <Text numberOfLines={1} style={[typography.Title_1, { color: COLORS.dark_800, fontSize: 14, maxWidth: 150 }]}>
                                    INVITATIONS
                                </Text>

                            </View>

                        </View>

                        <TouchableOpacity
                            onPress={() =>
                                this.setState({ newInviteModalOpen: true })
                            }
                            activeOpacity={0.5}
                            style={[styles.mycircle, { backgroundColor: '#D9E1E4' }]}>
                            <View
                                style={{
                                    marginRight: 6,
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                }}>
                                <Icon
                                    name="Add_Group"
                                    color={COLORS.dark_700}
                                    size={14}
                                    style={{ marginTop: Platform.OS === 'ios' ? 0 : 8 }}
                                />
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                }}>
                                <Text
                                    style={[
                                        defaultStyle.Button_Lead,
                                        { color: COLORS.dark_600, fontSize: 13 },
                                    ]}>
                                    Invite
                                </Text>
                            </View>
                            
                        </TouchableOpacity>

                    </View>

                </View>

            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {this.newInviteModal()}

                {/***** Header starts *****/}

                {/* <ProfileEditHeader name="Invitations" iconName="Mail_OL" goback={this.goback} /> */}
                {this.stickyHeader()}

                {/***** Header ends *****/}

                <View style={{ alignSelf: 'center', flexDirection: 'row', backgroundColor: COLORS.altgreen_t50, width: 300, marginTop: 90, marginBottom: 0, justifyContent: 'center', borderRadius: 4 }}>

                    <TouchableOpacity
                        style={[defaultShape.InTab_Btn, { backgroundColor: '#fff', width: 150, borderRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: COLORS.dark_800 }]}>Sent</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ReceivedInvitations', { id: this.props.route.params.id })}
                        style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 150, borderTopRightRadius: 4, borderBottomRightRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Received</Text>
                    </TouchableOpacity>

                </View>

                <View style={{ width: '100%', height: 24, backgroundColor: COLORS.altgreen_200, justifyContent: 'center', alignItems: 'center', marginTop: 18 }}>
                    <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400, textAlign: 'center' }]}>{this.state.receivedInvitations.length} PENDING</Text>
                </View>

                <FlatList
                    keyboardShouldPersistTaps='handled'
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 26, paddingTop: 16 }}
                    style={{ height: '50%' }}
                    keyExtractor={(item) => item.id}
                    data={this.state.receivedInvitations}
                    initialNumToRender={10}
                    renderItem={({ item }) => (

                        <View style={{ marginBottom: 8 }}>
                            <View style={styles.item}>
                                <Pressable onPress={() => this.props.navigation.navigate('ProfileStack', {
                                    screen: 'OtherProfileScreen',
                                    params: { userId: item.userId },
                                })} style={{ flexDirection: 'row', width: '66%', marginLeft: 10 }}>
                                    <Image source={item.originalProfileImage ? { uri: item.originalProfileImage } : defaultProfile} style={[styles.image, { marginLeft: '7%' }]} />

                                    <View style={styles.nameMsg}>
                                        <Text style={styles.name}>{item.username.split(' ').slice(0, 2).join(' ').charAt(0).toUpperCase() + item.username.split(' ').slice(0, 2).join(' ').slice(1)}</Text>

                                        {item.country && item.country ?
                                            <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>{item.country.charAt(0).toUpperCase() + item.country.slice(1)}</Text> :
                                            <></>}

                                        <Text numberOfLines={1} style={[defaultStyle.Note2, { color: '#698f8a', maxWidth: 180, fontSize: 10 }]}>Invited by <Text style={{
                                            fontWeight: '600',
                                            color: '#4B4F56',
                                            fontSize: 10,
                                            textAlign: 'left',
                                            textTransform: 'capitalize'
                                        }}>{item.createdByName}</Text></Text>

                                    </View>
                                </Pressable>
                                {item.status === 'Pending' ?
                                    <TouchableOpacity
                                        onPress={() => this.withdrawRequest(item.userId)}
                                        style={{ borderRadius: 17, borderColor: COLORS.altgreen_300, borderWidth: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 6, paddingVertical: 5, marginRight: 12 }}
                                        activeOpacity={0.6} >
                                        <Text style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>Withdraw Invite</Text>
                                    </TouchableOpacity>
                                    :
                                    item.status === 'Withdrawn' ?
                                        <View
                                            style={{ borderRadius: 17, justifyContent: 'space-between', alignItems: 'center', marginRight: 18, backgroundColor: COLORS.grey_200, paddingHorizontal: 6, paddingVertical: 0, flexDirection: 'row' }}>
                                            <Icon name='FollowTick' size={12} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 7 : 0, marginRight: 3 }} />
                                            <Text style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>Withdrawn</Text>
                                        </View> : <></>}

                            </View>

                        </View>


                    )}
                />

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    item: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#F7F7F500',
        width: '100%',
        height: 52,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',
        marginBottom: 8
    },
    mycircle: {
        // width: '92%',
        height: 34,
        backgroundColor: COLORS.dark_700,
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignSelf: 'center',
        marginRight: 4,
        alignItems: 'center',
        borderRadius: 8,
        paddingHorizontal: 14
    },
    image: {
        height: 36,
        width: 36,
        borderRadius: 18
    },
    nameMsg: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: '6%'
    },
    name: {
        fontWeight: '700',
        color: '#4B4F56',
        fontSize: 14,
        textAlign: 'left'
    },
})
