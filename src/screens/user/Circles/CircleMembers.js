import React, { Component } from 'react'
import { Clipboard, Share, Modal, ImageBackground, TextInput, Dimensions, FlatList, View, Text, TouchableOpacity, Image, StyleSheet, Platform, SafeAreaView } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'
import axios from 'axios'
import Snackbar from 'react-native-snackbar'
import AsyncStorage from '@react-native-community/async-storage'

import defaultProfile from '../../../../assets/defaultProfile.png'
import circleDefault from '../../../../assets/CirclesDefault.png'
import httpService from '../../../services/AxiosInterceptors'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultCover from '../../../../assets/defaultCover.png'
import { REACT_APP_userServiceURL, REACT_APP_domainUrl } from '../../../../env.json'
import { COLORS } from '../../../Components/Shared/Colors'
import typography from '../../../Components/Shared/Typography'
import defaultShape from '../../../Components/Shared/Shape'
import defaultStyle from '../../../Components/Shared/Typography'

httpService.setupInterceptors()
const Icon = createIconSetFromIcoMoon(icoMoonConfig)

const screenHeight = Dimensions.get('window').height

export default class AddCircleMembers extends Component {

    constructor(props) {
        super(props)

        this.state = {
            users: [],
            selectedPage: '',
            name: '',
            searchText: '',
            searchIcon: true,
            userId: '',
            memberListData: [],
            circleData: {},
            postModalOpen: false,
            sortBy: 'firstName',
            optionsModalOpen: false,
            shareModalOpen: false,
            customUrl: '',
            pressedUserId: '',
            reasonForReportingModalOpen: false,
            reasonForReporting: 'FAKE_SPAM_OR_SCAM',
            description: '',
            pressedItem: {}
        }
    }

    componentDidMount() {

        let obj = JSON.parse(this.props.route.params.circleData)
        this.setState({ circleData: obj })

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value })
            this.props.route.params && this.props.route.params.id ? this.getMemberList(0, 100, this.props.route.params.id, value) : null

        }).catch((e) => {
            console.log(e)
        })
    }

    searchNames = (query) => {

        const regex = new RegExp(`${query.trim()}`, 'i')

        if (this.state.sortBy === 'firstName' && this.state.memberListData) {
            return this.state.memberListData.sort((a, b) => a.username.toUpperCase() > b.username.toUpperCase()).filter(item => (item.username).search(regex) >= 0)
        }
        else if (this.state.sortBy === 'lastName' && this.state.memberListData) {
            return this.state.memberListData.sort((a, b) => a.username.toUpperCase().split(' ')[1] > b.username.toUpperCase().split(' ')[1]).filter(item => (item.username).search(regex) >= 0)
        }
        else if (this.state.sortBy === 'recentlyAdded' && this.state.memberListData) {
            return this.state.memberListData.sort((a, b) => a.createTime < b.createTime).filter(item => (item.username).search(regex) >= 0)
        }
        else if (this.state.sortBy === 'country' && this.state.memberListData) {
            let array = this.state.memberListData.sort((a, b) => {

                return (a.country === null) - (b.country === null)
            })
            let array2 = array.sort((a, b) => {

                return a.country && b.country ? a.country.toUpperCase() > b.country.toUpperCase() : a
            })
            return array2.filter(item => (item.username).search(regex) >= 0)
        }
    }

    postModal = () => {

        return (

            <Modal onRequestClose={() => this.setState({ postModalOpen: false })}
                visible={this.state.postModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={[defaultShape.Linear_Gradient_View, { bottom: 100 }]}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ postModalOpen: false })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
                    </TouchableOpacity>

                    <Text style={[typography.Caption, { fontSize: 12, color: COLORS.dark_500, textAlign: 'center', alignSelf: 'center', position: 'absolute', bottom: 110, zIndex: 2 }]}>Sort By</Text>

                    <View style={[defaultShape.Modal_Categories_Container, { paddingTop: 10, height: 140, flexDirection: 'row', justifyContent: 'space-evenly' }]}>

                        <TouchableOpacity style={this.state.sortBy === 'firstName' ? styles.selectedText : styles.unselectedText} activeOpacity={0.6} onPress={() => { this.setState({ postModalOpen: false, sortBy: 'firstName' }) }} >

                            <Text style={[typography.Caption, { fontSize: 11, color: this.state.sortBy === 'firstName' ? COLORS.altgreen_200 : COLORS.dark_500, textAlign: 'center', alignSelf: 'center' }]}>First Name</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={this.state.sortBy === 'lastName' ? styles.selectedText : styles.unselectedText} activeOpacity={0.6} onPress={() => { this.setState({ postModalOpen: false, sortBy: 'lastName' }) }}>

                            <Text style={[typography.Caption, { fontSize: 11, color: this.state.sortBy === 'lastName' ? COLORS.altgreen_200 : COLORS.dark_500, textAlign: 'center', alignSelf: 'center' }]}>Last Name</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={this.state.sortBy === 'recentlyAdded' ? styles.selectedText : styles.unselectedText} activeOpacity={0.6} onPress={() => { this.setState({ postModalOpen: false, sortBy: 'recentlyAdded' }) }}>

                            <Text style={[typography.Caption, { fontSize: 11, color: this.state.sortBy === 'recentlyAdded' ? COLORS.altgreen_200 : COLORS.dark_500, textAlign: 'center', alignSelf: 'center' }]}>Recently Added</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={this.state.sortBy === 'country' ? styles.selectedText : styles.unselectedText} activeOpacity={0.6} onPress={() => { this.setState({ postModalOpen: false, sortBy: 'country' }) }}>

                            <Text style={[typography.Caption, { fontSize: 11, color: this.state.sortBy === 'country' ? COLORS.altgreen_200 : COLORS.dark_500, textAlign: 'center', alignSelf: 'center' }]}>Country</Text>
                        </TouchableOpacity>

                    </View>

                </View>

            </Modal>

        )
    }

    getMemberList = (page, size, id, userId) => {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/circle/get/member/list/' + userId + '/' + id + '/' + "?page=" + page + "&size=" + size,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then((response) => {
            let res = response.data;
            if (res && res.status === '200 OK') {
                this.setState({
                    memberListData: res.body.content
                })
                // console.log('res.body.content[0]', res.body.content)
            }
        }).catch((err) => {
            // console.log('error!!!', err)
        })
    }

    stickyHeader = () => {
        return (
            <View style={{ backgroundColor: COLORS.white, position: 'absolute', top: 0, zIndex: 2 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: COLORS.white }}>

                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', padding: 10 }}>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TouchableOpacity activeOpacity={0.5}
                                onPress={() => this.props.navigation.navigate('CircleProfile')}
                                style={{ backgroundColor: COLORS.white, alignItems: 'center', justifyContent: 'center', height: 42, width: 42, borderRadius: 21, marginTop: 6 }}>
                                <Icon name="Arrow-Left" size={16} color="#00394D" style={{ marginTop: 5 }} />
                            </TouchableOpacity>

                            {/* {(this.props.user.body && this.props.user.body.originalProfileImage) ?
                                <Image source={{ uri: this.props.user.body.originalProfileImage }} style={{ width: 30, height: 30, borderRadius: 15 }} />
                                : null} */}
                            <Image source={this.state.circleData && this.state.circleData.profileImage ? { uri: this.state.circleData.profileImage } : circleDefault} style={{ width: 30, height: 30, borderRadius: 15 }} />



                            <View style={{ alignSelf: 'center', marginLeft: 8 }}>
                                {/* {this.props.user.body !== undefined ? */}
                                <Text numberOfLines={1} style={[typography.Title_1, { color: COLORS.dark_800, fontSize: 14, maxWidth: 150 }]}>
                                    {/* {this.props.user.body.userName} */}
                                    {this.state.circleData && this.state.circleData.title}
                                </Text>


                                <Text style={[typography.Subtitle_1, { color: COLORS.altgreen_400, fontSize: 11, marginTop: -6 }]}>

                                    {this.state.circleData && this.state.circleData.location ? this.state.circleData.location.city + ', ' : null}
                                    {this.state.circleData && this.state.circleData.location ? this.state.circleData.location.country : null}
                                </Text>
                            </View>

                        </View>

                        <TouchableOpacity
                            onPress={() =>
                                this.props.navigation.navigate('SentInvitations', { id: this.state.circleData.id })
                            }
                            activeOpacity={0.5}
                            style={[styles.mycircle, { backgroundColor: '#D9E1E4' }]}>
                            <View
                                style={{
                                    // marginLeft: 15,
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                }}>
                                <Text
                                    style={[
                                        defaultStyle.Button_Lead,
                                        { color: COLORS.dark_600, marginLeft: 10 },
                                    ]}>
                                    Invitations
                                </Text>
                            </View>
                            <View
                                style={{
                                    marginRight: 6,
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                }}>
                                <Icon
                                    name="Arrow_Right"
                                    color={COLORS.dark_700}
                                    size={14}
                                    style={{ marginTop: Platform.OS === 'ios' ? 0 : 8 }}
                                />
                            </View>
                        </TouchableOpacity>

                    </View>

                </View>

                {this.state.memberListData.length >= 6 ?
                    <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-evenly', width: 300 }}>

                        <TouchableOpacity activeOpacity={0.8} style={{ paddingTop: 8, width: 100 }} onPress={() => this.props.navigation.navigate('CircleActivities', { id: this.props.route.params.id, circleData: JSON.stringify(this.state.circleData) })}>
                            <Text style={[typography.Button_Lead, { color: COLORS.altgreen_400, marginBottom: 6, textAlign: 'center' }]}>ACTIVITIES</Text>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={0.8} style={{ paddingTop: 8, width: 100 }} onPress={() => this.props.navigation.navigate('OngoingProjects', { id: this.props.route.params.id, circleData: JSON.stringify(this.state.circleData) })}>
                            <Text style={[defaultStyle.Button_Lead, { color: COLORS.altgreen_400, marginBottom: 6, textAlign: 'center' }]}>PROJECTS</Text>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={0.8} style={{ width: 100 }}>
                            <Text style={[typography.Button_2, { color: COLORS.dark_800, textAlign: 'center', marginBottom: 6, paddingTop: 8 }]}>MEMBERS</Text>
                            <View style={{ width: 100, height: 5, backgroundColor: COLORS.dark_800, borderTopLeftRadius: 4, borderTopRightRadius: 4 }}></View>
                        </TouchableOpacity>

                    </View>
                    :
                    <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-evenly', width: 240 }}>

                        <TouchableOpacity activeOpacity={0.8} style={{ paddingTop: 8, width: 120 }} onPress={() => this.props.navigation.navigate('OngoingProjects', { id: this.props.route.params.id, circleData: JSON.stringify(this.state.circleData) })}>
                            <Text style={[defaultStyle.Button_Lead, { color: COLORS.altgreen_400, marginBottom: 6, textAlign: 'center' }]}>PROJECTS</Text>
                        </TouchableOpacity>
                        
                        <TouchableOpacity activeOpacity={0.8} style={{ width: 120 }}>
                            <Text style={[typography.Button_2, { color: COLORS.dark_800, textAlign: 'center', marginBottom: 6, paddingTop: 8 }]}>MEMBERS</Text>
                            <View style={{ width: 120, height: 5, backgroundColor: COLORS.dark_800, borderTopLeftRadius: 4, borderTopRightRadius: 4 }}></View>
                        </TouchableOpacity>

                    </View>}

            </View>
        )
    }

    revokeMemberShip = () => {

        let postData = {
            circleId: this.state.circleData.id,
            adminId: this.state.userId,
            userId: this.state.pressedItem.userId
        }
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/circle/revoke/member/request/',
            headers: { 'Content-Type': 'application/json' },
            data: postData,
            withCredentials: true
        }).then((response) => {
            let res = response.data
            if (res && res.status === '202 ACCEPTED') {
                // console.log(res.status)
                this.getMemberList(0, 100, this.props.route.params.id, this.state.userId)
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    revokeAdmin = () => {

        let postData = {
            circleId: this.state.circleData.id,
            superAdminId: this.state.userId && this.state.userId,
            userId: this.state.pressedItem.userId
        }
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/circle/make/admin/request/',
            headers: { 'Content-Type': 'application/json' },
            data: postData,
            withCredentials: true
        }).then((response) => {
            let res = response.data
            if (res && res.status === '201 CREATED') {
                // console.log(res.status)
                this.getMemberList(0, 100, this.props.route.params.id, this.state.userId)

            }
        }).catch((err) => {
            console.log(err)
        })
    }

    removeAdmin = () => {

        let postData = {
            circleId: this.state.circleData.id,
            superAdminId: this.state.userId && this.state.userId,
            adminId: this.state.pressedItem.userId
        }
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/circle/revoke/admin/request/',
            headers: { 'Content-Type': 'application/json' },
            data: postData,
            withCredentials: true
        }).then((response) => {
            let res = response.data
            if (res && res.status === '202 ACCEPTED') {
                // console.log(res.status)
                this.getMemberList(0, 100, this.props.route.params.id, this.state.userId)

            }
        }).catch((err) => {
            console.log(err)
        })
    }

    optionsModal = () => {
        return (

            <Modal visible={this.state.optionsModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ optionsModalOpen: false })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
                    </TouchableOpacity>

                    <View style={defaultShape.Modal_Categories_Container}>

                        <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Connect</Text>
                        </View>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : defaultShape.ActList_Cell_Gylph_Alt}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({ optionsModalOpen: false }),
                                    // this.props.changeState({ navigate: true, navigateId: this.state.pressedUserId })
                                    this.props.navigation.navigate('ProfileStack', {
                                        screen: 'OtherProfileScreen',
                                        params: { userId: this.state.pressedUserId },
                                    })
                            }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name='SingleUser' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 9 } : {}} />
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>View Profile</Text>
                            </View>
                            <Icon name="Arrow_Right" size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : defaultShape.ActList_Cell_Gylph_Alt} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false, shareModalOpen: true }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Icon name='Share' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Share</Text>
                            </View>
                            <Icon name='Arrow_Right' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>

                        {this.state.pressedItem.type !== 'Super Admin' && <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15 }] : defaultShape.ActList_Cell_Gylph_Alt} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false }, () => this.revokeMemberShip()) }}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Remove Member</Text>
                            <Icon name='RemoveUser' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>}

                        {this.state.pressedItem.type === 'Member' ?
                            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]} activeOpacity={0.6} onPress={() => this.setState({ optionsModalOpen: false }, () => this.revokeAdmin())}>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Make Admin</Text>
                                <Icon name='Administrator' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                            </TouchableOpacity>
                            :
                            this.state.pressedItem.type === 'Admin' ?
                                <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]} activeOpacity={0.6} onPress={() => this.setState({ optionsModalOpen: false }, () => this.removeAdmin())}>
                                    <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Remove Admin</Text>
                                    <Icon name='BlockPerson' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                                </TouchableOpacity>
                                : <></>}
                    </View>
                </View>

            </Modal>

        )
    }

    onShare = async () => {
        try {
            const result = await Share.share({
                message:
                    REACT_APP_domainUrl + '/profile/' + this.state.customUrl,
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    console.log('shared with activity type of result.activityType')
                } else {
                    console.log('shared')
                }
            } else if (result.action === Share.dismissedAction) {
                console.log('dismissed')
            }
        } catch (error) {
            console.log(error.message)
        }
    }

    shareModal = () => {
        return (

            <Modal visible={this.state.shareModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ shareModalOpen: false })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
                    </TouchableOpacity>

                    <View style={defaultShape.Modal_Categories_Container}>

                        <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Share</Text>
                        </View>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15 }] : defaultShape.ActList_Cell_Gylph_Alt} activeOpacity={0.6} onPress={() => { this.setState({ shareModalOpen: false }) }}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Repost on WeNaturalists</Text>
                            <Icon name='Forward' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => {
                            Clipboard.setString(REACT_APP_domainUrl + '/profile/' + this.state.customUrl)
                            Snackbar.show({
                                backgroundColor: '#97A600',
                                text: "Link Copied",
                                textColor: "#00394D",
                                duration: Snackbar.LENGTH_LONG,
                            })
                        }}
                            style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt]}
                            activeOpacity={0.6}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Copy link to profile</Text>
                            <Icon name='TxEdi_AddLink' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt]} activeOpacity={0.6} onPress={() => { this.setState({ shareModalOpen: false }) }}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Share through Mail</Text>
                            <Icon name='Mail_OL' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => { this.setState({ shareModalOpen: false }, () => this.onShare()) }}
                            style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]}
                            activeOpacity={0.6}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Share via others</Text>

                            <View style={[{ flexDirection: 'row', justifyContent: 'space-evenly', width: 100, marginRight: -6 }]}>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name="Social_FB" size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name='Social_Twitter' size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name='Social_LinkedIn' size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name="Meatballs" size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                            </View>

                        </TouchableOpacity>


                    </View>



                </View>

            </Modal>

        )
    }

    reasonForReportingModal = () => {
        return (

            <Modal visible={this.state.reasonForReportingModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={[defaultShape.Linear_Gradient_View, { height: 700, bottom: 0 }]}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={[defaultShape.CloseBtn, { marginBottom: 0 }]}
                        onPress={() => this.setState({ reasonForReportingModalOpen: false })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }} />
                    </TouchableOpacity>


                    <View style={{
                        borderRadius: 20,
                        backgroundColor: COLORS.altgreen_100,
                        alignItems: 'center',
                        paddingTop: 15,
                        paddingBottom: 10,
                        // paddingLeft: 20,
                        width: '90%',
                        alignSelf: 'center',
                        marginBottom: 30,
                        marginTop: 15
                    }}>

                        <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Reason for reporting</Text>
                        </View>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, borderBottomWidth: 0 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'FAKE_SPAM_OR_SCAM' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Fake, spam or scam</Text>
                            </View>


                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'ACCOUNT_MAY_BE_HACKED' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Account may be hacked</Text>
                            </View>

                        </TouchableOpacity>
                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'IMPERSONATING_SOMEONE' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'IMPERSONATING_SOMEONE' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text numberOfLines={2} style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Impersonating someone</Text>
                            </View>

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'VIOLATES_TERMS_OF_USE' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'VIOLATES_TERMS_OF_USE' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Violates Terms Of Use</Text>
                            </View>

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'OTHERS' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'OTHERS' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Others</Text>
                            </View>

                        </TouchableOpacity>

                        {this.state.reasonForReporting === 'OTHERS' &&
                            <View style={{
                                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 6, paddingRight: 12
                            }}>

                                <TextInput
                                    // theme={{ colors: { text: COLORS.dark_700, primary: COLORS.altgreen_300, placeholder: COLORS.altgreen_300 } }}
                                    placeholder="Write the details"
                                    multiline
                                    placeholderTextColor={COLORS.altgreen_300}
                                    // selectionColor='#C8DB6E'
                                    style={[defaultStyle.Subtitle_1, { width: '90%', height: 56, backgroundColor: COLORS.altgreen_100, color: COLORS.dark_700 }]}
                                    onChangeText={(value) => this.setState({ description: value })}
                                    value={this.state.description}
                                />

                            </View>}

                        <TouchableOpacity activeOpacity={0.5}
                            onPress={() => {
                                this.state.reasonForReporting === 'OTHERS' && this.state.description === '' ?
                                    Snackbar.show({
                                        backgroundColor: '#B22222',
                                        text: "Please enter the detail",
                                        duration: Snackbar.LENGTH_LONG,
                                    })
                                    :
                                    (this.handleReportAbuseSubmit(), this.setState({ reasonForReportingModalOpen: false }))
                            }}
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                marginRight: 15,
                                height: 27,
                                marginVertical: 10,
                                borderRadius: 16,
                                textAlign: 'center',
                                borderWidth: 1,
                                borderColor: '#698F8A'
                            }}>
                            <Text style={{
                                color: '#698F8A',
                                fontSize: 14,
                                paddingHorizontal: 14,
                                paddingVertical: 20,
                                fontFamily: 'Montserrat-Medium',
                                fontWeight: 'bold'
                            }}>Submit</Text>
                        </TouchableOpacity>

                    </View>
                </View>

            </Modal>

        )
    }

    handleReportAbuseSubmit = () => {

        let data = {
            reporterId: this.state.userId,
            entityId: this.state.pressedUserId,
            // entityType: this.state.entityType,
            entityType: 'USER',
            reason: this.state.reasonForReporting,
            description: this.state.description
        }

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
            data: data,
            withCredentials: true
        }).then(response => {
            this.setState({ reasonForReporting: this.state.reasonForReporting })
            if (response && response.status === 201) {
                Snackbar.show({
                    backgroundColor: COLORS.primarydark,
                    text: "Your request has been taken and appropriate action will be taken as per our report abuse policy",
                    textColor: COLORS.altgreen_100,
                    duration: Snackbar.LENGTH_LONG,
                })
                // console.log(response)
            } else {
                // console.log(response)

            }
        }).catch((err) => {
            this.setState({ reasonForReporting: this.state.reasonForReporting })
            if (err && err.response && err.response.status === 409) {
                // console.log(err.response)
                Snackbar.show({
                    backgroundColor: COLORS.primarydark,
                    text: "Your report request was already taken",
                    textColor: COLORS.altgreen_100,
                    duration: Snackbar.LENGTH_LONG,
                })

            } else {
                // console.log(err)
                Snackbar.show({
                    backgroundColor: '#B22222',
                    text: "Please check your network or try again later",
                    duration: Snackbar.LENGTH_LONG,
                })
            }
        })
        setTimeout(() => {
            this.setState({ reasonForReporting: 'FAKE_SPAM_OR_SCAM', description: '' })
        }, 1000)
    }

    render() {
        return (
            <SafeAreaView style={{ backgroundColor: '#F7F7F5', height: '107%', position: 'absolute', bottom: -50, width: '100%' }}>


                {this.stickyHeader()}
                {this.postModal()}
                {this.optionsModal()}
                {this.shareModal()}
                {this.reasonForReportingModal()}

                <View style={{ backgroundColor: COLORS.bgFill_200, marginTop: 100 }}>

                    <Text style={[typography.Button_2, { marginLeft: 24, marginTop: 16, color: COLORS.dark_800 }]}>ADMINS</Text>

                    <FlatList
                        keyboardShouldPersistTaps='handled'
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ height: 70, paddingLeft: 16, flexGrow: 0 }}
                        // style={{ backgroundColor: 'pink', height: 60 }}
                        keyExtractor={(item) => item.id}
                        data={this.state.memberListData.sort((a, b) => a.username.toUpperCase() > b.username.toUpperCase())}
                        // ref={ref => this.scrollView = ref}
                        // onContentSizeChange={() => {
                        //     this.scrollView.scrollToEnd({ animated: true })
                        // }}
                        renderItem={({ item }) => (

                            // <View></View>

                            item.type === 'Super Admin' || item.type === 'Admin' ? <TouchableOpacity
                                // onPress={() => this.props.changeState({ selectedMemberList: this.props.selectedMemberList.filter((person) => person !== item) })}
                                activeOpacity={0.6} style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', backgroundColor: COLORS.white, borderRadius: 28, paddingRight: 18, marginVertical: 10, marginRight: 8 }} >


                                <ImageBackground source={item.originalProfileImage ? { uri: item.originalProfileImage } : defaultProfile}
                                    imageStyle={{ borderRadius: 28 }}
                                    style={{ height: 50, width: 50, borderRadius: 25 }} >

                                </ImageBackground>

                                <View style={{ marginLeft: 8 }}>
                                    <Text style={[typography.Button_Lead, { color: COLORS.dark_800, fontSize: 13 }]}>{item.username}</Text>
                                    <Text style={[typography.Caption, { color: COLORS.altgreen_300 }]}>{item.type}</Text>
                                </View>

                            </TouchableOpacity> : <></>

                        )}
                    />
                </View>

                <View style={{
                    height: 56, backgroundColor: COLORS.bgFill_200, flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', marginBottom: 6, marginTop: -1
                }}>

                    <TextInput
                        style={styles.textInput}
                        placeholderTextColor="#D9E1E4"
                        onChangeText={(value) => { this.setState({ searchText: value }) }}
                        color='#154A59'
                        placeholder='Search'
                        onFocus={() => this.setState({ searchIcon: false })}
                        onBlur={() => this.setState({ searchIcon: true })}
                        underlineColorAndroid="transparent"
                        ref={input => { this.textInput = input }}
                    />

                    <TouchableOpacity onPress={() => this.setState({ postModalOpen: true })}>
                        <Icon name='FilterSlide' size={18} color={COLORS.altgreen_400} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                    </TouchableOpacity>


                </View>



                <View style={{ backgroundColor: '#E7F3E3', paddingVertical: 6 }}>
                    {this.state.memberListData.length === 1 ?
                        <Text style={styles.requestEndorsementText}>1 MEMBER</Text>
                        :
                        <Text style={styles.requestEndorsementText}>{this.state.memberListData.length} MEMBERS</Text>
                    }
                </View>





                <FlatList
                    keyboardShouldPersistTaps='handled'
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 80, paddingTop: 10 }}
                    keyExtractor={(item) => item.id}
                    data={this.searchNames(this.state.searchText)}
                    initialNumToRender={10}
                    renderItem={({ item }) => (



                        // <View style={{ marginVertical: 4, alignItems: 'center', width: '100%', justifyContent: 'space-between', backgroundColor: 'red' }}

                        // >
                        <View style={styles.item}>


                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image source={item.originalProfileImage ? { uri: item.originalProfileImage } : defaultProfile} style={[styles.image, { marginLeft: '6%' }]} />

                                <View style={styles.nameMsg}>
                                    <Text style={styles.name} numberOfLines={1}>
                                        {item.username.charAt(0).toUpperCase() + item.username.slice(1)}
                                    </Text>

                                    {item.type ?
                                        <Text style={[typography.Note2, { color: COLORS.altgreen_300 }]}>
                                            {item.type}
                                        </Text> : <></>}

                                    <Text style={[typography.Note2, { color: COLORS.altgreen_300 }]}>
                                        {item.country}
                                    </Text>

                                </View>

                                <TouchableOpacity onPress={() => this.setState({ optionsModalOpen: true, pressedUserId: item.userId, customUrl: item.customUrl, pressedItem: item }, () => console.log(item))}
                                    style={{ width: 34, height: 34, justifyContent: 'center', alignItems: 'center', marginRight: 6 }} >
                                    <Icon name='Kebab' size={14} color={COLORS.grey_350} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                                </TouchableOpacity>

                            </View>



                        </View>

                        // </View>


                    )}
                />

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        textAlign: 'left',
        fontSize: 15,
        position: "absolute",
        top: 30,
        height: screenHeight,
        backgroundColor: '#fff'
    },
    mycircle: {
        // width: '92%',
        height: 34,
        backgroundColor: COLORS.dark_700,
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignSelf: 'center',
        marginRight: 4,
        alignItems: 'center',
        borderRadius: 8,
    },
    unselectedText: {
        justifyContent: 'center', alignItems: 'center', borderColor: COLORS.altgreen_300, borderWidth: 1, paddingVertical: 4, paddingHorizontal: 10, borderRadius: 20
    },
    selectedText: {
        backgroundColor: COLORS.dark_700,
        justifyContent: 'center', alignItems: 'center', paddingVertical: 5, paddingHorizontal: 10, borderRadius: 20
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    updateButton: {
        flexDirection: 'row', width: 110, height: 28, borderRadius: 20, alignItems: 'center', justifyContent: 'center', marginVertical: 10, backgroundColor: COLORS.dark_700
    },
    requestEndorsementText: {
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 12,
        color: '#698F8A',
        textAlign: 'center'
    },
    connectsSelectedText: {
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        color: '#698F8A',
        textAlign: 'center',
        marginVertical: 10
    },
    crossIcon2: {
        marginTop: Platform.OS === 'android' ? 10 : 0
    },
    crossButtonContainer: {
        alignSelf: 'center',
        width: 42,
        height: 42,
        borderRadius: 21,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E7F3E3',
        marginBottom: 10
    },
    linearGradientView2: {
        width: '100%',
        height: 200,
        position: 'absolute',
        top: -50,
        alignSelf: 'center'
    },
    linearGradient2: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6
    },
    sendRequest: {
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 15,
        color: '#E7F3E3',
        marginLeft: 6
    },
    floatingIcon: {
        //flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: 175,
        height: 39,
        borderRadius: 28,
        backgroundColor: '#367681',
        position: 'absolute',
        //top: 416,
        bottom: 66,
        right: 12,
    },
    editIcon: {
        alignSelf: 'center'
    },
    crossIcon: {
        marginTop: Platform.OS === 'android' ? 6 : 0
        // position: 'absolute',
        // right: -10,
        // bottom: -16,
        // alignSelf: 'flex-end'
    },
    selectedName: {
        fontSize: 11,
        color: '#154A59',
        textAlign: 'center',
        marginHorizontal: 6,
        fontFamily: 'Montserrat-Medium'
    },
    searchIcon: {
        position: 'absolute',
        left: 136,
        top: 15,
        // backgroundColor: 'red',
        zIndex: 2
    },
    border: {
        borderWidth: 1,
        marginRight: '6%',
        width: '95%',
        marginTop: '-1%',
        borderColor: '#91B3A2',
        alignSelf: 'center',
        borderRadius: 1,
    },
    textInput: {
        fontSize: 17,
        fontFamily: 'Montserrat-Medium',
        padding: 10,
        backgroundColor: '#FFF',
        width: '80%',
        height: 46,
        zIndex: 1,
        textAlign: 'center',
        borderRadius: 8,
        borderColor: '#D9E1E4',
        borderWidth: 1,
        shadowColor: '#36768140',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    tabNavigator: {
        marginTop: '-8%',
    },

    item: {
        // flex: 1,
        flexDirection: 'row',
        marginVertical: 4,
        backgroundColor: '#F7F7F500',
        width: '100%',
        height: 48,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',
        // backgroundColor: 'pink'
        // borderBottomWidth: 1,
        //borderBottomRadius: 1,
        // borderBottomColor: '#E2E7E9',
        // borderRadius: 10
    },
    image: {
        height: 28,
        width: 28,
        borderRadius: 14,
        //marginLeft: '7%',
    },
    name: {
        fontWeight: '700',
        color: '#4B4F56',
        fontSize: 14,
        textAlign: 'left',
        // backgroundColor: 'pink',
        width: '70%'
    },
    message: {
        color: '#698F8A',
        fontSize: 10.5,
    },

    // --- New ---

    time: {
        color: '#AABCC3',
        fontSize: 10,
        marginRight: '10%'
    },
    nameMsg: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: '5%',
        //textAlign: 'left'
    },
    imageGroup: {
        height: 26,
        width: 26,
        borderRadius: 13,
    },
})
