import React, { Component } from 'react'
import {
    Share,
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    TouchableOpacity,
    FlatList,
    Image,
    Platform,
    Modal,
    ScrollView,
    Clipboard
} from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import LinearGradient from 'react-native-linear-gradient'
import Snackbar from 'react-native-snackbar'
import { TextInput } from 'react-native-paper'

import circleDefault from '../../../../assets/CirclesDefault.png'
import defaultStyle from '../../../Components/Shared/Typography'
import defaultCover from '../../../../assets/defaultCover.png'
import SearchBar from '../../../Components/User/SearchBar'
import { COLORS } from '../../../Components/Shared/Colors'
import typography from '../../../Components/Shared/Typography'
import defaultShape from '../../../Components/Shared/Shape'
import projectDefault from '../../../../assets/project-default.jpg'
import { REACT_APP_userServiceURL, REACT_APP_domainUrl } from '../../../../env.json'
import icoMoonConfig from '../../../../assets/Icons/selection.json'

const moment = require('moment')
const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class OngoingProjects extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            filter: 'ALL',
            page: 0,
            projects: [],
            filterModalOpen: false,
            projectDetailsModalOpen: false,
            memberListData: [],
            circleData: {},
            timeline: true,
            activityType: 'ALL',
            projectFilterType: 'ALL',
            optionsModalOpen: false,
            shareModalOpen: false,
            pressedActivitySlug: '',
            reasonForReportingModalOpen: false,
            pressedProject: {},
            reasonForReporting: '',
            description: '',
            isReported: false,
        }
    }

    componentDidMount() {

        let obj = JSON.parse(this.props.route.params.circleData)
        this.setState({ circleData: obj })

        AsyncStorage.getItem('refreshToken').then((value) => {
            if (value === null) {
                this.props.navigation.replace('Login')
            }
        });
        AsyncStorage.getItem('userId').then((value) => {
            value && this.setState({ userId: value })
            this.props.route.params && this.props.route.params.id ? this.getMemberList(0, 100, this.props.route.params.id, value) : null
            this.ongoingProjectList(value)
        })
    }

    ongoingProjectList = (userId) => {
        let filter = this.state.timeline ? "timeline" : "popular"
        let isPinned = false
        let url =
            REACT_APP_userServiceURL +
            `/participants/running-project/${this.props.route.params.id}/${userId}/?filterType=${filter}&isPinned=${isPinned}&userActivityType=${this.state.activityType}&projectFilterType=${this.state.filter}&page=0&size=1000`
        axios({
            method: "get",
            url: url,
            headers: { "Content-Type": "application/json" },
            withCredentials: true,
        })
            .then((response) => {
                if (
                    response.status === 200 &&
                    response.data &&
                    response.data.body

                ) {
                    // console.log('ongoingProjectList', response.data.body.content[0])
                    this.setState({ projects: response.data.body.content })
                }

            })
            .catch((error) => {
                console.log(error)
            })
    }

    getMemberList = (page, size, id, userId) => {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/circle/get/member/list/' + userId + '/' + id + '/' + "?page=" + page + "&size=" + size,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then((response) => {
            let res = response.data;
            if (res && res.status === '200 OK') {
                this.setState({
                    memberListData: res.body.content
                })
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    changeState = (value) => {
        this.setState(value);
    }

    navigateNotification = () => {
        this.props.navigation.navigate('Notification');
    }

    navigateProfile = () => {
        AsyncStorage.getItem('userData').then((value) => {
            let tempuserdata = JSON.parse(value);
            if (tempuserdata.type === 'INDIVIDUAL') {
                this.props.navigation.navigate('ProfileStack');
            } else {
                this.props.navigation.navigate('ProfileStack', {
                    screen: 'CompanyProfileScreen',
                });
            }
        });
    }

    unixTime2 = (UNIX_timestamp) => {
        var date = new Date(UNIX_timestamp);
        var months = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
        ];
        var year = date.getFullYear();
        var month = months[date.getMonth()];
        var day = date.getDate();
        var time = date.getTime();

        var todaysDate = new Date();
        var time2 = todaysDate.getTime();

        var difference = Math.floor((time2 - time) / 1000);

        if (difference < 60) {
            return difference + 's';
        }

        if (difference > 59 && difference < 3600) {
            return Math.floor(difference / 60) + 'm ago';
        }

        if (difference >= 3600 && difference < 86400) {
            return Math.floor(difference / 3600) + 'h ago';
        }

        if (difference >= 86400 && difference < 864000) {
            return Math.floor(difference / 72000) + 'd ago';
        }

        if (difference >= 864000) {
            return 'on ' + day + ' ' + month + ' ' + year;
        }
    }

    flatlistHeader = () => {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    marginTop: 22,
                    paddingBottom: 10,
                    paddingHorizontal: 15,
                    alignItems: 'center',
                }}>
                <Text
                    style={[
                        typography.Title_2,
                        { color: COLORS.altgreen_400, marginRight: 34, textTransform: 'capitalize' },
                    ]}>
                    Sort By {this.state.filter.toLowerCase()}
                </Text>
                <TouchableOpacity
                    onPress={() => this.setState({ filterModalOpen: true })}
                    style={[styles.addProj, { backgroundColor: COLORS.NoColor }]}>
                    <Icon name="FilterSlide" color={COLORS.altgreen_400} size={18} />
                </TouchableOpacity>
            </View>
        )
    }

    renderProjectItem = (item) => {
        return (
            <View style={styles.projItem}>
                {item.item.project.type === 'ASSIGNMENT' ||
                    item.item.project.type === 'JOB' || item.item.project.type === 'FUNDRAISE' ? (
                    <View style={{ height: 80, marginHorizontal: 15, marginTop: 10 }}>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}>
                            <Image
                                source={
                                    item.item.project && item.item.project.coverImage
                                        ? { uri: item.item.project.coverImage }
                                        : projectDefault
                                }
                                style={{ height: 60, width: 60, borderRadius: 30 }}
                            />
                            <View>
                                <Text
                                    style={[
                                        typography.Button_Lead,
                                        { color: COLORS.dark_800, marginLeft: 10 },
                                    ]}>
                                    {item.item.creatorName}
                                </Text>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                    }}>
                                    <Icon
                                        name="Location"
                                        color={COLORS.altgreen_300}
                                        size={12}
                                        style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }}
                                    />
                                    <Text
                                        style={[
                                            typography.Note2,
                                            { color: COLORS.altgreen_300, marginLeft: 5 },
                                        ]}>
                                        {item.item.project.location.country}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                ) : (
                    <Image
                        source={
                            item.item.project.coverImage
                                ? { uri: item.item.project.coverImage }
                                : projectDefault
                        }
                        style={styles.projCoverImg}
                    />
                )}
                <TouchableOpacity
                    onPress={() => this.setState({ projectDetailsModalOpen: true })}>
                    <Text
                        numberOfLines={1}
                        style={[
                            typography.Button_Lead,
                            { color: COLORS.dark_800, marginTop: 10, marginLeft: 10, maxWidth: '90%' },
                        ]}>
                        {item.item.project.title}
                    </Text>
                </TouchableOpacity>
                <View
                    style={{
                        flexDirection: 'row',
                        marginTop: 10,
                        marginHorizontal: 10,
                        paddingBottom: 5,
                    }}>
                    {item.item.project.type === 'ASSIGNMENT' ||
                        item.item.project.type === 'JOB' ? null : (
                        <View
                            style={{
                                width: 40,
                                alignItems: 'center',
                                justifyContent: 'center',
                                paddingRight: 5,
                                borderRightColor: COLORS.altgreen_400,
                                borderRightWidth: 2,
                            }}>
                            <Text
                                style={[
                                    typography.H6,
                                    { color: COLORS.dark_600, marginLeft: 5 },
                                ]}>
                                {moment.unix(item.item.project.createTime / 1000).format('DD')}
                            </Text>
                            <Text
                                style={[
                                    typography.Note2,
                                    { color: COLORS.altgreen_400, marginLeft: 5 },
                                ]}>
                                {moment
                                    .unix(item.item.project.createTime / 1000)
                                    .format('MMM')
                                    .toUpperCase()}
                            </Text>
                            <Text
                                style={[
                                    typography.Note2,
                                    { color: COLORS.altgreen_400, marginLeft: 5 },
                                ]}>
                                {moment
                                    .unix(item.item.project.createTime / 1000)
                                    .format('YYYY')}
                            </Text>
                        </View>
                    )}
                    <Text
                        numberOfLines={2}
                        style={[
                            typography.Caption,
                            { color: COLORS.dark_600, marginLeft: 5, maxWidth: '90%' },
                        ]}>
                        {item.item.project.shortBrief}
                    </Text>
                </View>

                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingRight: 15,
                        paddingVertical: Platform.OS === 'ios' ? 8 : 0,
                        borderTopColor: COLORS.grey_300,
                        borderTopWidth: 0.6,
                    }}>
                    {item.item.project.type === 'ASSIGNMENT' ||
                        item.item.project.type === 'JOB' ? (
                        <Text
                            style={[
                                typography.Note2,
                                { color: COLORS.altgreen_300, marginLeft: 15 },
                            ]}>
                            Posted {this.unixTime2(item.item.project.createTime)}
                        </Text>
                    ) : (
                        <View
                            style={{
                                flexDirection: 'row',
                                marginLeft: 15,
                                alignItems: 'center',
                            }}>
                            <Icon
                                name="Location"
                                color={COLORS.altgreen_300}
                                size={12}
                                style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }}
                            />
                            <Text
                                style={[
                                    typography.Note2,
                                    { color: COLORS.altgreen_300, marginLeft: 5 },
                                ]}>
                                {item.item.project && item.item.project.location && item.item.project.location.country}
                            </Text>
                        </View>
                    )}
                    <TouchableOpacity style={{ marginVertical: 6 }} onPress={() => this.setState({ optionsModalOpen: true, pressedActivitySlug: item.item.project.slug, pressedProject: item.item }, () => this.verifyReported())}>
                        <Icon
                            name="Meatballs"
                            color={COLORS.altgreen_300}
                            size={12}
                            style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    filterModal = () => {
        return (
            <Modal
                visible={this.state.filterModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View style={[defaultShape.Linear_Gradient_View, { bottom: 100 }]}>
                        <LinearGradient
                            colors={[
                                COLORS.dark_800 + '00',
                                COLORS.dark_800 + 'CC',
                                COLORS.dark_800,
                            ]}
                            style={defaultShape.Linear_Gradient}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ filterModalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color={COLORS.dark_600}
                            style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                        />
                    </TouchableOpacity>

                    <Text
                        style={[
                            typography.Caption,
                            {
                                fontSize: 12,
                                color: COLORS.dark_500,
                                textAlign: 'center',
                                alignSelf: 'center',
                                position: 'absolute',
                                bottom: 110,
                                zIndex: 2,
                            },
                        ]}>
                        Sort By
                    </Text>

                    <View
                        style={[
                            defaultShape.Modal_Categories_Container,
                            {
                                paddingTop: 10,
                                height: 140,
                                flexDirection: 'row',
                            },
                        ]}>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false} contentContainerStyle={{ paddingRight: 10, paddingLeft: 4 }}>
                            <TouchableOpacity
                                style={
                                    this.state.filter === 'ALL'
                                        ? styles.selectedText
                                        : styles.unselectedText
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState(
                                        {
                                            filterModalOpen: false,
                                            filter: 'ALL',
                                            projects: [],
                                            page: 0,
                                        },
                                        () => this.ongoingProjectList(this.state.userId),
                                    );
                                }}>
                                <Text
                                    style={[
                                        typography.Caption,
                                        {
                                            fontSize: 11,
                                            color:
                                                this.state.filter === 'ALL'
                                                    ? COLORS.altgreen_200
                                                    : COLORS.dark_500,
                                            textAlign: 'center',
                                            alignSelf: 'center',
                                        },
                                    ]}>
                                    All
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    this.state.filter === 'JOB'
                                        ? styles.selectedText
                                        : styles.unselectedText
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState(
                                        {
                                            filterModalOpen: false,
                                            filter: 'JOB',
                                            projects: [],
                                            page: 0,
                                        },
                                        () => this.ongoingProjectList(this.state.userId),
                                    );
                                }}>
                                <Text
                                    style={[
                                        typography.Caption,
                                        {
                                            fontSize: 11,
                                            color:
                                                this.state.filter === 'JOB'
                                                    ? COLORS.altgreen_200
                                                    : COLORS.dark_500,
                                            textAlign: 'center',
                                            alignSelf: 'center',
                                        },
                                    ]}>
                                    Jobs
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    this.state.filter === 'ASSIGNMENT'
                                        ? styles.selectedText
                                        : styles.unselectedText
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState(
                                        {
                                            filterModalOpen: false,
                                            filter: 'ASSIGNMENT',
                                            projects: [],
                                            page: 0,
                                        },
                                        () => this.ongoingProjectList(this.state.userId),
                                    );
                                }}>
                                <Text
                                    style={[
                                        typography.Caption,
                                        {
                                            fontSize: 11,
                                            color:
                                                this.state.filter === 'ASSIGNMENT'
                                                    ? COLORS.altgreen_200
                                                    : COLORS.dark_500,
                                            textAlign: 'center',
                                            alignSelf: 'center',
                                        },
                                    ]}>
                                    Assignments
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    this.state.filter === 'EVENT'
                                        ? styles.selectedText
                                        : styles.unselectedText
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState(
                                        {
                                            filterModalOpen: false,
                                            filter: 'EVENT',
                                            projects: [],
                                            page: 0,
                                        },
                                        () => this.ongoingProjectList(this.state.userId),
                                    );
                                }}>
                                <Text
                                    style={[
                                        typography.Caption,
                                        {
                                            fontSize: 11,
                                            color:
                                                this.state.filter === 'EVENT'
                                                    ? COLORS.altgreen_200
                                                    : COLORS.dark_500,
                                            textAlign: 'center',
                                            alignSelf: 'center',
                                        },
                                    ]}>
                                    Events
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    this.state.filter === 'TRAINING'
                                        ? styles.selectedText
                                        : styles.unselectedText
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState(
                                        {
                                            filterModalOpen: false,
                                            filter: 'TRAINING',
                                            projects: [],
                                            page: 0,
                                        },
                                        () => this.ongoingProjectList(this.state.userId),
                                    );
                                }}>
                                <Text
                                    style={[
                                        typography.Caption,
                                        {
                                            fontSize: 11,
                                            color:
                                                this.state.filter === 'TRAINING'
                                                    ? COLORS.altgreen_200
                                                    : COLORS.dark_500,
                                            textAlign: 'center',
                                            alignSelf: 'center',
                                        },
                                    ]}>
                                    Training
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    this.state.filter === 'EXPEDITION'
                                        ? styles.selectedText
                                        : styles.unselectedText
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState(
                                        {
                                            filterModalOpen: false,
                                            filter: 'EXPEDITION',
                                            projects: [],
                                            page: 0,
                                        },
                                        () => this.ongoingProjectList(this.state.userId),
                                    );
                                }}>
                                <Text
                                    style={[
                                        typography.Caption,
                                        {
                                            fontSize: 11,
                                            color:
                                                this.state.filter === 'EXPEDITION'
                                                    ? COLORS.altgreen_200
                                                    : COLORS.dark_500,
                                            textAlign: 'center',
                                            alignSelf: 'center',
                                        },
                                    ]}>
                                    Expedition
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    this.state.filter === 'FUNDRAISE'
                                        ? styles.selectedText
                                        : styles.unselectedText
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState(
                                        {
                                            filterModalOpen: false,
                                            filter: 'FUNDRAISE',
                                            projects: [],
                                            page: 0,
                                        },
                                        () => this.ongoingProjectList(this.state.userId),
                                    );
                                }}>
                                <Text
                                    style={[
                                        typography.Caption,
                                        {
                                            fontSize: 11,
                                            color:
                                                this.state.filter === 'FUNDRAISE'
                                                    ? COLORS.altgreen_200
                                                    : COLORS.dark_500,
                                            textAlign: 'center',
                                            alignSelf: 'center',
                                        },
                                    ]}>
                                    Fund Raise
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    this.state.filter === 'STORYBOOK'
                                        ? styles.selectedText
                                        : styles.unselectedText
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState(
                                        {
                                            filterModalOpen: false,
                                            filter: 'STORYBOOK',
                                            projects: [],
                                            page: 0,
                                        },
                                        () => this.ongoingProjectList(this.state.userId),
                                    );
                                }}>
                                <Text
                                    style={[
                                        typography.Caption,
                                        {
                                            fontSize: 11,
                                            color:
                                                this.state.filter === 'STORYBOOK'
                                                    ? COLORS.altgreen_200
                                                    : COLORS.dark_500,
                                            textAlign: 'center',
                                            alignSelf: 'center',
                                        },
                                    ]}>
                                    Story Book
                                </Text>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                </View>
            </Modal>
        )
    }

    projectDetailsModal = () => {
        return (
            <Modal
                visible={true}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View style={[defaultShape.Linear_Gradient_View, { bottom: 100 }]}>
                        <LinearGradient
                            colors={[
                                COLORS.dark_800 + '00',
                                COLORS.dark_800 + 'CC',
                                COLORS.dark_800,
                            ]}
                            style={defaultShape.Linear_Gradient}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ projectDetailsModalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color={COLORS.dark_600}
                            style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                        />
                    </TouchableOpacity>

                    <Text
                        style={[
                            typography.Caption,
                            {
                                fontSize: 12,
                                color: COLORS.dark_500,
                                textAlign: 'center',
                                alignSelf: 'center',
                                position: 'absolute',
                                bottom: 110,
                                zIndex: 2,
                            },
                        ]}>
                        Sort By
                    </Text>

                    <View
                        style={[
                            defaultShape.Modal_Categories_Container,
                            {
                                paddingTop: 10,
                                height: 140,
                                flexDirection: 'row',
                            },
                        ]}></View>
                </View>
            </Modal>
        )
    }

    stickyHeader = () => {
        return (
            <View style={{ backgroundColor: COLORS.white, position: 'absolute', top: 0, zIndex: 2 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: COLORS.white }}>

                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center', padding: 10 }}>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TouchableOpacity activeOpacity={0.5}
                                onPress={() => this.props.navigation.navigate('CircleProfile')}
                                style={{ backgroundColor: COLORS.white, alignItems: 'center', justifyContent: 'center', height: 42, width: 42, borderRadius: 21, marginTop: 6 }}>
                                <Icon name="Arrow-Left" size={16} color="#00394D" style={{ marginTop: 5 }} />
                            </TouchableOpacity>

                            {/* {(this.props.user.body && this.props.user.body.originalProfileImage) ?
                            <Image source={{ uri: this.props.user.body.originalProfileImage }} style={{ width: 30, height: 30, borderRadius: 15 }} />
                            : null} */}
                            <Image source={this.state.circleData && this.state.circleData.profileImage ? { uri: this.state.circleData.profileImage } : circleDefault} style={{ width: 30, height: 30, borderRadius: 15 }} />



                            <View style={{ alignSelf: 'center', marginLeft: 8 }}>
                                {/* {this.props.user.body !== undefined ? */}
                                <Text style={[typography.Title_1, { color: COLORS.dark_800, fontSize: 14, }]}>
                                    {/* {this.props.user.body.userName} */}
                                    {this.state.circleData && this.state.circleData.title}
                                </Text>


                                <Text style={[typography.Subtitle_1, { color: COLORS.altgreen_400, fontSize: 11, marginTop: -6 }]}>

                                    {this.state.circleData && this.state.circleData.location ? this.state.circleData.location.city + ', ' : null}
                                    {this.state.circleData && this.state.circleData.location ? this.state.circleData.location.country : null}
                                </Text>
                            </View>

                        </View>

                    </View>

                </View>

                {this.state.memberListData && this.state.memberListData.length >= 6 ?
                    <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-evenly', width: 300 }}>

                        <TouchableOpacity activeOpacity={0.8} style={{ paddingTop: 8, width: 100 }} onPress={() => this.props.navigation.navigate('CircleActivities', { id: this.props.route.params.id, circleData: JSON.stringify(this.state.circleData) })}>
                            <Text style={[typography.Button_Lead, { color: COLORS.altgreen_400, marginBottom: 6, textAlign: 'center' }]}>ACTIVITIES</Text>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={0.8} style={{ width: 100 }}>
                            <Text style={[typography.Button_2, { color: COLORS.dark_800, textAlign: 'center', marginBottom: 6, paddingTop: 8 }]}>PROJECTS</Text>
                            <View style={{ width: 100, height: 5, backgroundColor: COLORS.dark_800, borderTopLeftRadius: 4, borderTopRightRadius: 4 }}></View>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={0.8} style={{ width: 100 }} onPress={() => this.props.navigation.navigate('CircleMembers', { id: this.props.route.params.id, circleData: JSON.stringify(this.state.circleData) })}>
                            <Text style={[typography.Button_2, { color: COLORS.dark_800, textAlign: 'center', marginBottom: 6, paddingTop: 8 }]}>MEMBERS</Text>
                        </TouchableOpacity>

                    </View>
                    :
                    <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-evenly', width: 240 }}>

                        <TouchableOpacity activeOpacity={0.8} style={{ width: 120 }}>
                            <Text style={[typography.Button_2, { color: COLORS.dark_800, textAlign: 'center', marginBottom: 6, paddingTop: 8 }]}>PROJECTS</Text>
                            <View style={{ width: 100, height: 5, backgroundColor: COLORS.dark_800, borderTopLeftRadius: 4, borderTopRightRadius: 4 }}></View>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={0.8} style={{ width: 120 }} onPress={() => this.props.navigation.navigate('CircleMembers', { id: this.props.route.params.id, circleData: JSON.stringify(this.state.circleData) })}>
                            <Text style={[typography.Button_2, { color: COLORS.dark_800, textAlign: 'center', marginBottom: 6, paddingTop: 8 }]}>MEMBERS</Text>
                        </TouchableOpacity>

                    </View>}

            </View>
        )
    }

    onShare = async () => {
        try {
            const result = await Share.share({
                message: REACT_APP_domainUrl + '/project/feeds/' + this.state.pressedActivitySlug,
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // console.log('shared with activity type of result.activityType')
                } else {
                    // console.log('shared')
                }
            } else if (result.action === Share.dismissedAction) {
                // console.log('dismissed')
            }
        } catch (error) {
            // console.log(error.message)
        }
    }

    shareModal = () => {
        return (
            <Modal
                visible={this.state.shareModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient
                            colors={[
                                COLORS.dark_800 + '00',
                                COLORS.dark_800 + 'CC',
                                COLORS.dark_800,
                            ]}
                            style={defaultShape.Linear_Gradient}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ shareModalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color={COLORS.dark_600}
                            style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                        />
                    </TouchableOpacity>

                    <View style={defaultShape.Modal_Categories_Container}>
                        <View
                            style={[
                                defaultShape.ActList_Cell_Gylph_Alt,
                                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
                            ]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                                Share
                            </Text>
                        </View>

                        {/* <TouchableOpacity
                            style={
                                Platform.OS === 'ios'
                                    ? [
                                        defaultShape.ActList_Cell_Gylph_Alt,
                                        { paddingTop: 25, paddingBottom: 15 },
                                    ]
                                    : defaultShape.ActList_Cell_Gylph_Alt
                            }
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({ shareModalOpen: false }, () => this.props.navigation.navigate('SharePoll', { pressedActivityId: this.state.pressedProject.project.id,  }));
                            }}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                                Repost on WeNaturalists
                            </Text>
                            <Icon
                                name="Forward"
                                size={17}
                                color={COLORS.altgreen_300}
                                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                            />
                        </TouchableOpacity> */}

                        <TouchableOpacity
                            onPress={() => {
                                Clipboard.setString(
                                    REACT_APP_domainUrl + '/project/feeds/' + this.state.pressedActivitySlug
                                );
                                Snackbar.show({
                                    backgroundColor: '#97A600',
                                    text: 'Link Copied',
                                    textColor: '#00394D',
                                    duration: Snackbar.LENGTH_LONG,
                                });
                            }}
                            style={
                                Platform.OS === 'ios'
                                    ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                    : [defaultShape.ActList_Cell_Gylph_Alt]
                            }
                            activeOpacity={0.6}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                                Copy link
                            </Text>
                            <Icon
                                name="TxEdi_AddLink"
                                size={17}
                                color={COLORS.altgreen_300}
                                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ shareModalOpen: false }, () => this.onShare());
                            }}
                            style={
                                Platform.OS === 'ios'
                                    ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                    : [
                                        defaultShape.ActList_Cell_Gylph_Alt,
                                        { borderBottomWidth: 0 },
                                    ]
                            }
                            activeOpacity={0.6}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                                Share via others
                            </Text>

                            <View
                                style={[
                                    {
                                        flexDirection: 'row',
                                        justifyContent: 'space-evenly',
                                        width: 100,
                                        marginRight: -6,
                                    },
                                ]}>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon
                                        name="Social_FB"
                                        size={14}
                                        color={COLORS.dark_600}
                                        style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                                    />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon
                                        name="Social_Twitter"
                                        size={14}
                                        color={COLORS.dark_600}
                                        style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                                    />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon
                                        name="Social_LinkedIn"
                                        size={14}
                                        color={COLORS.dark_600}
                                        style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                                    />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon
                                        name="Meatballs"
                                        size={14}
                                        color={COLORS.dark_600}
                                        style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                                    />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }

    reasonForReportingModal = () => {
        return (

            <Modal visible={this.state.reasonForReportingModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={[defaultShape.Linear_Gradient_View, { height: 700, bottom: 0 }]}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={[defaultShape.CloseBtn, { marginBottom: 0 }]}
                        onPress={() => this.setState({ reasonForReportingModalOpen: false, reasonForReporting: '', description: '' })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }} />
                    </TouchableOpacity>


                    <View style={{
                        borderRadius: 20,
                        backgroundColor: COLORS.altgreen_100,
                        alignItems: 'center',
                        paddingTop: 15,
                        paddingBottom: 10,
                        // paddingLeft: 20,
                        width: '90%',
                        alignSelf: 'center',
                        marginBottom: 30,
                        marginTop: 15
                    }}>

                        <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Reason for reporting</Text>
                        </View>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '94%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Inappropriate, abusive or offensive content</Text>
                            </View>

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'CONTENT_PROMOTING_VIOLENCE_OR_TERRORISM' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'CONTENT_PROMOTING_VIOLENCE_OR_TERRORISM' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Content promoting violence or terrorism</Text>
                            </View>

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15, borderBottomWidth: 0 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'FAKE_SPAM_OR_SCAM' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Fake, spam or scam</Text>
                            </View>


                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'ACCOUNT_MAY_BE_HACKED' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Account may be hacked</Text>
                            </View>

                        </TouchableOpacity>
                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '90.5%' }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'DEFAMATION_TRADEMARK_OR_COPYRIGHT_VIOLATION' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'DEFAMATION_TRADEMARK_OR_COPYRIGHT_VIOLATION' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text numberOfLines={2} style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Defamation, trademark or copyright violation</Text>
                            </View>

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'HARASSMENT_OR_THREAT' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'HARASSMENT_OR_THREAT' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Harassment or threat</Text>
                            </View>

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%', }]} activeOpacity={0.6} onPress={() => { this.setState({ reasonForReporting: 'OTHERS' }) }} >

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: 17, height: 17, borderRadius: 8.5, borderColor: COLORS.altgreen_300, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.altgreen_200 }}>
                                    {this.state.reasonForReporting === 'OTHERS' ? <Icon name="Bullet_Fill" size={17} color={COLORS.dark_800} style={{ marginLeft: -2, marginTop: Platform.OS === 'android' ? 8 : 0 }} /> : <></>}
                                </View>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Others</Text>
                            </View>

                        </TouchableOpacity>

                        {this.state.reasonForReporting === 'OTHERS' &&
                            <View style={{
                                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 6, paddingRight: 12
                            }}>

                                <TextInput
                                    theme={{ colors: { text: COLORS.dark_700, primary: COLORS.altgreen_300, placeholder: COLORS.altgreen_300 } }}
                                    label="Write the details"
                                    multiline
                                    selectionColor='#C8DB6E'
                                    style={[defaultStyle.Subtitle_1, { width: '90%', height: 56, backgroundColor: COLORS.altgreen_100, color: COLORS.dark_700 }]}
                                    onChangeText={(value) => this.setState({ description: value })}
                                    value={this.state.description}
                                />

                            </View>}

                        <TouchableOpacity activeOpacity={0.5}
                            onPress={() => {
                                this.state.reasonForReporting === '' ?
                                    Snackbar.show({
                                        backgroundColor: '#B22222',
                                        text: "Please select an option",
                                        duration: Snackbar.LENGTH_LONG,
                                    })
                                    :
                                    this.state.reasonForReporting === 'OTHERS' && this.state.description === '' ?
                                        Snackbar.show({
                                            backgroundColor: '#B22222',
                                            text: "Please enter the detail",
                                            duration: Snackbar.LENGTH_LONG,
                                        })
                                        :
                                        (this.handleReportAbuseSubmit(), this.setState({ reasonForReportingModalOpen: false }))
                            }}
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                marginRight: 15,
                                height: 27,
                                marginVertical: 10,
                                borderRadius: 16,
                                textAlign: 'center',
                                borderWidth: 1,
                                borderColor: '#698F8A'
                            }}>
                            <Text style={{
                                color: '#698F8A',
                                fontSize: 14,
                                paddingHorizontal: 14,
                                paddingVertical: 20,
                                fontFamily: 'Montserrat-Medium',
                                fontWeight: 'bold'
                            }}>Submit</Text>
                        </TouchableOpacity>

                    </View>
                </View>

            </Modal>

        )
    }

    optionsModal = () => {
        return (
            <Modal
                visible={this.state.optionsModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient
                            colors={[
                                COLORS.dark_800 + '00',
                                COLORS.dark_800 + 'CC',
                                COLORS.dark_800,
                            ]}
                            style={defaultShape.Linear_Gradient}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ optionsModalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color={COLORS.dark_600}
                            style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                        />
                    </TouchableOpacity>

                    <View style={defaultShape.Modal_Categories_Container}>
                        <View
                            style={[
                                defaultShape.ActList_Cell_Gylph_Alt,
                                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
                            ]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                                Post
                            </Text>
                        </View>

                        <TouchableOpacity
                            style={
                                Platform.OS === 'ios'
                                    ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                    : defaultShape.ActList_Cell_Gylph_Alt
                            }
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({ optionsModalOpen: false, shareModalOpen: true });
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <Icon
                                    name="Share"
                                    size={17}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                                <Text
                                    style={[
                                        typography.Button_Lead,
                                        { color: COLORS.dark_600, marginLeft: 8 },
                                    ]}>
                                    Share
                                </Text>
                            </View>
                            <Icon
                                name="Arrow_Right"
                                size={17}
                                color={COLORS.altgreen_300}
                                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={
                                Platform.OS === 'ios'
                                    ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                    : [
                                        defaultShape.ActList_Cell_Gylph_Alt,
                                    ]
                            }
                            activeOpacity={0.6}
                            onPress={() => {
                                this.handleHideModal()
                            }}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                                Hide
                            </Text>
                            <Icon
                                name="Hide"
                                size={17}
                                color={COLORS.altgreen_300}
                                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]} activeOpacity={0.6} onPress={() => {
                            this.setState({ reasonForReportingModalOpen: !this.state.isReported ? true : false, optionsModalOpen: false })
                            this.state.isReported ?
                                setTimeout(() => {
                                    Snackbar.show({
                                        backgroundColor: '#B22222',
                                        text: 'Your report request was already taken',
                                        duration: Snackbar.LENGTH_LONG,
                                    })
                                }, 500) : null
                        }}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Report</Text>
                            <Icon name='ReportComment_OL' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }

    verifyReported = () => {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/reportabuse/verifyAlreadyReported?reporterId='
                + this.state.userId + '&entityId=' + this.state.pressedProject.project.id,
            withCredentials: true
        }).then(response => {
            if (response && response.status === 200 && response.data && response.data.body) {
                // console.log(response.data.body.reported)
                this.setState({ isReported: response.data.body.reported })

            } else {
                // console.log(response)
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    handleReportAbuseSubmit = () => {

        let data = {
            reporterId: this.state.userId,
            entityId: this.state.pressedProject.project.id,
            // entityType: this.state.entityType,
            entityType: this.state.pressedProject.projectType,
            reason: this.state.reasonForReporting,
            description: this.state.description
        }

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
            data: data,
            withCredentials: true
        }).then(response => {
            if (response && response.status === 201) {
                Snackbar.show({
                    backgroundColor: COLORS.primarydark,
                    text: "Your request has been taken and appropriate action will be taken as per our report abuse policy",
                    textColor: COLORS.altgreen_100,
                    duration: Snackbar.LENGTH_LONG,
                })
                // console.log(response)
            } else {
                // console.log(response)

            }
        }).catch((err) => {
            if (err && err.response && err.response.status === 409) {
                // console.log(err.response)
                Snackbar.show({
                    backgroundColor: COLORS.primarydark,
                    text: "Your report request was already taken",
                    textColor: COLORS.altgreen_100,
                    duration: Snackbar.LENGTH_LONG,
                })

            } else {
                console.log(err)
                Snackbar.show({
                    backgroundColor: '#B22222',
                    text: "Please check your network or try again later",
                    duration: Snackbar.LENGTH_LONG,
                })
            }
        })
        setTimeout(() => {
            this.setState({ reasonForReporting: '', description: '' })
        }, 1000)
    }

    handleHideModal = () => {
        let data = {
          userId: this.state.userId,
          activityId: this.state.pressedProject.project.id,
          entityType: this.state.pressedProject.projectType,
        }
    
        axios({
          method: "post",
          url: REACT_APP_userServiceURL + "/backend/hidden/hide",
          data: data,
          withCredentials: true,
        })
          .then((response) => {
    
            if (response && response.status === 201) {
              console.log('response.status', response.status)
            }
          })
          .catch((err) => {
    
            if (err && err.response && err.response.status === 409) {
              console.log('response.status', err.response.status)
            }
          })
          this.ongoingProjectList(this.state.userId)
          this.setState({ optionsModalOpen: false })
      }

    render() {
        // console.log('pressedProject', this.state.pressedProject)
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {this.filterModal()}
                {this.stickyHeader()}
                {this.optionsModal()}
                {this.shareModal()}
                {this.reasonForReportingModal()}

                <View style={{ alignSelf: 'center', flexDirection: 'row', backgroundColor: COLORS.altgreen_t50, width: 300, marginTop: 126, marginBottom: 0, justifyContent: 'center', borderRadius: 4 }}>

                    <TouchableOpacity
                        style={[defaultShape.InTab_Btn, { backgroundColor: '#fff', width: 150, borderRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: COLORS.dark_800 }]}>Ongoing</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('CompletedProjects', {
                        id: this.props.route.params.id,
                        circleData: JSON.stringify(this.state.circleData)
                    })}
                        style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 150, borderTopRightRadius: 4, borderBottomRightRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Completed</Text>
                    </TouchableOpacity>

                </View>

                <FlatList
                    data={this.state.projects}
                    keyExtractor={(item) => item.projectId}
                    ListHeaderComponent={this.flatlistHeader}
                    renderItem={(item) => this.renderProjectItem(item)}
                />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        backgroundColor: COLORS.primarydark,
        height: 50,
        alignItems: 'flex-end',
        marginTop: 200
    },
    headerItemSelected: {
        borderBottomColor: COLORS.green_500,
        borderBottomWidth: 6,
        paddingBottom: 8,
        marginLeft: 12,
    },
    headerItemNotSelected: {
        paddingBottom: 2,
        paddingBottom: 14,
        marginLeft: 12,
    },
    addProj: {
        backgroundColor: COLORS.green_500,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: 50,
        position: 'absolute',
        right: 0,
    },
    projItem: {
        marginHorizontal: 15,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        marginVertical: 7.5,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    projCoverImg: {
        width: '100%',
        height: 100,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
    },
    selectedTab: {
        borderBottomColor: COLORS.dark_800,
        borderBottomWidth: 4,
        paddingBottom: 4,
        marginLeft: 10,
    },
    unselectedTab: {
        marginLeft: 10,
    },
    selectedTabText: {
        color: COLORS.dark_800,
    },
    unselectedTabText: {
        color: COLORS.altgreen_300,
    },
    unselectedText: {
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: COLORS.altgreen_300,
        borderWidth: 1,
        paddingVertical: 4,
        paddingHorizontal: 10,
        borderRadius: 20,
        marginLeft: 10,
        minWidth: 50
    },
    selectedText: {
        backgroundColor: COLORS.dark_700,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 20,
        marginLeft: 10,
        minWidth: 50
    }
})
