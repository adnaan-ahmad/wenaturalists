import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  ScrollView,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {cloneDeep} from 'lodash';

import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import typography from '../../../Components/Shared/Typography';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import TopContriButerItem from '../../../Components/User/HashTag/TopContriButerItem';
import defaultStyle from '../../../Components/Shared/Typography';
import RecentActivityItem from '../../../Components/User/Common/RecentActivityItem';
import BlogItem from '../../../Components/User/Common/BlogItem';
import PhotoItem from '../../../Components/User/Common/PhotoItem';
import VideoItem from '../../../Components/User/Common/VideoItem';
import ForumsItem from '../../../Components/User/Common/ForumsItem';
import PollItem from '../../../Components/User/Common/PollItem';
import FeedsItem from '../../../Components/User/Common/FeedsItem';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
export default class HashTagDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slug: props.route.params.slug,
      userId: '',
      relatedHashtag: [],
      topContributers: [],
      recentActivities: [],
      relatedBlogs: [],
      relatedPhotos: [],
      realtedVideos: [],
      relatedForums: [],
      relatedPoll: [],
      relatedFeeds: [],
      blogPage: 0,
      photoPage: 0,
      videoPage: 0,
      forumPage: 0,
      pollPage: 0,
      feedsPage: 0,
    };
  }

  navigation = (value, params) => {
    this.props.navigation.navigate(value, params);
  };

  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      value &&
        this.setState(
          {
            userId: value,
          },
          () => {
            this.fetchRelatedHashtag();
            this.fetchTopContributer();
            this.fetchRecentActivity();
            this.fetchRelatedBlogs();
            this.fetchRelatedPhotos();
            this.fetchRelatedVideos();
            this.fetchRelatedForums();
            this.fetchRelatedPoll();
            this.fetchRelatedFeeds();
          },
        );
    });
  }

  /** ----- start fetch related hashtags ------ **/
  fetchRelatedHashtag = () => {
    axios({
      method: 'GET',
      url: `${REACT_APP_userServiceURL}/tags/relatedhashtags/?userId=${this.state.userId}&hashTag=${this.state.slug}&page=0&size=11`,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            relatedHashtag: response.data.body,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /** ----- end fetch related hashtags ------ **/

  /** ---- start top contributer ----- **/
  fetchTopContributer = () => {
    axios({
      method: 'GET',
      url: `${REACT_APP_userServiceURL}/tags/topcontributorhashtag/?userId=${this.state.userId}&hashTag=${this.state.slug}&page=0&size=5`,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            topContributers: response.data.body.content,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /** ---- end top contributer -------**/

  /** ------- start fetch recent activity --------- **/
  fetchRecentActivity = () => {
    axios({
      method: 'GET',
      url: `${REACT_APP_userServiceURL}/tags/tagrecentactivity/?userId=${this.state.userId}&hashTag=${this.state.slug}&page=0&size=10`,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            recentActivities: response.data.body.content,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /** ------- end fetch recent activity --------- **/

  // handle load more
  handleBlogLoadMore = () => {
    this.setState({blogPage: this.state.blogPage + 1}, () =>
      this.fetchRelatedBlogs(),
    );
  };

  /** ------ start fetch related blogs -----------**/
  fetchRelatedBlogs = () => {
    axios({
      method: 'GET',
      url: `${REACT_APP_userServiceURL}/tags/getNewsFeed/hashTag/viewAll/?userId=${
        this.state.userId
      }&hashTag=${
        this.state.slug
      }&category=POST&newsFeedType=ARTICLE&page=${0}&size=5`,
      withCredentials: true,
    })
      .then((response) => {
        console.log('Blog data:', response.data.body);
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            relatedBlogs: this.state.relatedBlogs.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /** ------ end fetch related blogs -----------**/

  //   loadmore photos
  loadMorePhotos = () => {
    this.setState({photoPage: this.state.photoPage + 1}, () => {
      this.fetchRelatedPhotos();
    });
  };

  /** start fetch related photos **/
  fetchRelatedPhotos = () => {
    axios({
      method: 'GET',
      url: `${REACT_APP_userServiceURL}/tags/getNewsFeed/hashTag/viewAll/?userId=${
        this.state.userId
      }&hashTag=${
        this.state.slug
      }&category=POST&newsFeedType=IMAGE&page=${0}&size=5`,
      withCredentials: true,
    })
      .then((response) => {
        console.log('data object', response.data);
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            relatedPhotos: this.state.relatedPhotos.concat(
              response.data.body.content,
            ),
            photosCount:
              response.data.body.page && response.data.body.page.totalElements,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /** end fetch related photos **/

  //   load more video items
  loadMoreVideo = () => {
    this.setState({videoPage: this.state.videoPage + 1}, () => {
      this.fetchRelatedVideos();
    });
  };

  /** start fetch related videos **/
  fetchRelatedVideos = () => {
    axios({
      method: 'GET',
      url: `${REACT_APP_userServiceURL}/tags/getNewsFeed/hashTag/viewAll/?userId=${
        this.state.userId
      }&hashTag=${
        this.state.slug
      }&category=POST&newsFeedType=VIDEO&page=${0}&size=4`,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            realtedVideos: this.state.realtedVideos.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /** end fetch related videos **/

  /** start loadmore forums**/
  loadMoreForums = () => {
    this.setState({forumPage: this.state.forumPage + 1}, () => {
      this.fetchRelatedForums();
    });
  };
  /** end loadmore forums**/

  /** start fetch realted forums **/
  fetchRelatedForums = () => {
    axios({
      method: 'GET',
      url: `${REACT_APP_userServiceURL}/tags/getNewsFeed/hashTag/?userId=${
        this.state.userId
      }&hashTag=${this.state.slug}&category=FORUM&page=${0}&size=5`,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            relatedForums: this.state.relatedForums.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /** end fetch realted forums **/

  // loadmore related polls
  loadMorePolls = () => {
    this.setState({pollPage: this.state.pollPage + 1}, () => {
      this.fetchRelatedPoll();
    });
  };

  /** start fetch related poll **/
  fetchRelatedPoll = () => {
    axios({
      method: 'GET',
      url: `${REACT_APP_userServiceURL}/tags/getNewsFeed/hashTag/?userId=${
        this.state.userId
      }&hashTag=${this.state.slug}&category=POLL&page=${0}&size=5`,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            relatedPoll: this.state.relatedPoll.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /** end fetch related poll **/

  /** start fetch related feeds **/
  loadMoreFeeds = () => {
    this.setState(
      {
        feedsPage: this.state.feedsPage + 1,
      },
      () => {
        this.fetchRelatedFeeds();
      },
    );
  };

  fetchRelatedFeeds = () => {
    axios({
      method: 'GET',
      url: `${REACT_APP_userServiceURL}/tags/getNewsFeed/hashTag/viewAll/?userId=${
        this.state.userId
      }&hashTag=${
        this.state.slug
      }&category=POST&newsFeedType=FEED&page=${0}&size=5`,
      withCredentials: true,
    })
      .then((response) => {
        console.log('Help loader');
        console.log(response.data.body.content);
        if (
          response &&
          response.data &&
          response.data.body &&
          response.data.statusCode === 200
        ) {
          this.setState({
            relatedFeeds: this.state.relatedFeeds.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /** end fetch related feeds **/

  handleFollowUnfollow = (isFollowed, userId) => {
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    console.log('object url new', url);
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          let relatedBlogs = cloneDeep(this.state.relatedBlogs);
          let relatedPhotos = cloneDeep(this.state.relatedPhotos);
          let realtedVideos = cloneDeep(this.state.realtedVideos);
          let relatedForums = cloneDeep(this.state.relatedForums);
          let relatedPoll = cloneDeep(this.state.relatedPoll);
          let relatedFeeds = cloneDeep(this.state.relatedFeeds);

          for (let i of relatedBlogs) {
            if (i.userId === userId) {
              // console.log(i.followed, !isFollowed);
              i.followed = !isFollowed;
            }
          }
          for (let i of relatedPhotos) {
            if (i.userId === userId) {
              // console.log(i.followed, !isFollowed);
              i.followed = !isFollowed;
            }
          }
          for (let i of realtedVideos) {
            if (i.userId === userId) {
              // console.log(i.followed, !isFollowed);
              i.followed = !isFollowed;
            }
          }
          for (let i of relatedForums) {
            if (i.userId === userId) {
              // console.log(i.followed, !isFollowed);
              i.followed = !isFollowed;
            }
          }
          for (let i of relatedPoll) {
            if (i.userId === userId) {
              // console.log(i.followed, !isFollowed);
              i.followed = !isFollowed;
            }
          }

          for (let i of relatedFeeds) {
            if (i.userId === userId) {
              // console.log(i.followed, !isFollowed);
              i.followed = !isFollowed;
            }
          }
          this.setState({
            relatedBlogs: relatedBlogs,
            relatedPhotos: relatedPhotos,
            realtedVideos: realtedVideos,
            relatedForums: relatedForums,
            relatedPoll: relatedPoll,
            relatedFeeds: relatedFeeds,
          });
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    const {
      slug,
      relatedHashtag,
      topContributers,
      recentActivities,
      relatedBlogs,
      relatedPhotos,
      realtedVideos,
      relatedForums,
      relatedPoll,
      relatedFeeds,
    } = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        <ScrollView>
          {/***** Header starts *****/}
          <View style={styles.headerView}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={defaultShape.Nav_Gylph_Btn}>
              <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
            </TouchableOpacity>
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_600,
                  marginLeft: 10,
                  marginTop: Platform.OS === 'android' ? -8 : 0,
                },
              ]}>
              {'#' + slug}
            </Text>
          </View>
          {/***** Header ends *****/}
          {/* related hashtags */}
          <View style={{backgroundColor: COLORS.altgreen_100}}>
            <FlatList
              keyboardShouldPersistTaps="handled"
              keyExtractor={(item) => item}
              data={relatedHashtag}
              contentContainerStyle={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                paddingTop: 10,
                paddingBottom: 10,
                justifyContent: 'flex-start',
                paddingHorizontal: 10,
              }}
              renderItem={(item) => (
                <View style={{flexDirection: 'row', paddingLeft: 10}}>
                  <Text style={typography.Caption}>#</Text>
                  <Text style={[typography.Caption, {color: COLORS.dark_800}]}>
                    {item.item}
                  </Text>
                </View>
              )}
            />
          </View>
          {/* related hashtags */}

          {/****** Top Contributers Starts ******/}
          <View
            style={{
              backgroundColor: COLORS.primarydark,
              paddingVertical: 20,
              paddingHorizontal: 15,
            }}>
            <Text
              style={[
                typography.OVERLINE,
                {marginBottom: 10, color: COLORS.white},
              ]}>
              TOP CONTRIBUTERS
            </Text>

            <FlatList
              horizontal
              keyExtractor={(item) => item.id}
              showsHorizontalScrollIndicator={false}
              data={topContributers}
              keyExtractor={(item) => item}
              renderItem={(item) => <TopContriButerItem item={item} />}
            />
          </View>
          {/****** Top Contributers Ends ******/}
          {/* --------- start recent activity ------- */}
          {recentActivities && recentActivities.length ? (
            <View
              style={{
                backgroundColor: '#D9E1E4',
                paddingVertical: 8,
                paddingLeft: 16,
              }}>
              <Text
                style={[defaultStyle.OVERLINE, {color: COLORS.altgreen_400}]}>
                RECENT ACTIVITY
              </Text>
              <FlatList
                style={{paddingVertical: 8}}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                keyExtractor={(item) => item.id}
                alwaysBounceHorizontal={false}
                data={recentActivities}
                renderItem={(item) => (
                  <RecentActivityItem
                    data={item.item}
                    navigation={this.navigation}
                  />
                )}
              />
            </View>
          ) : (
            <></>
          )}
          {/* --------- end recent activity ------- */}
          {/* ----- start related blogs ------ */}
          {relatedBlogs && relatedBlogs.length ? (
            <View style={styles.container}>
              <Text
                style={[
                  defaultStyle.OVERLINE,
                  {color: COLORS.altgreen_400, paddingLeft: 16},
                ]}>
                BLOGS
              </Text>
              <FlatList
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                keyExtractor={(item) => item.id}
                style={{backgroundColor: COLORS.bgFill_200, paddingVertical: 8}}
                alwaysBounceHorizontal={false}
                data={relatedBlogs}
                ListFooterComponent={
                  <View
                    style={{
                      height: 300,
                      width: 150,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate(
                          'SeeallPopularStories',
                          this.state.userId,
                        )
                      }
                      style={[defaultShape.ContextBtn_OL]}>
                      <Text
                        style={[typography.Caption, {color: COLORS.dark_500}]}>
                        See more
                      </Text>
                    </TouchableOpacity>
                  </View>
                }
                renderItem={(item) => (
                  <BlogItem
                    handleFollowUnfollow={this.handleFollowUnfollow}
                    item={item.item}
                    index={item.index}
                    navigation={this.navigation}
                  />
                )}
              />
            </View>
          ) : (
            <></>
          )}
          {/* ----- end related blogs ------ */}
          {/* ----- start related photos ------ */}
          {relatedPhotos && relatedPhotos.length ? (
            <View style={styles.container}>
              <Text
                style={[
                  defaultStyle.OVERLINE,
                  {color: COLORS.altgreen_400, paddingLeft: 16},
                ]}>
                PHOTOS
              </Text>
              <FlatList
                style={{paddingVertical: 8}}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                keyExtractor={(item) => item.id}
                style={{backgroundColor: COLORS.bgFill_200, marginTop: 0}}
                alwaysBounceHorizontal={false}
                data={relatedPhotos}
                ListFooterComponent={
                  <View
                    style={{
                      height: 300,
                      width: 150,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate(
                          'SeeallPopularStories',
                          this.state.userId,
                        )
                      }
                      style={[defaultShape.ContextBtn_OL]}>
                      <Text
                        style={[typography.Caption, {color: COLORS.dark_500}]}>
                        See more
                      </Text>
                    </TouchableOpacity>
                  </View>
                }
                onEndReachedThreshold={1}
                renderItem={(item) => (
                  <PhotoItem
                    item={item}
                    navigation={this.navigation}
                    handleFollowUnfollow={this.handleFollowUnfollow}
                  />
                )}
              />
            </View>
          ) : (
            <></>
          )}
          {/* ----- end related photos ------ */}

          {/* ---- start related videos ------  */}
          {realtedVideos && realtedVideos.length ? (
            <View style={styles.container}>
              <Text
                style={[
                  defaultStyle.OVERLINE,
                  {color: COLORS.altgreen_400, paddingLeft: 16},
                ]}>
                VIDEOS
              </Text>
              <FlatList
                style={{paddingVertical: 8}}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                keyExtractor={(item) => item.id}
                style={{backgroundColor: COLORS.bgFill_200, marginTop: 0}}
                alwaysBounceHorizontal={false}
                ListFooterComponent={
                  <View
                    style={{
                      height: 300,
                      width: 150,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate(
                          'SeeallPopularStories',
                          this.state.userId,
                        )
                      }
                      style={[defaultShape.ContextBtn_OL]}>
                      <Text
                        style={[typography.Caption, {color: COLORS.dark_500}]}>
                        See more
                      </Text>
                    </TouchableOpacity>
                  </View>
                }
                data={realtedVideos}
                renderItem={(item) => (
                  <VideoItem
                    item={item}
                    navigation={this.navigation}
                    handleFollowUnfollow={this.handleFollowUnfollow}
                  />
                )}
              />
            </View>
          ) : (
            <></>
          )}
          {/* ---- end related videos ------  */}

          {/* ---- start related forum ----- */}
          {relatedForums && relatedForums.length ? (
            <View style={styles.container}>
              <Text
                style={[
                  defaultStyle.OVERLINE,
                  {color: COLORS.altgreen_400, paddingLeft: 16},
                ]}>
                FORUMS
              </Text>
              <FlatList
                style={{paddingVertical: 8}}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                keyExtractor={(item) => item.id}
                style={{backgroundColor: COLORS.bgFill_200, marginTop: 0}}
                alwaysBounceHorizontal={false}
                ListFooterComponent={
                  <View
                    style={{
                      height: 150,
                      width: 150,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.navigation('AllForumHashtags', {
                          slug: this.state.slug,
                        })
                      }
                      style={[defaultShape.ContextBtn_OL]}>
                      <Text
                        style={[typography.Caption, {color: COLORS.dark_500}]}>
                        See more
                      </Text>
                    </TouchableOpacity>
                  </View>
                }
                data={relatedForums}
                renderItem={(item) => (
                  <ForumsItem
                    item={item}
                    navigation={this.navigation}
                    handleFollowUnfollow={this.handleFollowUnfollow}
                  />
                )}
              />
            </View>
          ) : (
            <></>
          )}
          {/* ---- end related forum ----- */}
          {/* ---- strat related polls ------ */}
          {relatedPoll && relatedPoll.length ? (
            <View style={styles.container}>
              <Text
                style={[
                  defaultStyle.OVERLINE,
                  {color: COLORS.altgreen_400, paddingLeft: 16},
                ]}>
                POLLS
              </Text>
              <FlatList
                style={{paddingVertical: 8}}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                style={{backgroundColor: COLORS.bgFill_200, marginTop: 0}}
                alwaysBounceHorizontal={false}
                data={relatedPoll}
                keyExtractor={(item) => item.id}
                ListFooterComponent={
                  <View
                    style={{
                      height: 150,
                      width: 150,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.navigation('AllPollHashtag', {
                          slug: this.state.slug,
                        })
                      }
                      style={[defaultShape.ContextBtn_OL]}>
                      <Text
                        style={[typography.Caption, {color: COLORS.dark_500}]}>
                        See more
                      </Text>
                    </TouchableOpacity>
                  </View>
                }
                renderItem={(item) => (
                  <PollItem
                    item={item}
                    navigation={this.navigation}
                    handleFollowUnfollow={this.handleFollowUnfollow}
                  />
                )}
              />
            </View>
          ) : (
            <></>
          )}
          {/* ---- end related polls ------ */}

          {/* start related feeds */}
          {relatedFeeds && relatedFeeds.length ? (
            <View style={styles.container}>
              <Text
                style={[
                  defaultStyle.OVERLINE,
                  {color: COLORS.altgreen_400, paddingLeft: 16},
                ]}>
                Others
              </Text>
              <FlatList
                style={{paddingVertical: 8}}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                style={{backgroundColor: COLORS.bgFill_200, marginTop: 0}}
                alwaysBounceHorizontal={false}
                data={relatedFeeds}
                keyExtractor={(item) => item.id}
                ListFooterComponent={
                  <View
                    style={{
                      height: 300,
                      width: 150,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate(
                          'SeeallPopularStories',
                          this.state.userId,
                        )
                      }
                      style={[defaultShape.ContextBtn_OL]}>
                      <Text
                        style={[typography.Caption, {color: COLORS.dark_500}]}>
                        See more
                      </Text>
                    </TouchableOpacity>
                  </View>
                }
                renderItem={(item) => (
                  <FeedsItem
                    item={item}
                    navigation={this.navigation}
                    handleFollowUnfollow={this.handleFollowUnfollow}
                  />
                )}
              />
            </View>
          ) : (
            <></>
          )}
          {/* end related feeds */}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headerView: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 54,
    marginBottom: 5,
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  container: {
    flex: 1,
  },
});
