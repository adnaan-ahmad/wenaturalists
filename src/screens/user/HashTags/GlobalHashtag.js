import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Modal,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';

import HashTagItem from '../../../Components/User/HashTag/HashTagItem';
import defaultShape from '../../../Components/Shared/Shape';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {REACT_APP_userServiceURL} from '../../../../env.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
export default class GlobalHashtag extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      hashtagList: [],
      filterModalOpen: false,
      filter: 'popular',
      filterData: 'POPULAR',
      currentFilterPressed: '',
      page: 0,
      size: 10,
    };
  }

  /** ----- start inital rendering ------- **/
  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      value &&
        this.setState({
          userId: value,
        });
    });
    this.getPopularHashtags('POPULAR');
  }
  /** ----- end initial rendering ------ **/

  /** ---- start fetch popular hashtag ------ **/
  getPopularHashtags = (filterData) => {
    console.log(
      'object',
      `${REACT_APP_userServiceURL}/tags/getGlobal?userId=${this.state.userId}&page=${this.state.page}&size=24&filter=${filterData}`,
    );
    axios({
      method: 'get',
      url: `${REACT_APP_userServiceURL}/tags/getGlobal?userId=${this.state.userId}&page=${this.state.page}&size=24&filter=${filterData}`,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            hashtagList: this.state.hashtagList.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  };
  /** ----- end fetch popular hashtags ------- **/

  // handle load more
  handleLoadMore = () => {
    this.setState({page: this.state.page + 1}, () => this.getPopularHashtags());
  };

  /** -------- start handle filter dropdown and functions -------**/
  currentFilterPressed = (filterprop) => {
    this.setState({page: 0, currentFilterPressed: filterprop}, () => {
      if (filterprop === 'popular') {
        this.getPopularHashtags('POPULAR');
      } else if (filterprop === 'recent') {
        this.getPopularHashtags('RECENT');
      } else if (filterprop === 'trending') {
        this.getPopularHashtags('TRENDING');
      }
    });
  };
  filterModal = () => {
    return (
      <Modal
        visible={this.state.filterModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}></View>
          <LinearGradient
            colors={[
              COLORS.dark_800 + '00',
              COLORS.dark_800 + 'CC',
              COLORS.dark_800,
            ]}
            style={defaultShape.Linear_Gradient}></LinearGradient>
        </View>
        <TouchableOpacity
          activeOpacity={0.7}
          style={defaultShape.CloseBtn}
          onPress={() => this.setState({filterModalOpen: false})}>
          <Icon
            name="Cross"
            size={13}
            color={COLORS.dark_600}
            style={styles.crossIcon}
          />
        </TouchableOpacity>
        <Text
          style={[
            typography.Caption,
            {
              fontSize: 12,
              color: COLORS.dark_500,
              textAlign: 'center',
              alignSelf: 'center',
              position: 'absolute',
              bottom: 110,
              zIndex: 2,
            },
          ]}>
          Sort By
        </Text>
        <View
          style={[
            defaultShape.Modal_Categories_Container,
            {
              paddingTop: 10,
              height: 140,
              flexDirection: 'row',
            },
          ]}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <TouchableOpacity
              style={
                this.state.filter === 'popular'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() =>
                this.setState(
                  {
                    filterModalOpen: false,
                    filter: 'popular',
                    hashtagList: [],
                  },
                  () => this.currentFilterPressed('popular'),
                )
              }>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.filter === 'popular'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Popular
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.filter === 'recent'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() =>
                this.setState(
                  {
                    filterModalOpen: false,
                    filter: 'recent',
                    hashtagList: [],
                  },
                  () => this.currentFilterPressed('recent'),
                )
              }>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.filter === 'recent'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Recent
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.filter === 'trending'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() =>
                this.setState(
                  {
                    filterModalOpen: false,
                    filter: 'trending',
                    hashtagList: [],
                  },
                  () => this.currentFilterPressed('trending'),
                )
              }>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.filter === 'trending'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Trending
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </Modal>
    );
  };
  sortByComponent = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
          marginTop: 15,
          paddingBottom: 10,
          paddingHorizontal: 15,
          alignItems: 'center',
        }}>
        <Text
          style={[
            typography.Title_2,
            {color: COLORS.altgreen_400, marginRight: 10},
          ]}>
          Sort by
        </Text>
        <TouchableOpacity
          onPress={() => this.setState({filterModalOpen: true})}
          style={[styles.sortbyfilter]}>
          <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
            {this.state.filter}
          </Text>
          <Icon
            name="Arrow_Down"
            color={COLORS.altgreen_400}
            size={12}
            style={{marginTop: Platform.OS === 'ios' ? 0 : 8, marginLeft: 5}}
          />
        </TouchableOpacity>
      </View>
    );
  };
  /** -------- end handle filter dropdown and functions -------**/

  hashTagDetailNavigation = (value, params) => {
    this.props.navigation.navigate(value, params);
  };

  render() {
    const {hashtagList} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.filterModal()}
        {/***** Header starts *****/}
        <View style={styles.headerView}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
          </TouchableOpacity>
          <Text
            style={[
              typography.Button_Lead,
              {
                color: COLORS.dark_600,
                marginLeft: 10,
                marginTop: Platform.OS === 'android' ? -8 : 0,
              },
            ]}>
            Hashtag
          </Text>
        </View>
        {/***** Header ends *****/}
        {this.sortByComponent()}
        {/* ------ Start hashtag item on looping ------- */}
        <View>
          <FlatList
            keyboardShouldPersistTaps="handled"
            keyExtractor={(item) => item.id}
            data={hashtagList}
            onEndReached={this.handleLoadMore}
            renderItem={(item) => (
              <HashTagItem
                item={item.item}
                hashTagDetailNavigation={this.hashTagDetailNavigation}
              />
            )}
          />
        </View>
        {/*-------  End hashtag item on looping ---------  */}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headerView: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 54,
    marginBottom: 5,
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  contentView: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    marginTop: 12,
    justifyContent: 'space-between',
    height: 44,
    backgroundColor: COLORS.white,
  },
  selectedTab: {
    borderBottomColor: COLORS.dark_800,
    borderBottomWidth: 4,
    paddingBottom: 4,
    marginLeft: 10,
  },
  unselectedTab: {
    marginLeft: 10,
  },
  selectedTabText: {
    color: COLORS.dark_800,
  },
  unselectedTabText: {
    color: COLORS.altgreen_300,
  },
  unselectedText: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLORS.altgreen_300,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginLeft: 10,
  },
  selectedText: {
    backgroundColor: COLORS.dark_700,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginLeft: 10,
  },
  sortbyfilter: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderRadius: 4,
    paddingHorizontal: 10,
    height: 25,
    marginRight: 5,
  },
});
