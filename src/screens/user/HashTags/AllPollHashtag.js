import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  ScrollView,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {cloneDeep} from 'lodash';

import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import typography from '../../../Components/Shared/Typography';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import defaultStyle from '../../../Components/Shared/Typography';
import PollItem from '../../../Components/User/Common/PollItem';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
export default class AllPollHashtag extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slug: props.route.params.slug,
      relatedPoll: [],
      pollPage: 0,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      value &&
        this.setState({
          userId: value,
        });
    });
    this.fetchRelatedPoll();
  }

  /** start fetch related poll **/
  // loadmore related polls
  loadMorePolls = () => {
    this.setState({pollPage: this.state.pollPage + 1}, () => {
      this.fetchRelatedPoll();
    });
  };

  fetchRelatedPoll = () => {
    axios({
      method: 'GET',
      url: `${REACT_APP_userServiceURL}/tags/getNewsFeed/hashTag/?userId=${
        this.state.userId
      }&hashTag=${this.state.slug}&category=POLL&page=${0}&size=5`,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.statusCode === 200) {
          this.setState({
            relatedPoll: this.state.relatedPoll.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  /** end fetch related poll **/

  handleFollowUnfollow = (isFollowed, userId) => {
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    console.log('object url new', url);
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          let relatedForums = cloneDeep(this.state.relatedForums);
          for (let i of relatedForums) {
            if (i.userId === userId) {
              // console.log(i.followed, !isFollowed);
              i.followed = !isFollowed;
            }
          }
          this.setState({
            relatedForums: relatedForums,
          });
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  navigation = (value, params) => {
    this.props.navigation.navigate(value, params);
  };

  render() {
    const {slug, relatedPoll} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.headerView}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
          </TouchableOpacity>
          <Text
            style={[
              typography.Button_Lead,
              {
                color: COLORS.dark_600,
                marginLeft: 10,
                marginTop: Platform.OS === 'android' ? -8 : 0,
              },
            ]}>
            {'#' + slug}
          </Text>
        </View>
        {/* ---- start related forum ----- */}
        {relatedPoll && relatedPoll.length ? (
          <View style={styles.container}>
            <Text
              style={[
                defaultStyle.OVERLINE,
                {color: COLORS.altgreen_400, paddingLeft: 16},
              ]}>
              POLLS
            </Text>
            <FlatList
              style={{paddingVertical: 8}}
              keyExtractor={(item) => item.id}
              style={{backgroundColor: COLORS.bgFill_200, marginTop: 0}}
              onEndReached={this.loadMorePolls}
              data={relatedPoll}
              renderItem={(item) => (
                <PollItem
                  item={item}
                  detail={true}
                  navigation={this.navigation}
                  handleFollowUnfollow={this.handleFollowUnfollow}
                />
              )}
            />
          </View>
        ) : (
          <></>
        )}
        {/* ---- end related forum ----- */}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headerView: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 54,
    marginBottom: 5,
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  container: {
    flex: 1,
  },
});
