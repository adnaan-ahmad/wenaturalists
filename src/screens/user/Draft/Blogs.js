import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, ActivityIndicator, SafeAreaView, ImageBackground } from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'
import HTMLView from 'react-native-htmlview'
import axios from 'axios'

import defaultShape from '../../../Components/Shared/Shape'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import { causesRequest } from '../../../services/Redux/Actions/User/CausesActions'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultCover from '../../../../assets/defaultCover.png'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultStyle from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const moment = require('moment')

class Blogs extends Component {
    constructor(props) {
        super(props)
        this.state = {
            supporting: true,
            userId: '',
            blogDraftList: []
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {

            this.setState({ userId: value })
            this.getDraftList(value)

            this.props.route.params && this.props.route.params.id !== '' ?
                this.props.causesRequest({ userId: this.props.route.params.id, otherUserId: value })
                : this.props.causesRequest({ userId: value, otherUserId: '' })

        }).catch((e) => {
            console.log(e)
        })

    }

    getDraftList = (userId) => {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/drafts/search-draft-by-type-userId/ARTICLE/?userId=' + userId,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then((response) => {
            let res = response.data
            console.log('getDraftList', response.data.body.content[5])
            this.setState({ blogDraftList: response.data.body.content })
            if (res.message === 'Success!') {

            }
        }).catch((err) => {
            if (err && err.response && err.response.data) {
                console.log(err.response.data)
            }
        })
    }

    handleSupportStatusChange(causeId) {

        let postBody = {
            "userId": this.state.userId,
            "causeId": [causeId]
        };
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/public/cause/join',
            headers: { 'Content-Type': 'application/json' },
            data: postBody,
            withCredentials: true
        }).then((response) => {
            let res = response.data
            console.log('SSSSSSSSSSSSSS', res.status)
            if (res.status === '201 CREATED') {
                this.props.causesRequest({ userId: this.state.userId, otherUserId: '' })
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    removeCause(causeId) {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/public/cause/delete?id=' + causeId + '&userId=' + this.state.userId,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            console.log('DDDDDDDDDDDDD', response.data.message)
            if (response && response.data && response.data.message === 'Success!') {
                this.props.causesRequest({ userId: this.state.userId, otherUserId: '' })
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    render() {
        // console.log('this.props.userCauses.body', this.props.userCauses.body && this.props.userCauses.body.causeList.content[6])
        return (
            <SafeAreaView>

                <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 15 }] : styles.header}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.navigate('DrawerScreen')}>
                            <Icon name="Arrow-Left" size={15} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                        </TouchableOpacity>
                        <Text style={[defaultStyle.H3, { color: COLORS.dark_800, fontSize: 18, marginLeft: 10 }]}>
                            DRAFTS
                        </Text>
                    </View>
                </View>



                <View style={{ alignSelf: 'center', flexDirection: 'row', backgroundColor: COLORS.altgreen_t50, width: 320, marginTop: 20, justifyContent: 'center', borderRadius: 4 }}>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ForumPoll')}
                        style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: '33.33%', borderRadius: 4 }]}>
                        <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>Forum/Poll</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Projects')}
                        style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: '33.33%', borderRadius: 4 }]}>
                        <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>Projects</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[defaultShape.InTab_Btn, { backgroundColor: '#fff', width: '33.33%', borderTopRightRadius: 4, borderBottomRightRadius: 4 }]}>
                        <Text style={[defaultStyle.Caption, { color: COLORS.dark_800 }]}>Blogs</Text>
                    </TouchableOpacity>


                </View>

                {this.state.blogDraftList ?
                    <FlatList
                        contentContainerStyle={styles.causesFlatList}
                        showsVerticalScrollIndicator={false}
                        alwaysBounceHorizontal={false}
                        keyExtractor={(item) => item.id}
                        data={this.state.blogDraftList}
                        renderItem={({ item, index }) => {

                            return <TouchableOpacity onPress={() => this.props.navigation.navigate('EditBlogDraft', {
                                description: item.customAttributeValuePairs.description,
                                title: item.customAttributeValuePairs.articleTitle,
                                hashTags: item.customAttributeValuePairs.hashTags,
                                coverImage: item.customAttributeValuePairs.coverImage ? item.customAttributeValuePairs.coverImage : null,
                                draftId: item.id
                            })}
                                style={styles.individualCause}
                                activeOpacity={0.8}>

                                {item.customAttributeValuePairs.coverImage ? 
                                <Image source={item.customAttributeValuePairs.coverImage ? { uri: item.customAttributeValuePairs.coverImage } : defaultCover} style={styles.causesImage} />
                                    :
                                <View style={[styles.causesImage, { justifyContent: 'center', alignItems: 'center', backgroundColor: COLORS.altgreen_100 }]}>
                                    <View style={{ height: 42, width: 42, backgroundColor: COLORS.altgreen_200, borderRadius: 21, justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="UploadPhoto" size={18} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'ios' ? 0 : 8 }} />

                                    </View>

                                    <Text style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>Add Cover Image</Text>
                                </View>
                                }

                                <View style={styles.linearGradientView}>
                                    <View style={styles.linearGradient}>



                                        <Text style={[defaultStyle.Title_2, { color: COLORS.primarydark, marginTop: 10, marginBottom: 4 }]}>
                                            {item.customAttributeValuePairs.articleTitle}
                                        </Text>


                                        {item.customAttributeValuePairs.description.split(' ').length > 25 ?
                                            <HTMLView
                                                value={item.customAttributeValuePairs.description.split(' ').slice(0, 8).join(' ').trim().concat(' ...')}
                                                stylesheet={styles2}
                                            />
                                            :
                                            <HTMLView
                                                value={item.customAttributeValuePairs.description.split(' ').slice(0, 8).join(' ').trim()}
                                                stylesheet={styles2}
                                            />
                                        }
                                        {/* <Text numberOfLines={3} style={{ }}>{item.customAttributeValuePairs.description}</Text> */}



                                        <Text style={[defaultStyle.Subtitle_2, { color: '#888', fontSize: 11, marginTop: 4 }]}>Drafted on {moment.unix(item.updateTime / 1000).format('Do MMM, YYYY')}</Text>

                                    </View>
                                </View>

                            </TouchableOpacity>

                        }}
                    />
                    :
                    <ActivityIndicator size='small' color="#00394D" />
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6,
        backgroundColor: COLORS.white,
        paddingBottom: 10
    },

    linearGradientView: {
        width: '90%',
        // height: 70,
        // position: 'absolute',
        // bottom: 0
    },
    causesText: {
        color: '#FFFFFF',
        fontSize: 18,
        fontFamily: 'Montserrat-SemiBold',
        position: 'absolute',
        bottom: 20,
        zIndex: 2
    },
    causesImage: {
        width: '90%',
        height: 130,
        marginTop: 14,
        justifyContent: 'center',
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        alignSelf: 'center'
    },
    individualCause: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 2
    },
    seeAllText: {
        color: '#698F8A',
        fontSize: 11,
        fontFamily: 'Montserrat-Medium'
    },
    seeAll: {
        marginHorizontal: 16,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        height: 26,
        width: 70,
        borderWidth: 1,
        borderColor: '#698F8A',
        borderRadius: 13,
        paddingHorizontal: 6,
        paddingVertical: 2
    },
    causesFlatList: {
        paddingTop: 6,
        paddingBottom: 130
    },
    causesSupportedText: {
        color: '#154A59',
        fontSize: 14,
        fontFamily: 'Montserrat-Bold',
        marginLeft: 8
    },
    causesSupported: {
        backgroundColor: '#E7F3E3',
        paddingVertical: 20,
        paddingLeft: 16
    },


    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 20,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    }
})

const styles2 = StyleSheet.create({
    p: {
        color: COLORS.dark_600, fontSize: 12.5
    },
    span: {
        color: COLORS.dark_600, fontSize: 12.5
    },
    div: {
        color: COLORS.dark_600, fontSize: 12.5
    },
})

const mapStateToProps = (state) => {
    return {
        userCausesProgress: state.causesReducer.userCausesProgress,
        userCauses: state.causesReducer.userCauses,
        errorCauses: state.causesReducer.errorCauses
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        causesRequest: (data) => dispatch(causesRequest(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Blogs)
