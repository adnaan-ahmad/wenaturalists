import React, {Component} from 'react';
import {
  Alert,
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
  Modal,
  ScrollView,
  Share,
  Platform,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {TextInput} from 'react-native-paper';
import DatePicker from 'react-native-datepicker';
import {connect} from 'react-redux';
import {cloneDeep} from 'lodash';
import Snackbar from 'react-native-snackbar';
import Clipboard from '@react-native-community/clipboard';
import LinearGradient from 'react-native-linear-gradient';
import HTMLView from 'react-native-htmlview';
import ImagePicker from 'react-native-image-crop-picker';
import DocumentPicker from 'react-native-document-picker';
import {
  actions,
  getContentCSS,
  RichEditor,
  RichToolbar,
} from 'react-native-pell-rich-editor';

import SuggestedHashTags from '../../../Components/User/Common/SuggestedHashTags';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import defaultProfile from '../../../../assets/defaultProfile.png';
import {personalProfileRequest} from '../../../services/Redux/Actions/User/PersonalProfileActions';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultCover from '../../../../assets/defaultCover.png';
import defaultShape from '../../../Components/Shared/Shape';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import PollHeader from '../../../Components/User/Poll/PollHeader';
import AddHashTags from '../../../Components/User/Common/AddHashTags';
import {trimDescription} from '../../../Components/Shared/commonFunction';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');

class ForumAndPoll extends Component {
  richText = React.createRef();
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      selectedTab: '',
      searchParam: '',
      page: 0,
      pollData: [],
      forumPollData: [],
      answerId: '',
      activityId: '',
      slug: '',
      selectedTrending: false,
      createPollModal2Open: false,
      shareModalOpen: false,
      startDate: 0,
      endDate: 0,
      hashTags: [],
      keyPressed: '',
      pollQuestion: '',
      pollDescription: '',
      optionOne: '',
      optionTwo: '',
      publishButton: true,
      shareholders: [
        {name: '', id: ''},
        {name: '', id: ''},
      ],
      currentValue: '',
      createForumModal2Open: false,
      forumTitle: '',
      writeSomething: '',
      addHashTagsModalOpen: false,
      pressedActivityId: '',
      userType: '',
      userName: '',
      currentOpen: '',
      suggestedHashTagsModalOpen: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      this.props.personalProfileRequest({userId: value, otherUserId: ''});
      value && this.setState({userId: value});
      value && this.getPollList(value, '', '');
      this.getDraftList(value);
    });
    AsyncStorage.getItem('userData').then((value) => {
      console.log('userData', JSON.parse(value));
      this.setState({
        userType: JSON.parse(value).type,
        userName: JSON.parse(value).firstName
          ? JSON.parse(value).firstName
          : JSON.parse(value).companyName,
      });
    });
  }

  editorInitializedCallback() {
    this.richText.current?.registerToolbar(function (items) {
      // console.log('Toolbar click, selected items (insert end callback):', items);
    });
  }

  createTwoButtonAlert = (id, type) => {
    Alert.alert('', 'Are you sure you want to delete this ' + type + ' ?', [
      {
        text: 'YES',
        onPress: () => this.handleForumPollDelete(id, type),
        style: 'cancel',
      },
      {
        text: 'NO',
      },
    ]);
  };

  handleForumPollDelete = (id, type) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/' +
        type +
        '/deleteDraft?draftId=' +
        id,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        // console.log(res.status)
        if (res.status === '202 ACCEPTED') {
          this.getDraftList(this.state.userId);
        }
      })
      .catch((err) => {
        // console.log(err.response.data.message)
      });
  };

  changeHashTagsState = (value) => {
    this.setState(value);
  };

  addHashTagsModal = () => {
    return (
      <Modal
        visible={this.state.addHashTagsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <AddHashTags
          changeHashTagsState={this.changeHashTagsState}
          hashTags={this.state.hashTags}
          currentOpen={this.state.currentOpen}
        />
      </Modal>
    );
  };

  changeHashTagsState = (value) => {
    this.setState(value);
  };

  suggestedHashTagsModal = () => {
    return (
      <Modal
        visible={this.state.suggestedHashTagsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <SuggestedHashTags
          changeHashTagsState={this.changeHashTagsState}
          hashTags={this.state.hashTags}
          writeSomething={this.state.writeSomething}
          pollDescription={this.state.pollDescription}
          currentSelected={
            this.state.createForumModal2Open
              ? 'FORUM'
              : this.state.createPollModal2Open
              ? 'POLL'
              : ''
          }
        />
      </Modal>
    );
  };

  createForumModal = () => {
    return (
      <Modal
        onRequestClose={() => this.setState({createForumModal2Open: false})}
        visible={this.state.createForumModal2Open}
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <SafeAreaView style={{flex: 1}}>
          <ScrollView
            style={{marginTop: '-6%', backgroundColor: COLORS.grey_100}}
            keyboardShouldPersistTaps="handled">
            <View
              style={[
                defaultShape.Modal_Categories_Container,
                {backgroundColor: COLORS.grey_100},
              ]}>
              <View
                style={
                  Platform.OS === 'ios'
                    ? [styles.header, {paddingVertical: 12}]
                    : styles.header
                }>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({createForumModal2Open: false})
                      }>
                      <Icon
                        name="Cross"
                        size={14}
                        color={COLORS.dark_500}
                        style={{
                          paddingTop: Platform.OS === 'ios' ? 0 : 8,
                          marginRight: 8,
                        }}
                      />
                    </TouchableOpacity>
                    <Image
                      source={
                        this.props.user.body &&
                        this.props.user.body.originalProfileImage
                          ? {uri: this.props.user.body.originalProfileImage}
                          : this.state.userType === 'COMPANY'
                          ? defaultBusiness
                          : defaultProfile
                      }
                      style={{width: 30, height: 30, borderRadius: 15}}
                    />
                    <Text
                      numberOfLines={1}
                      onPress={() => console.log(this.props.user.body)}
                      style={[
                        typography.Button_Lead,
                        {color: COLORS.dark_800, marginLeft: 8, maxWidth: 180},
                      ]}>
                      Post as{' '}
                      <Text
                        style={[
                          typography.Button_1,
                          {color: COLORS.dark_800, marginLeft: 8, fontSize: 15},
                        ]}>
                        {this.state.userName}
                      </Text>
                    </Text>
                  </View>

                  {this.state.publishButton ? (
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        onPress={() => this.handleSubmitForum()}
                        activeOpacity={0.9}
                        style={[
                          defaultShape.ContextBtn_FL_Drk,
                          {
                            paddingLeft: 10,
                            paddingVertical: 16,
                            alignSelf: 'flex-end',
                            marginRight: 0,
                            borderRadius: 0,
                            borderTopLeftRadius: 4,
                            borderBottomLeftRadius: 4,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                          },
                        ]}>
                        <Text
                          style={[
                            typography.Caption,
                            {color: COLORS.altgreen_300},
                          ]}>
                          PUBLISH
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => this.setState({publishButton: false})}
                        activeOpacity={0.9}
                        style={[
                          defaultShape.ContextBtn_FL_Drk,
                          {
                            paddingRight: 10,
                            paddingVertical: 16,
                            alignSelf: 'flex-end',
                            marginRight: 0,
                            borderRadius: 0,
                            borderTopRightRadius: 4,
                            borderBottomRightRadius: 4,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginLeft: -10,
                          },
                        ]}>
                        <Icon
                          name="Arrow2_Down"
                          color="#91b3a2"
                          size={16}
                          style={{
                            marginTop: Platform.OS === 'android' ? 9 : 0,
                            marginLeft: 6,
                          }}
                        />
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <View style={{flexDirection: 'column'}}>
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          onPress={() => this.handleSubmitForum()}
                          activeOpacity={0.9}
                          style={[
                            defaultShape.ContextBtn_FL_Drk,
                            {
                              paddingLeft: 10,
                              paddingVertical: 16,
                              alignSelf: 'flex-end',
                              marginRight: 0,
                              borderRadius: 0,
                              borderTopLeftRadius: 4,
                              borderTopRightRadius: 4,
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                            },
                          ]}>
                          <Text
                            style={[
                              typography.Caption,
                              {color: COLORS.altgreen_300},
                            ]}>
                            PUBLISH
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => this.setState({publishButton: true})}
                          activeOpacity={0.9}
                          style={[
                            defaultShape.ContextBtn_FL_Drk,
                            {
                              paddingRight: 9.8,
                              paddingVertical: 16,
                              alignSelf: 'flex-end',
                              marginRight: 0,
                              borderRadius: 0,
                              borderTopRightRadius: 4,
                              borderTopRightRadius: 4,
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                              marginLeft: -10,
                            },
                          ]}>
                          <Icon
                            name="Arrow2_Down"
                            color="#91b3a2"
                            size={16}
                            style={{
                              marginTop: Platform.OS === 'android' ? 9 : 0,
                              marginLeft: 6,
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                      <TouchableOpacity
                        onPress={() => this.saveForumAsDraft()}
                        activeOpacity={0.9}
                        style={[
                          defaultShape.ContextBtn_FL_Drk,
                          {
                            width: 108.09,
                            paddingVertical: 16,
                            alignSelf: 'flex-end',
                            marginRight: 0,
                            borderRadius: 0,
                            borderBottomLeftRadius: 4,
                            borderBottomRightRadius: 4,
                          },
                        ]}>
                        <Text
                          style={[
                            typography.Caption,
                            {color: COLORS.altgreen_300, marginLeft: -42},
                          ]}>
                          DRAFT
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )}
                </View>
              </View>

              <View
                style={{
                  backgroundColor: COLORS.grey_100,
                  alignItems: 'center',
                  width: '100%',
                  paddingBottom: 16,
                }}>
                <View
                  style={{
                    width: '100%',
                    height: 90,
                    backgroundColor: '#00394d',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={[typography.H3, {color: '#dadd21'}]}>
                    CREATE A FORUM
                  </Text>
                </View>

                <View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      paddingLeft: 6,
                      paddingRight: 12,
                      marginTop: 8,
                    }}>
                    <TextInput
                      theme={{
                        colors: {
                          text: COLORS.dark_700,
                          primary: COLORS.altgreen_300,
                          placeholder: COLORS.altgreen_300,
                        },
                      }}
                      label="Add forum title"
                      multiline
                      selectionColor="#C8DB6E"
                      style={[
                        typography.H3,
                        {
                          width: '90%',
                          backgroundColor: COLORS.grey_100,
                          color: COLORS.dark_700,
                          borderRadius: 50,
                        },
                      ]}
                      onChangeText={(value) =>
                        this.setState({forumTitle: value})
                      }
                      value={this.state.forumTitle}
                    />
                  </View>

                  <View>
                    <RichToolbar
                      style={[styles.richBar]}
                      flatContainerStyle={styles.flatStyle}
                      editor={this.richText}
                      disabled={false}
                      selectedIconTint={'#2095F2'}
                      disabledIconTint={'#bfbfbf'}
                    />
                    <RichEditor
                      ref={this.richText}
                      initialContentHTML={this.state.writeSomething}
                      useContainer={true}
                      style={styles.rich}
                      initialHeight={300}
                      placeholder={'please write here ...'}
                      onChange={(value) =>
                        this.setState({writeSomething: value})
                      }
                      editorInitializedCallback={() =>
                        this.editorInitializedCallback()
                      }
                    />
                    <RichToolbar
                      style={[styles.richBar]}
                      flatContainerStyle={styles.flatStyle}
                      editor={this.richText}
                      insertVideo={this.insertVideo}
                      onPressAddImage={() => {
                        ImagePicker.openPicker({
                          width: 300,
                          height: 400,
                          mediaType: 'photo',
                        }).then((image) => {
                          ImagePicker.openCropper({
                            path: image.path,
                            width: 300,
                            height: 200,
                            includeBase64: true,
                          })
                            .then((cropImage) => {
                              console.log('PressAddImage', cropImage);
                              this.richText.current?.insertImage(
                                `data:${cropImage.mime};base64,${cropImage.data}`,
                              );
                            })
                            .catch((err) => console.log(err));
                        });
                      }}
                      disabled={false}
                      actions={[
                        // actions.undo,
                        // actions.redo,
                        // actions.insertVideo,
                        actions.insertImage,
                        actions.setStrikethrough,
                        actions.checkboxList,
                        actions.insertOrderedList,
                        actions.blockquote,
                        actions.alignLeft,
                        actions.alignCenter,
                        actions.alignRight,
                        actions.code,
                        actions.line,

                        actions.foreColor,
                        actions.hiliteColor,
                        actions.heading1,
                        actions.heading4,
                        'fontSize',
                      ]}
                      iconMap={{
                        [actions.heading1]: ({tintColor}) => (
                          <Text style={[styles.tib, {color: tintColor}]}>
                            H1
                          </Text>
                        ),
                        [actions.heading4]: ({tintColor}) => (
                          <Text style={[styles.tib, {color: tintColor}]}>
                            H3
                          </Text>
                        ),
                      }}
                      fontSize={this.fontSize}
                      selectedIconTint={'#2095F2'}
                      disabledIconTint={'#bfbfbf'}
                    />
                  </View>

                  {/* <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => {
                      this.secondTextInput.focus();
                    }}
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      paddingLeft: 6,
                      paddingRight: 12,
                      marginTop: 20,
                      backgroundColor: COLORS.white,
                      borderRadius: 10,
                    }}>
                    <TextInput
                      ref={(input) => {
                        this.secondTextInput = input;
                      }}
                      theme={{
                        colors: {
                          text: COLORS.dark_700,
                          primary: COLORS.altgreen_300,
                          placeholder: COLORS.altgreen_300,
                        },
                      }}
                      label="Write Something here"
                      multiline
                      selectionColor="#C8DB6E"
                      style={[
                        typography.H5,
                        {
                          minHeight: 260,
                          width: '90%',
                          backgroundColor: COLORS.white,
                          color: COLORS.dark_700,
                        },
                      ]}
                      onChangeText={(value) =>
                        this.setState({
                          writeSomething: value,
                          seconds: 59,
                          suggestedHashTagsModalOpen: value[value.length - 1] === '#' ? true : false,
                          createForumModal2Open: value[value.length - 1] === '#' ? false : true
                        })
                      }
                      value={this.state.writeSomething}
                    />
                  </TouchableOpacity> */}
                </View>

                <View
                  style={{
                    backgroundColor: COLORS.grey_100,
                    alignItems: 'flex-start',
                    alignSelf: 'flex-start',
                    paddingVertical: 20,
                    paddingLeft: 30,
                    width: '100%',
                    marginTop: 10,
                  }}>
                  <FlatList
                    keyboardShouldPersistTaps="handled"
                    scrollEventThrottle={0}
                    ref={(ref) => (this.scrollView = ref)}
                    onContentSizeChange={() => {
                      this.scrollView.scrollToEnd({animated: false});
                      this.setState({hashTagModalOpen: true});
                    }}
                    columnWrapperStyle={{
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                    }}
                    numColumns={3}
                    showsVerticalScrollIndicator={false}
                    data={[...this.state.hashTags, 'lastData']}
                    keyExtractor={(item, index) => item + index}
                    renderItem={
                      ({item, index}) =>
                        index < this.state.hashTags.length ? (
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({
                                hashTags: this.state.hashTags.filter(
                                  (value, index2) =>
                                    value + index2 !== item + index,
                                ),
                              })
                            }
                            activeOpacity={0.6}
                            style={{
                              flexDirection: 'row',
                              backgroundColor: COLORS.altgreen_t50 + '80',
                              paddingHorizontal: 10,
                              marginVertical: 6,
                              height: 28,
                              borderRadius: 17,
                              justifyContent: 'center',
                              alignItems: 'center',
                              marginRight: 6,
                            }}>
                            <Text
                              style={[
                                typography.Caption,
                                {
                                  color: COLORS.dark_500,
                                  marginRight: 4,
                                  marginTop: Platform.OS === 'ios' ? -2 : 0,
                                },
                              ]}>
                              {item}
                            </Text>
                            <Icon
                              name="Cross_Rounded"
                              color={COLORS.dark_500}
                              size={15}
                              style={{
                                marginTop: Platform.OS === 'android' ? 10 : 0,
                              }}
                            />
                          </TouchableOpacity>
                        ) : (
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({
                                addHashTagsModalOpen: true,
                                createForumModal2Open: false,
                                currentOpen: 'FORUM',
                                seconds: 59,
                              })
                            }
                            activeOpacity={0.6}
                            style={{
                              flexDirection: 'row',
                              backgroundColor: COLORS.grey_200,
                              width: 137,
                              height: 28,
                              borderRadius: 17,
                              justifyContent: 'center',
                              alignItems: 'center',
                              marginTop: 7,
                            }}>
                            <Icon
                              name="Hashtag"
                              color={COLORS.dark_500}
                              size={14}
                              style={{
                                marginTop: Platform.OS === 'android' ? 7 : 0,
                              }}
                            />
                            <Text
                              style={[
                                typography.Caption,
                                {
                                  color: COLORS.dark_500,
                                  marginLeft: 5,
                                  marginTop: Platform.OS === 'ios' ? -2 : 0,
                                },
                              ]}>
                              Add Hashtag
                            </Text>
                          </TouchableOpacity>
                        )
                      // (
                      //   <TextInput
                      //     theme={{
                      //       colors: {
                      //         text: COLORS.dark_600,
                      //         primary: COLORS.altgreen_300,
                      //         placeholder: COLORS.altgreen_300,
                      //       },
                      //     }}
                      //     // label="Write something interesting"
                      //     placeholder="Add Hashtags"
                      //     // autoFocus={true}
                      //     selectionColor="#C8DB6E"
                      //     style={[
                      //       typography.H6,
                      //       {
                      //         height: 28,
                      //         backgroundColor: COLORS.white,
                      //         color: COLORS.dark_600,
                      //         textAlign: 'center',
                      //         marginVertical: 6,
                      //       },
                      //     ]}
                      //     onChangeText={(value) => {
                      //       this.setState({query2: value.trim()});
                      //       value[value.length - 1] === ' ' && value.trim()
                      //         ? this.setState({
                      //             hashTags: [
                      //               ...this.state.hashTags,
                      //               value.trim(),
                      //             ],
                      //             query2: '',
                      //           })
                      //         : null;
                      //     }}
                      //     value={this.state.query2}
                      //   />
                      // )
                    }
                  />
                </View>
              </View>

              {/* <View
                style={
                  this.state.focus && Platform.OS === 'ios'
                    ? [styles.commentBoxView, { bottom: 260 }]
                    : styles.commentBoxView
                }>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                  }}>
                  <TouchableOpacity
                    style={defaultShape.AdjunctBtn_Prim}
                    activeOpacity={0.6}>
                    <Icon
                      name="Location"
                      color={COLORS.dark_600}
                      size={17}
                      style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    // onPress={this.documentPickerImage}
                    style={defaultShape.AdjunctBtn_Prim}
                    activeOpacity={0.6}>
                    <Icon
                      name="UploadPhoto"
                      color={COLORS.dark_600}
                      size={17}
                      style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    // onPress={this.documentPickerVideo}
                    style={defaultShape.AdjunctBtn_Prim}
                    activeOpacity={0.6}>
                    <Icon
                      name="AddVideo"
                      color={COLORS.dark_600}
                      size={17}
                      style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    // onPress={this.documentPickerAudio}
                    style={defaultShape.AdjunctBtn_Prim}
                    activeOpacity={0.6}>
                    <Icon
                      name="Recorder"
                      color={COLORS.dark_600}
                      size={17}
                      style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                    />
                  </TouchableOpacity>

                  <TouchableOpacity
                    // onPress={this.documentPickerSecondary}
                    style={defaultShape.AdjunctBtn_Prim}
                    activeOpacity={0.6}>
                    <Icon
                      name="Clip"
                      color={COLORS.dark_600}
                      size={17}
                      style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.setState({ infoModal: true })}
                    style={defaultShape.AdjunctBtn_Small_Sec}
                    activeOpacity={0.6}>
                    <Icon
                      name="Info_I"
                      color={COLORS.dark_600}
                      size={14}
                      style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                    />
                  </TouchableOpacity>
                </View>
              </View> */}
            </View>
          </ScrollView>
        </SafeAreaView>
      </Modal>
    );
  };

  getDraftList = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/drafts/find-draft-by-type-userId/ALLFORUMANDPOLL?userId=' +
        userId +
        '&page=0' +
        '&size=500',
      cache: true,
      withCredentials: true,
      headers: {'Content-Type': 'application/json'},
    })
      .then((response) => {
        let res = response.data;
        // console.log('getDraftListNew2', response.data.body.content[0]);
        this.setState({forumPollData: response.data.body.content});
        if (res.message === 'Success!') {
        }
      })
      .catch((err) => {
        console.log('getDraftList3', err);
        if (err && err.response && err.response.data) {
          // console.log(err.response.data)
        }
      });
  };

  changeState = (item) => {
    this.setState(item, () =>
      this.getPollList(
        this.state.userId,
        this.state.selectedTab,
        this.state.searchParam,
      ),
    );
  };

  getPollList = (userId, showType, searchParam) => {
    axios({
      method: 'get',
      url: this.state.selectedTrending
        ? REACT_APP_userServiceURL +
          '/poll/trending?userId=' +
          userId +
          '&page=' +
          this.state.page +
          '&size=10'
        : REACT_APP_userServiceURL +
          '/poll/list?showType=' +
          showType +
          '&filterType=POLL&userId=' +
          userId +
          '&searchParam=' +
          searchParam +
          '&page=' +
          this.state.page +
          '&size=10',
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          this.setState({
            pollData: this.state.pollData.concat(res.body.content),
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handlePinnedForumAndPoll = (item) => {
    let tempPollData = cloneDeep(this.state.pollData);
    let pinnedBody = {
      userId: this.state.userId,
      entityId: item.item.id,
      entityType: 'POLL',
      pinned: !item.item.pinned,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/pinned/create',
      headers: {'Content-Type': 'application/json'},
      data: pinnedBody,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          tempPollData[item.index].pinned = !item.item.pinned;
          this.setState({pollData: tempPollData});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleLike = (activityId, liked, index) => {
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          let tempPollData = cloneDeep(this.state.pollData);
          tempPollData[index].liked = !liked;
          liked === false
            ? (tempPollData[index].likesCount += 1)
            : (tempPollData[index].likesCount -= 1);
          this.setState({pollData: tempPollData});
        } else {
          console.log(response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleVote = (activityId, answerId, itemIndex) => {
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      answerId: answerId,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/public/poll/vote',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          let tempPollData = cloneDeep(this.state.pollData);
          tempPollData[itemIndex].votePoll = true;
          tempPollData[itemIndex].answerList[answerId - 1].vote = true;
          tempPollData[itemIndex].answerList[answerId - 1].totalVote += 1;
          this.setState({pollData: tempPollData});
        } else {
          console.log(response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  totalVoteCount = (item) => {
    let count = 0;
    for (let i of item) {
      count += i.totalVote;
    }
    return count;
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message: REACT_APP_domainUrl + '/poll/' + this.state.slug,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of ', result.activityType);
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false});
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl + '/post/' + this.state.pressedActivityId,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false});
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share through Mail
              </Text>
              <Icon
                name="Mail_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleSubmitForum = () => {
    if (
      this.state.forumTitle.trim() !== '' &&
      this.state.writeSomething.trim() !== '' &&
      this.state.hashTags.length > 0
    ) {
      let postBody = {
        userId: this.state.userId,
        title: this.state.forumTitle,
        content: this.state.writeSomething,
        // "content": 'zxcvbnm<p><img src="content://com.android.providers.media.documents/document/image%3A3626" /></p>',
        hashTags: this.state.hashTags
          ? this.state.hashTags.map((item) => {
              return item.replace(/#/g, '');
            })
          : [],
        status: 'ENABLE',
        userType: 'WENAT',
        createdBy: this.state.userName,
      };
      if (this.state.pressedActivityId) {
        postBody.draftId = this.state.pressedActivityId;
      }

      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/backend/public/forum/create',
        headers: {'Content-Type': 'application/json'},
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (res.status === '201 CREATED') {
            this.getDraftList(this.state.userId);
            Snackbar.show({
              backgroundColor: COLORS.dark_900,
              text: 'Forum created successfully',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
            setTimeout(() => {
              this.setState({
                createForumModal2Open: false,
                forumTitle: '',
                writeSomething: '',
                hashTags: [],
              });
            }, 1000);
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            console.log(err.response.data);
            Snackbar.show({
              backgroundColor: '#B22222',
              text: err.response.data.message,
              duration: Snackbar.LENGTH_LONG,
            });
          } else {
            console.log('else part', err.response.data);
          }
        });
      this.setState({
        forumPollData: this.state.forumPollData.filter(
          (item) => item.id !== this.state.pressedActivityId,
        ),
      });
    } else {
      if (this.state.forumTitle.trim() === '')
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter the title',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.writeSomething.trim() === '')
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter the description',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.hashTags.length === 0)
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Add at least 1 hashtag',
          duration: Snackbar.LENGTH_LONG,
        });
    }
  };

  // getShareHolders = (item) => {
  //   item.item.customAttributeValuePairs.shareholders.map((data) => {

  //   })

  //   [
  //     { name: item.item.customAttributeValuePairs.shareholders.length ? item.item.customAttributeValuePairs.shareholders[0].name : '', id: 1 },
  //     { name: item.item.customAttributeValuePairs.shareholders.length ? item.item.customAttributeValuePairs.shareholders[1].name : '', id: 2 },
  //   ]
  // }

  renderPollItem = (item) => {
    return item.item.type === 'POLL' ? (
      <View style={styles.forumItemView}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: '104%',
          }}>
          <Text
            onPress={() =>
              this.setState({
                pressedActivityId: item.item.id,
                pollQuestion: item.item.customAttributeValuePairs.pollQuestion,
                pollDescription: item.item.customAttributeValuePairs
                  .pollDescription
                  ? item.item.customAttributeValuePairs.pollDescription
                  : '',
                // optionOne: item.item.customAttributeValuePairs.shareholders.length ? item.item.customAttributeValuePairs.shareholders[0].name : '',
                // optionTwo: item.item.customAttributeValuePairs.shareholders.length ? item.item.customAttributeValuePairs.shareholders[1].name : '',
                shareholders: item.item.customAttributeValuePairs.shareholders,
                hashTags: item.item.customAttributeValuePairs.hashTags
                  ? item.item.customAttributeValuePairs.hashTags
                  : [],
                // startDate: item.item.customAttributeValuePairs.startDate ? this.unixTimeDatePicker(item.item.customAttributeValuePairs.startDate) : Date.now(),
                // endDate: item.item.customAttributeValuePairs.endDate ? this.unixTimeDatePicker(item.item.customAttributeValuePairs.endDate) : Date.now(),
                createPollModal2Open: true,
              })
            }
            numberOfLines={1}
            style={{
              color: 'rgb(0, 57, 77)',
              fontSize: 19,
              fontWeight: '600',
              maxWidth: '90%',
            }}>
            {item.item.customAttributeValuePairs.pollQuestion}
          </Text>

          <TouchableOpacity
            activeOpacity={0.5}
            style={defaultShape.Nav_Gylph_Btn}
            onPress={() => this.createTwoButtonAlert(item.item.id, 'poll')}>
            <Icon
              name="TrashBin"
              color={COLORS.altgreen_300}
              size={13}
              style={{marginTop: Platform.OS === 'android' && 6}}
            />
          </TouchableOpacity>
        </View>
        {item.item.customAttributeValuePairs.pollDescription ? (
          <Text style={{color: 'rgb(67, 69, 74)', fontSize: 14, marginTop: 4}}>
            {trimDescription(item.item.customAttributeValuePairs.pollDescription)}
          </Text>
        ) : (
          <></>
        )}
        <Text
          onPress={() =>
            console.log(
              item.item && item.item.customAttributeValuePairs
                ? item.item.customAttributeValuePairs.shareholders
                : null,
            )
          }
          numberOfLines={1}
          style={[
            {color: '#698f8a', marginVertical: 10},
            typography.Subtitle_1,
          ]}>
          Select an option:
        </Text>

        {item.item && item.item.customAttributeValuePairs ? (
          item.item.customAttributeValuePairs.shareholders.map(
            (value, index) => (
              <View
                key={index}
                style={{
                  backgroundColor: 'rgb(242, 243, 244)',
                  borderRadius: 4,
                  paddingHorizontal: 6,
                  paddingVertical: 6,
                  marginVertical: 4,
                  width: 'auto',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: 'rgb(66, 111, 115)',
                    fontWeight: '600',
                    fontSize: 15,
                  }}>
                  {value.name}
                </Text>
              </View>
            ),
          )
        ) : (
          <></>
        )}

        <Text
          style={[
            {
              color: 'rgb(136, 136, 136)',
              marginTop: 8,
              fontSize: 14,
              fontWeight: '500',
            },
            typography.Note2,
          ]}>
          Drafted on{' '}
          {moment.unix(item.item.updateTime / 1000).format('Do MMM, YYYY')}
        </Text>
      </View>
    ) : (
      <View style={styles.forumItemView}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: '104%',
          }}>
          <Text
            onPress={() =>
              this.setState({
                pressedActivityId: item.item.id,
                forumTitle: item.item.customAttributeValuePairs.title,
                writeSomething: item.item.customAttributeValuePairs.content
                  ? trimDescription(item.item.customAttributeValuePairs.content)
                  : '',
                hashTags: item.item.customAttributeValuePairs.hashTags
                  ? item.item.customAttributeValuePairs.hashTags
                  : [],
                createForumModal2Open: true,
              })
            }
            numberOfLines={1}
            style={{
              color: 'rgb(0, 57, 77)',
              fontSize: 19,
              fontWeight: '600',
              maxWidth: '90%',
            }}>
            {item.item.customAttributeValuePairs.title}
          </Text>
          <TouchableOpacity
            activeOpacity={0.5}
            style={defaultShape.Nav_Gylph_Btn}
            onPress={() => this.createTwoButtonAlert(item.item.id, 'forum')}>
            <Icon
              name="TrashBin"
              color={COLORS.altgreen_300}
              size={13}
              style={{marginTop: Platform.OS === 'android' && 6}}
            />
          </TouchableOpacity>
        </View>
        {item.item.customAttributeValuePairs.content ? (
          // <HTMLView
          //   value={item.item.customAttributeValuePairs.content}
          //   stylesheet={styles2}
          // />
          <Text
            style={[
              {
                color: COLORS.altgreen_400,
              },
              typography.Body_1,
            ]}
            numberOfLines={3}>
            {trimDescription(item.item.customAttributeValuePairs.content)}
          </Text>
        ) : (
          <></>
        )}

        <Text
          style={[
            {
              color: 'rgb(136, 136, 136)',
              marginTop: 8,
              fontSize: 14,
              fontWeight: '500',
            },
            typography.Note2,
          ]}>
          Drafted on{' '}
          {moment.unix(item.item.updateTime / 1000).format('Do MMM, YYYY')}
        </Text>
      </View>
    );
  };

  unixTimeDatePicker = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    return day + '-' + month + '-' + year;
  };

  currentDate = () => {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = dd + '-' + mm + '-' + yyyy;

    return today;
  };

  handleSubmit = () => {
    if (
      this.state.pollQuestion.trim() !== '' &&
      this.state.hashTags.length > 0 &&
      this.state.shareholders[0].name.trim() !== '' &&
      this.state.shareholders[1].name.trim() !== '' &&
      this.state.startDate !== 0 &&
      this.state.endDate !== 0 &&
      this.state.shareholders.every((item) => item.name.trim() !== '')
    ) {
      let postBody = {
        userId: this.state.userId,
        userName:
          this.props.user.body && this.props.user.body.firstName
            ? this.props.user.body.firstName
            : this.props.user.body.companyName,
        question: this.state.pollQuestion,
        hashTags: this.state.hashTags
          ? this.state.hashTags.map((item) => {
              return item.replace(/#/g, '');
            })
          : [],
        description: this.state.pollDescription,
        pollAnswersList: this.state.shareholders,
        status: 'ENABLE',
        startDate: this.state.startDate,
        endDate: this.state.endDate,
        userType: 'WENAT',
        createdBy:
          this.props.user.body && this.props.user.body.firstName
            ? this.props.user.body.firstName
            : this.props.user.body.companyName,
      };

      if (this.state.pressedActivityId) {
        postBody.draftId = this.state.pressedActivityId;
      }

      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/backend/public/poll/create',
        headers: {'Content-Type': 'application/json'},
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          console.log(res.status);
          if (res.status === '201 CREATED') {
            Snackbar.show({
              backgroundColor: COLORS.dark_900,
              text: 'Poll created successfully',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
            this.getPollList(
              this.state.userId,
              this.state.selectedTab,
              this.state.searchParam,
            );
            this.getDraftList(this.state.userId);
            setTimeout(() => {
              this.setState({
                createPollModal2Open: false,
                pollQuestion: '',
                pollDescription: '',
                hashTags: [],
                startDate: 0,
                endDate: 0,
                shareholders: [
                  {name: '', id: ''},
                  {name: '', id: ''},
                ],
                publishButton: true,
              });
            }, 1000);
          }
        })
        .catch((err) => {
          console.log(err.response.data.message);
          if (err && err.response && err.response.data) {
            if (err.response.data.message === 'For input string: ""')
              Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Please enter an option. You may also remove the option.',
                duration: Snackbar.LENGTH_LONG,
              });
            else {
              console.log(err.response.data);
              Snackbar.show({
                backgroundColor: '#B22222',
                text: err.response.data.message,
                duration: Snackbar.LENGTH_LONG,
              });
            }
          }
        });
      this.setState({
        forumPollData: this.state.forumPollData.filter(
          (item) => item.id !== this.state.pressedActivityId,
        ),
      });
    } else {
      if (this.state.pollQuestion.trim() === '')
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter a valid poll question',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (
        this.state.shareholders[0].name.trim() === '' ||
        this.state.shareholders[1].name.trim() === ''
      )
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter an option',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.startDate === 0)
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter the start date',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.endDate === 0)
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter the end date',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.hashTags.length === 0)
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Add at least 1 hashtag',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (
        this.state.shareholders.length > 2 &&
        this.state.shareholders.every((item) => item.name.trim() !== '') ===
          false
      )
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter an option. You may also remove the option',
          duration: Snackbar.LENGTH_LONG,
        });
    }
  };

  saveAsDraft = () => {
    if (this.state.pollQuestion.trim() !== '') {
      let parameterDetails = {};

      parameterDetails.userType = 'WENAT';
      parameterDetails.createdBy =
        this.props.user.body && this.props.user.body.firstName;
      parameterDetails.pollQuestion = this.state.pollQuestion;
      if (this.state.pollDescription !== '')
        parameterDetails.pollDescription = this.state.pollDescription;
      parameterDetails.shareholders = this.state.shareholders;
      if (this.state.startDate)
        parameterDetails.startDate = this.state.startDate;
      if (this.state.endDate) parameterDetails.endDate = this.state.endDate;

      if (this.state.hashTags.length > 0)
        parameterDetails.hashTags = this.state.hashTags.map((item) => {
          return item.replace(/#/g, '');
        });

      let postBody = {
        id: this.state.pressedActivityId ? this.state.pressedActivityId : null,
        userId: this.state.userId,
        parameterDetails: parameterDetails,
        type: 'POLL',
      };
      // if (this.state.pressedActivityId) {
      //   postBody.draftId = this.state.pressedActivityId;
      // }

      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/backend/drafts/create',
        headers: {'Content-Type': 'application/json'},
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          console.log(res.status);
          if (res.status === '201 CREATED') {
            Snackbar.show({
              backgroundColor: COLORS.dark_900,
              text: 'Poll Saved as draft',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
            setTimeout(() => {
              this.setState({
                createPollModal2Open: false,
                pollQuestion: '',
                pollDescription: '',
                hashTags: [],
                startDate: 0,
                endDate: 0,
                shareholders: [
                  {name: '', id: ''},
                  {name: '', id: ''},
                ],
                publishButton: true,
              });
              this.getPollList(
                this.state.userId,
                this.state.selectedTab,
                this.state.searchParam,
              );
              this.getDraftList(this.state.userId);
            }, 1000);
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            console.log(err.response.data.message);
            Snackbar.show({
              backgroundColor: '#B22222',
              text: err.response.data.message,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    } else {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please enter Poll Question',
        duration: Snackbar.LENGTH_LONG,
      });
    }
  };

  saveForumAsDraft = () => {
    if (this.state.forumTitle.trim() !== '') {
      let parameterDetails = {};

      parameterDetails.title = this.state.forumTitle;
      if (this.state.writeSomething !== '')
        parameterDetails.content = this.state.writeSomething;
      if (this.state.hashTags.length > 0)
        parameterDetails.hashTags = this.state.hashTags.map((item) => {
          return item.replace(/#/g, '');
        });
      parameterDetails.createdBy = this.state.userName;

      let postBody = {
        id: this.state.pressedActivityId ? this.state.pressedActivityId : null,
        userId: this.state.userId,
        parameterDetails: parameterDetails,
        type: 'FORUM',
      };
      // if (this.state.pressedActivityId) {
      //   postBody.draftId = this.state.pressedActivityId;
      // }

      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/backend/drafts/create',
        headers: {'Content-Type': 'application/json'},
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          console.log(res.status);
          if (res.status === '201 CREATED') {
            Snackbar.show({
              backgroundColor: COLORS.dark_900,
              text: 'Forum Saved as draft',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
            setTimeout(() => {
              this.setState({
                createForumModal2Open: false,
                forumTitle: '',
                writeSomething: '',
                hashTags: [],
                publishButton: true,
              });
              this.getDraftList(this.state.userId);
            }, 1000);
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            console.log(err.response.data.message);
            Snackbar.show({
              backgroundColor: '#B22222',
              text: err.response.data.message,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    } else {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please enter the Title',
        duration: Snackbar.LENGTH_LONG,
      });
    }
  };

  // createPollModal = () => {
  //   return (
  //     <Modal
  //       onRequestClose={() => this.setState({ createPollModal2Open: false })}
  //       visible={this.state.createPollModal2Open}
  //       animationType="slide"
  //       supportedOrientations={['portrait', 'landscape']}>
  //       <SafeAreaView style={{ flex: 1 }}>
  //         <ScrollView
  //           style={{ marginTop: '-6%' }}
  //           keyboardShouldPersistTaps="handled">
  //           <View
  //             style={[
  //               defaultShape.Modal_Categories_Container,
  //               { backgroundColor: COLORS.white },
  //             ]}>
  //             <View
  //               style={
  //                 Platform.OS === 'ios'
  //                   ? [styles.header, { paddingVertical: 12 }]
  //                   : styles.header
  //               }>
  //               <View
  //                 style={{
  //                   flexDirection: 'row',
  //                   alignItems: 'center',
  //                   justifyContent: 'space-between',
  //                   width: '100%',
  //                 }}>
  //                 <View style={{ flexDirection: 'row', alignItems: 'center' }}>
  //                   <TouchableOpacity
  //                     onPress={() =>
  //                       this.setState({ createPollModal2Open: false })
  //                     }>
  //                     <Icon
  //                       name="Cross"
  //                       size={14}
  //                       color={COLORS.dark_500}
  //                       style={{
  //                         paddingTop: Platform.OS === 'ios' ? 0 : 8,
  //                         marginRight: 8,
  //                       }}
  //                     />
  //                   </TouchableOpacity>

  //                   <Image
  //                     source={
  //                       this.props.user.body &&
  //                         this.props.user.body.originalProfileImage
  //                         ? { uri: this.props.user.body.originalProfileImage }
  //                         : defaultCover
  //                     }
  //                     style={{ width: 30, height: 30, borderRadius: 15 }}
  //                   />
  //                   <Text
  //                     style={[
  //                       typography.Button_Lead,
  //                       { color: COLORS.dark_800, marginLeft: 8 },
  //                     ]}>
  //                     Post as{' '}
  //                     <Text
  //                       style={[
  //                         typography.Button_1,
  //                         { color: COLORS.dark_800, marginLeft: 8, fontSize: 15 },
  //                       ]}>
  //                       {this.props.user.body && this.props.user.body.userName}
  //                     </Text>
  //                   </Text>
  //                 </View>

  //                 {this.state.publishButton ? (
  //                   <View style={{ flexDirection: 'row' }}>
  //                     <TouchableOpacity
  //                       onPress={() => this.state.createPollModal2Open ? this.handleSubmit() : this.handleSubmitForum()}
  //                       activeOpacity={0.9}
  //                       style={[
  //                         defaultShape.ContextBtn_FL_Drk,
  //                         {
  //                           paddingLeft: 10,
  //                           paddingVertical: 16,
  //                           alignSelf: 'flex-end',
  //                           marginRight: 0,
  //                           borderRadius: 0,
  //                           borderTopLeftRadius: 4,
  //                           borderBottomLeftRadius: 4,
  //                           flexDirection: 'row',
  //                           justifyContent: 'center',
  //                           alignItems: 'center',
  //                         },
  //                       ]}>
  //                       <Text
  //                         style={[
  //                           typography.Caption,
  //                           { color: COLORS.altgreen_300 },
  //                         ]}>
  //                         PUBLISH
  //                       </Text>
  //                     </TouchableOpacity>
  //                     <TouchableOpacity
  //                       onPress={() => this.setState({ publishButton: false })}
  //                       activeOpacity={0.9}
  //                       style={[
  //                         defaultShape.ContextBtn_FL_Drk,
  //                         {
  //                           paddingRight: 10,
  //                           paddingVertical: 16,
  //                           alignSelf: 'flex-end',
  //                           marginRight: 0,
  //                           borderRadius: 0,
  //                           borderTopRightRadius: 4,
  //                           borderBottomRightRadius: 4,
  //                           flexDirection: 'row',
  //                           justifyContent: 'center',
  //                           alignItems: 'center',
  //                           marginLeft: -10,
  //                         },
  //                       ]}>
  //                       <Icon
  //                         name="Arrow2_Down"
  //                         color="#91b3a2"
  //                         size={16}
  //                         style={{
  //                           marginTop: Platform.OS === 'android' ? 9 : 0,
  //                           marginLeft: 6,
  //                         }}
  //                       />
  //                     </TouchableOpacity>
  //                   </View>
  //                 ) : (
  //                   <View style={{ flexDirection: 'column' }}>
  //                     <View style={{ flexDirection: 'row' }}>
  //                       <TouchableOpacity
  //                         onPress={() => this.state.createPollModal2Open ? this.handleSubmit() : this.handleSubmitForum()}
  //                         activeOpacity={0.9}
  //                         style={[
  //                           defaultShape.ContextBtn_FL_Drk,
  //                           {
  //                             paddingLeft: 10,
  //                             paddingVertical: 16,
  //                             alignSelf: 'flex-end',
  //                             marginRight: 0,
  //                             borderRadius: 0,
  //                             borderTopLeftRadius: 4,
  //                             borderTopRightRadius: 4,
  //                             flexDirection: 'row',
  //                             justifyContent: 'center',
  //                             alignItems: 'center',
  //                           },
  //                         ]}>
  //                         <Text
  //                           style={[
  //                             typography.Caption,
  //                             { color: COLORS.altgreen_300 },
  //                           ]}>
  //                           PUBLISH
  //                         </Text>
  //                       </TouchableOpacity>
  //                       <TouchableOpacity
  //                         onPress={() => this.setState({ publishButton: true })}
  //                         activeOpacity={0.9}
  //                         style={[
  //                           defaultShape.ContextBtn_FL_Drk,
  //                           {
  //                             paddingRight: 9.8,
  //                             paddingVertical: 16,
  //                             alignSelf: 'flex-end',
  //                             marginRight: 0,
  //                             borderRadius: 0,
  //                             borderTopRightRadius: 4,
  //                             borderTopRightRadius: 4,
  //                             flexDirection: 'row',
  //                             justifyContent: 'center',
  //                             alignItems: 'center',
  //                             marginLeft: -10,
  //                           },
  //                         ]}>
  //                         <Icon
  //                           name="Arrow2_Down"
  //                           color="#91b3a2"
  //                           size={16}
  //                           style={{
  //                             marginTop: Platform.OS === 'android' ? 9 : 0,
  //                             marginLeft: 6,
  //                           }}
  //                         />
  //                       </TouchableOpacity>
  //                     </View>
  //                     <TouchableOpacity
  //                       onPress={() => this.state.createPollModal2Open ? this.saveAsDraft() : this.saveForumAsDraft()}
  //                       activeOpacity={0.9}
  //                       style={[
  //                         defaultShape.ContextBtn_FL_Drk,
  //                         {
  //                           width: 108.09,
  //                           paddingVertical: 16,
  //                           alignSelf: 'flex-end',
  //                           marginRight: 0,
  //                           borderRadius: 0,
  //                           borderBottomLeftRadius: 4,
  //                           borderBottomRightRadius: 4,
  //                         },
  //                       ]}>
  //                       <Text
  //                         style={[
  //                           typography.Caption,
  //                           { color: COLORS.altgreen_300 },
  //                         ]}>
  //                         DRAFT
  //                       </Text>
  //                     </TouchableOpacity>
  //                   </View>
  //                 )}
  //               </View>
  //             </View>

  //             <View
  //               style={{
  //                 backgroundColor: COLORS.white,
  //                 alignItems: 'center',
  //                 width: '100%',
  //                 paddingBottom: 16,
  //               }}>
  //               <View
  //                 style={{
  //                   width: '100%',
  //                   height: 90,
  //                   backgroundColor: '#00394d',
  //                   justifyContent: 'center',
  //                   alignItems: 'center',
  //                 }}>
  //                 <Text style={[typography.H3, { color: '#dadd21' }]}>
  //                   CREATE A POLL
  //                 </Text>
  //               </View>

  //               <View style={{}}>
  //                 <View
  //                   style={{
  //                     flexDirection: 'row',
  //                     alignItems: 'center',
  //                     justifyContent: 'space-between',
  //                     paddingLeft: 6,
  //                     paddingRight: 12,
  //                     marginTop: 8,
  //                   }}>
  //                   <TextInput
  //                     theme={{
  //                       colors: {
  //                         text: COLORS.dark_700,
  //                         primary: COLORS.altgreen_300,
  //                         placeholder: COLORS.altgreen_300,
  //                       },
  //                     }}
  //                     label="Add a Poll Question"
  //                     multiline
  //                     selectionColor="#C8DB6E"
  //                     style={[
  //                       typography.H3,
  //                       {
  //                         width: '90%',
  //                         backgroundColor: COLORS.white,
  //                         color: COLORS.dark_700,
  //                         borderRadius: 50,
  //                       },
  //                     ]}
  //                     onChangeText={(value) =>
  //                       this.setState({ pollQuestion: value })
  //                     }
  //                     value={this.state.pollQuestion}
  //                   />
  //                 </View>

  //                 <View
  //                   style={{
  //                     flexDirection: 'row',
  //                     alignItems: 'center',
  //                     justifyContent: 'space-between',
  //                     paddingLeft: 6,
  //                     paddingRight: 12,
  //                     marginTop: 8,
  //                   }}>
  //                   <TextInput
  //                     theme={{
  //                       colors: {
  //                         text: COLORS.dark_700,
  //                         primary: COLORS.altgreen_300,
  //                         placeholder: COLORS.altgreen_300,
  //                       },
  //                     }}
  //                     label="Write Poll Description"
  //                     multiline
  //                     selectionColor="#C8DB6E"
  //                     style={[
  //                       typography.H5,
  //                       {
  //                         width: '90%',
  //                         backgroundColor: COLORS.white,
  //                         color: COLORS.dark_700,
  //                       },
  //                     ]}
  //                     onChangeText={(value) =>
  //                       this.setState({ pollDescription: value })
  //                     }
  //                     value={this.state.pollDescription}
  //                   />
  //                 </View>
  //               </View>

  //               <Text
  //                 style={[
  //                   typography.Title_1,
  //                   {
  //                     fontFamily: 'Montserrat-Medium',
  //                     color: '#698f8a',
  //                     alignSelf: 'flex-start',
  //                     marginLeft: 30,
  //                     fontSize: 17,
  //                     marginTop: 20,
  //                   },
  //                 ]}>
  //                 Add Options
  //               </Text>

  //               <View
  //                 style={{
  //                   flexDirection: 'row',
  //                   alignItems: 'center',
  //                   justifyContent: 'space-between',
  //                   paddingLeft: 6,
  //                   paddingRight: 12,
  //                   marginTop: 6,
  //                 }}>
  //                 <TextInput
  //                   mode="outlined"
  //                   theme={{
  //                     colors: {
  //                       text: COLORS.dark_700,
  //                       primary: COLORS.altgreen_300,
  //                       placeholder: COLORS.altgreen_300,
  //                     },
  //                   }}
  //                   label="Option #1"
  //                   selectionColor="#C8DB6E"
  //                   style={[
  //                     typography.Subtitle_1,
  //                     {
  //                       width: '90%',
  //                       height: 50,
  //                       color: COLORS.dark_700,
  //                     },
  //                   ]}
  //                   onChangeText={(value) => {
  //                     this.setState({
  //                       optionOne: value,
  //                     });
  //                   }}
  //                   value={this.state.optionOne}
  //                 />
  //               </View>

  //               <View
  //                 style={{
  //                   flexDirection: 'row',
  //                   alignItems: 'center',
  //                   justifyContent: 'space-between',
  //                   paddingLeft: 6,
  //                   paddingRight: 12,
  //                   marginTop: 6,
  //                 }}>
  //                 <TextInput
  //                   mode="outlined"
  //                   theme={{
  //                     colors: {
  //                       text: COLORS.dark_700,
  //                       primary: COLORS.altgreen_300,
  //                       placeholder: COLORS.altgreen_300,
  //                     },
  //                   }}
  //                   label="Option #2"
  //                   selectionColor="#C8DB6E"
  //                   style={[
  //                     typography.Subtitle_1,
  //                     {
  //                       width: '90%',
  //                       height: 50,
  //                       color: COLORS.dark_700,
  //                     },
  //                   ]}
  //                   onChangeText={(value) => this.setState({ optionTwo: value })}
  //                   value={this.state.optionTwo}
  //                 />
  //               </View>
  //             </View>

  //             <View
  //               style={{
  //                 backgroundColor: 'white',
  //                 alignItems: 'center',
  //                 width: '100%',
  //                 marginTop: 0,
  //               }}>
  //               <View
  //                 style={{
  //                   flexDirection: 'row',
  //                   alignItems: 'center',
  //                   justifyContent: 'space-between',
  //                   marginTop: 10,
  //                   width: '90%',
  //                 }}>
  //                 <TouchableOpacity
  //                   activeOpacity={0.6}
  //                   style={{
  //                     height: 56,
  //                     width: '40%',
  //                     flexDirection: 'row',
  //                     alignItems: 'center',
  //                     justifyContent: 'space-between',
  //                     marginTop: 20,
  //                     paddingLeft: 6,
  //                     paddingRight: 12,
  //                   }}>
  //                   <View style={{ width: '100%' }}>
  //                     <Text
  //                       style={[
  //                         typography.Note2,
  //                         { color: COLORS.altgreen_300 },
  //                       ]}>
  //                       Start Date
  //                     </Text>
  //                     <DatePicker
  //                       style={{ width: '100%', borderWidth: 0 }}
  //                       date={
  //                         typeof this.state.startDate == 'string' &&
  //                           this.state.startDate.includes('-')
  //                           ? this.state.startDate
  //                           : this.unixTimeDatePicker(this.state.startDate)
  //                       }
  //                       mode="date"
  //                       placeholder="select date"
  //                       format="DD-MM-YYYY"
  //                       minDate={this.currentDate()}
  //                       maxDate="01-01-2100"
  //                       // maxDate="16-07-2021"
  //                       confirmBtnText="Confirm"
  //                       cancelBtnText="Cancel"
  //                       customStyles={{
  //                         dateIcon: {
  //                           position: 'absolute',
  //                           right: 0,
  //                           top: -10,
  //                           marginRight: 0,
  //                         },
  //                         dateInput: {
  //                           alignItems: 'flex-start',
  //                           borderWidth: 0,
  //                         },
  //                         dateText: {
  //                           color: COLORS.dark_700,
  //                           marginTop: -20,
  //                         },
  //                       }}
  //                       onDateChange={(date) => {
  //                         let tempdate = date.split('-').reverse().join('-');
  //                         let convertedUnixTime = new Date(tempdate).getTime();
  //                         this.setState({ startDate: convertedUnixTime });
  //                       }}
  //                     />
  //                   </View>
  //                   {/* <Icon name="Calender_OL" color='#698F8A' size={16} style={{ marginTop: Platform.OS === 'android' ? 12 : 0, zIndex: 2 }} /> */}
  //                 </TouchableOpacity>

  //                 <TouchableOpacity
  //                   activeOpacity={0.6}
  //                   style={{
  //                     height: 56,
  //                     width: '40%',
  //                     flexDirection: 'row',
  //                     alignItems: 'center',
  //                     justifyContent: 'space-between',
  //                     marginTop: 20,
  //                     paddingLeft: 6,
  //                     paddingRight: 12,
  //                   }}>
  //                   <View style={{ width: '100%' }}>
  //                     <Text
  //                       style={[
  //                         typography.Note2,
  //                         { color: COLORS.altgreen_300 },
  //                       ]}>
  //                       End Date
  //                     </Text>
  //                     <DatePicker
  //                       style={{ width: '100%', borderWidth: 0 }}
  //                       date={
  //                         typeof this.state.endDate == 'string' &&
  //                           this.state.endDate.includes('-')
  //                           ? this.state.endDate
  //                           : this.unixTimeDatePicker(this.state.endDate)
  //                       }
  //                       mode="date"
  //                       placeholder="select date"
  //                       format="DD-MM-YYYY"
  //                       minDate={this.currentDate()}
  //                       maxDate="01-01-2100"
  //                       // maxDate="16-07-2021"
  //                       confirmBtnText="Confirm"
  //                       cancelBtnText="Cancel"
  //                       customStyles={{
  //                         dateIcon: {
  //                           position: 'absolute',
  //                           right: 0,
  //                           top: -10,
  //                           marginRight: 0,
  //                         },
  //                         dateInput: {
  //                           alignItems: 'flex-start',
  //                           borderWidth: 0,
  //                         },
  //                         dateText: {
  //                           color: COLORS.dark_700,
  //                           marginTop: -20,
  //                         },
  //                       }}
  //                       onDateChange={(date) => {
  //                         let tempdate = date.split('-').reverse().join('-');
  //                         let convertedUnixTime = new Date(tempdate).getTime();
  //                         this.setState({ endDate: convertedUnixTime });
  //                       }}
  //                     />
  //                   </View>
  //                 </TouchableOpacity>
  //               </View>

  //               <View
  //                 style={{
  //                   backgroundColor: COLORS.white,
  //                   alignItems: 'flex-start',
  //                   alignSelf: 'flex-start',
  //                   paddingVertical: 20,
  //                   paddingLeft: 30,
  //                   width: '100%',
  //                 }}>
  //                 <FlatList
  //                   keyboardShouldPersistTaps="handled"
  //                   scrollEventThrottle={0}
  //                   ref={(ref) => (this.scrollView = ref)}
  //                   onContentSizeChange={() => {
  //                     this.scrollView.scrollToEnd({ animated: false });
  //                     this.setState({ hashTagModalOpen: true });
  //                   }}
  //                   columnWrapperStyle={{
  //                     flexDirection: 'row',
  //                     flexWrap: 'wrap',
  //                   }}
  //                   numColumns={3}
  //                   showsVerticalScrollIndicator={false}
  //                   data={[...this.state.hashTags, 'lastData']}
  //                   keyExtractor={(item, index) => item + index}
  //                   renderItem={({ item, index }) =>
  //                     index < this.state.hashTags.length ? (
  //                       <TouchableOpacity
  //                         onPress={() =>
  //                           this.setState({
  //                             hashTags: this.state.hashTags.filter(
  //                               (value, index2) =>
  //                                 value + index2 !== item + index,
  //                             ),
  //                           })
  //                         }
  //                         activeOpacity={0.6}
  //                         style={{
  //                           flexDirection: 'row',
  //                           backgroundColor: COLORS.altgreen_t50 + '80',
  //                           paddingHorizontal: 10,
  //                           marginVertical: 6,
  //                           height: 28,
  //                           borderRadius: 17,
  //                           justifyContent: 'center',
  //                           alignItems: 'center',
  //                           marginRight: 6,
  //                         }}>
  //                         <Text
  //                           style={[
  //                             typography.Caption,
  //                             {
  //                               color: COLORS.dark_500,
  //                               marginRight: 4,
  //                               marginTop: Platform.OS === 'ios' ? -2 : 0,
  //                             },
  //                           ]}>
  //                           {item}
  //                         </Text>
  //                         <Icon
  //                           name="Cross_Rounded"
  //                           color={COLORS.dark_500}
  //                           size={15}
  //                           style={{
  //                             marginTop: Platform.OS === 'android' ? 10 : 0,
  //                           }}
  //                         />
  //                       </TouchableOpacity>
  //                     ) : (
  //                       <TextInput
  //                         theme={{
  //                           colors: {
  //                             text: COLORS.dark_600,
  //                             primary: COLORS.altgreen_300,
  //                             placeholder: COLORS.altgreen_300,
  //                           },
  //                         }}
  //                         // label="Write something interesting"
  //                         placeholder="Add Hashtags"
  //                         // autoFocus={true}
  //                         selectionColor="#C8DB6E"
  //                         style={[
  //                           typography.H6,
  //                           {
  //                             height: 28,
  //                             backgroundColor: COLORS.white,
  //                             color: COLORS.dark_600,
  //                             textAlign: 'center',
  //                             marginVertical: 6,
  //                           },
  //                         ]}
  //                         onChangeText={(value) => {
  //                           this.setState({ query2: value.trim() });
  //                           value[value.length - 1] === ' ' && value.trim()
  //                             ? this.setState({
  //                               hashTags: [
  //                                 ...this.state.hashTags,
  //                                 value.trim(),
  //                               ],
  //                               query2: '',
  //                             })
  //                             : null;
  //                         }}
  //                         value={this.state.query2}
  //                       />
  //                     )
  //                   }
  //                 />

  //                 {/* <View style={{ width: '80%', backgroundColor: this.state.hashTags.length ? COLORS.green_300 : COLORS.grey_200, height: 2 }}></View> */}
  //               </View>
  //             </View>

  //             <View style={styles.border2}></View>
  //             <View
  //               style={[
  //                 styles.border,
  //                 { position: 'absolute', bottom: 80 },
  //               ]}></View>
  //           </View>
  //         </ScrollView>
  //       </SafeAreaView>
  //     </Modal>
  //   );
  // };

  getMinEndDate = () => {
    var day = new Date(this.state.startDate);

    var nextDay = new Date(day);
    nextDay.setDate(day.getDate() + 1);
    return nextDay;
  };

  createPollModal = () => {
    return (
      <Modal
        onRequestClose={() => this.setState({createPollModal2Open: false})}
        visible={this.state.createPollModal2Open}
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <SafeAreaView style={{flex: 1}}>
          <ScrollView
            style={{marginTop: '-6%'}}
            keyboardShouldPersistTaps="handled">
            <View
              style={[
                defaultShape.Modal_Categories_Container,
                {backgroundColor: COLORS.white},
              ]}>
              <View
                style={
                  Platform.OS === 'ios'
                    ? [styles.header, {paddingVertical: 12}]
                    : styles.header
                }>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                  }}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <TouchableOpacity
                      onPress={() =>
                        // this.state.pollQuestion.trim() === '' ? this.setState({ createPollModal2Open: false }) : this.saveAsDraft()
                        // this.saveAsDraft()
                        this.setState({
                          createPollModal2Open: false,
                          pollQuestion: '',
                          pollDescription: '',
                          hashTags: [],
                          startDate: 0,
                          endDate: 0,
                          shareholders: [
                            {name: '', id: ''},
                            {name: '', id: ''},
                          ],
                          publishButton: true,
                          addHashTagsModalOpen: false,
                        })
                      }>
                      <Icon
                        name="Cross"
                        size={14}
                        color={COLORS.dark_500}
                        style={{
                          paddingTop: Platform.OS === 'ios' ? 0 : 8,
                          marginRight: 8,
                        }}
                      />
                    </TouchableOpacity>

                    <Image
                      source={
                        this.props.user.body &&
                        this.props.user.body.originalProfileImage
                          ? {uri: this.props.user.body.originalProfileImage}
                          : this.state.userType === 'COMPANY'
                          ? defaultBusiness
                          : defaultProfile
                      }
                      style={{width: 30, height: 30, borderRadius: 15}}
                    />
                    <Text
                      onPress={() => console.log(this.props.user.body)}
                      numberOfLines={1}
                      style={[
                        typography.Button_Lead,
                        {color: COLORS.dark_800, marginLeft: 8, maxWidth: 180},
                      ]}>
                      Post as{' '}
                      <Text
                        style={[
                          typography.Button_1,
                          {color: COLORS.dark_800, marginLeft: 8, fontSize: 15},
                        ]}>
                        {this.state.userName}
                      </Text>
                    </Text>
                  </View>

                  {this.state.publishButton ? (
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        onPress={() => this.handleSubmit()}
                        activeOpacity={0.9}
                        style={[
                          defaultShape.ContextBtn_FL_Drk,
                          {
                            paddingLeft: 10,
                            paddingVertical: 16,
                            alignSelf: 'flex-end',
                            marginRight: 0,
                            borderRadius: 0,
                            borderTopLeftRadius: 4,
                            borderBottomLeftRadius: 4,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                          },
                        ]}>
                        <Text
                          style={[
                            typography.Caption,
                            {color: COLORS.altgreen_300},
                          ]}>
                          PUBLISH
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => this.setState({publishButton: false})}
                        activeOpacity={0.9}
                        style={[
                          defaultShape.ContextBtn_FL_Drk,
                          {
                            paddingRight: 10,
                            paddingVertical: 16,
                            alignSelf: 'flex-end',
                            marginRight: 0,
                            borderRadius: 0,
                            borderTopRightRadius: 4,
                            borderBottomRightRadius: 4,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginLeft: -10,
                          },
                        ]}>
                        <Icon
                          name="Arrow2_Down"
                          color="#91b3a2"
                          size={16}
                          style={{
                            marginTop: Platform.OS === 'android' ? 9 : 0,
                            marginLeft: 6,
                          }}
                        />
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <View style={{flexDirection: 'column'}}>
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          onPress={() => this.handleSubmit()}
                          activeOpacity={0.9}
                          style={[
                            defaultShape.ContextBtn_FL_Drk,
                            {
                              paddingLeft: 10,
                              paddingVertical: 16,
                              alignSelf: 'flex-end',
                              marginRight: 0,
                              borderRadius: 0,
                              borderTopLeftRadius: 4,
                              borderTopRightRadius: 4,
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                            },
                          ]}>
                          <Text
                            style={[
                              typography.Caption,
                              {color: COLORS.altgreen_300},
                            ]}>
                            PUBLISH
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => this.setState({publishButton: true})}
                          activeOpacity={0.9}
                          style={[
                            defaultShape.ContextBtn_FL_Drk,
                            {
                              paddingRight: 9.8,
                              paddingVertical: 16,
                              alignSelf: 'flex-end',
                              marginRight: 0,
                              borderRadius: 0,
                              borderTopRightRadius: 4,
                              borderTopRightRadius: 4,
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                              marginLeft: -10,
                            },
                          ]}>
                          <Icon
                            name="Arrow2_Down"
                            color="#91b3a2"
                            size={16}
                            style={{
                              marginTop: Platform.OS === 'android' ? 9 : 0,
                              marginLeft: 6,
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                      <TouchableOpacity
                        onPress={() => this.saveAsDraft()}
                        activeOpacity={0.9}
                        style={[
                          defaultShape.ContextBtn_FL_Drk,
                          {
                            width: 108.09,
                            paddingVertical: 16,
                            alignSelf: 'flex-end',
                            alignItems: 'flex-start',
                            marginRight: 0,
                            borderRadius: 0,
                            borderBottomLeftRadius: 4,
                            borderBottomRightRadius: 4,
                          },
                        ]}>
                        <Text
                          style={[
                            typography.Caption,
                            {color: COLORS.altgreen_300, marginLeft: 1.5},
                          ]}>
                          DRAFT
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )}
                </View>
              </View>

              <View
                style={{
                  backgroundColor: COLORS.white,
                  alignItems: 'center',
                  width: '100%',
                  paddingBottom: 16,
                }}>
                <View
                  style={{
                    width: '100%',
                    height: 90,
                    backgroundColor: '#00394d',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={[typography.H3, {color: '#dadd21'}]}>
                    CREATE A POLL
                  </Text>
                </View>

                <View style={{}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      paddingLeft: 6,
                      paddingRight: 12,
                      marginTop: 8,
                    }}>
                    <TextInput
                      theme={{
                        colors: {
                          text: COLORS.dark_700,
                          primary: COLORS.altgreen_300,
                          placeholder: COLORS.altgreen_300,
                        },
                      }}
                      label="Add a Poll Question"
                      multiline
                      selectionColor="#C8DB6E"
                      style={[
                        typography.H3,
                        {
                          width: '90%',
                          backgroundColor: COLORS.white,
                          color: COLORS.dark_700,
                          borderRadius: 50,
                        },
                      ]}
                      onChangeText={(value) =>
                        this.setState({pollQuestion: value, seconds: 59})
                      }
                      value={this.state.pollQuestion}
                    />
                  </View>

                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      paddingLeft: 6,
                      paddingRight: 12,
                      marginTop: 8,
                    }}>
                    <TextInput
                      theme={{
                        colors: {
                          text: COLORS.dark_700,
                          primary: COLORS.altgreen_300,
                          placeholder: COLORS.altgreen_300,
                        },
                      }}
                      label="Write Poll Description"
                      multiline
                      selectionColor="#C8DB6E"
                      style={[
                        typography.H5,
                        {
                          width: '90%',
                          backgroundColor: COLORS.white,
                          color: COLORS.dark_700,
                        },
                      ]}
                      onChangeText={(value) =>
                        this.setState({
                          pollDescription: value,
                          seconds: 59,
                          // suggestedHashTagsModalOpen: value[value.length - 1] === '#' ? true : false,
                          // createPollModal2Open: value[value.length - 1] === '#' ? false : true
                        })
                      }
                      value={this.state.pollDescription}
                    />
                  </View>
                </View>

                <Text
                  style={[
                    typography.Title_1,
                    {
                      fontFamily: 'Montserrat-Medium',
                      color: '#698f8a',
                      alignSelf: 'flex-start',
                      marginLeft: 30,
                      fontSize: 17,
                      marginTop: 20,
                    },
                  ]}>
                  Add Options
                </Text>

                {this.state.shareholders.map((item, index) => {
                  return (
                    <View
                      key={index}
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        paddingLeft: 6,
                        paddingRight: 12,
                        marginTop: 6,
                        marginLeft: index <= 1 ? 2 : 0,
                      }}>
                      <TextInput
                        mode="outlined"
                        theme={{
                          colors: {
                            text: COLORS.dark_700,
                            primary: COLORS.altgreen_300,
                            placeholder: COLORS.altgreen_300,
                          },
                        }}
                        label={`${'Option #' + (index + 1)}`}
                        selectionColor="#C8DB6E"
                        style={[
                          typography.Subtitle_1,
                          {
                            width: '91%',
                            height: 50,
                            color: COLORS.dark_700,
                          },
                        ]}
                        onChangeText={(value) =>
                          this.handleShareholderNameChange(index, value)
                        }
                        value={item.name}
                      />

                      {index > 1 ? (
                        <TouchableOpacity
                          onPress={() => this.handleRemoveShareholder(index)}
                          activeOpacity={0.5}
                          style={{
                            alignItems: 'center',
                            flexDirection: 'row',
                            marginLeft: -36,
                          }}>
                          <Icon
                            name="Delete"
                            color={COLORS.altgreen_300}
                            size={14}
                            style={{
                              marginTop: Platform.OS === 'android' ? 10 : 0,
                              marginHorizontal: 6,
                            }}
                          />
                        </TouchableOpacity>
                      ) : null}
                    </View>
                  );
                })}

                <TouchableOpacity
                  onPress={() => this.handleAddShareholder()}
                  activeOpacity={0.8}
                  style={{
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignSelf: 'flex-end',
                    marginRight: 32,
                  }}>
                  <Icon
                    name="AddList"
                    color={COLORS.altgreen_300}
                    size={14}
                    style={{
                      marginTop: Platform.OS === 'android' ? 12 : 0,
                      marginHorizontal: 6,
                    }}
                  />
                  <Text
                    style={[
                      typography.Title_1,
                      {
                        fontFamily: 'Montserrat-Medium',
                        color: '#698f8a',
                        // alignSelf: 'flex-start',
                        // marginLeft: 30,
                        fontSize: 14,
                        marginTop: 0,
                      },
                    ]}>
                    Add More
                  </Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  backgroundColor: 'white',
                  alignItems: 'center',
                  width: '100%',
                  marginTop: 0,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 10,
                    width: '90%',
                  }}>
                  <TouchableOpacity
                    activeOpacity={0.6}
                    style={{
                      height: 56,
                      width: '40%',
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginTop: 20,
                      paddingLeft: 6,
                      paddingRight: 12,
                    }}>
                    <View style={{width: '100%'}}>
                      <Text
                        style={[
                          typography.Note2,
                          {color: COLORS.altgreen_300},
                        ]}>
                        Start Date
                      </Text>
                      <DatePicker
                        style={{width: '100%', borderWidth: 0}}
                        date={
                          this.state.startDate === 0
                            ? ''
                            : this.unixTimeDatePicker(this.state.startDate)

                          // typeof this.state.startDate == 'string' &&
                          //   this.state.startDate.includes('-')
                          //   ? this.state.startDate
                          //   : this.unixTimeDatePicker(this.state.startDate)
                        }
                        mode="date"
                        placeholder="Start date"
                        format="DD-MM-YYYY"
                        minDate={this.currentDate()}
                        maxDate="01-01-2100"
                        // maxDate="16-07-2021"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        // hideText='true'
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: -10,
                            marginRight: 0,
                          },
                          dateInput: {
                            alignItems: 'flex-start',
                            borderWidth: 0,
                          },
                          dateText: {
                            color: COLORS.dark_700,
                            marginTop: -20,
                          },
                        }}
                        onDateChange={(date) => {
                          let tempdate = date.split('-').reverse().join('-');
                          let convertedUnixTime = new Date(tempdate).getTime();
                          this.setState({
                            startDate: convertedUnixTime,
                            seconds: 59,
                            endDate:
                              convertedUnixTime >=
                              new Date(this.state.endDate).getTime()
                                ? 0
                                : this.state.endDate,
                          });
                        }}
                      />
                    </View>
                    {/* <Icon name="Calender_OL" color='#698F8A' size={16} style={{ marginTop: Platform.OS === 'android' ? 12 : 0, zIndex: 2 }} /> */}
                  </TouchableOpacity>

                  {this.state.startDate ? (
                    <TouchableOpacity
                      activeOpacity={0.6}
                      style={{
                        height: 56,
                        width: '40%',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginTop: 20,
                        paddingLeft: 6,
                        paddingRight: 12,
                      }}>
                      <View style={{width: '100%'}}>
                        <Text
                          style={[
                            typography.Note2,
                            {color: COLORS.altgreen_300},
                          ]}>
                          End Date
                        </Text>
                        <DatePicker
                          style={{width: '100%', borderWidth: 0}}
                          date={
                            this.state.endDate === 0
                              ? ''
                              : this.unixTimeDatePicker(this.state.endDate)
                          }
                          mode="date"
                          placeholder="End date"
                          format="DD-MM-YYYY"
                          minDate={this.getMinEndDate()}
                          maxDate="01-01-2100"
                          // maxDate="16-07-2021"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          customStyles={{
                            dateIcon: {
                              position: 'absolute',
                              right: 0,
                              top: -10,
                              marginRight: 0,
                            },
                            dateInput: {
                              alignItems: 'flex-start',
                              borderWidth: 0,
                            },
                            dateText: {
                              color: COLORS.dark_700,
                              marginTop: -20,
                            },
                          }}
                          onDateChange={(date) => {
                            let tempdate = date.split('-').reverse().join('-');
                            let convertedUnixTime = new Date(
                              tempdate,
                            ).getTime();
                            this.setState({
                              endDate: convertedUnixTime,
                              seconds: 59,
                            });
                          }}
                        />
                      </View>
                    </TouchableOpacity>
                  ) : null}
                </View>

                <View
                  style={{
                    backgroundColor: COLORS.white,
                    alignItems: 'flex-start',
                    alignSelf: 'flex-start',
                    paddingVertical: 20,
                    paddingLeft: 30,
                    width: '100%',
                  }}>
                  <FlatList
                    keyboardShouldPersistTaps="handled"
                    scrollEventThrottle={0}
                    ref={(ref) => (this.scrollView = ref)}
                    onContentSizeChange={() => {
                      this.scrollView.scrollToEnd({animated: false});
                      this.setState({hashTagModalOpen: true});
                    }}
                    columnWrapperStyle={{
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                    }}
                    numColumns={3}
                    showsVerticalScrollIndicator={false}
                    data={[...this.state.hashTags, 'lastData']}
                    keyExtractor={(item, index) => item + index}
                    renderItem={({item, index}) =>
                      index < this.state.hashTags.length ? (
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              hashTags: this.state.hashTags.filter(
                                (value, index2) =>
                                  value + index2 !== item + index,
                              ),
                              seconds: 59,
                            })
                          }
                          activeOpacity={0.6}
                          style={{
                            flexDirection: 'row',
                            backgroundColor: COLORS.altgreen_t50 + '80',
                            paddingHorizontal: 10,
                            marginVertical: 6,
                            height: 28,
                            borderRadius: 17,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginRight: 6,
                          }}>
                          <Text
                            style={[
                              typography.Caption,
                              {
                                color: COLORS.dark_500,
                                marginRight: 4,
                                marginTop: Platform.OS === 'ios' ? -2 : 0,
                              },
                            ]}>
                            {item}
                          </Text>
                          <Icon
                            name="Cross_Rounded"
                            color={COLORS.dark_500}
                            size={15}
                            style={{
                              marginTop: Platform.OS === 'android' ? 10 : 0,
                            }}
                          />
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              addHashTagsModalOpen: true,
                              createPollModal2Open: false,
                              currentOpen: 'POLL',
                              seconds: 59,
                            })
                          }
                          activeOpacity={0.6}
                          style={{
                            flexDirection: 'row',
                            backgroundColor: COLORS.grey_200,
                            width: 137,
                            height: 28,
                            borderRadius: 17,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 7,
                          }}>
                          <Icon
                            name="Hashtag"
                            color={COLORS.dark_500}
                            size={14}
                            style={{
                              marginTop: Platform.OS === 'android' ? 7 : 0,
                            }}
                          />
                          <Text
                            style={[
                              typography.Caption,
                              {
                                color: COLORS.dark_500,
                                marginLeft: 5,
                                marginTop: Platform.OS === 'ios' ? -2 : 0,
                              },
                            ]}>
                            Add Hashtag
                          </Text>
                        </TouchableOpacity>
                      )
                    }
                  />

                  {/* <View style={{ width: '80%', backgroundColor: this.state.hashTags.length ? COLORS.green_300 : COLORS.grey_200, height: 2 }}></View> */}
                </View>
              </View>

              <View style={styles.border2}></View>
              <View
                style={[
                  styles.border,
                  {position: 'absolute', bottom: 80},
                ]}></View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </Modal>
    );
  };

  handleShareholderNameChange = (idx, value) => {
    const newShareholders = this.state.shareholders.map((shareholder, sidx) => {
      if (idx !== sidx) return shareholder;
      return {...shareholder, name: value, id: idx + 1};
    });

    this.setState({shareholders: newShareholders, seconds: 59});
  };

  handleRemoveShareholder = (idx) => {
    this.setState({
      shareholders: this.state.shareholders.filter((s, sidx) => idx !== sidx),
      seconds: 59,
    });
  };

  handleAddShareholder = () => {
    this.setState({
      shareholders: this.state.shareholders.concat([{name: '', id: ''}]),
      seconds: 59,
    });
  };

  handleLoadMore = () => {
    this.setState({page: this.state.page + 1}, () =>
      this.getPollList(
        this.state.userId,
        this.state.selectedTab,
        this.state.searchParam,
      ),
    );
  };

  render() {
    // console.log('shareholders', this.state.shareholders)
    return (
      <SafeAreaView style={{flex: 1}}>
        {/*********** Header starts ***********/}
        {this.createPollModal()}
        {this.shareModal()}
        {this.createForumModal()}
        {this.addHashTagsModal()}
        {this.suggestedHashTagsModal()}

        <View
          style={
            Platform.OS === 'ios'
              ? [styles.header, {paddingVertical: 15}]
              : styles.header
          }>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              style={{
                width: 40,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => this.props.navigation.navigate('DrawerScreen')}>
              <Icon
                name="Arrow-Left"
                size={15}
                color="#91B3A2"
                style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
              />
            </TouchableOpacity>
            <Text
              style={[
                typography.H3,
                {
                  color: COLORS.dark_800,
                  fontSize: 18,
                  marginLeft: 10,
                },
              ]}>
              DRAFTS
            </Text>
          </View>
        </View>

        {/*********** Header ends ***********/}

        <View
          style={{
            alignSelf: 'center',
            flexDirection: 'row',
            backgroundColor: COLORS.altgreen_t50,
            width: 320,
            marginTop: 20,
            marginBottom: 4,
            justifyContent: 'center',
            borderRadius: 4,
          }}>
          <TouchableOpacity
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor: '#fff',
                width: '33.33%',
                borderTopLeftRadius: 4,
                borderBottomLeftRadius: 4,
              },
            ]}>
            <Text style={[typography.Caption, {color: COLORS.dark_800}]}>
              Forum/Poll
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Projects')}
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor: COLORS.altgreen_t50,
                width: '33.33%',
                borderRadius: 4,
              },
            ]}>
            <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
              Projects
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Blogs')}
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor: COLORS.altgreen_t50,
                width: '33.33%',
                borderRadius: 4,
              },
            ]}>
            <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
              Blogs
            </Text>
          </TouchableOpacity>
        </View>

        {/*********** Forum ItemList starts ***********/}

        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 20}}
          data={this.state.forumPollData}
          keyExtractor={(item) => item.id}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={1}
          renderItem={(item) => this.renderPollItem(item)}
        />

        {/*********** Forum ItemList ends ***********/}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  rich: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: '#e3e3e3',
    minHeight: 300,
  },
  flatStyle: {
    paddingHorizontal: 12,
  },
  richBar: {
    borderColor: '#efefef',
    borderTopWidth: StyleSheet.hairlineWidth,
  },
  richBarDark: {
    backgroundColor: '#191d20',
    borderColor: '#696969',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  forumItemView: {
    width: '90%',
    marginTop: 15,
    backgroundColor: COLORS.white,
    borderRadius: 4,
    alignSelf: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  submitBtn: {
    width: 90,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primarydark,
    borderRadius: 4,
    marginVertical: 10,
  },
  pollItem: {
    maxWidth: '100%',
    minHeight: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f2f3f4',
    borderRadius: 4,
    marginBottom: 10,
    padding: 8,
  },
  votedPollItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    maxWidth: '100%',
    minHeight: 30,
    alignItems: 'center',
    backgroundColor: '#f2f3f4',
    borderRadius: 4,
    marginBottom: 10,
  },
  progressView: {
    maxWidth: '100%',
    minHeight: 30,
    borderRadius: 4,
    marginBottom: 10,
    position: 'absolute',
    left: 0,
    zIndex: 1,
  },
});

const styles2 = StyleSheet.create({
  p: {
    color: 'rgb(67, 69, 74)',
    fontSize: 14,
    marginTop: 4,
  },
  span: {
    color: 'rgb(67, 69, 74)',
    fontSize: 14,
    marginTop: 4,
  },
  div: {
    color: 'rgb(67, 69, 74)',
    fontSize: 14,
    marginTop: 4,
  },
});

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ForumAndPoll);
