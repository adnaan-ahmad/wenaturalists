import React, { Component } from 'react'
import { Alert, Linking, Clipboard, Share, Dimensions, Modal, ImageBackground, View, Text, TouchableOpacity, StyleSheet, ScrollView, TouchableHighlight, FlatList, Image, SafeAreaView, Platform } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import messaging from '@react-native-firebase/messaging'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import { connect } from 'react-redux'
import Video from 'react-native-video'
import VideoPlayer from 'react-native-video-controls'
import LinearGradient from 'react-native-linear-gradient'
import Snackbar from 'react-native-snackbar'
import RNUrlPreview from 'react-native-url-preview'
import Autolink from 'react-native-autolink'

import ImagePicker from 'react-native-image-crop-picker'
import { TextInput } from 'react-native-paper'
import DatePicker from 'react-native-datepicker'
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader'
import projectDefault from '../../../../assets/project-default.jpg'
import typography from '../../../Components/Shared/Typography'
import { personalProfileRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions'
import { feedsPhotosRequest, feedsVideosRequest } from '../../../services/Redux/Actions/User/FeedsActions'
import { REACT_APP_userServiceURL, REACT_APP_domainUrl } from '../../../../env.json'
import SearchBar from '../../../Components/User/SearchBar'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import httpService from '../../../services/AxiosInterceptors'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultProfile from '../../../../assets/defaultProfile.png'
import goToTop from '../../../../assets/GoToTop.svg'
import defaultStyle from '../../../Components/Shared/Typography'
import defaultCover from '../../../../assets/defaultCover.png'
import postIcon from '../../../../assets/Post_Icon.png'
import blogIcon from '../../../../assets/Blog_Icon.png'
import linkIcon from '../../../../assets/Link_Icon.png'

httpService.setupInterceptors()
const screenHeight = Dimensions.get('window').height
const screenWidth = Dimensions.get('window').width
const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const moment = require('moment')
const regexp = new RegExp('^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator

class AwardsEdit extends Component {

    constructor(props) {
        super(props)
        this.state = {
            selected: 'LATEST',
            userId: '',
            notification: false,
            redirectToProfile: false,
            pageNumber: 0,
            pageSize: 30,
            postModalOpen: false,
            optionsModalOpen: false,
            headerOptionsModalOpen: false,
            shareModalOpen: false,
            likeModalOpen: false,
            readMore: false,
            currentScrollPosition: 0,
            hideControls: false,
            pressedActivityId: '',
            peopleLiked: [],
            activitiesData: [],
            videoHeight: 190,
            videoWidth: '97%',
            shareModal2Open: false,
            contactInfoModalOpen: false,
            awardList: [],
            pressedId: '',
            addAwardsModalOpen: false,
            awardTitle: '',
            thumbnail: {},
            issuer: '',
            credentialId: '',
            credentialVerificationLink: '',
            briefDescription: '',
            startDate: 0,
            endDate: 0,
            lifetimeValidity: false,
            isEditAward: false,
            dataId: '',
            attachmentFile: []
        }
    }

    componentDidMount() {
        // this.props.route.params ? 
        // console.log('================================ this.props.route.params.id() =======================',this.props.route.params.id)
        // : null

        // AsyncStorage.getItem('refreshToken').then((value) => console.log('refreshToken from Feeds.js : ', value))

        AsyncStorage.getItem("userId").then((value) => {
            // this.props.feedsPhotosRequest({ userId: value, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })

            this.props.route.params && this.props.route.params.id !== '' ?
                this.props.personalProfileRequest({ userId: this.props.route.params.id, otherUserId: value })
                : this.props.personalProfileRequest({ userId: value, otherUserId: '' })

            this.setState({ userId: value })
            this.getUserActivitiesPosts()
            this.getAllAwardList(value)
        }).catch((e) => {
            console.log(e)
        })

        // this.props.userFeedsPhotos.body.content ? 
        // this.setState({ likedPosts: this.props.userFeedsPhotos.body.content.filter((item) => item.liked) }) 
        // : null

        try {
            messaging().onNotificationOpenedApp(remoteMessage => {
                if (remoteMessage) {

                    // console.log('remoteMessage :', remoteMessage)
                    console.log(
                        'Notification caused app to open from background state:',
                        remoteMessage.data.url,
                        remoteMessage.data.userId
                    )
                }
            })
        }
        catch (error) {
            console.log(error)
        }

        try {
            messaging()
                .getInitialNotification()
                .then(remoteMessage => {
                    if (remoteMessage) {

                        // console.log('remoteMessage :', remoteMessage)
                        console.log(
                            'Notification caused app to open from quit state:',
                            remoteMessage.data.url,
                            remoteMessage.data.userId
                        )
                    }
                })
        }
        catch (error) {
            console.log(error)
        }

    }

    getAllAwardList(userId) {
        let entityId = this.state.awardEntityId;
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/award/list?entityId=' + userId,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.message === 'Success!') {
                // console.log('getAllAwardList', response.data.body)
                this.setState({
                    awardList: response.data.body
                })
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    onShare = async () => {
        try {
            const result = await Share.share({
                message:
                    REACT_APP_domainUrl + '/post/' + this.state.pressedActivityId,
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    console.log('shared with activity type of result.activityType')
                } else {
                    console.log('shared')
                }
            } else if (result.action === Share.dismissedAction) {
                console.log('dismissed')
            }
        } catch (error) {
            console.log(error.message)
        }
    };

    handleLoadMore = () => {
        console.log("handle load more called")
        this.getUserActivitiesPosts()
        // this.props.feedsPhotosRequest({
        //     userId: this.state.userId,
        //     type: this.state.selected,
        //     pageNumber: this.state.pageNumber,
        //     size: this.state.pageSize + 30
        // })

        this.setState({ pageSize: this.state.pageSize + 30 })
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     if (nextProps.user !== this.props.user) {
    //         return true
    //     }
    //     if (nextProps.userFeedsPhotos !== this.props.userFeedsPhotos) {
    //         return true
    //     }
    //     if (nextProps.userFeedsVideos !== this.props.userFeedsVideos) {
    //         return true
    //     }

    //     if (nextState.contactInfoModalOpen !== this.state.contactInfoModalOpen) {
    //         return true
    //     }
    //     if (nextState.shareModal2Open !== this.state.shareModal2Open) {
    //         return true
    //     }
    //     if (nextState.pageNumber !== this.state.pageNumber) {
    //         return true
    //     }
    //     if (nextState.pageSize !== this.state.pageSize) {
    //         return true
    //     }
    //     if (nextState.userId !== this.state.userId) {
    //         return true
    //     }
    //     if (nextState.notification !== this.state.notification) {
    //         return true
    //     }
    //     if (nextState.redirectToProfile !== this.state.redirectToProfile) {
    //         return true
    //     }
    //     if (nextState.selected !== this.state.selected) {
    //         return true
    //     }
    //     if (nextState.postModalOpen !== this.state.postModalOpen) {
    //         return true
    //     }
    //     if (nextState.readMore !== this.state.readMore) {
    //         return true
    //     }
    //     if (nextState.currentScrollPosition !== this.state.currentScrollPosition) {
    //         return true
    //     }
    //     if (nextState.hideControls !== this.state.hideControls) {
    //         return true
    //     }
    //     if (nextState.pressedActivityId !== this.state.pressedActivityId) {
    //         return true
    //     }
    //     if (nextState.optionsModalOpen !== this.state.optionsModalOpen) {
    //         return true
    //     }
    //     if (nextState.shareModalOpen !== this.state.shareModalOpen) {
    //         return true
    //     }
    //     if (nextState.likeModalOpen !== this.state.likeModalOpen) {
    //         return true
    //     }
    //     if (nextState.peopleLiked !== this.state.peopleLiked) {
    //         return true
    //     }
    //     if (nextState.videoHeight !== this.state.videoHeight) {
    //         return true
    //     }
    //     if (nextState.videoWidth !== this.state.videoWidth) {
    //         return true
    //     }
    //     if (nextState.headerOptionsModalOpen !== this.state.headerOptionsModalOpen) {
    //         return true
    //     }
    //     // if (nextState.likedPosts !== this.state.likedPosts) {
    //     //   return true
    //     // }
    //     if (nextState.activitiesData !== this.state.activitiesData) {
    //         return true
    //     }
    //     if (nextState.selected !== this.state.selected) {
    //         return true
    //     }
    //     return false
    // }

    componentDidUpdate(prevProps, prevState) {

        // if (prevState.likedPosts !== this.state.likedPosts) {

        // }

        if (this.state.notification) {

            this.setState({ notification: false }, () => this.props.navigation.navigate('Notification'))
        }
        if (this.state.redirectToProfile) {

            this.setState({ redirectToProfile: false }, () => this.props.navigation.navigate('ProfileStack'))
        }

    }

    changeState = (value) => {
        this.setState(value)
    }

    renderHeader = (item) => {
        return (
            <View style={styles.unreadNotiItem}>

                <View style={{ width: '40%', flexDirection: 'row' }}>

                    {item.originalProfileImage ?
                        <Image style={[defaultShape.Media_Round, {}]}
                            source={{ uri: item.originalProfileImage }} />

                        :
                        <Image style={[defaultShape.Media_Round, { backgroundColor: 'orange' }]}
                            source={defaultProfile} />}


                    <View style={{ alignItems: 'center' }}>

                        <Text style={[defaultStyle.Title_2, { color: COLORS.dark_800, marginLeft: 8 }]}>{item.userName.split(' ').slice(0, 2).join(' ')}</Text>

                        <Text style={[{ color: COLORS.altgreen_300, marginLeft: 2, fontSize: 10, fontFamily: 'Montserrat-Medium' }]}>{this.unixTime2(item.createTime)}</Text>
                    </View>
                </View>

                <TouchableOpacity activeOpacity={0.5} style={[defaultShape.Nav_Gylph_Btn, {}]}
                    onPress={() => this.setState({ optionsModalOpen: true, pressedActivityId: item.id })}>
                    <Icon name="Kebab" color={COLORS.altgreen_300} size={14} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                </TouchableOpacity>

            </View>
        )
    }

    renderSharedHeader = (item) => {
        return (
            <View style={styles.unreadNotiItem}>

                {item.username ?
                    <View style={{ width: '40%', flexDirection: 'row' }}>

                        {item.profileImage ?
                            <Image style={[defaultShape.Media_Round, {}]}
                                source={{ uri: item.profileImage }} />

                            :
                            <Image style={[defaultShape.Media_Round]}
                                source={defaultProfile} />}


                        <View style={{ alignItems: 'center' }}>


                            <Text style={[defaultStyle.Title_2, { color: COLORS.dark_800, marginLeft: 8 }]}>{item.username.split(' ').slice(0, 2).join(' ')}</Text>


                            {this.unixTime(item.createTime) !== NaN + ' ' + undefined + ' ' + NaN ?
                                <Text style={[{ color: COLORS.altgreen_300, marginLeft: 2, fontSize: 10, fontFamily: 'Montserrat-Medium' }]}>{this.unixTime(item.createTime)}</Text>
                                : <></>}
                        </View>
                    </View>
                    :
                    <View style={{}}>


                        <Text style={[defaultStyle.Title_2, { color: COLORS.dark_800, marginLeft: 2 }]}>{item.title}</Text>
                        {item.location ? <Text style={[{ color: COLORS.altgreen_300, marginLeft: 2, fontSize: 10, fontFamily: 'Montserrat-Medium' }]}>{item.location.city}, {item.location.state}, {item.location.country}</Text> : <></>}

                    </View>
                }

                {/* <TouchableOpacity activeOpacity={0.5} style={[defaultShape.Nav_Gylph_Btn, {}]}
          onPress={() => this.setState({ optionsModalOpen: true, pressedActivityId: item.id })}>
          <Icon name="Kebab" color={COLORS.altgreen_300} size={14} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
        </TouchableOpacity> */}

            </View>
        )
    }

    renderFooter = (item) => {
        return (
            <View style={[styles.unreadNotiItem, { paddingRight: 26 }]}>

                <View style={{ flexDirection: 'row' }}>

                    <TouchableOpacity onPress={() => this.setState({ pressedActivityId: item.id }, this.handleLike(item.id, item.liked))}
                        activeOpacity={0.5} style={{ alignItems: 'center', flexDirection: 'row' }}>
                        <Icon name={item.liked ? "Like_FL" : "Like"} color={COLORS.green_500} size={14} style={{ marginTop: Platform.OS === 'android' ? 10 : 0, marginHorizontal: 6 }} />
                        <Text style={[{ color: COLORS.altgreen_300 }, defaultStyle.Caption]}>{item.likesCount}</Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={() => this.props.navigation.navigate('IndividualFeedsPost', { id: item.id })} activeOpacity={0.5} style={{ alignItems: 'center', flexDirection: 'row', marginLeft: 12 }}>
                        <Icon name="Comment" color={COLORS.green_500} size={14} style={{ marginTop: Platform.OS === 'android' ? 10 : 0, marginRight: 6 }} />
                        <Text style={[{ color: COLORS.altgreen_300 }, defaultStyle.Caption]}>{item.commentCount}</Text>
                    </TouchableOpacity>
                </View>

                {/* <TouchableOpacity onPress={() => this.setState({ shareModalOpen: true })} activeOpacity={0.5} style={[defaultShape.Nav_Gylph_Btn, { flexDirection: 'row' }]}>
          <Text style={[defaultStyle.Caption, { color: COLORS.grey_350, marginLeft: 8 }]}>Share</Text>
          <Icon name="Share" color={COLORS.altgreen_300} size={14} style={{ marginTop: Platform.OS === 'android' ? 10 : 0, marginLeft: 6 }} />
        </TouchableOpacity> */}

            </View>
        )
    }


    getUsersWhoLiked = () => {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/like/getUsers/' + this.state.pressedActivityId + '?userId=' + this.state.userId
                + "&page=" + 0 + "&size=" + 10,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.body && response.status === 200) {
                // console.log('%%%%%%%%%%%%%%%% response.data.body %%%%%%%%%%%%%%%%%%%', response.data.body)
                this.setState({ peopleLiked: response.data.body.content })
            }
        }).catch((err) => {
            console.log(err)
        })
    }


    handleLike = (activityId, liked) => {
        // let newState = !this.state.liked;
        // let likesCount = this.state.likesCount;

        // this.setState({
        //     'liked': newState,
        //     'likesCount': newState ? likesCount + 1 : Math.max(0, likesCount - 1),
        //     'likeSuccess': false
        // })

        const data = {
            userId: this.state.userId,
            activityId: activityId,
            liked: !liked
        }

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/like/create/',
            data: data,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.message === 'Success!') {
                this.getUserActivitiesPosts()
                // this.setState({ ...this.state })
                // this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                // this.setState({
                //   'likeSuccess': true,
                //   'likesCount': newState ? likesCount + 1 : Math.max(0, likesCount - 1)
                // })
            } else {
                // this.setState({ 'liked': !newState })
            }
        }).catch((err) => {
            // this.setState({ 'liked': !newState, 'likeSuccess': true })
            console.log(err);
        })
    }


    unixTime = (UNIX_timestamp) => {
        var date = new Date(UNIX_timestamp)
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        var year = date.getFullYear()
        var month = months[date.getMonth()]
        var day = date.getDate()
        return day + ' ' + month + ' ' + year
    }

    unixTime2 = (UNIX_timestamp) => {
        var date = new Date(UNIX_timestamp)
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        var year = date.getFullYear()
        var month = months[date.getMonth()]
        var day = date.getDate()
        var time = date.getTime()

        var todaysDate = new Date()
        var time2 = todaysDate.getTime()

        var difference = Math.floor((time2 - time) / 1000)

        if (difference < 60) {
            return difference + ' s'
        }

        if (difference > 59 && difference < 3600) {
            return Math.floor(difference / 60) + ' m'
        }

        if (difference >= 3600 && difference < 86400) {
            return Math.floor(difference / 3600) + ' h'
        }

        if (difference >= 86400 && difference < 864000) {
            return Math.floor(difference / 72000) + ' d'
        }

        if (difference >= 864000) {
            return day + ' ' + month + ' ' + year
        }
    }

    renderImage = (item) => {

        // if (this.state.selected === 'PHOTOS') {

        if (item.attachmentIds.length > 5) {
            return (
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('IndividualFeedsPost', { id: item.id })}>

                    <View style={{ flexDirection: 'row', marginLeft: -10 }}>
                        <Image style={{ height: 182, width: '50%' }} source={{ uri: item.attachmentIds[0].attachmentUrl }} />
                        <Image style={{ height: 182, width: '50%' }} source={{ uri: item.attachmentIds[1].attachmentUrl }} />
                    </View>

                    <View style={{ flexDirection: 'row', marginLeft: -10 }}>
                        <Image style={{ height: 120, width: '33.33%' }} source={{ uri: item.attachmentIds[2].attachmentUrl }} />
                        <Image style={{ height: 120, width: '33.33%' }} source={{ uri: item.attachmentIds[3].attachmentUrl }} />

                        <ImageBackground source={{ uri: item.attachmentIds[4].attachmentUrl }}
                            imageStyle={{}}
                            style={{ height: 120, width: '57.8%' }} >
                            <View style={{ height: 120, width: '57.8%', backgroundColor: COLORS.dark_900 + 'BF', opacity: 0.95, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 28, color: COLORS.white }}>+{item.attachmentIds.length - 4}</Text>
                            </View>
                        </ImageBackground>

                    </View>
                </TouchableOpacity>
            )
        }

        else if (item.attachmentIds.length === 5) {
            return (
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('IndividualFeedsPost', { id: item.id })}>

                    <View style={{ flexDirection: 'row', marginLeft: -10 }}>
                        <Image style={{ height: 182, width: '50%' }} source={{ uri: item.attachmentIds[0].attachmentUrl }} />
                        <Image style={{ height: 182, width: '50%' }} source={{ uri: item.attachmentIds[1].attachmentUrl }} />
                    </View>

                    <View style={{ flexDirection: 'row', marginLeft: -10 }}>
                        <Image style={{ height: 120, width: '33.33%' }} source={{ uri: item.attachmentIds[2].attachmentUrl }} />
                        <Image style={{ height: 120, width: '33.33%' }} source={{ uri: item.attachmentIds[3].attachmentUrl }} />
                        <Image style={{ height: 120, width: '33.33%' }} source={{ uri: item.attachmentIds[4].attachmentUrl }} />

                    </View>
                </TouchableOpacity>
            )
        }

        else if (item.attachmentIds.length === 4) {
            return (
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('IndividualFeedsPost', { id: item.id })}>

                    <View style={{ flexDirection: 'row', marginLeft: -10 }}>
                        <Image style={{ height: 151, width: '50%' }} source={{ uri: item.attachmentIds[0].attachmentUrl }} />
                        <Image style={{ height: 151, width: '50%' }} source={{ uri: item.attachmentIds[1].attachmentUrl }} />
                    </View>

                    <View style={{ flexDirection: 'row', marginLeft: -10 }}>
                        <Image style={{ height: 151, width: '50%' }} source={{ uri: item.attachmentIds[2].attachmentUrl }} />
                        <Image style={{ height: 151, width: '50%' }} source={{ uri: item.attachmentIds[3].attachmentUrl }} />
                    </View>

                </TouchableOpacity>
            )
        }

        else if (item.attachmentIds.length === 3) {
            return (
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('IndividualFeedsPost', { id: item.id })}>

                    <View style={{ flexDirection: 'row', marginLeft: -10 }}>
                        <Image style={{ height: 151, width: '100%' }} source={{ uri: item.attachmentIds[0].attachmentUrl }} />

                    </View>

                    <View style={{ flexDirection: 'row', marginLeft: -10 }}>
                        <Image style={{ height: 151, width: '50%' }} source={{ uri: item.attachmentIds[1].attachmentUrl }} />
                        <Image style={{ height: 151, width: '50%' }} source={{ uri: item.attachmentIds[2].attachmentUrl }} />
                    </View>

                </TouchableOpacity>
            )
        }

        else if (item.attachmentIds.length === 2) {
            return (
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('IndividualFeedsPost', { id: item.id })}>

                    <View style={{ flexDirection: 'row', marginLeft: -10 }}>
                        <Image style={{ height: 151, width: '100%' }} source={{ uri: item.attachmentIds[0].attachmentUrl }} />

                    </View>

                    <View style={{ flexDirection: 'row', marginLeft: -10 }}>
                        <Image style={{ height: 151, width: '100%' }} source={{ uri: item.attachmentIds[1].attachmentUrl }} />

                    </View>

                </TouchableOpacity>
            )
        }

        else if (item.attachmentIds.length === 1) {
            return (
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('IndividualFeedsPost', { id: item.id })}>
                    {/* {this.state.selected === 'AUDIO' && item.attachmentIds[0].attachmentUrl || this.state.selected === 'VIDEOS' ? */}
                    {item.attachmentIds[0].attachmentType === 'AUDIO' && this.state.selected !== 'VIDEOS' ?

                        <VideoPlayer
                            style={{ width: '97%', height: 190 }}
                            // disableBack={true}
                            tapAnywhereToPause={true}
                            // onBack={() => this.setState({ hideControls: !this.state.hideControls })}
                            // onError={() => this.setState({ showVideo: false })}
                            disableFullscreen={true}
                            // disablePlayPause={true}
                            disableSeekbar={true}
                            disableVolume={true}
                            disableTimer={true}
                            disableBack={true}

                            paused={true}
                            // source={{ uri: item.attachmentIds[0].attachmentUrl }}
                            source={{ uri: 'https://dscovr-assets-dev.s3.amazonaws.com/posts/1603347470339-file_example_OGG_1920_13_3mg.ogg' }}
                            navigator={this.props.navigator}
                        />
                        :

                        item.attachmentIds[0].attachmentType === 'VIDEO' && this.state.selected !== 'AUDIO' ?

                            <VideoPlayer
                                style={{
                                    width: this.state.videoWidth, height: this.state.videoHeight,
                                    // position: "absolute",
                                    // top: 0,
                                    // left: 0,
                                    // bottom: 0,
                                    // right: 0,
                                }}
                                // disableBack={true}
                                tapAnywhereToPause={true}
                                // onBack={() => this.setState({ hideControls: !this.state.hideControls })}
                                // onError={() => this.setState({ showVideo: false })}
                                // disableFullscreen={true}
                                // disablePlayPause={true}
                                disableSeekbar={true}
                                disableVolume={true}
                                disableTimer={true}
                                disableBack={true}
                                // onEnterFullscreen={() => this.setState({ videoHeight: screenHeight, videoWidth: screenWidth })}
                                // onExitFullscreen={() => this.setState({ videoHeight: 190, videoWidth: '97%' })}
                                paused={true}
                                source={{ uri: item.attachmentIds[0].attachmentUrl }}
                                navigator={this.props.navigator}
                            // fullscreen={true}
                            // resizeMode="cover"
                            />
                            :
                            <Image style={{ height: 345, marginLeft: -10 }} source={{ uri: item.attachmentIds[0].attachmentUrl }} />
                    }
                </TouchableOpacity>
            )
        }
        // }

        // else if(this.state.selected === 'VIDEOS' && this.props.userFeedsPhotos.body) {
        //   return (
        //     <Video style={{ width: 300, height: 200 }} 
        //     paused={true}
        //     source={{ uri: item.attachmentIds[0].attachmentUrl }} />
        //   )
        // }

        else return <></>
    }


    postModal = () => {
        return (
            <Modal
                visible={this.state.postModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View style={[defaultShape.Linear_Gradient_View, { bottom: 100 }]}>
                        <LinearGradient
                            colors={[
                                COLORS.dark_800 + '00',
                                COLORS.dark_800 + 'CC',
                                COLORS.dark_800,
                            ]}
                            style={defaultShape.Linear_Gradient}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ postModalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color={COLORS.dark_600}
                            style={styles.crossIcon}
                        />
                    </TouchableOpacity>

                    <View
                        style={[
                            defaultShape.Modal_Categories_Container,
                            {
                                paddingTop: 10,
                                height: 140,
                                flexDirection: 'row',
                                justifyContent: 'space-evenly',
                            },
                        ]}>
                        <TouchableOpacity
                            style={{ justifyContent: 'center', alignItems: 'center' }}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({ postModalOpen: false }, () =>
                                    this.props.navigation.navigate('NewFeedsPost', { link: false }),
                                );
                            }}>
                            {/* <Icon name="Add_Post" size={44} color={COLORS.primarygreen} style={{ alignSelf: 'center' }} /> */}
                            <Image style={{ alignSelf: 'center' }} source={postIcon} />
                            <Text
                                style={[
                                    typography.Button_Lead,
                                    {
                                        color: COLORS.dark_800,
                                        textAlign: 'center',
                                        alignSelf: 'center',
                                        position: 'absolute',
                                        top: 50,
                                    },
                                ]}>
                                Post
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{ justifyContent: 'center', alignItems: 'center' }}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({ postModalOpen: false }, () =>
                                    this.props.navigation.navigate('NewBlogPost'),
                                );
                            }}>
                            {/* <Icon name='Blog' size={44} color={COLORS.primarygreen} /> */}
                            <Image style={{ alignSelf: 'center' }} source={blogIcon} />
                            <Text
                                style={[
                                    typography.Button_Lead,
                                    {
                                        color: COLORS.dark_800,
                                        textAlign: 'center',
                                        position: 'absolute',
                                        top: 47,
                                    },
                                ]}>
                                Blog
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{ justifyContent: 'center', alignItems: 'center' }}
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({ postModalOpen: false }, () =>
                                    this.props.navigation.navigate('NewFeedsPost', { link: true }),
                                );
                            }}>
                            {/* <Icon name='Link_Post' size={44} color={COLORS.primarygreen} /> */}
                            <Image style={{ alignSelf: 'center' }} source={linkIcon} />
                            <Text
                                style={[
                                    typography.Button_Lead,
                                    {
                                        color: COLORS.dark_800,
                                        textAlign: 'center',
                                        position: 'absolute',
                                        top: 50,
                                    },
                                ]}>
                                Link
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }

    headerOptionsModal = () => {
        return (

            <Modal visible={this.state.headerOptionsModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ headerOptionsModalOpen: false })} >
                        <Icon name='Cross' size={13} color='#367681' style={styles.crossIcon} />
                    </TouchableOpacity>

                    {this.props.route.params && this.props.route.params.id !== '' ?
                        <View style={defaultShape.Modal_Categories_Container}>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal} activeOpacity={0.6} onPress={() => this.setState({ headerOptionsModalOpen: false, contactInfoModalOpen: true })} >
                                <Icon name="Email_At" size={16} color="#154A59" style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                                <Text style={styles.modalText}>View Contact Info</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal, { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' }]} activeOpacity={0.6} onPress={() => { this.setState({ headerOptionsModalOpen: false }) }}>
                                <Icon name='Feedback' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 10 } : {}} />
                                <Text style={styles.modalText}>Endorse</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [styles.termsmodal, { paddingTop: 25, paddingBottom: 15 }] : styles.termsmodal} activeOpacity={0.6} onPress={() => { this.setState({ headerOptionsModalOpen: false }) }}>
                                <Icon name='Caution' size={16} color='#913838' style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                                <Text style={styles.modalText}>Report User</Text>
                            </TouchableOpacity>

                        </View>
                        :
                        <View style={[defaultShape.Modal_Categories_Container, { justifyContent: 'space-evenly' }]}>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal} activeOpacity={0.6} onPress={() => { this.setState({ headerOptionsModalOpen: false, shareModal2Open: true }) }} >
                                <Icon name="Share" size={16} color="#154A59" style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                                <Text style={styles.modalText}>Share Profile Page</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={[Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal, { borderBottomWidth: 0.55, borderBottomColor: '#E8ECEB' }]} activeOpacity={0.6} onPress={() => { this.setState({ headerOptionsModalOpen: false, contactInfoModalOpen: true }) }}>
                                <Icon name='Email_At' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                                <Text style={styles.modalText}>View Contact Info</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [styles.termsmodal, { paddingTop: 25, paddingBottom: 15 }] : styles.termsmodal} activeOpacity={0.6} onPress={() => { this.setState({ headerOptionsModalOpen: false }) }}>
                                <Icon name='EditBox' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                                <Text style={styles.modalText}>Edit Profile</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal} activeOpacity={0.6} onPress={() => { this.setState({ headerOptionsModalOpen: false }), this.props.navigation.navigate('ReorderProfileScreen') }}>
                                <Icon name='TxEdi_Bullet' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                                <Text style={styles.modalText}>Reorder Profile page</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal} activeOpacity={0.6} onPress={() => { this.setState({ headerOptionsModalOpen: false }) }}>
                                <Icon name='Setting' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                                <Text style={styles.modalText}>Privacy & Settings</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [styles.termsmodal, { paddingVertical: 15 }] : styles.termsmodal} activeOpacity={0.6} onPress={() => { this.setState({ headerOptionsModalOpen: false }) }}>
                                <Icon name='Help' size={16} color='#154A59' style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                                <Text style={styles.modalText}>Get Supports</Text>
                            </TouchableOpacity>

                        </View>
                    }
                </View>

            </Modal>

        )
    }

    handleHideModal = (id) => {
        let data = {
            userId: this.state.userId,
            activityId: id,
            entityType: "POST",
        }

        axios({
            method: "post",
            url: REACT_APP_userServiceURL + "/backend/hidden/hide",
            data: data,
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.status === 201) {
                    // console.log(response.status)
                }
            })
            .catch((err) => {
                if (err && err.response && err.response.status === 409) {
                    // console.log(err.response.status)
                }
            })
        this.setState({ optionsModalOpen: false, activitiesData: this.state.activitiesData.filter((item) => item.id !== this.state.pressedActivityId) })
    }

    optionsModal = () => {
        return (

            <Modal visible={this.state.optionsModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ optionsModalOpen: false })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
                    </TouchableOpacity>

                    {this.props.route.params && this.props.route.params.id !== '' ?

                        <View style={defaultShape.Modal_Categories_Container}>

                            <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
                                <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>Post</Text>
                            </View>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : defaultShape.ActList_Cell_Gylph_Alt} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false, likeModalOpen: true }, () => this.getUsersWhoLiked()) }} >

                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name='Like_FL' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 9 } : {}} />
                                    <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>See who liked it</Text>
                                </View>
                                <Icon name="Arrow_Right" size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                            </TouchableOpacity>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : defaultShape.ActList_Cell_Gylph_Alt} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false, shareModalOpen: true }) }} >

                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name='Share' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                                    <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Share</Text>
                                </View>
                                <Icon name='Arrow_Right' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    Platform.OS === 'ios'
                                        ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                        : [
                                            defaultShape.ActList_Cell_Gylph_Alt,
                                        ]
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.handleHideModal(this.state.pressedActivityId)
                                }}>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                                    Hide
                                </Text>
                                <Icon
                                    name="Hide"
                                    size={17}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                            </TouchableOpacity>



                            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false }) }}>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Don't show me posts like this</Text>
                                <Icon name='Hide' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                            </TouchableOpacity>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]} activeOpacity={0.6} onPress={() => this.setState({ reasonForReportingModalOpen: true })}>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Report Post</Text>
                                <Icon name='ReportComment_OL' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                            </TouchableOpacity>

                        </View>

                        :

                        <View style={defaultShape.Modal_Categories_Container}>

                            <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
                                <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>Post</Text>
                            </View>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : defaultShape.ActList_Cell_Gylph_Alt} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false, likeModalOpen: true }, () => this.getUsersWhoLiked()) }} >

                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name='Like_FL' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 9 } : {}} />
                                    <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>See who liked it</Text>
                                </View>
                                <Icon name="Arrow_Right" size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                            </TouchableOpacity>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : defaultShape.ActList_Cell_Gylph_Alt} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false, shareModalOpen: true }) }} >

                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name='Share' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />
                                    <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600, marginLeft: 8 }]}>Share</Text>
                                </View>
                                <Icon name='Arrow_Right' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                            </TouchableOpacity>

                            <TouchableOpacity
                                style={
                                    Platform.OS === 'ios'
                                        ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                        : [
                                            defaultShape.ActList_Cell_Gylph_Alt,
                                        ]
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.handleHideModal(this.state.pressedActivityId)
                                }}>
                                <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                                    Hide
                                </Text>
                                <Icon
                                    name="Hide"
                                    size={17}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                            </TouchableOpacity>

                            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]} activeOpacity={0.6} onPress={() => { this.setState({ optionsModalOpen: false }) }}>
                                <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>Edit Post</Text>
                                <Icon name='EditBox' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.handleDeleteSubmit()}
                                style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]} activeOpacity={0.6} >
                                <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>Delete Post</Text>
                                <Icon name='TrashBin' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                            </TouchableOpacity>

                        </View>
                    }
                </View>

            </Modal>

        )
    }


    likeModal = () => {
        return (

            <Modal visible={this.state.likeModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto', height: '80%', position: 'absolute', top: 360, width: '100%' }}>

                    <View style={[defaultShape.Linear_Gradient_View, { bottom: 300 }]}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ likeModalOpen: false, peopleLiked: [] })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
                    </TouchableOpacity>

                    <View style={defaultShape.Modal_Categories_Container}>

                        <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
                            <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>Who liked this post</Text>
                        </View>

                        <FlatList
                            keyboardShouldPersistTaps='handled'
                            showsVerticalScrollIndicator={false}
                            // contentContainerStyle={{ justifyContent: 'flex-start' }}
                            style={{ height: '60%', width: '100%', marginTop: 6 }}
                            keyExtractor={(item) => item.userId}
                            data={this.state.peopleLiked}
                            // initialNumToRender={10}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity activeOpacity={0.6} style={{ flexDirection: 'row', height: 44, alignItems: 'center' }}>
                                    <Image source={item.userProfileImage ? { uri: item.userProfileImage } : defaultProfile} style={{ marginLeft: '7%', marginRight: 10, width: 32, height: 32, borderRadius: 16 }} />
                                    <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>{item.userName}</Text>
                                </TouchableOpacity>
                            )}
                        />

                    </View>

                </View>

            </Modal>

        )
    }

    onShare2 = async () => {
        try {
            const result = await Share.share({
                message:
                    REACT_APP_domainUrl + '/profile/' + this.props.user.body.customUrl,
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    console.log('shared with activity type of result.activityType')
                } else {
                    console.log('shared')
                }
            } else if (result.action === Share.dismissedAction) {
                console.log('dismissed')
            }
        } catch (error) {
            console.log(error.message)
        }
    }

    shareModal2 = () => {
        return (

            <Modal visible={this.state.shareModal2Open} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ shareModal2Open: false })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
                    </TouchableOpacity>

                    <View style={defaultShape.Modal_Categories_Container}>

                        <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Share</Text>
                        </View>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15 }] : defaultShape.ActList_Cell_Gylph_Alt} activeOpacity={0.6} onPress={() => { this.setState({ shareModal2Open: false }) }}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Repost on WeNaturalists</Text>
                            <Icon name='Forward' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => {

                            Clipboard.setString(REACT_APP_domainUrl + '/profile/' + this.props.user.body.customUrl)
                            Snackbar.show({
                                backgroundColor: '#97A600',
                                text: "Link Copied",
                                textColor: "#00394D",
                                duration: Snackbar.LENGTH_LONG,
                            })
                        }}
                            style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt]}
                            activeOpacity={0.6}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Copy link to post</Text>
                            <Icon name='TxEdi_AddLink' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt]} activeOpacity={0.6} onPress={() => { this.setState({ shareModal2Open: false }) }}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Share through Mail</Text>
                            <Icon name='Mail_OL' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => { this.setState({ shareModal2Open: false }, () => this.onShare2()) }}
                            style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]}
                            activeOpacity={0.6}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Share via others</Text>

                            <View style={[{ flexDirection: 'row', justifyContent: 'space-evenly', width: 100, marginRight: -6 }]}>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name="Social_FB" size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name='Social_Twitter' size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name='Social_LinkedIn' size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name="Meatballs" size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                            </View>

                        </TouchableOpacity>


                    </View>



                </View>

            </Modal>

        )
    }

    handleScroll = (event) => {
        // console.log(event.nativeEvent.contentOffset.y)
        this.setState({ currentScrollPosition: event.nativeEvent.contentOffset.y })
    }

    goToTop = () => {
        this.flatListRef.scrollToOffset({ animated: true, offset: 0 });
    }

    listHeaderComponent = () => {
        return (
            <View style={styles.container}>

                <View style={styles.feedDetails}>

                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} alwaysBounceHorizontal={false}>
                        <TouchableOpacity onPress={() => this.setState({ selected: 'LATEST' },
                            // () => this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                            () => this.getUserActivitiesPosts()
                        )}
                            style={this.state.selected === 'LATEST' ? styles.selected : styles.notSelected}>
                            <Text style={this.state.selected === 'LATEST' ? styles.selectedText : styles.notSelectedText}>Latest</Text>
                        </TouchableOpacity>

                        {/* <TouchableOpacity onPress={() => this.setState({ selected: 'TOP' },
                            // () => this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                            () => this.getUserActivitiesPosts()
                            )}
                            style={this.state.selected === 'TOP' ? styles.selected : styles.notSelected}>
                            <Text style={this.state.selected === 'TOP' ? styles.selectedText : styles.notSelectedText}>Trending</Text>
                        </TouchableOpacity> */}

                        <TouchableOpacity onPress={() => this.setState({ selected: 'PHOTOS' },
                            // () => this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                            () => this.getUserActivitiesPosts()
                        )}
                            style={this.state.selected === 'PHOTOS' ? styles.selected : styles.notSelected}>
                            <Text style={this.state.selected === 'PHOTOS' ? styles.selectedText : styles.notSelectedText}>Photos</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.setState({ selected: 'VIDEOS' },
                            // () => this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                            () => this.getUserActivitiesPosts()
                        )}
                            style={this.state.selected === 'VIDEOS' ? styles.selected : styles.notSelected}>
                            <Text style={this.state.selected === 'VIDEOS' ? styles.selectedText : styles.notSelectedText}>Videos</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.setState({ selected: 'ARTICLES' },
                            // () => this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                            () => this.getUserActivitiesPosts()
                        )}
                            style={this.state.selected === 'ARTICLES' ? styles.selected : styles.notSelected}>
                            <Text style={this.state.selected === 'ARTICLES' ? styles.selectedText : styles.notSelectedText}>Blogs</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.setState({ selected: 'AUDIO' },
                            // () => this.props.feedsPhotosRequest({ userId: this.state.userId, type: this.state.selected, pageNumber: this.state.pageNumber, size: this.state.pageSize })
                            () => this.getUserActivitiesPosts()
                        )}
                            style={this.state.selected === 'AUDIO' ? styles.selected : styles.notSelected}>
                            <Text style={this.state.selected === 'AUDIO' ? styles.selectedText : styles.notSelectedText}>Audio</Text>
                        </TouchableOpacity>

                    </ScrollView>
                </View>
            </View>
        )
    }

    trimDescription = (item) => {

        var trimmed = item.split('^^__').join(' ').indexOf('@@@__')

        var str = item.split('^^__').join(' ').indexOf(' ', item.split('^^__').join(' ').indexOf('@@@__'))

        var sub = item.substring(trimmed, str)

        item = item.replace(' ' + sub, '')
        item = item.replace('@@@^^^', ' ')
        item = item.replace('  ', '')

        item = item.replace(/&nbsp;/g, ' ')
        item = item.replace(/<br\s*[\/]?>/gi, '\n')

        const regex = /(<([^>]+)>)/ig
        item = item.replace(regex, '')

        return item.split('^^__').join(' ')
    }

    openWebsite = (website) => {

        if (website && website.includes('http')) return Linking.openURL(website)

        else if (website && !website.includes('http')) return Linking.openURL('https://' + website)

    }

    renderDescription = (item) => {

        return (
            <Autolink
                text={this.trimDescription(item.description)}
                email
                hashtag="instagram"
                mention="twitter"
                phone="sms"
                url
            />
        )
    }

    renderSharedPost = (item) => {
        return (
            <View style={styles.renderSharedPost}>
                {this.renderSharedHeader(item)}

                {item.description ? this.renderDescription(item)
                    : <View style={{ backgroundColor: 'transparent', height: 10 }}></View>}

                {item.postType === 'LINK' ? (
                    <RNUrlPreview
                        text={item.postLinkTypeUrl}
                        titleNumberOfLines={1}
                        titleStyle={[typography.Title_2, { color: COLORS.dark_800 }]}
                        descriptionStyle={[
                            typography.Note,
                            {
                                fontFamily: 'Montserrat-Medium',
                                color: COLORS.altgreen_300,
                                marginTop: 8,
                            },
                        ]}
                        containerStyle={{
                            marginTop: 15,
                            width: '98%',
                            flexDirection: 'column',
                            backgroundColor: COLORS.altgreen_100,
                        }}
                        imageStyle={{ width: '100%', height: 150 }}
                        imageProps={{ width: '100%', height: 150 }}
                    />
                ) : (
                    <></>
                )}

                <View style={{ width: '103%' }}>
                    {item.attachmentIds && item.attachmentIds.length ?
                        this.renderImage(item) : <></>}
                </View>

                {item.hashTags && item.hashTags.length ?

                    this.renderHashTags(item)

                    : <></>}

            </View>
        )
    }

    renderHashTags = (item) => {
        return (
            <ScrollView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap', marginLeft: 6, marginTop: 8 }}>
                {item.hashTags.map((hashTag, index) => (
                    <View key={index}>
                        <Text style={[defaultStyle.Subtitle_2, { color: COLORS.grey_350, marginRight: 8 }]}>#{hashTag}</Text>
                    </View>
                ))}
            </ScrollView>
        )
    }

    getUserActivitiesPosts() {

        let url = this.props.route.params && this.props.route.params.id !== '' ?
            REACT_APP_userServiceURL + '/post/user/getNewsFeed?userId=' + this.props.route.params.id + '&postRequestingUserId=' + this.state.userId + '&newsFeedType=' + this.state.selected + "&page=" + this.state.pageNumber + "&size=" + this.state.pageSize
            :
            REACT_APP_userServiceURL + '/post/user/getNewsFeed?userId=' + this.state.userId + '&postRequestingUserId=' + this.state.userId + '&newsFeedType=' + this.state.selected + "&page=" + this.state.pageNumber + "&size=" + this.state.pageSize

        axios({
            method: 'get',
            url: url,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.body && response.status === 200) {
                // console.log('+++++++++++++++++++++ Activities Data +++++++++++++++++++++++++++',response.data.body.content[0])
                this.setState({ activitiesData: response.data.body.content })

            }
        }).catch((err) => {
            console.log(err)
        })
    }

    shareModal = () => {
        return (

            <Modal visible={this.state.shareModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ shareModalOpen: false })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
                    </TouchableOpacity>

                    <View style={defaultShape.Modal_Categories_Container}>

                        <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 }]}>
                            <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>Share</Text>
                        </View>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15 }] : defaultShape.ActList_Cell_Gylph_Alt} activeOpacity={0.6} onPress={() => { this.setState({ shareModalOpen: false }) }}>
                            <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>Repost on WeNaturalists</Text>
                            <Icon name='Forward' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => {
                            Clipboard.setString(REACT_APP_domainUrl + '/post/' + this.state.pressedActivityId)
                            Snackbar.show({
                                backgroundColor: '#97A600',
                                text: "Link Copied",
                                textColor: "#00394D",
                                duration: Snackbar.LENGTH_LONG,
                            })
                        }}
                            style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt]}
                            activeOpacity={0.6}>
                            <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>Copy link to profile</Text>
                            <Icon name='TxEdi_AddLink' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt]} activeOpacity={0.6} onPress={() => { this.setState({ shareModalOpen: false }) }}>
                            <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>Share through Mail</Text>
                            <Icon name='Mail_OL' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => { this.setState({ shareModalOpen: false }, () => this.onShare()) }}
                            style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }] : [defaultShape.ActList_Cell_Gylph_Alt, { borderBottomWidth: 0 }]}
                            activeOpacity={0.6}>
                            <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>Share via others</Text>

                            <View style={[{ flexDirection: 'row', justifyContent: 'space-evenly', width: 100, marginRight: -6 }]}>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name="Social_FB" size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name='Social_Twitter' size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name='Social_LinkedIn' size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name="Meatballs" size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                                </View>

                            </View>

                        </TouchableOpacity>


                    </View>



                </View>

            </Modal>

        )
    }

    contactInfoModal = () => {
        return (

            <Modal visible={this.state.contactInfoModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ contactInfoModalOpen: false })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={styles.crossIcon} />
                    </TouchableOpacity>

                    <View style={defaultShape.Modal_Categories_Container}>

                        <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0, borderBottomWidth: 0 }]}>
                            <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>CONTACT INFO</Text>
                        </View>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15, borderBottomWidth: 0, flexDirection: 'column', alignItems: 'flex-start' }] : [defaultShape.ActList_Cell_Gylph_Alt, { flexDirection: 'column', alignItems: 'flex-start', borderBottomWidth: 0, paddingVertical: 12 }]} activeOpacity={0.6} onPress={() => { this.setState({ contactInfoModalOpen: false }) }}>
                            <Text style={[defaultStyle.Button_2, { color: COLORS.dark_600 }]}>Phone Number :</Text>
                            <Text style={[defaultStyle.Caption, { color: COLORS.dark_600, fontSize: 12.5 }]}>+{this.props.user.body && this.props.user.body.countryCode} {this.props.user.body && this.props.user.body.mobile}</Text>

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15, borderBottomWidth: 0, flexDirection: 'column', alignItems: 'flex-start' }] : [defaultShape.ActList_Cell_Gylph_Alt, { flexDirection: 'column', alignItems: 'flex-start', borderBottomWidth: 0, paddingVertical: 12 }]} activeOpacity={0.6} onPress={() => { this.setState({ contactInfoModalOpen: false }) }}>
                            <Text style={[defaultStyle.Button_2, { color: COLORS.dark_600 }]}>Email :</Text>
                            <Text style={[defaultStyle.Caption, { color: COLORS.dark_600, fontSize: 12.5 }]}>{this.props.user.body && this.props.user.body.email}</Text>

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15, borderBottomWidth: 0, flexDirection: 'column', alignItems: 'flex-start' }] : [defaultShape.ActList_Cell_Gylph_Alt, { flexDirection: 'column', alignItems: 'flex-start', borderBottomWidth: 0, paddingVertical: 12 }]} activeOpacity={0.6} onPress={() => { this.setState({ contactInfoModalOpen: false }) }}>
                            <Text style={[defaultStyle.Button_2, { color: COLORS.dark_600 }]}>Address :</Text>
                            <Text style={[defaultStyle.Caption, { color: COLORS.dark_600, fontSize: 12.5 }]}>{this.props.user.body && this.props.user.body.city}, {this.props.route.params ? this.props.route.params.state : null}, {this.props.user.body && this.props.user.body.country}</Text>

                        </TouchableOpacity>

                    </View>

                </View>

            </Modal>

        )
    }

    handleDeleteSubmit = () => {
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/backend/post/delete/' + this.state.pressedActivityId,
            withCredentials: true
        }).then(response => {
            if (response && response.status === 202) {
                // console.log(response.status)
            }
        }).catch((err) => {
            // console.log(err)
        })
        this.setState({ optionsModalOpen: false, activitiesData: this.state.activitiesData.filter((item) => item.id !== this.state.pressedActivityId) })
    }

    createTwoButtonAlert = (id) =>
        Alert.alert('', 'Are you sure you want to delete your Award?', [
            {
                text: 'YES',
                onPress: () => this.deleteAward(id),
                style: 'cancel',
            },
            { text: 'NO' },
        ])

    deleteAward = (id) => {
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + "/backend/award/delete/" + id,
            withCredentials: true,
        }).then(response => {
            this.setState({ error: '' })
            if (response && response.data && response.data.message === 'Success!') {
                console.log(response.data.message)
                this.getAllAwardList(this.state.userId)
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    renderAwardItem = (item) => {
        return (
            <View style={[styles.projItem, {}]}>

                <View style={{ marginHorizontal: 15, marginTop: 10 }}>

                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'flex-start',
                            height: 40,
                            justifyContent: 'flex-end'
                        }}>
                        <TouchableOpacity
                            onPress={() => this.setState({
                                addAwardsModalOpen: true,
                                dataId: item.item.id,
                                isEditAward: true,
                                awardTitle: item.item.title,
                                issuer: item.item.issuer,
                                startDate: item.item.issueTime,
                                endDate: item.item.endingTime,
                                credentialId: item.item.credentialId,
                                credentialVerificationLink: item.item.credentialVerificationLink,
                                briefDescription: item.item.description,
                                lifetimeValidity: item.item.lifetimeValidity,
                                thumbnail: { uri: item.item.thumbnail && item.item.thumbnail.attachmentUrl }
                            })}
                            style={defaultShape.AdjunctBtn_Small_Sec}>
                            <Icon
                                name="EditBox"
                                color={COLORS.dark_600}
                                size={14}
                                style={Platform.OS === 'android' && { marginTop: 5 }}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.createTwoButtonAlert(item.item.id)}
                            style={[defaultShape.AdjunctBtn_Small_Sec, { marginLeft: 10 }]}>
                            <Icon
                                name="TrashBin"
                                color={COLORS.alertred_200}
                                size={14}
                                style={Platform.OS === 'android' && { marginTop: 5 }}
                            />
                        </TouchableOpacity>
                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            // backgroundColor: 'yellow'
                        }}>
                        <Image
                            source={
                                item.item.thumbnail && item.item.thumbnail.attachmentUrl
                                    ? { uri: item.item.thumbnail.attachmentUrl }
                                    : projectDefault
                            }
                            style={{ height: 60, width: 60, borderRadius: 10, position: 'absolute', top: 3 }}
                        />
                        <View style={{ paddingLeft: 60 }}>
                            <Text
                                style={[
                                    typography.Button_Lead,
                                    { color: COLORS.dark_800, marginLeft: 14, textTransform: 'capitalize' },
                                ]}>
                                {item.item.title}
                            </Text>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginLeft: 10,
                                    alignItems: 'center',
                                    marginVertical: 4
                                }}>

                                <Text
                                    style={[
                                        typography.Note2,
                                        { color: '#888', marginLeft: 5 },
                                    ]}>
                                    {moment.unix(item.item.issueTime / 1000).format('MMM YYYY')} - {!item.item.lifetimeValidity ? moment.unix(item.item.endingTime / 1000).format('MMM YYYY') : 'No Expiration Date'}
                                </Text>
                            </View>

                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginLeft: 10,
                                    alignItems: 'center',
                                    marginBottom: 2
                                }}>

                                <Text
                                    style={[
                                        typography.Note2,
                                        { color: '#888', marginLeft: 5 },
                                    ]}>
                                    {item.item.issuer}
                                </Text>
                            </View>

                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginLeft: 10,
                                    alignItems: 'center',
                                    marginBottom: 2
                                }}>

                                <Text
                                    style={[
                                        typography.Note2,
                                        { color: '#888', marginLeft: 5 },
                                    ]}>
                                    Credential ID- {item.item.credentialId}
                                </Text>
                            </View>


                            {item.item.credentialVerificationLink ?
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        marginLeft: 10,
                                        alignItems: 'center',
                                    }}>

                                    <Text onPress={() => this.openWebsite(item.item.credentialVerificationLink)}
                                        style={[
                                            typography.Note2,
                                            { color: '#bfc52e', marginLeft: 5, fontSize: 11 },
                                        ]}>
                                        Verify Credential
                                    </Text>
                                </View> : <></>}

                            <View style={{ height: 1, width: 260, backgroundColor: '#d4dde0', marginLeft: 15, marginTop: 10 }}></View>

                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginLeft: 10,
                                    alignItems: 'center',
                                    marginTop: 10
                                }}>

                                <Text numberOfLines={this.state.pressedId === item.item.id ? 1000 : 5}
                                    style={[
                                        typography.Note2,
                                        { color: COLORS.altgreen_300, marginLeft: 5 },
                                    ]}>
                                    {item.item.description}
                                </Text>
                            </View>

                            {item.item.description.split(' ').length > 50 ?
                                <TouchableOpacity activeOpacity={0.6} style={{ height: 30, width: 80, marginLeft: 15 }}
                                    onPress={() => this.setState({
                                        pressedId: this.state.pressedId === item.item.id ? '' : item.item.id
                                    })}>
                                    <Text style={{ color: '#4068eb', fontWeight: '700', fontSize: 12 }}>
                                        {this.state.pressedId === item.item.id ? 'Read less' : 'Read more'}
                                    </Text>
                                </TouchableOpacity> : null
                            }

                            {item.item.attachments && item.item.attachments[0] && item.item.attachments[0].attachmentUrl &&
                                <Image
                                    source={{ uri: item.item.attachments[0].attachmentUrl }}
                                    style={{ height: 60, width: 140, borderRadius: 10, marginLeft: 15, marginTop: 10 }}
                                />}
                        </View>
                    </View>
                </View>

            </View>
        )
    }

    goback = () => {
        this.props.navigation.goBack()
    }

    addButtonFunction = () => {
        this.setState({
            addAwardsModalOpen: true,
            addInstitute: '',
            addFieldOfStudy: '',
            addStartTimeUnix: 0,
            addEndTimeUnix: 0
        })
    }

    getMinEndDate = () => {
        var day = new Date(this.state.startDate)

        var nextDay = new Date(day)
        nextDay.setDate(day.getDate())
        return nextDay
    }

    unixTimeDatePicker = (UNIX_timestamp) => {
        var date = new Date(UNIX_timestamp);
        var months = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
        ];
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var hour = date.getHours();
        var min = date.getMinutes();
        var sec = date.getSeconds();

        return day + '-' + month + '-' + year;
    }

    currentDate = () => {
        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = dd + '-' + mm + '-' + yyyy;

        return today;
    }

    openGallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
        })
            .then((image) => {
                console.log(image);
                this.setState({
                    thumbnail: {
                        uri: image.path,
                        type: image.mime,
                        name: image.path.split('/')[image.path.split('/').length - 1],
                    },
                });
            })
            .catch((err) => console.log(err));
    }

    submitAwardForm = () => {
        let postData = {
            entityId: this.state.userId,
            title: this.state.awardTitle,
            issuer: this.state.issuer,
            issueTime: this.state.startDate,
            endingTime: this.state.endDate,
            credentialId: this.state.credentialId,
            credentialVerificationLink: this.state.credentialVerificationLink,
            description: this.state.briefDescription
        }

        // if (this.state.thumbnail) {
        //     postData.thumbnail = this.state.thumbnail
        // }

        if (this.state.attachmentFile) {
            postData.attachmentIds = this.state.attachmentFile
        }
        postData.userType = 'INDIVIDUAL'
        // if (this.state.awardEntityType === 'INDIVIDUAL') {
        //     postData.userType = 'INDIVIDUAL';
        // } else if (this.state.awardEntityType === 'CIRCLE') {
        //     postData.userType = 'CIRCLE';
        // } else {
        //     postData.userType = 'COMPANY';
        // }
        let url = ''
        if (this.state.isEditAward) {
            postData.id = this.state.dataId
            postData.lifetimeValidity = this.state.lifetimeValidity
            url = 'backend/award/edit'
        } else {
            postData.lifetimeValidity = this.state.lifetimeValidity
            url = 'backend/award/create'
        }
        axios({
            method: "POST",
            url: `${REACT_APP_userServiceURL}/${url}`,
            data: postData,
            // headers: { "Content-Type": "multipart/form-data" },
            withCredentials: true,
        }).then((response) => {
            if (response && response.data && response.data.message === "Success!") {
                console.log(response.data.message)
                Snackbar.show({
                    backgroundColor: '#97A600',
                    text: this.state.isEditAward ? 'Edited successfully' : 'Award created successfully',
                    textColor: '#00394D',
                    duration: Snackbar.LENGTH_LONG,
                })
                setTimeout(() => {
                    this.setState({
                        addAwardsModalOpen: false,
                        dataId: '',
                        isEditAward: false,
                        awardTitle: '',
                        issuer: '',
                        startDate: 0,
                        endDate: 0,
                        credentialId: '',
                        credentialVerificationLink: '',
                        briefDescription: '',
                        lifetimeValidity: false,
                        thumbnail: {}
                    })
                }, 2000)

                // this.hideModal();
                // this.setState({
                //     commentAttachments: [],
                // })
                this.getAllAwardList(this.state.userId)
            }
        })
            .catch((error) => {
                console.log(error)
            })
    }

    validateForm = () => {
        if (this.state.awardTitle.trim() === '') {
            Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Please enter title',
                duration: Snackbar.LENGTH_LONG,
            })
        } else if (this.state.issuer.trim() === '') {
            Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Please enter issuer',
                duration: Snackbar.LENGTH_LONG,
            })
        } else if (this.state.startDate === 0) {
            Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Please enter Issue Date',
                duration: Snackbar.LENGTH_LONG,
            })
        } else if (this.state.endDate === 0 && !this.state.lifetimeValidity) {
            Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Please enter End Date',
                duration: Snackbar.LENGTH_LONG,
            })
        } else if (this.state.credentialVerificationLink.trim() !== '' && !regexp.test(this.state.credentialVerificationLink)) {
            Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Please enter a valid url',
                duration: Snackbar.LENGTH_LONG,
            })
        }
        else this.submitAwardForm()
    }

    addAwardsModal = () => {
        return (
            <Modal
                onRequestClose={() => this.setState({
                    addAwardsModalOpen: false,
                    awardTitle: '',
                    thumbnail: {},
                    issuer: '',
                    credentialId: '',
                    credentialVerificationLink: '',
                    briefDescription: '',
                    startDate: 0,
                    endDate: 0,
                    lifetimeValidity: false
                })}
                visible={this.state.addAwardsModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <ScrollView
                    style={{ marginTop: -19 }}
                    keyboardShouldPersistTaps="handled">
                    <View
                        style={[
                            defaultShape.Modal_Categories_Container,
                            { height: '100%', backgroundColor: COLORS.grey_100 },
                        ]}>
                        <View
                            style={
                                Platform.OS === 'ios'
                                    ? [styles.header, { paddingVertical: 12 }]
                                    : styles.header
                            }>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    width: '100%',
                                }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <TouchableOpacity
                                        style={{
                                            width: 40,
                                            height: 30,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}
                                        onPress={() => {
                                            this.setState({
                                                addAwardsModalOpen: false,
                                                awardTitle: '',
                                                thumbnail: {},
                                                issuer: '',
                                                credentialId: '',
                                                credentialVerificationLink: '',
                                                briefDescription: '',
                                                startDate: 0,
                                                endDate: 0,
                                                lifetimeValidity: false
                                            })
                                        }}>
                                        <Icon
                                            name="Arrow-Left"
                                            size={16}
                                            color="#91B3A2"
                                            style={
                                                Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }
                                            }
                                        />
                                    </TouchableOpacity>
                                    <Icon
                                        name="AddProject_Fl"
                                        size={18}
                                        color={COLORS.dark_800}
                                        style={{
                                            marginTop: Platform.OS === 'ios' ? 0 : 8,
                                            marginLeft: 10,
                                        }}
                                    />
                                    <Text
                                        style={[
                                            defaultStyle.Button_Lead,
                                            { color: COLORS.dark_800, marginLeft: 6 },
                                        ]}>
                                        Add Awards
                                    </Text>
                                </View>
                                <TouchableOpacity
                                    onPress={() => this.validateForm()}
                                    activeOpacity={0.7}
                                    style={[styles.updateButton, { marginTop: 10, marginRight: 10 }]}>
                                    <Text
                                        style={[
                                            defaultStyle.Caption,
                                            { color: COLORS.altgreen_200 },
                                        ]}>
                                        Continue
                                    </Text>
                                    <Icon
                                        name="Arrow_Right"
                                        size={13}
                                        color={COLORS.altgreen_200}
                                        style={{
                                            marginTop: Platform.OS === 'ios' ? 0 : 8,
                                            marginLeft: 4,
                                        }}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                paddingLeft: 6,
                                paddingRight: 12,
                                marginBottom: 80,
                                marginTop: 10,
                            }}>
                            <TextInput
                                theme={{
                                    colors: {
                                        text: COLORS.dark_700,
                                        primary: COLORS.altgreen_300,
                                        placeholder: COLORS.altgreen_300,
                                    },
                                }}
                                label={<Text>Title <Text style={{ color: 'red' }}>*</Text></Text>}
                                selectionColor="#C8DB6E"
                                style={[
                                    defaultStyle.Subtitle_1,
                                    {
                                        width: '90%',
                                        backgroundColor: COLORS.bgfill_200,
                                        color: COLORS.dark_700,
                                    },
                                ]}
                                onChangeText={(value) => this.setState({ awardTitle: value })}
                                value={this.state.awardTitle}
                            />
                        </View>

                        <View
                            style={{
                                backgroundColor: COLORS.white,
                                alignItems: 'center',
                                width: '100%',
                                paddingBottom: 20,
                            }}>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                onPress={() => this.openGallery()}
                                style={{
                                    height: 150,
                                    width: 300,
                                    backgroundColor: COLORS.altgreen_100,
                                    borderColor: COLORS.altgreen_200,
                                    borderWidth: 1,
                                    borderRadius: 8,
                                    marginTop: -60,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                {this.state.thumbnail && this.state.thumbnail.uri ? (
                                    <Image
                                        source={{ uri: this.state.thumbnail.uri }}
                                        style={{ height: 150, width: 300, borderRadius: 8 }}
                                    />
                                ) : (
                                    <View
                                        style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <View
                                            style={{
                                                height: 42,
                                                width: 42,
                                                backgroundColor: COLORS.altgreen_200,
                                                borderRadius: 21,
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}>
                                            <Icon
                                                name="UploadPhoto"
                                                size={18}
                                                color={COLORS.dark_600}
                                                style={{ marginTop: Platform.OS === 'ios' ? 0 : 8 }}
                                            />
                                        </View>

                                        <Text
                                            style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>
                                            Add Thumbnail
                                        </Text>
                                    </View>
                                )}
                            </TouchableOpacity>



                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    paddingLeft: 6,
                                    paddingRight: 12,
                                    marginVertical: 10,
                                }}>
                                <TextInput
                                    theme={{
                                        colors: {
                                            text: COLORS.dark_700,
                                            primary: COLORS.altgreen_300,
                                            placeholder: COLORS.altgreen_300,
                                        },
                                    }}
                                    label={<Text>Issuer <Text style={{ color: 'red' }}>*</Text></Text>}
                                    selectionColor="#C8DB6E"
                                    style={[
                                        defaultStyle.Subtitle_1,
                                        {
                                            width: '90%',
                                            backgroundColor: COLORS.bgfill_200,
                                            color: COLORS.dark_700,
                                        },
                                    ]}
                                    onChangeText={(value) =>
                                        this.setState({ issuer: value })
                                    }
                                    value={this.state.issuer}
                                />
                            </View>

                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    paddingLeft: 6,
                                    paddingRight: 12,
                                }}>
                                <TextInput
                                    theme={{
                                        colors: {
                                            text: COLORS.dark_700,
                                            primary: COLORS.altgreen_300,
                                            placeholder: COLORS.altgreen_300,
                                        },
                                    }}
                                    label="Credential ID"
                                    selectionColor="#C8DB6E"
                                    style={[
                                        defaultStyle.Subtitle_1,
                                        {
                                            width: '90%',
                                            backgroundColor: COLORS.bgfill_200,
                                            color: COLORS.dark_700,
                                        },
                                    ]}
                                    onChangeText={(value) =>
                                        this.setState({ credentialId: value })
                                    }
                                    value={this.state.credentialId}
                                />
                            </View>

                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    paddingLeft: 6,
                                    paddingRight: 12,
                                }}>
                                <TextInput
                                    theme={{
                                        colors: {
                                            text: COLORS.dark_700,
                                            primary: COLORS.altgreen_300,
                                            placeholder: COLORS.altgreen_300,
                                        },
                                    }}
                                    label="Credential Verification Link"
                                    selectionColor="#C8DB6E"
                                    style={[
                                        defaultStyle.Subtitle_1,
                                        {
                                            width: '90%',
                                            backgroundColor: COLORS.bgfill_200,
                                            color: COLORS.dark_700,
                                        },
                                    ]}
                                    onChangeText={(value) =>
                                        this.setState({ credentialVerificationLink: value })
                                    }
                                    value={this.state.credentialVerificationLink}
                                />
                            </View>

                        </View>

                        <View
                            style={{
                                backgroundColor: COLORS.altgreen_50,
                                alignItems: 'center',
                                width: '100%',
                                paddingBottom: 16,
                                borderBottomColor: '#0000001A',
                                borderBottomWidth: 0.5,
                                borderTopColor: '#0000001A',
                                borderTopWidth: 0.5,
                                paddingBottom: 20,
                                paddingTop: 4,
                            }}>

                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => {
                                    this.secondTextInput.focus()
                                }}
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    paddingLeft: 6,
                                    paddingRight: 12,
                                    marginTop: 20,
                                    backgroundColor: COLORS.white,
                                    borderRadius: 10,
                                }}>
                                <TextInput
                                    ref={(input) => {
                                        this.secondTextInput = input
                                    }}
                                    theme={{
                                        colors: {
                                            text: COLORS.dark_700,
                                            primary: COLORS.altgreen_300,
                                            placeholder: COLORS.altgreen_300,
                                        },
                                    }}
                                    label="Brief Description"
                                    multiline
                                    selectionColor="#C8DB6E"
                                    style={[
                                        defaultStyle.Subtitle_1,
                                        {
                                            minHeight: 200,
                                            width: '90%',
                                            backgroundColor: COLORS.white,
                                            color: COLORS.dark_700,
                                        },
                                    ]}
                                    onChangeText={(value) =>
                                        this.setState({ briefDescription: value })
                                    }
                                    value={this.state.briefDescription}
                                />
                            </TouchableOpacity>
                        </View>



                        <View
                            style={{
                                backgroundColor: COLORS.white,
                                alignItems: 'center',
                                width: '100%',
                                paddingBottom: 24,
                                marginTop: 0,
                            }}>
                            <TouchableOpacity
                                activeOpacity={0.6}
                                style={{
                                    height: 56,
                                    width: '86%',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    marginTop: 20,
                                    paddingLeft: 6,
                                    paddingRight: 12,
                                }}>
                                <View style={{ width: '40%' }}>
                                    <Text
                                        style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                                        Issue Date <Text style={{ color: 'red' }}>*</Text>
                                    </Text>
                                    <DatePicker
                                        style={{ width: '100%', borderWidth: 0 }}
                                        date={
                                            this.state.startDate === 0
                                                ? ''
                                                : this.unixTimeDatePicker(this.state.startDate)
                                        }
                                        mode="date"
                                        placeholder="Start Date"
                                        format="DD-MM-YYYY"
                                        minDate="01-01-1900"
                                        maxDate={this.currentDate()}
                                        // maxDate="16-07-2021"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                right: 0,
                                                top: -10,
                                                marginRight: 0,
                                            },
                                            dateInput: {
                                                alignItems: 'flex-start',
                                                borderWidth: 0,
                                            },
                                            dateText: {
                                                color: COLORS.dark_700,
                                                marginTop: -20,
                                            },
                                        }}
                                        onDateChange={(date) => {
                                            let tempdate = date.split('-').reverse().join('-');
                                            let convertedUnixTime = new Date(tempdate).getTime();
                                            this.setState({
                                                startDate: convertedUnixTime,
                                                seconds: 59,
                                                endDate:
                                                    convertedUnixTime >
                                                        new Date(this.state.endDate).getTime()
                                                        ? 0
                                                        : this.state.endDate,
                                            });
                                        }}
                                    />
                                </View>

                                {this.state.startDate && !this.state.lifetimeValidity ? (
                                    <View style={{ width: '40%' }}>
                                        <Text
                                            style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                                            End Date
                                        </Text>
                                        <DatePicker
                                            style={{ width: '100%', borderWidth: 0 }}
                                            date={
                                                this.state.endDate === 0
                                                    ? ''
                                                    : this.unixTimeDatePicker(this.state.endDate)
                                            }
                                            mode="date"
                                            placeholder="End Date"
                                            format="DD-MM-YYYY"
                                            minDate={this.getMinEndDate()}
                                            maxDate={this.currentDate()}
                                            // maxDate="16-07-2021"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    right: 0,
                                                    top: -10,
                                                    marginRight: 0,
                                                },
                                                dateInput: {
                                                    alignItems: 'flex-start',
                                                    borderWidth: 0,
                                                },
                                                dateText: {
                                                    color: COLORS.dark_700,
                                                    marginTop: -20,
                                                },
                                            }}
                                            onDateChange={(date) => {
                                                let tempdate = date.split('-').reverse().join('-');
                                                let convertedUnixTime = new Date(
                                                    tempdate,
                                                ).getTime();
                                                this.setState({
                                                    endDate: convertedUnixTime,
                                                    seconds: 59,
                                                });
                                            }}
                                        />
                                    </View>) : null}
                                {/* <Icon name="Calender_OL" color='#698F8A' size={16} style={{ marginTop: Platform.OS === 'android' ? 12 : 0, zIndex: 2 }} /> */}
                            </TouchableOpacity>
                            {/* <View style={styles.border2}></View> */}
                        </View>

                        <View
                            style={{
                                height: 73,
                                width: '60%',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-evenly',
                                marginTop: 16,
                                paddingHorizontal: 50,
                                // marginLeft: 30,
                                backgroundColor: COLORS.altgreen_100,
                                borderRadius: 8,
                                // alignSelf: 'flex-start'
                            }}>
                            <TouchableOpacity
                                onPress={() =>
                                    this.setState({ lifetimeValidity: !this.state.lifetimeValidity })
                                }
                                activeOpacity={0.6}
                                style={{
                                    width: 20,
                                    height: 20,
                                    borderRadius: 10,
                                    borderColor: COLORS.green_500,
                                    borderWidth: 2,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: this.state.lifetimeValidity
                                        ? COLORS.green_500
                                        : COLORS.altgreen_t50,
                                }}>
                                {this.state.lifetimeValidity ? (
                                    <Icon
                                        name="Tick"
                                        size={10}
                                        color={COLORS.dark_800}
                                        style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                                    />
                                ) : (
                                    <></>
                                )}
                            </TouchableOpacity>

                            <View>
                                <Text style={[defaultStyle.Caption, { color: '#698F8A' }]}>
                                    Lifetime Validity
                                </Text>
                            </View>

                        </View>
                    </View>
                </ScrollView>
            </Modal>
        )
    }

    render() {
        return (

            <SafeAreaView style={{ flex: 1 }}>
                {this.postModal()}
                {this.optionsModal()}
                {this.shareModal()}
                {this.likeModal()}
                {this.headerOptionsModal()}
                {this.shareModal2()}
                {this.contactInfoModal()}
                {this.addAwardsModal()}

                <ProfileEditHeader
                    name="Awards"
                    iconName="Endorse"
                    goback={this.goback}
                    addButton={true}
                    addButtonFunction={this.addButtonFunction}
                />

                {this.state.awardList && this.state.awardList.length ?
                    <View style={styles.container}>
                        <FlatList
                            data={this.state.awardList}
                            keyExtractor={(item) => item.id}
                            ref={(ref) => { this.flatListRef = ref; }}
                            style={{ marginTop: 0 }}
                            renderItem={(item) => this.renderAwardItem(item)}
                        />
                    </View>
                    :
                    <Icon
                        name="Shield_Tick"
                        size={52}
                        color={COLORS.altgreen_300}
                        style={{ marginTop: Platform.OS === 'android' ? 10 : 0, marginTop: '40%', alignSelf: 'center' }}
                    />

                }

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // backgroundColor: '#00394D',
        flex: 1,
    },
    updateButton: {
        flexDirection: 'row',
        width: 110,
        height: 28,
        borderRadius: 17,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
        backgroundColor: COLORS.dark_700,
    },
    modalText: {
        fontSize: 14,
        color: '#154A59',
        fontFamily: 'Montserrat-SemiBold',
        marginLeft: 14
    },
    termsmodal: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        width: '85%',
        paddingVertical: 5.5,
        paddingLeft: 30
    },
    renderSharedPost: {
        width: '94%',
        marginTop: 5,
        marginLeft: 6,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: COLORS.grey_300,
        padding: 10,
    },
    crossIcon: {
        marginTop: Platform.OS === 'android' ? 5 : 0
    },
    renderItemStyle: {
        backgroundColor: COLORS.white,
        paddingLeft: 10,
        paddingVertical: 10,
        marginHorizontal: 10,
        marginVertical: 6,
        borderRadius: 8,
        borderWidth: 0.5,
        borderColor: COLORS.grey_300
    },
    unreadNotiItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: COLORS.white,
        borderRadius: 8
        // borderBottomColor: COLORS.grey_400,
        // borderBottomWidth: 0.3,
        // height: 44
    },
    scrollViewContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    editIcon: {
        marginTop: 8,
        marginRight: 8
    },
    editIcon2: {
        marginTop: Platform.OS === 'android' ? 8 : 0,
        // marginRight: 8
    },
    floatingIconText: {
        color: '#00394D',
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 2,
    },
    floatingIcon: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: 113,
        height: 44,
        borderRadius: 40,
        backgroundColor: '#D8DE21',
        position: 'absolute',
        bottom: 16,
        right: 12,
    },
    floatingIcon2: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: 28,
        height: 42,
        borderRadius: 40,
        backgroundColor: '#1A4D5F80',
        position: 'absolute',
        bottom: 66,
        right: 12,
    },
    feedDetails: {
        marginTop: 20,
        marginLeft: 20,
        paddingBottom: 16
    },
    selected: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#367681',
        marginRight: 15,
        height: 27,
        // width: 66,
        borderRadius: 16,
        textAlign: 'center',
    },
    notSelected: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15,
        height: 27,
        // width: 66,
        borderWidth: 1,
        borderColor: '#698F8A',
        borderRadius: 16,
    },
    selectedText: {
        color: '#F7F7F5',
        fontSize: 12,
        paddingHorizontal: 14,
        fontFamily: 'Montserrat-Medium'
    },
    notSelectedText: {
        color: '#698F8A',
        fontSize: 12,
        paddingHorizontal: 14,
        fontFamily: 'Montserrat-Medium'
    },
    searchBar: {
        // marginTop: 8
    },
    explore: {
        flexDirection: 'row',
        paddingTop: 8,
        marginLeft: 15
    },
    feed: {
        flexDirection: 'row',
        paddingTop: 8,
        marginLeft: 20,
        borderBottomColor: '#BFC52E',
        borderBottomWidth: 4,
        width: 100
    },
    header: {
        flexDirection: 'row',
        borderBottomColor: '#154A59',
        borderBottomWidth: 1,
        paddingTop: 15,
        height: 65
    },
    feedText: {
        color: '#FFFFFF',
        fontSize: 22,
        marginLeft: 5,
        fontFamily: 'Montserrat-SemiBold'
    },
    exploreText: {
        color: '#91B3A2',
        fontSize: 22,
        marginLeft: 5,
        fontFamily: 'Montserrat-Regular'
    },
    projItem: {
        marginHorizontal: 15,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        marginVertical: 7.5,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        paddingBottom: 20
    },
})

const mapStateToProps = (state) => {
    return {
        userDataProgress: state.personalProfileReducer.userDataProgress,
        user: state.personalProfileReducer.user,
        error: state.personalProfileReducer.error,

        userFeedsPhotosProgress: state.feedsReducer.userFeedsPhotosProgress,
        userFeedsPhotos: state.feedsReducer.userFeedsPhotos,
        errorFeedsPhotos: state.feedsReducer.errorFeedsPhotos,

        userFeedsVideosProgress: state.feedsReducer.userFeedsVideosProgress,
        userFeedsVideos: state.feedsReducer.userFeedsVideos,
        errorFeedsVideos: state.feedsReducer.errorFeedsVideos
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
        feedsPhotosRequest: (data) => dispatch(feedsPhotosRequest(data)),
        feedsVideosRequest: (data) => dispatch(feedsVideosRequest(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AwardsEdit)
