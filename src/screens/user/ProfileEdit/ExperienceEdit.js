import React, { Component } from 'react';
import {
  Alert,
  Modal,
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Platform,
  StyleSheet,
  ScrollView,
  FlatList,
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';
import { TextInput } from 'react-native-paper';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-snackbar';
import ImagePicker from 'react-native-image-crop-picker';
import LinearGradient from 'react-native-linear-gradient';

import projectDefault from '../../../../assets/project-default.jpg'
import styles2 from '../../../Components/GlobalCss/User/SignUpCss';
import CreateCircle from '../../../Components/User/EditProfile/CreateCircle';
import { personalExperienceRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import { COLORS } from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultCover from '../../../../assets/defaultCover.png';
import defaultStyle from '../../../Components/Shared/Typography';
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import { update } from 'lodash';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

class ExperienceEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      addJobModalOpen: false,
      addCircleMembersModalOpen: false,
      employmentTypeModalOpen: false,
      shareModalOpen: false,

      jobTitle: '',
      organizationName: '',
      organizationWebsite: '',
      briefDescription: '',
      jobCoverImage: {},
      acceptPolicy: false,

      employmentTypeSuggestions: [
        'Full-time',
        'Part-time',
        'Self-employed',
        'Freelance',
        'Internship',
        'Trainee',
      ],
      selectedEmploymentType: '',
      startDate: 0,
      endDate: 0,

      country: 'Select Country *',
      state: 'Select State *',
      city: 'Select City *',

      pressedExperienceId: '',
      jobType: '',
      update: false,
      modalterm: false
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({ userId: value });
        this.props.personalExperienceRequest({ userId: value, otherUserId: '' });

        // this.props.personalExperienceRequest({ userId: value, otherUserId: '' })
      })
      .catch((e) => {
        console.log(e);
      });
  }

  termsModal = () => {
    return (
      <Modal
        onRequestClose={() => this.setState({
          modalterm: false
        })}
        visible={this.state.modalterm}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={styles2.modalCountry}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            style={{
              alignSelf: 'center',
              width: 42,
              height: 42,
              borderRadius: 42 / 2,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#F7F7F5',
              marginRight: 10,
              marginBottom: 10,
            }}
            onPress={() => this.setState({ modalterm: false, addJobModalOpen: true })}>
            <Icon
              name="Cross"
              size={16}
              color="#367681"
              style={{ marginTop: 5 }}
            />
          </TouchableOpacity>

          <View
            style={{
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              backgroundColor: '#EEF1EC',
              alignItems: 'center',
              paddingTop: 20,
              height: 240,
            }}>
            <TouchableOpacity
              style={styles2.termsmodal}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({ modalterm: false }),
                  this.props.navigation.navigate('TermsConditions');
              }}>
              <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>
                Terms & Conditions
              </Text>
              <Icon name="Export" color="#367681" />
            </TouchableOpacity>

            <TouchableOpacity
              style={styles2.termsmodal}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({ modalterm: false }),
                  this.props.navigation.navigate('UserAgreement');
              }}>
              <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>
                User Agreement
              </Text>
              <Icon name="Export" color="#367681" />
            </TouchableOpacity>

            <TouchableOpacity
              style={styles2.termsmodal}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({ modalterm: false }),
                  this.props.navigation.navigate('PrivacyPolicy');
              }}>
              <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>
                Privacy Policy
              </Text>
              <Icon name="Export" color="#367681" />
            </TouchableOpacity>

            <TouchableOpacity
              style={[styles2.termsmodal, { borderBottomWidth: 0 }]}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({ modalterm: false }),
                  this.props.navigation.navigate('CookiePolicy');
              }}>
              <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>
                Cookie Policy
              </Text>
              <Icon name="Export" color="#367681" />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  unixTime = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    return day + ' ' + month + ' ' + year;
  };

  deleteExperience = (id) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/profile/delete/experience?id=' + id,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          this.props.personalExperienceRequest({
            userId: this.state.userId,
            otherUserId: '',
          });
        }
      })
      .catch((err) => {
        console.log('error in experience delete: ', err.response.data.message);
      });
  };

  createTwoButtonAlert = (id) =>
    Alert.alert('', 'Are you sure you want to delete your experience?', [
      {
        text: 'YES',
        onPress: () => this.deleteExperience(id),
        style: 'cancel',
      },
      { text: 'NO' },
    ]);

  setCurrentPressedJob = (item) => {
    console.log('item.country', item)

    this.setState({
      addJobModalOpen: true,
      jobTitle: item.title,
      selectedEmploymentType: item.employmentType
        ? this.employmentType2(item.employmentType)
        : '',
      organizationName: item.companyName,
      organizationWebsite: item.website,
      briefDescription: item.description,
      country: item.location.country,
      state: item.location.state,
      city: item.location.city,
      startDate: item.startTime ? item.startTime : 0,
      endDate: item.endTime ? item.endTime : 0,
      acceptPolicy: item.agreeToTerms,
      jobCoverImage: {
        uri: item.coverImageUrl,
        type: 'image/jpeg',
        name: 'newName',
      },
      pressedExperienceId: item.id,
      jobType: item.type,
      update: true
    })
  }

  // this.state.startDate === 0
  // ? ''
  // : this.unixTimeDatePicker(this.state.startDate)

  renderItem = (item) => {
    return (
      <View style={styles.experienceCard}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginRight: 20,
          }}>
          <View style={styles.expCardHeader}>
            {item.coverImageUrl !== null ? (
              <Image
                source={{ uri: item.coverImageUrl }}
                style={{ width: 50, height: 50, borderRadius: 6 }}
              />
            ) : (
              <Image
                source={projectDefault}
                style={{ width: 50, height: 50, borderRadius: 6 }}
              />
            )}
            <View
              style={{
                height: 60,
                backgroundColor: '#FFF',
                justifyContent: 'center',
              }}>
              <Text numberOfLines={1} style={[styles.expTitle, { maxWidth: 180 }]}>
                {item.title.charAt(0).toUpperCase() + item.title.slice(1)}
              </Text>
              <Text numberOfLines={1} style={[styles.expCompanyName, { maxWidth: 180 }]}>
                {item.companyName.charAt(0).toUpperCase() + item.companyName.slice(1)}
              </Text>
              {item.employmentType && <Text
                style={[
                  defaultStyle.Caption,
                  { marginLeft: 12, color: '#888', textTransform: 'capitalize', fontSize: 11 },
                ]}>
                {this.employmentType2(item.employmentType)}
              </Text>}
              <Text numberOfLines={1} style={[styles.expLocation, { textTransform: 'capitalize', maxWidth: 180 }]}>
                {item.location.city}, {item.location.country}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'flex-start',
              height: 60,
            }}>
            <TouchableOpacity
              onPress={() => this.setCurrentPressedJob(item)}
              style={defaultShape.AdjunctBtn_Small_Sec}>
              <Icon
                name="EditBox"
                color={COLORS.dark_600}
                size={14}
                style={Platform.OS === 'android' && { marginTop: 5 }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.createTwoButtonAlert(item.id)}
              style={[defaultShape.AdjunctBtn_Small_Sec, { marginLeft: 10 }]}>
              <Icon
                name="TrashBin"
                color={COLORS.alertred_200}
                size={14}
                style={Platform.OS === 'android' && { marginTop: 5 }}
              />
            </TouchableOpacity>
          </View>
        </View>

        <Text numberOfLines={100} style={styles.expDescription}>
          {item.description}
        </Text>

        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 10,
            backgroundColor: '#E7F3E3',
            borderBottomRightRadius: 6,
            borderBottomLeftRadius: 6,
            width: '100%',
            height: 40,
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          {this.unixTime(item.startTime).split(' ').splice(1, 2).join(' ') !==
            this.unixTime(item.endTime).split(' ').splice(1).join(' ') &&
            this.unixTime(item.endTime).split(' ').splice(1).join(' ') !==
            'Jan 1970' ? (
            <Text
              style={{
                color: '#698F8A',
                fontSize: 12,
                fontFamily: 'Montserrat-Medium',
                fontStyle: 'italic',
              }}>
              {this.unixTime(item.startTime).split(' ').splice(1).join(' ')} to{' '}
              {this.unixTime(item.endTime).split(' ').splice(1, 2).join(' ')}
            </Text>
          ) : this.unixTime(item.endTime).split(' ').splice(1).join(' ') ===
            'Jan 1970' ? (
            <Text
              style={{
                color: '#698F8A',
                fontSize: 12,
                fontFamily: 'Montserrat-Medium',
                fontStyle: 'italic',
              }}>
              {this.unixTime(item.startTime).split(' ').splice(1).join(' ')} -
              Running
            </Text>
          ) : (
            <Text
              style={{
                color: '#698F8A',
                fontSize: 12,
                fontFamily: 'Montserrat-Medium',
                fontStyle: 'italic',
              }}>
              {this.unixTime(item.startTime).split(' ').splice(1).join(' ')}
            </Text>
          )}
          <Text
            style={{
              color: '#698F8A',
              fontSize: 10,
              fontFamily: 'Montserrat-Medium',
              fontStyle: 'italic',
              borderWidth: 1,
              borderColor: '#91B3A2',
              borderRadius: 4,
              paddingHorizontal: 6,
              paddingVertical: 2,
            }}>
            {item.type}
          </Text>
        </View>
      </View>
    );
  }

  goback = () => {
    this.props.navigation.goBack()
  }

  addButtonFunction = () => {
    this.setState({ addJobModalOpen: true, update: false })
  }

  unixTimeDatePicker = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    return day + '-' + month + '-' + year;
  };

  toUnixTimeStamp = () => {
    let tempStartDate = this.state.startDate.split('-').reverse().join('-');
    // console.log('new Date(tempStartDate).getTime()', new Date(tempStartDate).getTime())
    return new Date(tempStartDate).getTime();
  };

  currentDate = () => {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = dd + '-' + mm + '-' + yyyy;

    return today;
  };

  employmentType = (value) => {
    if (value === 'Full-time') return 'FULL_TIME';
    if (value === 'Part-time') return 'PART_TIME';
    if (value === 'Self-employed') return 'SELF_EMPLOYED';
    if (value === 'Freelance') return 'FREELANCE';
    if (value === 'Internship') return 'INTERNSHIP';
    if (value === 'Trainee') return 'TRAINEE';
  };

  employmentType2 = (value) => {
    if (value === 'FULL_TIME') return 'Full-time';
    if (value === 'PART_TIME') return 'Part-time';
    if (value === 'SELF_EMPLOYED') return 'Self-employed';
    if (value === 'FREELANCE') return 'Freelance';
    if (value === 'INTERNSHIP') return 'Internship';
    if (value === 'TRAINEE') return 'Trainee';
  };

  addNewProject = () => {
    // console.log(this.toUnixTimeStamp(this.state.endDate) - this.toUnixTimeStamp(this.state.startDate))

    const formData = new FormData();

    let photo = {
      uri: this.state.jobCoverImage.uri,
      type: this.state.jobCoverImage.type,
      name: this.state.jobCoverImage.name,
    };

    formData.append(
      'coverImage',
      this.state.jobCoverImage && this.state.jobCoverImage.uri ? photo : null,
    );
    let postBody = {
      userId: this.state.userId,
      type: 'JOB',
      title: this.state.jobTitle,
      employmentType:
        this.state.selectedEmploymentType !== ''
          ? this.employmentType(this.state.selectedEmploymentType)
          : null,
      companyName: this.state.organizationName,
      website: this.state.organizationWebsite.trim() !== '' ? this.state.organizationWebsite : '',
      description: this.state.briefDescription,
      country: this.state.country,
      state: this.state.state,
      city: this.state.city,
      startTime: this.state.startDate,
      endTime: this.state.endDate,
      noOfDays: 0,
      agreeToTerms: this.state.acceptPolicy,
    };
    formData.append('data', JSON.stringify(postBody));
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/profile/update/experience',
      headers: { 'Content-Type': 'multipart/form-data' },
      data: formData,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '200 OK') {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'Job created successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });

          this.props.personalExperienceRequest({
            userId: this.state.userId,
            otherUserId: '',
          });

          setTimeout(() => {
            this.setState({
              addJobModalOpen: false,
              jobTitle: '',
              organizationName: '',
              organizationWebsite: '',
              briefDescription: '',
              country: 'Select Country *',
              state: 'Select State *',
              city: 'Select City *',
              startDate: 0,
              endDate: 0,
              jobCoverImage: {},
              acceptPolicy: false,
              selectedEmploymentType: '',
            });
          }, 1000);
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          if (err.response.data.status === '409 CONFLICT') {
            Snackbar.show({
              backgroundColor: '#B22222',
              text: err.response.data.message,
              duration: Snackbar.LENGTH_LONG,
            });
          } else {
            Snackbar.show({
              backgroundColor: '#B22222',
              text: 'Unable to Add Job, please try again',
              duration: Snackbar.LENGTH_LONG,
            });
          }
        }
      });
  };

  updateProject = () => {
    // console.log(this.toUnixTimeStamp(this.state.endDate) - this.toUnixTimeStamp(this.state.startDate))

    const formData = new FormData();

    let photo = {
      uri: this.state.jobCoverImage.uri,
      type: this.state.jobCoverImage.type,
      name: this.state.jobCoverImage.name,
    };

    formData.append(
      'coverImage',
      this.state.jobCoverImage && this.state.jobCoverImage.uri ? photo : null,
    );
    let postBody = {
      userId: this.state.userId,
      experienceId: this.state.pressedExperienceId,
      type: this.state.jobType,
      title: this.state.jobTitle,
      employmentType:
        this.state.selectedEmploymentType !== ''
          ? this.employmentType(this.state.selectedEmploymentType)
          : null,
      companyName: this.state.organizationName,
      website: this.state.organizationWebsite,
      description: this.state.briefDescription,
      country: this.state.country,
      state: this.state.state,
      city: this.state.city,
      startTime: this.state.startDate,
      endTime: this.state.endDate,
      noOfDays: 0,
      agreeToTerms: this.state.acceptPolicy,
    };
    formData.append('data', JSON.stringify(postBody));
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/profile/update/experience',
      headers: { 'Content-Type': 'multipart/form-data' },
      data: formData,
      withCredentials: true,
    })
      .then((response) => {
        console.log(response.data.status);
        let res = response.data;
        if (res.status === '200 OK') {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'Job updated successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });

          this.props.personalExperienceRequest({
            userId: this.state.userId,
            otherUserId: '',
          });

          setTimeout(() => {
            this.setState({
              addJobModalOpen: false,
              jobTitle: '',
              organizationName: '',
              organizationWebsite: '',
              briefDescription: '',
              country: 'Select Country *',
              state: 'Select State *',
              city: 'Select City *',
              startDate: 0,
              endDate: 0,
              jobCoverImage: {},
              acceptPolicy: false,
              selectedEmploymentType: '',
              update: false,
            });
          }, 1000);
        }
      })
      .catch((err) => {
        this.setState({ update: false });
        if (err && err.response && err.response.data) {
          if (err.response.data.status === '409 CONFLICT') {
            Snackbar.show({
              backgroundColor: '#B22222',
              text: err.response.data.message,
              duration: Snackbar.LENGTH_LONG,
            });
          } else {
            Snackbar.show({
              backgroundColor: '#B22222',
              text: 'Unable to update Job, please try again',
              duration: Snackbar.LENGTH_LONG,
            });
          }
        }
      });
  };

  validateForm = () => {
    if (this.state.jobTitle.trim() === '') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please enter a valid title',
        duration: Snackbar.LENGTH_LONG,
      });
    } else if (this.state.organizationName.trim() === '') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please enter the organization name',
        duration: Snackbar.LENGTH_LONG,
      });
    } else if (this.state.briefDescription.trim() === '') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please enter description',
        duration: Snackbar.LENGTH_LONG,
      });
    } else if (this.state.country === 'Select Country *') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please select a country',
        duration: Snackbar.LENGTH_LONG,
      })
    } else if (this.state.state === 'Select State *') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please select a state',
        duration: Snackbar.LENGTH_LONG,
      })
    } else if (this.state.city === 'Select City *') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please select a city',
        duration: Snackbar.LENGTH_LONG,
      })
    } else if (this.state.startDate === 0) {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please enter a valid start date',
        duration: Snackbar.LENGTH_LONG,
      })
    } else if (this.state.acceptPolicy === false) {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please Agree Terms and Condition',
        duration: Snackbar.LENGTH_LONG,
      })
    } else if (this.state.update) this.updateProject()
    else this.addNewProject()
  }

  openCamera = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    })
      .then((image) => {
        console.log(image);

        this.setState({
          jobCoverImage: {
            uri: image.path,
            type: image.mime,
            name: image.path.split('/')[image.path.split('/').length - 1],
          },
        });
      })
      .catch((err) => console.log(err));
  };

  openGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    })
      .then((image) => {
        console.log(image);
        this.setState({
          jobCoverImage: {
            uri: image.path,
            type: image.mime,
            name: image.path.split('/')[image.path.split('/').length - 1],
          },
        });
      })
      .catch((err) => console.log(err));
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ shareModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text
                style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>
                Cover Image
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { paddingTop: 25, paddingBottom: 15 },
                  ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ shareModalOpen: false }, () =>
                  this.openGallery(),
                );
              }}>
              <Text
                style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                Choose from Gallery
              </Text>
              <Icon
                name="Img"
                size={18}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { paddingVertical: 15, borderBottomWidth: 0 },
                  ]
                  : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { borderBottomWidth: 0 },
                  ]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ shareModalOpen: false }, () => this.openCamera());
              }}>
              <Text
                style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                Click a Photo
              </Text>
              <Icon
                name="Camera"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  employmentTypeModal = () => {
    return (
      <Modal
        visible={this.state.employmentTypeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View
            style={{
              width: '100%',
              height: 700,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ employmentTypeModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              { height: 450, backgroundColor: COLORS.bgfill, paddingBottom: 10 },
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  { color: COLORS.dark_900, textAlign: 'center' },
                ]}>
                EMPLOYMENT TYPE
              </Text>
            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingBottom: 0,
                  paddingTop: 46,
                  width: '100%',
                }}
                style={{ height: '50%' }}
                keyExtractor={(item) => item}
                data={this.state.employmentTypeSuggestions}
                initialNumToRender={10}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        selectedEmploymentType: item,
                        employmentTypeModalOpen: false,
                      });
                    }}
                    style={styles.item}
                    activeOpacity={0.7}>
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.Button_Lead,
                        { color: COLORS.dark_600, textAlign: 'center' },
                      ]}>
                      {item}
                    </Text>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      {/* {this.state.selectedEmploymentType.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  addJobModal = () => {
    return (
      <Modal
        onRequestClose={() => this.setState({
          addJobModalOpen: false,
          jobTitle: '',
          organizationName: '',
          organizationWebsite: '',
          briefDescription: '',
          country: 'Select Country *',
          state: 'Select State *',
          city: 'Select City *',
          startDate: 0,
          endDate: 0,
          jobCoverImage: {},
          acceptPolicy: false,
          selectedEmploymentType: ''
        })}
        visible={this.state.addJobModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <ScrollView
          style={{ marginTop: -19 }}
          keyboardShouldPersistTaps="handled">
          <View
            style={[
              defaultShape.Modal_Categories_Container,
              { height: '100%', backgroundColor: COLORS.grey_100 },
            ]}>
            <View
              style={
                Platform.OS === 'ios'
                  ? [styles.header, { paddingVertical: 12 }]
                  : styles.header
              }>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  width: '100%',
                }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <TouchableOpacity
                    style={{
                      width: 40,
                      height: 30,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={() => {
                      this.setState({
                        addJobModalOpen: false,
                        jobTitle: '',
                        organizationName: '',
                        organizationWebsite: '',
                        briefDescription: '',
                        country: 'Select Country *',
                        state: 'Select State *',
                        city: 'Select City *',
                        startDate: 0,
                        endDate: 0,
                        jobCoverImage: {},
                        acceptPolicy: false,
                        selectedEmploymentType: ''
                      })
                    }}>
                    <Icon
                      name="Arrow-Left"
                      size={16}
                      color="#91B3A2"
                      style={
                        Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }
                      }
                    />
                  </TouchableOpacity>
                  <Icon
                    name="AddProject_Fl"
                    size={18}
                    color={COLORS.dark_800}
                    style={{
                      marginTop: Platform.OS === 'ios' ? 0 : 8,
                      marginLeft: 10,
                    }}
                  />
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      { color: COLORS.dark_800, marginLeft: 6 },
                    ]}>
                    {this.state.update ? 'Update Experience' : 'Add Job'}
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => this.validateForm()}
                  activeOpacity={0.7}
                  style={[styles.updateButton, { marginTop: 10 }]}>
                  <Text
                    style={[
                      defaultStyle.Caption,
                      { color: COLORS.altgreen_200 },
                    ]}>
                    {this.state.update ? 'Update' : 'Continue'}
                  </Text>
                  <Icon
                    name="Arrow_Right"
                    size={13}
                    color={COLORS.altgreen_200}
                    style={{
                      marginTop: Platform.OS === 'ios' ? 0 : 8,
                      marginLeft: 4,
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingLeft: 6,
                paddingRight: 12,
                marginBottom: 80,
                marginTop: 10,
              }}>
              <TextInput
                theme={{
                  colors: {
                    text: COLORS.dark_700,
                    primary: COLORS.altgreen_300,
                    placeholder: COLORS.altgreen_300,
                  },
                }}
                label={<Text>Job Title <Text style={{ color: 'red' }}>*</Text></Text>}
                selectionColor="#C8DB6E"
                style={[
                  defaultStyle.Subtitle_1,
                  {
                    width: '90%',
                    backgroundColor: COLORS.bgfill_200,
                    color: COLORS.dark_700,
                  },
                ]}
                onChangeText={(value) => this.setState({ jobTitle: value })}
                value={this.state.jobTitle}
              />
            </View>

            <View
              style={{
                backgroundColor: COLORS.white,
                alignItems: 'center',
                width: '100%',
                paddingBottom: 20,
              }}>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.setState({ shareModalOpen: true })}
                style={{
                  height: 150,
                  width: 300,
                  backgroundColor: COLORS.altgreen_200,
                  borderColor: COLORS.altgreen_200,
                  borderWidth: 1,
                  borderRadius: 8,
                  marginTop: -60,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {this.state.jobCoverImage && this.state.jobCoverImage.uri ? (
                  <Image
                    source={{ uri: this.state.jobCoverImage.uri }}
                    style={{ height: 150, width: 300, borderRadius: 8 }}
                  />
                ) : (
                  <View
                    style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <View
                      style={{
                        height: 42,
                        width: 42,
                        backgroundColor: COLORS.altgreen_250,
                        borderRadius: 21,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Icon
                        name="UploadPhoto"
                        size={18}
                        color={COLORS.dark_600}
                        style={{ marginTop: Platform.OS === 'ios' ? 0 : 8 }}
                      />
                    </View>

                    <Text
                      style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>
                      ADD COVER IMAGE
                    </Text>
                    <Text
                      style={[defaultStyle.Caption, { color: '#91b3a2', fontSize: 10 }]}>
                      300PX BY 150px RECOMMENDED
                    </Text>
                  </View>
                )}
              </TouchableOpacity>

              <Icon
                name="Company"
                size={18}
                color="#698F8A"
                style={{ marginTop: Platform.OS === 'ios' ? 6 : 14 }}
              />

              <Text
                style={[
                  defaultStyle.Caption,
                  {
                    color: COLORS.altgreen_300,
                    marginTop: Platform.OS === 'ios' ? 8 : -10,
                  },
                ]}>
                ORGANIZATION DETAIL
              </Text>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 6,
                  paddingRight: 12,
                  marginVertical: 10,
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label={<Text>Organization Name <Text style={{ color: 'red' }}>*</Text></Text>}
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Subtitle_1,
                    {
                      width: '90%',
                      backgroundColor: COLORS.bgfill_200,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) =>
                    this.setState({ organizationName: value })
                  }
                  value={this.state.organizationName}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label="Organization Website"
                  placeholder='https://'
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Subtitle_1,
                    {
                      width: '90%',
                      backgroundColor: COLORS.bgfill_200,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) =>
                    this.setState({ organizationWebsite: value })
                  }
                  value={this.state.organizationWebsite}
                />
              </View>
            </View>

            <View
              style={{
                backgroundColor: COLORS.altgreen_50,
                alignItems: 'center',
                width: '100%',
                paddingBottom: 16,
                borderBottomColor: '#0000001A',
                borderBottomWidth: 0.5,
                borderTopColor: '#0000001A',
                borderTopWidth: 0.5,
                paddingBottom: 20,
                paddingTop: 4,
              }}>
              <Icon
                name="Task"
                size={18}
                color="#698F8A"
                style={{ marginTop: Platform.OS === 'ios' ? 6 : 14 }}
              />

              <Text
                style={[
                  defaultStyle.Caption,
                  {
                    color: COLORS.altgreen_300,
                    marginTop: Platform.OS === 'ios' ? 8 : -10,
                  },
                ]}>
                JOB DETAIL
              </Text>

              <TouchableOpacity
                onPress={() => this.setState({ employmentTypeModalOpen: true })}
                activeOpacity={0.6}
                style={{
                  height: 44,
                  width: '86%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 20,
                  borderWidth: 1,
                  borderColor: COLORS.grey_300,
                  borderRadius: 8,
                  paddingHorizontal: 10,
                }}>
                {/* <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>Circle Type</Text> */}
                <Text
                  style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                  {this.state.selectedEmploymentType !== ''
                    ? this.state.selectedEmploymentType
                    : 'Employment Type'}
                </Text>

                {/* <View style={{ width: '60%', height: 26, borderRadius: 13, justifyContent: 'center', alignItems: 'center', marginTop: 6 }}>

                            </View> */}

                <View
                  style={{
                    width: 16,
                    height: 16,
                    borderRadius: 8,
                    backgroundColor: '#698F8A',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 0,
                  }}>
                  <Icon
                    name="Arrow_Down"
                    size={9}
                    color="#FFF"
                    style={{ marginTop: Platform.OS === 'android' ? 4 : 0 }}
                  />
                </View>
              </TouchableOpacity>
              {/* <View style={styles.border2}></View> */}

              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  this.secondTextInput.focus()
                }}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 6,
                  paddingRight: 12,
                  marginTop: 20,
                  backgroundColor: COLORS.white,
                  borderRadius: 10,
                }}>
                <TextInput
                  ref={(input) => {
                    this.secondTextInput = input
                  }}
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label={<Text>Brief Description <Text style={{ color: 'red' }}>*</Text></Text>}
                  multiline
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Subtitle_1,
                    {
                      minHeight: 200,
                      width: '90%',
                      backgroundColor: COLORS.white,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) =>
                    this.setState({ briefDescription: value })
                  }
                  value={this.state.briefDescription}
                />
              </TouchableOpacity>
            </View>

            <Icon
              name="Location"
              size={18}
              color="#698F8A"
              style={{ marginTop: Platform.OS === 'ios' ? 6 : 14 }}
            />

            <Text
              style={[
                defaultStyle.Caption,
                {
                  color: COLORS.altgreen_300,
                  marginTop: Platform.OS === 'ios' ? 8 : -10,
                },
              ]}>
              LOCATION DETAIL
            </Text>

            {/* <CreateCircle changeState={this.changeState} /> */}
            <CreateCircle
              country={this.state.country}
              state={this.state.state}
              city={this.state.city}
              changeState={this.changeState}
            />

            <View
              style={{
                backgroundColor: COLORS.white,
                alignItems: 'center',
                width: '100%',
                paddingBottom: 24,
                marginTop: 0,
              }}>
              <TouchableOpacity
                activeOpacity={0.6}
                style={{
                  height: 56,
                  width: '86%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 20,
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <View style={{ width: '40%' }}>
                  <Text
                    style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                    Start Date <Text style={{ color: 'red' }}>*</Text>
                  </Text>
                  <DatePicker
                    style={{ width: '100%', borderWidth: 0 }}
                    date={
                      this.state.startDate === 0
                        ? ''
                        : this.unixTimeDatePicker(this.state.startDate)
                    }
                    mode="date"
                    placeholder="Start Date"
                    format="DD-MM-YYYY"
                    minDate="01-01-1900"
                    maxDate={this.currentDate()}
                    // maxDate="16-07-2021"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        top: -10,
                        marginRight: 0,
                      },
                      dateInput: {
                        alignItems: 'flex-start',
                        borderWidth: 0,
                      },
                      dateText: {
                        color: COLORS.dark_700,
                        marginTop: -20,
                      },
                    }}
                    onDateChange={(date) => {
                      let tempdate = date.split('-').reverse().join('-');
                      let convertedUnixTime = new Date(tempdate).getTime();
                      this.setState({
                        startDate: convertedUnixTime,
                        seconds: 59,
                        endDate:
                          convertedUnixTime >
                            new Date(this.state.endDate).getTime()
                            ? 0
                            : this.state.endDate,
                      });
                    }}
                  />
                </View>

                {this.state.startDate ? (
                  <View style={{ width: '40%' }}>
                    <Text
                      style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                      End Date
                    </Text>
                    <DatePicker
                      style={{ width: '100%', borderWidth: 0 }}
                      date={
                        this.state.endDate === 0
                          ? ''
                          : this.unixTimeDatePicker(this.state.endDate)
                      }
                      mode="date"
                      placeholder="End Date"
                      format="DD-MM-YYYY"
                      minDate={this.getMinEndDate()}
                      maxDate={this.currentDate()}
                      // maxDate="16-07-2021"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          right: 0,
                          top: -10,
                          marginRight: 0,
                        },
                        dateInput: {
                          alignItems: 'flex-start',
                          borderWidth: 0,
                        },
                        dateText: {
                          color: COLORS.dark_700,
                          marginTop: -20,
                        },
                      }}
                      onDateChange={(date) => {
                        let tempdate = date.split('-').reverse().join('-');
                        let convertedUnixTime = new Date(
                          tempdate,
                        ).getTime();
                        this.setState({
                          endDate: convertedUnixTime,
                          seconds: 59,
                        });
                      }}
                    />
                  </View>) : null}
                {/* <Icon name="Calender_OL" color='#698F8A' size={16} style={{ marginTop: Platform.OS === 'android' ? 12 : 0, zIndex: 2 }} /> */}
              </TouchableOpacity>
              {/* <View style={styles.border2}></View> */}
            </View>

            <View
              style={{
                height: 83,
                width: '86%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-evenly',
                marginTop: 16,
                paddingLeft: 0,
                paddingRight: 0,
                backgroundColor: COLORS.altgreen_100,
                borderRadius: 8,
              }}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({ acceptPolicy: !this.state.acceptPolicy })
                }
                activeOpacity={0.6}
                style={{
                  width: 20,
                  height: 20,
                  borderRadius: 10,
                  borderColor: COLORS.green_500,
                  borderWidth: 2,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: this.state.acceptPolicy
                    ? COLORS.green_500
                    : COLORS.altgreen_t50,
                }}>
                {this.state.acceptPolicy ? (
                  <Icon
                    name="Tick"
                    size={10}
                    color={COLORS.dark_800}
                    style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                  />
                ) : (
                  <></>
                )}
              </TouchableOpacity>

              <View>
                <Text style={[defaultStyle.Caption, { color: '#698F8A' }]}>
                  I accept the WeNaturalists Terms &
                </Text>
                <Text style={[defaultStyle.Caption, { color: '#698F8A' }]}>
                  Conditions, User Agreement, Privacy
                </Text>
                <Text style={[defaultStyle.Caption, { color: '#698F8A' }]}>
                  Policy, and Cookie Policy.
                </Text>
              </View>

              <TouchableOpacity
                onPress={() => this.setState({ modalterm: true, addJobModalOpen: false })}
                activeOpacity={0.5}
                style={{
                  height: 30,
                  width: 30,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Icon
                  name="Export"
                  size={16}
                  color={COLORS.altgreen_300}
                  style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </Modal>
    )
  }

  getMinEndDate = () => {
    var day = new Date(this.state.startDate)

    var nextDay = new Date(day)
    nextDay.setDate(day.getDate())
    return nextDay
  }

  changeState = (value) => {
    this.setState(value)
  }

  render() {
    console.log('this.state.startDate', this.state.startDate)
    return (
      <SafeAreaView style={{ flex: 1 }}>
        {this.addJobModal()}
        {this.shareModal()}
        {this.employmentTypeModal()}
        {this.termsModal()}

        {/***** Header starts *****/}

        <ProfileEditHeader
          name="Experience"
          iconName="Jobs"
          goback={this.goback}
          addButton={true}
          addButtonFunction={this.addButtonFunction}
        />

        {/***** Header ends *****/}
        {this.props.userExperience.body && this.props.userExperience.body.experience.content.length ?

          <View style={{ marginBottom: 60 }}>
            <FlatList
              style={{ paddingTop: 6 }}
              keyExtractor={(item) => item.id}
              contentContainerStyle={{ paddingBottom: 20 }}
              data={
                this.props.userExperience.body
                  ? this.props.userExperience.body.experience.content
                  : []
              }
              renderItem={({ item }) => this.renderItem(item)}
            />
          </View>
          :
          <Icon
            name="EditBox"
            size={52}
            color={COLORS.altgreen_300}
            style={{ marginTop: Platform.OS === 'android' ? 10 : 0, marginTop: '40%', alignSelf: 'center' }}
          />

        }
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  },
  item: {
    flexDirection: 'row',
    width: '83%',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'left',
    marginLeft: 30,
    // marginRight: 100,
    paddingHorizontal: '3%',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey_200,
    // backgroundColor: 'pink'
  },
  linearGradientView: {
    width: '100%',
    height: 500,
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
  },
  termsmodal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '85%',
    paddingVertical: 11,
    borderBottomWidth: 0.55,
    borderBottomColor: '#E8ECEB',
  },
  modalText: {
    fontSize: 14,
    color: '#154A59',
    fontFamily: 'Montserrat-SemiBold',
  },
  modalCategoriesContainer: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    paddingVertical: 20,
    height: 250,
  },
  crossIcon: {
    marginTop: 5,
  },
  crossButtonContainer: {
    alignSelf: 'center',
    width: 42,
    height: 42,
    borderRadius: 21,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E7F3E3',
    marginBottom: 10,
  },
  modalCountry: {
    marginTop: 'auto',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  experienceCard: {
    width: '90%',
    marginVertical: 10,
    backgroundColor: '#fff',
    borderRadius: 8,
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  expCardHeader: {
    flexDirection: 'row',
    marginTop: 5,
    height: 80,
    alignItems: 'center',
    borderBottomColor: '#90949C',
    borderBottomWidth: 0.5,
    marginLeft: 15,
  },
  expTitle: {
    marginLeft: 12,
    color: '#00394D',
    fontSize: 14,
    fontFamily: 'Montserrat-Bold',
  },
  expCompanyName: {
    width: 200,
    marginLeft: 12,
    color: '#698F8A',
    fontSize: 12,
    fontFamily: 'Montserrat-SemiBold',
  },
  expLocation: {
    marginLeft: 12,
    color: '#90949C',
    fontSize: 12,
    fontFamily: 'Montserrat-Medium',
  },
  expDescription: {
    flexWrap: 'wrap',
    color: '#607580',
    fontSize: 12,
    margin: 15,
  },
  updateButton: {
    flexDirection: 'row',
    // width: 110,
    paddingHorizontal: 10,
    height: 28,
    borderRadius: 17,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: COLORS.dark_700,
  },
});

const mapStateToProps = (state) => {
  return {
    userExperienceProgress: state.personalProfileReducer.userExperienceProgress,
    userExperience: state.personalProfileReducer.userExperience,
    errorExperience: state.personalProfileReducer.errorExperience,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalExperienceRequest: (data) =>
      dispatch(personalExperienceRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExperienceEdit);
