import React, { Component } from 'react';
import {
  FlatList,
  TouchableHighlight,
  Modal,
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  StyleSheet,
  ScrollView,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  Linking
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import Svg, { Image, Polygon, Defs, ClipPath } from 'react-native-svg';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import { TextInput } from 'react-native-paper';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Snackbar from 'react-native-snackbar';
import * as _ from 'lodash';
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-crop-picker';
import Autolink from 'react-native-autolink'
import { tagDescription } from '../../../Components/Shared/commonFunction'

import SuggestedHashTags from '../../../Components/User/Common/SuggestedHashTags';
import CreateCircle from '../../../Components/User/EditProfile/CreateCircle';
import CountryCode from '../../../Json/CountryCode.json';
import {
  personalProfileRequest,
  personalAddressRequest,
} from '../../../services/Redux/Actions/User/PersonalProfileActions';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import { COLORS } from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultCover from '../../../../assets/defaultCover.png';
import defaultStyle from '../../../Components/Shared/Typography';
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader';
import EditUserAddress from '../../../Components/User/EditProfile/EditUserAddress';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const windowWidth = Dimensions.get('window').width;

class PersonalInfoEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      suggestedHashTagsModalOpen: false,
      hashTags: [],
      userId: '',
      isLoading: false,
      websiteIcon: true,
      query: '',
      countryCodes: [],
      otp: '',
      transactionId: '',
      dob:
        this.props.user.body &&
          this.props.user.body.dob &&
          this.props.user.body.dob !== 0
          ? this.props.user.body.dob
          : '',
      coverImage:
        this.props.user.body && this.props.user.body.originalCoverImage
          ? this.props.user.body.originalCoverImage
          : '',
      profileImage:
        this.props.user.body && this.props.user.body.originalProfileImage
          ? this.props.user.body.originalProfileImage
          : '',

      editBioModalOpen: false,
      editWebsiteModalOpen: false,
      editEmailModalOpen: false,
      editPhoneNoModalOpen: false,
      editCustomUrlModalOpen: false,
      editPersonalInfoModalOpen: false,
      editUserAddressModalOpen: false,
      whatDefinesYouModalOpen: false,
      modalCountry: false,
      phoneOtpModal: false,
      emailOtpModal: false,

      writeSomething: this.props.user.body ? this.props.user.body.bio : '',
      websiteUrl: this.props.user.body ? this.props.user.body.website : '',
      emailAddress: this.props.user.body ? this.props.user.body.email : '',
      phoneNumber: this.props.user.body ? this.props.user.body.mobile : '',
      selectedCountryCode:
        this.props.user.body && this.props.user.body.countryCode
          ? this.props.user.body.countryCode
          : 'Code',
      customUrl:
        this.props.user.body && this.props.user.body.customUrl
          ? this.props.user.body.customUrl
          : '',
      firstName:
        this.props.user.body && this.props.user.body.firstName
          ? this.props.user.body.firstName
          : '',
      lastName:
        this.props.user.body && this.props.user.body.lastName
          ? this.props.user.body.lastName.charAt(0).toUpperCase() +
          this.props.user.body.lastName.slice(1)
          : '',
      persona: this.props.user.body ? this.props.user.body.persona : '',
      dateOfBirth: this.props.user.body ? this.props.user.body.dob : '',
      address: this.props.userAddress.body
        ? this.props.userAddress.body.otherDetails
        : '',
      country: this.props.userAddress.body
        ? this.props.userAddress.body.country
        : '',
      state: this.props.userAddress.body
        ? this.props.userAddress.body.state
        : '',
      city: this.props.userAddress.body ? this.props.userAddress.body.city : '',

      whatDefinesYouSuggestions: [],
      searchWhatDefinesYou: '',
      seconds: 59,
      minutes: 9
    }
  }

  componentDidMount() {
    if (this.state.countryCodes.length === 0)
      CountryCode.sort((a, b) => a.Dial - b.Dial).map((item) => {
        if (item.Name !== 'select') {
          this.state.countryCodes.push({
            label: item.Unicode + ' +' + item.Dial + ' ' + item.Name,
            value: { code: item.Dial, country: item.Name },
          });
        }
      });

    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('LoginWebview');
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({ userId: value })

        this.props.personalProfileRequest({ userId: value, otherUserId: '' });
        this.props.personalAddressRequest({ userId: value, otherUserId: '' });
      })
      .catch((e) => {
        console.log(e);
      });

    this.getMasterConfig();

    this.interval = setInterval(
      () => this.setState((prevState) => ({ seconds: prevState.seconds - 1 })),
      1000
    )
  }

  componentDidUpdate() {

    if (this.state.minutes === 0 && this.state.seconds === 0) {
      clearInterval(this.interval);
    }
    if (this.state.seconds === -1) {
      this.setState({ minutes: this.state.minutes - 1, seconds: 59 })
    }
    if (this.state.seconds.toString().length === 1) {
      this.setState({ seconds: '0' + this.state.seconds })
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  goback = () => {
    this.props.navigation.goBack();
  }

  unixTime = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();

    return day + ' ' + month + ' ' + year;
  };

  unixTimeDatePicker = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();

    return day + '-' + month + '-' + year;
  };

  currentDate = () => {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = dd + '-' + mm + '-' + yyyy;

    return today;
  };

  //**************** persona suggesstions api call starts ******************//

  getMasterConfig = () => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/master/config/get',
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          this.setState({ whatDefinesYouSuggestions: res.body.fields.persona });
        }
      })
      .catch((err) => {
        console.log('persona suggesstion error: ', err);
      });
  };

  //**************** persona suggesstions api call ends ******************//

  //**************** Custom url validation and api call starts ******************//

  validateCustomUrl() {
    const customUrl = /^[A-Za-z][A-Za-z0-9]*$/;
    let formIsValid = true;

    if (
      _.isUndefined(this.state.customUrl) ||
      _.isEmpty(this.state.customUrl) ||
      _.isNull(this.state.customUrl)
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'customUrl is mandatory',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      this.setState({ isLoading: false });
      formIsValid = false;
    } else if (!customUrl.test(this.state.customUrl)) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Please enter a valid Url',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      this.setState({ isLoading: false });
      formIsValid = false;
    } else if (
      this.state.customUrl.trim().replace(' ', '').length <= 1 ||
      this.state.customUrl.trim().replace(' ', '').length > 20
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text:
          'URL should have more than 1 character and less than 20 Characters',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      this.setState({ isLoading: false });
      formIsValid = false;
    }

    return formIsValid;
  }

  submitCustomUrlDetail = () => {
    if (this.validateCustomUrl()) {
      this.setState({ isLoading: true });
      let postBody = {
        userId: this.state.userId,
        customUrl: this.state.customUrl.toLowerCase(),
      };
      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/profile/update/custom/url',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (res.status === '200 OK') {
            this.setState({ editCustomUrlModalOpen: false, isLoading: false });
            this.props.personalProfileRequest({
              userId: this.state.userId,
              otherUserId: '',
            });
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            Snackbar.show({
              backgroundColor: COLORS.alert_red,
              text: 'Custom url already exists',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    }
  };

  //**************** Custom url validation and api call ends ******************//

  //**************** Bio validation and api call starts ******************//

  validateBioDetails = () => {
    let formIsValid = true;
    if (
      this.state.writeSomething === null ||
      _.isUndefined(this.state.writeSomething.trim()) ||
      _.isEmpty(this.state.writeSomething.trim()) ||
      _.isNull(this.state.writeSomething.trim())
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Bio cannot be empty',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      this.setState({ isLoading: false });
      formIsValid = false;
    }
    return formIsValid;
  };

  submitBioDetail = () => {
    if (this.validateBioDetails()) {
      this.setState({ isLoading: true });
      let postBody = {
        userId: this.state.userId,
        bio: this.state.writeSomething.trim(),
      };
      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/profile/update/bio',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (res.status === '200 OK') {
            this.setState({ isLoading: false, editBioModalOpen: false });
            this.props.personalProfileRequest({
              userId: this.state.userId,
              otherUserId: '',
            });
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            this.setState({ isLoading: false });
            Snackbar.show({
              backgroundColor: COLORS.alert_red,
              text: err.response.data.message,
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    }
  };

  //**************** Bio validation and api call ends ******************//

  //**************** website validation and api call starts ******************//

  validateUrl(value) {
    return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(
      value,
    );
  }

  validateWebsite() {
    let formIsValid = true;

    if (this.state.websiteUrl && !this.validateUrl(this.state.websiteUrl)) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Please provide a valid website url',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      formIsValid = false;
    }
    return formIsValid;
  }

  submitWebsiteDetail = () => {
    if (this.validateWebsite()) {
      this.setState({ isLoading: true });
      let postBody = {
        userId: this.state.userId,
        website: this.state.websiteUrl,
      };
      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/profile/update/website',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (res.status === '200 OK') {
            this.setState({ isLoading: false, editWebsiteModalOpen: false });
            this.props.personalProfileRequest({
              userId: this.state.userId,
              otherUserId: '',
            });
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            this.setState({ isLoading: false });
            Snackbar.show({
              backgroundColor: COLORS.alert_red,
              text: err.response.data.message,
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    }
  };

  //**************** website validation and api call ends ******************//

  //**************** personal info validation and api call starts ******************//

  validateForm() {
    let formIsValid = true;

    if (this.state.lastName === '') {
      if (
        _.isUndefined(this.state.lastName) ||
        _.isEmpty((this.state.lastName || '').toString()) ||
        _.isNull(this.state.lastName)
      ) {
        Snackbar.show({
          backgroundColor: COLORS.alert_red,
          text: 'Please enter your last name',
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        });
        formIsValid = false;
      }
    } else {
      const ln = /^[a-zA-Z\s]+$/;
      if (this.state.lastName === '' || ln.test(this.state.lastName)) {
        this.setState({ lastName: this.state.lastName });
      }
      if (
        this.state.lastName.trim().replace(' ', '').length > 25 ||
        this.state.lastName.trim().replace(' ', '').length < 2
      ) {
        Snackbar.show({
          backgroundColor: COLORS.alert_red,
          text: 'Last name must have minimum 2 characters',
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        });
        formIsValid = false;
      }
    }

    if (this.state.firstName === '') {
      if (
        _.isUndefined(this.state.firstName) ||
        _.isEmpty((this.state.firstName || '').toString()) ||
        _.isNull(this.state.firstName)
      ) {
        Snackbar.show({
          backgroundColor: COLORS.alert_red,
          text: 'Please enter your first name',
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        });
        formIsValid = false;
      }
    } else {
      const fn = /^[a-zA-Z\s]+$/;
      if (this.state.firstName === '' || fn.test(this.state.firstName)) {
        this.setState({ firstName: this.state.firstName });
      }

      if (
        this.state.firstName.trim().replace(' ', '').length > 25 ||
        this.state.firstName.trim().replace(' ', '').length < 2
      ) {
        Snackbar.show({
          backgroundColor: COLORS.alert_red,
          text: 'First name must have minimum 2 characters',
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        });
        formIsValid = false;
      }
    }

    return formIsValid;
  }

  submitProfileDetail = () => {
    this.setState({ isLoading: true });

    let tempdob =
      typeof this.state.dob == 'string' &&
      this.state.dob.split('-').reverse().join('-');
    let convertedUnixTime = new Date(tempdob).getTime();

    if (this.validateForm()) {
      let postBody = {
        userId: this.state.userId,
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        dob:
          typeof this.state.dob == 'string'
            ? convertedUnixTime
            : this.state.dateOfBirth,
        persona: this.state.persona,
      };
      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/profile/update/personal/info',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (res.status === '200 OK') {
            this.setState({ isLoading: false, editPersonalInfoModalOpen: false });
            this.props.personalProfileRequest({
              userId: this.state.userId,
              otherUserId: '',
            });
            typeof this.state.dob == 'string' &&
              this.setState({ dateOfBirth: convertedUnixTime });
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            this.setState({ isLoading: false });
            Snackbar.show({
              backgroundColor: COLORS.alert_red,
              text: err.response.data.message,
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    }
  };

  //**************** personal info validation and api call ends ******************//

  //**************** profile & cover image update api call starts ******************//

  openPicker = () => {
    ImagePicker.openPicker({
      width: windowWidth * 0.9,
      height: 217,
      cropping: true,
      mediaType: 'photo',
    }).then((image) => {
      console.log(image);
      this.onCropComplete(image, 'cover');
    });
  };

  openPickerProfile = () => {
    ImagePicker.openPicker({
      width: 110,
      height: 145,
      cropping: true,
      mediaType: 'photo',
    }).then((image) => {
      console.log(image);
      this.onCropComplete(image, 'profile');
    });
  };

  onCropComplete = (crop, value) => {
    value === 'cover'
      ? this.setState({ coverImage: crop.path })
      : this.setState({ profileImage: crop.path });

    let photo = {
      uri: crop.path,
      type: crop.mime,
      name: crop.path.split('-').pop(),
    };

    this.setState({ isLoading: true });
    const formData = new FormData();
    value === 'cover'
      ? formData.append('coverImage', photo)
      : formData.append('profileImage', photo);
    formData.append('userId', this.state.userId);
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/profile/update/images',
      headers: { 'Content-Type': 'multipart/form-data' },
      data: formData,
      withCredentials: true
    })
      .then((response) => {
        let res = response.data;
        console.log('update cover: ', res)
        if (res.message === 'Success!') {
          this.setState({ isLoading: false })
          this.props.personalProfileRequest({
            userId: this.state.userId,
            otherUserId: '',
          })
          // this.fetchUserImage()
        }
      })
      .catch((err) => {
        this.setState({ isLoading: false })
        console.log('error in update cover: ', err)
      })
  }

  fetchUserImage = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get?id=' +
        this.state.userId +
        '&otherUserId=' +
        '',
      cache: true,
      withCredentials: true,
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        // this.setState({ profileimg: response.data.body.profileImage })
        // console.log("Profile data response : ", response)
      })
      .catch((err) => console.log('Profile data error : ', err));
  };

  //**************** profile & cover image update api call ends ******************//

  //**************** delete profile api call starts ******************//

  deleteProfilePic = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/delete/profile/pic?userId=' +
        this.state.userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '200 OK') {
          this.props.personalProfileRequest({
            userId: this.state.userId,
            otherUserId: '',
          });
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          this.setState({ isLoading: false });
          console.log('error in delete profile pic: ', err);
        }
      });
  };

  //**************** delete profile api call ends ******************//

  //**************** Delete cover image api call starts ******************//

  deleteCoverPic = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/delete/baner/image?userId=' +
        this.state.userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '200 OK') {
          this.props.personalProfileRequest({
            userId: this.state.userId,
            otherUserId: '',
          });
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          this.setState({ isLoading: false });
          console.log('error in delete cover pic: ', err);
        }
      });
  };

  //**************** Delete cover image api call ends ******************//

  //**************** email update api call & validation starts ******************//

  validateEmail() {
    let formIsValid = true;
    if (
      _.isUndefined(this.state.emailAddress) ||
      _.isEmpty((this.state.emailAddress || '').toString()) ||
      _.isNull(this.state.emailAddress)
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Please enter your email address',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      this.setState({ isLoading: false });
      formIsValid = false;
    } else {
      const email = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
      if (!email.test(this.state.emailAddress)) {
        Snackbar.show({
          backgroundColor: COLORS.alert_red,
          text: 'Please enter a valid email address',
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        });
        this.setState({ isLoading: false });
        formIsValid = false;
      }
    }
    return formIsValid;
  }

  submitEmailValidation = () => {
    if (this.validateEmail()) {
      let postBody = {
        userId: this.state.userId,
        email: this.state.emailAddress,
      };
      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/user/reset/email/',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (response.status === 202) {
            this.setState({
              transactionId: res.body.transactionId,
              editEmailModalOpen: false,
              emailOtpModal: true,
              seconds: 59
            });
          }
        })
        .catch((err) => {
          if (err) {
            if (err && err.response && err.response.status == 409) {
              Snackbar.show({
                backgroundColor: COLORS.alert_red,
                text:
                  'This email address is already registered, please try using different email address',
                textColor: COLORS.altgreen_100,
                duration: Snackbar.LENGTH_LONG,
              });
            } else if (err.response && err.response.data) {
              Snackbar.show({
                backgroundColor: COLORS.alert_red,
                text: err.response.data.message,
                textColor: COLORS.altgreen_100,
                duration: Snackbar.LENGTH_LONG,
              });
            }
          }
        });
    }
  };

  handleOtpSubmit = () => {
    let postBody = {
      userId: this.state.userId,
      transactionId: this.state.transactionId,
      otp: this.state.otp,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/otp/verify/',
      headers: { 'Content-Type': 'application/json' },
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        if (response.status === 202) {
          let postBodyNew = {
            userId: this.state.userId,
            email: this.state.emailAddress,
          };
          axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/profile/update/email/',
            headers: { 'Content-Type': 'application/json' },
            data: postBodyNew,
            withCredentials: true,
          })
            .then((newresponse) => {
              if (newresponse.status === 202) {
                this.props.personalProfileRequest({
                  userId: this.state.userId,
                  otherUserId: '',
                });
                this.setState({ emailOtpModal: false, seconds: 59, minutes: 9 })
              }
            })
            .catch((err) => {
              if (err && err.response.data) {
                Snackbar.show({
                  backgroundColor: COLORS.alert_red,
                  text: err.response.data.message,
                  textColor: COLORS.altgreen_100,
                  duration: Snackbar.LENGTH_LONG,
                });
              }
            });
        }
      })
      .catch((err) => {
        if (err && err.response.data) {
          Snackbar.show({
            backgroundColor: COLORS.alert_red,
            text: err.response.data.message,
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        }

      });
    setTimeout(() => {
      this.clearOtp()
    }, 2000)
  }

  clearOtp = () => {
    this.setState({ otp: '' });
  };

  //**************** email update api call & validation ends ******************//

  //**************** Phone number update api call & validation starts ******************//

  validatePhoneForm() {
    let formIsValid = true;

    if (
      _.isUndefined(this.state.selectedCountryCode) ||
      _.isEmpty(this.state.selectedCountryCode) ||
      _.isNull(this.state.selectedCountryCode)
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Please select the country code',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      formIsValid = false;
    } else if (
      _.isUndefined(this.state.phoneNumber) ||
      _.isEmpty((this.state.phoneNumber || '').toString()) ||
      _.isNull(this.state.phoneNumber)
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Please enter your phone number',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      formIsValid = false;
    } else {
      const re = /^[0-9\b]+$/;
      if (this.state.phoneNumber === '' || re.test(this.state.phoneNumber)) {
        this.setState({ phoneNumber: this.state.phoneNumber });
      } else if (
        this.state.selectedCountryCode === '91' &&
        this.state.phoneNumber
      ) {
        const regExpPhone = /^[6-9\b]+$/;
        if (
          this.state.phoneNumber.trim().replace(' ', '').length > 10 ||
          this.state.phoneNumber.trim().replace(' ', '').length < 10 ||
          !regExpPhone.test(this.state.phoneNumber[0])
        ) {
          Snackbar.show({
            backgroundColor: COLORS.alert_red,
            text: 'Please enter valid mobile number',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          formIsValid = false;
        }
      }
    }

    return formIsValid;
  }

  verifyMobile = () => {
    if (this.validatePhoneForm()) {
      axios({
        method: 'get',
        url:
          REACT_APP_userServiceURL +
          '/profile/check/mobile/detail?id=' +
          this.state.userId +
          '&mobile=' +
          this.state.phoneNumber +
          '&countryCode=' +
          this.state.selectedCountryCode,
        headers: { 'Content-Type': 'application/json' },
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (res.message === 'Success!') {
            this.setState({
              isLoading: false,
              transactionId: res.body.transactionId,
              editPhoneNoModalOpen: false,
              phoneOtpModal: true,
              seconds: 59
            });
          } else if (res.status === '409 CONFLICT') {
            this.setState({ isLoading: false });
            Snackbar.show({
              backgroundColor: COLORS.alert_red,
              text: res.message,
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            this.setState({ isLoading: false });
            Snackbar.show({
              backgroundColor: COLORS.alert_red,
              text: err.response.data.message,
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    }
  };

  handleUpdatePhone = () => {
    this.setState({ isLoading: true });
    let postBody = {
      userId: this.state.userId,
      transactionId: this.state.transactionId,
      countryISDCode: this.state.selectedCountryCode,
      otp: this.state.otp,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/otp/change/mobile/verify',
      headers: { 'Content-Type': 'application/json' },
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          this.setState({ isLoading: false, phoneOtpModal: false, seconds: 59, minutes: 9 });
          Snackbar.show({
            backgroundColor: COLORS.dark_900,
            text: 'Phone number updated successfully',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          this.props.personalProfileRequest({
            userId: this.state.userId,
            otherUserId: '',
          });
        }
      })
      .catch((err) => {
        if (err && err.response.data) {
          this.setState({ isLoading: false });
          Snackbar.show({
            backgroundColor: COLORS.alert_red,
            text: err.response.data.message,
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          })
        }
      })
    setTimeout(() => {
      this.clearOtp()
    }, 2000)
  }

  //**************** Phone number update api call & validation ends ******************//

  editBioModal = () => {
    return (
      <Modal
        visible={this.state.editBioModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={styles.linearGradientView}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={styles.linearGradient}></LinearGradient>
          </View>

          <ScrollView keyboardShouldPersistTaps="handled">
            <TouchableOpacity
              activeOpacity={0.7}
              style={defaultShape.CloseBtn}
              onPress={() =>
                this.setState({
                  editBioModalOpen: false,
                  writeSomething: this.props.user.body
                    ? this.props.user.body.bio
                    : '',
                })
              }>
              <Icon
                name="Cross"
                size={13}
                color="#367681"
                style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
              />
            </TouchableOpacity>

            <View
              style={[
                defaultShape.Modal_Categories_Container,
                { height: 450, backgroundColor: COLORS.bgfill },
              ]}>
              <View style={styles.modalHeaderView}>
                <Text
                  style={[
                    defaultStyle.Button_2,
                    { color: COLORS.dark_900, textAlign: 'center' },
                  ]}>
                  EDIT BIO
                </Text>
              </View>

              <TextInput
                theme={{
                  colors: {
                    text: COLORS.dark_600,
                    primary: COLORS.bgfill,
                    placeholder: COLORS.altgreen_300,
                  },
                }}
                // label="Write something interesting"
                placeholder="Add Bio"
                // autoFocus={true}
                multiline
                selectionColor="#C8DB6E"
                style={[
                  defaultStyle.H6,
                  {
                    height: '76%',
                    backgroundColor: COLORS.bgfill,
                    color: COLORS.dark_600,
                    marginTop: '12%',
                    marginHorizontal: '2%',
                    alignSelf: 'flex-start',
                    textAlign: 'left',
                  },
                ]}
                onChangeText={(value) => this.setState({ writeSomething: value, suggestedHashTagsModalOpen: value[value.length - 1] === '#' ? true : false, editBioModalOpen: value[value.length - 1] === '#' ? false : true })}
                value={this.state.writeSomething}
              />

              <View style={styles.border}></View>

              <TouchableOpacity
                onPress={this.submitBioDetail}
                activeOpacity={0.7}
                style={styles.updateButton}>
                <Text
                  style={[defaultStyle.Caption, { color: COLORS.altgreen_100 }]}>
                  Update
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      </Modal>
    )
  }

  changeHashTagsState = (value) => {
    this.setState(value)
  }

  suggestedHashTagsModal = () => {
    return (
      <Modal
        visible={this.state.suggestedHashTagsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <SuggestedHashTags
          changeHashTagsState={this.changeHashTagsState}
          hashTags={this.state.hashTags}
          writeSomething={this.state.writeSomething}
        />
      </Modal>
    )
  }

  editWebsiteModal = () => {
    return (
      <Modal
        visible={this.state.editWebsiteModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{ flex: 1 }}>
          <View style={{ marginTop: 'auto' }}>
            <View style={styles.linearGradientView}>
              <LinearGradient
                colors={['#154A5900', '#154A59CC', '#154A59']}
                style={styles.linearGradient}></LinearGradient>
            </View>

            <TouchableOpacity
              activeOpacity={0.7}
              style={defaultShape.CloseBtn}
              onPress={() =>
                this.setState({
                  editWebsiteModalOpen: false,
                  websiteUrl: this.props.user.body
                    ? this.props.user.body.website
                    : '',
                })
              }>
              <Icon
                name="Cross"
                size={13}
                color="#367681"
                style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
              />
            </TouchableOpacity>

            <View
              style={[
                defaultShape.Modal_Categories_Container,
                { height: 250, backgroundColor: COLORS.bgfill },
              ]}>
              <View style={styles.modalHeaderView}>
                <Text
                  style={[
                    defaultStyle.Button_2,
                    { color: COLORS.dark_900, textAlign: 'center' },
                  ]}>
                  EDIT WEBSITE
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: 56,
                  marginLeft: 10,
                }}>
                {this.state.websiteIcon && this.state.websiteUrl === '' ? (
                  <Icon
                    name="Globe"
                    color={COLORS.primarydark}
                    size={12}
                    style={{
                      marginTop: Platform.OS === 'android' ? 15 : 0,
                      marginRight: -20,
                      zIndex: 2,
                    }}
                  />
                ) : (
                  <></>
                )}

                <TextInput
                  theme={{
                    colors: {
                      text: '#00394d',
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label="     Paste URL or type"
                  mode="outlined"
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Body_1,
                    {
                      width: '90%',
                      height: 34,
                      backgroundColor: COLORS.altgreen_t50,
                      color: COLORS.dark_600,
                    },
                  ]}
                  onChangeText={(value) => this.setState({ websiteUrl: value })}
                  value={this.state.websiteUrl}
                  onFocus={() => this.setState({ websiteIcon: false })}
                  onBlur={() => this.setState({ websiteIcon: true })}
                />
              </View>

              <TouchableOpacity
                onPress={this.submitWebsiteDetail}
                activeOpacity={0.7}
                style={[styles.updateButton, { marginTop: 60 }]}>
                <Text
                  style={[defaultStyle.Caption, { color: COLORS.altgreen_100 }]}>
                  Update
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  }

  editEmailModal = () => {
    return (
      <Modal
        visible={this.state.editEmailModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{ flex: 1 }}>
          <View style={{ marginTop: 'auto' }}>
            <View style={styles.linearGradientView}>
              <LinearGradient
                colors={['#154A5900', '#154A59CC', '#154A59']}
                style={styles.linearGradient}></LinearGradient>
            </View>

            <TouchableOpacity
              activeOpacity={0.7}
              style={defaultShape.CloseBtn}
              onPress={() =>
                this.setState({
                  editEmailModalOpen: false,
                  emailAddress:
                    this.props.user.body && this.props.user.body.email
                      ? this.props.user.body.email
                      : '',
                })
              }>
              <Icon
                name="Cross"
                size={13}
                color="#367681"
                style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
              />
            </TouchableOpacity>

            <View
              style={[
                defaultShape.Modal_Categories_Container,
                { height: 250, backgroundColor: COLORS.bgfill },
              ]}>
              <View style={styles.modalHeaderView}>
                <Text
                  style={[
                    defaultStyle.Button_2,
                    { color: COLORS.dark_900, textAlign: 'center' },
                  ]}>
                  EDIT EMAIL ADDRESS
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 56,
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label="Email Address"
                  // mode='outlined'
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Subtitle_1,
                    {
                      width: '90%',
                      height: 56,
                      backgroundColor: COLORS.bgfill,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) => this.setState({ emailAddress: value })}
                  value={this.state.emailAddress}
                />
                <Icon
                  name="Shield_Tick"
                  color={COLORS.confirmgreen_200}
                  size={16}
                  style={{
                    marginTop: Platform.OS === 'android' ? 15 : 0,
                    zIndex: 2,
                  }}
                />
              </View>

              <View style={styles.border2}></View>

              <TouchableOpacity
                onPress={() => this.setState({ seconds: 59, minutes: 9 }, () => this.submitEmailValidation())}
                activeOpacity={0.7}
                style={[styles.updateButton, { marginTop: 60 }]}>
                <Text
                  style={[defaultStyle.Caption, { color: COLORS.altgreen_100 }]}>
                  Continue
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    )
  }

  editPersonalInfoModal = () => {
    return (
      <Modal
        visible={this.state.editPersonalInfoModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={styles.linearGradientView}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={styles.linearGradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({
                editPersonalInfoModalOpen: false,
                firstName: this.props.user.body.firstName,
                lastName:
                  this.props.user.body.lastName.charAt(0).toUpperCase() +
                  this.props.user.body.lastName.slice(1),
                persona: this.props.user.body
                  ? this.props.user.body.persona
                  : '',
                dob:
                  this.props.user.body &&
                    this.props.user.body.dob &&
                    this.props.user.body.dob !== 0
                    ? this.props.user.body.dob
                    : ''
              })
            }>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              { height: 373, backgroundColor: COLORS.bgfill },
            ]}>
            <View style={styles.modalHeaderView}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  { color: COLORS.dark_900, textAlign: 'center' },
                ]}>
                EDIT PERSONAL INFO
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 56,
                width: '90%',
              }}>
              <TextInput
                theme={{
                  colors: {
                    text: COLORS.dark_700,
                    primary: COLORS.altgreen_300,
                    placeholder: COLORS.altgreen_300,
                  },
                }}
                label="First Name"
                // mode='outlined'
                selectionColor="#C8DB6E"
                style={[
                  defaultStyle.Subtitle_1,
                  {
                    width: '40%',
                    height: 56,
                    backgroundColor: COLORS.bgfill,
                    color: COLORS.dark_700,
                  },
                ]}
                onChangeText={(value) => this.setState({ firstName: value })}
                value={this.state.firstName}
              />

              <TextInput
                theme={{
                  colors: {
                    text: COLORS.dark_700,
                    primary: COLORS.altgreen_300,
                    placeholder: COLORS.altgreen_300,
                  },
                }}
                label="Last Name"
                // mode='outlined'
                selectionColor="#C8DB6E"
                style={[
                  defaultStyle.Subtitle_1,
                  {
                    width: '40%',
                    height: 56,
                    backgroundColor: COLORS.bgfill,
                    color: COLORS.dark_700,
                  },
                ]}
                onChangeText={(value) => this.setState({ lastName: value })}
                value={this.state.lastName}
              />
            </View>

            <TouchableOpacity
              activeOpacity={0.6}
              style={{
                height: 56,
                width: '90%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 20,
                paddingLeft: 6,
                paddingRight: 12,
              }}>
              <View style={{ width: '100%' }}>
                <Text
                  style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                  Birth Date
                </Text>

                <DatePicker
                  style={{ width: '100%', borderWidth: 0 }}
                  date={
                    this.state.dob === ''
                      ? ''
                      : typeof this.state.dob == 'string' &&
                        this.state.dob.includes('-')
                        ? this.state.dob
                        : this.unixTimeDatePicker(this.state.dob)
                  }
                  mode="date"
                  placeholder="select date"
                  format="DD-MM-YYYY"
                  minDate="01-01-1900"
                  maxDate={this.currentDate()}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      right: 0,
                      top: -10,
                      marginRight: 0,
                    },
                    dateInput: {
                      alignItems: 'flex-start',
                      borderWidth: 0,
                    },
                    dateText: {
                      color: COLORS.dark_700,
                      marginTop: -20,
                    },
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(date) => {
                    this.setState({ dob: date });
                  }}
                />
              </View>
            </TouchableOpacity>
            <View style={styles.border2}></View>

            <TouchableOpacity
              onPress={() =>
                this.setState({
                  editPersonalInfoModalOpen: false,
                  whatDefinesYouModalOpen: true,
                })
              }
              activeOpacity={0.6}
              style={{
                height: 56,
                width: '90%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 10,
                paddingLeft: 6,
                paddingRight: 6,
              }}>
              <View>
                <Text
                  style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                  What defines you best
                </Text>
                <Text
                  style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                  {this.state.persona}
                </Text>
              </View>

              <View
                style={{
                  width: 26,
                  height: 26,
                  borderRadius: 13,
                  backgroundColor: '#E7F3E3',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 6,
                }}>
                <Icon
                  name="Arrow_Down"
                  size={12}
                  color="#91B3A2"
                  style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
                />
              </View>
            </TouchableOpacity>
            <View style={styles.border2}></View>

            <TouchableOpacity
              onPress={this.submitProfileDetail}
              activeOpacity={0.7}
              style={[styles.updateButton, { marginTop: 30 }]}>
              <Text
                style={[defaultStyle.Caption, { color: COLORS.altgreen_100 }]}>
                Update
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  findCountry = (query) => {
    if (query === '') {
      return this.state.countryCodes;
    }

    const regex = new RegExp(`${query.trim()}`, 'i');

    return this.state.countryCodes.filter(
      (item) => (item.value.code + item.value.country).search(regex) >= 0,
    );
  };

  countryCodeModal = () => {
    return (
      <Modal
        visible={this.state.modalCountry}
        animationType="slide"
        transparent={true}
        backdropColor="transparent"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <TouchableOpacity
            style={{
              alignSelf: 'center',
              width: 42,
              height: 42,
              borderRadius: 42 / 2,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#F7F7F5',
              marginRight: 10,
              marginBottom: 10,
            }}
            onPress={() => this.setState({ modalCountry: false, editPhoneNoModalOpen: true, query: '' })}>
            <Icon
              name="Cross"
              size={16}
              color="#367681"
              style={{ marginTop: 5 }}
            />
          </TouchableOpacity>

          <View
            style={{
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              backgroundColor: '#fff',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Icon
              name="Search"
              color="#A9A9A9"
              size={14}
              style={{ marginLeft: 20, marginTop: 20, alignSelf: 'center' }}
            />
            <TextInput
              theme={{
                colors: {
                  text: '#367681',
                  primary: '#154A59',
                  placeholder: '#A9A9A9',
                },
              }}
              // mode='outlined'
              placeholder="Search"
              selectionColor="#C8DB6E"
              style={[
                styles.inputFocus,
                {
                  height: 30,
                  width: '100%',
                  marginTop: 10,
                  backgroundColor: '#fff',
                  color: '#D0E8C8',
                },
              ]}
              onChangeText={(value) => this.setState({ query: value })}
              value={this.state.query}
            />
          </View>

          <FlatList
            keyboardShouldPersistTaps="handled"
            data={this.findCountry(this.state.query)}
            style={{ backgroundColor: '#F7F7F5', width: '100%', height: '65%' }}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item) => item.value.code + item.value.country}
            renderItem={({ item }) => (
              <Text
                style={{
                  padding: 10,
                  color: '#154A59',
                  fontSize: 16,
                  marginLeft: 15,
                  borderBottomColor: '#ccc',
                  borderBottomWidth: 0.8,
                }}
                onPress={() =>
                  this.setState({
                    selectedCountryCode: item.value.code,
                    country: item.value.country,
                    modalCountry: false,
                    editPhoneNoModalOpen: true,
                    query: ''
                  })
                }>
                {item.label}
              </Text>
            )}
          />
        </View>
      </Modal>
    )
  }

  editPhoneNoModal = () => {
    return (
      <Modal
        visible={this.state.editPhoneNoModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{ flex: 1 }}>
          <View style={{ marginTop: 'auto' }}>
            <View style={styles.linearGradientView}>
              <LinearGradient
                colors={['#154A5900', '#154A59CC', '#154A59']}
                style={styles.linearGradient}></LinearGradient>
            </View>

            <TouchableOpacity
              activeOpacity={0.7}
              style={defaultShape.CloseBtn}
              onPress={() =>
                this.setState({
                  editPhoneNoModalOpen: false,
                  phoneNumber:
                    this.props.user.body && this.props.user.body.mobile,
                  selectedCountryCode:
                    this.props.user.body && this.props.user.body.countryCode
                      ? this.props.user.body.countryCode
                      : 'Code',
                })
              }>
              <Icon
                name="Cross"
                size={13}
                color="#367681"
                style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
              />
            </TouchableOpacity>

            <View
              style={[
                defaultShape.Modal_Categories_Container,
                { height: 250, backgroundColor: COLORS.bgfill },
              ]}>
              <View style={styles.modalHeaderView}>
                <Text
                  style={[
                    defaultStyle.Button_2,
                    { color: COLORS.dark_900, textAlign: 'center' },
                  ]}>
                  EDIT PHONE NUMBER
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 56,
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <TouchableHighlight
                  style={styles.countryCodeBlur}
                  underlayColor="#F7F7F5"
                  onPress={() =>
                    this.setState({
                      modalCountry: true,
                      editPhoneNoModalOpen: false,
                    })
                  }>
                  <View
                    style={{
                      backgroundColor: '#E2E7E9',
                      height: 30,
                      width: 70,
                      borderRadius: 20,
                      marginTop: 10,
                      alignSelf: 'flex-start',
                      marginLeft: 20,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    {/* {this.state.selectedCountryCode !== 'Code' ? 
                        <Text style={{ color: '#91B3A2', fontSize: 12, marginTop: 6, marginRight: 16 }}>Code</Text> : null} */}
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        alignItems: 'center',
                      }}>
                      <Text
                        numberOfLines={1}
                        style={
                          this.state.selectedCountryCode === 'Code'
                            ? { fontSize: 16, color: '#91B3A2' }
                            : { fontSize: 16, color: '#154A59' }
                        }>
                        {this.state.selectedCountryCode !== 'Code'
                          ? '+' + this.state.selectedCountryCode
                          : this.state.selectedCountryCode}
                      </Text>
                      <Icon
                        name="Arrow2_Down"
                        size={14}
                        color="#91B3A2"
                        style={
                          Platform.OS === 'android'
                            ? { marginLeft: 5, marginTop: 8 }
                            : { marginLeft: 5 }
                        }
                      />
                    </View>
                  </View>
                </TouchableHighlight>

                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label="Phone Number"
                  keyboardType="phone-pad"
                  maxLength={10}
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Subtitle_1,
                    {
                      width: '76%',
                      height: 56,
                      backgroundColor: COLORS.bgfill,
                      color: COLORS.dark_700,
                      marginLeft: 10,
                      marginTop: 5,
                    },
                  ]}
                  onChangeText={(value) => this.setState({ phoneNumber: value })}
                  value={this.state.phoneNumber.toString()}
                />
                {/* <Icon name="Shield_Tick" color={COLORS.confirmgreen_200} size={16} style={{ marginTop: Platform.OS === 'android' ? 15 : 0, zIndex: 2 }} /> */}
              </View>

              <View style={styles.border2}></View>

              <TouchableOpacity
                onPress={() => this.setState({ seconds: 59, minutes: 9 }, () => this.verifyMobile())}
                activeOpacity={0.7}
                style={[styles.updateButton, { marginTop: 60 }]}>
                <Text
                  style={[defaultStyle.Caption, { color: COLORS.altgreen_100 }]}>
                  Continue
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    )
  }

  editCustomUrlModal = () => {
    return (
      <Modal
        visible={this.state.editCustomUrlModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={styles.linearGradientView}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={styles.linearGradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({
                editCustomUrlModalOpen: false,
                customUrl:
                  this.props.user.body && this.props.user.body.customUrl,
              })
            }>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              { height: 360, backgroundColor: COLORS.bgfill },
            ]}>
            <View style={styles.modalHeaderView}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  { color: COLORS.dark_900, textAlign: 'center' },
                ]}>
                EDIT CUSTOM URL
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 56,
                marginLeft: 0,
              }}>
              <Icon
                name="Link_Post"
                color={COLORS.primarydark}
                size={12}
                style={{ marginTop: Platform.OS === 'android' ? 8 : 0 }}
              />

              <Text
                style={[
                  defaultStyle.Body_1,
                  { color: COLORS.grey_500, marginLeft: 6 },
                ]}>
                {REACT_APP_domainUrl}/profile/
              </Text>

              <TextInput
                theme={{
                  colors: {
                    text: COLORS.grey_500,
                    primary: COLORS.altgreen_300,
                    placeholder: COLORS.altgreen_300,
                  },
                }}
                label="Custom URL"
                mode="outlined"
                maxLength={20}
                selectionColor="#C8DB6E"
                style={[
                  defaultStyle.Body_1,
                  {
                    width: '30%',
                    height: 34,
                    backgroundColor: COLORS.altgreen_t50,
                    color: COLORS.dark_600,
                    marginLeft: 6,
                  },
                ]}
                onChangeText={(value) => this.setState({ customUrl: value })}
                value={this.state.customUrl}
              />
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
                marginTop: 20,
                marginLeft: 0,
                width: '80%',
              }}>
              <Icon
                name="FollowTick"
                color={COLORS.confirmgreen_200}
                size={12}
                style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
              />

              <Text
                style={[
                  defaultStyle.Note2,
                  { color: COLORS.dark_600, marginLeft: 6 },
                ]}>
                URL is available
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
                marginTop: 0,
                marginLeft: 0,
                width: '80%',
              }}>
              <Icon
                name="FollowTick"
                color={COLORS.confirmgreen_200}
                size={12}
                style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
              />

              <Text
                style={[
                  defaultStyle.Note2,
                  { color: COLORS.dark_600, marginLeft: 6 },
                ]}>
                Your public display name is the same as your profile name.
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
                marginTop: 0,
                marginLeft: 0,
                width: '80%',
              }}>
              <Icon
                name="FollowTick"
                color={COLORS.confirmgreen_200}
                size={12}
                style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
              />

              <Text
                style={[
                  defaultStyle.Note2,
                  { color: COLORS.dark_600, marginLeft: 6 },
                ]}>
                Custom URL ending should have at least 2 and should not exceed
                20 characters.
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
                marginTop: 0,
                marginLeft: 0,
                width: '80%',
              }}>
              <Icon
                name="Caution"
                color={COLORS.alert_red}
                size={12}
                style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
              />

              <Text
                style={[
                  defaultStyle.Note2,
                  { color: COLORS.alert_red, marginLeft: 6 },
                ]}>
                Custom URL should not contain special characters namely
                ?`:!@#%^&*()~
              </Text>
            </View>

            <TouchableOpacity
              onPress={this.submitCustomUrlDetail}
              activeOpacity={0.7}
              style={[styles.updateButton, { marginTop: 16 }]}>
              <Text
                style={[defaultStyle.Caption, { color: COLORS.altgreen_100 }]}>
                Update
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }

  submitAddressDetail = () => {
    if (
      this.state.country === 'Select Country'
    ) {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please select a country',
        duration: Snackbar.LENGTH_LONG,
      });
    }
    else if (
      this.state.state === 'Select State'
    ) {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please select a state',
        duration: Snackbar.LENGTH_LONG,
      });
    }
    else if (
      this.state.city === 'Select City'
    ) {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please select a city',
        duration: Snackbar.LENGTH_LONG,
      })
    }
    else {
      this.setState({ editUserAddressModalOpen: false })
      let postBody = {
        userId: this.state.userId,
        country: this.state.country,
        state: this.state.state,
        city: this.state.city,
        otherDetails: this.state.address,
      };
      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/profile/update/address',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (res.status === '200 OK') {
            // console.log(res.status);
            this.props.personalAddressRequest({
              userId: this.state.userId,
              otherUserId: '',
            });
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            // console.log(err.response.data);
          }
        })
    }
  }

  changeState = (value) => {
    this.setState(value);
  };

  editUserAddressModal = () => {
    return (
      <Modal
        visible={this.state.editUserAddressModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{ flex: 1 }}>
          <View style={{ marginTop: 'auto' }}>
            <View style={styles.linearGradientView}>
              <LinearGradient
                colors={['#154A5900', '#154A59CC', '#154A59']}
                style={styles.linearGradient}></LinearGradient>
            </View>


            <TouchableOpacity
              activeOpacity={0.7}
              style={defaultShape.CloseBtn}
              onPress={() =>
                this.setState({
                  editUserAddressModalOpen: false,
                  address: this.props.userAddress.body
                    ? this.props.userAddress.body.otherDetails
                    : '',
                  country: this.props.userAddress.body
                    ? this.props.userAddress.body.country
                    : '',
                  state: this.props.userAddress.body
                    ? this.props.userAddress.body.state
                    : '',
                  city: this.props.userAddress.body
                    ? this.props.userAddress.body.city
                    : '',
                })
              }>
              <Icon
                name="Cross"
                size={13}
                color="#367681"
                style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
              />
            </TouchableOpacity>

            <View
              style={[
                defaultShape.Modal_Categories_Container,
                { height: 469, backgroundColor: COLORS.bgfill },
              ]}>
              <View style={[styles.modalHeaderView]}>
                <Text
                  style={[
                    defaultStyle.Button_2,
                    { color: COLORS.dark_900, textAlign: 'center' },
                  ]}>
                  EDIT ADDRESS
                </Text>
              </View>

              <EditUserAddress
                country={this.state.country}
                state={this.state.state}
                city={this.state.city}
                changeState={this.changeState}
              />
              {/* <CreateCircle country={this.state.country} state={this.state.state} city={this.state.city} changeState={this.changeState} marginTop={30} /> */}

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label="Type in address"
                  // multiline
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Subtitle_1,
                    {
                      width: '90%',
                      backgroundColor: COLORS.bgfill,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) => this.setState({ address: value })}
                  value={this.state.address}
                />
                {/* <Icon name="Shield_Tick" color={COLORS.confirmgreen_200} size={16} style={{ marginTop: Platform.OS === 'android' ? 15 : 0, zIndex: 2 }} /> */}
              </View>

              <View style={styles.border2}></View>
              <View
                style={[
                  styles.border,
                  { position: 'absolute', bottom: 80 },
                ]}></View>

              <TouchableOpacity
                onPress={() => {

                  this.submitAddressDetail();
                }}
                activeOpacity={0.7}
                style={[styles.updateButton, { marginTop: 60 }]}>
                <Text
                  style={[defaultStyle.Caption, { color: COLORS.altgreen_100 }]}>
                  Update
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  };

  searchWhatDefinesYou = (query) => {
    if (query === '') {
      return this.state.whatDefinesYouSuggestions;
    }

    const regex = new RegExp(`${query.trim()}`, 'i');

    return this.state.whatDefinesYouSuggestions.filter(
      (item) => item.search(regex) >= 0,
    );
  };

  whatDefinesYouModal = () => {
    return (
      <Modal
        visible={this.state.whatDefinesYouModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View
            style={{
              width: '100%',
              height: 700,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ whatDefinesYouModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              { height: 450, backgroundColor: COLORS.bgfill, paddingBottom: 10 },
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  { color: COLORS.dark_900, textAlign: 'center' },
                ]}>
                WHAT DEFINES YOU BEST
              </Text>
            </View>

            <View
              style={{
                height: 40,
                backgroundColor: '#F7F7F5',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '13%',
                zIndex: 2,
              }}>
              <TextInput
                style={styles.textInput}
                theme={{
                  colors: {
                    text: COLORS.dark_700,
                    primary: '#F7F7F5',
                    placeholder: COLORS.altgreen_300,
                  },
                }}
                // label="Type in address"
                selectionColor="#C8DB6E"
                onChangeText={(value) => {
                  this.setState({ searchWhatDefinesYou: value });
                }}
                placeholder="Search"
                onFocus={() => this.setState({ searchIcon: false })}
                onBlur={() => this.setState({ searchIcon: true })}
                underlineColorAndroid="transparent"
                ref={(input) => {
                  this.textInput = input;
                }}
              />
            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingBottom: 0,
                  paddingTop: 6,
                  width: '100%',
                }}
                style={{ height: '50%' }}
                keyExtractor={(item) => item}
                data={this.searchWhatDefinesYou(
                  this.state.searchWhatDefinesYou,
                )}
                initialNumToRender={10}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        persona: item,
                        searchWhatDefinesYou: '',
                        whatDefinesYouModalOpen: false,
                        editPersonalInfoModalOpen: true,
                      });
                    }}
                    style={styles.item}
                    activeOpacity={0.7}>
                    <Text
                      style={[
                        defaultStyle.Button_Lead,
                        { color: COLORS.dark_600, textAlign: 'center' },
                      ]}>
                      {item}
                    </Text>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      {/* {this.state.selectedWhatDefinesYou.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  phoneOtpModal = () => {
    return (
      <Modal
        visible={this.state.phoneOtpModal}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            alignSelf: 'center',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              { height: 700, bottom: 0 },
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.white,
              // paddingHorizontal: 30,
              paddingTop: 15,
              // paddingBottom: 10,
              width: '80%',
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {
                  color: COLORS.dark_600,
                  alignSelf: 'center',
                  textAlign: 'center',
                },
              ]}>
              Enter the 6 digit OTP sent on {`\n`} your phone
            </Text>

            <View
              style={{
                paddingHorizontal: 16,
                height: 27,
                backgroundColor: COLORS.grey_100,
                borderRadius: 20,
                alignSelf: 'center',
                marginTop: 10,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>
                +{this.state.selectedCountryCode} {this.state.phoneNumber}
              </Text>
            </View>

            <View
              style={{
                height: 70,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '3%',
              }}>
              <OTPInputView
                placeholderCharacter=""
                placeholderTextColor="#91B3A257"
                style={{ width: 260, color: '#00394D', marginLeft: -12 }}
                pinCount={6}
                code={this.state.otp}
                onCodeChanged={(code) => this.setState({ otp: code })}
                autoFocusOnLoad={false}
                codeInputFieldStyle={styles.borderStyleBase}
                codeInputHighlightStyle={styles.borderStyleHighLighted}
                onCodeFilled={(code) => {
                  console.log(`Code is ${code}, you are good to go!`);
                }}
              />
            </View>

            {this.state.minutes === 0 && this.state.seconds === '0' + 0 ?
              <TouchableOpacity
                activeOpacity={0.5}
                style={{
                  justifyContent: 'center',
                  backgroundColor: COLORS.altgreen_100,
                  width: 118,
                  height: 28,
                  alignSelf: 'center',
                  marginTop: '5%',
                  borderRadius: 40,
                  alignItems: 'center',
                }}>
                <Text style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>
                  Resend OTP
                </Text>
              </TouchableOpacity>
              :
              <Text style={styles.resend}>OTP will expire in {this.state.minutes}:{this.state.seconds}</Text>}

            <View
              style={{
                width: '100%',
                height: 52,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignItems: 'center',
                borderTopWidth: 1,
                borderTopColor: COLORS.grey_300,
                marginTop: 20,
              }}>
              <Text
                onPress={() =>
                  this.setState({
                    phoneOtpModal: false,
                    selectedCountryCode:
                      this.props.user.body && this.props.user.body.countryCode
                        ? this.props.user.body.countryCode
                        : 'Code',
                    phoneNumber: this.props.user.body
                      ? this.props.user.body.mobile
                      : '',
                    seconds: 59,
                    minutes: 9,
                    otp: ''
                  })
                }
                style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                Cancel
              </Text>
              <View
                style={{
                  height: '100%',
                  width: 1,
                  backgroundColor: COLORS.grey_300,
                }}></View>
              <Text
                onPress={this.handleUpdatePhone}
                style={[defaultStyle.Button_2, { color: COLORS.dark_800 }]}>
                DONE
              </Text>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  emailOtpModal = () => {
    return (
      <Modal
        visible={this.state.emailOtpModal}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            alignSelf: 'center',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              { height: 700, bottom: 0 },
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.white,
              paddingTop: 15,
              width: '80%',
            }}>
            <Text
              style={[
                defaultStyle.Title_2,
                {
                  color: COLORS.dark_600,
                  alignSelf: 'center',
                  textAlign: 'center',
                },
              ]}>
              Enter the 6 digit OTP sent on {`\n`} your email
            </Text>

            <View
              style={{
                paddingHorizontal: 16,
                height: 27,
                backgroundColor: COLORS.grey_100,
                borderRadius: 20,
                alignSelf: 'center',
                marginTop: 10,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>
                {this.state.emailAddress}
              </Text>
            </View>

            <View
              style={{
                height: 70,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '3%',
              }}>
              <OTPInputView
                placeholderCharacter=""
                placeholderTextColor="#91B3A257"
                style={{ width: 260, color: '#00394D', marginLeft: -12 }}
                pinCount={6}
                code={this.state.otp}
                onCodeChanged={(code) => this.setState({ otp: code })}
                autoFocusOnLoad={false}
                codeInputFieldStyle={styles.borderStyleBase}
                codeInputHighlightStyle={styles.borderStyleHighLighted}
                onCodeFilled={(code) => {
                  console.log(`Code is ${code}, you are good to go!`);
                }}
              />
            </View>

            {this.state.minutes === 0 && this.state.seconds === '0' + 0 ?
              <TouchableOpacity
                activeOpacity={0.5}
                style={{
                  justifyContent: 'center',
                  backgroundColor: COLORS.altgreen_100,
                  width: 118,
                  height: 28,
                  alignSelf: 'center',
                  marginTop: '5%',
                  borderRadius: 40,
                  alignItems: 'center',
                }}>
                <Text style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>
                  Resend OTP
                </Text>
              </TouchableOpacity>
              :
              <Text style={styles.resend}>OTP will expire in {this.state.minutes}:{this.state.seconds}</Text>}

            {/* <View style={{
                justifyContent: 'center', alignItems: 'center', backgroundColor: '#EDEFEF', width: 194, height: 38, alignSelf: 'center', marginTop: '5%', borderRadius: 40, flexDirection: 'row',
              }}>
                <View style={{ width: this.state.animatingWidth, alignSelf: 'flex-start', height: 38, backgroundColor: '#698F8A', borderTopLeftRadius: 26, borderBottomLeftRadius: 26, position: 'absolute', left: 0, bottom: 0, }}></View>
                <Text style={styles.resend}>OTP will expire in {this.state.minutes}:{this.state.seconds}</Text>

              </View> */}


            <View
              style={{
                width: '100%',
                height: 52,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignItems: 'center',
                borderTopWidth: 1,
                borderTopColor: COLORS.grey_300,
                marginTop: 20,
              }}>
              <Text
                onPress={() =>
                  this.setState({
                    emailOtpModal: false,
                    emailAddress: this.props.user.body
                      ? this.props.user.body.email
                      : '',
                    seconds: 59,
                    minutes: 9,
                    otp: ''
                  })
                }
                style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                Cancel
              </Text>
              <View
                style={{
                  height: '100%',
                  width: 1,
                  backgroundColor: COLORS.grey_300,
                }}></View>
              <Text
                onPress={this.handleOtpSubmit}
                style={[defaultStyle.Button_2, { color: COLORS.dark_800 }]}>
                DONE
              </Text>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  // resendOtp = () => {
  //   let postBody = {
  //       "userId": this.state.personalProfileInitialData.userId,
  //       "mediaData":this.state.personalProfileInitialData.email,
  //       "otpType": 'CHANGE_EMAIL_RESEND',
  //       "otpSource":'EMAIL'
  //   };
  //   axios({
  //       method: 'post',
  //       url: REACT_APP_userServiceURL + '/otp/re-send',
  //       headers: {'Content-Type': 'application/json'},
  //       data: postBody,
  //       withCredentials: true
  //   }).then((response) => {
  //       let res = response.data;
  //       console.log(res.message)
  //       if (res.message === 'Success!') {
  //           this.setState({message: 'OTP resent successfully', minutes: 9, seconds: 59, success: true, resendOTP: false})
  //           this.resendButtonWait()
  //     }
  //   }).catch((err) => {
  //       console.log(err)
  //       if (err && err.response && err.response.data) {
  //           this.setState({
  //               isLoaded: true,
  //               error: {message: err.response.data.message, err: err.response}
  //           })

  //       }

  //   })
  // }

  openWebsite = (website) => {
    if (website && website.includes('http')) return Linking.openURL(website)
    else if (website && !website.includes('http'))
      return Linking.openURL('https://' + website)
  }

  trimDescription = (item) => {
    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  renderBio = (item) => {
    return (
      <Autolink
        text={item && this.trimDescription(tagDescription(item))}
        email
        hashtag="instagram"
        mention="twitter"
        phone="sms"
        numberOfLines={5}
        matchers={[
          {
            pattern: /@\[([^[]*)]\(([^(^)]*)\)/g,
            style: { color: COLORS.mention_color, fontWeight: 'bold' },
            getLinkText: (replacerArgs) => `${replacerArgs[2]}`,
            onPress: (match) => {
              this.getUserDetailsByCustomUrl(match.getReplacerArgs()[1]);
            },
          },
          {
            pattern: /#\[([^[]*)]\(([^(^)]*)\)/g,
            style: { color: COLORS.mention_color, fontWeight: 'bold' },
            getLinkText: (replacerArgs) => `#${replacerArgs[2]}`,
            onPress: (match) => {
              this.props.navigation.navigate('HashTagDetail', {
                slug: match.getReplacerArgs()[1],
              });
            },
          },
        ]}
        style={[
          defaultStyle.Body_1,
          {
            color: COLORS.dark_500,
            marginLeft: 6,
            marginRight: 17,
            marginVertical: 8
          }
        ]}
        url
      />
    )
  }

  render() {
    // console.log('this.state.writeSomething', this.state.writeSomething)
    return (
      <SafeAreaView style={{ flex: 1 }}>
        {/***** Header starts *****/}

        <ProfileEditHeader
          name="Personal Info"
          iconName="SingleUser_OL"
          goback={this.goback}
        />

        {/***** Header ends *****/}

        {this.editBioModal()}
        {this.editWebsiteModal()}
        {this.editEmailModal()}
        {this.editPhoneNoModal()}
        {this.editCustomUrlModal()}
        {this.editPersonalInfoModal()}
        {this.editUserAddressModal()}
        {this.whatDefinesYouModal()}
        {this.countryCodeModal()}
        {this.phoneOtpModal()}
        {this.emailOtpModal()}
        {this.suggestedHashTagsModal()}

        <ScrollView>
          {/***** Profile & Cover image starts *****/}

          <View style={styles.profileCoverImgView}>
            <ImageBackground
              source={
                this.props.user.body && this.props.user.body.originalCoverImage
                  ? { uri: this.props.user.body.originalCoverImage }
                  : defaultCover
              }
              style={{ height: 138, width: '100%', zIndex: 2 }}>
              <View
                style={{
                  alignSelf: 'flex-end',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: 10,
                  marginRight: 10,
                  width: 70,
                }}>
                <TouchableOpacity
                  onPress={this.openPicker}
                  activeOpacity={0.5}
                  style={defaultShape.AdjunctBtn_Small_Sec}>
                  <Icon
                    name="UploadPhoto"
                    color={COLORS.dark_600}
                    size={14}
                    style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
                  />
                </TouchableOpacity>
                {this.props.user.body &&
                  this.props.user.body.originalCoverImage ? (
                  <TouchableOpacity
                    onPress={this.deleteCoverPic}
                    activeOpacity={0.5}
                    style={defaultShape.AdjunctBtn_Small_Sec}>
                    <Icon
                      name="TrashBin"
                      color={COLORS.alertred_200}
                      size={14}
                      style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
                    />
                  </TouchableOpacity>
                ) : (
                  <></>
                )}
              </View>
            </ImageBackground>

            <View
              style={{
                position: 'absolute',
                bottom: 0,
                // left: 90,
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '92%',
                // zIndex: 2,
              }}>
              <Svg
                height="145"
                width="110"
                style={{ zIndex: 2, marginLeft: 30, justifyContent: 'center' }}>
                <Defs>
                  <ClipPath id="clip">
                    <Polygon
                      points="55 0, 110 32, 110 94, 70 114.5, 79.2 145, 0 94, 0 32"
                      fill="transparent"
                      stroke="white"
                      strokeWidth="3"
                      zIndex={2}
                      position="absolute"
                    />
                  </ClipPath>
                </Defs>

                {this.props.user.body &&
                  this.props.user.body.originalProfileImage ? (
                  <Image
                    width="100%"
                    height="100%"
                    preserveAspectRatio="xMidYMid slice"
                    opacity="1"
                    href={this.props.user.body.originalProfileImage}
                    clipPath="url(#clip)"
                  />
                ) : (
                  <Image
                    width="100%"
                    height="100%"
                    preserveAspectRatio="xMidYMid slice"
                    opacity="1"
                    href={defaultProfile}
                    clipPath="url(#clip)"
                  />
                )}

                <Polygon
                  points="55 0, 110 32, 110 94, 70 114.5, 79.2 145, 0 94, 0 32"
                  fill="transparent"
                  stroke="white"
                  strokeWidth="3"
                  zIndex={2}
                />
              </Svg>

              <View
                style={{
                  alignSelf: 'center',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  position: 'absolute',
                  top: 70,
                  left: 48,
                  width: 70,
                  zIndex: 3,
                }}>
                <TouchableOpacity
                  onPress={this.openPickerProfile}
                  activeOpacity={0.5}
                  style={defaultShape.AdjunctBtn_Small_Sec}>
                  <Icon
                    name="UploadPhoto"
                    color={COLORS.dark_600}
                    size={14}
                    style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
                  />
                </TouchableOpacity>

                {this.props.user.body &&
                  this.props.user.body.originalProfileImage ? (
                  <TouchableOpacity
                    onPress={this.deleteProfilePic}
                    activeOpacity={0.5}
                    style={defaultShape.AdjunctBtn_Small_Sec}>
                    <Icon
                      name="TrashBin"
                      color={COLORS.alertred_200}
                      size={14}
                      style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
                    />
                  </TouchableOpacity>
                ) : (
                  <></>
                )}
              </View>

              {/* <TouchableOpacity style={[defaultShape.ContextBtn_FL_Drk, { zIndex: 2, alignSelf: 'center', marginTop: 40, width: 76 }]}>
                <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_200 }]}>Update</Text>
              </TouchableOpacity> */}
            </View>
          </View>

          {/***** Profile & Cover image ends *****/}

          {/***** Name dob persona starts *****/}

          <View
            style={{ height: 162, backgroundColor: COLORS.white, marginTop: 15 }}>
            <TouchableOpacity
              onPress={() => this.setState({ editPersonalInfoModalOpen: true })}
              style={[
                defaultShape.Nav_Gylph_Btn,
                { position: 'absolute', top: 5, right: 5, zIndex: 1 },
              ]}>
              <Icon name="EditBox" size={16} color={COLORS.grey_400} />
            </TouchableOpacity>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Name
              </Text>
              <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                {this.props.user.body && this.props.user.body.userName
                  ? this.props.user.body.userName
                  : ''}
              </Text>
            </View>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Birth Date
              </Text>
              <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                {this.state.dateOfBirth > 0
                  ? this.unixTime(this.state.dateOfBirth)
                  : ''}
              </Text>
            </View>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                What defines you best
              </Text>
              <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                {this.state.persona}
              </Text>
            </View>
          </View>

          {/***** Name dob persona ends *****/}

          {/***** Email starts *****/}

          <View
            style={{ height: 68, backgroundColor: COLORS.white, marginTop: 15 }}>
            <TouchableOpacity
              onPress={() => this.setState({ editEmailModalOpen: true })}
              style={[
                defaultShape.Nav_Gylph_Btn,
                { position: 'absolute', top: 5, right: 5, zIndex: 1 },
              ]}>
              <Icon name="EditBox" size={16} color={COLORS.grey_400} />
            </TouchableOpacity>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Email Address
              </Text>
              <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                {this.props.user.body && this.props.user.body.email
                  ? this.props.user.body.email
                  : ''}
              </Text>
            </View>
          </View>

          {/***** Email ends *****/}

          {/***** Custom url starts *****/}

          <View
            style={{ height: 90, backgroundColor: COLORS.white, marginTop: 15 }}>
            <TouchableOpacity
              onPress={() => this.setState({ editCustomUrlModalOpen: true })}
              style={[
                defaultShape.Nav_Gylph_Btn,
                { position: 'absolute', top: 5, right: 5, zIndex: 1 },
              ]}>
              <Icon name="EditBox" size={16} color={COLORS.grey_400} />
            </TouchableOpacity>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Custom URL
              </Text>

              <View style={styles.urlView}>
                <Icon
                  name="Link_Post"
                  color={COLORS.primarydark}
                  size={12}
                  style={{
                    marginTop: Platform.OS === 'android' ? 8 : 0,
                    zIndex: 2,
                  }}
                />

                <Text
                  style={[
                    defaultStyle.Body_1,
                    { color: COLORS.grey_500, marginLeft: 5 },
                  ]}>
                  {this.props.user.body && this.props.user.body.customUrl
                    ? REACT_APP_domainUrl +
                    '/profile/' +
                    this.props.user.body.customUrl
                    : ''}
                </Text>
              </View>
            </View>
          </View>

          {/***** Custom url ends *****/}

          {/***** Phone starts *****/}

          <View
            style={{ height: 68, backgroundColor: COLORS.white, marginTop: 15 }}>
            <TouchableOpacity
              onPress={() => this.setState({ editPhoneNoModalOpen: true })}
              style={[
                defaultShape.Nav_Gylph_Btn,
                { position: 'absolute', top: 5, right: 5, zIndex: 1 },
              ]}>
              <Icon name="EditBox" size={16} color={COLORS.grey_400} />
            </TouchableOpacity>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Phone Number
              </Text>
              <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                {this.props.user.body &&
                  this.props.user.body.countryCode &&
                  this.props.user.body.mobile
                  ? '+' +
                  this.props.user.body.countryCode +
                  ' ' +
                  this.props.user.body.mobile
                  : ''}
              </Text>
            </View>
          </View>

          {/***** Phone ends *****/}

          {/***** Bio starts *****/}

          <View
            style={{
              maxHeight: 150,
              backgroundColor: COLORS.white,
              marginTop: 15,
              paddingBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() => this.setState({ editBioModalOpen: true })}
              style={[
                defaultShape.Nav_Gylph_Btn,
                { position: 'absolute', top: 5, right: 5, zIndex: 1 },
              ]}>
              <Icon name="EditBox" size={16} color={COLORS.grey_400} />
            </TouchableOpacity>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Bio
              </Text>
              <Text
                numberOfLines={5}
                style={[
                  defaultStyle.Subtitle_1,
                  { color: COLORS.dark_700, maxWidth: '85%' },
                ]}>
                {this.props.user.body && this.props.user.body.bio
                  ? this.renderBio(this.props.user.body.bio)
                  : ''}
              </Text>
            </View>
          </View>

          {/***** Bio ends *****/}

          {/***** Website starts *****/}

          <View
            style={{ height: 68, backgroundColor: COLORS.white, marginTop: 15 }}>
            <TouchableOpacity
              onPress={() => this.setState({ editWebsiteModalOpen: true })}
              style={[
                defaultShape.Nav_Gylph_Btn,
                { position: 'absolute', top: 5, right: 5, zIndex: 1 },
              ]}>
              <Icon name="EditBox" size={16} color={COLORS.grey_400} />
            </TouchableOpacity>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Website
              </Text>
              <Text
                onPress={() => this.props.user.body && this.props.user.body.website && this.openWebsite(this.props.user.body.website)}
                style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                {this.props.user.body && this.props.user.body.website
                  ? this.props.user.body.website
                  : ''}
              </Text>
            </View>
          </View>

          {/***** Website ends *****/}

          {/***** Address starts *****/}

          <View
            style={{
              height: 68,
              backgroundColor: COLORS.white,
              marginTop: 15,
              marginBottom: 40,
            }}>
            <TouchableOpacity
              onPress={() => this.setState({ editUserAddressModalOpen: true })}
              style={[
                defaultShape.Nav_Gylph_Btn,
                { position: 'absolute', top: 5, right: 5, zIndex: 1 },
              ]}>
              <Icon name="EditBox" size={16} color={COLORS.grey_400} />
            </TouchableOpacity>

            <View style={{ marginLeft: 28, marginTop: 15 }}>
              <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                Address
              </Text>
              <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                {this.props.userAddress.body
                  ? this.props.userAddress.body.city +
                  ', ' +
                  this.props.userAddress.body.state +
                  ', ' +
                  this.props.userAddress.body.country
                  : ''}
              </Text>
            </View>
          </View>

          {/***** Address ends *****/}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headerView: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 54,
    marginBottom: 5,
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  resend: {
    fontSize: 14,
    textAlign: 'center',
    color: '#367681',
    marginTop: 10,
    marginBottom: 6,
    fontFamily: 'Montserrat-Medium',
  },
  borderStyleBase: {
    color: '#00394D',
    fontWeight: 'bold',
    fontSize: 24,
    width: 36,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 2,
    borderBottomColor: '#BFC52E',
  },
  borderStyleHighLighted: {
    borderColor: '#03DAC6',
  },
  inputFocus: {
    padding: 10,
    fontSize: 14,
    backgroundColor: '#FFFFFF',
    height: 43,
  },
  item: {
    flexDirection: 'row',
    width: '83%',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'left',
    marginLeft: 30,
    // marginRight: 100,
    paddingHorizontal: '3%',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey_200,
    // backgroundColor: 'pink'
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '90%',
    height: 26,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    zIndex: 2,
  },
  linearGradientView: {
    width: '100%',
    height: 700,
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  },
  profileCoverImgView: {
    height: 160,
    width: '90%',
    alignSelf: 'center',
    alignItems: 'center',
    // justifyContent: 'center'
  },
  urlView: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    backgroundColor: COLORS.altgreen_t50,
    borderRadius: 4,
    marginTop: 5,
    width: '90%',
    borderWidth: 1,
    borderColor: COLORS.altgreen_300,
    borderStyle: 'dashed',
  },
  updateButton: {
    width: 86,
    height: 28,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: COLORS.dark_600,
  },
  border: {
    width: '80%',
    alignSelf: 'center',
    backgroundColor: COLORS.grey_200,
    height: 2,
  },
  border2: {
    width: '100%',
    alignSelf: 'center',
    backgroundColor: COLORS.bgfill,
    height: 10,
    zIndex: 2,
    marginTop: -6,
  },
  modalHeaderView: {
    zIndex: 2,
    width: '100%',
    height: 58,
    backgroundColor: COLORS.altgreen_100,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
});

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,

    userAddressProgress: state.personalProfileReducer.userAddressProgress,
    userAddress: state.personalProfileReducer.userAddress,
    errorAddress: state.personalProfileReducer.errorAddress,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
    personalAddressRequest: (data) => dispatch(personalAddressRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfoEdit);
