import React, { Component } from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, Platform, StyleSheet } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultProfile from '../../../../assets/defaultProfile.png'
import typography from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class ProfileEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                {/***** Header starts *****/}

                <View style={styles.headerView}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                        style={defaultShape.Nav_Gylph_Btn}>
                        <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
                    </TouchableOpacity>
                    <Icon name="Profile_Edit" size={18} color={COLORS.dark_600} style={{ marginLeft: 20 }} />
                    <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 10, marginTop: Platform.OS === 'android' ? -8 : 0 }]}>
                        Edit Profile
                    </Text>
                </View>

                {/***** Header ends *****/}

                <TouchableOpacity onPress={() => this.props.navigation.navigate('PersonalInfoEdit')}
                    style={styles.contentView}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name="SingleUser_OL" size={20} color={COLORS.dark_600} style={{ marginLeft: 20 }} />
                        <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 10, marginTop: Platform.OS === 'android' ? -8 : 0 }]}>
                            Personal Info
                        </Text>
                    </View>
                    <Icon name="Arrow_Right" size={20} color={COLORS.dark_600} style={{ marginRight: 10 }} />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('EducationEdit')}
                    style={[styles.contentView, { marginTop: 2 }]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name="Certificate" size={20} color={COLORS.dark_600} style={{ marginLeft: 20, marginTop: Platform.OS === 'android' && 7 }} />
                        <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 10, marginTop: Platform.OS === 'android' ? -8 : 0 }]}>
                            Education
                        </Text>
                    </View>
                    <Icon name="Arrow_Right" size={20} color={COLORS.dark_600} style={{ marginRight: 10 }} />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('AwardsEdit')}
                    style={[styles.contentView]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name="Endorse" size={20} color={COLORS.dark_600} style={{ marginLeft: 20 }} />
                        <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 10, marginTop: Platform.OS === 'android' ? -8 : 0 }]}>
                            Awards
                        </Text>
                    </View>
                    <Icon name="Arrow_Right" size={20} color={COLORS.dark_600} style={{ marginRight: 10 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ExperienceEdit')}
                    style={[styles.contentView, { marginTop: 2 }]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name="Jobs" size={20} color={COLORS.dark_600} style={{ marginLeft: 20 }} />
                        <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 10, marginTop: Platform.OS === 'android' ? -8 : 0 }]}>
                            Experience
                        </Text>
                    </View>
                    <Icon name="Arrow_Right" size={20} color={COLORS.dark_600} style={{ marginRight: 10 }} />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrganizationEdit')}
                    style={styles.contentView}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name="Company" size={20} color={COLORS.dark_600} style={{ marginLeft: 20 }} />
                        <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 10, marginTop: Platform.OS === 'android' ? -8 : 0 }]}>
                            Organization
                        </Text>
                    </View>
                    <Icon name="Arrow_Right" size={20} color={COLORS.dark_600} style={{ marginRight: 10 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('CirclesEdit')}
                style={[styles.contentView, { marginTop: 2 }]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name="Circle_Ol" size={20} color={COLORS.dark_600} style={{ marginLeft: 20 }} />
                        <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 10, marginTop: Platform.OS === 'android' ? -8 : 0 }]}>
                            Circle
                        </Text>
                    </View>
                    <Icon name="Arrow_Right" size={20} color={COLORS.dark_600} style={{ marginRight: 10 }} />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('SkillsSpecializationEdit')}
                    style={styles.contentView}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name="Diamond" size={20} color={COLORS.dark_600} style={{ marginLeft: 20 }} />
                        <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 10, marginTop: Platform.OS === 'android' ? -8 : 0 }]}>
                            Skills & Specialization
                        </Text>
                    </View>
                    <Icon name="Arrow_Right" size={20} color={COLORS.dark_600} style={{ marginRight: 10 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('InterestsCausesEdit')}
                    style={[styles.contentView, { marginTop: 2 }]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon name="Causes_OL" size={20} color={COLORS.dark_600} style={{ marginLeft: 20 }} />
                        <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginLeft: 10, marginTop: Platform.OS === 'android' ? -8 : 0 }]}>
                            Interests & Causes
                        </Text>
                    </View>
                    <Icon name="Arrow_Right" size={20} color={COLORS.dark_600} style={{ marginRight: 10 }} />
                </TouchableOpacity>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 54,
        marginBottom: 5,
        backgroundColor: COLORS.white,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    contentView: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        marginTop: 12,
        justifyContent: 'space-between',
        height: 44,
        backgroundColor: COLORS.white
    }
})