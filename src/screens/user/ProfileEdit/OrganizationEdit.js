import React, { Component } from 'react'
import { Modal, View, Text, SafeAreaView, Image, TouchableOpacity, Platform, StyleSheet, ScrollView, FlatList } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import axios from 'axios'
import cloneDeep from 'lodash/cloneDeep'
import { TextInput } from 'react-native-paper'
import LinearGradient from 'react-native-linear-gradient'
import ImagePicker from 'react-native-image-crop-picker'
import Snackbar from 'react-native-snackbar'

import styles2 from '../../../Components/GlobalCss/User/SignUpCss'
import Header from '../../../Components/User/SignUp/Header'
import { personalBusinessPageRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultProfile from '../../../../assets/defaultProfile.png'
import defaultBusiness from '../../../../assets/DefaultBusiness.png'
import typography from '../../../Components/Shared/Typography'
import defaultStyle from '../../../Components/Shared/Typography'
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader'
import { REACT_APP_userServiceURL, REACT_APP_authUser, REACT_APP_authSecret } from '../../../../env.json'
import InstitutionLogo from '../../../../assets/InstitutionLogo.png'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
class OrganizationEdit extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            organizationList: [],
            addJobModalOpen: false,
            modalterm: false,
            organizationType: '',
            organizationTypeModalOpen: false,
            organizationTypeSuggestions: [],
            organizationName: '',
            organizationEmail: '',
            coverImage: {},
            profileImage: {}
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value })
            // this.props.personalBusinessPageRequest({ userId: value, otherUserId: '' })
            this.getOrganizationList(value)
        }).catch((e) => {
            console.log(e)
        })

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/api/authenticate?username=' + REACT_APP_authUser + '&password=' + REACT_APP_authSecret,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
            .then((response) => {
                // console.log('---token---', response.headers.authorization)
                this.getOrganizationList2(response.headers.authorization)
            })

    }

    getOrganizationList2(token) {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/master/config/get',
            headers: { 'Content-Type': 'application/json', 'Authorization': token }

        }).then((response) => {
            let res = response.data
            if (res.message === 'Success!') {
                // console.log('---getOrganizationList---', res.body.fields.companyTypes)
                this.setState({ organizationTypeSuggestions: res.body.fields.companyTypes })

            }
        }).catch((err) => {
            this.setState({
                isLoaded: true,
                error: { message: err.response.data.message, err: err.response }
            })
        })
    }

    getOrganizationList(userId) {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/profile/get/business/pages?userId=' + userId,
            cache: true,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
            }
        }).then((response) => {
            let res = response.data
            if (res.message === 'Success!') {
                console.log(res.body)
                this.setState({ organizationList: res.body.businessPage })
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    goback = () => this.props.navigation.goBack()

    addButtonFunction = () => {
        this.setState({ addJobModalOpen: true })
    }

    unixTime = (UNIX_timestamp) => {
        var date = new Date(UNIX_timestamp)
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        var year = date.getFullYear()
        var month = months[date.getMonth()]
        var day = date.getDate()
        return day + ' ' + month + ' ' + year
    }

    renderItem = (item) => {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.props.navigation.navigate('ProfileStack', {
                    screen: 'CompanyProfileScreen',
                    params: { userId: item.companyId },
                })}
                style={styles.businessPageList}>

                {item.profileImageUrl ?
                    <Image source={{ uri: item.profileImageUrl }} style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }} />
                    :
                    <Image source={defaultBusiness} style={{ height: 46, width: 46, borderRadius: 6, marginRight: 10 }} />
                }

                <View>
                    <Text numberOfLines={1} style={{ color: COLORS.dark_900, fontFamily: 'Montserrat-SemiBold', fontSize: 14, maxWidth: 300 }}>
                        {item.companyName.charAt(0).toUpperCase() + item.companyName.slice(1)}
                    </Text>
                    <Text style={{ color: COLORS.altgreen_300, fontFamily: 'Montserrat-SemiBold', fontSize: 12 }}>
                        {item.country && item.country}
                    </Text>
                </View>

            </TouchableOpacity>
        )
    }

    termsModal = () => {
        return (
            <Modal onRequestClose={() => this.setState({ modalterm: false })} visible={this.state.modalterm} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={styles2.modalCountry}>

                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity style={{ alignSelf: 'center', width: 42, height: 42, borderRadius: 42 / 2, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F7F7F5', marginRight: 10, marginBottom: 10 }}
                        onPress={() => this.setState({ modalterm: false })} >
                        <Icon name='Cross' size={16} color='#367681' style={{ marginTop: 5 }} />
                    </TouchableOpacity>

                    <View style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundColor: '#EEF1EC', alignItems: 'center', paddingTop: 20, height: 240 }}>

                        <TouchableOpacity style={styles2.termsmodal} activeOpacity={0.7} onPress={() => { this.setState({ modalterm: false, addJobModalOpen: false }), this.props.navigation.navigate('TermsConditions') }} >
                            <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>Terms & Conditions</Text>
                            <Icon name="Export" color="#367681" />
                        </TouchableOpacity>

                        <TouchableOpacity style={styles2.termsmodal} activeOpacity={0.7} onPress={() => { this.setState({ modalterm: false, addJobModalOpen: false }), this.props.navigation.navigate('UserAgreement') }}>
                            <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>User Agreement</Text>
                            <Icon name="Export" color="#367681" />
                        </TouchableOpacity>

                        <TouchableOpacity style={styles2.termsmodal} activeOpacity={0.7} onPress={() => { this.setState({ modalterm: false, addJobModalOpen: false }), this.props.navigation.navigate('PrivacyPolicy') }}>
                            <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>Privacy Policy</Text>
                            <Icon name="Export" color="#367681" />
                        </TouchableOpacity>

                        <TouchableOpacity style={[styles2.termsmodal, { borderBottomWidth: 0 }]} activeOpacity={0.7} onPress={() => { this.setState({ modalterm: false, addJobModalOpen: false }), this.props.navigation.navigate('CookiePolicy') }}>
                            <Text style={{ fontSize: 16, color: '#154A59', fontWeight: '700' }}>Cookie Policy</Text>
                            <Icon name="Export" color="#367681" />
                        </TouchableOpacity>

                    </View>

                </View>

            </Modal>
        )
    }

    organizationTypeModal = () => {
        return (
            <Modal visible={this.state.organizationTypeModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

                <View style={{ marginTop: 'auto' }}>

                    <View style={{
                        width: '100%',
                        height: 700,
                        position: 'absolute',
                        bottom: 0,
                        alignSelf: 'center'
                    }}>
                        <LinearGradient colors={['#154A5900', '#154A59CC', '#154A59']} style={{
                            flex: 1,
                            paddingLeft: 15,
                            paddingRight: 15,
                            borderBottomLeftRadius: 6,
                            borderBottomRightRadius: 6
                        }}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ organizationTypeModalOpen: false })} >
                        <Icon name='Cross' size={13} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
                    </TouchableOpacity>

                    <View style={[defaultShape.Modal_Categories_Container, { height: 450, backgroundColor: COLORS.bgfill, paddingBottom: 10 }]}>
                        <View style={{ width: '100%', height: 58, backgroundColor: COLORS.altgreen_100, justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, borderTopLeftRadius: 20, borderTopRightRadius: 20, zIndex: 2 }}>
                            <Text style={[defaultStyle.Button_2, { color: COLORS.dark_900, textAlign: 'center' }]}>
                                ORGANIZATION TYPE
                            </Text>
                        </View>

                        <View>
                            <FlatList
                                keyboardShouldPersistTaps='handled'
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{ paddingBottom: 0, paddingTop: 46, width: '100%' }}
                                style={{ height: '50%' }}
                                keyExtractor={(item) => item}
                                data={this.state.organizationTypeSuggestions}
                                initialNumToRender={10}
                                renderItem={({ item }) => (
                                    <TouchableOpacity
                                        onPress={() => { this.setState({ organizationType: item, organizationTypeModalOpen: false }) }}
                                        style={styles.item} activeOpacity={0.7}>

                                        <Text
                                            numberOfLines={1}
                                            style={[defaultStyle.Button_Lead, { color: COLORS.dark_600, textAlign: 'center' }]}>{item}</Text>
                                        <View style={{ width: 17, height: 17, borderRadius: 8.5, alignItems: 'center', justifyContent: 'center' }}>
                                            {/* {this.state.organizationType.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                                        </View>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>

                    </View>
                </View>

            </Modal>
        )
    }

    openGallery = () => {

        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        })
            .then(image => {
                console.log(image)
                this.setState({
                    coverImage: {
                        uri: image.path,
                        type: image.mime,
                        name: image.path.split('/')[image.path.split('/').length - 1]
                    }
                })
            })
            .catch(err => console.log(err))

    }

    openGallery2 = () => {

        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        })
            .then(image => {
                console.log(image)
                this.setState({
                    profileImage: {
                        uri: image.path,
                        type: image.mime,
                        name: image.path.split('/')[image.path.split('/').length - 1]
                    }
                })
            })
            .catch(err => console.log(err))

    }

    handleSubmit2 = () => {

        const formData = new FormData()

        let photo = {
            uri: this.state.coverImage.uri,
            type: this.state.coverImage.type,
            name: this.state.coverImage.name
        }

        let photo2 = {
            uri: this.state.profileImage.uri,
            type: this.state.profileImage.type,
            name: this.state.profileImage.name
        }
        if (this.state.coverImage && this.state.coverImage.uri) formData.append('coverImage', photo)

        if (this.state.profileImage && this.state.profileImage.uri) formData.append('profileImage', photo2)

        let postBody = {
            "userId": this.state.userId
        };
        formData.append('info', JSON.stringify(postBody))
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/user/update/personal/info',
            headers: { 'Content-Type': 'multipart/form-data' },
            data: formData,
            withCredentials: true
        }).then((response) => {
            let res = response.data
            console.log(res.message)
            if (res.message === 'Success!') {

                Snackbar.show({
                    backgroundColor: '#97A600',
                    text: "Organization created successfully",
                    textColor: "#00394D",
                    duration: Snackbar.LENGTH_LONG,
                })
                this.getOrganizationList(this.state.userId)

                setTimeout(() => {
                    this.setState({
                        addJobModalOpen: false,
                        organizationName: '',
                        organizationEmail: '',
                        organizationType: '',
                        coverImage: {},
                        profileImage: {}
                    })
                }, 1000)

            }
        }).catch((err) => {
            console.log(err.response.data.message)
            if (err && err.response && err.response.data && err.response.data.message) {
                Snackbar.show({
                    backgroundColor: '#B22222',
                    text: err.response.data.message,
                    duration: Snackbar.LENGTH_LONG
                })
            }
        })

    }

    handleSubmit = () => {

        let postBody = {
            "userId": this.state.userId,
            "companyName": this.state.organizationName.trim(),
            "email": this.state.organizationEmail.toLowerCase(),
            "organizationType": this.state.organizationType,
            "operatorInfoSameAsUser": true,
            "agreeToTerms": true
        };
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/user/signup/business',
            headers: { 'Content-Type': 'application/json' },
            data: postBody,
            withCredentials: true
        }).then((response) => {
            let res = response.data
            console.log(res.message)
            if (res.message === 'Success!' && (this.state.coverImage === {} && !this.state.coverImage.uri) && (this.state.profileImage === {} && !this.state.profileImage.uri)) {

                Snackbar.show({
                    backgroundColor: '#97A600',
                    text: "Organization created successfully",
                    textColor: "#00394D",
                    duration: Snackbar.LENGTH_LONG,
                })
                this.getOrganizationList(this.state.userId)

                setTimeout(() => {
                    this.setState({
                        addJobModalOpen: false,
                        organizationName: '',
                        organizationEmail: '',
                        organizationType: ''
                    })
                }, 1000)
            }
            else this.handleSubmit2()

        }).catch((err) => {
            console.log(err.response.data.message)
            if (err && err.response && err.response.data && err.response.data.message) {
                Snackbar.show({
                    backgroundColor: '#B22222',
                    text: err.response.data.message,
                    duration: Snackbar.LENGTH_LONG
                })
            }
        })
    }

    validateForm = () => {
        if (this.state.organizationName.trim() === '') {

            Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Please enter the organization name',
                duration: Snackbar.LENGTH_LONG,
            })
        }

        else if (this.state.organizationEmail.trim() === '') {

            Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Please enter the organization email address',
                duration: Snackbar.LENGTH_LONG,
            })
        }

        else if (this.state.organizationType === '') {

            Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Please select the type of organization',
                duration: Snackbar.LENGTH_LONG,
            })
        }

        else this.handleSubmit()
    }

    addJobModal = () => {
        return (
            <Modal
                onRequestClose={() => this.setState({ addJobModalOpen: false })}
                visible={this.state.addJobModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>

                <ScrollView style={{ marginTop: -19 }}
                    keyboardShouldPersistTaps='handled'>

                    <View style={[defaultShape.Modal_Categories_Container, { height: '100%', backgroundColor: COLORS.grey_100 }]}>

                        <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 12 }] : styles.header}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%' }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => { this.setState({ addJobModalOpen: false }) }}>
                                        <Icon name="Arrow-Left" size={16} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                                    </TouchableOpacity>
                                    <Icon name="AddProject_Fl" size={18} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'ios' ? 0 : 8, marginLeft: 10 }} />
                                    <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_800, marginLeft: 6 }]}>
                                        Add Organzation
                                    </Text>
                                </View>
                                <TouchableOpacity onPress={() => this.validateForm()}
                                    activeOpacity={0.7} style={[styles.updateButton, { marginTop: 10 }]}>
                                    <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_200 }]}>Agree & Join</Text>
                                    <Icon name="Arrow_Right" size={13} color={COLORS.altgreen_200} style={{ marginTop: Platform.OS === 'ios' ? 0 : 8, marginLeft: 4 }} />
                                </TouchableOpacity>
                            </View>
                        </View>



                        <Header progress={-1}
                            text='Join Our Community' customStyle={(this.state.modalCountry || this.state.modalterm) ? { fontFamily: 'Montserrat-Medium' } : { fontFamily: 'Montserrat-Medium' }}
                            opacity={(this.state.modalCountry || this.state.modalterm) ? 0.3 : 1} />



                        <View style={{ backgroundColor: COLORS.white, alignItems: 'center', width: '100%', paddingBottom: 20, marginTop: 60 }}>

                            <TouchableOpacity activeOpacity={0.7} onPress={() => this.openGallery()}
                                style={{ height: 150, width: 300, backgroundColor: COLORS.altgreen_100, borderColor: COLORS.altgreen_200, borderWidth: 1, borderRadius: 8, marginTop: -60, alignItems: 'center', justifyContent: 'flex-start', paddingTop: 16 }}>

                                {this.state.coverImage && this.state.coverImage.uri ?
                                    <Image source={{ uri: this.state.coverImage.uri }} style={{ height: 150, width: 300, borderRadius: 8, marginTop: -16 }} />
                                    :
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <View style={{ height: 42, width: 42, backgroundColor: COLORS.altgreen_200, borderRadius: 21, justifyContent: 'center', alignItems: 'center' }}>
                                            <Icon name="UploadPhoto" size={18} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'ios' ? 0 : 8 }} />

                                        </View>

                                        <Text style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>Add Cover Image</Text>
                                    </View>
                                }
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.openGallery2()}
                                activeOpacity={0.7}
                                style={{ height: 120, width: 120, backgroundColor: COLORS.bgfill, borderColor: COLORS.grey_200, borderWidth: 1, borderRadius: 60, marginTop: -60, justifyContent: 'center', alignItems: 'center' }}>

                                {this.state.profileImage && this.state.profileImage.uri ?
                                    <Image source={{ uri: this.state.profileImage.uri }} style={{ height: 120, width: 120, borderRadius: 60 }} />
                                    :
                                    <View style={{ height: 42, width: 42, backgroundColor: COLORS.altgreen_200, borderRadius: 21, justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="UploadPhoto" size={18} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'ios' ? 0 : 8 }} />
                                    </View>
                                }

                            </TouchableOpacity>

                            {/* <Icon name="Company" size={18} color='#698F8A' style={{ marginTop: Platform.OS === 'ios' ? 6 : 14 }} />

                            <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_300, marginTop: -10 }]}>ORGANIZATION DETAIL</Text> */}

                            <View style={{
                                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 6, paddingRight: 12, marginVertical: 10
                            }}>

                                <TextInput
                                    theme={{ colors: { text: COLORS.dark_700, primary: COLORS.altgreen_300, placeholder: COLORS.altgreen_300 } }}
                                    label="Organization Name"
                                    selectionColor='#C8DB6E'
                                    style={[defaultStyle.Subtitle_1, { width: '90%', backgroundColor: COLORS.bgfill_200, color: COLORS.dark_700 }]}
                                    onChangeText={(value) => this.setState({ organizationName: value })}
                                    value={this.state.organizationName}
                                />

                            </View>

                            <View style={{
                                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 6, paddingRight: 12
                            }}>

                                <TextInput
                                    theme={{ colors: { text: COLORS.dark_700, primary: COLORS.altgreen_300, placeholder: COLORS.altgreen_300 } }}
                                    label="Organization Email"
                                    selectionColor='#C8DB6E'
                                    style={[defaultStyle.Subtitle_1, { width: '90%', backgroundColor: COLORS.bgfill_200, color: COLORS.dark_700 }]}
                                    onChangeText={(value) => this.setState({ organizationEmail: value })}
                                    value={this.state.organizationEmail}
                                />

                            </View>

                            <TouchableOpacity onPress={() => this.setState({ organizationTypeModalOpen: true })}
                                activeOpacity={0.6}
                                style={{
                                    height: 44, width: '86%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 20, borderWidth: 1, borderColor: COLORS.grey_300, borderRadius: 8, paddingHorizontal: 10
                                }}>


                                {/* <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>Circle Type</Text> */}
                                <Text style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>{this.state.organizationType !== '' ? this.state.organizationType : 'Organization Type'}</Text>


                                {/* <View style={{ width: '60%', height: 26, borderRadius: 13, justifyContent: 'center', alignItems: 'center', marginTop: 6 }}>
    
                                </View> */}

                                <View style={{ width: 16, height: 16, borderRadius: 8, backgroundColor: '#698F8A', justifyContent: 'center', alignItems: 'center', marginTop: 0 }}>
                                    <Icon name='Arrow_Down' size={9} color='#FFF' style={{ marginTop: Platform.OS === 'android' ? 4 : 0 }} />
                                </View>
                            </TouchableOpacity>

                        </View>


                        <View
                            style={{
                                height: 83, width: '86%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', marginTop: 16, paddingLeft: 0, paddingRight: 0, backgroundColor: COLORS.altgreen_100, borderRadius: 8
                            }}>

                            {/* <TouchableOpacity onPress={() => this.setState({ acceptPolicy: !this.state.acceptPolicy })}
                                activeOpacity={0.6}
                                style={{ width: 20, height: 20, borderRadius: 10, borderColor: COLORS.green_500, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: this.state.acceptPolicy ? COLORS.green_500 : COLORS.altgreen_t50 }}>
                                {this.state.acceptPolicy ? <Icon name="Tick" size={10} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>}
                            </TouchableOpacity> */}

                            <View>
                                <Text style={[defaultStyle.Caption, { color: '#698F8A' }]}>I accept the WeNaturalists Terms &</Text>
                                <Text style={[defaultStyle.Caption, { color: '#698F8A' }]}>Conditions, User Agreement, Privacy</Text>
                                <Text style={[defaultStyle.Caption, { color: '#698F8A' }]}>Policy, and Cookie Policy.</Text>
                            </View>

                            <TouchableOpacity onPress={() => this.setState({ modalterm: true })}
                                activeOpacity={0.5} style={{ height: 30, width: 30, alignItems: 'center', justifyContent: 'center' }}>
                                <Icon name="Export" size={16} color={COLORS.altgreen_300} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                            </TouchableOpacity>

                        </View>


                    </View>
                </ScrollView>

            </Modal>
        )
    }

    listFooterComponent = () => {
        return (
            <View style={styles.warning}>
                <Icon name="Caution" size={16} color={COLORS.dark_600} style={{ marginRight: 10 }} />
                <Text style={[typography.Caption, { color: COLORS.altgreen_400, marginTop: Platform.OS === 'android' ? -4 : 0 }]}>
                    {this.state.organizationList.length >= 2 ?
                        'You have created maximum Organization pages allowed per account'
                        :
                        'You can create upto 2 Organization pages'}
                </Text>
            </View>
        )
    }

    render() {

        return (
            <SafeAreaView style={{ flex: 1 }}>

                {this.addJobModal()}
                {this.termsModal()}
                {this.organizationTypeModal()}

                {/***** Header starts *****/}

                <ProfileEditHeader
                    name="Organization"
                    iconName="Company"
                    goback={this.goback}
                    addButton={this.state.organizationList && this.state.organizationList.length < 2 ? true : false}
                    addButtonFunction={this.addButtonFunction} />

                {/***** Header ends *****/}

                <View>
                    <FlatList
                        style={{ paddingTop: 15 }}
                        keyExtractor={(item) => item.companyId}
                        contentContainerStyle={{ paddingBottom: 100 }}
                        ListFooterComponent={this.listFooterComponent}
                        // data={this.state.organizationList ? this.state.organizationList.sort((a, b) => a.companyName.toUpperCase(0) > b.companyName.toUpperCase(0)).slice(0, 2) : []}
                        data={this.state.organizationList}
                        renderItem={({ item }) => (
                            this.renderItem(item)
                        )}
                    />
                </View>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    item: {
        flexDirection: 'row',
        width: '83%',
        height: 48,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'left',
        marginLeft: 30,
        // marginRight: 100,
        paddingHorizontal: '3%',
        borderBottomWidth: 1,
        borderBottomColor: COLORS.grey_200,
        // backgroundColor: 'pink'
    },
    updateButton: {
        flexDirection: 'row', width: 120, height: 28, borderRadius: 17, alignItems: 'center', justifyContent: 'center', marginVertical: 10, backgroundColor: COLORS.dark_700
    },
    businessPageList: {
        height: 56,
        width: '90%',
        alignItems: 'center',
        alignSelf: 'center',
        flexDirection: 'row',
        paddingBottom: 10,
        marginTop: 15,
        borderBottomWidth: 1,
        borderBottomColor: COLORS.grey_300
    },
    warning: {
        // position: 'absolute',
        // bottom: 60,
        width: '90%',
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: COLORS.grey_200,
        borderRadius: 8,
        paddingVertical: 5,
        paddingHorizontal: 15,
        marginTop: 20
    }
})

const mapStateToProps = (state) => {
    return {
        userBusinessPageProgress: state.personalProfileReducer.userBusinessPageProgress,
        userBusinessPage: state.personalProfileReducer.userBusinessPage,
        errorBusinessPage: state.personalProfileReducer.errorBusinessPage,
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        personalBusinessPageRequest: (data) => dispatch(personalBusinessPageRequest(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrganizationEdit)