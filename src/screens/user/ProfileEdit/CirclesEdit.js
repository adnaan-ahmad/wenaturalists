import React, { Component } from 'react';
import {
  Alert,
  ScrollView,
  Modal,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  ActivityIndicator,
  SafeAreaView,
  Platform,
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { TextInput } from 'react-native-paper';
import axios from 'axios';
import DatePicker from 'react-native-datepicker';
import Snackbar from 'react-native-snackbar';
import ImagePicker from 'react-native-image-crop-picker';
import cloneDeep from 'lodash/cloneDeep';

import EducationDefault from '../../../../assets/EducationDefault.webp'
import CircleZeroData from '../../../../assets/CircleZeroData.webp'
import circleDefault from '../../../../assets/CirclesDefault.png';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import defaultShape from '../../../Components/Shared/Shape';
import { userCircleRequest } from '../../../services/Redux/Actions/User/CircleActions';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultCover from '../../../../assets/defaultCover.png';
import { COLORS } from '../../../Components/Shared/Colors';
import defaultStyle from '../../../Components/Shared/Typography';
import CreateCircle from '../../../Components/User/EditProfile/CreateCircle';
import AddCircleMembers from '../../../Components/User/EditProfile/AddCircleMembers';
import { connectsRequest } from '../../../services/Redux/Actions/User/ConnectsActions';
import { personalConnectionInfoRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

class CirclesEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      createCircleModalOpen: false,
      addCircleMembersModalOpen: false,
      circleTypeModalOpen: false,
      shareModalOpen: false,

      userId: '',
      circleName: '',
      briefDescription: '',
      circleImage: {},

      selectedMemberList: [],
      circleTypeSuggestions: [],
      searchCircleType: '',
      selectedCircleType: '',
      startDate: this.currentDate(),

      country: 'Select Country *',
      state: 'Select State *',
      city: 'Select City *',
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login')
      }
    })

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({ userId: value });
        this.getDataByCircleId(value);
        this.props.userCircleRequest({ userId: value, otherUserId: '' });
        this.props.personalConnectionInfoRequest({ userId: value });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  createTwoButtonAlert = (id) =>
    Alert.alert('', 'Are you sure you want to delete this Circle?', [
      {
        text: 'YES',
        onPress: () => this.deleteCircle(id),
        style: 'cancel',
      },
      { text: 'NO' },
    ]);

  renderItem = (item) => {
    return (
      <View style={styles.circleCard}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() =>
            this.props.navigation.navigate('CircleProfileStack', {
              screen: 'CircleProfile',
              params: { slug: item.slug },
            })
          }
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          {item.profileImage ? (
            <Image
              style={{ width: 60, height: 60, borderRadius: 30 }}
              source={{ uri: item.profileImage }}
            />
          ) : (
            <Image
              style={{ width: 60, height: 60, borderRadius: 30 }}
              source={circleDefault}
            />
          )}

          <View style={{ marginLeft: 16 }}>
            <Text numberOfLines={1} style={styles.circleTitle}>
              {item.title.charAt(0).toUpperCase() + item.title.slice(1)}
            </Text>
            <Text numberOfLines={1} style={[styles.circleSubTitle, { color: 'rgb(105, 143, 138)' }]}>
              {item.memberType}
            </Text>
            <Text numberOfLines={1} style={styles.circleSubTitle}>
              {item.location}
            </Text>
          </View>
        </TouchableOpacity>
        {item.canDelete ? (
          <TouchableOpacity
            onPress={
              () => this.createTwoButtonAlert(item.id)
              // this.deleteCircle(item.id)
            }
            style={{
              width: 31,
              height: 31,
              borderRadius: 15.5,
              backgroundColor: COLORS.alertred_200,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            activeOpacity={0.6}>
            <Icon
              name="TrashBin"
              size={14}
              color={COLORS.alert_red}
              style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
            />
          </TouchableOpacity>
        ) : (
          <></>
        )}
      </View>
    );
  };

  unixTimeDatePicker = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    return day + '-' + month + '-' + year;
  };

  currentDate = () => {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = dd + '-' + mm + '-' + yyyy;

    return today
  }

  validateForm = () => {
    if (this.state.circleName.trim() === '') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please enter the title for your circle',
        duration: Snackbar.LENGTH_LONG,
      })
    }
    else if (this.props.userCircle.body.content.some(item => item.title.trim().toUpperCase() == this.state.circleName.trim().toUpperCase())) {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'This circle title already exists. Please choose a different one',
        duration: Snackbar.LENGTH_LONG,
      })
    } else if (this.state.selectedCircleType.trim() === '') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please select the type of your circle',
        duration: Snackbar.LENGTH_LONG,
      })
    } else if (this.state.country === 'Select Country *') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please select a country',
        duration: Snackbar.LENGTH_LONG,
      })
    } else if (this.state.state === 'Select State *') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please select a state',
        duration: Snackbar.LENGTH_LONG,
      });
    } else if (this.state.city === 'Select City *') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please select a city',
        duration: Snackbar.LENGTH_LONG,
      });
    } else if (this.state.briefDescription.trim() === '') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please enter the description of your circle',
        duration: Snackbar.LENGTH_LONG,
      });
    } else {
      this.setState({ addCircleMembersModalOpen: true }, () =>
        this.props.userConnectionInfo.body
          ? this.props.connectsRequest({
            userId: this.state.userId,
            size: this.props.userConnectionInfo.body.connections,
          })
          : null,
      );
    }
  };

  openCamera = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    })
      .then((image) => {
        // console.log(image)

        this.setState({
          circleImage: {
            uri: image.path,
            type: image.mime,
            name: image.path.split('/')[image.path.split('/').length - 1],
          },
        });
      })
      .catch((err) => console.log(err));
  };

  openGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    })
      .then((image) => {
        console.log(image);
        this.setState({
          circleImage: {
            uri: image.path,
            type: image.mime,
            name: image.path.split('/')[image.path.split('/').length - 1],
          },
        });
      })
      .catch((err) => console.log(err));
  };

  createCircleModal = () => {
    return (
      <Modal
        onRequestClose={() => this.setState({ createCircleModalOpen: false })}
        visible={this.state.createCircleModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <ScrollView
          style={{ marginTop: -19 }}
          keyboardShouldPersistTaps="handled">
          <View
            style={[
              defaultShape.Modal_Categories_Container,
              { height: '100%', backgroundColor: COLORS.grey_100 },
            ]}>
            <View
              style={
                Platform.OS === 'ios'
                  ? [styles.header, { paddingVertical: 12 }]
                  : styles.header
              }>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  width: '100%',
                }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <TouchableOpacity
                    style={{
                      width: 40,
                      height: 30,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={() => {
                      this.setState({
                        createCircleModalOpen: false,
                        circleName: '',
                        briefDescription: '',
                        selectedMemberList: [],
                        selectedCircleType: '',
                        country: 'Select Country *',
                        state: 'Select State *',
                        city: 'Select City *',
                        startDate: this.currentDate(),
                        circleImage: {}
                      })
                    }}>
                    <Icon
                      name="Arrow-Left"
                      size={16}
                      color="#91B3A2"
                      style={
                        Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }
                      }
                    />
                  </TouchableOpacity>
                  <Icon
                    name="Circles_Fl"
                    size={18}
                    color={COLORS.dark_800}
                    style={{
                      marginTop: Platform.OS === 'ios' ? 0 : 8,
                      marginLeft: 10,
                    }}
                  />
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      { color: COLORS.dark_800, marginLeft: 5 },
                    ]}>
                    Create a Circle
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => this.validateForm()}
                  activeOpacity={0.7}
                  style={[styles.updateButton, { marginTop: 10 }]}>
                  <Text
                    style={[
                      defaultStyle.Caption,
                      { color: COLORS.altgreen_200 },
                    ]}>
                    Next
                  </Text>
                  <Icon
                    name="Arrow_Right"
                    size={13}
                    color={COLORS.altgreen_200}
                    style={{
                      marginTop: Platform.OS === 'ios' ? 0 : 8,
                      marginLeft: 4,
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>

            <View
              style={{
                backgroundColor: COLORS.white,
                alignItems: 'center',
                width: '100%',
                paddingBottom: 16,
              }}>
              {/* <TouchableOpacity activeOpacity={0.7}
                                style={{ height: 150, width: 300, backgroundColor: COLORS.altgreen_100, borderColor: COLORS.altgreen_200, borderWidth: 1, borderRadius: 8, marginTop: 16, alignItems: 'center', paddingTop: 10 }}>
                                <View style={{ height: 42, width: 42, backgroundColor: COLORS.altgreen_200, borderRadius: 21, justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name="UploadPhoto" size={18} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'ios' ? 0 : 8 }} />

                                </View>
                                <Text style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>Add Cover Image</Text>
                            </TouchableOpacity> */}

              <TouchableOpacity
                onPress={() => this.setState({ shareModalOpen: true })}
                activeOpacity={0.7}
                style={{
                  height: 120,
                  width: 120,
                  backgroundColor: COLORS.bgfill,
                  borderColor: COLORS.grey_200,
                  borderWidth: 1,
                  borderRadius: 60,
                  marginTop: '6%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {this.state.circleImage && this.state.circleImage.uri ? (
                  <Image
                    source={{ uri: this.state.circleImage.uri }}
                    style={{ height: 120, width: 120, borderRadius: 60 }}
                  />
                ) : (
                  <View
                    style={{
                      height: 42,
                      width: 42,
                      backgroundColor: COLORS.altgreen_200,
                      borderRadius: 21,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      name="UploadPhoto"
                      size={18}
                      color={COLORS.dark_600}
                      style={{ marginTop: Platform.OS === 'ios' ? 0 : 8 }}
                    />
                  </View>
                )}
              </TouchableOpacity>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label="Circle Name *"
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Subtitle_1,
                    {
                      width: '90%',
                      backgroundColor: COLORS.bgfill_200,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) => this.setState({ circleName: value })}
                  value={this.state.circleName}
                />
              </View>

              <TouchableOpacity
                onPress={() => this.setState({ circleTypeModalOpen: true })}
                activeOpacity={0.6}
                style={{
                  height: 44,
                  width: '86%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 20,
                  borderWidth: 1,
                  borderColor: COLORS.grey_300,
                  borderRadius: 8,
                  paddingHorizontal: 10,
                }}>
                {/* <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>Circle Type</Text> */}
                <Text
                  style={[defaultStyle.Subtitle_1, { color: COLORS.dark_700 }]}>
                  {this.state.selectedCircleType !== ''
                    ? this.state.selectedCircleType
                    : 'Circle Type *'}
                </Text>

                {/* <View style={{ width: '60%', height: 26, borderRadius: 13, justifyContent: 'center', alignItems: 'center', marginTop: 6 }}>

                                </View> */}

                <View
                  style={{
                    width: 16,
                    height: 16,
                    borderRadius: 8,
                    backgroundColor: '#698F8A',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 0,
                  }}>
                  <Icon
                    name="Arrow_Down"
                    size={9}
                    color="#FFF"
                    style={{ marginTop: Platform.OS === 'android' ? 4 : 0 }}
                  />
                </View>
              </TouchableOpacity>
              {/* <View style={styles.border2}></View> */}
            </View>

            <Icon
              name="Location"
              size={18}
              color="#698F8A"
              style={{ marginTop: 14 }}
            />

            <Text
              style={[
                defaultStyle.Caption,
                {
                  color: COLORS.altgreen_300,
                  marginTop: Platform.OS === 'ios' ? 8 : -10,
                },
              ]}>
              LOCATION DETAIL
            </Text>

            <CreateCircle changeState={this.changeState} />
            {/* <CreateCircle country={this.state.country} state={this.state.state} city={this.state.city} /> */}

            <View
              style={{
                backgroundColor: COLORS.white,
                alignItems: 'center',
                width: '100%',
                paddingBottom: 24,
                marginTop: 0,
              }}>
              <TouchableOpacity
                activeOpacity={0.6}
                style={{
                  height: 56,
                  width: '86%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 20,
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <View style={{ width: '100%' }}>
                  <Text
                    style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                    Start Date *
                  </Text>
                  <DatePicker
                    style={{ width: '100%', borderWidth: 0 }}
                    date={
                      typeof this.state.startDate == 'string' &&
                        this.state.startDate.includes('-')
                        ? this.state.startDate
                        : this.unixTimeDatePicker(this.state.startDate)
                    }
                    mode="date"
                    placeholder="select date"
                    format="DD-MM-YYYY"
                    minDate="01-01-1900"
                    maxDate={this.currentDate()}
                    // maxDate="16-07-2021"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        top: -10,
                        marginRight: 0,
                      },
                      dateInput: {
                        alignItems: 'flex-start',
                        borderWidth: 0,
                      },
                      dateText: {
                        color: COLORS.dark_700,
                        marginTop: -20,
                      },
                    }}
                    onDateChange={(date) => {
                      this.setState({ startDate: date });
                    }}
                  />
                </View>
                {/* <Icon name="Calender_OL" color='#698F8A' size={16} style={{ marginTop: Platform.OS === 'android' ? 12 : 0, zIndex: 2 }} /> */}
              </TouchableOpacity>
              {/* <View style={styles.border2}></View> */}

              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  this.secondTextInput.focus()
                }}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 6,
                  paddingRight: 12,
                  marginTop: 20,
                  backgroundColor: COLORS.grey_100,
                  borderRadius: 10,
                }}>
                <TextInput
                  ref={(input) => {
                    this.secondTextInput = input
                  }}
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label="Brief Description *"
                  multiline
                  selectionColor="#C8DB6E"
                  // style={[
                  //   defaultStyle.Subtitle_1,
                  //   {
                  //     width: '90%',
                  //     backgroundColor: COLORS.white,
                  //     color: COLORS.dark_700,
                  //   },
                  // ]}
                  style={[
                    defaultStyle.H5,
                    {
                      minHeight: 200,
                      width: '90%',
                      backgroundColor: COLORS.grey_100,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) =>
                    this.setState({ briefDescription: value })
                  }
                  value={this.state.briefDescription}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.border2}></View>
            <View
              style={[
                styles.border,
                { position: 'absolute', bottom: 80 },
              ]}></View>
          </View>
        </ScrollView>
      </Modal >
    );
  };

  changeState = (value) => {
    this.setState(value);
  };

  addCircleMembersModal = () => {
    return (
      <Modal
        onRequestClose={() => this.setState({ addCircleMembersModalOpen: false })}
        visible={this.state.addCircleMembersModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <AddCircleMembers
          changeState={this.changeState}
          selectedMemberList={this.state.selectedMemberList}
          connectsData={
            this.props.userConnects.body
              ? this.props.userConnects.body.content.sort(
                (a, b) =>
                  a.firstName.toUpperCase() > b.firstName.toUpperCase(),
              )
              : null
          }
          handleCircleSubmit={this.handleCircleSubmit}
        // selectedMemberList={this.state.selectedMemberList}
        />
      </Modal>
    );
  };

  toUnixTimeStamp = () => {
    let tempStartDate = this.state.startDate.split('-').reverse().join('-');
    // console.log('new Date(tempStartDate).getTime()', new Date(tempStartDate).getTime())
    return new Date(tempStartDate).getTime();
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ shareModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text
                style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>
                Profile Image
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { paddingTop: 25, paddingBottom: 15 },
                  ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ shareModalOpen: false }, () =>
                  this.openGallery(),
                );
              }}>
              <Text
                style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                Choose from Gallery
              </Text>
              <Icon
                name="Img"
                size={18}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { paddingVertical: 15, borderBottomWidth: 0 },
                  ]
                  : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { borderBottomWidth: 0 },
                  ]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ shareModalOpen: false }, () => this.openCamera());
              }}>
              <Text
                style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                Click a Photo
              </Text>
              <Icon
                name="Camera"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleCircleSubmit = () => {
    const formData = new FormData();

    let params = {
      superAdminId: this.state.userId,
      title: this.state.circleName,
      description: this.state.briefDescription,
      memberIds: this.state.selectedMemberList.map((user) => user.id),
      type: this.state.selectedCircleType,
      country: this.state.country,
      state: this.state.state,
      city: this.state.city,
      incorporationDate: this.toUnixTimeStamp(this.state.startDate),
    };

    let photo = {
      uri: this.state.circleImage.uri,
      type: this.state.circleImage.type,
      name: this.state.circleImage.name,
    };

    formData.append(
      'profileImage',
      this.state.circleImage && this.state.circleImage.uri ? photo : null,
    );
    // formData.append('coverImage', this.state.circleImage && this.state.circleImage.uri ? photo : null)

    formData.append('data', JSON.stringify(params));
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/circle/create',
      headers: { 'Content-Type': 'multipart/form-data' },
      data: formData,
      withCredentials: true,
    })
      .then((response) => {
        let res = response && response.data;

        if (res.message === 'Success!') {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'Circle created successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log('res.message', res.message)
          this.props.userCircleRequest({
            userId: this.state.userId,
            otherUserId: '',
          });

          setTimeout(() => {
            this.setState({
              circleName: '',
              briefDescription: '',
              selectedMemberList: [],
              selectedCircleType: '',
              country: 'Select Country *',
              state: 'Select State *',
              city: 'Select City *',
              startDate: this.currentDate(),
              circleImage: {},
            });
          }, 1000);
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          console.log('err.response.data', err.response.data);
          if (err.response.data.status === '409 CONFLICT') {
            this.setState({ addCircleMembersModalOpen: false, createCircleModalOpen: true })
            setTimeout(() => {
              Snackbar.show({
                backgroundColor: '#B22222',
                text: 'This circle title already exists. Please choose a different one',
                duration: Snackbar.LENGTH_LONG,
              })
            }, 1000)
          } else {
            Snackbar.show({
              backgroundColor: '#B22222',
              text: 'Unable to create circle, please try again',
              duration: Snackbar.LENGTH_LONG,
            });
          }
        }
      });
  };

  searchCircleType = (query) => {
    if (query === '') {
      return this.state.circleTypeSuggestions;
    }

    const regex = new RegExp(`${query.trim()}`, 'i');

    return this.state.circleTypeSuggestions.filter(
      (item) => item.search(regex) >= 0,
    );
  };

  circleTypeModal = () => {
    return (
      <Modal
        visible={this.state.circleTypeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View
            style={{
              width: '100%',
              height: 700,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ circleTypeModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              { height: 450, backgroundColor: COLORS.bgfill, paddingBottom: 10 },
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  { color: COLORS.dark_900, textAlign: 'center' },
                ]}>
                SELECT CIRCLE TYPE
              </Text>
            </View>

            <View
              style={{
                height: 40,
                backgroundColor: '#F7F7F5',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '13%',
                zIndex: 2,
              }}>
              <TextInput
                style={styles.textInput}
                theme={{
                  colors: {
                    text: COLORS.dark_700,
                    primary: '#F7F7F5',
                    placeholder: COLORS.altgreen_300,
                  },
                }}
                // label="Type in address"
                selectionColor="#C8DB6E"
                onChangeText={(value) => {
                  this.setState({ searchCircleType: value });
                }}
                placeholder="Search"
                onFocus={() => this.setState({ searchIcon: false })}
                onBlur={() => this.setState({ searchIcon: true })}
                underlineColorAndroid="transparent"
                ref={(input) => {
                  this.textInput = input;
                }}
              />
            </View>

            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingBottom: 0,
                  paddingTop: 6,
                  width: '100%',
                }}
                style={{ height: '50%' }}
                keyExtractor={(item) => item}
                data={this.searchCircleType(this.state.searchCircleType)}
                initialNumToRender={10}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        selectedCircleType: item,
                        searchCircleType: '',
                        circleTypeModalOpen: false,
                      });
                    }}
                    style={styles.item}
                    activeOpacity={0.7}>
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.Button_Lead,
                        { color: COLORS.dark_600, textAlign: 'center' },
                      ]}>
                      {item}
                    </Text>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      {/* {this.state.selectedCircleType.includes(item) ? <Icon name="Tick" size={9} color={COLORS.dark_800} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} /> : <></>} */}
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  getDataByCircleId = (value) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/circle/get/config/' +
        value +
        '/' +
        '?circleId=' +
        value,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log('response.data.body.persona', response.data.body.persona)
          this.setState({
            circleTypeSuggestions: response.data.body.persona,
            //   userList: response.data.body.inviteeDetails
          });
        }
      })
      .catch((err) => {
        console.log('Error...', err);
      });
  };

  deleteCircle = (circleId) => {
    let postData = {
      circleId: circleId,
      userId: this.state.userId,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/circle/delete',
      data: postData,
      header: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.status === '202 ACCEPTED'
        ) {
          // console.log('response.data.status', response.data.status)
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'Circle deleted successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });
          this.props.userCircleRequest({
            userId: this.state.userId,
            otherUserId: '',
          });
        }
      })
      .catch((err) => {
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Unable to delete this circle',
          duration: Snackbar.LENGTH_LONG,
        });
        console.log(err);
      });
  };

  render() {
    console.log(
      'this.props.userCircle.body.content',
      this.props.userCircle.body && this.props.userCircle.body.content[0],
    );
    return (
      <SafeAreaView style={{ marginBottom: 16 }}>
        {this.createCircleModal()}
        {this.addCircleMembersModal()}
        {this.circleTypeModal()}
        {this.shareModal()}

        <View
          style={
            Platform.OS === 'ios'
              ? [styles.header, { paddingVertical: 12 }]
              : styles.header
          }>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              width: '100%',
            }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TouchableOpacity
                style={{
                  width: 40,
                  height: 30,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() => this.props.navigation.goBack()}>
                <Icon
                  name="Arrow-Left"
                  size={16}
                  color="#91B3A2"
                  style={
                    Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }
                  }
                />
              </TouchableOpacity>
              <Icon
                name="Circle_Ol"
                size={18}
                color={COLORS.dark_800}
                style={{
                  marginTop: Platform.OS === 'ios' ? 0 : 8,
                  marginLeft: 10,
                }}
              />
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  { color: COLORS.dark_800, marginLeft: 5 },
                ]}>
                CIRCLE
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                this.setState({ createCircleModalOpen: true });
              }}
              style={[
                defaultShape.Nav_Gylph_Btn,
                {
                  alignSelf: 'flex-end',
                  marginRight: 0,
                  marginTop: Platform.OS === 'ios' ? 0 : 8,
                },
              ]}>
              <Icon name="AddList" color={COLORS.altgreen_300} size={16} />
            </TouchableOpacity>
          </View>
        </View>

        {this.props.userCircle.body && this.props.userCircle.body.content.length ?
          <View style={{}}>
            <FlatList
              initialNumToRender={10}
              contentContainerStyle={{
                paddingTop: 10,
                paddingBottom: 46,
                justifyContent: 'center',
              }}
              // columnWrapperStyle={{flexDirection : "row", flexWrap : "wrap", paddingVertical:10, justifyContent:'center'}}
              // numColumns={4000}
              keyExtractor={(item) => item.id}
              data={this.props.userCircle.body.content}
              renderItem={({ item }) =>
                item.memberType === 'Super Admin' && this.renderItem(item)
              }
            />
          </View>
          :
          <View style={{ paddingHorizontal: 20 }}>
            <Image
              source={CircleZeroData}
              style={{ maxHeight: '100%', maxWidth: '100%', alignSelf: 'center', marginTop: '40%' }}
            />
            <Text style={{ color: '#698f8a', fontSize: 14, textAlign: 'center', marginTop: 16 }}>Create your own Circle to interact with your community of people with shared interests</Text>
          </View>
        }
        {/* <ActivityIndicator size='small' color="#00394D" /> */}
        {/* } */}
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  item: {
    flexDirection: 'row',
    width: '83%',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'left',
    marginLeft: 30,
    // marginRight: 100,
    paddingHorizontal: '3%',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey_200,
    // backgroundColor: 'pink'
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '90%',
    height: 26,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    zIndex: 2,
  },
  updateButton: {
    flexDirection: 'row',
    width: 81,
    height: 28,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: COLORS.dark_600,
  },
  circleCard: {
    flexDirection: 'row',
    width: '90%',
    height: 80,
    borderRadius: 8,
    backgroundColor: COLORS.bgFill_200,
    marginHorizontal: 8,
    marginVertical: 4,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: COLORS.grey_200,
    borderBottomWidth: 1,
    alignSelf: 'center',
    // paddingLeft: 12,
    // paddingRight: 26
  },
  circleTitle: {
    color: '#00394D',
    fontSize: 12,
    fontFamily: 'Montserrat-Bold',
    // marginTop: 10,
    // maxWidth: '85%'
  },
  circleSubTitle: {
    color: '#607580',
    fontSize: 10,
    fontFamily: 'Montserrat-Medium',
    // maxWidth: '85%'
  },
});

const mapStateToProps = (state) => {
  return {
    userCircleProgress: state.circleReducer.userCircleProgress,
    userCircle: state.circleReducer.userCircle,
    errorCircle: state.circleReducer.errorCircle,

    userConnectionInfoProgress:
      state.personalProfileReducer.userConnectionInfoProgress,
    userConnectionInfo: state.personalProfileReducer.userConnectionInfo,
    errorConnectionInfo: state.personalProfileReducer.errorConnectionInfo,

    userConnectsProgress: state.connectsReducer.userConnectsProgress,
    userConnects: state.connectsReducer.userConnects,
    errorConnects: state.connectsReducer.errorConnects,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userCircleRequest: (data) => dispatch(userCircleRequest(data)),
    personalConnectionInfoRequest: (data) =>
      dispatch(personalConnectionInfoRequest(data)),
    connectsRequest: (data) => dispatch(connectsRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CirclesEdit);
