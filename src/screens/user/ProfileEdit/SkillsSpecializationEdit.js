import React, {Component} from 'react';
import {
  TextInput,
  Modal,
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  StyleSheet,
  ScrollView,
  FlatList,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';
import Snackbar from 'react-native-snackbar';
import LinearGradient from 'react-native-linear-gradient';

import {personalProfileRequest} from '../../../services/Redux/Actions/User/PersonalProfileActions';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader';
import {REACT_APP_userServiceURL} from '../../../../env.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

class SkillsSpecializationEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      skills:
        this.props.user.body && this.props.user.body.skills
          ? this.props.user.body.skills
          : [],
      specialization:
        this.props.user.body && this.props.user.body.specialities
          ? this.props.user.body.specialities
          : [],
      skillsModalOpen: false,
      skillsSuggestions: [],
      searchSkill: '',
      specializationsModalOpen: false,
      searchSpecialization: '',
      selectedSkills:
        this.props.user.body && this.props.user.body.skills
          ? this.props.user.body.skills
          : [],
      specializationSuggestions: [],
      searchSpecialization: '',
      selectedSpecializations:
        this.props.user.body && this.props.user.body.specialities
          ? this.props.user.body.specialities
          : []
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({userId: value});
        this.props.personalProfileRequest({userId: value, otherUserId: ''});
      })
      .catch((e) => {
        console.log(e);
      });

    this.getMasterConfig();
  }

  goback = () => {
    this.props.navigation.goBack();
  };

  removeSkills = (index) => {
    let tempSkills = cloneDeep(this.state.skills);
    tempSkills.splice(index, 1);
    this.setState({skills: tempSkills, selectedSkills: tempSkills});
    // this.state.skills.splice(index, 1)
  };

  removeSpecialization = (index) => {
    let tempSpecialization = cloneDeep(this.state.specialization);
    tempSpecialization.splice(index, 1);
    this.setState({
      specialization: tempSpecialization,
      selectedSpecializations: tempSpecialization,
    });
    // this.state.specialization.splice(index, 1)
  };

  renderSkillsItem = (item) => {
    return (
      <View style={styles.skillsItemView}>
        <Text style={[defaultStyle.Caption, {color: COLORS.dark_600}]}>
          {item.item}
        </Text>
        <TouchableOpacity
          onPress={() => this.removeSkills(item.index)}
          style={{marginLeft: 5}}>
          <Icon name="Cross" size={10} color={COLORS.altgreen_300} />
        </TouchableOpacity>
      </View>
    );
  };

  renderSpecializationItem = (item) => {
    return (
      <View style={styles.specializationItemView}>
        <Text style={[defaultStyle.Caption, {color: COLORS.dark_600}]}>
          {item.item}
        </Text>
        <TouchableOpacity
          onPress={() => this.removeSpecialization(item.index)}
          style={{marginLeft: 5}}>
          <Icon name="Cross" size={10} color={COLORS.altgreen_300} />
        </TouchableOpacity>
      </View>
    );
  };

  handleSubmit = () => {
    let postBodySkills = {
      userId: this.state.userId,
      skills: this.state.skills,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/profile/update/skills',
      headers: {'Content-Type': 'application/json'},
      data: postBodySkills,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '200 OK') {
          console.log('skills updated: ', res);
          this.updateSpecialization();
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          Snackbar.show({
            backgroundColor: COLORS.alert_red,
            text: err.response.data.message,
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
  };

  getMasterConfig = () => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/master/config/get',
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          let list = [];
          let skills = res.body.fields.skills.sort();
          let specialities = res.body.fields.specialities.sort();
          skills.map((data, index) => {
            list.push({text: data, id: index});
          });
          this.setState({
            skillsSuggestions: list,
            specializationSuggestions: specialities,
          });
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          console.log('err in fetching skills: ', err.response.data.message);
        }
      });
  };

  updateSpecialization = () => {
    let postBodySpecialization = {
      userId: this.state.userId,
      specialities: this.state.specialization,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/profile/update/specialities',
      headers: {'Content-Type': 'application/json'},
      data: postBodySpecialization,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '200 OK') {
          console.log('specialization updated: ', res);
          this.props.personalProfileRequest({
            userId: this.state.userId,
            otherUserId: '',
          });
          Snackbar.show({
            backgroundColor: COLORS.dark_900,
            text: 'Skills & Specialization updated successfully',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          Snackbar.show({
            backgroundColor: COLORS.alert_red,
            text: err.response.data.message,
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
  };

  searchSkills = (query) => {
    if (query === '') {
      return this.state.skillsSuggestions;
    }

    const regex = new RegExp(`${query.trim()}`, 'i');

    return this.state.skillsSuggestions.filter(
      (item) => item.text.search(regex) >= 0,
    );
  };

  searchSpecializations = (query) => {
    if (query === '') {
      return this.state.specializationSuggestions;
    }

    const regex = new RegExp(`${query.trim()}`, 'i');

    return this.state.specializationSuggestions.filter(
      (item) => item.search(regex) >= 0,
    );
  };

  skillsModal = () => {
    return (
      <Modal
        visible={this.state.skillsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 700,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({skillsModalOpen: false, selectedSkills: this.state.originalSkills})}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 450, backgroundColor: COLORS.bgfill},
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                ADD SKILLS
              </Text>
            </View>

            <View
              style={{
                height: 40,
                backgroundColor: '#F7F7F5',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '12%',
              }}>
              <TextInput
                style={styles.textInput}
                placeholderTextColor="#D9E1E4"
                onChangeText={(value) => {
                  this.setState({searchSkill: value});
                }}
                color="#154A59"
                placeholder="Search"
                onFocus={() => this.setState({searchIcon: false})}
                onBlur={() => this.setState({searchIcon: true})}
                underlineColorAndroid="transparent"
                ref={(input) => {
                  this.textInput = input;
                }}
              />
            </View>

            <FlatList
              keyboardShouldPersistTaps="handled"
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{paddingBottom: 10, paddingTop: 6}}
              style={{height: '50%'}}
              keyExtractor={(item) => item.id}
              data={this.searchSkills(this.state.searchSkill)}
              initialNumToRender={10}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => {
                    this.state.selectedSkills.includes(item.text)
                      ? this.setState({
                          selectedSkills: this.state.selectedSkills.filter(
                            (value) => value !== item.text,
                          ),
                        })
                      : this.setState({
                          selectedSkills: [
                            ...this.state.selectedSkills,
                            item.text,
                          ],
                        });
                  }}
                  style={styles.item}
                  activeOpacity={0.7}>
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {color: COLORS.dark_600, textAlign: 'left'},
                    ]}>
                    {item.text}
                  </Text>
                  <View
                    style={{
                      width: 17,
                      height: 17,
                      borderRadius: 8.5,
                      borderColor: COLORS.green_500,
                      borderWidth: 2,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: this.state.selectedSkills.includes(
                        item.text,
                      )
                        ? COLORS.green_500
                        : COLORS.altgreen_t50,
                    }}>
                    {this.state.selectedSkills.includes(item.text) ? (
                      <Icon
                        name="Tick"
                        size={9}
                        color={COLORS.dark_800}
                        style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
                      />
                    ) : (
                      <></>
                    )}
                  </View>
                </TouchableOpacity>
              )}
            />

            <TouchableOpacity
              onPress={() => {
                this.setState({
                  skillsModalOpen: false,
                  skills: this.state.selectedSkills,
                });
              }}
              activeOpacity={0.7}
              style={[
                {
                  width: 66,
                  height: 28,
                  borderRadius: 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginVertical: 10,
                  backgroundColor: COLORS.dark_600,
                },
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_100}]}>
                Add
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  specializationsModal = () => {
    return (
      <Modal
        visible={this.state.specializationsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 700,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({specializationsModalOpen: false, selectedSpecializations: this.state.originalSpecializations})}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 450, backgroundColor: COLORS.bgfill},
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                ADD SPECIALIZATION
              </Text>
            </View>

            <View
              style={{
                height: 40,
                backgroundColor: '#F7F7F5',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '12%',
              }}>
              <TextInput
                style={styles.textInput}
                placeholderTextColor="#D9E1E4"
                onChangeText={(value) => {
                  this.setState({searchSpecialization: value});
                }}
                color="#154A59"
                placeholder="Search"
                onFocus={() => this.setState({searchIcon: false})}
                onBlur={() => this.setState({searchIcon: true})}
                underlineColorAndroid="transparent"
                ref={(input) => {
                  this.textInput = input;
                }}
              />
            </View>

            <FlatList
              keyboardShouldPersistTaps="handled"
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{paddingBottom: 10, paddingTop: 6}}
              style={{height: '50%'}}
              keyExtractor={(item) => item}
              data={this.searchSpecializations(this.state.searchSpecialization)}
              initialNumToRender={10}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => {
                    this.state.selectedSpecializations.includes(item)
                      ? this.setState({
                          selectedSpecializations: this.state.selectedSpecializations.filter(
                            (value) => value !== item,
                          ),
                        })
                      : this.setState({
                          selectedSpecializations: [
                            ...this.state.selectedSpecializations,
                            item,
                          ],
                        });
                  }}
                  style={styles.item}
                  activeOpacity={0.7}>
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {color: COLORS.dark_600, textAlign: 'left'},
                    ]}>
                    {item}
                  </Text>
                  <View
                    style={{
                      width: 17,
                      height: 17,
                      borderRadius: 8.5,
                      borderColor: COLORS.green_500,
                      borderWidth: 2,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: this.state.selectedSpecializations.includes(
                        item,
                      )
                        ? COLORS.green_500
                        : COLORS.altgreen_t50,
                    }}>
                    {this.state.selectedSpecializations.includes(item) ? (
                      <Icon
                        name="Tick"
                        size={9}
                        color={COLORS.dark_800}
                        style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
                      />
                    ) : (
                      <></>
                    )}
                  </View>
                </TouchableOpacity>
              )}
            />

            <TouchableOpacity
              onPress={() => {
                this.setState({
                  specializationsModalOpen: false,
                  specialization: this.state.selectedSpecializations,
                });
              }}
              activeOpacity={0.7}
              style={[
                {
                  width: 66,
                  height: 28,
                  borderRadius: 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginVertical: 10,
                  backgroundColor: COLORS.dark_600,
                },
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_100}]}>
                Add
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {/***** Header starts *****/}

        <ProfileEditHeader
          name="Skills & Specialization"
          iconName="Diamond"
          goback={this.goback}
          updateButton={true}
          updateButtonFunction={this.handleSubmit}
        />

        {/***** Header ends *****/}

        {this.skillsModal()}
        {this.specializationsModal()}

        <ScrollView>
          {/***** Skills starts *****/}

          <View style={styles.skillsView}>
            <Icon name="Skill" size={20} color={COLORS.altgreen_400} />
            <Text
              style={[
                defaultStyle.Caption,
                {
                  color: COLORS.altgreen_300,
                  marginTop: Platform.OS === 'ios' ? 5 : -10,
                },
              ]}>
              SKILLS
            </Text>
          </View>

          <View>
            <FlatList
              contentContainerStyle={{
                flexWrap: 'wrap',
                flexDirection: 'row',
                paddingHorizontal: 8,
                marginTop: 10,
                paddingBottom: 10,
              }}
              data={this.state.skills}
              keyExtractor={(item) => item}
              ListFooterComponent={
                <TouchableOpacity
                  onPress={() => this.setState({skillsModalOpen: true, originalSkills: this.state.selectedSkills })}
                  style={styles.addSkillSpecialization}>
                  <Text
                    style={[defaultStyle.Caption, {color: COLORS.dark_600}]}>
                    + Add Skills
                  </Text>
                </TouchableOpacity>
              }
              renderItem={(item) => this.renderSkillsItem(item)}
            />
          </View>

          {/***** Skills ends *****/}

          {/***** Specialization starts *****/}

          <View style={styles.skillsView}>
            <Icon name="Diamond" size={20} color={COLORS.altgreen_400} />
            <Text
              style={[
                defaultStyle.Caption,
                {
                  color: COLORS.altgreen_300,
                  marginTop: Platform.OS === 'ios' ? 5 : -10,
                },
              ]}>
              SPECIALIZATION
            </Text>
          </View>

          <View>
            <FlatList
              contentContainerStyle={{
                flexWrap: 'wrap',
                flexDirection: 'row',
                paddingHorizontal: 8,
                marginTop: 10,
                paddingBottom: 10,
              }}
              data={this.state.specialization}
              keyExtractor={(item) => item}
              ListFooterComponent={
                <TouchableOpacity
                  style={styles.addSkillSpecialization}
                  onPress={() =>
                    this.setState({specializationsModalOpen: true, originalSpecializations: this.state.selectedSpecializations})
                  }>
                  <Text
                    style={[defaultStyle.Caption, {color: COLORS.dark_600}]}>
                    + Add Specialization
                  </Text>
                </TouchableOpacity>
              }
              renderItem={(item) => this.renderSpecializationItem(item)}
            />
          </View>

          {/***** Specialization ends *****/}
        </ScrollView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  skillsView: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    // borderBottomColor: COLORS.grey_350,
    // borderBottomWidth: 2
  },
  item: {
    flexDirection: 'row',
    width: '70%',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'flex-start',
    marginLeft: '6%',
    paddingHorizontal: '3%',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey_200,
  },
  skillsItemView: {
    flexDirection: 'row',
    paddingHorizontal: 8,
    paddingVertical: 4,
    backgroundColor: COLORS.altgreen_t50,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: COLORS.altgreen_300,
    marginLeft: 8,
    marginTop: 12,
  },
  specializationItemView: {
    flexDirection: 'row',
    paddingHorizontal: 8,
    paddingVertical: 4,
    backgroundColor: COLORS.green_100,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: COLORS.green_500,
    marginLeft: 8,
    marginTop: 12,
  },
  addSkillSpecialization: {
    padding: 7,
    backgroundColor: COLORS.grey_200,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: COLORS.grey_300,
    marginLeft: 8,
    marginTop: 12,
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '90%',
    zIndex: 1,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
});

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SkillsSpecializationEdit);
