import React, { Component } from 'react';
import {
  Alert,
  Modal,
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Platform,
  StyleSheet,
  ScrollView,
  FlatList,
  KeyboardAvoidingView
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';
import LinearGradient from 'react-native-linear-gradient';
import { TextInput } from 'react-native-paper';
import Snackbar from 'react-native-snackbar';
import * as _ from 'lodash';
import DatePicker from 'react-native-datepicker';

import { personalEducationRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import { COLORS } from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import EducationDefault from '../../../../assets/EducationDefault.webp';
import defaultCover from '../../../../assets/defaultCover.png';
import typography from '../../../Components/Shared/Typography';
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import InstitutionLogo from '../../../../assets/InstitutionLogo.png';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
class EducationEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      isLoading: false,
      editEducationModalOpen: false,
      addEducationModalOpen: false,
      educationId: '',
      institute: '',
      fieldOfStudy: '',
      startTimeUnix: 0,
      endTimeUnix: 0,

      addInstitute: '',
      addFieldOfStudy: '',
      addStartTimeUnix: 0,
      addEndTimeUnix: 0,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({ userId: value });
        this.props.personalEducationRequest({ userId: value, otherUserId: '' });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  goback = () => {
    this.props.navigation.goBack();
  };

  addButtonFunction = () => {
    this.setState({
      addEducationModalOpen: true,
      addInstitute: '',
      addFieldOfStudy: '',
      addStartTimeUnix: 0,
      addEndTimeUnix: 0,
    });
  };

  currentDate = () => {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = dd + '-' + mm + '-' + yyyy;

    return today;
  };

  unixTime = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    return day + ' ' + month + ' ' + year;
  };

  unixTimeDatePicker = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();

    return day + '-' + month + '-' + year;
  };

  validateForm() {
    let formIsValid = true;

    if (
      _.isUndefined(this.state.addFieldOfStudy) ||
      _.isEmpty((this.state.addFieldOfStudy || '').toString()) ||
      _.isNull(this.state.addFieldOfStudy)
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Course is mandatory',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      formIsValid = false;
      this.setState({ isLoading: false });
    }
    if (
      _.isUndefined(this.state.addStartTimeUnix) ||
      _.isEmpty((this.state.addStartTimeUnix || '').toString()) ||
      _.isNull(this.state.addStartTimeUnix)
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Start date is mandatory',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      formIsValid = false;
      this.setState({ isLoading: false });
    }
    if (
      _.isUndefined(this.state.addInstitute) ||
      _.isEmpty((this.state.addInstitute || '').toString()) ||
      _.isNull(this.state.addInstitute)
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Institution is mandatory',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      formIsValid = false;
      this.setState({ isLoading: false });
    }
    return formIsValid;
  }

  addNewEducation = () => {
    this.setState({ isLoading: true });
    if (this.validateForm()) {
      var today = new Date().getTime();
      let postBody = {
        userId: this.state.userId,
        specialisations: this.state.addFieldOfStudy,
        institution: this.state.addInstitute,
        startTime: this.state.addStartTimeUnix,
        endTime: this.state.addEndTimeUnix,
        graduate: this.state.addEndTimeUnix > today ? false : true,
      };
      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/profile/update/education',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (res.status === '200 OK') {
            this.props.personalEducationRequest({
              userId: this.state.userId,
              otherUserId: '',
            });
            this.setState({ isLoading: false, addEducationModalOpen: false });
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            this.setState({ isLoading: false });
            Snackbar.show({
              backgroundColor: COLORS.alert_red,
              text: err.response.data.message,
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    }
  };

  updateValidateForm() {
    let formIsValid = true;
    if (
      _.isUndefined(this.state.institute) ||
      _.isEmpty((this.state.institute || '').toString()) ||
      _.isNull(this.state.institute)
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Institution is mandatory',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      formIsValid = false;
      this.setState({ isLoading: false });
    }
    if (
      _.isUndefined(this.state.startTimeUnix) ||
      _.isEmpty((this.state.startTimeUnix || '').toString()) ||
      _.isNull(this.state.startTimeUnix)
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'start date is mandatory',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      formIsValid = false;
      this.setState({ isLoading: false });
    }
    if (
      _.isUndefined(this.state.fieldOfStudy) ||
      _.isEmpty((this.state.fieldOfStudy || '').toString()) ||
      _.isNull(this.state.fieldOfStudy)
    ) {
      Snackbar.show({
        backgroundColor: COLORS.alert_red,
        text: 'Specialization is mandatory',
        textColor: COLORS.altgreen_100,
        duration: Snackbar.LENGTH_LONG,
      });
      formIsValid = false;
      this.setState({ isLoading: false });
    }

    return formIsValid;
  }

  updateEducation = () => {
    this.setState({ isLoading: true });
    if (this.updateValidateForm()) {
      var today = new Date().getTime();
      let postBody = {
        userId: this.state.userId,
        educationId: this.state.educationId,
        specialisations: this.state.fieldOfStudy,
        institution: this.state.institute,
        startTime: this.state.startTimeUnix,
        endTime: this.state.endTimeUnix,
        graduate: this.state.endTimeUnix > today ? false : true,
      };
      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/profile/update/education',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (res.status === '200 OK') {
            this.props.personalEducationRequest({
              userId: this.state.userId,
              otherUserId: '',
            });
          }
          this.setState({ isLoading: false, editEducationModalOpen: false });
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            this.setState({ isLoading: false });
            Snackbar.show({
              backgroundColor: COLORS.alert_red,
              text: err.response.data.message,
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    }
  };

  deleteEducation = (id) => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/profile/delete/education?id=' + id,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          this.props.personalEducationRequest({
            userId: this.state.userId,
            otherUserId: '',
          });
        }
      })
      .catch((err) => {
        console.log('error in education delete: ', err.response.data.message);
      });
  };

  createTwoButtonAlert = (id) =>
    Alert.alert(
      "",
      "Are you sure you want to delete your education?",
      [
        {
          text: "YES",
          onPress: () => this.deleteEducation(id),
          style: "cancel"
        },
        { text: "NO" }
      ]
    )

  renderItem = (item) => {
    return (
      <View style={styles.educationList}>
        <View style={{ flexDirection: 'row' }}>
          <Image
            source={InstitutionLogo}
            style={{ height: 36, width: 36, borderRadius: 18, marginRight: 15 }}
          />
          <View>
            <Text style={[typography.Button_Lead, { color: COLORS.dark_800, maxWidth: 200 }]}>
              {item.specialisations}
            </Text>
            <Text style={styles.eduSubTitle}>{item.institution}</Text>
            {this.unixTime(item.endTime).split(' ').splice(1, 2).join(' ') !==
              'Jan 1970' ? (
              <Text style={[typography.Note, { color: COLORS.grey_400 }]}>
                {this.unixTime(item.startTime).split(' ').splice(1).join(' ')}{' '}
                to{' '}
                {this.unixTime(item.endTime).split(' ').splice(1, 2).join(' ')}
              </Text>
            ) : (
              <Text style={[typography.Note, { color: COLORS.grey_400 }]}>
                {this.unixTime(item.startTime).split(' ').splice(1).join(' ')}{' '}
                to Present
              </Text>
            )}
          </View>
        </View>

        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            style={defaultShape.AdjunctBtn_Small_Sec}
            onPress={() =>
              this.setState({
                editEducationModalOpen: true,
                startTimeUnix: item.startTime,
                endTimeUnix: item.endTime,
                institute: item.institution,
                fieldOfStudy: item.specialisations,
                educationId: item.id,
              })
            }>
            <Icon
              name="EditBox"
              color={COLORS.dark_600}
              size={14}
              style={Platform.OS === 'android' && { marginTop: 5 }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.createTwoButtonAlert(item.id)}
            style={[defaultShape.AdjunctBtn_Small_Sec, { marginLeft: 10 }]}>
            <Icon
              name="TrashBin"
              color={COLORS.alertred_200}
              size={14}
              style={Platform.OS === 'android' && { marginTop: 5 }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  getMinEndDate = (startTime) => {
    var day = new Date(startTime)

    var nextDay = new Date(day)
    nextDay.setDate(day.getDate())
    return nextDay
  }

  editEducationModal = () => {
    return (
      <Modal
        visible={this.state.editEducationModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{ flex: 1 }}>
          <View style={{ marginTop: 'auto' }}>
            <View style={defaultShape.Linear_Gradient_View}>
              <LinearGradient
                colors={['#154A5900', '#154A59CC', '#154A59']}
                style={defaultShape.Linear_Gradient}></LinearGradient>
            </View>

            <TouchableOpacity
              activeOpacity={0.7}
              style={defaultShape.CloseBtn}
              onPress={() => this.setState({ editEducationModalOpen: false })}>
              <Icon
                name="Cross"
                size={13}
                color="#367681"
                style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
              />
            </TouchableOpacity>

            <View
              style={[
                defaultShape.Modal_Categories_Container,
                { height: 373, backgroundColor: COLORS.bgfill },
              ]}>
              <View style={styles.modalHeaderView}>
                <Text
                  style={[
                    typography.Button_2,
                    { color: COLORS.dark_900, textAlign: 'center' },
                  ]}>
                  UPDATE EDUCATION
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 56,
                  width: '90%',
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label={<Text>Institution <Text style={{ color: 'red' }}>*</Text></Text>}
                  // mode='outlined'
                  selectionColor="#C8DB6E"
                  style={[
                    typography.Subtitle_1,
                    {
                      width: '98%',
                      height: 56,
                      backgroundColor: COLORS.bgfill,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) => this.setState({ institute: value })}
                  value={this.state.institute}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 10,
                  width: '90%',
                }}>
                <TouchableOpacity
                  activeOpacity={0.6}
                  style={{
                    height: 56,
                    width: '40%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 20,
                    paddingLeft: 6,
                    paddingRight: 12,
                  }}>
                  <View style={{ width: '100%' }}>
                    <Text
                      style={[typography.Note2, { color: COLORS.altgreen_300 }]}>
                      Time Period <Text style={{ color: 'red' }}>*</Text>
                    </Text>

                    <DatePicker
                      style={{ width: '100%', borderWidth: 0 }}
                      date={
                        this.state.startTimeUnix === 0
                          ? ''
                          : this.unixTimeDatePicker(this.state.startTimeUnix)
                      }
                      mode="date"
                      placeholder="Start date"
                      format="DD-MM-YYYY"
                      minDate="01-01-1900"
                      maxDate={this.currentDate()}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          right: 0,
                          top: -10,
                          marginRight: 0,
                        },
                        dateInput: {
                          alignItems: 'flex-start',
                          borderWidth: 0,
                        },
                        dateText: {
                          color: COLORS.dark_700,
                          marginTop: -20,
                        },
                        // ... You can check the source to find the other keys.
                      }}
                      onDateChange={(date) => {
                        let tempdate = date.split('-').reverse().join('-');
                        let convertedUnixTime = new Date(tempdate).getTime();
                        this.setState({ startTimeUnix: convertedUnixTime, endTimeUnix:
                          convertedUnixTime >
                            new Date(this.state.endTimeUnix).getTime()
                            ? 0
                            : this.state.endTimeUnix });
                      }}
                    />
                  </View>
                  {/* <Icon name="Calender_OL" color='#698F8A' size={16} style={{ marginTop: Platform.OS === 'android' ? 12 : 0, zIndex: 2 }} /> */}
                </TouchableOpacity>

                {this.state.startTimeUnix ? (
                <TouchableOpacity
                  activeOpacity={0.6}
                  style={{
                    height: 56,
                    width: '40%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 20,
                    paddingLeft: 6,
                    paddingRight: 12,
                  }}>
                  <View style={{ width: '100%' }}>
                    <Text
                      style={[typography.Note2, { color: COLORS.altgreen_300 }]}>
                      End Date
                    </Text>

                    <DatePicker
                      style={{ width: '100%', borderWidth: 0 }}
                      date={
                        this.state.endTimeUnix === 0
                          ? ''
                          : this.unixTimeDatePicker(this.state.endTimeUnix)
                      }
                      mode="date"
                      placeholder="End date"
                      format="DD-MM-YYYY"
                      minDate={this.getMinEndDate(this.state.startTimeUnix)}
                      maxDate={this.currentDate()}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          right: 0,
                          top: -10,
                          marginRight: 0,
                        },
                        dateInput: {
                          alignItems: 'flex-start',
                          borderWidth: 0,
                        },
                        dateText: {
                          color: COLORS.dark_700,
                          marginTop: -20,
                        },
                        // ... You can check the source to find the other keys.
                      }}
                      onDateChange={(date) => {
                        let tempdate = date.split('-').reverse().join('-');
                        let convertedUnixTime = new Date(tempdate).getTime();
                        this.setState({ endTimeUnix: convertedUnixTime });
                      }}
                    />
                  </View>
                  {/* <Icon name="Calender_OL" color='#698F8A' size={16} style={{ marginTop: Platform.OS === 'android' ? 12 : 0, zIndex: 2 }} /> */}
                </TouchableOpacity>) : null}
              </View>
              <View style={styles.border2}></View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 10,
                  width: '90%',
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label={<Text>Field of Study <Text style={{ color: 'red' }}>*</Text></Text>}
                  // mode='outlined'
                  selectionColor="#C8DB6E"
                  style={[
                    typography.Subtitle_1,
                    {
                      width: '98%',
                      height: 56,
                      backgroundColor: COLORS.bgfill,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) => this.setState({ fieldOfStudy: value })}
                  value={this.state.fieldOfStudy}
                />
              </View>
              <View style={styles.border2}></View>

              <TouchableOpacity
                onPress={this.updateEducation}
                activeOpacity={0.7}
                style={[styles.updateButton, { marginTop: 30 }]}>
                <Text
                  style={[typography.Caption, { color: COLORS.altgreen_100 }]}>
                  Update
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  };

  addEducationModal = () => {
    return (
      <Modal
        visible={this.state.addEducationModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{ flex: 1 }}>
          <View style={{ marginTop: 'auto' }}>
            <View style={defaultShape.Linear_Gradient_View}>
              <LinearGradient
                colors={['#154A5900', '#154A59CC', '#154A59']}
                style={defaultShape.Linear_Gradient}></LinearGradient>
            </View>

            <TouchableOpacity
              activeOpacity={0.7}
              style={defaultShape.CloseBtn}
              onPress={() => this.setState({ addEducationModalOpen: false })}>
              <Icon
                name="Cross"
                size={13}
                color="#367681"
                style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }}
              />
            </TouchableOpacity>

            <View
              style={[
                defaultShape.Modal_Categories_Container,
                { height: 373, backgroundColor: COLORS.bgfill },
              ]}>
              <View style={styles.modalHeaderView}>
                <Text
                  style={[
                    typography.Button_2,
                    { color: COLORS.dark_900, textAlign: 'center' },
                  ]}>
                  UPDATE EDUCATION
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 56,
                  width: '90%',
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label={<Text>Institution <Text style={{ color: 'red' }}>*</Text></Text>}
                  // mode='outlined'
                  selectionColor="#C8DB6E"
                  style={[
                    typography.Subtitle_1,
                    {
                      width: '98%',
                      height: 56,
                      backgroundColor: COLORS.bgfill,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) => this.setState({ addInstitute: value })}
                  value={this.state.addInstitute}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 10,
                  width: '90%',
                }}>
                <TouchableOpacity
                  activeOpacity={0.6}
                  style={{
                    height: 56,
                    width: '40%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 20,
                    paddingLeft: 6,
                    paddingRight: 12,
                  }}>
                  <View style={{ width: '100%' }}>
                    <Text
                      style={[typography.Note2, { color: COLORS.altgreen_300 }]}>
                      Time Period <Text style={{ color: 'red' }}>*</Text>
                    </Text>

                    <DatePicker
                      style={{ width: '100%', borderWidth: 0 }}
                      date={
                        this.state.addStartTimeUnix === 0
                          ? ''
                          : this.unixTimeDatePicker(this.state.addStartTimeUnix)
                      }
                      mode="date"
                      placeholder="Start date"
                      format="DD-MM-YYYY"
                      minDate="01-01-1900"
                      maxDate={this.currentDate()}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          right: 0,
                          top: -10,
                          marginRight: 0,
                        },
                        dateInput: {
                          alignItems: 'flex-start',
                          borderWidth: 0,
                        },
                        dateText: {
                          color: COLORS.dark_700,
                          marginTop: -20,
                        },
                        // ... You can check the source to find the other keys.
                      }}
                      onDateChange={(date) => {
                        let tempdate = date.split('-').reverse().join('-');
                        let convertedUnixTime = new Date(tempdate).getTime();
                        this.setState({ addStartTimeUnix: convertedUnixTime, addEndTimeUnix:
                          convertedUnixTime >
                            new Date(this.state.addEndTimeUnix).getTime()
                            ? 0
                            : this.state.addEndTimeUnix });
                      }}
                    />
                  </View>
                  {/* <Icon name="Calender_OL" color='#698F8A' size={16} style={{ marginTop: Platform.OS === 'android' ? 12 : 0, zIndex: 2 }} /> */}
                </TouchableOpacity>

                {this.state.addStartTimeUnix ? (
                <TouchableOpacity
                  activeOpacity={0.6}
                  style={{
                    height: 56,
                    width: '40%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 20,
                    paddingLeft: 6,
                    paddingRight: 12,
                  }}>
                  <View style={{ width: '100%' }}>
                    <Text
                      style={[typography.Note2, { color: COLORS.altgreen_300 }]}>
                      End Date
                    </Text>

                    <DatePicker
                      style={{ width: '100%', borderWidth: 0 }}
                      date={
                        this.state.addEndTimeUnix === 0
                          ? ''
                          : this.unixTimeDatePicker(this.state.addEndTimeUnix)
                      }
                      mode="date"
                      placeholder="End date"
                      format="DD-MM-YYYY"
                      minDate={this.getMinEndDate(this.state.addStartTimeUnix)}
                      maxDate={this.currentDate()}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          right: 0,
                          top: -10,
                          marginRight: 0,
                        },
                        dateInput: {
                          alignItems: 'flex-start',
                          borderWidth: 0,
                        },
                        dateText: {
                          color: COLORS.dark_700,
                          marginTop: -20,
                        },
                        // ... You can check the source to find the other keys.
                      }}
                      onDateChange={(date) => {
                        let tempdate = date.split('-').reverse().join('-');
                        let convertedUnixTime = new Date(tempdate).getTime();
                        this.setState({ addEndTimeUnix: convertedUnixTime });
                      }}
                    />
                  </View>
                  {/* <Icon name="Calender_OL" color='#698F8A' size={16} style={{ marginTop: Platform.OS === 'android' ? 12 : 0, zIndex: 2 }} /> */}
                </TouchableOpacity>) : null}
              </View>
              <View style={styles.border2}></View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 10,
                  width: '90%',
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  label={<Text>Field of Study <Text style={{ color: 'red' }}>*</Text></Text>}
                  // mode='outlined'
                  selectionColor="#C8DB6E"
                  style={[
                    typography.Subtitle_1,
                    {
                      width: '98%',
                      height: 56,
                      backgroundColor: COLORS.bgfill,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) =>
                    this.setState({ addFieldOfStudy: value })
                  }
                  value={this.state.addFieldOfStudy}
                />
              </View>
              <View style={styles.border2}></View>

              <TouchableOpacity
                onPress={this.addNewEducation}
                activeOpacity={0.7}
                style={[styles.updateButton, { marginTop: 30 }]}>
                <Text
                  style={[typography.Caption, { color: COLORS.altgreen_100 }]}>
                  Add
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        {/***** Header starts *****/}

        <ProfileEditHeader
          name="Education"
          iconName="Certificate"
          goback={this.goback}
          addButton={true}
          addButtonFunction={this.addButtonFunction}
        />

        {/***** Header ends *****/}

        {this.editEducationModal()}
        {this.addEducationModal()}
        {this.props.userEducation.body && this.props.userEducation.body.list.length ?
        <View style={{ }}>
        
          <FlatList
            keyExtractor={(item) => item.id}
            contentContainerStyle={{ paddingBottom: 70 }}
            data={
              this.props.userEducation.body
                ? this.props.userEducation.body.list
                : []
            }
            renderItem={({ item }) => this.renderItem(item)}
          />

        </View>
        :
        <Image
            source={EducationDefault}
            style={{ maxHeight: '100%', maxWidth: '100%', alignSelf: 'center', marginTop: '40%' }}
          />}
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalHeaderView: {
    zIndex: 2,
    width: '100%',
    height: 58,
    backgroundColor: COLORS.altgreen_100,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  updateButton: {
    width: 86,
    height: 28,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: COLORS.dark_600,
  },
  educationList: {
    width: '85%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    paddingVertical: 10,
    marginVertical: 5,
  },
  eduTitle: {
    color: '#00394D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 12,
  },
  eduSubTitle: {
    color: '#698F8A',
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    maxWidth: 180,
  },
  eduDate: {
    color: '#90949C',
    fontStyle: 'italic',
    fontSize: 10,
  },
});

const mapStateToProps = (state) => {
  return {
    userEducationProgress: state.personalProfileReducer.userEducationProgress,
    userEducation: state.personalProfileReducer.userEducation,
    errorEducation: state.personalProfileReducer.errorEducation,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalEducationRequest: (data) =>
      dispatch(personalEducationRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EducationEdit);
