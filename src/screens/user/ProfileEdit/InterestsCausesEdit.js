import React, {Component} from 'react';
import {
  TextInput,
  Modal,
  View,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  StyleSheet,
  ScrollView,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';
import Snackbar from 'react-native-snackbar';
import LinearGradient from 'react-native-linear-gradient';

import {personalProfileRequest} from '../../../services/Redux/Actions/User/PersonalProfileActions';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultCover from '../../../../assets/defaultCover.png';
import typography from '../../../Components/Shared/Typography';
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

class InterestsCausesEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      isLoading: false,
      interests:
        this.props.user.body && this.props.user.body.interests
          ? this.props.user.body.interests
          : [],
      interestsSuggestions: [],
      selectedInterests:
        this.props.user.body && this.props.user.body.interests
          ? this.props.user.body.interests
          : [],
      interestsModalOpen: false,
      searchInterests: '',

      causesModal: false,
      personalJoinedCauses: [],
      personalNotJoinedCauses: [],
      selectedCausesIds: []
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({userId: value});
        this.props.personalProfileRequest({userId: value, otherUserId: ''});
        this.getJoinedList(value);
        this.getUnJoinedList(value);
      })
      .catch((e) => {
        console.log(e);
      });

    this.getMasterConfig();
  }

  goback = () => {
    this.props.navigation.goBack();
  };

  getMasterConfig = () => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/master/config/get',
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          let list = [];
          let interests = res.body.fields.interest.sort();
          interests.map((data, index) => {
            list.push({text: data, id: index});
          });
          this.setState({interestsSuggestions: list});
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          console.log('err in fetching interests: ', err.response.data.message);
        }
      });
  };

  handleSubmit = () => {
    this.setState({isLoading: true});

    let postBody = {
      userId: this.state.userId,
      interests: this.state.interests,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/profile/update/interest',
      headers: {'Content-Type': 'application/json'},
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '200 OK') {
          this.setState({isLoading: false});
          this.props.personalProfileRequest({
            userId: this.state.userId,
            otherUserId: '',
          });
          Snackbar.show({
            backgroundColor: COLORS.dark_900,
            text: 'Interests and Causes updated successfully',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          this.setState({isLoading: false});
          Snackbar.show({
            backgroundColor: COLORS.alert_red,
            text: err.response.data.message,
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
  };

  removeInterests = (index) => {
    let tempInterests = cloneDeep(this.state.interests);
    tempInterests.splice(index, 1);
    this.setState({interests: tempInterests, selectedInterests: tempInterests});
  };

  searchInterests = (query) => {
    if (query === '') {
      return this.state.interestsSuggestions;
    }

    const regex = new RegExp(`${query.trim()}`, 'i');

    return this.state.interestsSuggestions.filter(
      (item) => item.text.search(regex) >= 0,
    );
  };

  getUnJoinedList = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/cause/unjoined/list?userId=' +
        userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          let list = response.data.body.list;
          this.setState({personalNotJoinedCauses: list});
          console.log('causes unjoined res: ', list);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getJoinedList = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/cause/joined/list?userId=' +
        userId +
        '&page=' +
        0 +
        '&size=' +
        100,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({personalJoinedCauses: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  joinCause = () => {
    this.setState({isLoading: true});

    let postBody = {
      userId: this.state.userId,
      causeId: this.state.selectedCausesIds,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/public/cause/join',
      headers: {'Content-Type': 'application/json'},
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '201 CREATED') {
          this.setState({
            selectedCausesIds: [],
            causesModal: false,
            isLoading: false,
          });
          this.getJoinedList(this.state.userId);
          this.getUnJoinedList(this.state.userId);
        }
      })
      .catch((err) => {
        this.setState({causesModal: false, isLoading: false});
      });
  };

  deleteCause(id) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/cause/delete?id=' +
        id +
        '&userId=' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.getJoinedList(this.state.userId);
          this.getUnJoinedList(this.state.userId);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  addCause = (id, index) => {
    let addList = cloneDeep(this.state.selectedCausesIds);
    addList.push(id);
    this.setState({
      selectedCausesIds: addList,
      personalNotJoinedCauses: [
        ...this.state.personalNotJoinedCauses,
        (this.state.personalNotJoinedCauses[index].joined = true),
      ],
    });
  };

  removeCause = (id, index) => {
    let removeList = cloneDeep(this.state.selectedCausesIds);
    let removeIndex = removeList.indexOf(id);
    removeList.splice(removeIndex, 1);
    this.setState({
      selectedCausesIds: removeList,
      personalNotJoinedCauses: [
        ...this.state.personalNotJoinedCauses,
        (this.state.personalNotJoinedCauses[index].joined = false),
      ],
    });
  };

  renderInterestsItem = (item) => {
    return (
      <View style={styles.interestsItemView}>
        <Text style={[typography.Caption, {color: COLORS.dark_600}]}>
          {item.item}
        </Text>
        <TouchableOpacity
          onPress={() => this.removeInterests(item.index)}
          style={{marginLeft: 5}}>
          <Icon
            name="Cross"
            size={10}
            color={COLORS.altgreen_300}
            style={{marginTop: Platform.OS === 'ios' ? 5 : 0}}
          />
        </TouchableOpacity>
      </View>
    )
  }

  renderCausesItem = (item) => {
    return (
      <View
        style={{
          width: 162,
          height: 90,
          borderRadius: 12,
          margin: 6,
          alignSelf: 'center',
        }}>
        <Image
          source={{uri: item.item.imageUrl}}
          style={{width: '100%', height: '100%', borderRadius: 12}}
        />
        <View style={styles.linearGradientView}>
          <LinearGradient
            colors={['#154A59', '#154A59CC', '#154A5900']}
            style={styles.linearGradient}>
            <Text
              numberOfLines={1}
              style={[
                typography.Caption,
                {color: COLORS.white, marginTop: 5, maxWidth: 100},
              ]}>
              {item.item.name}
            </Text>
            <TouchableOpacity
              onPress={() => this.deleteCause(item.item.id)}
              style={{
                width: 14,
                height: 14,
                borderRadius: 7,
                backgroundColor: COLORS.altgreen_200,
                marginTop: 5,
                alignItems: 'center',
              }}>
              <Icon
                name="Cross"
                size={7}
                color={COLORS.dark_800}
                style={{marginTop: Platform.OS === 'ios' ? 3 : 0}}
              />
            </TouchableOpacity>
          </LinearGradient>
        </View>
      </View>
    );
  };

  renderUnjoinedCausesItem = (item) => {
    return (
      <View
        style={{
          width: '90%',
          height: 75,
          borderRadius: 12,
          margin: 6,
          alignSelf: 'center',
        }}>
        <Image
          source={{uri: item.item.imageUrl}}
          style={{width: '100%', height: '100%', borderRadius: 12}}
        />
        <View style={[styles.linearGradientView, {height: '100%'}]}>
          <LinearGradient
            colors={['#154A59', '#154A5999', '#154A5966']}
            style={[
              styles.linearGradient,
              {borderRadius: 12, paddingHorizontal: 15},
            ]}>
            <Text
              numberOfLines={1}
              style={[
                typography.Title_1,
                {color: COLORS.white, alignSelf: 'center'},
              ]}>
              {item.item.name}
            </Text>
            {!item.item.joined ? (
              <TouchableOpacity
                onPress={() => this.addCause(item.item.id, item.index)}
                style={[
                  defaultShape.ActionBtn_Gylph,
                  {alignSelf: 'center', alignItems: 'center'},
                ]}>
                <Icon
                  name="Causes_Plus"
                  size={16}
                  color={COLORS.dark_800}
                  style={{marginTop: Platform.OS === 'ios' ? 5 : 0}}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => this.removeCause(item.item.id, item.index)}
                style={[
                  defaultShape.Nav_Gylph_Btn,
                  {alignSelf: 'center', alignItems: 'center'},
                ]}>
                <Icon name="Causes_Tick" size={16} color={COLORS.green_400} />
              </TouchableOpacity>
            )}
          </LinearGradient>
        </View>
      </View>
    );
  };

  interestsModal = () => {
    return (
      <Modal
        visible={this.state.interestsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={{
              width: '100%',
              height: 700,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
              }}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({interestsModalOpen: false, selectedInterests: this.state.originalInterests})}>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 450, backgroundColor: COLORS.bgfill},
            ]}>
            <View
              style={{
                width: '100%',
                height: 58,
                backgroundColor: COLORS.altgreen_100,
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                top: 0,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}>
              <Text
                style={[
                  typography.Button_2,
                  {color: COLORS.dark_900, textAlign: 'center'},
                ]}>
                ADD INTERESTS
              </Text>
            </View>

            <View
              style={{
                height: 40,
                backgroundColor: '#F7F7F5',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '12%',
              }}>
              <TextInput
                style={styles.textInput}
                placeholderTextColor="#D9E1E4"
                onChangeText={(value) => {
                  this.setState({searchInterests: value});
                }}
                color="#154A59"
                placeholder="Search"
                onFocus={() => this.setState({searchIcon: false})}
                onBlur={() => this.setState({searchIcon: true})}
                underlineColorAndroid="transparent"
                ref={(input) => {
                  this.textInput = input;
                }}
              />
            </View>

            <FlatList
              keyboardShouldPersistTaps="handled"
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{paddingBottom: 10, paddingTop: 6}}
              style={{height: '50%'}}
              keyExtractor={(item) => item.id}
              data={this.searchInterests(this.state.searchInterests)}
              initialNumToRender={10}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => {
                    this.state.selectedInterests.includes(item.text)
                      ? this.setState({
                          selectedInterests: this.state.selectedInterests.filter(
                            (value) => value !== item.text,
                          ),
                        })
                      : this.setState({
                          selectedInterests: [
                            ...this.state.selectedInterests,
                            item.text,
                          ],
                        });
                  }}
                  style={styles.item}
                  activeOpacity={0.7}>
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, textAlign: 'left'},
                    ]}>
                    {item.text}
                  </Text>
                  <View
                    style={{
                      width: 17,
                      height: 17,
                      borderRadius: 8.5,
                      borderColor: COLORS.green_500,
                      borderWidth: 2,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: this.state.selectedInterests.includes(
                        item.text,
                      )
                        ? COLORS.green_500
                        : COLORS.altgreen_t50,
                    }}>
                    {this.state.selectedInterests.includes(item.text) ? (
                      <Icon
                        name="Tick"
                        size={9}
                        color={COLORS.dark_800}
                        style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
                      />
                    ) : (
                      <></>
                    )}
                  </View>
                </TouchableOpacity>
              )}
            />

            <TouchableOpacity
              onPress={() => {
                this.setState({
                  interestsModalOpen: false,
                  interests: this.state.selectedInterests,
                });
              }}
              activeOpacity={0.7}
              style={[
                {
                  width: 66,
                  height: 28,
                  borderRadius: 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginVertical: 10,
                  backgroundColor: COLORS.dark_600,
                },
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_100}]}>
                Add
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  addCausesModal = () => {
    return (
      <Modal
        visible={this.state.causesModal}
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <SafeAreaView
          style={{
            height: '100%',
            width: '100%',
            backgroundColor: COLORS.bgfill,
          }}>
          <View style={styles.header}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => this.setState({causesModal: false})}
                style={defaultShape.Nav_Gylph_Btn}>
                <Icon
                  name="Arrow-Left"
                  color={COLORS.altgreen_300}
                  size={16}
                  style={{marginTop: Platform.OS === 'android' ? 8 : 0}}
                />
              </TouchableOpacity>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{marginLeft: 10}}>
                  <Text
                    numberOfLines={1}
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, maxWidth: 160},
                    ]}>
                    Add Causes
                  </Text>
                </View>
              </View>
            </View>

            <TouchableOpacity
              onPress={this.joinCause}
              activeOpacity={0.6}
              style={[
                defaultShape.ContextBtn_FL_Drk,
                {marginRight: 10, paddingHorizontal: 16},
              ]}>
              {this.state.isLoading ? (
                <ActivityIndicator size="small" color={COLORS.white} />
              ) : (
                <Text
                  style={[typography.Caption, {color: COLORS.altgreen_300}]}>
                  Done
                </Text>
              )}
            </TouchableOpacity>
          </View>

          <View style={{marginBottom: 80}}>
            <FlatList
              style={{paddingVertical: 20}}
              data={this.state.personalNotJoinedCauses}
              keyExtractor={(item) => item.id}
              renderItem={(item) => this.renderUnjoinedCausesItem(item)}
            />
          </View>
        </SafeAreaView>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {/***** Header starts *****/}

        <ProfileEditHeader
          name="Interests & Causes"
          iconName="Causes_OL"
          goback={this.goback}
          updateButton={true}
          updateButtonFunction={this.handleSubmit}
        />

        {/***** Header ends *****/}

        {this.interestsModal()}
        {this.addCausesModal()}

        <ScrollView>
          {/***** Interests starts *****/}

          <View style={styles.interestsView}>
            <Icon
              name="Select_Multiple"
              size={20}
              color={COLORS.altgreen_400}
            />
            <Text
              style={[
                typography.Caption,
                {
                  color: COLORS.altgreen_300,
                  marginTop: Platform.OS === 'ios' ? 5 : -10,
                },
              ]}>
              INTERESTS
            </Text>
          </View>

          <View>
            <FlatList
              contentContainerStyle={{
                flexWrap: 'wrap',
                flexDirection: 'row',
                paddingHorizontal: 8,
                marginTop: 10,
                paddingBottom: 10,
              }}
              data={this.state.interests}
              keyExtractor={(item) => item}
              ListFooterComponent={
                <TouchableOpacity
                  onPress={() => this.setState({interestsModalOpen: true, originalInterests: this.state.selectedInterests})}
                  style={styles.addInterests}>
                  <Text style={[typography.Caption, {color: COLORS.dark_600}]}>
                    + Add Interests
                  </Text>
                </TouchableOpacity>
              }
              renderItem={(item) => this.renderInterestsItem(item)}
            />
          </View>

          {/***** Interests ends *****/}

          {/***** Causes starts *****/}

          <View style={styles.interestsView}>
            <Icon name="Causes_OL" size={20} color={COLORS.altgreen_400} />
            <Text
              style={[
                typography.Caption,
                {
                  color: COLORS.altgreen_300,
                  marginTop: Platform.OS === 'ios' ? 5 : -10,
                },
              ]}>
              CAUSES
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => this.setState({causesModal: true})}
            style={[
              defaultShape.AdjunctBtn_Label_Blurred,
              {width: 120, marginTop: 20, alignSelf: 'center'},
            ]}>
            <Text style={[typography.Caption, {color: COLORS.dark_500}]}>
              + Add Causes
            </Text>
          </TouchableOpacity>

          <View style={{alignItems: 'center', marginTop: 6}}>
            <FlatList
              contentContainerStyle={{
                width: '100%',
                flexWrap: 'wrap',
                flexDirection: 'row',
                paddingHorizontal: 8,
                marginTop: 10,
                paddingBottom: 10,
              }}
              data={this.state.personalJoinedCauses}
              keyExtractor={(item) => item.name}
              renderItem={(item) => this.renderCausesItem(item)}
            />
          </View>

          {/***** Causes ends *****/}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: 52,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: COLORS.white,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  interestsView: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    flexDirection: 'row',
    width: '70%',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'flex-start',
    marginLeft: '6%',
    paddingHorizontal: '3%',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.grey_200,
  },
  interestsItemView: {
    flexDirection: 'row',
    paddingHorizontal: 8,
    paddingVertical: 4,
    backgroundColor: COLORS.altgreen_t50,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: COLORS.altgreen_300,
    marginLeft: 8,
    marginTop: 12,
  },
  linearGradientView: {
    width: '100%',
    height: 45,
    position: 'absolute',
    top: 0,
  },
  linearGradient: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },
  addInterests: {
    padding: 7,
    backgroundColor: COLORS.grey_200,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: COLORS.grey_300,
    marginLeft: 8,
    marginTop: 12,
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '90%',
    zIndex: 1,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
});

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InterestsCausesEdit);
