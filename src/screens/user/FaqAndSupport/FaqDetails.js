import React, { Component } from 'react'
import { Share, Linking, TextInput, FlatList, ScrollView, Modal, TouchableOpacity, Platform, Text, View, SafeAreaView, StyleSheet } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'
import axios from 'axios'
import HTMLView from 'react-native-htmlview'
import HTML from 'react-native-render-html'
import Clipboard from '@react-native-community/clipboard'
import Snackbar from 'react-native-snackbar'
import RNUrlPreview from 'react-native-url-preview'

import { REACT_APP_userServiceURL, REACT_APP_domainUrl } from '../../../../env.json'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import typography from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class HiddenContent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            contactInfoModalOpen: false,
            searchParam: '',
            faqData: [],
            answerModalOpen: false,
            question: '',
            answer: '',
            searchText: '',
            currentData: {},
            shareModalOpen: false
        }
    }

    componentDidMount() {
        this.getFaqList()
    }

    getFaqList() {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/backend/public/faq/get?&section=' + this.props.route.params.section + "&searchParam=" + this.state.searchParam,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then((response) => {
            let res = response.data
            if (res.message === 'Success!') {
                this.setState({ faqData: res.body })
            }
        })

    }

    contactInfoModal = () => {
        return (

            <Modal visible={this.state.contactInfoModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>

                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ contactInfoModalOpen: false })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
                    </TouchableOpacity>

                    <View style={defaultShape.Modal_Categories_Container}>

                        <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0, borderBottomWidth: 0 }]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>CONTACT SUPPORT</Text>
                        </View>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15, borderBottomWidth: 0, flexDirection: 'column', alignItems: 'flex-start' }] : [defaultShape.ActList_Cell_Gylph_Alt, { flexDirection: 'column', alignItems: 'flex-start', borderBottomWidth: 0, paddingVertical: 12 }]} activeOpacity={0.6} onPress={() => { this.setState({ contactInfoModalOpen: false }, () => Linking.openURL('tel:9920605555')) }}>
                            <Text style={[typography.Button_2, { color: COLORS.dark_600 }]}>Phone Number :</Text>
                            <Text style={[typography.Caption, { color: COLORS.dark_600, fontSize: 12.5 }]}>+91 9920605555</Text>

                        </TouchableOpacity>

                        <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15, borderBottomWidth: 0, flexDirection: 'column', alignItems: 'flex-start' }] : [defaultShape.ActList_Cell_Gylph_Alt, { flexDirection: 'column', alignItems: 'flex-start', borderBottomWidth: 0, paddingVertical: 12 }]} activeOpacity={0.6} onPress={() => { this.setState({ contactInfoModalOpen: false }, () => Linking.openURL('mailto:support@wenaturalists.com')) }}>
                            <Text style={[typography.Button_2, { color: COLORS.dark_600 }]}>Email :</Text>
                            <Text style={[typography.Caption, { color: COLORS.dark_600, fontSize: 12.5 }]}>support@wenaturalists.com</Text>

                        </TouchableOpacity>

                    </View>

                </View>

            </Modal>

        )
    }

    onShare = async () => {
        try {
            const result = await Share.share({
                message: REACT_APP_domainUrl + '/faq-details/' + this.state.currentData.id,
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    console.log('shared with activity type of ', result.activityType);
                } else {
                    console.log('shared');
                }
            } else if (result.action === Share.dismissedAction) {
                console.log('dismissed');
            }
        } catch (error) {
            console.log(error.message);
        }
    }

    shareModal = () => {
        return (
            <Modal
                visible={this.state.shareModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient
                            colors={[
                                COLORS.dark_800 + '00',
                                COLORS.dark_800 + 'CC',
                                COLORS.dark_800,
                            ]}
                            style={defaultShape.Linear_Gradient}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ shareModalOpen: false })}>
                        <Icon
                            name="Cross"
                            size={13}
                            color={COLORS.dark_600}
                            style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
                        />
                    </TouchableOpacity>

                    <View style={defaultShape.Modal_Categories_Container}>
                        <View
                            style={[
                                defaultShape.ActList_Cell_Gylph_Alt,
                                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
                            ]}>
                            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                                Share
                            </Text>
                        </View>

                        <TouchableOpacity
                            onPress={() => {
                                Clipboard.setString(
                                    REACT_APP_domainUrl + '/faq-details/' + this.state.currentData.id,
                                );
                                this.setState({ shareModalOpen: false })
                                setTimeout(() => {
                                    Snackbar.show({
                                        backgroundColor: '#97A600',
                                        text: 'Link Copied',
                                        textColor: '#00394D',
                                        duration: Snackbar.LENGTH_LONG,
                                    })
                                }, 500)
                            }}
                            style={
                                Platform.OS === 'ios'
                                    ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                    : [defaultShape.ActList_Cell_Gylph_Alt]
                            }
                            activeOpacity={0.6}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                                Copy link
                            </Text>
                            <Icon
                                name="TxEdi_AddLink"
                                size={17}
                                color={COLORS.altgreen_300}
                                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ shareModalOpen: false }, () => this.onShare());
                            }}
                            style={
                                Platform.OS === 'ios'
                                    ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                                    : [
                                        defaultShape.ActList_Cell_Gylph_Alt,
                                        { borderBottomWidth: 0 },
                                    ]
                            }
                            activeOpacity={0.6}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                                Share via others
                            </Text>

                            <View
                                style={[
                                    {
                                        flexDirection: 'row',
                                        justifyContent: 'space-evenly',
                                        width: 100,
                                        marginRight: -6,
                                    },
                                ]}>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon
                                        name="Social_FB"
                                        size={14}
                                        color={COLORS.dark_600}
                                        style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                                    />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon
                                        name="Social_Twitter"
                                        size={14}
                                        color={COLORS.dark_600}
                                        style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                                    />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon
                                        name="Social_LinkedIn"
                                        size={14}
                                        color={COLORS.dark_600}
                                        style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                                    />
                                </View>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon
                                        name="Meatballs"
                                        size={14}
                                        color={COLORS.dark_600}
                                        style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                                    />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }

    answerModal = () => {

        return (

            <Modal onRequestClose={() => this.setState({ answerModalOpen: false })} visible={this.state.answerModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
                <SafeAreaView style={{ marginTop: 'auto' }}>

                    <View style={[defaultShape.Linear_Gradient_View, { height: 660, bottom: 100 }]}>
                        <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
                        </LinearGradient>
                    </View>

                    <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
                        onPress={() => this.setState({ answerModalOpen: false })} >
                        <Icon name='Cross' size={13} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
                    </TouchableOpacity>

                    <View style={[defaultShape.Modal_Categories_Container, { height: 'auto', maxHeight: 600, minHeight: 130 }]}>

                        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 5, borderBottomWidth: 0, flexDirection: 'column', alignItems: 'flex-start' }] : [defaultShape.ActList_Cell_Gylph_Alt, { flexDirection: 'column', alignItems: 'flex-start', borderBottomWidth: 0, paddingTop: 5, paddingBottom: 5 }]}>
                            <Text style={[typography.Button_2, { color: COLORS.dark_600, marginBottom: 10 }]}>{this.state.question.charAt(0).toUpperCase() + this.state.question.slice(1)}</Text>

                            {this.state.currentData.canShare ?
                                <TouchableOpacity
                                    onPress={() =>
                                        this.setState({ shareModalOpen: true })
                                    }
                                    activeOpacity={0.5}
                                    style={{
                                        alignItems: 'center',
                                        flexDirection: 'row',
                                        marginLeft: 12,
                                    }}>
                                    <Icon
                                        name="Share"
                                        color={COLORS.green_500}
                                        size={14}
                                        style={{
                                            marginTop: Platform.OS === 'android' ? 10 : 0,
                                            marginRight: 6,
                                        }}
                                    />
                                    <Text style={[{ color: COLORS.altgreen_300 }, typography.Caption]}>
                                        Share
                                    </Text>
                                </TouchableOpacity> : null}

                            <HTMLView
                                value={this.state.answer}
                                stylesheet={styles2}
                            />

                            {/* <HTML
                                html={this.state.answer}
                                tagsStyles={tagsStyles}
                                // classesStyles={classesStyles}
                                // imagesMaxWidth={Dimensions.get('window').width * .9}
                                // staticContentMaxWidth={Dimensions.get('window').width * .9}
                            /> */}

                            {this.state.currentData.link ?

                                <RNUrlPreview
                                    text={this.state.currentData.link}
                                    titleNumberOfLines={1}
                                    titleStyle={[typography.Title_2, { color: COLORS.dark_800 }]}
                                    descriptionStyle={[
                                        typography.Note,
                                        {
                                            fontFamily: 'Montserrat-Medium',
                                            color: COLORS.altgreen_300,
                                            marginTop: 8,
                                            // width: '70%'
                                        },
                                    ]}
                                    containerStyle={{
                                        marginTop: 15,
                                        width: '100%',
                                        flexDirection: 'column',
                                        backgroundColor: COLORS.altgreen_100,
                                    }}
                                    imageStyle={{ width: '100%', height: 20 }}
                                    imageProps={{ width: '100%', height: 20 }}
                                /> : null}

                        </ScrollView>

                    </View>

                </SafeAreaView>

            </Modal>

        )
    }

    renderFaqs = (item, index) => {
        return (
            <View style={{ borderBottomColor: '#eee', borderBottomWidth: 0.6, backgroundColor: '#FFF', width: '94%', alignSelf: 'center', borderTopLeftRadius: index === 0 ? 8 : 0, borderTopRightRadius: index === 0 ? 8 : 0, borderBottomLeftRadius: index === this.state.faqData.length - 1 ? 8 : 0, borderBottomRightRadius: index === this.state.faqData.length - 1 ? 8 : 0 }}>
                <TouchableOpacity activeOpacity={0.7}
                    onPress={() => this.setState({ answerModalOpen: true, question: item.question, answer: item.answer, currentData: item })}
                    style={{ width: '94%', justifyContent: 'space-between', alignSelf: 'center', flexDirection: 'row', alignItems: 'center', paddingLeft: 16, paddingRight: 20, paddingVertical: 10 }}>

                    <Text style={[typography.Caption, { color: COLORS.dark_800, fontSize: 13, marginLeft: 8, width: '86%' }]}>{item.question.charAt(0).toUpperCase() + item.question.slice(1)}</Text>
                    <Icon name="Arrow_Down" size={14} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                </TouchableOpacity>

                {item.canShare ?
                    <TouchableOpacity
                        onPress={() =>
                            this.setState({ shareModalOpen: true, currentData: item, marginTop: -30 })
                        }
                        activeOpacity={0.5}
                        style={{
                            alignItems: 'center',
                            flexDirection: 'row',
                            marginLeft: 12,
                            // width: '94%',
                            paddingLeft: 16

                        }}>
                        <Icon
                            name="Share"
                            color={COLORS.green_500}
                            size={14}
                            style={{
                                marginTop: Platform.OS === 'android' ? 10 : 0,
                                marginRight: 6,
                                marginLeft: 8
                            }}
                        />
                        <Text style={[{ color: COLORS.altgreen_300 }, typography.Caption]}>
                            Share
                        </Text>
                    </TouchableOpacity> : null}
            </View>
        )
    }

    searchQuestions = (query) => {
        if (query === '') {
            return this.state.faqData
        }

        const regex = new RegExp(`${query.trim()}`, 'i')

        return this.state.faqData.filter(item => (item.question).search(regex) >= 0)
    }

    render() {
        this.state.answer ? console.log(this.state.answer) : null
        return (
            <SafeAreaView style={{ flex: 1, paddingBottom: 10 }}>
                <ScrollView keyboardShouldPersistTaps='handled' style={{}}>
                    {this.contactInfoModal()}
                    {this.answerModal()}
                    {this.shareModal()}

                    <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 20 }] : styles.header}>

                        <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack()}>
                            <Icon name="Arrow-Left" size={15} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                        </TouchableOpacity>
                        <Text style={[typography.H3, { color: COLORS.dark_800, fontSize: 22 }]}>
                            {this.props.route.params.title}
                        </Text>
                        <View style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }}>
                            <Icon name="Arrow-Left" size={15} color="#FFF" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                        </View>

                    </View>

                    <View style={{
                        height: 60, backgroundColor: '#F7F7F5', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 10
                    }}>

                        <TextInput
                            style={styles.textInput}
                            placeholderTextColor="#D9E1E4"
                            onChangeText={(value) => { this.setState({ searchText: value }) }}
                            color='#154A59'
                            placeholder='Search'
                            onFocus={() => this.setState({ searchIcon: false })}
                            onBlur={() => this.setState({ searchIcon: true })}
                            underlineColorAndroid="transparent"
                            ref={input => { this.textInput = input }}
                        />


                    </View>

                    <FlatList
                        showsVerticalScrollIndicator={false}
                        style={{ paddingTop: 10 }}
                        data={this.state.faqData}
                        data={this.searchQuestions(this.state.searchText)}
                        keyboardShouldPersistTaps='handled'
                        contentContainerStyle={{ paddingTop: 4, paddingBottom: 30 }}
                        keyExtractor={(item) => item.id}
                        renderItem={({ item, index }) => this.renderFaqs(item, index)}
                    />

                    <View
                        style={{ flexDirection: 'row', width: '90%', alignSelf: 'center', backgroundColor: '#e7f3e3', marginTop: 0, borderRadius: 8, paddingVertical: 16, paddingLeft: 26, paddingRight: 26 }}>
                        <Icon name="Call_Service" size={40} color={COLORS.green_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
                        <View style={{ marginLeft: 20 }}>
                            <Text style={[typography.H3, { color: 'rgba(0,57,77,.95)', fontSize: 19 }]}>
                                Contact Support
                            </Text>

                            <Text style={[typography.Subtitle_2, { color: '#91b3a2', marginTop: 6 }]}>
                                Do you have more questions? {'\n'}Feel free to contact <Text onPress={() => this.setState({ contactInfoModalOpen: true })} style={[typography.Body_1_bold, { textDecorationLine: 'underline', color: '#154a59' }]}>Support.</Text></Text>
                        </View>
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 15,
        paddingVertical: 20,
        backgroundColor: '#FFF',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    textInput: {
        fontSize: 17,
        fontFamily: 'Montserrat-Medium',
        padding: 10,
        backgroundColor: '#FFF',
        width: '90%',
        zIndex: 1,
        textAlign: 'center',
        borderRadius: 8,
        borderColor: '#D9E1E4',
        borderWidth: 1,
        shadowColor: '#36768140',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
})

const tagsStyles = {
    p: {
        color: COLORS.dark_600, fontSize: 12.5
    },
    span: {
        color: COLORS.dark_600, fontSize: 12.5
    },
    div: {
        color: COLORS.dark_600, fontSize: 12.5
    },
    img: {
        width: 10, height: 10
    },
    h1: {
        color: '#6728C7',
        textAlign: 'center',
        marginBottom: 10
    },
    // img: {
    //   marginLeft: 'auto',
    //   marginRight: 'auto',
    //   marginTop: 20
    // }
}

const styles2 = StyleSheet.create({
    p: {
        color: COLORS.dark_600, fontSize: 12.5
    },
    span: {
        color: COLORS.dark_600, fontSize: 12.5
    },
    div: {
        color: COLORS.dark_600, fontSize: 12.5
    },
    img: {
        width: '100%', height: '100%'
    }
})
