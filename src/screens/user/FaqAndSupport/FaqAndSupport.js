import React, { Component } from 'react'
import { Linking, ScrollView, Modal, TouchableOpacity, Platform, Text, View, SafeAreaView, StyleSheet } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'

import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import typography from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class HiddenContent extends Component {

  constructor(props) {
    super(props)
    this.state = {
      contactInfoModalOpen: false
    }
  }

  contactInfoModal = () => {
    return (

      <Modal visible={this.state.contactInfoModalOpen} transparent animationType="slide" supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>

          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient colors={[COLORS.dark_800 + '00', COLORS.dark_800 + 'CC', COLORS.dark_800]} style={defaultShape.Linear_Gradient}>
            </LinearGradient>
          </View>

          <TouchableOpacity activeOpacity={0.7} style={defaultShape.CloseBtn}
            onPress={() => this.setState({ contactInfoModalOpen: false })} >
            <Icon name='Cross' size={13} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 6 : 0 }} />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>

            <View style={[defaultShape.ActList_Cell_Gylph_Alt, { justifyContent: 'center', paddingBottom: 10, paddingTop: 0, borderBottomWidth: 0 }]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>CONTACT SUPPORT</Text>
            </View>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15, borderBottomWidth: 0, flexDirection: 'column', alignItems: 'flex-start' }] : [defaultShape.ActList_Cell_Gylph_Alt, { flexDirection: 'column', alignItems: 'flex-start', borderBottomWidth: 0, paddingVertical: 12 }]} activeOpacity={0.6} onPress={() => { this.setState({ contactInfoModalOpen: false }, () => Linking.openURL('tel:9920605555')) }}>
              <Text style={[typography.Button_2, { color: COLORS.dark_600 }]}>Phone Number :</Text>
              <Text style={[typography.Caption, { color: COLORS.dark_600, fontSize: 12.5 }]}>+91 9920605555</Text>

            </TouchableOpacity>

            <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15, borderBottomWidth: 0, flexDirection: 'column', alignItems: 'flex-start' }] : [defaultShape.ActList_Cell_Gylph_Alt, { flexDirection: 'column', alignItems: 'flex-start', borderBottomWidth: 0, paddingVertical: 12 }]} activeOpacity={0.6} onPress={() => { this.setState({ contactInfoModalOpen: false }, () => Linking.openURL('mailto:support@wenaturalists.com')) }}>
              <Text style={[typography.Button_2, { color: COLORS.dark_600 }]}>Email :</Text>
              <Text style={[typography.Caption, { color: COLORS.dark_600, fontSize: 12.5 }]}>support@wenaturalists.com</Text>

            </TouchableOpacity>

          </View>

        </View>

      </Modal>

    )
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, paddingBottom: 10 }}>
        <ScrollView style={{}}>
          {this.contactInfoModal()}

          <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 20 }] : styles.header}>

            <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack()}>
              <Icon name="Arrow-Left" size={15} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
            </TouchableOpacity>
            <Text style={[typography.H3, { color: COLORS.dark_800, fontSize: 22 }]}>
              FAQ & Support
            </Text>
            <View style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="Arrow-Left" size={15} color="#FFF" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
            </View>

          </View>

          <View
            style={{ width: '90%', alignSelf: 'center', justifyContent: 'space-evenly', marginTop: 20, borderRadius: 8, paddingVertical: Platform.OS === 'ios' ? 10 : 1 }}>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('FaqDetails', { title: 'Profile', section: 'PROFILE' })}
              style={{ flexDirection: 'row', backgroundColor: '#FFF', alignItems: 'center', paddingLeft: 26, borderTopLeftRadius: 8, borderTopRightRadius: 8, paddingVertical: Platform.OS === 'ios' ? 10 : 1, borderBottomColor: '#eee', borderBottomWidth: 0.6 }}>
              <Icon name="SingleUser_OL" size={21} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
              <Text style={[typography.Body_1, { color: COLORS.dark_800, fontSize: 17, marginLeft: 8 }]}>Profile</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('FaqDetails', { title: 'Feeds', section: 'POSTING' })}
              style={{ flexDirection: 'row', backgroundColor: '#FFF', alignItems: 'center', paddingLeft: 26, paddingVertical: Platform.OS === 'ios' ? 10 : 1, borderBottomColor: '#eee', borderBottomWidth: 0.6 }}>
              <Icon name="WN_Feeds_OL" size={21} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
              <Text style={[typography.Body_1, { color: COLORS.dark_800, fontSize: 17, marginLeft: 8 }]}>Feeds</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('FaqDetails', { title: 'Network', section: 'COMPANY' })}
              style={{ flexDirection: 'row', backgroundColor: '#FFF', alignItems: 'center', paddingLeft: 26, paddingVertical: Platform.OS === 'ios' ? 10 : 1, borderBottomColor: '#eee', borderBottomWidth: 0.6 }}>
              <Icon name="WN_Network_OL" size={21} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
              <Text style={[typography.Body_1, { color: COLORS.dark_800, fontSize: 17, marginLeft: 8 }]}>Network</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('FaqDetails', { title: 'Messages', section: 'CHATS' })}
              style={{ flexDirection: 'row', backgroundColor: '#FFF', alignItems: 'center', paddingLeft: 26, paddingVertical: Platform.OS === 'ios' ? 10 : 1, borderBottomColor: '#eee', borderBottomWidth: 0.6 }}>
              <Icon name="WN_Messeges_OL" size={21} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
              <Text style={[typography.Body_1, { color: COLORS.dark_800, fontSize: 17, marginLeft: 8 }]}>Messages</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('FaqDetails', { title: 'Explore', section: 'EXPLORE' })}
              style={{ flexDirection: 'row', backgroundColor: '#FFF', alignItems: 'center', paddingLeft: 26, paddingVertical: Platform.OS === 'ios' ? 10 : 1, borderBottomColor: '#eee', borderBottomWidth: 0.6 }}>
              <Icon name="WN_Explore_OL" size={21} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
              <Text style={[typography.Body_1, { color: COLORS.dark_800, fontSize: 17, marginLeft: 8 }]}>Explore</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('FaqDetails', { title: 'Projects', section: 'PROJECT' })}
              style={{ flexDirection: 'row', backgroundColor: '#FFF', alignItems: 'center', paddingLeft: 26, paddingVertical: Platform.OS === 'ios' ? 10 : 1, borderBottomColor: '#eee', borderBottomWidth: 0.6 }}>
              <Icon name="WN_Projects_OL" size={21} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
              <Text style={[typography.Body_1, { color: COLORS.dark_800, fontSize: 17, marginLeft: 8 }]}>Projects</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('FaqDetails', { title: 'Causes', section: 'SHARING' })}
              style={{ flexDirection: 'row', backgroundColor: '#FFF', alignItems: 'center', paddingLeft: 26, paddingVertical: Platform.OS === 'ios' ? 10 : 1, borderBottomColor: '#eee', borderBottomWidth: 0.6 }}>
              <Icon name="Causes_OL" size={21} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
              <Text style={[typography.Body_1, { color: COLORS.dark_800, fontSize: 17, marginLeft: 8 }]}>Causes</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('FaqDetails', { title: 'Forum And Poll', section: 'FORUM' })}
              style={{ flexDirection: 'row', backgroundColor: '#FFF', alignItems: 'center', paddingLeft: 26, paddingVertical: Platform.OS === 'ios' ? 10 : 1, borderBottomColor: '#eee', borderBottomWidth: 0.6 }}>
              <Icon name="Forum_F" size={21} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
              <Text style={[typography.Body_1, { color: COLORS.dark_800, fontSize: 17, marginLeft: 8 }]}>Forum And Poll</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('FaqDetails', { title: 'Support', section: 'CommonQuestions' })}
              style={{ flexDirection: 'row', backgroundColor: '#FFF', alignItems: 'center', paddingLeft: 26, borderBottomLeftRadius: 8, borderBottomRightRadius: 8, paddingVertical: Platform.OS === 'ios' ? 10 : 1, borderBottomColor: '#eee', borderBottomWidth: 0.6 }}>
              <Icon name="Help" size={21} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
              <Text style={[typography.Body_1, { color: COLORS.dark_800, fontSize: 17, marginLeft: 8 }]}>Support</Text>
            </TouchableOpacity>
          </View>

          <View
            style={{ flexDirection: 'row', width: '90%', alignSelf: 'center', backgroundColor: '#e7f3e3', marginTop: 16, borderRadius: 8, paddingVertical: 16, paddingLeft: 26, paddingRight: 26 }}>
            <Icon name="Call_Service" size={40} color={COLORS.green_600} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
            <View style={{ marginLeft: 20 }}>
              <Text style={[typography.H3, { color: 'rgba(0,57,77,.95)', fontSize: 19 }]}>
                Contact Support
              </Text>

              <Text style={[typography.Subtitle_2, { color: '#91b3a2', marginTop: 6 }]}>
                Do you have more questions? {'\n'}Feel free to contact <Text onPress={() => this.setState({ contactInfoModalOpen: true })} style={[typography.Body_1_bold, { textDecorationLine: 'underline', color: '#154a59' }]}>Support.</Text>
              </Text>
            </View>
          </View>

        </ScrollView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingVertical: 20,
    backgroundColor: '#FFF',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  }
})
