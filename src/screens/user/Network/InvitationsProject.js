import React, { Component } from 'react'
import { Image, StyleSheet, FlatList, TouchableOpacity, View, Text, SafeAreaView } from 'react-native'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import projectDefault from '../../../../assets/project-default.jpg'
import typography from '../../../Components/Shared/Typography'
import defaultCover from '../../../../assets/defaultProfile.png'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultStyle from '../../../Components/Shared/Typography'
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader'

const moment = require('moment')
const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class ReceivedInvitations extends Component {

  constructor(props) {
    super(props)
    this.state = {
      userId: '',
      receivedInvitations: []
    }
  }

  componentDidMount() {

    AsyncStorage.getItem("refreshToken").then((value) => {
      if (value === null) {
        this.props.navigation.replace("Login")
      }
    })

    AsyncStorage.getItem("userId").then((value) => {
      this.setState({ userId: value })
      this.getInvitations(value)

    }).catch((e) => {
      console.log(e)
    })

  }

  goback = () => { this.props.navigation.navigate('NetworkConnects') }


  getInvitations(userId) {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/invitation/list?userId=' + userId + '&type=PROJECT' + '&status=' + '' + "&page=" + 0 + "&size=" + 500,
      // url: REACT_APP_userServiceURL + '/invitation/admin/list?userId=' + userId + "&page=" + 0 + "&size=" + 500,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.body && response.status === 200) {
        // console.log('getCircleInvitations', response.data.body.content[0])
        this.setState({ receivedInvitations: response.data.body.content })
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  ignorePendingConnect = (companyId, id) => {

    let postData = {
      companyId: companyId,
      userId: this.state.userId,
      notificationId: id,
      status: 'DECLINED'
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/operator/update/status',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.status === '202 ACCEPTED') {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  acceptConnectInvitation = (companyId, id) => {

    let postData = {
      companyId: companyId,
      userId: this.state.userId,
      notificationId: id,
      status: 'APPROVED'
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/operator/update/status',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.status === '202 ACCEPTED') {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  withdrawRequest = (entityId) => {

    let postData = {
      projectId: entityId,
      otherParticipatingUserId: this.state.userId
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/participation/withdraw',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.status === '202 ACCEPTED') {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  acceptRequestByParticipant = (requestId, notificationId, otherUserId) => {

    let postData = {
      "id": requestId,
      "participationPartyType": 'INDIVIDUAL_OR_RECRUITER',
      "notificationId": notificationId,
      "creatorUserId": otherUserId
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/participants/offline/accept/request',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.status === '201 CREATED') {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  declineRequestByParticipant = (requestId, notificationId) => {

    let postData = {
      "id": requestId,
      "notificationId": notificationId
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/participation/offline/cancel/request',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.status === '201 CREATED') {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  acceptRequestByKeySpeaker = (requestId, notificationId) => {

    let postData = {
      "id": requestId,
      "participationPartyType": 'KEY_SPEAKERS',
      "notificationId": notificationId
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/invitee/accept/request',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.status === 202) {
        // console.log(response.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  declineRequestByKeySpeaker = (requestId, notificationId) => {

    let postData = {
      "id": requestId,
      "notificationId": notificationId
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/invitee/cancel/request',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.status === 202) {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  acceptTransferAdmin = (entityId, adminRightChangeQueueId, notificationId) => {

    let postData = {
      entityId: entityId,
      userId: this.state.userId,
      adminRightQueueId: adminRightChangeQueueId,
      notificationId: notificationId,
      status: 'PROCESSED',
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/deactivation/admin-right-change',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.status === '202 ACCEPTED') {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  declineTransferAdmin = (entityId, adminRightChangeQueueId, notificationId) => {

    let postData = {
      entityId: entityId,
      userId: this.state.userId,
      adminRightQueueId: adminRightChangeQueueId,
      notificationId: notificationId,
      status: 'REJECTED'
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/deactivation/admin-right-change',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.status === '202 ACCEPTED') {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  acceptTransferAdminForProject = (entityId, superAdminId, notificationId) => {

    let postData = {
      entityId: entityId,
      userId: this.state.userId,
      superAdminId: superAdminId,
      notificationId: notificationId
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/admin/accept/request',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.status === '202 ACCEPTED') {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  declineTransferAdminForProject = (entityId, superAdminId, notificationId) => {

    let postData = {
      entityId: entityId,
      userId: this.state.userId,
      superAdminId: superAdminId,
      notificationId: notificationId
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/admin/reject/request',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.status === '202 ACCEPTED') {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  acceptSuperAdminTransferForProject = (entityId, superAdminId, notificationId) => {

    let postData = {
      entityId: entityId,
      adminId: this.state.userId,
      superAdminId: superAdminId,
      notificationId: notificationId
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/admin/accept/transfer/right',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.status === '202 ACCEPTED') {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  declineSuperAdminTransferForProject = (entityId, superAdminId, notificationId) => {

    let postData = {
      entityId: entityId,
      adminId: this.state.userId,
      superAdminId: superAdminId,
      notificationId: notificationId
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/admin/decline/transfer/right',
      data: postData,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.status === '202 ACCEPTED') {
        // console.log(response.data.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  updateProject = (participationDetailId, otherParticipantUserId, entityId, stage) => {
    let postBody = {
      "id": participationDetailId,
      "approverId": this.state.userId,
      "otherParticipatingUserId": otherParticipantUserId,
      "projectId": entityId,
      "stage": stage
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/participants/update',
      headers: { 'Content-Type': 'application/json' },
      data: postBody,
      withCredentials: true
    }).then((response) => {
      let res = response.data
      if (res.status === '201 CREATED') {
        // console.log(res.status)
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })

  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>

        {/***** Header starts *****/}

        <ProfileEditHeader name="Invitations" iconName="Mail_OL" goback={this.goback} />

        {/***** Header ends *****/}

        <View style={{ alignSelf: 'center', flexDirection: 'row', backgroundColor: COLORS.altgreen_t50, width: 320, marginTop: 20, marginBottom: 10, justifyContent: 'center', borderRadius: 4 }}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('InvitationsNetwork')}
            style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 80, borderTopLeftRadius: 4, borderBottomLeftRadius: 4 }]}>
            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Network</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('InvitationsCircle')}
            style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 80 }]}>
            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Circles</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[defaultShape.InTab_Btn, { backgroundColor: '#fff', width: 80 }]}>
            <Text style={[typography.Caption, { color: COLORS.dark_800 }]}>Projects</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('InvitationsAdmin')}
            style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 80, borderTopRightRadius: 4, borderBottomRightRadius: 4 }]}>
            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Admins</Text>
          </TouchableOpacity>
        </View>

        <View style={{ width: '100%', height: 24, backgroundColor: COLORS.altgreen_200, justifyContent: 'center', alignItems: 'center', marginTop: 18 }}>
          <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400, textAlign: 'center' }]}>{this.state.receivedInvitations.length} PENDING</Text>
        </View>

        <FlatList
          keyboardShouldPersistTaps='handled'
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 26, paddingTop: 16 }}
          style={{ height: '50%' }}
          keyExtractor={(item) => item.id}
          data={this.state.receivedInvitations}
          initialNumToRender={10}
          renderItem={({ item }) => (

            <View style={{ justifyContent: 'center', marginBottom: 20 }}>

              {/* <View style={styles.item}> */}

              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.forumItemView}>



                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image
                    source={
                      item.coverImage
                        ? { uri: item.coverImage }
                        : projectDefault
                    }
                    style={styles.profileImage}
                  />
                  <View>
                    <Text
                      style={[typography.Title_2, { color: COLORS.dark_800, fontSize: 16 }]}
                      numberOfLines={1}>
                      {item.params && item.params.projectType}
                    </Text>

                    <Text numberOfLines={1}
                      style={[
                        typography.Caption,
                        { color: COLORS.dark_600, maxWidth: 240 },
                      ]}>
                      {item.title}
                    </Text>
                  </View>
                </View>

              </TouchableOpacity>


              <View style={styles.nameMsg}>
                <Text style={[defaultStyle.Note2, { color: '#698f8a', maxWidth: '90%', fontSize: 12, marginTop: 3 }]}>{item.message.trim()} <Text style={styles.name}>{item.params && item.params.projectType}</Text></Text>
                <Text style={[defaultStyle.Subtitle_2, { color: '#99b2bf', fontSize: 10, marginTop: 2 }]}>{moment.unix(item.updateTime / 1000).format('Do MMM, YYYY')}</Text>
              </View>


              {item.status === 'RECEIVED' && item.invitationType === 'ADMIN_RIGHT' ?
                <View style={{ flexDirection: 'row', alignSelf: 'center' }}>

                  <TouchableOpacity
                    onPress={() => this.acceptTransferAdmin(item.entityId, item.params.adminRightChangeQueueId, item.params.notificationId)}
                    style={{ width: 70, height: 28, borderRadius: 17, backgroundColor: COLORS.dark_700, justifyContent: 'center', alignItems: 'center', marginRight: 6 }}
                    activeOpacity={0.8} >
                    <Text style={[defaultStyle.OVERLINE, { color: COLORS.altgreen_200, fontSize: 12 }]}>Accept</Text>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.declineTransferAdmin(item.entityId, item.params.adminRightChangeQueueId, item.params.notificationId)}
                    style={{ width: 30, height: 28, borderRadius: 15, justifyContent: 'center', alignItems: 'center', marginRight: 16, backgroundColor: COLORS.grey_200 }} activeOpacity={0.5} >
                    <Icon name='Cross' size={10} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                  </TouchableOpacity>

                </View>

                : item.status === 'RECEIVED' && item.invitationType === 'ADMIN_REQUEST_SENT' ?
                  <View style={{ flexDirection: 'row', alignSelf: 'center' }}>

                    <TouchableOpacity
                      onPress={() => this.acceptTransferAdminForProject(item.entityId, item.params.superAdminId, item.params.notificationId)}
                      style={{ width: 70, height: 28, borderRadius: 17, backgroundColor: COLORS.dark_700, justifyContent: 'center', alignItems: 'center', marginRight: 6 }}
                      activeOpacity={0.8} >
                      <Text style={[defaultStyle.OVERLINE, { color: COLORS.altgreen_200, fontSize: 12 }]}>Accept</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.declineTransferAdminForProject(item.entityId, item.params.superAdminId, item.params.notificationId)}
                      style={{ width: 30, height: 28, borderRadius: 15, justifyContent: 'center', alignItems: 'center', marginRight: 16, backgroundColor: COLORS.grey_200 }} activeOpacity={0.5} >
                      <Icon name='Cross' size={10} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                    </TouchableOpacity>

                  </View>

                  : item.status === 'RECEIVED' && item.invitationType === 'ADMIN_TRANSFER_REQUEST_SENT' ?
                    <View style={{ flexDirection: 'row', alignSelf: 'center' }}>

                      <TouchableOpacity
                        onPress={() => this.acceptSuperAdminTransferForProject(item.entityId, item.params.superAdminId, item.params.notificationId)}
                        style={{ width: 70, height: 28, borderRadius: 17, backgroundColor: COLORS.dark_700, justifyContent: 'center', alignItems: 'center', marginRight: 6 }}
                        activeOpacity={0.8} >
                        <Text style={[defaultStyle.OVERLINE, { color: COLORS.altgreen_200, fontSize: 12 }]}>Accept</Text>
                      </TouchableOpacity>

                      <TouchableOpacity onPress={() => this.declineSuperAdminTransferForProject(item.entityId, item.params.superAdminId, item.params.notificationId)}
                        style={{ width: 30, height: 28, borderRadius: 15, justifyContent: 'center', alignItems: 'center', marginRight: 16, backgroundColor: COLORS.grey_200 }} activeOpacity={0.5} >
                        <Icon name='Cross' size={10} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                      </TouchableOpacity>

                    </View>

                    : item.status === 'RECEIVED' && item.invitationType === 'PARTICIPANT' ?

                      <View style={{ flexDirection: 'row', alignSelf: 'center' }}>

                        <TouchableOpacity
                          onPress={() => this.acceptRequestByParticipant(item.params.requestId, item.params.notificationId, item.params.otherUserId)}
                          style={{ width: 70, height: 28, borderRadius: 17, backgroundColor: COLORS.dark_700, justifyContent: 'center', alignItems: 'center', marginRight: 6 }}
                          activeOpacity={0.8} >
                          <Text style={[defaultStyle.OVERLINE, { color: COLORS.altgreen_200, fontSize: 12 }]}>Accept</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.declineRequestByParticipant(item.params.requestId, item.params.notificationId)}
                          style={{ width: 30, height: 28, borderRadius: 15, justifyContent: 'center', alignItems: 'center', marginRight: 16, backgroundColor: COLORS.grey_200 }} activeOpacity={0.5} >
                          <Icon name='Cross' size={10} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                        </TouchableOpacity>

                      </View>

                      : item.status === 'RECEIVED' && item.invitationType === 'KEY_SPEAKER' ?

                        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>

                          <TouchableOpacity
                            onPress={() => this.acceptRequestByKeySpeaker(item.params.requestId, item.params.notificationId)}
                            style={{ width: 70, height: 28, borderRadius: 17, backgroundColor: COLORS.dark_700, justifyContent: 'center', alignItems: 'center', marginRight: 6 }}
                            activeOpacity={0.8} >
                            <Text style={[defaultStyle.OVERLINE, { color: COLORS.altgreen_200, fontSize: 12 }]}>Accept</Text>
                          </TouchableOpacity>

                          <TouchableOpacity onPress={() => this.declineRequestByKeySpeaker(item.params.requestId, item.params.notificationId)}
                            style={{ width: 30, height: 28, borderRadius: 15, justifyContent: 'center', alignItems: 'center', marginRight: 16, backgroundColor: COLORS.grey_200 }} activeOpacity={0.5} >
                            <Icon name='Cross' size={10} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                          </TouchableOpacity>

                        </View>

                        : item.status === 'RECEIVED' && item.invitationType === 'MEMBER' ?

                          <View style={{ flexDirection: 'row', alignSelf: 'center' }}>

                            <TouchableOpacity
                              onPress={() => this.updateProject(item.params.participationDetailId, item.params.otherParticipantUserId, item.entityId, 'PUBLISHED')}
                              style={{ width: 70, height: 28, borderRadius: 17, backgroundColor: COLORS.dark_700, justifyContent: 'center', alignItems: 'center', marginRight: 6 }}
                              activeOpacity={0.8} >
                              <Text style={[defaultStyle.OVERLINE, { color: COLORS.altgreen_200, fontSize: 12 }]}>Accept</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.updateProject(item.params.participationDetailId, item.params.otherParticipantUserId, item.entityId, 'DENIED')}
                              style={{ width: 30, height: 28, borderRadius: 15, justifyContent: 'center', alignItems: 'center', marginRight: 16, backgroundColor: COLORS.grey_200 }} activeOpacity={0.5} >
                              <Icon name='Cross' size={10} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                            </TouchableOpacity>

                          </View>

                          : item.status === 'SENT' && item.invitationType === 'MEMBER' ?

                            <TouchableOpacity
                              onPress={() => this.withdrawRequest(item.entityId)}
                              style={{ width: 85, height: 28, borderRadius: 17, borderColor: COLORS.altgreen_300, borderWidth: 1, justifyContent: 'center', alignItems: 'center', marginLeft: -26, alignSelf: 'center' }}
                              activeOpacity={0.6} >
                              <Text style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>Withdraw</Text>
                            </TouchableOpacity> : <></>}

            </View>


          )}
        />

      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  item: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F7F7F500',
    width: '100%',
    height: 52,
    // alignItems: 'center',
    justifyContent: 'space-between',
    // textAlign: 'center',
    marginBottom: 0,
    paddingLeft: 20
  },
  image: {
    height: 36,
    width: 36,
    borderRadius: 18
  },
  nameMsg: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 30,
    marginTop: 10,
    marginBottom: 4
  },
  name: {
    fontWeight: '600',
    color: '#4B4F56',
    fontSize: 11,
    textAlign: 'left'
  },
  forumItemView: {
    width: '86%',
    marginTop: 15,
    backgroundColor: COLORS.white,
    borderRadius: 4.5,
    alignSelf: 'center',
    paddingRight: 15,
    // paddingVertical: 16,
  },
  profileImage: {
    height: 66,
    width: '22%',
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    marginRight: 14,
  },
})
