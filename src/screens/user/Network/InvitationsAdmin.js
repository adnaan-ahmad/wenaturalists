import React, { Component } from 'react'
import { Image, StyleSheet, FlatList, TouchableOpacity, View, Text, SafeAreaView } from 'react-native'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import typography from '../../../Components/Shared/Typography'
import defaultCover from '../../../../assets/defaultProfile.png'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultStyle from '../../../Components/Shared/Typography'
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader'

const moment = require('moment')
const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class ReceivedInvitations extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            receivedInvitations: []
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value })
            this.getInvitations(value)

        }).catch((e) => {
            console.log(e)
        })

    }

    goback = () => { this.props.navigation.navigate('NetworkConnects') }


    getInvitations(userId) {
        axios({
            method: 'get',
            //   url: REACT_APP_userServiceURL + '/invitation/list?userId=' + userId + '&type=CIRCLE' + '&status=' + '' + "&page=" + 0 + "&size=" + 500,
            url: REACT_APP_userServiceURL + '/invitation/admin/list?userId=' + userId + "&page=" + 0 + "&size=" + 500,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.body && response.status === 200) {
                // console.log('getCircleInvitations', response.data.body.content[1])
                this.setState({ receivedInvitations: response.data.body.content })
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    ignorePendingConnect = (companyId, id) => {

        let postData = {
            companyId: companyId,
            userId: this.state.userId,
            notificationId: id,
            status: 'DECLINED'
        }

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/operator/update/status',
            data: postData,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.status === '202 ACCEPTED') {
                // console.log(response.data.status)
                this.getInvitations(this.state.userId)
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    acceptConnectInvitation = (companyId, id) => {

        let postData = {
            companyId: companyId,
            userId: this.state.userId,
            notificationId: id,
            status: 'APPROVED'
        }

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/operator/update/status',
            data: postData,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.status === '202 ACCEPTED') {
                // console.log(response.data.status)
                this.getInvitations(this.state.userId)
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                {/***** Header starts *****/}

                <ProfileEditHeader name="Invitations" iconName="Mail_OL" goback={this.goback} />

                {/***** Header ends *****/}

                <View style={{ alignSelf: 'center', flexDirection: 'row', backgroundColor: COLORS.altgreen_t50, width: 320, marginTop: 20, marginBottom: 10, justifyContent: 'center', borderRadius: 4 }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('InvitationsNetwork')}
                        style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 80, borderTopLeftRadius: 4, borderBottomLeftRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Network</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('InvitationsCircle')}
                        style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 80 }]}>
                        <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Circles</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('InvitationsProject')}
                        style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 80 }]}>
                        <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Projects</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[defaultShape.InTab_Btn, { backgroundColor: '#fff', width: 80, borderTopRightRadius: 4, borderBottomRightRadius: 4 }]}>
                        <Text style={[typography.Caption, { color: COLORS.dark_800 }]}>Admins</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ width: '100%', height: 24, backgroundColor: COLORS.altgreen_200, justifyContent: 'center', alignItems: 'center', marginTop: 18 }}>
                    <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400, textAlign: 'center' }]}>{this.state.receivedInvitations.length} PENDING</Text>
                </View>

                <FlatList
                    keyboardShouldPersistTaps='handled'
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 26, paddingTop: 16 }}
                    style={{ height: '50%' }}
                    keyExtractor={(item) => item.id}
                    data={this.state.receivedInvitations}
                    initialNumToRender={10}
                    renderItem={({ item }) => (

                        <View style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 12 }}>
                            <View style={styles.item}>

                                <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 16 }}>
                                    <Image source={item.coverImage ? { uri: item.coverImage } : { uri: "https://cdn.dscovr.com/images/circleDefault.webp" }} style={[styles.image, { marginLeft: '7%', marginTop: -10, zIndex: 2 }]} />
                                    <Image source={item.originalProfileImage ? { uri: item.originalProfileImage } : defaultCover} style={[styles.image, { marginLeft: -10 }]} />
                                </View>

                                <View style={styles.nameMsg}>
                                    <Text style={[defaultStyle.Note2, { color: '#698f8a', maxWidth: 230, fontSize: 10.5 }]}>{item.username.split(' ').slice(0, 2).join(' ').charAt(0).toUpperCase() + item.username.split(' ').slice(0, 2).join(' ').slice(1)} {item.message.trim()} <Text style={styles.name}>{item.params && item.params.businessPageName}</Text></Text>
                                    <Text style={[defaultStyle.Subtitle_2, { color: '#99b2bf', fontSize: 10, marginTop: 3 }]}>{moment.unix(item.updateTime / 1000).format('Do MMM, YYYY')}</Text>
                                </View>

                            </View>


                            <View style={{ flexDirection: 'row', marginTop: 10 }}>

                                <TouchableOpacity
                                    onPress={() => this.acceptConnectInvitation(item.params.userId, item.id)}
                                    style={{ width: 60, height: 28, borderRadius: 17, backgroundColor: COLORS.dark_700, justifyContent: 'center', alignItems: 'center', marginRight: 6 }}
                                    activeOpacity={0.8} >
                                    <Text style={[defaultStyle.OVERLINE, { color: COLORS.altgreen_200, fontSize: 10.5 }]}>Accept</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.ignorePendingConnect(item.params.userId, item.id)}
                                    style={{ width: 30, height: 28, borderRadius: 15, justifyContent: 'center', alignItems: 'center', marginRight: 16, backgroundColor: COLORS.grey_200 }} activeOpacity={0.5} >
                                    <Icon name='Cross' size={10} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                                </TouchableOpacity>

                            </View>

                        </View>


                    )}
                />

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    item: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#F7F7F500',
        width: '100%',
        height: 52,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',
        marginBottom: 0,
        paddingLeft: 20
    },
    image: {
        height: 36,
        width: 36,
        borderRadius: 18
    },
    nameMsg: {
        flex: 1,
        flexDirection: 'column',
        marginBottom: 10
    },
    name: {
        fontWeight: '600',
        color: '#4B4F56',
        fontSize: 11,
        textAlign: 'left'
    },
})
