import React, { Component } from 'react';
import {
  Clipboard,
  Share,
  Modal,
  TextInput,
  FlatList,
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Platform,
  SafeAreaView,
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';

import { COLORS } from '../../../Components/Shared/Colors';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultShape from '../../../Components/Shared/Shape';
import typography from '../../../Components/Shared/Typography';
import {
  REACT_APP_domainUrl,
  REACT_APP_userServiceURL,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class MyOrganization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      pageNumber: 0,
      company: [],
      grpDetails: {},
      sortBy: 'firstName',
      searchIcon: true,
      searchText: '',
      postModalOpen: false,
      optionsModalOpen: false,
      grpDetailsModalOpen: false,
      groupId: '',
      description: '',
      totalElements: 0,
      pressedGroupName: '',
      pressedGroupDescription: ''
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login')
      }
    })

    AsyncStorage.getItem('userId')
      .then((value) => {
        value && this.setState({ userId: value });
        this.getPublicChatGroup(value);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  submitRequest = () => {
    let postBody = {
      userId: this.state.userId,
      other: this.state.groupId,
      messageGroupType: 'MemberGroup',
      userActionType: 'AddRequestGroup',
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/messaging/message_service/api/users/submitRequest',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        if (response.data.message === 'Success!') {
          this.setState(
            { company: [], pageNumber: 0, optionsModalOpen: false },
            () => this.getPublicChatGroup(this.state.userId),
          );
        }
      })
      .catch((err) => console.log(err));
  };

  handleLoadmore = () => {
    this.setState({ pageNumber: this.state.pageNumber + 1 }, () =>
      this.getPublicChatGroup(this.state.userId),
    );
  };

  getPublicChatGroup = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/messages/getPublishedMessageGroup?messageGroupType=MemberGroup&userId=' +
        userId +
        '&page=' +
        this.state.pageNumber +
        '&size=10',
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({
            company: this.state.company.concat(response.data.body.content),
            totalElements: response.data.body.totalElements,
          });
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }

  getGroupMemberList = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/messages/getMessageGroupById?messageGroupId=' +
        this.state.groupId +
        '&userId=' +
        this.state.userId,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 200 && response.data && response.data.body) {
          let userDetailsDTOList = response.data.body.userDetailsDTOList
          let requestSentUsers = response.data.body.requestSent
          let exitUsers = response.data.body.exitUsers

          if (userDetailsDTOList && requestSentUsers) {
            if (requestSentUsers) {
              userDetailsDTOList = userDetailsDTOList.filter(function (el) {
                return !requestSentUsers.includes(el.id)
              });
            }
            if (exitUsers) {
              userDetailsDTOList = userDetailsDTOList.filter(function (el) {
                return !exitUsers.includes(el.id)
              });
            }

          }

          this.setState({
            grpDetails: userDetailsDTOList,
            pressedGroupName: response.data.body.name,
            pressedGroupDescription: response.data.body.desc,
            grpDetailsModalOpen: true,
            optionsModalOpen: false,
          })
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }

  searchNames = (query) => {
    const regex = new RegExp(`${query.trim()}`, 'i')

    if (this.state.sortBy === 'firstName' && this.state.company.length > 0) {
      return this.state.company
        .sort((a, b) => a.name.toUpperCase() > b.name.toUpperCase())
        .filter((item) => item.name.search(regex) >= 0);
    } else if (
      this.state.sortBy === 'recentlyAdded' &&
      this.state.company.length > 0
    ) {
      return this.state.company.filter((item) => item.name.search(regex) >= 0);
    }
  };

  postModal = () => {
    return (
      <Modal
        visible={this.state.postModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={[defaultShape.Linear_Gradient_View, { bottom: 100 }]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ postModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <Text
            style={[
              typography.Caption,
              {
                fontSize: 12,
                color: COLORS.dark_500,
                textAlign: 'center',
                alignSelf: 'center',
                position: 'absolute',
                bottom: 110,
                zIndex: 2,
              },
            ]}>
            Sort By
          </Text>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              },
            ]}>
            <TouchableOpacity
              style={
                this.state.sortBy === 'firstName'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ postModalOpen: false, sortBy: 'firstName' });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'firstName'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Name
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.sortBy === 'recentlyAdded'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ postModalOpen: false, sortBy: 'recentlyAdded' });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'recentlyAdded'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Recently Added
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }

  grpDetailsModal = () => {
    return (
      <Modal
        visible={this.state.grpDetailsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={[defaultShape.Linear_Gradient_View, { bottom: 0 }]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ grpDetailsModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[defaultShape.Modal_Categories_Container, { maxHeight: 400 }]}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                  flexDirection: 'column',
                  paddingBottom: 10,
                  paddingTop: 0,
                  borderBottomColor: COLORS.grey_350,
                },
              ]}>

              <Text style={[typography.Title_2, { color: COLORS.dark_700 }]}>
                {this.state.pressedGroupName}
              </Text>

              {this.state.pressedGroupDescription ?
                <Text
                  style={[
                    typography.Note2,
                    { color: COLORS.grey_400, marginTop: 5 },
                  ]}>
                  {this.state.pressedGroupDescription}
                </Text> : <></>}
            </View>

            <FlatList
              showsVerticalScrollIndicator={false}
              data={
                this.state.grpDetails
              }
              style={{ width: '85%' }}
              keyExtractor={(item) => item.id}
              renderItem={(item) => (
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 10,
                    borderBottomColor: COLORS.grey_300,
                    borderBottomWidth: 0.5,
                    paddingBottom: 5,
                    alignItems: 'center',
                  }}>
                  <Image
                    source={
                      item.item.personalInfo.profileImage
                        ? { uri: item.item.personalInfo.profileImage }
                        : defaultProfile
                    }
                    style={styles.image}
                  />
                  <Text
                    style={[
                      typography.Caption,
                      { color: COLORS.altgreen_400, marginLeft: 10 },
                    ]}>
                    {item.item.username}
                  </Text>
                </View>
              )}
            />
          </View>
        </View>
      </Modal>
    );
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ optionsModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                Public Chat Group
              </Text>
            </View>

            <TouchableOpacity
              onPress={this.submitRequest}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Send-Message"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 9 } : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Send Request
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this.getGroupMemberList}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Info"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Group Details
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        {this.postModal()}
        {this.optionsModal()}
        {this.grpDetailsModal()}

        {/******** Header starts ********/}

        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_400} size={16} />
          </TouchableOpacity>

          <View style={{ alignItems: 'center', marginRight: 30 }}>
            <View
              style={[
                defaultShape.Media_Round,
                {
                  backgroundColor: COLORS.altgreen_300,
                  alignItems: 'center',
                  justifyContent: 'center',
                },
              ]}>
              <Icon
                name="Community"
                color={COLORS.altgreen_50}
                size={16}
                style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
              />
            </View>
            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
              Groups
            </Text>
          </View>

          <View></View>
        </View>

        {/******** Header ends ********/}

        <View
          style={{
            height: 56,
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            marginBottom: 6,
            marginTop: 6,
          }}>
          <TextInput
            style={styles.textInput}
            placeholderTextColor="#D9E1E4"
            onChangeText={(value) => {
              this.setState({ searchText: value });
            }}
            color="#154A59"
            placeholder="Search"
            onFocus={() => this.setState({ searchIcon: false })}
            onBlur={() => this.setState({ searchIcon: true })}
            underlineColorAndroid="transparent"
            ref={(input) => {
              this.textInput = input;
            }}
          />

          <TouchableOpacity
            onPress={() => this.setState({ postModalOpen: true })}>
            <Icon
              name="FilterSlide"
              size={18}
              color={COLORS.altgreen_400}
              style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: '100%',
            height: 24,
            backgroundColor: COLORS.altgreen_200,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 6,
          }}>
          <Text
            style={[
              typography.Caption,
              { color: COLORS.altgreen_400, textAlign: 'center' },
            ]}>
            {this.state.totalElements} groups
          </Text>
        </View>

        <FlatList
          keyboardShouldPersistTaps="handled"
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 80,
            width: '90%',
            alignSelf: 'center',
            marginLeft: 15,
          }}
          keyExtractor={(item) => item.id}
          data={this.searchNames(this.state.searchText)}
          // data={this.state.company}
          onEndReached={this.handleLoadmore}
          onEndReachedThreshold={2}
          renderItem={({ item }) => (
            <TouchableOpacity activeOpacity={0.6}>
              <View style={styles.item}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <View style={{ height: 52, width: 60 }}>
                    <Image
                      source={
                        item.userDtoList[0].imageUrl
                          ? { uri: item.userDtoList[0].imageUrl }
                          : defaultProfile
                      }
                      style={[
                        styles.image,
                        { position: 'absolute', left: 0, top: 0 },
                      ]}
                    />
                    <Image
                      source={
                        item.userDtoList.length >= 2 &&
                          item.userDtoList[1].imageUrl
                          ? { uri: item.userDtoList[1].imageUrl }
                          : defaultProfile
                      }
                      style={[
                        styles.image,
                        { position: 'absolute', left: 0, bottom: 0 },
                      ]}
                    />
                    <Image
                      source={
                        item.userDtoList.length >= 3 &&
                          item.userDtoList[2].imageUrl
                          ? { uri: item.userDtoList[2].imageUrl }
                          : defaultProfile
                      }
                      style={[
                        styles.image,
                        { position: 'absolute', right: 5, top: 10 },
                      ]}
                    />
                  </View>

                  <View style={styles.nameMsg}>
                    <Text numberOfLines={1} style={styles.name}>
                      {item.name}
                    </Text>
                  </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        optionsModalOpen: true,
                        groupId: item.id,
                      })
                    }
                    style={{
                      width: 34,
                      height: 34,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    activeOpacity={0.5}>
                    <Icon
                      name="Kebab"
                      size={14}
                      color={COLORS.grey_350}
                      style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    height: 64,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: '#fff',
    width: '100%',
    height: 60,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 8,
  },
  image: {
    height: 30,
    width: 30,
    borderRadius: 15,
  },
  nameMsg: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 10,
    maxWidth: 180,
  },
  name: {
    fontWeight: '700',
    color: '#4B4F56',
    fontSize: 14,
    textAlign: 'left',
    maxWidth: 180,
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '80%',
    height: 46,
    zIndex: 1,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  unselectedText: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLORS.altgreen_300,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 10,
    borderRadius: 20,
  },
  selectedText: {
    backgroundColor: COLORS.dark_700,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 20,
  },
});
