import React, { Component } from 'react'
import { Alert, Image, StyleSheet, FlatList, TouchableOpacity, View, Text, SafeAreaView } from 'react-native'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import typography from '../../../Components/Shared/Typography'
import defaultProfile from '../../../../assets/defaultProfile.png'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultStyle from '../../../Components/Shared/Typography'
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader'

const moment = require('moment')
const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class SentInvitations extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userId: '',
            sentInvitations: []
        }
    }

    componentDidMount() {

        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value })
            this.getRequestedInvitations(value)

        }).catch((e) => {
            // console.log(e)
        })

    }

    goback = () => { this.props.navigation.navigate('MyNetwork') }

    getRequestedInvitations(userId) {
        axios({
            method: 'get',
            url: REACT_APP_userServiceURL + '/network/' + userId + '/requestedInvitations'
                + "?page=" + 0 + "&size=" + 1000,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.data && response.data.message === 'Success!') {
                // console.log('sentInvitations:', response.data.body.page.content[0])
                this.setState({ sentInvitations: response.data.body.content.reverse() })
            }
        }).catch((err) => {
            // console.log(err)
        })
    }

    removeRequest = (id) => {

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/removeInvite/' + id,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true
        }).then(response => {
            if (response && response.status === 202) {
                // console.log(response.status)
                this.getRequestedInvitations(this.state.userId)
            }
        }).catch((err) => {
            // console.log(err)
        })
    }

    removeRequestAlert = (id) => {
        this.setState({ optionsModalOpen: false });
        Alert.alert('', 'Are you sure you want to cancel your request? You will not be able to connect the member for next 30 days.', [
          {
            text: 'YES',
            onPress: () => this.removeRequest(id),
            style: 'cancel',
          },
          {
            text: 'NO',
            // onPress: () => console.log('user cancelled deletion'),
          },
        ]);
      }

    listHeaderComponent = () => {
        return (
            <View style={{ width: '100%', height: 26, backgroundColor: COLORS.altgreen_200, justifyContent: 'center', alignItems: 'center', marginTop: -16, marginBottom: 14 }}>
                <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400, textAlign: 'center' }]}>{this.state.sentInvitations.length} PENDING</Text>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                {/***** Header starts *****/}

                <View style={styles.header}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={defaultShape.Nav_Gylph_Btn}>
                        <Icon name="Arrow-Left" color={COLORS.altgreen_400} size={16} />
                    </TouchableOpacity>

                    <View style={{ alignItems: 'center', marginRight: 30 }}>
                        <View
                            style={[
                                defaultShape.AdjunctBtn_Prim,
                                { backgroundColor: COLORS.altgreen_300, width: 36, height: 36, marginTop: 0 },
                            ]}>
                            <Icon
                                name="AddUser"
                                size={18}
                                color={COLORS.altgreen_50}
                                style={{ marginTop: Platform.OS === 'ios' ? 0 : 5 }}
                            />
                        </View>
                        <Text style={[typography.Button_Lead, { color: COLORS.dark_600, marginTop: 4 }]}>
                            Your Requests
                        </Text>
                    </View>

                    <View></View>
                </View>

                {/***** Header ends *****/}



                <FlatList
                    keyboardShouldPersistTaps='handled'
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 26, paddingTop: 16 }}
                    style={{ height: '50%' }}
                    keyExtractor={(item) => item.id}
                    ListHeaderComponent={this.listHeaderComponent}
                    data={this.state.sentInvitations}
                    initialNumToRender={10}
                    renderItem={({ item }) => (

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfileStack', {
                            screen: 'OtherProfileScreen',
                            params: { userId: item.id },
                        })}
                            activeOpacity={0.7} >
                            <View style={styles.item}>

                                <Image source={item.personalInfo && item.personalInfo.profileImage ? { uri: item.personalInfo.profileImage } : defaultProfile} style={[styles.image, { marginLeft: '7%' }]} />

                                <View style={styles.nameMsg}>
                                    <Text numberOfLines={1} style={styles.name}>{item.username.split(' ').slice(0, 2).join(' ').charAt(0).toUpperCase() + item.username.split(' ').slice(0, 2).join(' ').slice(1)}
                                        <Text
                                            style={[
                                                defaultStyle.Note2,
                                                { color: COLORS.grey_400, fontSize: 10 },
                                            ]}>
                                            {item.connectDepth === 1
                                                ? ' • 1st'
                                                : item.connectDepth === 2
                                                    ? ' • 2nd'
                                                    : ''}
                                        </Text>
                                    </Text>
                                    {item && item.persona ?
                                        <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>{item.persona.charAt(0).toUpperCase() + item.persona.slice(1)}</Text> :
                                        <></>}
                                    {item.addressDetail && item.addressDetail.country ?
                                        <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>{item.addressDetail.country.charAt(0).toUpperCase() + item.addressDetail.country.slice(1)}</Text> :
                                        <></>}
                                    {item.relationTimestamp && <Text style={[defaultStyle.Subtitle_2, { color: '#99b2bf', fontSize: 10, marginTop: 3 }]}>Requested on {moment.unix(item.relationTimestamp / 1000).format('Do MMM, YYYY')}</Text>}
                                </View>

                                {/* <View style={styles.nameMsg}>

                  <Text style={styles.name}>Derek James</Text>

                  <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>Head Naturalist</Text>

                  <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>Australia</Text>

                </View> */}

                                <TouchableOpacity
                                    onPress={() => this.removeRequestAlert(item.id)}
                                    style={{ width: 85, height: 28, borderRadius: 17, borderColor: COLORS.altgreen_300, borderWidth: 1, justifyContent: 'center', alignItems: 'center', marginRight: 20 }}
                                    activeOpacity={0.6} >
                                    <Text style={[defaultStyle.Caption, { color: COLORS.dark_500 }]}>Withdraw</Text>
                                </TouchableOpacity>

                            </View>

                        </TouchableOpacity>


                    )}
                />

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    item: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#F7F7F500',
        width: '100%',
        height: 52,
        alignItems: 'center',
        justifyContent: 'space-between',
        textAlign: 'center',
        marginBottom: 8
    },
    header: {
        flexDirection: 'row',
        height: 70,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: COLORS.white,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
        // paddingVertical: 4
    },
    image: {
        height: 36,
        width: 36,
        borderRadius: 18
    },
    nameMsg: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: '6%'
    },
    name: {
        fontWeight: '700',
        color: '#4B4F56',
        fontSize: 14,
        textAlign: 'left',
        maxWidth: '90%'
    },
})