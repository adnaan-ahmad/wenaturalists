import React, { Component } from 'react';
import {
  Alert,
  Clipboard,
  Share,
  Modal,
  TextInput,
  FlatList,
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Platform,
  SafeAreaView,
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';

import defaultStyle from '../../../Components/Shared/Typography'
import { COLORS } from '../../../Components/Shared/Colors';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultShape from '../../../Components/Shared/Shape';
import typography from '../../../Components/Shared/Typography';
import {
  REACT_APP_domainUrl,
  REACT_APP_userServiceURL,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class MyConnects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      pageNumber: 0,
      connects: [],
      sortBy: 'firstName',
      searchIcon: true,
      searchText: '',
      postModalOpen: false,
      optionsModalOpen: false,
      pressedUserId: '',
      shareModalOpen: false,
      customUrl: '',
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      pressedUserName: '',
      isReported: false
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        value && this.setState({ userId: value });
        this.getConnects(value);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  getConnects(userId) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/connections?requestingUserId=' +
        userId +
        '&page=' + this.state.pageNumber +
        '&size=10',
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log('my connects: ', response.data.body.content)
          this.setState({
            connects: this.state.connects.concat(response.data.body.content),
          })
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  handleLoadmore = () => {
    this.setState({ pageNumber: this.state.pageNumber + 1 }, () =>
      this.getConnects(this.state.userId),
    );
  };

  searchNames = (query) => {
    const regex = new RegExp(`${query.trim()}`, 'i');

    if (this.state.sortBy === 'firstName' && this.state.connects.length > 0) {
      return (
        this.state.connects
          // .sort((a, b) => a.username.toUpperCase() > b.username.toUpperCase())
          .sort((a, b) => a.createTime < b.createTime)
          .filter((item) => item.firstName.search(regex) >= 0)
      );
    } else if (
      this.state.sortBy === 'lastName' &&
      this.state.connects.length > 0
    ) {
      return (
        this.state.connects
          // .sort(
          //   (a, b) =>
          //     a.username.toUpperCase().split(' ')[1] >
          //     b.username.toUpperCase().split(' ')[1],
          // )
          .sort((a, b) => a.createTime < b.createTime)
          .filter((item) => item.lastName.search(regex) >= 0)
      );
    } else if (
      this.state.sortBy === 'recentlyAdded' &&
      this.state.connects.length > 0
    ) {
      return this.state.connects
        .sort((a, b) => a.createTime < b.createTime)
        .filter((item) => item.username.search(regex) >= 0);
    } else if (
      this.state.sortBy === 'country' &&
      this.state.connects.length > 0
    ) {
      return this.state.connects
        .sort((a, b) => a.createTime < b.createTime)
        .filter((item) => item.username.search(regex) >= 0);
    }
  };

  postModal = () => {
    return (
      <Modal
        visible={this.state.postModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={[defaultShape.Linear_Gradient_View, { bottom: 100 }]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ postModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <Text
            style={[
              typography.Caption,
              {
                fontSize: 12,
                color: COLORS.dark_500,
                textAlign: 'center',
                alignSelf: 'center',
                position: 'absolute',
                bottom: 110,
                zIndex: 2,
              },
            ]}>
            Search By
          </Text>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              },
            ]}>
            <TouchableOpacity
              style={
                this.state.sortBy === 'firstName'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ postModalOpen: false, sortBy: 'firstName' });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'firstName'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                First Name
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.sortBy === 'lastName'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ postModalOpen: false, sortBy: 'lastName' });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'lastName'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Last Name
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.sortBy === 'recentlyAdded'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ postModalOpen: false, sortBy: 'recentlyAdded' });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'recentlyAdded'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Recently Added
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                this.state.sortBy === 'country'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ postModalOpen: false, sortBy: 'country' });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'country'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Country
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ optionsModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                Connect
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ optionsModalOpen: false }),
                  // this.props.changeState({ navigate: true, navigateId: this.state.pressedUserId })
                  this.props.navigation.navigate('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: { userId: this.state.pressedUserId },
                  });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="SingleUser"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 9 } : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  View Profile
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ optionsModalOpen: false, shareModalOpen: true });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Share
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { paddingTop: 25, paddingBottom: 15 },
                  ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ optionsModalOpen: false }, () => {

                  Alert.alert(
                    "",
                    `Are you sure you want to remove ${this.state.pressedUserName} as your connect? You will not be able to connect the member for next 30 days`,
                    [
                      {
                        text: "YES",
                        onPress: () => this.unconnectUser(this.state.pressedUserId),
                        style: "cancel"
                      },
                      { text: "NO" }
                    ]
                  )
                },
                  // this.unconnectUser(this.state.pressedUserId),
                );
              }}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Remove Connect
              </Text>
              <Icon
                name="RemoveUser"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { borderBottomWidth: 0 },
                  ]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.verifyReported()
                setTimeout(() => {
                  this.setState({
                  optionsModalOpen: false,
                  // reasonForReportingModalOpen: true,
                })
                }, 2000) 
              }
                
              }>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Report Connect
              </Text>
              <Icon
                name="ReportComment_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ shareModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { paddingTop: 25, paddingBottom: 15 },
                  ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ shareModalOpen: false });
              }}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl + '/profile/' + this.state.customUrl,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Copy link to profile
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ shareModalOpen: false });
              }}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Share through Mail
              </Text>
              <Icon
                name="Mail_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({ shareModalOpen: false }, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { borderBottomWidth: 0 },
                  ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message: REACT_APP_domainUrl + '/profile/' + this.state.customUrl,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              { height: 700, bottom: 0 },
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={[defaultShape.CloseBtn, { marginBottom: 0 }]}
            onPress={() =>
              this.setState({
                reasonForReportingModalOpen: false,
                reasonForReporting: '',
              })
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
            />
          </TouchableOpacity>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.altgreen_100,
              alignItems: 'center',
              paddingTop: 15,
              paddingBottom: 10,
              // paddingLeft: 20,
              width: '90%',
              alignSelf: 'center',
              marginBottom: 30,
              marginTop: 15,
            }}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                Reason for reporting
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { paddingVertical: 15, borderBottomWidth: 0 },
                  ]
                  : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ reasonForReporting: 'FAKE_SPAM_OR_SCAM' });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Fake, spam or scam
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ reasonForReporting: 'ACCOUNT_MAY_BE_HACKED' });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Account may be hacked
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ reasonForReporting: 'IMPERSONATING_SOMEONE' });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'IMPERSONATING_SOMEONE' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  numberOfLines={2}
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Impersonating someone
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ reasonForReporting: 'VIOLATES_TERMS_OF_USE' });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'VIOLATES_TERMS_OF_USE' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Violates Terms Of Use
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting: 'OTHERS'
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'OTHERS' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Others
                </Text>
              </View>
            </TouchableOpacity>

            {this.state.reasonForReporting === 'OTHERS' &&
              <View style={{
                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 6, paddingRight: 12
              }}>

                <TextInput
                  theme={{ colors: { text: COLORS.dark_700, primary: COLORS.altgreen_300, placeholder: COLORS.altgreen_300 } }}
                  placeholder="Write the details"
                  placeholderTextColor={COLORS.dark_600}
                  multiline
                  selectionColor='#C8DB6E'
                  style={[defaultStyle.Subtitle_1, { width: '90%', height: 56, backgroundColor: COLORS.altgreen_100, color: COLORS.dark_700 }]}
                  onChangeText={(value) => this.setState({ description: value })}
                  value={this.state.description}
                />

              </View>}

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                this.state.reasonForReporting === '' ?
                  Snackbar.show({
                    backgroundColor: '#B22222',
                    text: "Please select an option",
                    duration: Snackbar.LENGTH_LONG,
                  })
                  :
                  this.state.reasonForReporting === 'OTHERS' && this.state.description.trim() === '' ?
                    Snackbar.show({
                      backgroundColor: '#B22222',
                      text: "Please enter the detail",
                      duration: Snackbar.LENGTH_LONG,
                    })
                    :
                    (this.handleReportAbuseSubmit(), this.setState({ reasonForReportingModalOpen: false }))
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 15,
                height: 27,
                marginVertical: 10,
                borderRadius: 16,
                textAlign: 'center',
                borderWidth: 1,
                borderColor: '#698F8A',
              }}>
              <Text
                style={{
                  color: '#698F8A',
                  fontSize: 14,
                  paddingHorizontal: 14,
                  paddingVertical: 20,
                  fontFamily: 'Montserrat-Medium',
                  fontWeight: 'bold',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }

  verifyReported = () => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/verifyAlreadyReported?reporterId='
        + this.state.userId + '&entityId=' + this.state.pressedUserId,
      withCredentials: true
    }).then(response => {
      if (response && response.status === 200 && response.data && response.data.body) {
        // console.log(response.data.body.reported)
        !response.data.body.reported ? 
        this.setState({ isReported: response.data.body.reported, reasonForReportingModalOpen: true })
        :
        Snackbar.show({
          backgroundColor: COLORS.primarydark,
          text: "Your report request was already taken",
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        })

      } else {
        console.log(response)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedUserId,
      // entityType: this.state.entityType,
      entityType: 'USER',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({ reasonForReporting: this.state.reasonForReporting });
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        this.setState({ reasonForReporting: this.state.reasonForReporting });
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
  };

  unconnectUser = (id) => {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unconnect/' +
        id,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          console.log(response.status);


        }
      })
      .catch((err) => {
        console.log(err)
      })
    this.setState({ connects: this.state.connects.filter(item => id !== item.id) })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        {this.postModal()}
        {this.optionsModal()}
        {this.shareModal()}
        {this.reasonForReportingModal()}

        {/******** Header starts ********/}

        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_400} size={16} />
          </TouchableOpacity>

          <View style={{ alignItems: 'center', marginRight: 30 }}>
            <View
              style={[
                defaultShape.Media_Round,
                {
                  backgroundColor: COLORS.green_500,
                  alignItems: 'center',
                  justifyContent: 'center',
                },
              ]}>
              <Icon
                name="Network_F"
                color={COLORS.dark_700}
                size={16}
                style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
              />
            </View>
            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
              My Connects
            </Text>
          </View>

          <View></View>
        </View>

        {/******** Header ends ********/}

        <View
          style={{
            height: 56,
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            marginBottom: 6,
            marginTop: 6,
          }}>
          <TextInput
            style={styles.textInput}
            placeholderTextColor="#D9E1E4"
            onChangeText={(value) => {
              this.setState({ searchText: value });
            }}
            color="#154A59"
            placeholder="Search"
            onFocus={() => this.setState({ searchIcon: false })}
            onBlur={() => this.setState({ searchIcon: true })}
            underlineColorAndroid="transparent"
            ref={(input) => {
              this.textInput = input;
            }}
          />

          <TouchableOpacity
            onPress={() => this.setState({ postModalOpen: true })}>
            <Icon
              name="FilterSlide"
              size={18}
              color={COLORS.altgreen_400}
              style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: '100%',
            height: 24,
            backgroundColor: COLORS.altgreen_200,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 6,
          }}>
          <Text
            style={[
              typography.Caption,
              { color: COLORS.altgreen_400, textAlign: 'center' },
            ]}>
            {this.state.connects.length} CONNECTS
          </Text>
        </View>

        <FlatList
          keyboardShouldPersistTaps="handled"
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 80,
            width: '90%',
            alignSelf: 'center',
            marginLeft: 15,
          }}
          keyExtractor={(item) => item.id}
          data={this.searchNames(this.state.searchText)}
          // data={this.state.connects}
          onEndReached={this.handleLoadmore}
          onEndReachedThreshold={1}
          renderItem={({ item }) => (
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() =>
                this.props.navigation.navigate('ProfileStack', {
                  screen: 'OtherProfileScreen',
                  params: { userId: item.connectStatus.profileId },
                })
              }>
              <View style={styles.item}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image
                    source={
                      item.personalInfo.profileImage
                        ? { uri: item.personalInfo.profileImage }
                        : defaultProfile
                    }
                    style={styles.image}
                  />

                  <View style={styles.nameMsg}>
                    <Text numberOfLines={1} style={styles.name}>
                      {item.username
                        .split(' ')
                        .slice(0, 2)
                        .join(' ')
                        .charAt(0)
                        .toUpperCase() +
                        item.username.split(' ').slice(0, 2).join(' ').slice(1)}
                    </Text>
                    {item && item.persona ? (
                      <Text
                        numberOfLines={1}
                        style={[
                          typography.Note2,
                          { color: COLORS.altgreen_300 },
                        ]}>
                        {item.persona.charAt(0).toUpperCase() +
                          item.persona.slice(1)}
                      </Text>
                    ) : (
                      <></>
                    )}
                    {item && item.addressDetail ? (
                      <Text
                        numberOfLines={1}
                        style={[
                          typography.Note2,
                          { color: COLORS.altgreen_300 },
                        ]}>
                        {item.addressDetail.country.charAt(0).toUpperCase() +
                          item.addressDetail.country.slice(1)}
                      </Text>
                    ) : (
                      <></>
                    )}

                    {item.mutualConnectCount ? (
                      <Text
                        style={[typography.Note2, { color: '#888' }]}>
                        {item.mutualConnectCount} mutual connects
                      </Text>
                    ) : (
                      <></>
                    )}
                  </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('Chats', {
                        userId: this.state.userId,
                        otherUserId: item.id,
                        lastActive: null,
                        grpType: 'Private',
                        name: item.username,
                        otherUserProfile:
                          item.personalInfo && item.personalInfo.profileImage
                            ? item.personalInfo.profileImage
                            : null,
                      })
                    }
                    style={{
                      width: 34,
                      height: 34,
                      borderRadius: 17,
                      backgroundColor: COLORS.altgreen_200,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 2,
                    }}
                    activeOpacity={0.5}>
                    <Icon
                      name="Messeges_F"
                      size={14}
                      color={COLORS.dark_600}
                      style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        optionsModalOpen: true,
                        pressedUserId: item.id,
                        customUrl: item.customUrl,
                        pressedUserName: item.username
                      })
                    }
                    style={{
                      width: 34,
                      height: 34,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    activeOpacity={0.5}>
                    <Icon
                      name="Kebab"
                      size={14}
                      color={COLORS.grey_350}
                      style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    height: 64,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F7F7F500',
    width: '100%',
    height: 52,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
    marginTop: 8
  },
  image: {
    height: 34,
    width: 34,
    borderRadius: 17,
  },
  nameMsg: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 10,
    maxWidth: 180,
  },
  name: {
    fontWeight: '700',
    color: '#4B4F56',
    fontSize: 13,
    textAlign: 'left',
    maxWidth: 180,
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '80%',
    height: 46,
    zIndex: 1,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  unselectedText: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLORS.altgreen_300,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 10,
    borderRadius: 20,
  },
  selectedText: {
    backgroundColor: COLORS.dark_700,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 20,
  },
});
