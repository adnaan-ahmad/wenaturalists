import React, {Component} from 'react';
import {
  Image,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Platform,
  Modal,
  TextInput,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import Snackbar from 'react-native-snackbar';
import LinearGradient from 'react-native-linear-gradient';

import {REACT_APP_userServiceURL} from '../../../../env.json';
import SearchBar from '../../../Components/User/SearchBar';
import defaultCover from '../../../../assets/defaultCover.png';
import defaultProfile from '../../../../assets/defaultProfile.png';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import typography from '../../../Components/Shared/Typography';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class NetworkConnects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      otherUserId: '',
      currentUserType: '',
      mutualConnectCount: 0,
      mutualConnectList: [],
      mutualConnectListModalOpen: false,
      notification: false,
      redirectToProfile: false,
      unAuthOrganizationList: [],
      popularUsersData: [],
      recommendedUsers: [],
      receivedInvitations: '',
      requestedList: [],
      pageSize1: 0,
      pageSize2: 0,
      pageSizeMutualConnects: 0,
      notification: false,
      redirectToProfile: false,
      optionsModalOpen: false,
      pressedItem: {},
      reasonForReporting: '',
      description: '',
      reasonForReportingModalOpen: false,
      userType: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({userId: value});
        this.getPopularUsers(value);
        this.getRecommendedUsers(value, '');
        this.getInvitations(value);

        axios({
          method: 'get',
          url:
            REACT_APP_userServiceURL +
            '/profile/get?id=' +
            value +
            '&otherUserId=' +
            '',
          cache: true,
          withCredentials: true,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => {
            this.setState({userType: response.data.body.type});
            // console.log('--- response.data.body ---', response.data.body)
          })
          .catch((err) => {
            // console.log("Profile data error : ", err)
          });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  navigateNotification = () => {
    this.props.navigation.navigate('Notification');
  };

  navigateProfile = () => {
    if (this.state.userType === 'INDIVIDUAL') {
      this.props.navigation.navigate('ProfileStack');
    } else if (this.state.userType === 'COMPANY') {
      this.props.navigation.navigate('ProfileStack', {
        screen: 'CompanyProfileScreen',
      });
    }
  };

  navigation = (value, params) => {
    return this.props.navigation.navigate(value, params);
  };

  handleLoadmore1 = () => {
    this.setState({pageSize1: this.state.pageSize1 + 1}, () =>
      this.getPopularUsers(this.state.userId),
    );
  };

  handleLoadmore2 = () => {
    this.setState({pageSize2: this.state.pageSize2 + 1}, () =>
      this.getRecommendedUsers(this.state.userId, ''),
    );
  };

  getPopularUsers = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/popularUsers' +
        '?removeUserId=' +
        '' +
        '&page=0' +
        // this.state.pageSize1 +
        '&size=' +
        1000,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({
            popularUsersData: this.state.popularUsersData.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getRecommendedUsers = (userId, removeUserId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/recommendedUsers' +
        '?removeUserId=' +
        removeUserId +
        '&page=' +
        this.state.pageSize2 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({
            recommendedUsers: this.state.recommendedUsers.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  removeRecommendedUsers = (userId, removeUserId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/recommendedUsers' +
        '?removeUserId=' +
        removeUserId +
        '&page=' +
        this.state.pageSize2 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          let tempindex = this.state.recommendedUsers.findIndex(
            (x) => x.id === removeUserId,
          );
          this.state.recommendedUsers.splice(tempindex, 1);
          this.setState({
            recommendedUsers: this.state.recommendedUsers,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  removePopularUsers = (userId, removeUserId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/popularUsers' +
        '?removeUserId=' +
        removeUserId +
        '&page=' +
        this.state.pageSize1 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          let tempindex = this.state.popularUsersData.findIndex(
            (x) => x.id === removeUserId,
          );
          this.state.popularUsersData.splice(tempindex, 1);
          this.setState({
            popularUsersData: this.state.popularUsersData,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getInvitations = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/connectInvitations' +
        '?page=' +
        0 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({
            receivedInvitations: response.data.body.page.totalElements,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getMutualConnectList = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        this.state.userId +
        '/mutualConnects/' +
        this.state.otherUserId +
        '?page=' +
        this.state.pageSizeMutualConnects +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({
            mutualConnectList: this.state.mutualConnectList.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  loadMoreMutualConnectList = () => {
    this.setState(
      {pageSizeMutualConnects: this.state.pageSizeMutualConnects + 1},
      () => {
        this.getMutualConnectList();
      },
    );
  };

  changeState = (value) => {
    this.setState(value);
  };

  header = () => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('MyNetworkStack')}
          activeOpacity={0.5}
          style={styles.mycircle}>
          <View
            style={{
              marginLeft: 15,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon
              name="UserCircle"
              color={COLORS.altgreen_200}
              size={18}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 10}}
            />
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.white, marginLeft: 10},
              ]}>
              My Network
            </Text>
          </View>
          <View style={defaultShape.Nav_Gylph_Btn}>
            <View
              style={{
                height: 16,
                width: 16,
                borderRadius: 8,
                backgroundColor: COLORS.dark_500,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                name="Arrow_Right"
                color={COLORS.dark_700}
                size={10}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
            </View>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('NetworkInvitationStack')
          }
          activeOpacity={0.5}
          style={[styles.mycircle, {backgroundColor: '#D9E1E4'}]}>
          <View
            style={{
              marginLeft: 15,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon
              name="Mail_OL"
              color={COLORS.dark_600}
              size={18}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 10}}
            />
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 10},
              ]}>
              Invitations
            </Text>
          </View>
          <View
            style={{
              marginRight: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.grey_400, marginRight: 6, fontSize: 13},
              ]}>
              {this.state.receivedInvitations}
            </Text>
            <Icon
              name="Arrow_Right"
              color={COLORS.dark_700}
              size={14}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 7}}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  mutualConnectListModal = () => {
    return (
      <Modal
        visible={this.state.mutualConnectListModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({mutualConnectListModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[defaultShape.Modal_Categories_Container, {maxHeight: 400}]}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'column',
                  paddingBottom: 10,
                  paddingTop: 0,
                  borderBottomColor: COLORS.grey_350,
                },
              ]}>
              <Text
                style={[
                  defaultStyle.Subtitle_1,
                  {color: COLORS.dark_700, alignSelf: 'center'},
                ]}>
                Mutual Connects
              </Text>
            </View>

            <FlatList
              showsVerticalScrollIndicator={false}
              data={this.state.mutualConnectList}
              style={{width: '85%'}}
              keyExtractor={(item) => item.id}
              onEndReached={this.loadMoreMutualConnectList}
              onEndReachedThreshold={2}
              renderItem={(item) => (
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 10,
                    borderBottomColor: COLORS.grey_300,
                    borderBottomWidth: 0.5,
                    paddingBottom: 5,
                    alignItems: 'center',
                  }}>
                  <Image
                    source={
                      item.item.personalInfo.profileImage
                        ? {uri: item.item.personalInfo.profileImage}
                        : defaultProfile
                    }
                    style={{height: 30, width: 30, borderRadius: 15}}
                  />
                  <Text
                    style={[
                      defaultStyle.Caption,
                      {color: COLORS.altgreen_400, marginLeft: 10},
                    ]}>
                    {item.item.username}
                  </Text>
                </View>
              )}
            />
          </View>
        </View>
      </Modal>
    );
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({optionsModalOpen: false, currentUserType: ''})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            {this.state.currentUserType === 'popular' && (
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => this.followUser(this.state.otherUserId)}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="RSS"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    Follow
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            )}

            {this.state.currentUserType === 'popular' && (
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({optionsModalOpen: false}, () =>
                    this.sendConnectInvite(
                      this.state.otherUserId,
                      'popularUser',
                    ),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="AddUser"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                  <Text
                    style={[
                      defaultStyle.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    Connect
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            )}

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState(
                  {
                    optionsModalOpen: false,
                    mutualConnectListModalOpen: true,
                    mutualConnectList: [],
                    pageSizeMutualConnects: 0,
                  },
                  () => this.getMutualConnectList(),
                );
              }}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Mutual Connects
              </Text>
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.altgreen_300},
                ]}>
                {this.state.mutualConnectCount}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({optionsModalOpen: false}, () => {
                  this.props.navigation.navigate('Chats', {
                    userId: this.state.userId,
                    otherUserId: this.state.otherUserId,
                    lastActive: null,
                    grpType: 'Private',
                    name: this.state.pressedItem.username,
                    otherUserProfile:
                      this.state.pressedItem.personalInfo &&
                      this.state.pressedItem.personalInfo.profileImage
                        ? this.state.pressedItem.personalInfo.profileImage
                        : null,
                  });
                });
              }}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Send Message
              </Text>
              <Icon
                name="WN_Messeges_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            {this.state.currentUserType === 'popular' && (
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.verifyReported();
                  setTimeout(() => {
                    this.setState({
                      optionsModalOpen: false,
                      // reasonForReportingModalOpen: true,
                    });
                  }, 2000);
                }}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  Report User
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </Modal>
    );
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {height: 700, bottom: 0},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={[defaultShape.CloseBtn, {marginBottom: 0}]}
            onPress={() =>
              this.setState({
                reasonForReportingModalOpen: false,
                reasonForReporting: '',
                description: '',
              })
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.altgreen_100,
              alignItems: 'center',
              paddingTop: 15,
              paddingBottom: 10,
              // paddingLeft: 20,
              width: '90%',
              alignSelf: 'center',
              marginBottom: 30,
              marginTop: 15,
            }}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Reason for reporting
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingVertical: 15, borderBottomWidth: 0},
                    ]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'FAKE_SPAM_OR_SCAM'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Fake, spam or scam
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'ACCOUNT_MAY_BE_HACKED'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Account may be hacked
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'IMPERSONATING_SOMEONE'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'IMPERSONATING_SOMEONE' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  numberOfLines={2}
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Impersonating someone
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'VIOLATES_TERMS_OF_USE'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'VIOLATES_TERMS_OF_USE' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Violates Terms Of Use
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting: 'OTHERS',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'OTHERS' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Others
                </Text>
              </View>
            </TouchableOpacity>

            {this.state.reasonForReporting === 'OTHERS' && (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  placeholder="Write the details"
                  placeholderTextColor={COLORS.dark_600}
                  multiline
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Subtitle_1,
                    {
                      width: '90%',
                      height: 56,
                      backgroundColor: COLORS.altgreen_100,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) => this.setState({description: value})}
                  value={this.state.description}
                />
              </View>
            )}

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                this.state.reasonForReporting === ''
                  ? Snackbar.show({
                      backgroundColor: '#B22222',
                      text: 'Please select an option',
                      duration: Snackbar.LENGTH_LONG,
                    })
                  : this.state.reasonForReporting === 'OTHERS' &&
                    this.state.description.trim() === ''
                  ? Snackbar.show({
                      backgroundColor: '#B22222',
                      text: 'Please enter the detail',
                      duration: Snackbar.LENGTH_LONG,
                    })
                  : (this.handleReportAbuseSubmit(),
                    this.setState({reasonForReportingModalOpen: false}));
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 15,
                height: 27,
                marginVertical: 10,
                borderRadius: 16,
                textAlign: 'center',
                borderWidth: 1,
                borderColor: '#698F8A',
              }}>
              <Text
                style={{
                  color: '#698F8A',
                  fontSize: 14,
                  paddingHorizontal: 14,
                  paddingVertical: 20,
                  fontFamily: 'Montserrat-Medium',
                  fontWeight: 'bold',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.otherUserId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          !response.data.body.reported
            ? this.setState({
                isReported: response.data.body.reported,
                reasonForReportingModalOpen: true,
              })
            : Snackbar.show({
                backgroundColor: COLORS.primarydark,
                text: 'Your report request was already taken',
                textColor: COLORS.altgreen_100,
                duration: Snackbar.LENGTH_LONG,
              });
        } else {
          console.log(response);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.otherUserId,
      // entityType: this.state.entityType,
      entityType: 'USER',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });

    setTimeout(() => {
      this.setState({reasonForReporting: '', description: ''});
    }, 2000);
  };

  renderPopularUser = (item) => {
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate('ProfileStack', {
            screen: 'OtherProfileScreen',
            params: {userId: item.item.id},
          })
        }
        activeOpacity={0.9}
        style={{height: 139, width: 148, marginRight: 15}}>
        <View
          style={[
            defaultShape.card8_a,
            {
              height: 130,
              width: 148,
              position: 'absolute',
              bottom: 0,
              zIndex: 1,
            },
          ]}>
          <Image
            source={
              item.item.personalInfo.profileImage
                ? {uri: item.item.personalInfo.profileImage}
                : defaultProfile
            }
            style={{
              height: 72,
              width: 72,
              borderRadius: 36,
              position: 'absolute',
              left: 5,
              top: -15,
              zIndex: 2,
            }}
          />

          <TouchableOpacity
            onPress={() =>
              this.removePopularUsers(this.state.userId, item.item.id)
            }
            style={{
              position: 'absolute',
              alignItems: 'center',
              top: 0,
              right: 0,
              backgroundColor: COLORS.altgreen_100,
              height: 20,
              width: 20,
              borderTopRightRadius: 8,
              zIndex: 2,
            }}>
            <Icon
              name="Cross"
              color={COLORS.altgreen_400}
              size={8}
              style={{marginTop: 5}}
            />
          </TouchableOpacity>

          {item.item.connectDepth > 0 && (
            <View
              style={{
                paddingHorizontal: 10,
                paddingVertical: 4,
                backgroundColor: COLORS.white,
                position: 'absolute',
                top: 35,
                right: 0,
                zIndex: 2,
                borderTopLeftRadius: 4,
              }}>
              <Text
                style={[
                  defaultStyle.Note_2,
                  {color: COLORS.grey_400, fontSize: 12},
                ]}>
                {item.item.connectDepth === 1
                  ? '1st'
                  : item.item.connectDepth === 2
                  ? '2nd'
                  : '3rd'}
              </Text>
            </View>
          )}

          <Image
            source={
              item.item.personalInfo.coverImage
                ? {uri: item.item.personalInfo.coverImage}
                : defaultCover
            }
            style={{
              height: 48,
              width: 146,
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
            }}
          />
          <Text
            numberOfLines={1}
            style={[
              defaultStyle.Title_2,
              {color: '#002D3D', marginLeft: 10, marginTop: 10, fontSize: 13},
            ]}>
            {item.item.username}
          </Text>
          <Text
            style={[
              defaultStyle.Subtitle_2,
              {color: COLORS.altgreen_300, marginLeft: 10},
            ]}>
            {item.item.persona}
          </Text>
          {item.item.addressDetail && item.item.addressDetail.country && (
            <Text
              numberOfLines={1}
              style={[
                defaultStyle.Note_2,
                {color: COLORS.grey_400, marginLeft: 10, fontSize: 12},
              ]}>
              {item.item.addressDetail && item.item.addressDetail.country
                ? item.item.addressDetail.country
                : null}
            </Text>
          )}

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <Text
              style={[
                defaultStyle.Note_2,
                {color: COLORS.grey_400, marginLeft: 10, fontSize: 12},
              ]}>
              {item.item.followerCount} followers
            </Text>

            <TouchableOpacity
              onPress={() =>
                this.setState({
                  optionsModalOpen: true,
                  otherUserId: item.item.id,
                  mutualConnectCount: item.item.mutualConnectCount,
                  currentUserType: 'popular',
                  pressedItem: item.item,
                })
              }
              style={{
                height: 20,
                width: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon name="Kebab" size={10} color={COLORS.altgreen_400} />
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  removeRequest = (id) => {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/removeInvite/' +
        id,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          this.setState({
            requestedList: this.state.requestedList.filter(
              (item) => item !== id,
            ),
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderPeopleYouMayKnow = (item) => {
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate('ProfileStack', {
            screen: 'OtherProfileScreen',
            params: {userId: item.item.id},
          })
        }
        activeOpacity={0.9}
        style={styles.circleItem}>
        <TouchableOpacity
          onPress={() =>
            this.setState({
              optionsModalOpen: true,
              otherUserId: item.item.id,
              mutualConnectCount: item.item.mutualConnectCount,
              currentUserType: 'recommended',
            })
          }
          style={{
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLORS.altgreen_100 + '80',
            borderBottomRightRadius: 8,
            top: 0,
            left: 0,
            height: 20,
            width: 20,
            zIndex: 2,
          }}>
          <Icon name="Kebab" size={10} color={COLORS.dark_800} />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() =>
            this.removeRecommendedUsers(this.state.userId, item.item.id)
          }
          style={{
            position: 'absolute',
            alignItems: 'center',
            top: 0,
            right: 0,
            backgroundColor: COLORS.altgreen_100,
            height: 20,
            width: 20,
            borderTopRightRadius: 8,
            zIndex: 2,
          }}>
          <Icon
            name="Cross"
            color={COLORS.altgreen_400}
            size={8}
            style={{marginTop: 5}}
          />
        </TouchableOpacity>
        <Image
          source={
            item.item.personalInfo.coverImage
              ? {uri: item.item.personalInfo.coverImage}
              : defaultCover
          }
          style={{
            width: '100%',
            height: 80,
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
          }}
        />
        <Image
          source={
            item.item.personalInfo.profileImage
              ? {uri: item.item.personalInfo.profileImage}
              : defaultProfile
          }
          style={{
            width: 60,
            height: 60,
            borderRadius: 30,
            position: 'absolute',
            top: 33,
          }}
        />

        <Text
          numberOfLines={1}
          style={[
            defaultStyle.Body_1,
            {
              marginTop: 16,
              color: COLORS.dark_900,
              maxWidth: '90%',
              fontWeight: 'bold',
              fontSize: 13,
            },
          ]}>
          {item.item.username}
        </Text>

        {item.item.persona ? (
          <Text
            numberOfLines={1}
            style={[
              defaultStyle.Body_1,
              {marginTop: 2, color: COLORS.altgreen_300, maxWidth: '90%'},
            ]}>
            <Icon name="Projects_F" color={COLORS.altgreen_300} size={12} />
            {'  ' + item.item.persona}
          </Text>
        ) : (
          <Text style={{marginTop: 2}}></Text>
        )}
        {item.item.addressDetail && item.item.addressDetail.country && (
          <Text
            numberOfLines={1}
            style={[
              defaultStyle.Body_1,
              {marginTop: 2, color: COLORS.altgreen_300, maxWidth: '90%'},
            ]}>
            <Icon name="Location" color={COLORS.altgreen_300} size={12} />
            {'  ' + item.item.addressDetail && item.item.addressDetail.country
              ? item.item.addressDetail.country
              : null}
          </Text>
        )}

        <TouchableOpacity
          onPress={() => {
            this.state.requestedList.includes(item.item.id)
              ? this.removeRequest(item.item.id)
              : this.sendConnectInvite(item.item.id);
          }}
          // onPress={()=>this.removeRecommendedUsers(this.state.userId,item.item.id)}
          style={[
            styles.followBtn,
            {
              backgroundColor: this.state.requestedList.includes(item.item.id)
                ? COLORS.green_600
                : COLORS.green_400,
            },
          ]}
          activeOpacity={0.7}>
          {this.state.requestedList.includes(item.item.id) ? (
            <Text style={[defaultStyle.Caption, {color: COLORS.white}]}>
              Cancel Request
            </Text>
          ) : (
            <Text style={[defaultStyle.Caption, {color: COLORS.white}]}>
              Connect
            </Text>
          )}
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  followUser = (id) => {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        id,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          let tempindex = this.state.popularUsersData.findIndex(
            (x) => x.id === id,
          );
          this.state.popularUsersData.splice(tempindex, 1);
          this.setState({
            popularUsersData: this.state.popularUsersData,
            optionsModalOpen: false,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  sendConnectInvite = (id, userType) => {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/invite/' +
        id,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          console.log(response.status);
          this.setState({requestedList: [...this.state.requestedList, id]});
        }
      })
      .catch((err) => {
        if (err && err.message === 'Request failed with status code 409') {
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'You can send connection request after 3 days to this member',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });

    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/connectionStatus/' +
        id,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data.status === '200 OK') {
          console.log(response.data.body);
        }
      })
      .catch((err) => {
        console.log(err);
      });

    {
      userType && userType === 'popularUser'
        ? axios({
            method: 'get',
            url:
              REACT_APP_userServiceURL +
              '/network/' +
              this.state.userId +
              '/popularUsers?removeUserId=' +
              id +
              '&page=0&size=10',
            withCredentials: true,
          })
            .then((response) => {
              if (response) {
                console.log(response.data.status);
                let tempindex = this.state.popularUsersData.findIndex(
                  (x) => x.id === id,
                );
                this.state.popularUsersData.splice(tempindex, 1);
                this.setState({
                  popularUsersData: this.state.popularUsersData,
                });
              }
            })
            .catch((err) => {
              console.log(err);
            })
        : axios({
            method: 'get',
            url:
              REACT_APP_userServiceURL +
              '/network/' +
              this.state.userId +
              '/recommendedUsers?removeUserId=' +
              id +
              '&page=0&size=10',
            withCredentials: true,
          })
            .then((response) => {
              if (response) {
                console.log(response.data.status);
              }
            })
            .catch((err) => {
              console.log(err);
            });
    }
  };

  listFooterComponent = () => {
    return this.state.popularUsersData.length > 5 ? (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate('SeeAllPopularUsersStack', {
            screen: 'SeeAllPopularUsers',
            params: {popularUsersData: this.state.popularUsersData},
          })
        }
        activeOpacity={0.5}
        style={{
          marginHorizontal: 16,
          alignSelf: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          height: 26,
          width: 70,
          borderWidth: 1,
          borderColor: '#698F8A',
          borderRadius: 13,
          paddingHorizontal: 6,
          paddingVertical: 2,
        }}>
        <Text
          style={{
            color: '#698F8A',
            fontSize: 11,
            fontFamily: 'Montserrat-Medium',
          }}>
          See All
        </Text>
      </TouchableOpacity>
    ) : (
      <></>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.optionsModal()}
        {this.mutualConnectListModal()}
        {this.reasonForReportingModal()}
        <View>
          <SearchBar
            changeState={this.changeState}
            navigateNotification={this.navigateNotification}
            navigateProfile={this.navigateProfile}
            navigation={this.navigation}
          />
        </View>

        <View>
          <FlatList
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={
              <>
                {this.header()}

                <View
                  style={{
                    alignSelf: 'center',
                    flexDirection: 'row',
                    backgroundColor: COLORS.altgreen_t50,
                    width: 320,
                    marginTop: 20,
                    justifyContent: 'center',
                    borderRadius: 4,
                  }}>
                  <TouchableOpacity
                    style={[
                      defaultShape.InTab_Btn,
                      {
                        backgroundColor: '#fff',
                        width: '33.33%',
                        borderRadius: 4,
                      },
                    ]}>
                    <Text
                      style={[defaultStyle.Caption, {color: COLORS.dark_800}]}>
                      Connects
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('NetworkCompanies', {
                        invitations: this.state.receivedInvitations,
                      })
                    }
                    style={[
                      defaultShape.InTab_Btn,
                      {
                        backgroundColor: COLORS.altgreen_t50,
                        width: '33.33%',
                        borderRadius: 4,
                      },
                    ]}>
                    <Text
                      style={[
                        defaultStyle.Caption,
                        {color: COLORS.altgreen_400},
                      ]}>
                      Organization
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('NetworkCircles', {
                        invitations: this.state.receivedInvitations,
                      })
                    }
                    style={[
                      defaultShape.InTab_Btn,
                      {
                        backgroundColor: COLORS.altgreen_t50,
                        width: '33.33%',
                        borderRadius: 4,
                      },
                    ]}>
                    <Text
                      style={[
                        defaultStyle.Caption,
                        {color: COLORS.altgreen_400},
                      ]}>
                      Circles
                    </Text>
                  </TouchableOpacity>
                </View>

                {/****** Popular Users Starts ******/}

                <View
                  style={{
                    backgroundColor: COLORS.altgreen_200,
                    paddingBottom: 16,
                    paddingTop: 8,
                    paddingHorizontal: 15,
                    marginTop: 26,
                  }}>
                  <View
                    style={{
                      width: 75,
                      height: 19,
                      backgroundColor: COLORS.altgreen_300,
                      borderRadius: 2,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginTop: -16,
                      marginLeft: 10,
                      marginBottom: 10,
                    }}>
                    <Text
                      style={[defaultStyle.OVERLINE, {color: COLORS.white}]}>
                      POPULAR
                    </Text>
                  </View>

                  <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={this.state.popularUsersData.slice(0, 5)}
                    keyExtractor={(item) => item.id}
                    // onEndReached={this.handleLoadmore1}
                    contentContainerStyle={{alignItems: 'center'}}
                    ListFooterComponent={this.listFooterComponent}
                    // onEndReachedThreshold={2}
                    renderItem={(item) => this.renderPopularUser(item)}
                  />
                </View>

                {/****** Popular Users Ends ******/}

                <View
                  style={{
                    width: 174,
                    height: 19,
                    backgroundColor: COLORS.altgreen_300,
                    borderRadius: 2,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 20,
                    marginLeft: 20,
                  }}>
                  <Text style={[defaultStyle.OVERLINE, {color: COLORS.white}]}>
                    PEOPLE YOU MAY KNOW
                  </Text>
                </View>
              </>
            }
            ListHeaderComponentStyle={{width: '100%'}}
            data={this.state.recommendedUsers}
            keyExtractor={(item) => item.id}
            contentContainerStyle={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              paddingBottom: 20,
              justifyContent: 'center',
            }}
            onEndReached={this.handleLoadmore2}
            onEndReachedThreshold={2}
            renderItem={(item) => this.renderPeopleYouMayKnow(item)}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  mycircle: {
    width: '92%',
    height: 44,
    backgroundColor: COLORS.dark_700,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginTop: 15,
    alignItems: 'center',
    borderRadius: 8,
  },
  circleItem: {
    height: 183,
    width: 148,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    alignItems: 'center',
    marginTop: 15,
    marginHorizontal: 7,
  },
  followBtn: {
    paddingHorizontal: 10,
    height: 28,
    borderRadius: 4,
    marginTop: 8,
    backgroundColor: COLORS.green_400,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
