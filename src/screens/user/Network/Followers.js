import React, { Component } from 'react'
import {
  Clipboard,
  Share,
  Modal,
  ImageBackground,
  TextInput,
  Dimensions,
  FlatList,
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Platform,
  SafeAreaView,
} from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import LinearGradient from 'react-native-linear-gradient'
import axios from 'axios'
import Snackbar from 'react-native-snackbar'
import AsyncStorage from '@react-native-community/async-storage'
import { cloneDeep } from 'lodash'

import httpService from '../../../services/AxiosInterceptors'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultProfile from '../../../../assets/defaultProfile.png'
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json'
import { COLORS } from '../../../Components/Shared/Colors'
import typography from '../../../Components/Shared/Typography'
import defaultShape from '../../../Components/Shared/Shape'
import defaultStyle from '../../../Components/Shared/Typography'

httpService.setupInterceptors()
const Icon = createIconSetFromIcoMoon(icoMoonConfig)

const screenHeight = Dimensions.get('window').height

export default class Followers extends Component {
  constructor(props) {
    super(props)

    this.state = {
      users: [],
      selectedPage: '',
      name: '',
      searchText: '',
      searchIcon: true,
      userId: '',
      followers: [],
      circleData: {},
      postModalOpen: false,
      sortBy: 'firstName',
      optionsModalOpen: false,
      shareModalOpen: false,
      customUrl: '',
      pressedUserId: '',
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      followees: [],
      pageNumber: 0,
      pressedItem: {}
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login')
      }
    })

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({ userId: value })
        this.getFollowers(value)
        this.getFollowees(value)
      })
      .catch((e) => {
        console.log(e)
      })
  }

  handleLoadmore = () => {
    this.setState({ pageNumber: this.state.pageNumber + 1 }, () =>
      this.getFollowers(this.state.userId),
    );
  };

  searchNames = (query) => {

    if (query === '') {
      return this.state.followers
    }

    const regex = new RegExp(`${query.trim()}`, 'i')

    return this.state.followers.filter(item => item.username.search(regex) >= 0)
  }

  // searchNames = (query) => {
  //   const regex = new RegExp(`${query.trim()}`, 'i');

  //   if (this.state.sortBy === 'firstName' && this.state.followers) {
  //     return this.state.followers
  //       .sort((a, b) => a.username.toUpperCase() > b.username.toUpperCase())
  //       .filter((item) => item.username.search(regex) >= 0);
  //   } else if (this.state.sortBy === 'lastName' && this.state.followers) {
  //     return this.state.followers
  //       .sort(
  //         (a, b) =>
  //           a.username.toUpperCase().split(' ')[1] >
  //           b.username.toUpperCase().split(' ')[1],
  //       )
  //       .filter((item) => item.username.search(regex) >= 0);
  //   } else if (this.state.sortBy === 'recentlyAdded' && this.state.followers) {
  //     return this.state.followers
  //       .sort((a, b) => a.createTime < b.createTime)
  //       .filter((item) => item.username.search(regex) >= 0);
  //   } else
  //     return this.state.followers
  //       .sort((a, b) => a.username.toUpperCase() > b.username.toUpperCase())
  //       .filter((item) => item.username.search(regex) >= 0);
  //   // else if (this.state.sortBy === 'country' && this.state.followers) {
  //   //     let array = this.state.followers.sort((a, b) => {

  //   //         return (a.addressDetail && a.addressDetail.country === null) - (b.addressDetail && b.addressDetail.country === null)
  //   //     })
  //   //     let array2 = array.sort((a, b) => {

  //   //         return a.addressDetail.country && b.addressDetail.country ? a.addressDetail.country.toUpperCase() > b.addressDetail.country.toUpperCase() : a
  //   //     })
  //   //     return array2.filter(item => (item.username).search(regex) >= 0)
  //   // }
  // };

  postModal = () => {
    return (
      <Modal
        onRequestClose={() => this.setState({ postModalOpen: false })}
        visible={this.state.postModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={[defaultShape.Linear_Gradient_View, { bottom: 100 }]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ postModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <Text
            style={[
              typography.Caption,
              {
                fontSize: 12,
                color: COLORS.dark_500,
                textAlign: 'center',
                alignSelf: 'center',
                position: 'absolute',
                bottom: 110,
                zIndex: 2,
              },
            ]}>
            Sort By
          </Text>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              },
            ]}>
            <TouchableOpacity
              style={
                this.state.sortBy === 'firstName'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ postModalOpen: false, sortBy: 'firstName' });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'firstName'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                First Name
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                this.state.sortBy === 'lastName'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ postModalOpen: false, sortBy: 'lastName' });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'lastName'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Last Name
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                this.state.sortBy === 'recentlyAdded'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ postModalOpen: false, sortBy: 'recentlyAdded' });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'recentlyAdded'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Recently Added
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                this.state.sortBy === 'country'
                  ? styles.selectedText
                  : styles.unselectedText
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ postModalOpen: false, sortBy: 'country' });
              }}>
              <Text
                style={[
                  typography.Caption,
                  {
                    fontSize: 11,
                    color:
                      this.state.sortBy === 'country'
                        ? COLORS.altgreen_200
                        : COLORS.dark_500,
                    textAlign: 'center',
                    alignSelf: 'center',
                  },
                ]}>
                Country
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }

  getFollowers(userId) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/followers?requestingUserId=' +
        userId +
        '&page=0' +
        // this.state.pageNumber +
        '&size=1000',
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          console.log('my connects: ', response.data.body.content[0])
          this.setState({
            followers: response.data.body.content,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getFollowees(userId) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/following' +
        '?page=' +
        0 +
        '&size=500',
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          console.log('getFollowees', response.data.body.content[0]);
          this.setState({ followees: response.data.body.content });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  stickyHeader = () => {
    return (
      <View
        style={{
          backgroundColor: COLORS.white,
          position: 'absolute',
          top: 0,
          zIndex: 2,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: COLORS.white,
          }}>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: 10,
            }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => this.props.navigation.navigate('MyNetwork')}
                style={{
                  backgroundColor: COLORS.white,
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 42,
                  width: 42,
                  borderRadius: 21,
                  marginTop: 6,
                }}>
                <Icon
                  name="Arrow-Left"
                  size={16}
                  color="#00394D"
                  style={{ marginTop: 5 }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            justifyContent: 'space-evenly',
            width: 300,
            marginTop: -46,
          }}>
          <TouchableOpacity activeOpacity={0.8} style={{ width: 120 }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 32,
                  height: 32,
                  borderRadius: 16,
                  backgroundColor: COLORS.green_200,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: 4,
                }}>
                <Icon
                  name="Network_F"
                  size={14}
                  color={COLORS.dark_600}
                  style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
                />
              </View>
              <View style={{ height: 50, justifyContent: 'center' }}>
                <Text style={[typography.Button_2, { color: COLORS.dark_800 }]}>
                  Followers
                </Text>
                <Text
                  style={[typography.Subtitle_2, { color: COLORS.altgreen_300 }]}>
                  {this.state.followers.length}
                </Text>
              </View>
            </View>
            <View
              style={{
                width: 130,
                height: 5,
                backgroundColor: COLORS.dark_800,
                borderTopLeftRadius: 4,
                borderTopRightRadius: 4,
              }}></View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Following')}
            activeOpacity={0.8}
            style={{ width: 120 }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 32,
                  height: 32,
                  borderRadius: 16,
                  backgroundColor: COLORS.altgreen_200,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: 4,
                }}>
                <Icon
                  name="Add_Group"
                  size={14}
                  color={COLORS.dark_600}
                  style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
                />
              </View>
              <View style={{ height: 50, justifyContent: 'center' }}>
                <Text style={[typography.Button_2, { color: COLORS.dark_500 }]}>
                  Following
                </Text>
                <Text
                  style={[typography.Subtitle_2, { color: COLORS.altgreen_300 }]}>
                  {this.state.followees.length}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ optionsModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                Connect
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { paddingTop: 25, paddingBottom: 15 },
                  ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ optionsModalOpen: false }, () => {
                  this.props.navigation.navigate('Chats', {
                    userId: this.state.userId,
                    otherUserId: this.state.pressedItem.id,
                    lastActive: null,
                    grpType: 'Private',
                    name: this.state.pressedItem.username,
                    otherUserProfile:
                      this.state.pressedItem.personalInfo && this.state.pressedItem.personalInfo.profileImage
                        ? this.state.pressedItem.personalInfo.profileImage
                        : null,
                  })
                })
              }}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Chat
              </Text>
              <Icon
                name="Messeges_F"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { borderBottomWidth: 0 },
                  ]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.verifyReported()
                setTimeout(() => {
                  this.setState({
                  optionsModalOpen: false,
                  // reasonForReportingModalOpen: true,
                })
                }, 2000) 
              }
                
              }>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Report Connect
              </Text>
              <Icon
                name="ReportComment_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }

  onShare = async () => {
    try {
      const result = await Share.share({
        message: REACT_APP_domainUrl + '/profile/' + this.state.customUrl,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  }

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ shareModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { paddingTop: 25, paddingBottom: 15 },
                  ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ shareModalOpen: false });
              }}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl + '/profile/' + this.state.customUrl,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Copy link to profile
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ shareModalOpen: false });
              }}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Share through Mail
              </Text>
              <Icon
                name="Mail_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({ shareModalOpen: false }, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { borderBottomWidth: 0 },
                  ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              { height: 700, bottom: 0 },
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={[defaultShape.CloseBtn, { marginBottom: 0 }]}
            onPress={() =>
              this.setState({
                reasonForReportingModalOpen: false,
                reasonForReporting: '',
              })
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
            />
          </TouchableOpacity>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.altgreen_100,
              alignItems: 'center',
              paddingTop: 15,
              paddingBottom: 10,
              // paddingLeft: 20,
              width: '90%',
              alignSelf: 'center',
              marginBottom: 30,
              marginTop: 15,
            }}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                Reason for reporting
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { paddingVertical: 15, borderBottomWidth: 0 },
                  ]
                  : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ reasonForReporting: 'FAKE_SPAM_OR_SCAM' });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Fake, spam or scam
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ reasonForReporting: 'ACCOUNT_MAY_BE_HACKED' });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Account may be hacked
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ reasonForReporting: 'IMPERSONATING_SOMEONE' });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'IMPERSONATING_SOMEONE' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  numberOfLines={2}
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Impersonating someone
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ reasonForReporting: 'VIOLATES_TERMS_OF_USE' });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'VIOLATES_TERMS_OF_USE' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Violates Terms Of Use
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting: 'OTHERS'
                })
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'OTHERS' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    { color: COLORS.dark_600, marginLeft: 8 },
                  ]}>
                  Others
                </Text>
              </View>
            </TouchableOpacity>

            {this.state.reasonForReporting === 'OTHERS' &&
              <View style={{
                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 6, paddingRight: 12
              }}>

                <TextInput
                  theme={{ colors: { text: COLORS.dark_700, primary: COLORS.altgreen_300, placeholder: COLORS.altgreen_300 } }}
                  placeholder="Write the details"
                  placeholderTextColor={COLORS.dark_600}
                  multiline
                  selectionColor='#C8DB6E'
                  style={[defaultStyle.Subtitle_1, { width: '90%', height: 56, backgroundColor: COLORS.altgreen_100, color: COLORS.dark_700 }]}
                  onChangeText={(value) => this.setState({ description: value })}
                  value={this.state.description}
                />

              </View>}

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                this.state.reasonForReporting === '' ?
                  Snackbar.show({
                    backgroundColor: '#B22222',
                    text: "Please select an option",
                    duration: Snackbar.LENGTH_LONG,
                  })
                  :
                  this.state.reasonForReporting === 'OTHERS' && this.state.description.trim() === '' ?
                    Snackbar.show({
                      backgroundColor: '#B22222',
                      text: "Please enter the detail",
                      duration: Snackbar.LENGTH_LONG,
                    })
                    :
                    (this.handleReportAbuseSubmit(), this.setState({ reasonForReportingModalOpen: false }))
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 15,
                height: 27,
                marginVertical: 10,
                borderRadius: 16,
                textAlign: 'center',
                borderWidth: 1,
                borderColor: '#698F8A',
              }}>
              <Text
                style={{
                  color: '#698F8A',
                  fontSize: 14,
                  paddingHorizontal: 14,
                  paddingVertical: 20,
                  fontFamily: 'Montserrat-Medium',
                  fontWeight: 'bold',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }

  verifyReported = () => {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/verifyAlreadyReported?reporterId='
        + this.state.userId + '&entityId=' + this.state.pressedUserId,
      withCredentials: true
    }).then(response => {
      if (response && response.status === 200 && response.data && response.data.body) {
        // console.log(response.data.body.reported)
        !response.data.body.reported ? 
        this.setState({ isReported: response.data.body.reported, reasonForReportingModalOpen: true })
        :
        Snackbar.show({
          backgroundColor: COLORS.primarydark,
          text: "Your report request was already taken",
          textColor: COLORS.altgreen_100,
          duration: Snackbar.LENGTH_LONG,
        })

      } else {
        console.log(response)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedUserId,
      // entityType: this.state.entityType,
      entityType: 'USER',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
      setTimeout(() => {
        this.setState({ reasonForReporting: '', description: '' })
      }, 2000)
  }

  handleFollow = (userId, index) => {
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/follows/' + userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.status === 202) {
        console.log('handleFollow-----', response.status)
        // this.getFollowers(this.state.userId)
        // let tempFollowers = cloneDeep(this.state.followers)
        // tempFollowers[index].followed = true
        //   tempFollowers[index].followed = true
        // this.setState({ followers: tempFollowers })
      } else {
        console.log('handleFollow-----', response)
      }
    }).catch((err) => {
      console.log('handleFollow-----', err)
    })
    let tempFollowers = cloneDeep(this.state.followers)
    tempFollowers[index].followed = true
    tempFollowers[index].followed = true
    this.setState({ followers: tempFollowers })
  }

  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: '#F7F7F5',
          height: '107%',
          position: 'absolute',
          bottom: -50,
          width: '100%',
        }}>
        {this.stickyHeader()}
        {this.postModal()}
        {this.optionsModal()}
        {this.shareModal()}
        {this.reasonForReportingModal()}

        <View
          style={{
            height: 56,
            backgroundColor: COLORS.bgFill_200,
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            marginBottom: 6,
            marginTop: 90,
          }}>
          <TextInput
            style={styles.textInput}
            placeholderTextColor="#D9E1E4"
            onChangeText={(value) => {
              this.setState({ searchText: value });
            }}
            color="#154A59"
            placeholder="Search"
            onFocus={() => this.setState({ searchIcon: false })}
            onBlur={() => this.setState({ searchIcon: true })}
            underlineColorAndroid="transparent"
            ref={(input) => {
              this.textInput = input;
            }}
          />

          {/* <TouchableOpacity
            onPress={() => this.setState({ postModalOpen: true })}>
            <Icon
              name="FilterSlide"
              size={18}
              color={COLORS.altgreen_400}
              style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
            />
          </TouchableOpacity> */}
        </View>

        
          <FlatList
            keyboardShouldPersistTaps="handled"
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: 80, paddingTop: 10 }}
            keyExtractor={(item) => item.id}
            data={this.searchNames(this.state.searchText)}
            // data={this.state.followers}
            initialNumToRender={10}
            // onEndReached={this.handleLoadmore}
            // onEndReachedThreshold={1}
            renderItem={({ item, index }) => (
              // <View style={{ marginVertical: 4, alignItems: 'center', width: '100%', justifyContent: 'space-between', backgroundColor: 'red' }}

              // >
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: { userId: item.id },
                  })
                }
                activeOpacity={0.7}
                style={styles.item}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image
                    source={
                      item.personalInfo.profileImage
                        ? { uri: item.personalInfo.profileImage }
                        : defaultProfile
                    }
                    style={[styles.image, { marginLeft: '6%' }]}
                  />

                  <View style={[styles.nameMsg]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Text style={styles.name} numberOfLines={1}>
                        {item.username.charAt(0).toUpperCase() +
                          item.username.slice(1)}</Text>


                      <Text style={[{ color: '#888', fontSize: 14, marginLeft: 4 }, typography.Note2]}>
                        {item.connectStatus === "PENDING_CONNECT" ? 'Pending' :

                          item.connectDepth === 1 ? '• 1st' :
                            item.connectDepth === 2 ? '• 2nd' :
                              item.connectDepth === -1 || item.connectDepth === 0 ? '' : ""
                        }
                      </Text>

                    </View>

                    {item.persona ? (
                      <Text numberOfLines={1}
                        style={[typography.Note2, { color: COLORS.altgreen_300, maxWidth: '80%' }]}>
                        {item.persona}
                      </Text>
                    ) : (
                      <></>
                    )}

                    {item.followerCount ? (
                      <Text
                        style={[typography.Note2, { color: '#888' }]}>
                        {item.followerCount} followers
                      </Text>
                    ) : (
                      <></>
                    )}

                    {item.mutualConnectCount ? (
                      <Text
                        style={[typography.Note2, { color: '#888' }]}>
                        {item.mutualConnectCount} mutual connects
                      </Text>
                    ) : (
                      <></>
                    )}

                  </View>

                  {/* <View style={[{ marginRight: 4 }]}>
                    <Text
                      style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>
                      Since
                    </Text>

                    <Text
                      style={[typography.Note2, { color: COLORS.altgreen_300 }]}>
                      18 Jan 2020
                    </Text>
                  </View> */}

                  <View style={{ flexDirection: 'row', marginLeft: -20 }}>
                    {!item.followed ?
                      <TouchableOpacity
                        onPress={() => this.handleFollow(item.id, index)}
                        style={{
                          width: 34,
                          height: 34,
                          borderRadius: 17,
                          backgroundColor: COLORS.altgreen_200,
                          justifyContent: 'center',
                          alignItems: 'center',
                          marginRight: 2,
                        }}
                        activeOpacity={0.5}>
                        <Icon
                          name="AddUser"
                          size={14}
                          color={COLORS.dark_600}
                          style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
                        />
                      </TouchableOpacity> : <></>}
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          optionsModalOpen: true,
                          pressedUserId: item.id,
                          customUrl: item.customUrl,
                          pressedUserName: item.username,
                          pressedItem: item
                        })
                      }
                      style={{
                        width: 34,
                        height: 34,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      activeOpacity={0.5}>
                      <Icon
                        name="Kebab"
                        size={14}
                        color={COLORS.grey_350}
                        style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </TouchableOpacity>

              // </View>
            )}
          />

      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    textAlign: 'left',
    fontSize: 15,
    position: 'absolute',
    top: 30,
    height: screenHeight,
    backgroundColor: '#fff',
  },
  unselectedText: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLORS.altgreen_300,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 10,
    borderRadius: 20,
  },
  selectedText: {
    backgroundColor: COLORS.dark_700,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 20,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  updateButton: {
    flexDirection: 'row',
    width: 110,
    height: 28,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: COLORS.dark_700,
  },
  requestEndorsementText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    color: '#698F8A',
    textAlign: 'center',
  },
  connectsSelectedText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: '#698F8A',
    textAlign: 'center',
    marginVertical: 10,
  },
  crossIcon2: {
    marginTop: Platform.OS === 'android' ? 10 : 0,
  },
  crossButtonContainer: {
    alignSelf: 'center',
    width: 42,
    height: 42,
    borderRadius: 21,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E7F3E3',
    marginBottom: 10,
  },
  linearGradientView2: {
    width: '100%',
    height: 200,
    position: 'absolute',
    top: -50,
    alignSelf: 'center',
  },
  linearGradient2: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  },
  sendRequest: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 15,
    color: '#E7F3E3',
    marginLeft: 6,
  },
  floatingIcon: {
    //flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 175,
    height: 39,
    borderRadius: 28,
    backgroundColor: '#367681',
    position: 'absolute',
    //top: 416,
    bottom: 66,
    right: 12,
  },
  editIcon: {
    alignSelf: 'center',
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 6 : 0,
    // position: 'absolute',
    // right: -10,
    // bottom: -16,
    // alignSelf: 'flex-end'
  },
  selectedName: {
    fontSize: 11,
    color: '#154A59',
    textAlign: 'center',
    marginHorizontal: 6,
    fontFamily: 'Montserrat-Medium',
  },
  searchIcon: {
    position: 'absolute',
    left: 136,
    top: 15,
    // backgroundColor: 'red',
    zIndex: 2,
  },
  border: {
    borderWidth: 1,
    marginRight: '6%',
    width: '95%',
    marginTop: '-1%',
    borderColor: '#91B3A2',
    alignSelf: 'center',
    borderRadius: 1,
  },
  textInput: {
    fontSize: 17,
    fontFamily: 'Montserrat-Medium',
    padding: 10,
    backgroundColor: '#FFF',
    width: '90%',
    height: 46,
    zIndex: 1,
    textAlign: 'center',
    borderRadius: 8,
    borderColor: '#D9E1E4',
    borderWidth: 1,
    shadowColor: '#36768140',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  tabNavigator: {
    marginTop: '-8%',
  },

  item: {
    // flex: 1,
    flexDirection: 'row',
    marginVertical: 12,
    backgroundColor: '#F7F7F500',
    width: '100%',
    height: 48,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'center',
    // backgroundColor: 'pink',
    paddingRight: 8
    // borderBottomWidth: 1,
    //borderBottomRadius: 1,
    // borderBottomColor: '#E2E7E9',
    // borderRadius: 10
  },
  image: {
    height: 28,
    width: 28,
    borderRadius: 14,
    //marginLeft: '7%',
  },
  name: {
    fontWeight: '700',
    color: '#4B4F56',
    fontSize: 13,
    textAlign: 'left',
    // backgroundColor: 'pink',
    maxWidth: 180,
  },
  message: {
    color: '#698F8A',
    fontSize: 10.5,
  },

  // --- New ---

  time: {
    color: '#AABCC3',
    fontSize: 10,
    marginRight: '10%',
  },
  nameMsg: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: '5%',
    //textAlign: 'left'
  },
  imageGroup: {
    height: 26,
    width: 26,
    borderRadius: 13,
  },
});
