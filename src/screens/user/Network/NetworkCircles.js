import React, {Component} from 'react';
import {
  ScrollView,
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

import SearchBar from '../../../Components/User/SearchBar';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import circleDefault from '../../../../assets/CirclesDefault.png';
import defaultCover from '../../../../assets/defaultCover.png';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class Circles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      userId: '',
      suggestedCircleData: [],
      pageSize: 10,
      followbtnid: '',
      userType: '',
      notification: false,
      redirectToProfile: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({userId: value});

        this.getSuggestedCircle(value);

        axios({
          method: 'get',
          url:
            REACT_APP_userServiceURL +
            '/profile/get?id=' +
            value +
            '&otherUserId=' +
            '',
          cache: true,
          withCredentials: true,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => {
            this.setState({userType: response.data.body.type});
            // console.log('--- response.data.body ---', response.data.body)
          })
          .catch((err) => {
            // console.log("Profile data error : ", err)
          });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  navigation = (value, params) => {
    return this.props.navigation.navigate(value, params);
  };

  getSuggestedCircle = (userId) => {
    let postBody = {
      userId: userId,
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/circle/unrelated-recommended?page=0' +
        '&size=' +
        this.state.pageSize,
      headers: {'Content-Type': 'application/json'},
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({
            isLoading: false,
            suggestedCircleData: response.data.body.content,
          });
          // console.log("circle suggesstion: ", response.data.body.content)
        }
      })
      .catch((err) => {
        this.setState({isLoading: false});
        console.log(err);
      });
  };

  handleFollowCircle = (id) => {
    this.setState({isLoading: true, followbtnid: id});
    let postBody = {
      userId: this.state.userId,
      entityId: id,
      entityType: 'CIRCLE',
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/entity/follow',
      data: postBody,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          this.getSuggestedCircle(this.state.userId);
          this.setState({isLoading: false});
        } else {
          this.setState({isLoading: false});
        }
      })
      .catch((err) => {
        this.setState({isLoading: false});
        console.log(err);
      });
  };

  handleLoadmore = () => {
    // console.log('object')
    this.setState({pageSize: this.state.pageSize + 10}, () =>
      this.getSuggestedCircle(this.state.userId),
    );
  };

  renderCircleItem = (item) => {
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate('CircleProfileStack', {
            screen: 'CircleProfile',
            params: {slug: item.item.slug},
          })
        }
        activeOpacity={0.8}
        style={styles.circleItem}>
        <Image
          source={
            item.item.resizedCoverImages
              ? {uri: item.item.resizedCoverImages.compressed}
              : defaultCover
          }
          style={{
            width: '100%',
            height: 80,
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
          }}
        />
        <Image
          source={
            item.item.profileImage
              ? {uri: item.item.profileImage}
              : circleDefault
          }
          style={{
            width: 60,
            height: 60,
            borderRadius: 30,
            position: 'absolute',
            top: 33,
          }}
        />

        <Text
          numberOfLines={1}
          style={[
            defaultStyle.Body_1,
            {marginTop: 20, color: COLORS.dark_900, maxWidth: '90%'},
          ]}>
          {item.item.title}
        </Text>

        <TouchableOpacity
          onPress={() => this.handleFollowCircle(item.item.id)}
          style={styles.followBtn}>
          {this.state.isLoading && item.item.id === this.state.followbtnid ? (
            <ActivityIndicator color={COLORS.white} size="small" />
          ) : (
            <Text style={[defaultStyle.Caption, {color: COLORS.white}]}>
              Follow
            </Text>
          )}
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  header = () => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('MyNetworkStack')}
          activeOpacity={0.5}
          style={styles.mycircle}>
          <View
            style={{
              marginLeft: 15,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon
              name="UserCircle"
              color={COLORS.altgreen_200}
              size={18}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 10}}
            />
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.white, marginLeft: 10},
              ]}>
              My Network
            </Text>
          </View>
          <View style={defaultShape.Nav_Gylph_Btn}>
            <View
              style={{
                height: 16,
                width: 16,
                borderRadius: 8,
                backgroundColor: COLORS.dark_500,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                name="Arrow_Right"
                color={COLORS.dark_700}
                size={10}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
            </View>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('NetworkInvitationStack')
          }
          activeOpacity={0.5}
          style={[styles.mycircle, {backgroundColor: '#D9E1E4'}]}>
          <View
            style={{
              marginLeft: 15,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon
              name="Mail_OL"
              color={COLORS.dark_600}
              size={18}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 10}}
            />
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 10},
              ]}>
              Invitations
            </Text>
          </View>
          <View
            style={{
              marginRight: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.grey_400, marginRight: 6, fontSize: 13},
              ]}>
              {this.props.route.params.invitations}
            </Text>
            <Icon
              name="Arrow_Right"
              color={COLORS.dark_700}
              size={14}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 7}}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.notification) {
      this.setState({notification: false}, () =>
        this.props.navigation.navigate('Notification'),
      );
    }
    // if (this.state.redirectToProfile) {

    //   if (this.state.userType === 'INDIVIDUAL') {
    //     this.setState({ redirectToProfile: false }, () => this.props.navigation.navigate('ProfileStack'))
    //   }

    //   if (this.state.userType === 'COMPANY') {
    //     this.setState({ redirectToProfile: false }, () => this.props.navigation.navigate('ProfileStack', {
    //       screen: 'CompanyProfileScreen'
    //     }))
    //   }
    // }
  }

  changeState = (value) => {
    this.setState(value);
  };

  navigateProfile = () => {
    if (this.state.userType === 'INDIVIDUAL') {
      this.props.navigation.navigate('ProfileStack');
    } else if (this.state.userType === 'COMPANY') {
      this.props.navigation.navigate('ProfileStack', {
        screen: 'CompanyProfileScreen',
      });
    }
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {/******** Header starts ********/}

        <View>
          <SearchBar
            changeState={this.changeState}
            navigateProfile={this.navigateProfile}
            navigation={this.navigation}
          />
        </View>

        <ScrollView>
          {this.header()}

          {/******** Header ends ********/}

          <View
            style={{
              alignSelf: 'center',
              flexDirection: 'row',
              backgroundColor: COLORS.altgreen_t50,
              width: 320,
              marginTop: 20,
              justifyContent: 'center',
              borderRadius: 4,
            }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('NetworkConnects')}
              style={[
                defaultShape.InTab_Btn,
                {
                  backgroundColor: COLORS.altgreen_t50,
                  width: '33.33%',
                  borderRadius: 4,
                },
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Connects
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('NetworkCompanies', {
                  invitations: this.props.route.params.invitations,
                })
              }
              style={[
                defaultShape.InTab_Btn,
                {
                  backgroundColor: COLORS.altgreen_t50,
                  width: '33.33%',
                  borderRadius: 4,
                },
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Organization
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[
                defaultShape.InTab_Btn,
                {backgroundColor: '#fff', width: '33.33%', borderRadius: 4},
              ]}>
              <Text style={[defaultStyle.Caption, {color: COLORS.dark_800}]}>
                Circles
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: 162,
              height: 19,
              backgroundColor: COLORS.altgreen_300,
              borderRadius: 2,
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 20,
              marginLeft: 20,
            }}>
            <Text style={[defaultStyle.OVERLINE, {color: COLORS.white}]}>
              CIRCLES YOU MAY LIKE
            </Text>
          </View>

          <View style={{width: '90%', alignSelf: 'center'}}>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={this.state.suggestedCircleData}
              keyExtractor={(item) => item.id}
              contentContainerStyle={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                paddingBottom: 100,
                justifyContent: 'center',
              }}
              // ListHeaderComponent={this.headerFlatlist}
              // ListHeaderComponentStyle={{ width: '100%' }}
              onEndReached={this.handleLoadmore}
              onEndReachedThreshold={2}
              renderItem={(item) => this.renderCircleItem(item)}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    height: 64,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  mycircle: {
    width: '100%',
    height: 44,
    backgroundColor: COLORS.dark_700,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginTop: 15,
    alignItems: 'center',
    borderRadius: 8,
  },
  searchbar: {
    width: '90%',
    height: 40,
    borderRadius: 8,
    backgroundColor: COLORS.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.8,
    borderColor: COLORS.grey_300,
  },
  circleItem: {
    height: 163,
    width: 148,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    alignItems: 'center',
    marginTop: 15,
    marginHorizontal: 7,
  },
  followBtn: {
    width: 78,
    height: 28,
    borderRadius: 4,
    marginTop: 8,
    backgroundColor: COLORS.green_400,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mycircle: {
    width: '92%',
    height: 44,
    backgroundColor: COLORS.dark_700,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginTop: 15,
    alignItems: 'center',
    borderRadius: 8,
  },
});
