import React, {Component} from 'react';
import {
  ScrollView,
  Image,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  SafeAreaView,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import SearchBar from '../../../Components/User/SearchBar';
import defaultCover from '../../../../assets/defaultCover.png';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultStyle from '../../../Components/Shared/Typography';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class NetworkCompanies extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notification: false,
      redirectToProfile: false,
      organizationList: [],
      pageSize: 0,
      userType: '',
      notification: false,
      redirectToProfile: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({userId: value});
        this.getBusinessPages(value);

        axios({
          method: 'get',
          url:
            REACT_APP_userServiceURL +
            '/profile/get?id=' +
            value +
            '&otherUserId=' +
            '',
          cache: true,
          withCredentials: true,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => {
            this.setState({userType: response.data.body.type});
            // console.log('--- response.data.body ---', response.data.body)
          })
          .catch((err) => {
            // console.log("Profile data error : ", err)
          });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  navigation = (value, params) => {
    return this.props.navigation.navigate(value, params);
  };

  changeState = (value) => {
    this.setState(value);
  };

  header = () => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('MyNetworkStack')}
          activeOpacity={0.5}
          style={styles.mycircle}>
          <View
            style={{
              marginLeft: 15,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon
              name="UserCircle"
              color={COLORS.altgreen_200}
              size={18}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 10}}
            />
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.white, marginLeft: 10},
              ]}>
              My Network
            </Text>
          </View>
          <View style={defaultShape.Nav_Gylph_Btn}>
            <View
              style={{
                height: 16,
                width: 16,
                borderRadius: 8,
                backgroundColor: COLORS.dark_500,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                name="Arrow_Right"
                color={COLORS.dark_700}
                size={10}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
            </View>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('NetworkInvitationStack')
          }
          activeOpacity={0.5}
          style={[styles.mycircle, {backgroundColor: '#D9E1E4'}]}>
          <View
            style={{
              marginLeft: 15,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon
              name="Mail_OL"
              color={COLORS.dark_600}
              size={18}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 10}}
            />
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.dark_600, marginLeft: 10},
              ]}>
              Invitations
            </Text>
          </View>
          <View
            style={{
              marginRight: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text
              style={[
                defaultStyle.Button_Lead,
                {color: COLORS.grey_400, marginRight: 6, fontSize: 13},
              ]}>
              {this.props.route.params.invitations}
            </Text>
            <Icon
              name="Arrow_Right"
              color={COLORS.dark_700}
              size={14}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 7}}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  handleLoadmore = () => {
    this.setState({pageSize: this.state.pageSize + 1}, () =>
      this.getBusinessPages(this.state.userId),
    );
  };

  getBusinessPages(userId) {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/unrelated-recommended-business-pages?page=' +
        this.state.pageSize +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        // console.log('getBusinessPages', response.data.body.content[0])
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({
            organizationList: this.state.organizationList.concat(
              response.data.body.content,
            ),
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  followUser = (id) => {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        id,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          this.getBusinessPages(this.state.userId);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderCompanyItem = (item) => {
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate('ProfileStack', {
            screen: 'CompanyProfileScreen',
            params: {userId: item.item.id},
          })
        }
        activeOpacity={0.9}
        style={styles.circleItem}>
        <Image
          source={
            item.item.personalInfo.coverImage
              ? {uri: item.item.personalInfo.coverImage}
              : defaultCover
          }
          style={{
            width: '100%',
            height: 65,
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
          }}
        />
        <Image
          source={
            item.item.personalInfo.profileImage
              ? {uri: item.item.personalInfo.profileImage}
              : defaultBusiness
          }
          style={{
            width: 60,
            height: 60,
            borderRadius: 6,
            position: 'absolute',
            top: 14,
          }}
        />

        <Text
          numberOfLines={1}
          style={[
            defaultStyle.Body_1,
            {
              marginTop: 16,
              color: COLORS.dark_900,
              maxWidth: '90%',
              fontWeight: 'bold',
              fontSize: 13,
            },
          ]}>
          {item.item.username}
        </Text>

        <Text
          numberOfLines={1}
          style={[
            defaultStyle.Body_1,
            {marginTop: 2, color: COLORS.dark_900, maxWidth: '90%'},
          ]}>
          {item.item.addressDetail && item.item.addressDetail.country
            ? item.item.addressDetail.country
            : null}
        </Text>

        <TouchableOpacity
          onPress={() => this.followUser(item.item.id)}
          style={styles.followBtn}
          activeOpacity={0.7}>
          <Text style={[defaultStyle.Caption, {color: COLORS.white}]}>
            Follow
          </Text>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  navigateProfile = () => {
    if (this.state.userType === 'INDIVIDUAL') {
      this.props.navigation.navigate('ProfileStack');
    } else if (this.state.userType === 'COMPANY') {
      this.props.navigation.navigate('ProfileStack', {
        screen: 'CompanyProfileScreen',
      });
    }
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View>
          <SearchBar
            changeState={this.changeState}
            navigateProfile={this.navigateProfile}
            navigation={this.navigation}
          />
        </View>

        <ScrollView>
          {this.header()}

          <View
            style={{
              alignSelf: 'center',
              flexDirection: 'row',
              backgroundColor: COLORS.altgreen_t50,
              width: 320,
              marginTop: 20,
              justifyContent: 'center',
              borderRadius: 4,
            }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('NetworkConnects')}
              style={[
                defaultShape.InTab_Btn,
                {
                  backgroundColor: COLORS.altgreen_t50,
                  width: '33.33%',
                  borderRadius: 4,
                },
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Connects
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                defaultShape.InTab_Btn,
                {backgroundColor: '#fff', width: '33.33%', borderRadius: 4},
              ]}>
              <Text style={[defaultStyle.Caption, {color: COLORS.dark_800}]}>
                Organization
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('NetworkCircles', {
                  invitations: this.props.route.params.invitations,
                })
              }
              style={[
                defaultShape.InTab_Btn,
                {
                  backgroundColor: COLORS.altgreen_t50,
                  width: '33.33%',
                  borderRadius: 4,
                },
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Circles
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: 121,
              height: 19,
              backgroundColor: COLORS.altgreen_300,
              borderRadius: 2,
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 20,
              marginLeft: 20,
            }}>
            <Text style={[defaultStyle.OVERLINE, {color: COLORS.white}]}>
              TOP ORGANIZATION
            </Text>
          </View>

          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.organizationList}
            keyExtractor={(item) => item.id}
            contentContainerStyle={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              paddingBottom: 20,
              justifyContent: 'center',
            }}
            onEndReached={this.handleLoadmore}
            onEndReachedThreshold={2}
            renderItem={(item) => this.renderCompanyItem(item)}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  mycircle: {
    width: '92%',
    height: 44,
    backgroundColor: COLORS.dark_700,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginTop: 15,
    alignItems: 'center',
    borderRadius: 8,
  },
  circleItem: {
    height: 183,
    width: 148,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    alignItems: 'center',
    marginTop: 15,
    marginHorizontal: 7,
  },
  followBtn: {
    width: 78,
    height: 28,
    borderRadius: 4,
    marginTop: 8,
    backgroundColor: COLORS.green_400,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
