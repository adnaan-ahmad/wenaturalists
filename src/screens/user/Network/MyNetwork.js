import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Platform,
  TouchableOpacity,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {connect} from 'react-redux';

import {userCircleRequest} from '../../../services/Redux/Actions/User/CircleActions';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import {COLORS} from '../../../Components/Shared/Colors';
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader';
import defaultShape from '../../../Components/Shared/Shape';
import typography from '../../../Components/Shared/Typography';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

class MyNetwork extends Component {
  constructor(props) {
    super(props);
    this.state = {
      connects: [],
      company: [],
      sentInvitations: []
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login')
      }
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        value && this.setState({userId: value})
        this.getConnects(value)
        this.getFollowingCompanies(value)
        this.getRequestedInvitations(value)
        this.props.userCircleRequest({userId: value, otherUserId: ''})
      })
      .catch((e) => {
        console.log(e)
      })
  }

  goback = () => {
    this.props.navigation.goBack()
  }

  getConnects(userId) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/connections?requestingUserId=' +
        userId +
        '&page=' +
        0 +
        '&size=500',
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log('my connects: ', response.data.body.content)
          this.setState({connects: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getFollowingCompanies(userId) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/followingCompanies?page=' +
        0 +
        '&size=500',
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.setState({company: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err)
      });
  }

  getRequestedInvitations(userId) {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/network/' + userId + '/requestedInvitations'
        + "?page=" + 0 + "&size=" + 1000,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.message === 'Success!') {
        this.setState({ sentInvitations: response.data.body.content })
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {/***** Header starts *****/}

        <ProfileEditHeader
          name="My Network"
          iconName="UserCircle"
          goback={this.goback}
        />

        {/***** Header ends *****/}

        <View style={{alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('MyConnects')}
            style={[styles.connectsView, {marginTop: 30}]}
            activeOpacity={0.8}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  defaultShape.AdjunctBtn_Prim,
                  {backgroundColor: COLORS.green_500},
                ]}>
                <Icon
                  name="Network_F"
                  size={18}
                  color={COLORS.dark_700}
                  style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                />
              </View>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8},
                ]}>
                Connected
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 5},
                ]}>
                {this.state.connects.length}
              </Text>
              <Icon
                name="Arrow_Right"
                size={18}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('YourRequests')}
            style={[styles.connectsView, { }]}
            activeOpacity={0.8}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  defaultShape.AdjunctBtn_Prim,
                  {backgroundColor: COLORS.altgreen_300},
                ]}>
                <Icon
                  name="AddUser"
                  size={18}
                  color={COLORS.altgreen_50}
                  style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                />
              </View>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8},
                ]}>
                Your Requests
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 5},
                ]}>
                {this.state.sentInvitations.length}
              </Text>
              <Icon
                name="Arrow_Right"
                size={18}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
            </View>
          </TouchableOpacity>

          <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
            <TouchableOpacity
              style={styles.connectsHalfView}
              onPress={() => this.props.navigation.navigate('Followers')}
              activeOpacity={0.8}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={[
                    defaultShape.AdjunctBtn_Prim,
                    {backgroundColor: COLORS.green_200},
                  ]}>
                  <Icon
                    name="TickedUser"
                    size={18}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                </View>
                <View>
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    People
                  </Text>
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    following you
                  </Text>
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={[styles.connectsHalfView, {marginLeft: 10}]}
              onPress={() => this.props.navigation.navigate('Following')}
              activeOpacity={0.8}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={[
                    defaultShape.AdjunctBtn_Prim,
                    {backgroundColor: COLORS.altgreen_200},
                  ]}>
                  <Icon
                    name="Add_Group"
                    size={18}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                </View>
                <View>
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    People
                  </Text>
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    you follow
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('MyOrganization')}
            style={styles.connectsView}
            activeOpacity={0.8}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  defaultShape.AdjunctBtn_Prim,
                  {backgroundColor: COLORS.dark_600},
                ]}>
                <Icon
                  name="Company"
                  size={18}
                  color={COLORS.altgreen_200}
                  style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                />
              </View>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8},
                ]}>
                Organizations
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 5},
                ]}>
                {this.state.company.length}
              </Text>
              <Icon
                name="Arrow_Right"
                size={18}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('CircleNetwork')}
            style={styles.connectsView}
            activeOpacity={0.8}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  defaultShape.AdjunctBtn_Prim,
                  {backgroundColor: COLORS.green_300},
                ]}>
                <Icon
                  name="Circle_Ol"
                  size={18}
                  color={COLORS.dark_600}
                  style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                />
              </View>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8},
                ]}>
                Circles
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 5},
                ]}>
                {this.props.userCircle.body &&
                  this.props.userCircle.body.content.length}
              </Text>
              <Icon
                name="Arrow_Right"
                size={18}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('PublicChatGroup')}
            style={styles.connectsView}
            activeOpacity={0.8}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  defaultShape.AdjunctBtn_Prim,
                  {backgroundColor: COLORS.altgreen_300},
                ]}>
                <Icon
                  name="Community"
                  size={18}
                  color={COLORS.altgreen_50}
                  style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                />
              </View>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 8},
                ]}>
                Groups
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.altgreen_300, marginRight: 5},
                ]}></Text>
              <Icon
                name="Arrow_Right"
                size={18}
                color={COLORS.altgreen_400}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
            </View>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  connectsView: {
    height: 56,
    backgroundColor: COLORS.white,
    width: '90%',
    borderRadius: 8,
    marginTop: 12,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 1,
  },
  connectsHalfView: {
    height: 56,
    backgroundColor: COLORS.white,
    width: '44%',
    borderRadius: 8,
    marginTop: 12,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,

    elevation: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    userCircleProgress: state.circleReducer.userCircleProgress,
    userCircle: state.circleReducer.userCircle,
    errorCircle: state.circleReducer.errorCircle,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userCircleRequest: (data) => dispatch(userCircleRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyNetwork);
