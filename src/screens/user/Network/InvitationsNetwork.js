import React, { Component } from 'react'
import { Image, StyleSheet, FlatList, TouchableOpacity, View, Text, SafeAreaView } from 'react-native'
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import typography from '../../../Components/Shared/Typography'
import defaultProfile from '../../../../assets/defaultProfile.png'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import { REACT_APP_userServiceURL } from '../../../../env.json'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultShape from '../../../Components/Shared/Shape'
import defaultStyle from '../../../Components/Shared/Typography'
import ProfileEditHeader from '../../../Components/User/Profile/ProfileEditHeader'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class ReceivedInvitations extends Component {

  constructor(props) {
    super(props)
    this.state = {
      userId: '',
      receivedInvitations: []
    }
  }

  componentDidMount() {

    AsyncStorage.getItem("refreshToken").then((value) => {
      if (value === null) {
        this.props.navigation.replace("Login")
      }
    })

    AsyncStorage.getItem("userId").then((value) => {
      this.setState({ userId: value })
      this.getInvitations(value)

    }).catch((e) => {
      console.log(e)
    })

  }

  goback = () => { this.props.navigation.goBack() }


  getInvitations(userId) {
    axios({
      method: 'get',
      url: REACT_APP_userServiceURL + '/network/' + userId + '/connectInvitations'
        + "?page=" + 0 + "&size=" + 200,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.data && response.data.message === 'Success!') {
        this.setState({ receivedInvitations: response.data.body.content })
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  ignorePendingConnect = (id) => {
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/graph/users/' + this.state.userId + '/ignoreInvite/' + id,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.status === 202) {
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  acceptConnectInvitation = (id) => {
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/graph/users/' + id + '/connectTo/' + this.state.userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    }).then(response => {
      if (response && response.status === 202) {
        this.getInvitations(this.state.userId)
      }
    }).catch((err) => {
      console.log(err)
    })
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>

        {/***** Header starts *****/}

        <ProfileEditHeader name="Invitations" iconName="Mail_OL" goback={this.goback} />

        {/***** Header ends *****/}

        {/* <View style={{ alignSelf: 'center', flexDirection: 'row', backgroundColor: COLORS.altgreen_t50, width: 320, marginTop: 20, justifyContent: 'center', borderRadius: 4 }}>

          <TouchableOpacity
            style={[defaultShape.InTab_Btn, { backgroundColor: '#fff', width: '50%', borderRadius: 4 }]}>
            <Text style={[defaultStyle.Caption, { color: COLORS.dark_800 }]}>Recieved</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('SentInvitations')}
            style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: '50%', borderRadius: 4 }]}>
            <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>Sent</Text>
          </TouchableOpacity>

        </View> */}

        <View style={{ alignSelf: 'center', flexDirection: 'row', backgroundColor: COLORS.altgreen_t50, width: 320, marginTop: 20, marginBottom: 10, justifyContent: 'center', borderRadius: 4 }}>
          <TouchableOpacity
            style={[defaultShape.InTab_Btn, { backgroundColor: '#fff', width: 80, borderTopLeftRadius: 4, borderBottomLeftRadius: 4 }]}>
            <Text style={[typography.Caption, { color: COLORS.dark_800 }]}>Network</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('InvitationsCircle')}
            style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 80 }]}>
            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Circles</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('InvitationsProject')}
            style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 80 }]}>
            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Projects</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('InvitationsAdmin')}
            style={[defaultShape.InTab_Btn, { backgroundColor: COLORS.altgreen_t50, width: 80, borderTopRightRadius: 4, borderBottomRightRadius: 4 }]}>
            <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>Admins</Text>
          </TouchableOpacity>
        </View>

        <View style={{ width: '100%', height: 24, backgroundColor: COLORS.altgreen_200, justifyContent: 'center', alignItems: 'center', marginTop: 18 }}>
          <Text style={[defaultStyle.Caption, { color: COLORS.altgreen_400, textAlign: 'center' }]}>{this.state.receivedInvitations.length} PENDING</Text>
        </View>

        <FlatList
          keyboardShouldPersistTaps='handled'
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 26, paddingTop: 16 }}
          style={{ height: '50%' }}
          keyExtractor={(item) => item.id}
          data={this.state.receivedInvitations}
          initialNumToRender={10}
          renderItem={({ item }) => (

            <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfileStack', {
              screen: 'OtherProfileScreen',
              params: { userId: item.id },
            })}
              activeOpacity={0.7} >
              <View style={styles.item}>

                <Image source={item.personalInfo && item.personalInfo.profileImage ? { uri: item.personalInfo.profileImage } : defaultProfile} style={[styles.image, { marginLeft: '7%' }]} />

                <View style={styles.nameMsg}>
                  <Text style={styles.name}>{item.username.split(' ').slice(0, 2).join(' ').charAt(0).toUpperCase() + item.username.split(' ').slice(0, 2).join(' ').slice(1)}</Text>
                  {item && item.persona ?
                    <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>{item.persona.charAt(0).toUpperCase() + item.persona.slice(1)}</Text> :
                    <></>}
                  {item.addressDetail && item.addressDetail.country ?
                    <Text style={[defaultStyle.Note2, { color: COLORS.altgreen_300 }]}>{item.addressDetail.country.charAt(0).toUpperCase() + item.addressDetail.country.slice(1)}</Text> :
                    <></>}
                </View>

                <TouchableOpacity
                  onPress={() => this.acceptConnectInvitation(item.id)}
                  style={{ width: 85, height: 28, borderRadius: 17, backgroundColor: COLORS.dark_700, justifyContent: 'center', alignItems: 'center', marginRight: 6 }}
                  activeOpacity={0.8} >
                  <Text style={[defaultStyle.OVERLINE, { color: COLORS.altgreen_200 }]}>Accept</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.ignorePendingConnect(item.id)}
                  style={{ width: 30, height: 30, borderRadius: 15, justifyContent: 'center', alignItems: 'center', marginRight: 16, backgroundColor: COLORS.grey_200 }} activeOpacity={0.5} >
                  <Icon name='Cross' size={12} color={COLORS.dark_600} style={{ marginTop: Platform.OS === 'android' ? 7 : 0 }} />
                </TouchableOpacity>

              </View>

            </TouchableOpacity>


          )}
        />

      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  item: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F7F7F500',
    width: '100%',
    height: 52,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'center',
    marginBottom: 8
  },
  image: {
    height: 36,
    width: 36,
    borderRadius: 18
  },
  nameMsg: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: '6%'
  },
  name: {
    fontWeight: '700',
    color: '#4B4F56',
    fontSize: 14,
    textAlign: 'left'
  },
})
