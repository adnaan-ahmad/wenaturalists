import React, { Component } from 'react'
import { TextInput, Modal, View, Text, StyleSheet, TouchableOpacity, Image, FlatList, ActivityIndicator, SafeAreaView, Platform } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import Snackbar from 'react-native-snackbar'
import LinearGradient from 'react-native-linear-gradient'

import { REACT_APP_userServiceURL } from '../../../../env.json'
import defaultShape from '../../../Components/Shared/Shape'
import icoMoonConfig from '../../../../assets/Icons/selection.json'
import defaultCover from '../../../../assets/defaultCover.png'
import { COLORS } from '../../../Components/Shared/Colors'
import defaultStyle from '../../../Components/Shared/Typography'
import defaultProfile from '../../../../assets/defaultProfile.png'
import typography from '../../../Components/Shared/Typography'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

class SeeAllPopularUsers extends Component {
    constructor(props) {
        super(props)
        this.state = {
            popularUsersData: [],
            userId: '',
            optionsModalOpen: false,
            otherUserId: '',
            mutualConnectCount: 0,
            currentUserType: '',
            requestedList: [],
            pressedItem: {},
            reasonForReporting: '',
            description: '',
            reasonForReportingModalOpen: false
        }
    }

    componentDidMount() {

        this.props.route.params.popularUsersData ?
            this.setState({ popularUsersData: this.props.route.params.popularUsersData }) : null


        AsyncStorage.getItem("refreshToken").then((value) => {
            if (value === null) {
                this.props.navigation.replace("Login")
            }
        })

        AsyncStorage.getItem("userId").then((value) => {
            this.setState({ userId: value })
        }).catch((e) => {
            // console.log(e)
        })
    }

    removePopularUsers = (userId, removeUserId) => {
        axios({
            method: 'get',
            url:
                REACT_APP_userServiceURL +
                '/network/' +
                userId +
                '/popularUsers' +
                '?removeUserId=' +
                removeUserId +
                '&page=0' +
                // this.state.pageSize1 +
                '&size=' +
                10,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.data && response.data.message === 'Success!') {
                    let tempindex = this.state.popularUsersData.findIndex(
                        (x) => x.id === removeUserId,
                    );
                    this.state.popularUsersData.splice(tempindex, 1);
                    this.setState({
                        popularUsersData: this.state.popularUsersData,
                    });
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }

    followUser = (id) => {
        axios({
            method: 'post',
            url:
                REACT_APP_userServiceURL +
                '/graph/users/' +
                this.state.userId +
                '/follows/' +
                id,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.status === 202) {
                    let tempindex = this.state.popularUsersData.findIndex(
                        (x) => x.id === id,
                    );
                    this.state.popularUsersData.splice(tempindex, 1);
                    this.setState({
                        popularUsersData: this.state.popularUsersData,
                        optionsModalOpen: false,
                    });
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }

    sendConnectInvite = (id, userType) => {
        axios({
            method: 'post',
            url:
                REACT_APP_userServiceURL +
                '/graph/users/' +
                this.state.userId +
                '/invite/' +
                id,
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.status === 202) {
                    console.log(response.status);
                    this.setState({ requestedList: [...this.state.requestedList, id] });
                }
            })
            .catch((err) => {
                if (err && err.message === 'Request failed with status code 409') {
                    Snackbar.show({
                        backgroundColor: '#B22222',
                        text: 'You can send connection request after 3 days to this member',
                        duration: Snackbar.LENGTH_LONG,
                    });
                }
            });

        axios({
            method: 'get',
            url:
                REACT_APP_userServiceURL +
                '/graph/users/' +
                this.state.userId +
                '/connectionStatus/' +
                id,
            withCredentials: true,
        })
            .then((response) => {
                if (response && response.data.status === '200 OK') {
                    console.log(response.data.body);
                }
            })
            .catch((err) => {
                console.log(err);
            });

        {
            userType && userType === 'popularUser'
                ? axios({
                    method: 'get',
                    url:
                        REACT_APP_userServiceURL +
                        '/network/' +
                        this.state.userId +
                        '/popularUsers?removeUserId=' +
                        id +
                        '&page=0&size=10',
                    withCredentials: true,
                })
                    .then((response) => {
                        if (response) {
                            console.log(response.data.status);
                            let tempindex = this.state.popularUsersData.findIndex(
                                (x) => x.id === id,
                            );
                            this.state.popularUsersData.splice(tempindex, 1);
                            this.setState({
                                popularUsersData: this.state.popularUsersData,
                            });
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                    })
                : axios({
                    method: 'get',
                    url:
                        REACT_APP_userServiceURL +
                        '/network/' +
                        this.state.userId +
                        '/recommendedUsers?removeUserId=' +
                        id +
                        '&page=0&size=10',
                    withCredentials: true,
                })
                    .then((response) => {
                        if (response) {
                            console.log(response.data.status);
                        }
                    })
                    .catch((err) => {
                        console.log(err)
                    })
        }
    }

    optionsModal = () => {
        return (
            <Modal
                visible={this.state.optionsModalOpen}
                transparent
                animationType="slide"
                supportedOrientations={['portrait', 'landscape']}>
                <View style={{ marginTop: 'auto' }}>
                    <View style={defaultShape.Linear_Gradient_View}>
                        <LinearGradient
                            colors={[
                                COLORS.dark_800 + '00',
                                COLORS.dark_800 + 'CC',
                                COLORS.dark_800,
                            ]}
                            style={defaultShape.Linear_Gradient}></LinearGradient>
                    </View>

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={defaultShape.CloseBtn}
                        onPress={() =>
                            this.setState({ optionsModalOpen: false, currentUserType: '' })
                        }>
                        <Icon
                            name="Cross"
                            size={13}
                            color={COLORS.dark_600}
                            style={styles.crossIcon}
                        />
                    </TouchableOpacity>

                    <View style={defaultShape.Modal_Categories_Container}>
                        {this.state.currentUserType === 'popular' && (
                            <TouchableOpacity
                                style={
                                    Platform.OS === 'ios'
                                        ? [
                                            defaultShape.ActList_Cell_Gylph_Alt,
                                            { paddingVertical: 15 },
                                        ]
                                        : defaultShape.ActList_Cell_Gylph_Alt
                                }
                                activeOpacity={0.6}
                                onPress={() => this.followUser(this.state.otherUserId)}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                    <Icon
                                        name="RSS"
                                        size={17}
                                        color={COLORS.altgreen_300}
                                        style={Platform.OS === 'android' ? { marginTop: 9 } : {}}
                                    />
                                    <Text
                                        style={[
                                            defaultStyle.Button_Lead,
                                            { color: COLORS.dark_600, marginLeft: 8 },
                                        ]}>
                                        Follow
                                    </Text>
                                </View>
                                <Icon
                                    name="Arrow_Right"
                                    size={17}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                            </TouchableOpacity>
                        )}

                        {this.state.currentUserType === 'popular' && (
                            <TouchableOpacity
                                style={
                                    Platform.OS === 'ios'
                                        ? [
                                            defaultShape.ActList_Cell_Gylph_Alt,
                                            { paddingVertical: 15 },
                                        ]
                                        : defaultShape.ActList_Cell_Gylph_Alt
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState({ optionsModalOpen: false }, () =>
                                        this.sendConnectInvite(
                                            this.state.otherUserId,
                                            'popularUser',
                                        ),
                                    );
                                }}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                    <Icon
                                        name="AddUser"
                                        size={17}
                                        color={COLORS.altgreen_300}
                                        style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                    />
                                    <Text
                                        style={[
                                            defaultStyle.Button_Lead,
                                            { color: COLORS.dark_600, marginLeft: 8 },
                                        ]}>
                                        Connect
                                    </Text>
                                </View>
                                <Icon
                                    name="Arrow_Right"
                                    size={17}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                            </TouchableOpacity>
                        )}

                        <TouchableOpacity
                            style={
                                Platform.OS === 'ios'
                                    ? [
                                        defaultShape.ActList_Cell_Gylph_Alt,
                                        { paddingTop: 25, paddingBottom: 15 },
                                    ]
                                    : defaultShape.ActList_Cell_Gylph_Alt
                            }
                            activeOpacity={0.6}
                            onPress={() => {
                                this.setState({ optionsModalOpen: false }, () => {
                                    this.props.navigation.navigate('Chats', {
                                        userId: this.state.userId,
                                        otherUserId: this.state.otherUserId,
                                        lastActive: null,
                                        grpType: 'Private',
                                        name: this.state.pressedItem.username,
                                        otherUserProfile:
                                            this.state.pressedItem.personalInfo && this.state.pressedItem.personalInfo.profileImage
                                                ? this.state.pressedItem.personalInfo.profileImage
                                                : null,
                                    })
                                })
                            }}>
                            <Text
                                style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                                Send Message
                            </Text>
                            <Icon
                                name="WN_Messeges_OL"
                                size={17}
                                color={COLORS.altgreen_300}
                                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                            />
                        </TouchableOpacity>

                        {this.state.currentUserType === 'popular' && (
                            <TouchableOpacity
                                style={
                                    Platform.OS === 'ios'
                                        ? [
                                            defaultShape.ActList_Cell_Gylph_Alt,
                                            { paddingVertical: 15 },
                                        ]
                                        : [
                                            defaultShape.ActList_Cell_Gylph_Alt,
                                            { borderBottomWidth: 0 },
                                        ]
                                }
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.verifyReported()
                                    setTimeout(() => {
                                        this.setState({
                                            optionsModalOpen: false,
                                            // reasonForReportingModalOpen: true,
                                        })
                                    }, 2000)
                                }

                                }>
                                <Text
                                    style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                                    Report User
                                </Text>
                                <Icon
                                    name="ReportComment_OL"
                                    size={17}
                                    color={COLORS.altgreen_300}
                                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                                />
                            </TouchableOpacity>
                        )}
                    </View>
                </View>
            </Modal>
        )
    }

    reasonForReportingModal = () => {
        return (
          <Modal
            visible={this.state.reasonForReportingModalOpen}
            transparent
            animationType="slide"
            supportedOrientations={['portrait', 'landscape']}>
            <View style={{ marginTop: 'auto' }}>
              <View
                style={[
                  defaultShape.Linear_Gradient_View,
                  { height: 700, bottom: 0 },
                ]}>
                <LinearGradient
                  colors={[
                    COLORS.dark_800 + '00',
                    COLORS.dark_800 + 'CC',
                    COLORS.dark_800,
                  ]}
                  style={defaultShape.Linear_Gradient}></LinearGradient>
              </View>
    
              <TouchableOpacity
                activeOpacity={0.7}
                style={[defaultShape.CloseBtn, { marginBottom: 0 }]}
                onPress={() =>
                  this.setState({
                    reasonForReportingModalOpen: false,
                    reasonForReporting: '',
                    description: ''
                  })
                }>
                <Icon
                  name="Cross"
                  size={13}
                  color={COLORS.dark_600}
                  style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
                />
              </TouchableOpacity>
    
              <View
                style={{
                  borderRadius: 20,
                  backgroundColor: COLORS.altgreen_100,
                  alignItems: 'center',
                  paddingTop: 15,
                  paddingBottom: 10,
                  // paddingLeft: 20,
                  width: '90%',
                  alignSelf: 'center',
                  marginBottom: 30,
                  marginTop: 15,
                }}>
                <View
                  style={[
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
                  ]}>
                  <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                    Reason for reporting
                  </Text>
                </View>
    
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        { paddingVertical: 15, borderBottomWidth: 0 },
                      ]
                      : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({ reasonForReporting: 'FAKE_SPAM_OR_SCAM' });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        borderColor: COLORS.altgreen_300,
                        borderWidth: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: COLORS.altgreen_200,
                      }}>
                      {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? (
                        <Icon
                          name="Bullet_Fill"
                          size={17}
                          color={COLORS.dark_800}
                          style={{
                            marginLeft: -2,
                            marginTop: Platform.OS === 'android' ? 8 : 0,
                          }}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                    <Text
                      style={[
                        typography.Button_Lead,
                        { color: COLORS.dark_600, marginLeft: 8 },
                      ]}>
                      Fake, spam or scam
                    </Text>
                  </View>
                </TouchableOpacity>
    
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                      : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({ reasonForReporting: 'ACCOUNT_MAY_BE_HACKED' });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        borderColor: COLORS.altgreen_300,
                        borderWidth: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: COLORS.altgreen_200,
                      }}>
                      {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? (
                        <Icon
                          name="Bullet_Fill"
                          size={17}
                          color={COLORS.dark_800}
                          style={{
                            marginLeft: -2,
                            marginTop: Platform.OS === 'android' ? 8 : 0,
                          }}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                    <Text
                      style={[
                        typography.Button_Lead,
                        { color: COLORS.dark_600, marginLeft: 8 },
                      ]}>
                      Account may be hacked
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                      : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({ reasonForReporting: 'IMPERSONATING_SOMEONE' });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        borderColor: COLORS.altgreen_300,
                        borderWidth: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: COLORS.altgreen_200,
                      }}>
                      {this.state.reasonForReporting === 'IMPERSONATING_SOMEONE' ? (
                        <Icon
                          name="Bullet_Fill"
                          size={17}
                          color={COLORS.dark_800}
                          style={{
                            marginLeft: -2,
                            marginTop: Platform.OS === 'android' ? 8 : 0,
                          }}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                    <Text
                      numberOfLines={2}
                      style={[
                        typography.Button_Lead,
                        { color: COLORS.dark_600, marginLeft: 8 },
                      ]}>
                      Impersonating someone
                    </Text>
                  </View>
                </TouchableOpacity>
    
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                      : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({ reasonForReporting: 'VIOLATES_TERMS_OF_USE' });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        borderColor: COLORS.altgreen_300,
                        borderWidth: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: COLORS.altgreen_200,
                      }}>
                      {this.state.reasonForReporting === 'VIOLATES_TERMS_OF_USE' ? (
                        <Icon
                          name="Bullet_Fill"
                          size={17}
                          color={COLORS.dark_800}
                          style={{
                            marginLeft: -2,
                            marginTop: Platform.OS === 'android' ? 8 : 0,
                          }}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                    <Text
                      style={[
                        typography.Button_Lead,
                        { color: COLORS.dark_600, marginLeft: 8 },
                      ]}>
                      Violates Terms Of Use
                    </Text>
                  </View>
                </TouchableOpacity>
    
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                      : [defaultShape.ActList_Cell_Gylph_Alt, { width: '95%' }]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.setState({
                      reasonForReporting: 'OTHERS'
                    })
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 17,
                        height: 17,
                        borderRadius: 8.5,
                        borderColor: COLORS.altgreen_300,
                        borderWidth: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: COLORS.altgreen_200,
                      }}>
                      {this.state.reasonForReporting === 'OTHERS' ? (
                        <Icon
                          name="Bullet_Fill"
                          size={17}
                          color={COLORS.dark_800}
                          style={{
                            marginLeft: -2,
                            marginTop: Platform.OS === 'android' ? 8 : 0,
                          }}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                    <Text
                      style={[
                        typography.Button_Lead,
                        { color: COLORS.dark_600, marginLeft: 8 },
                      ]}>
                      Others
                    </Text>
                  </View>
                </TouchableOpacity>
    
                {this.state.reasonForReporting === 'OTHERS' &&
                  <View style={{
                    flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 6, paddingRight: 12
                  }}>
    
                    <TextInput
                      theme={{ colors: { text: COLORS.dark_700, primary: COLORS.altgreen_300, placeholder: COLORS.altgreen_300 } }}
                      placeholder="Write the details"
                      placeholderTextColor={COLORS.dark_600}
                      multiline
                      selectionColor='#C8DB6E'
                      style={[defaultStyle.Subtitle_1, { width: '90%', height: 56, backgroundColor: COLORS.altgreen_100, color: COLORS.dark_700 }]}
                      onChangeText={(value) => this.setState({ description: value })}
                      value={this.state.description}
                    />
    
                  </View>}
    
                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => {
                    this.state.reasonForReporting === '' ?
                      Snackbar.show({
                        backgroundColor: '#B22222',
                        text: "Please select an option",
                        duration: Snackbar.LENGTH_LONG,
                      })
                      :
                      this.state.reasonForReporting === 'OTHERS' && this.state.description.trim() === '' ?
                        Snackbar.show({
                          backgroundColor: '#B22222',
                          text: "Please enter the detail",
                          duration: Snackbar.LENGTH_LONG,
                        })
                        :
                        (this.handleReportAbuseSubmit(), this.setState({ reasonForReportingModalOpen: false }))
                  }}
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginRight: 15,
                    height: 27,
                    marginVertical: 10,
                    borderRadius: 16,
                    textAlign: 'center',
                    borderWidth: 1,
                    borderColor: '#698F8A',
                  }}>
                  <Text
                    style={{
                      color: '#698F8A',
                      fontSize: 14,
                      paddingHorizontal: 14,
                      paddingVertical: 20,
                      fontFamily: 'Montserrat-Medium',
                      fontWeight: 'bold',
                    }}>
                    Submit
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        )
    }

    verifyReported = () => {
        axios({
          method: 'get',
          url: REACT_APP_userServiceURL + '/backend/reportabuse/verifyAlreadyReported?reporterId='
            + this.state.userId + '&entityId=' + this.state.otherUserId,
          withCredentials: true
        }).then(response => {
          if (response && response.status === 200 && response.data && response.data.body) {
            // console.log(response.data.body.reported)
            !response.data.body.reported ? 
            this.setState({ isReported: response.data.body.reported, reasonForReportingModalOpen: true })
            :
            Snackbar.show({
              backgroundColor: COLORS.primarydark,
              text: "Your report request was already taken",
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            })
    
          } else {
            console.log(response)
          }
        }).catch((err) => {
          console.log(err)
        })
    }

    handleReportAbuseSubmit = () => {
        let data = {
          reporterId: this.state.userId,
          entityId: this.state.otherUserId,
          // entityType: this.state.entityType,
          entityType: 'USER',
          reason: this.state.reasonForReporting,
          description: this.state.description,
        };
    
        axios({
          method: 'post',
          url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
          data: data,
          withCredentials: true,
        })
          .then((response) => {
            
            if (response && response.status === 201) {
              Snackbar.show({
                backgroundColor: COLORS.primarydark,
                text:
                  'Your request has been taken and appropriate action will be taken as per our report abuse policy',
                textColor: COLORS.altgreen_100,
                duration: Snackbar.LENGTH_LONG,
              });
              // console.log(response)
            } else {
              // console.log(response)
            }
          })
          .catch((err) => {
            
            if (err && err.response && err.response.status === 409) {
              // console.log(err.response)
              Snackbar.show({
                backgroundColor: COLORS.primarydark,
                text: 'Your report request was already taken',
                textColor: COLORS.altgreen_100,
                duration: Snackbar.LENGTH_LONG,
              });
            } else {
              // console.log(err)
              Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Please check your network or try again later',
                duration: Snackbar.LENGTH_LONG,
              })
            }
          })

          setTimeout(() => {
            this.setState({ reasonForReporting: '', description: '' })
          }, 2000)
    }

    renderPopularUser = (item) => {
        return (
            <TouchableOpacity
                onPress={() =>
                    this.props.navigation.navigate('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: { userId: item.id },
                    })
                }
                activeOpacity={0.9}
                style={{ height: 139, width: 148, marginHorizontal: 10, marginVertical: 8 }}>
                <View
                    style={[
                        defaultShape.card8_a,
                        {
                            height: 130,
                            width: 148,
                            position: 'absolute',
                            bottom: 0,
                            zIndex: 1,
                        },
                    ]}>
                    <Image
                        source={
                            item.personalInfo && item.personalInfo.profileImage
                                ? { uri: item.personalInfo.profileImage }
                                : defaultProfile
                        }
                        style={{
                            height: 72,
                            width: 72,
                            borderRadius: 36,
                            position: 'absolute',
                            left: 5,
                            top: -15,
                            zIndex: 2,
                        }}
                    />

                    <TouchableOpacity
                        onPress={() =>
                            this.removePopularUsers(this.state.userId, item.id)
                        }
                        style={{
                            position: 'absolute',
                            alignItems: 'center',
                            top: 0,
                            right: 0,
                            backgroundColor: COLORS.altgreen_100,
                            height: 20,
                            width: 20,
                            borderTopRightRadius: 8,
                            zIndex: 2,
                        }}>
                        <Icon
                            name="Cross"
                            color={COLORS.altgreen_400}
                            size={8}
                            style={{ marginTop: 5 }}
                        />
                    </TouchableOpacity>

                    {item.connectDepth > 0 && (
                        <View
                            style={{
                                paddingHorizontal: 10,
                                paddingVertical: 4,
                                backgroundColor: COLORS.white,
                                position: 'absolute',
                                top: 35,
                                right: 0,
                                zIndex: 2,
                                borderTopLeftRadius: 4,
                            }}>
                            <Text
                                style={[
                                    defaultStyle.Note_2,
                                    { color: COLORS.grey_400, fontSize: 12 },
                                ]}>
                                {item.connectDepth === 1
                                    ? '1st'
                                    : item.connectDepth === 2
                                        ? '2nd'
                                        : '3rd'}
                            </Text>
                        </View>
                    )}

                    <Image
                        source={
                            item.personalInfo.coverImage
                                ? { uri: item.personalInfo.coverImage }
                                : defaultCover
                        }
                        style={{
                            height: 48,
                            width: 146,
                            borderTopLeftRadius: 8,
                            borderTopRightRadius: 8,
                        }}
                    />
                    <Text
                        numberOfLines={1}
                        style={[
                            defaultStyle.Title_2,
                            { color: '#002D3D', marginLeft: 10, marginTop: 10, fontSize: 13 },
                        ]}>
                        {item.username}
                    </Text>
                    <Text
                        style={[
                            defaultStyle.Subtitle_2,
                            { color: COLORS.altgreen_300, marginLeft: 10 },
                        ]}>
                        {item.persona}
                    </Text>
                    {item.addressDetail && item.addressDetail.country && (
                        <Text
                            numberOfLines={1}
                            style={[
                                defaultStyle.Note_2,
                                { color: COLORS.grey_400, marginLeft: 10, fontSize: 12 },
                            ]}>
                            {item.addressDetail && item.addressDetail.country
                                ? item.addressDetail.country
                                : null}
                        </Text>
                    )}

                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                        }}>
                        <Text
                            style={[
                                defaultStyle.Note_2,
                                { color: COLORS.grey_400, marginLeft: 10, fontSize: 12 },
                            ]}>
                            {item.followerCount} followers
                        </Text>

                        <TouchableOpacity
                            onPress={() =>
                                this.setState({
                                    optionsModalOpen: true,
                                    otherUserId: item.id,
                                    mutualConnectCount: item.mutualConnectCount,
                                    currentUserType: 'popular',
                                    pressedItem: item
                                })
                            }
                            style={{
                                height: 20,
                                width: 20,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                            <Icon name="Kebab" size={10} color={COLORS.altgreen_400} />
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        
        return (
            <SafeAreaView>

                {this.optionsModal()}
                {this.reasonForReportingModal()}

                <View style={Platform.OS === 'ios' ? [styles.header, { paddingVertical: 12 }] : styles.header}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 10 }}>
                        <TouchableOpacity style={{ width: 40, height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack()}>
                            <Icon name="Arrow-Left" size={14} color="#91B3A2" style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }} />
                        </TouchableOpacity>
                        <Text style={[defaultStyle.Button_Lead, { color: COLORS.dark_800, marginLeft: 5 }]}>
                            POPULAR USERS
                        </Text>
                    </View>
                </View>

                {this.state.popularUsersData.length ?
                    <FlatList
                        // initialNumToRender={10}
                        contentContainerStyle={{ paddingBottom: 70, paddingTop: 10, justifyContent: 'center', alignSelf: 'center' }}
                        // columnWrapperStyle={{ flexDirection: "row", flexWrap: "wrap", paddingVertical: 10, justifyContent: 'center' }}
                        numColumns={2}
                        keyExtractor={(item) => item.id}
                        data={this.state.popularUsersData}
                        renderItem={({ item }) => (
                            this.renderPopularUser(item)
                        )}
                    />
                    :
                    <ActivityIndicator size='small' color="#00394D" />
                }

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    businessCradView: {
        height: 124,
        width: 140,
        alignItems: 'center',
        marginVertical: 11,
        marginHorizontal: 8
    },
    businessProfileCard: {
        height: 68,
        width: 68,
        borderRadius: 6,
        position: 'absolute',
        top: 0,
        zIndex: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
    businessDetailsCard: {
        height: 84,
        width: 140,
        borderRadius: 4,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,

        elevation: 1,
    },
    businessName: {
        color: COLORS.primarydark,
        fontSize: 12,
        fontFamily: 'Montserrat-Bold',
        marginTop: 20,
        maxWidth: '90%',
    },
    businessLocation: {
        color: COLORS.altgreen_400,
        fontSize: 12,
        fontFamily: 'Montserrat-Medium',
        maxWidth: '90%',
    }
})

export default SeeAllPopularUsers
