import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  Modal,
  PermissionsAndroid,
  FlatList,
} from 'react-native';
import axios from 'axios';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import * as Progress from 'react-native-progress';
import DocumentPicker from 'react-native-document-picker';
import Snackbar from 'react-native-snackbar';
import {v4 as uuidv4} from 'uuid';
import ImagePicker from 'react-native-image-crop-picker';
import LinearGradient from 'react-native-linear-gradient';
import VideoPlayer from 'react-native-video-controls';
import {Switch} from 'react-native-paper';
import {cloneDeep} from 'lodash';

import {
  REACT_APP_socketURL,
  REACT_APP_userServiceURL,
  REACT_APP_messageEncryptKey,
} from '../../../../env.json';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import AesUtil from '../../../services/Utils/AesUtil';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class ChatInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.route.params.userId,
      otherUserId: this.props.route.params.otherUserId,
      otherUserProfile: this.props.route.params.otherUserProfile,
      name: this.props.route.params.name,
      groupType: this.props.route.params.grpType,
      chatEmpty: this.props.route.params.isChatEmpty,
      page: 0,
      starredCount: 0,
      attachmentCount: 0,
      attachmentList: [],
      detailedAttachmentList: [],
      selectedAttachmentDetails: 'image',
      starList: [],
      memberList: [],
      starMsgView: false,
      attachmentDetailsView: false,
      notification: true,
      groupMemberModalOpen: false,
      reasonForReportingModalOpen: false,
      reasonForReporting: 'FAKE_SPAM_OR_SCAM',
      description: '',

      // download file state

      isDownloading: false,
      downloadStart: false,
      progressLimit: 0,
    };
  }

  componentDidMount() {
    this.getInfo();
    this.state.groupType === 'MemberGroup' && this.getAdminMemberDetails();
  }

  getInfo = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/messages/getMemberDetails?userId=' +
        this.state.userId +
        '&receiverId=' +
        this.state.otherUserId +
        '&messageGroupType=' +
        this.state.groupType +
        '&page=' +
        this.state.page +
        '&size=20',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          starredCount: response.data.body.starMessageList.totalElements,
          attachmentCount:
            response.data.body.attachmentMessageList.totalElements,
          attachmentList: response.data.body.attachmentMessageList.content,
          starList: this.state.starList.concat(
            response.data.body.starMessageList.content,
          ),
        });
      })
      .catch((err) => console.log('getInfo error : ', err));
  };

  // file download start *********************

  downloadProgressWrapper = () => {
    return (
      <Modal
        isVisible={this.state.isDownloading}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View>
          {this.state.progressLimit == 1 ? null : (
            <Progress.Pie
              style={styles.downloadInProgress}
              progress={this.state.progressLimit}
              size={50}
              color={'green'}
            />
          )}
          {this.state.progressLimit == 1 ? (
            <Text style={styles.downloadInComplete}>
              File Download Successfully
            </Text>
          ) : null}
        </View>
      </Modal>
    );
  };

  readStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.writeStorage(data);
      } else {
        // Permission Denied
        alert('Read Storage Permission Denied');
      }
    } else {
      this.getDownloadFile(data); //work on ios
    }
  };

  writeStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.getDownloadFile(data);
      } else {
        // Permission Denied
        alert('Write Storage Permission Denied');
      }
    }
  };

  getDownloadFile = (urlDownload) => {
    if (this.state.downloadStart == true) {
      return;
    } else {
      this.setState({downloadStart: true});
    }

    let directoryFile;
    let dirs;

    let fileName;
    try {
      fileName = urlDownload.split('/').pop();
    } catch (e) {
      fileName = uuidv4();
    }
    if (Platform.OS === 'android') {
      directoryFile = RNFetchBlob.fs.dirs;
      dirs = directoryFile.DownloadDir + '/' + fileName;
    } else {
      directoryFile = RNFS.DocumentDirectoryPath;
      dirs = directoryFile + '/' + fileName;
    }

    try {
      RNFetchBlob.config({
        path: dirs,
      })
        .fetch('GET', urlDownload, {})
        .progress((received, total) => {
          let temp = parseFloat(received / total);

          if (temp > 0.9) {
            temp = 1;
            setTimeout(() => {
              this.setState({isDownloading: false});
            }, 1000);
          }
          this.setState(
            {
              isDownloading: true,
              progressLimit: temp,
            },
            () => this.downloadProgressWrapper(),
          );
        })
        .then((res) => {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'File downloaded successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });

          this.setState({
            ...this.state,
            downloadStart: false,
          });
          setTimeout(() => {
            this.setState({isDownloading: false});
          }, 1000);
        })
        .catch((err) => console.log('fetch error: ', err));
    } catch (error) {
      console.log(err);
    }
  };

  // File download ends ***************************

  array_move = (arr, old_index, new_index) => {
    if (new_index >= arr.length) {
      var k = new_index - arr.length + 1;
      while (k--) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr;
  };

  getAdminMemberDetails = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/messages/getMessageGroupById?messageGroupId=' +
        this.state.otherUserId +
        '&userId=' +
        this.state.userId,
      withCredentials: true,
    })
      .then((response) => {
        let tempMemberList = response.data.body.userDetailsDTOList;
        for (let i of tempMemberList) {
          if (response.data.body.admins.indexOf(i.id) !== -1) {
            i.admin = true;
            this.array_move(tempMemberList, tempMemberList.indexOf(i), 0);
          } else {
            i.admin = false;
          }
        }
        this.setState({memberList: tempMemberList});
      })
      .catch((err) => console.log('admin-member details error : ', err));
  };

  unstar = (id) => {
    let postBody = {
      id: id,
      star: [this.state.userId],
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/messaging/message_service/api/message/removeUserToStarList',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let temp = cloneDeep(this.state.starList);
        let index = temp.findIndex((x) => x.id === id);
        temp.splice(index, 1);
        this.setState({
          starList: temp,
          starredCount: this.state.starredCount - 1,
        });
      })
      .catch((err) => console.log('getInfo error : ', err));
  };

  getDetailedAttachmentList = (type) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/messaging/message_service/api/message/getAllAttachmentFiles?userId=' +
        this.state.userId +
        '&receiverId=' +
        this.state.otherUserId +
        '&messageGroupType=' +
        this.state.groupType +
        '&attachmentTypeFormatted=' +
        type +
        '&page=' +
        this.state.page +
        '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          detailedAttachmentList: this.state.detailedAttachmentList.concat(
            response.data.body.content,
          ),
          selectedAttachmentDetails: type,
        });
      })
      .catch((err) => console.log('detailed attachmentlist error : ', err));
  };

  clearChat = () => {
    let postBody = {
      userId: this.state.userId,
      other: this.state.otherUserId,
      messageGroupType: this.state.groupType,
      userActionType: 'ClearChat',
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/messaging/message_service/api/users/submitRequest',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({chatEmpty: true, starList: []});
        this.getInfo();
      })
      .catch((err) => console.log(err));
  };

  renderAttchmentList = (item) => {
    return (
      <View style={{marginHorizontal: 8}}>
        {item.item.attachmentType.includes('image') ? (
          <Image
            source={{uri: item.item.attachmentLink}}
            style={{height: 100, width: 100, borderRadius: 8}}
          />
        ) : (
          <View
            style={{
              height: 120,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Icon
              name="Article"
              color={COLORS.green_600}
              size={Platform.OS === 'ios' ? 100 : 85}
              style={{
                marginRight: 5,
                marginTop: Platform.OS === 'ios' ? 0 : -30,
              }}
            />
            <Text
              numberOfLines={1}
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_600,
                  marginLeft: 5,
                  marginTop: Platform.OS === 'ios' ? 5 : -70,
                  maxWidth: 100,
                },
              ]}>
              {/* {item.item.attachmentLink.split('/').pop()} */}
              {item.item.attachmentFileName}
            </Text>
          </View>
        )}
      </View>
    );
  };

  verifyAlreadyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.otherUserId,
      withCredentials: true,
    })
      .then((response) => {
        response.data.body.reported
          ? Snackbar.show({
              backgroundColor: '#B22222',
              text: 'Your report request was already taken',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            })
          : this.setState({reasonForReportingModalOpen: true});
      })
      .catch((err) => console.log(err));
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.otherUserId,
      entityType: 'USER',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({reasonForReporting: this.state.reasonForReporting});
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        this.setState({reasonForReporting: this.state.reasonForReporting});
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
  };

  renderGroupMember = (item) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginLeft: 15,
          marginTop: 15,
        }}>
        <Image
          source={
            item.item.personalInfo.profileImage
              ? {uri: item.item.personalInfo.profileImage}
              : defaultProfile
          }
          style={{width: 40, height: 40, borderRadius: 20}}
        />
        <View style={{marginLeft: 10}}>
          <Text
            numberOfLines={1}
            style={[
              typography.Button_Lead,
              {color: COLORS.dark_800, maxWidth: 200},
            ]}>
            {item.item.username}
          </Text>
          {item.item.admin && (
            <Text
              numberOfLines={1}
              style={[
                typography.Note2,
                {color: COLORS.green_600, maxWidth: 220},
              ]}>
              ADMINISTRATOR
            </Text>
          )}
        </View>
      </View>
    );
  };

  groupMemberModal = () => {
    return (
      <Modal
        visible={this.state.groupMemberModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {height: 700, bottom: 0},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={[defaultShape.CloseBtn, {marginBottom: 15}]}
            onPress={() => this.setState({groupMemberModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingVertical: 0,
                maxHeight: 360,
                minHeight: 160,
                alignItems: 'flex-start',
              },
            ]}>
            <View
              style={{
                alignItems: 'center',
                backgroundColor: COLORS.altgreen_100,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                paddingVertical: 10,
                width: '100%',
              }}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Group Members
              </Text>
            </View>

            <FlatList
              showsVerticalScrollIndicator={false}
              data={this.state.memberList}
              keyExtractor={(item) => item.id}
              renderItem={(item) => this.renderGroupMember(item)}
            />
          </View>
        </View>
      </Modal>
    );
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {height: 700, bottom: 0},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={[defaultShape.CloseBtn, {marginBottom: 0}]}
            onPress={() => this.setState({reasonForReportingModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.altgreen_100,
              alignItems: 'center',
              paddingTop: 15,
              paddingBottom: 10,
              width: '90%',
              alignSelf: 'center',
              marginBottom: 30,
              marginTop: 15,
            }}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Reason for reporting
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingVertical: 15, borderBottomWidth: 0},
                    ]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'FAKE_SPAM_OR_SCAM'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Fake, spam or scam
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'ACCOUNT_MAY_BE_HACKED'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Account may be hacked
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'IMPERSONATING_SOMEONE'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'IMPERSONATING_SOMEONE' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  numberOfLines={2}
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Impersonating someone
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'VIOLATES_TERMS_OF_USE'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'VIOLATES_TERMS_OF_USE' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Violates Terms Of Use
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting: 'OTHERS',
                  description: 'OTHERS',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'OTHERS' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Others
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                this.handleReportAbuseSubmit(),
                  this.setState({reasonForReportingModalOpen: false});
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 15,
                height: 27,
                marginVertical: 10,
                borderRadius: 16,
                textAlign: 'center',
                borderWidth: 1,
                borderColor: '#698F8A',
              }}>
              <Text
                style={{
                  color: '#698F8A',
                  fontSize: 14,
                  paddingHorizontal: 14,
                  paddingVertical: 20,
                  fontFamily: 'Montserrat-Medium',
                  fontWeight: 'bold',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  normalView = () => {
    return (
      <>
        <View style={[styles.rowView, {marginTop: 15}]}>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_800,
                },
              ]}>
              Attachments & Links
            </Text>
            <Text
              style={[
                typography.Caption,
                {
                  color: COLORS.altgreen_400,
                  marginLeft: 5,
                },
              ]}>
              {'(' + this.state.attachmentCount}{' '}
              {this.state.attachmentCount > 1 ? 'Files)' : 'File)'}
            </Text>
          </View>

          <TouchableOpacity
            onPress={() =>
              this.setState(
                {attachmentDetailsView: true, detailedAttachmentList: []},
                () => this.getDetailedAttachmentList('image'),
              )
            }
            style={[defaultShape.Nav_Gylph_Btn, {height: 34}]}>
            <Icon name="Arrow_Right" color={COLORS.dark_800} size={16} />
          </TouchableOpacity>
        </View>
        {this.state.attachmentCount > 0 && (
          <View style={{backgroundColor: COLORS.white}}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={this.state.attachmentList}
              keyExtractor={(item) => item.id}
              renderItem={(item) => this.renderAttchmentList(item)}
            />
          </View>
        )}

        {this.state.groupType === 'MemberGroup' && (
          <View style={styles.rowView}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                  },
                ]}>
                Group Members
              </Text>
              <Text
                style={[
                  typography.Caption,
                  {
                    color: COLORS.altgreen_400,
                    marginLeft: 5,
                  },
                ]}>
                {'(' + this.state.memberList.length}{' '}
                {this.state.memberList.length > 1 ? 'Members)' : 'Member)'}
              </Text>
            </View>

            <TouchableOpacity
              onPress={() => this.setState({groupMemberModalOpen: true})}
              style={[defaultShape.Nav_Gylph_Btn, {height: 34}]}>
              <Icon name="Arrow_Right" color={COLORS.dark_800} size={16} />
            </TouchableOpacity>
          </View>
        )}

        <View style={styles.rowView}>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_800,
                },
              ]}>
              Starred Messages
            </Text>
            <Text
              style={[
                typography.Caption,
                {
                  color: COLORS.altgreen_400,
                  marginLeft: 5,
                },
              ]}>
              {'(' + this.state.starredCount}{' '}
              {this.state.starredCount > 1 ? 'Messages)' : 'Message)'}
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => this.setState({starMsgView: true})}
            style={[defaultShape.Nav_Gylph_Btn, {height: 34}]}>
            <Icon name="Arrow_Right" color={COLORS.dark_800} size={16} />
          </TouchableOpacity>
        </View>

        <View style={styles.rowView}>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_800,
                },
              ]}>
              Notifications
            </Text>
            {!this.state.notification && (
              <Text
                style={[
                  typography.Caption,
                  {
                    color: COLORS.altgreen_400,
                    marginLeft: 5,
                  },
                ]}>
                (Turned off for 1 week)
              </Text>
            )}
          </View>

          <Switch
            color={COLORS.green_400}
            value={this.state.notification}
            onValueChange={(value) => this.setState({notification: value})}
          />
        </View>

        <View style={styles.encryptAlert}>
          <Text style={[typography.Caption, {color: COLORS.dark_800}]}>
            <Icon name="PrivacyLock" color={COLORS.dark_800} size={14} />
            {
              ' Messages are end-to-end encrypted. No one outside of this chat can read them.'
            }
          </Text>
        </View>

        <View style={styles.endView}>
          {!this.state.chatEmpty && (
            <TouchableOpacity onPress={this.clearChat} style={styles.btn}>
              <Text
                style={[typography.Button_Lead, {color: COLORS.altgreen_400}]}>
                <Icon name="TrashBin" color={COLORS.altgreen_400} size={14} />
                {' CLEAR'}
              </Text>
            </TouchableOpacity>
          )}

          <TouchableOpacity style={[styles.btn, {marginLeft: 8}]}>
            <Text
              style={[typography.Button_Lead, {color: COLORS.altgreen_400}]}>
              <Icon name="xStop" color={COLORS.altgreen_400} size={14} />
              {' BLOCK'}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={this.verifyAlreadyReported}
            style={[
              styles.btn,
              {marginLeft: 8, borderColor: COLORS.alert_red},
            ]}>
            <Text style={[typography.Button_Lead, {color: COLORS.alert_red}]}>
              <Icon
                name="ReportComment_OL"
                color={COLORS.alert_red}
                size={14}
              />
              {' REPORT'}
            </Text>
          </TouchableOpacity>
        </View>
      </>
    );
  };

  starView = () => {
    return (
      <View style={{flex: 1}}>
        <FlatList
          data={this.state.starList}
          style={{paddingVertical: 10}}
          keyExtractor={(item) => item.id}
          renderItem={(item) => (
            <View style={styles.starView}>
              {item.item.attachment &&
              item.item.attachmentType.includes('image') ? (
                <Image
                  source={{uri: item.item.attachmentLink}}
                  style={{height: 160, width: 180, borderRadius: 8}}
                />
              ) : item.item.attachment &&
                item.item.attachmentType.includes('video') ? (
                <VideoPlayer
                  source={{uri: item.item.attachmentLink}}
                  disableSeekbar={true}
                  disableVolume={true}
                  disableTimer={true}
                  disableBack={true}
                  paused={true}
                  // poster={item.item.videoThumbnailUrl}
                  style={{
                    width: 180,
                    height: 160,
                    borderRadius: 8,
                  }}
                />
              ) : item.item.attachment &&
                item.item.attachmentType.includes('audio') ? (
                <VideoPlayer
                  source={{uri: item.item.attachmentLink}}
                  audioOnly={true}
                  paused={true}
                  disableSeekbar={true}
                  disableVolume={true}
                  disableTimer={false}
                  disableBack={true}
                  controls={false}
                  poster="https://www.agbiz.co.za/Content/images/audio.jpg"
                  style={{
                    width: 180,
                    height: 60,
                    borderRadius: 8,
                  }}
                />
              ) : item.item.attachment &&
                !item.item.attachmentType.includes('image') &&
                !item.item.attachmentType.includes('video') &&
                !item.item.attachmentType.includes('audio') ? (
                <TouchableOpacity
                  onPress={() => this.readStorage(item.item.attachmentLink)}
                  style={{
                    paddingHorizontal: 5,
                    paddingTop: 8,
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Article"
                    color={COLORS.green_600}
                    size={18}
                    style={{marginRight: 5}}
                  />
                  <Text
                    style={{
                      color: '#1D525E',
                      fontFamily: 'Montserrat-SemiBold',
                      fontSize: 12,
                      paddingBottom: 5,
                    }}>
                    {item.item.attachmentFileName}
                  </Text>
                </TouchableOpacity>
              ) : (
                <></>
              )}
              {!item.item.attachment && (
                <Text style={[typography.Caption, {color: COLORS.dark_600}]}>
                  {item.item.message}
                </Text>
              )}

              <TouchableOpacity
                onPress={() => this.unstar(item.item.id)}
                style={{
                  position: 'absolute',
                  bottom: 0,
                  right: -8,
                  height: 20,
                  width: 20,
                  borderRadius: 10,
                  backgroundColor: COLORS.white,
                  alignItems: 'center',
                  justifyContent: 'center',
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 1,
                  },
                  shadowOpacity: 0.2,
                  shadowRadius: 1.41,

                  elevation: 2,
                }}>
                <Icon name="Star" color={COLORS.green_600} size={12} />
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    );
  };

  attachmentView = () => {
    return (
      <View>
        <View
          style={{
            alignSelf: 'center',
            flexDirection: 'row',
            backgroundColor: COLORS.altgreen_t50,
            width: 320,
            marginTop: 20,
            justifyContent: 'center',
          }}>
          <TouchableOpacity
            onPress={() =>
              this.setState({detailedAttachmentList: [], page: 0}, () =>
                this.getDetailedAttachmentList('image'),
              )
            }
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor:
                  this.state.selectedAttachmentDetails === 'image'
                    ? '#fff'
                    : COLORS.altgreen_t50,
                width: 80,
              },
            ]}>
            <Text
              style={[
                typography.Caption,
                {
                  color:
                    this.state.selectedAttachmentDetails === 'image'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              PHOTOS
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.setState({detailedAttachmentList: [], page: 0}, () =>
                this.getDetailedAttachmentList('video'),
              )
            }
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor:
                  this.state.selectedAttachmentDetails === 'video'
                    ? '#fff'
                    : COLORS.altgreen_t50,
                width: 80,
              },
            ]}>
            <Text
              style={[
                typography.Caption,
                {
                  color:
                    this.state.selectedAttachmentDetails === 'video'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              VIDEOS
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.setState({detailedAttachmentList: [], page: 0}, () =>
                this.getDetailedAttachmentList('audio'),
              )
            }
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor:
                  this.state.selectedAttachmentDetails === 'audio'
                    ? '#fff'
                    : COLORS.altgreen_t50,
                width: 80,
              },
            ]}>
            <Text
              style={[
                typography.Caption,
                {
                  color:
                    this.state.selectedAttachmentDetails === 'audio'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              AUDIO
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.setState({detailedAttachmentList: [], page: 0}, () =>
                this.getDetailedAttachmentList('other'),
              )
            }
            style={[
              defaultShape.InTab_Btn,
              {
                backgroundColor:
                  this.state.selectedAttachmentDetails === 'other'
                    ? '#fff'
                    : COLORS.altgreen_t50,
                width: 80,
              },
            ]}>
            <Text
              style={[
                typography.Caption,
                {
                  color:
                    this.state.selectedAttachmentDetails === 'other'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              DOCS
            </Text>
          </TouchableOpacity>
        </View>

        <FlatList
          data={this.state.detailedAttachmentList}
          style={{paddingVertical: 10}}
          keyExtractor={(item) => item.id}
          renderItem={(item) => (
            <View style={styles.detailedAttachmentView}>
              {item.item.attachment &&
              item.item.attachmentType.includes('image') ? (
                <Image
                  source={{uri: item.item.attachmentLink}}
                  style={{height: 200, width: '100%', borderRadius: 8}}
                />
              ) : item.item.attachment &&
                item.item.attachmentType.includes('video') ? (
                <VideoPlayer
                  source={{uri: item.item.attachmentLink}}
                  disableSeekbar={true}
                  disableVolume={true}
                  disableTimer={false}
                  disableBack={true}
                  paused={true}
                  // poster={item.item.videoThumbnailUrl}
                  style={{
                    width: '100%',
                    height: 200,
                    borderRadius: 8,
                  }}
                />
              ) : item.item.attachment &&
                item.item.attachmentType.includes('audio') ? (
                <VideoPlayer
                  source={{uri: item.item.attachmentLink}}
                  audioOnly={true}
                  paused={true}
                  disableSeekbar={false}
                  disableVolume={true}
                  disableTimer={false}
                  disableBack={true}
                  controls={false}
                  poster="https://www.agbiz.co.za/Content/images/audio.jpg"
                  style={{
                    width: '100%',
                    height: 60,
                    borderRadius: 8,
                  }}
                />
              ) : item.item.attachment &&
                !item.item.attachmentType.includes('image') &&
                !item.item.attachmentType.includes('video') &&
                !item.item.attachmentType.includes('audio') ? (
                <View
                  style={{
                    paddingHorizontal: 5,
                    paddingTop: 8,
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Article"
                    color={COLORS.green_600}
                    size={100}
                    style={{
                      marginRight: 5,
                      marginTop: Platform.OS === 'ios' ? 0 : -20,
                    }}
                  />
                  <Text
                    style={{
                      color: '#1D525E',
                      fontFamily: 'Montserrat-SemiBold',
                      fontSize: 16,
                      marginTop: Platform.OS === 'ios' ? 10 : -85,
                    }}>
                    {item.item.attachmentFileName}
                  </Text>

                  <TouchableOpacity
                    onPress={() => this.readStorage(item.item.attachmentLink)}
                    style={{
                      height: 40,
                      width: 40,
                      borderRadius: 8,
                      backgroundColor: COLORS.altgreen_100,
                      position: 'absolute',
                      bottom: Platform.OS === 'ios' ? -30 : -25,
                      right: 0,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Icon
                      name="DownloadFile"
                      color={COLORS.green_600}
                      size={16}
                    />
                  </TouchableOpacity>
                </View>
              ) : (
                <></>
              )}
              {!item.item.attachment && (
                <Text style={[typography.Caption, {color: COLORS.dark_600}]}>
                  {item.item.message}
                </Text>
              )}
            </View>
          )}
        />
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.reasonForReportingModal()}
        {this.groupMemberModal()}

        <View style={styles.headerView}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() =>
                this.state.starMsgView || this.state.attachmentDetailsView
                  ? this.setState({
                      starMsgView: false,
                      attachmentDetailsView: false,
                    })
                  : this.props.navigation.goBack()
              }
              style={defaultShape.Nav_Gylph_Btn}>
              <Icon
                name={
                  this.state.starMsgView || this.state.attachmentDetailsView
                    ? 'Arrow-Left'
                    : 'Cross'
                }
                color={COLORS.altgreen_300}
                size={16}
              />
            </TouchableOpacity>
            <Image
              source={
                this.state.otherUserProfile
                  ? {uri: this.state.otherUserProfile}
                  : defaultProfile
              }
              style={{height: 36, width: 36, borderRadius: 18}}
            />
            <View>
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_600,
                    marginLeft: 10,
                    marginTop: Platform.OS === 'android' ? -8 : 0,
                  },
                ]}>
                {this.state.name}
              </Text>
              <Text
                style={[
                  typography.Note2,
                  {
                    color: COLORS.altgreen_400,
                    marginLeft: 10,
                  },
                ]}>
                last active at 4:31 pm
              </Text>
            </View>
          </View>

          <TouchableOpacity
            style={[
              defaultShape.Nav_Gylph_Btn,
              {marginRight: 10, flexDirection: 'row', width: 64},
            ]}>
            <Icon
              name="Export"
              color={COLORS.green_400}
              size={14}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 8}}
            />
            <Text
              style={[
                typography.Caption,
                {
                  color: COLORS.altgreen_400,
                  marginLeft: 5,
                },
              ]}>
              Export
            </Text>
          </TouchableOpacity>
        </View>

        {this.state.starMsgView
          ? this.starView()
          : this.state.attachmentDetailsView
          ? this.attachmentView()
          : this.normalView()}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  rowView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 8,
    marginTop: 10,
    backgroundColor: COLORS.white,
  },
  headerView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 54,
    marginBottom: 5,
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  encryptAlert: {
    backgroundColor: COLORS.green_300,
    borderRadius: 6,
    alignSelf: 'center',
    marginTop: 30,
    paddingVertical: 8,
    paddingHorizontal: 15,
    maxWidth: 300,
  },
  endView: {
    position: 'absolute',
    bottom: 40,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    paddingHorizontal: 15,
    paddingVertical: 8,
    flexDirection: 'row',
    borderRadius: 4,
    borderWidth: 1,
    borderColor: COLORS.altgreen_400,
  },
  starView: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderRadius: 8,
    minWidth: 80,
    maxWidth: 200,
    justifyContent: 'center',
    marginVertical: 6,
    marginLeft: 20,
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  detailedAttachmentView: {
    borderRadius: 8,
    width: '90%',
    height: 200,
    justifyContent: 'center',
    marginVertical: 6,
    marginLeft: 20,
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
