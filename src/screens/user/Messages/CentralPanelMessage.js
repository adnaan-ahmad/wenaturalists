import AsyncStorage from '@react-native-community/async-storage';
import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  Modal,
  TextInput,
  Platform,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import {Badge} from 'react-native-paper';
import {cloneDeep} from 'lodash';
import Snackbar from 'react-native-snackbar';

import {
  REACT_APP_socketURL,
  REACT_APP_userServiceURL,
} from '../../../../env.json';
import SearchBar from '../../../Components/User/SearchBar';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');
const Stomp = require('@stomp/stompjs');
const stompClientMessage = new Stomp.Client({
  brokerURL: REACT_APP_socketURL,
  connectHeaders: {},
  reconnectDelay: 5000,
  heartbeatIncoming: 20000,
  heartbeatOutgoing: 20000,
  forceBinaryWSFrames: true,
  appendMissingNULLonIncoming: true,
});

export default class CentralPanelMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      selectedTab: 'chats',
      grpname: '',
      grpdes: '',
      chatList: [],
      networkList: [],
      unreadMsgUser: [],
      addGroupModalOpen: false,
      searchInput: '',
      startGroup: false,
      createGroup: false,
      groupMemberSelect: [],
      userType: ''
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      value && this.setState({userId: value});
      this.getChatList(value);
      this.getNetworkList(value);
      this.getUnreadMessageCount(value);

      axios({
        method: 'get',
        url: REACT_APP_userServiceURL + '/profile/get?id=' + value + '&otherUserId=' + '',
        cache: true,
        withCredentials: true,
        headers: {
            "Content-Type": "application/json",
        },
    }).then((response) => {
        this.setState({ userType: response.data.body.type })
        // console.log('--- response.data.body ---', response.data.body)
    }).catch((err) => {
        // console.log("Profile data error : ", err)
    })
    });
  }

  changeState = (value) => {
    this.setState(value);
  };

  navigateNotification = () => {
    this.props.navigation.navigate('Notification');
  };

  navigateProfile = () => {

    if (this.state.userType === 'INDIVIDUAL') {
        this.props.navigation.navigate('ProfileStack')
    }
    else if (this.state.userType === 'COMPANY') {
        this.props.navigation.navigate('ProfileStack', {
            screen: 'CompanyProfileScreen',
        })
    }
  }

  navigation = (value, params) => {
    return this.props.navigation.navigate(value, params);
  };

  socketServerConnect = (userId) => {
    stompClientMessage.connectHeaders = {'X-Authorization': ''};

    let {chatList} = this.state;
    let array_move = (arr, old_index, new_index) => {
      if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
          arr.push(undefined);
        }
      }
      arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
      return arr;
    };

    let changeListState = (value) => {
      this.setState({chatList: value});
    };

    stompClientMessage.onConnect = function (frame) {
      this.subscribe('/topic/Private-' + userId, (msg) => {
        var msgBody = JSON.parse(msg.body);
        let index = chatList.findIndex((x) =>
          x.userId === msgBody.receiverId
            ? x.userId === msgBody.receiverId
            : x.userId === msgBody.senderId,
        );
        changeListState(array_move(chatList, index, 0));
      });
      {
        axios({
          method: 'get',
          url:
            REACT_APP_userServiceURL +
            '/messages/getMessageGroupByTypeAndId?messageGroupType=MemberGroup&userId=' +
            userId,
          withCredentials: true,
        })
          .then((response) => {
            for (let i of response.data.body) {
              this.subscribe('/topic/MemberGroup-' + i.id, (msg) =>
                console.log('subscribed to group: ', msg.body),
              );
            }
          })
          .catch((err) => console.log(err));
      }
      {
        axios({
          method: 'get',
          url:
            REACT_APP_userServiceURL +
            '/messaging/message_service/api/message_group/getByType?messageGroupType=NewsGroup',
          withCredentials: true,
        })
          .then((response) => {
            for (let i of response.data.body) {
              this.subscribe('/topic/NewsGroup-' + i.id, (msg) =>
                console.log('subscribed to webuzz: ', msg.body),
              );
            }
          })
          .catch((err) => console.log(err));
      }
    };
    stompClientMessage.onStompError = function (frame) {
      console.log('Broker reported error: ' + frame.headers['message']);
      console.log('Additional details: ' + frame.body);
    };

    stompClientMessage.activate();
  };

  changeTab = (value) => {
    if (value === 'chats') {
      this.setState({selectedTab: value}, () => {
        this.getChatList(this.state.userId),
          this.getUnreadMessageCount(this.state.userId);
      });
    } else if (value === 'groups') {
      this.setState({selectedTab: value}, () => {
        this.getGroupList();
        this.getUnreadMessageCount(this.state.userId);
      });
    } else {
      this.setState({selectedTab: value}, () => this.getWebuzzList());
    }
  };

  getUnreadMessageCount = (userId) => {
    let grptype = '';
    this.state.selectedTab === 'chats'
      ? (grptype = 'Private')
      : (grptype = 'MemberGroup');
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/messaging/message_service/api/message/getAllUnReadMessageBYmessageGroupType?messageGroupType=' +
        grptype +
        '&userId=' +
        userId,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          unreadMsgUser: response.data.body,
        });
      })
      .catch((err) => console.log(err));
  };

  updateMessageToRead = (receiverId) => {
    let postBody = {
      senderId: this.state.userId,
      messageGroupType:
        this.state.selectedTab === 'chats'
          ? 'Private'
          : this.state.selectedTab === 'groups'
          ? 'MemberGroup'
          : 'NewsGroup',
      receiverId: receiverId,
    };

    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/messaging/message_service/api/message/updateMessageToRead',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        console.log('updated to read', response.data);
      })
      .catch((err) => console.log(err));
  };

  getChatList = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/messages/getAllChats?userId=' +
        userId +
        '&messageGroupType=Private',
      withCredentials: true,
    })
      .then((response) => {
        console.log(response.data.body);
        this.setState(
          {
            chatList: response.data.body.messageList,
          },
          () => this.socketServerConnect(userId),
        );
      })
      .catch((err) => console.log('network list error : ', err));
  };

  getNetworkList = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/network/' +
        userId +
        '/connects?page=0&size=10000',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          networkList: response.data.body.content,
        });
      })
      .catch((err) => console.log('network list error : ', err));
  };

  getGroupList = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/messages/getMessageGroupByTypeAndId?messageGroupType=MemberGroup&userId=' +
        this.state.userId,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({chatList: response.data.body});
      })
      .catch((err) => console.log('group list error : ', err));
  };

  getWebuzzList = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/messaging/message_service/api/message_group/getByType?messageGroupType=NewsGroup',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({chatList: response.data.body});
      })
      .catch((err) => console.log(err));
  };

  searchNames = (query) => {
    const regex = new RegExp(`${query.trim()}`, 'i');

    return this.state.networkList
      .sort((a, b) => a.username.toUpperCase() > b.username.toUpperCase())
      .filter((item) => item.username.search(regex) >= 0);
  };

  addRemoveinGroup = (id, name, profile) => {
    let tempId = '';
    if (this.state.groupMemberSelect.length > 0) {
      for (let i of this.state.groupMemberSelect) {
        i.id === id && (tempId = id);
      }
    }

    if (tempId === '') {
      this.setState({
        groupMemberSelect: this.state.groupMemberSelect.concat({
          id: id,
          name: name,
          profile: profile,
        }),
      });
    } else {
      let index = this.state.groupMemberSelect.findIndex((x) => x.id === id);
      let tempArr = cloneDeep(this.state.groupMemberSelect);
      tempArr.splice(index, 1);
      this.setState({
        groupMemberSelect: tempArr,
      });
    }
  };

  createGroup = () => {
    let tempmember = [this.state.userId];
    for (let i of this.state.groupMemberSelect) {
      tempmember.push(i.id);
    }
    let postBody = {
      name: this.state.grpname,
      desc: this.state.grpdes,
      domain: 'Travel',
      messageGroupType: 'MemberGroup',
      members: tempmember,
      admins: [this.state.userId],
    };

    if (this.state.grpname === '') {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please enter group name',
        duration: Snackbar.LENGTH_LONG,
      });
    } else {
      axios({
        method: 'post',
        url:
          REACT_APP_userServiceURL +
          '/messaging/message_service/api/message_group/create',
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          response.data.resultType &&
            response.data.resultType !== 'failure' &&
            this.setState(
              {
                addGroupModalOpen: false,
                searchInput: '',
                startGroup: false,
                groupMemberSelect: [],
                grpname: '',
                grpdes: '',
              },
              () => this.changeTab('groups'),
            );

          response.data.resultType && response.data.resultType === 'failure'
            ? Snackbar.show({
                backgroundColor: '#B22222',
                text: response.data.message,
                duration: Snackbar.LENGTH_LONG,
              })
            : Snackbar.show({
                backgroundColor: COLORS.primarydark,
                text: 'Group created successfully!',
                textColor: COLORS.altgreen_100,
                duration: Snackbar.LENGTH_LONG,
              });
        })
        .catch((err) => {
          Snackbar.show({
            backgroundColor: '#B22222',
            text: err.message,
            duration: Snackbar.LENGTH_LONG,
          });
        });
    }
  };

  lastActiveTime = (value) => {
    if (Date.now() - value >= 86400000 && Date.now() - value < 86400000 * 2) {
      return 'Yesterday';
    } else if (Date.now() - value < 86400000) {
      return moment.unix(value / 1000).format('hh:mm A');
    } else if (
      Date.now() - value >= 86400000 * 2 &&
      Date.now() - value < 86400000 * 7
    ) {
      return moment.unix(value / 1000).format('dddd');
    } else {
      return moment.unix(value / 1000).format('Do MMM, YYYY');
    }
  };

  renderList = (item) => {
    let badgeCount = 0;
    for (let i of this.state.unreadMsgUser) {
      if (this.state.selectedTab === 'chats') {
        i.senderId === item.item.id
          ? (badgeCount += i.total)
          : (badgeCount += 0);
      } else {
        i.receiverId === item.item.id
          ? (badgeCount += i.total)
          : (badgeCount += 0);
      }
    }
    return (
      <TouchableOpacity
        onPress={() => {
          this.updateMessageToRead(item.item.id);

          this.setState({...this.state, addGroupModalOpen: false}, () => {
            this.props.navigation.navigate('Chats', {
              userId: this.state.userId,
              otherUserId:
                this.state.addGroupModalOpen === true ||
                this.state.selectedTab === 'chats'
                  ? item.item.userId
                  : item.item.id,
              lastActive: item.item.lastActive,
              grpType:
                this.state.addGroupModalOpen === true
                  ? 'Private'
                  : this.state.selectedTab === 'chats'
                  ? 'Private'
                  : this.state.selectedTab === 'groups'
                  ? 'MemberGroup'
                  : 'NewsGroup',
              name:
                this.state.selectedTab === 'chats'
                  ? item.item.username
                  : item.item.name,
              otherUserProfile:
                item.item.personalInfo && item.item.personalInfo.profileImage
                  ? item.item.personalInfo.profileImage
                  : null,
            });
          });
        }}
        activeOpacity={0.5}
        style={[
          styles.chatListItem,
          {
            backgroundColor:
              badgeCount > 0 ? COLORS.altgreen_100 : COLORS.NoColor,
          },
        ]}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Image
            source={
              item.item.personalInfo && item.item.personalInfo.profileImage
                ? {uri: item.item.personalInfo.profileImage}
                : defaultProfile
            }
            style={{width: 40, height: 40, borderRadius: 20}}
          />
          <View style={{marginLeft: 10}}>
            <Text
              numberOfLines={1}
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, maxWidth: 200},
              ]}>
              {this.state.selectedTab === 'chats' ||
              this.state.addGroupModalOpen === true
                ? item.item.username
                : item.item.name}
            </Text>
            {!this.state.addGroupModalOpen && (
              <Text
                numberOfLines={2}
                style={[
                  typography.Note2,
                  {color: COLORS.dark_500, maxWidth: 220},
                ]}>
                {this.state.selectedTab === 'chats'
                  ? item.item.message &&
                    item.item.message.includes('attachment')
                    ? 'Sent an attachment'
                    : item.item.message && item.item.message
                  : item.item.desc}
              </Text>
            )}
          </View>
        </View>
        {!this.state.addGroupModalOpen && (
          <View style={{alignItems: 'center'}}>
            <Text style={[typography.Note2, {color: COLORS.grey_400}]}>
              {this.state.selectedTab === 'chats' &&
                this.lastActiveTime(item.item.lastActive)}
            </Text>
            {this.state.selectedTab !== 'webuzz' &&
              !this.state.addGroupModalOpen &&
              badgeCount > 0 && (
                <Badge
                  size={16}
                  style={{
                    backgroundColor: COLORS.green_500,
                    color: COLORS.white,
                    marginTop: 3,
                  }}>
                  {badgeCount}
                </Badge>
              )}
          </View>
        )}
      </TouchableOpacity>
    );
  };

  renderListGroupCreate = (item) => {
    return (
      <TouchableOpacity
        onPress={() =>
          this.state.startGroup
            ? this.addRemoveinGroup(
                item.item.id,
                item.item.username,
                item.item.personalInfo.profileImage,
              )
            : this.setState({...this.state, addGroupModalOpen: false}, () => {
                this.props.navigation.navigate('Chats', {
                  userId: this.state.userId,
                  otherUserId: item.item.id,
                  grpType:
                    this.state.addGroupModalOpen === true
                      ? 'Private'
                      : this.state.selectedTab === 'chats'
                      ? 'Private'
                      : this.state.selectedTab === 'groups'
                      ? 'MemberGroup'
                      : 'NewsGroup',
                  name:
                    this.state.selectedTab === 'chats'
                      ? item.item.username
                      : item.item.name,
                  otherUserProfile:
                    item.item.personalInfo &&
                    item.item.personalInfo.profileImage
                      ? item.item.personalInfo.profileImage
                      : null,
                });
              })
        }
        activeOpacity={0.5}
        style={[
          styles.chatListItem,
          {
            backgroundColor: COLORS.NoColor,
          },
        ]}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Image
            source={
              item.item.personalInfo && item.item.personalInfo.profileImage
                ? {uri: item.item.personalInfo.profileImage}
                : defaultProfile
            }
            style={{width: 40, height: 40, borderRadius: 20}}
          />
          <View style={{marginLeft: 10}}>
            <Text
              numberOfLines={1}
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, maxWidth: 200},
              ]}>
              {item.item.username}
            </Text>
          </View>

          {this.state.groupMemberSelect.findIndex(
            (x) => x.id === item.item.id,
          ) !== -1 && (
            <View
              style={{
                position: 'absolute',
                left: 0,
                backgroundColor: COLORS.dark_600 + '80',
                height: 40,
                width: 40,
                borderRadius: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon name="Tick" color={COLORS.green_100} size={18} />
            </View>
          )}
        </View>
      </TouchableOpacity>
    );
  };

  addGroupModal = () => {
    return (
      <Modal
        visible={this.state.addGroupModalOpen}
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <SafeAreaView style={{flex: 1}}>
          <View style={styles.searchHeader}>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  addGroupModalOpen: false,
                  searchInput: '',
                  startGroup: false,
                  groupMemberSelect: [],
                  grpdes: '',
                  grpname: '',
                })
              }
              style={defaultShape.Nav_Gylph_Btn}>
              <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
            </TouchableOpacity>
            <View style={styles.searchInput}>
              <TextInput
                placeholder="Search Connects"
                placeholderTextColor={COLORS.altgreen_300}
                style={{width: '90%', color: COLORS.altgreen_300}}
                onChangeText={(value) => this.setState({searchInput: value})}
                value={this.state.searchInput}
              />
              <Icon name="Search" size={14} color={COLORS.altgreen_300} />
            </View>
          </View>

          {this.state.startGroup ? (
            <View style={{backgroundColor: COLORS.altgreen_100}}>
              <TextInput
                placeholder="Group Name"
                placeholderTextColor={COLORS.altgreen_400}
                style={{
                  height: 40,
                  marginLeft: 25,
                  width: '80%',
                  color: COLORS.dark_800,
                  fontFamily: 'Montserrat-SemiBold',
                  fontSize: 18,
                  borderBottomWidth: 2,
                  borderBottomColor: COLORS.dark_800,
                }}
                onChangeText={(value) => this.setState({grpname: value})}
                value={this.state.grpname}
              />
              <TextInput
                placeholder="Group Description"
                placeholderTextColor={COLORS.altgreen_400}
                style={{
                  height: 40,
                  marginLeft: 25,
                  width: '80%',
                  color: COLORS.dark_800,
                  fontFamily: 'Montserrat-SemiBold',
                  fontSize: 12,
                }}
                onChangeText={(value) => this.setState({grpdes: value})}
                value={this.state.grpdes}
              />
              <View style={styles.startNewGroup}>
                <FlatList
                  showsHorizontalScrollIndicator={false}
                  horizontal
                  ref={(ref) => (this.flatList = ref)}
                  onContentSizeChange={() =>
                    this.flatList.scrollToEnd({animated: true})
                  }
                  data={
                    this.state.groupMemberSelect.length > 0
                      ? this.state.groupMemberSelect
                      : [{id: 1}]
                  }
                  keyExtractor={(item) => item.id}
                  renderItem={(item) =>
                    this.state.groupMemberSelect.length > 0 ? (
                      <View style={{alignItems: 'center', marginRight: 10}}>
                        <Image
                          source={
                            item.item.profile
                              ? {uri: item.item.profile}
                              : defaultProfile
                          }
                          style={{height: 30, width: 30, borderRadius: 15}}
                        />
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Note2,
                            {color: COLORS.dark_800, maxWidth: 60},
                          ]}>
                          {item.item.name}
                        </Text>
                      </View>
                    ) : (
                      <View>
                        <Text
                          style={[
                            typography.Button_Lead,
                            {color: COLORS.dark_600},
                          ]}>
                          Select connects from below
                        </Text>
                      </View>
                    )
                  }
                />
              </View>
            </View>
          ) : (
            <TouchableOpacity
              onPress={() => this.setState({startGroup: true})}
              activeOpacity={0.6}
              style={styles.startNewGroup}>
              <Icon name="Add_Group" size={18} color={COLORS.dark_600} />
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 15},
                ]}>
                Start New Group
              </Text>
            </TouchableOpacity>
          )}

          <FlatList
            data={this.searchNames(this.state.searchInput)}
            keyExtractor={(item) => item.id}
            renderItem={(item) => this.renderListGroupCreate(item)}
          />

          {this.state.groupMemberSelect.length > 0 && (
            <TouchableOpacity
              onPress={this.createGroup}
              style={styles.addFloatingBtn}>
              <Icon
                name="Arrow-Right"
                size={22}
                color={COLORS.primarydark}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
            </TouchableOpacity>
          )}
        </SafeAreaView>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.addGroupModal()}
        <View style={{backgroundColor: COLORS.white}}>
          <SearchBar
            changeState={this.changeState}
            navigateNotification={this.navigateNotification}
            navigateProfile={this.navigateProfile}
            navigation={this.navigation}
          />
        </View>

        <View style={styles.tabView}>
          <TouchableOpacity
            onPress={() => this.changeTab('chats')}
            style={{
              width: '33.3%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={[
                typography.Body_1_bold,
                {
                  color:
                    this.state.selectedTab === 'chats'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              CHATS
            </Text>
            {this.state.selectedTab === 'chats' && (
              <View style={styles.selectedBorder}></View>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.changeTab('groups')}
            style={{
              width: '33.3%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={[
                typography.Caption,
                {
                  color:
                    this.state.selectedTab === 'groups'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              GROUPS
            </Text>
            {this.state.selectedTab === 'groups' && (
              <View style={styles.selectedBorder}></View>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.changeTab('webuzz')}
            style={{
              width: '33.3%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={[
                typography.Caption,
                {
                  color:
                    this.state.selectedTab === 'webuzz'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                },
              ]}>
              WE BUZZ
            </Text>
            {this.state.selectedTab === 'webuzz' && (
              <View style={styles.selectedBorder}></View>
            )}
          </TouchableOpacity>
        </View>

        <FlatList
          data={this.state.chatList}
          keyExtractor={(item) => item.customUrl}
          renderItem={(item) => this.renderList(item)}
        />

        <TouchableOpacity
          onPress={() => this.setState({addGroupModalOpen: true})}
          style={styles.addFloatingBtn}>
          <Icon
            name="Chat_New"
            size={22}
            color={COLORS.primarydark}
            style={{marginTop: Platform.OS === 'ios' ? 0 : 10}}
          />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: COLORS.white,
    paddingTop: 15,
    paddingBottom: 12,
    borderBottomColor: COLORS.dark_800,
    borderBottomWidth: 1,
  },
  selectedBorder: {
    width: '100%',
    height: 5,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: COLORS.dark_800,
    position: 'absolute',
    bottom: -12,
  },
  chatListItem: {
    flexDirection: 'row',
    paddingHorizontal: 18,
    justifyContent: 'space-between',
    paddingVertical: 5,
    marginTop: 12,
  },
  addFloatingBtn: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    height: 56,
    width: 56,
    borderRadius: 56 / 2,
    backgroundColor: COLORS.primarygreen,
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchHeader: {
    flexDirection: 'row',
    height: 50,
    width: '100%',
    alignItems: 'center',
    backgroundColor: '#326169',
  },
  searchInput: {
    width: '80%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginLeft: 15,
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.altgreen_300,
  },
  startNewGroup: {
    height: 60,
    width: '100%',
    paddingLeft: 25,
    flexDirection: 'row',
    backgroundColor: COLORS.altgreen_t50,
    alignItems: 'center',
  },
});
