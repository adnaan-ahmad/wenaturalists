import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  Modal,
  PermissionsAndroid,
} from 'react-native';
import axios from 'axios';
import {GiftedChat, Bubble, Time, Send} from 'react-native-gifted-chat';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import * as Progress from 'react-native-progress';
import DocumentPicker from 'react-native-document-picker';
import Snackbar from 'react-native-snackbar';
import {v4 as uuidv4} from 'uuid';
import ImagePicker from 'react-native-image-crop-picker';
import LinearGradient from 'react-native-linear-gradient';
import VideoPlayer from 'react-native-video-controls';

import {
  REACT_APP_socketURL,
  REACT_APP_userServiceURL,
  REACT_APP_messageEncryptKey,
} from '../../../../env.json';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import AesUtil from '../../../services/Utils/AesUtil';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {cloneDeep} from 'lodash';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');
var aesUtil = new AesUtil();
const Stomp = require('@stomp/stompjs');
const stompClientMessage = new Stomp.Client({
  brokerURL: REACT_APP_socketURL,
  connectHeaders: {'X-Authorization': ''},
  reconnectDelay: 5000,
  heartbeatIncoming: 20000,
  heartbeatOutgoing: 20000,
  forceBinaryWSFrames: true,
  appendMissingNULLonIncoming: true,
});

export default class Chats extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.route.params.userId,
      otherUserId: this.props.route.params.otherUserId,
      otherUserProfile: this.props.route.params.otherUserProfile,
      name: this.props.route.params.name,
      groupType: this.props.route.params.grpType,
      lastActive: this.props.route.params.lastActive,
      messages: [],
      attachments: {},
      page: 0,
      progressLimit: 0,
      fileUri: '',
      imageSendViewModalOpen: false,
      attachmentModalOpen: false,
      eachMsgViewModalOpen: false,
      currentMsg: {},
      loadEarlier: false,
      isDownloading: false,
      downloadStart: false,
      isChatEmpty: false,
    };
  }

  componentDidMount() {
    this.getPrivateMessage();
    let {userId, otherUserId, otherUserProfile, groupType} = this.state;
    let changeMsgState = (value) => {
      this.setState({messages: value.concat(this.state.messages)});
    };
    stompClientMessage.onConnect = function (frame) {
      this.subscribe(
        groupType === 'Private'
          ? '/topic/Private-' + userId
          : groupType === 'MemberGroup'
          ? '/topic/MemberGroup-' + otherUserId
          : '/topic/NewsGroup-' + otherUserId,
        (msg) => {
          var msgBody = JSON.parse(msg.body);
          if (
            msgBody.receiverId === userId ||
            msgBody.receiverId === otherUserId
          ) {
            changeMsgState([
              {
                _id: msgBody.id,
                text: msgBody.message,
                createdAt: new Date(msgBody.createTime),
                user: {
                  _id: msgBody.senderId,
                  avatar: otherUserProfile ? otherUserProfile : defaultProfile,
                },
                sent: true,
              },
            ]);
          } else {
            console.log('from chat onmsg :', msgBody);
          }
        },
      );
    };
    stompClientMessage.onStompError = function (frame) {
      console.log('Broker reported error: ' + frame.headers['message']);
      console.log('Additional details: ' + frame.body);
    };

    stompClientMessage.activate();
  }

  // file download start *********************

  downloadProgressWrapper = () => {
    return (
      <Modal
        isVisible={this.state.isDownloading}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View>
          {this.state.progressLimit == 1 ? null : (
            <Progress.Pie
              style={styles.downloadInProgress}
              progress={this.state.progressLimit}
              size={50}
              color={'green'}
            />
          )}
          {this.state.progressLimit == 1 ? (
            <Text style={styles.downloadInComplete}>
              File Download Successfully
            </Text>
          ) : null}
        </View>
      </Modal>
    );
  };

  readStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.writeStorage(data);
      } else {
        // Permission Denied
        alert('Read Storage Permission Denied');
      }
    } else {
      this.getDownloadFile(data); //work on ios
    }
  };

  writeStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.getDownloadFile(data);
      } else {
        // Permission Denied
        alert('Write Storage Permission Denied');
      }
    }
  };

  getDownloadFile = (urlDownload) => {
    if (this.state.downloadStart == true) {
      return;
    } else {
      this.setState({downloadStart: true});
    }

    let directoryFile;
    let dirs;

    let fileName;
    try {
      fileName = urlDownload.split('/').pop();
    } catch (e) {
      fileName = uuidv4();
    }
    if (Platform.OS === 'android') {
      directoryFile = RNFetchBlob.fs.dirs;
      dirs = directoryFile.DownloadDir + '/' + fileName;
    } else {
      directoryFile = RNFS.DocumentDirectoryPath;
      dirs = directoryFile + '/' + fileName;
    }

    try {
      RNFetchBlob.config({
        path: dirs,
      })
        .fetch('GET', urlDownload, {})
        .progress((received, total) => {
          let temp = parseFloat(received / total);

          if (temp > 0.9) {
            temp = 1;
            setTimeout(() => {
              this.setState({isDownloading: false});
            }, 1000);
          }
          this.setState(
            {
              isDownloading: true,
              progressLimit: temp,
            },
            () => this.downloadProgressWrapper(),
          );
        })
        .then((res) => {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'File downloaded successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });

          this.setState({
            ...this.state,
            downloadStart: false,
          });
          setTimeout(() => {
            this.setState({isDownloading: false});
          }, 1000);
        })
        .catch((err) => console.log('fetch error: ', err));
    } catch (error) {
      console.log(err);
    }
  };

  // File download ends ***************************

  getPrivateMessage = () => {
    let tempurl = '';
    if (this.state.groupType === 'Private') {
      tempurl =
        REACT_APP_userServiceURL +
        '/messages/privateMessage/get?userId1=' +
        this.state.userId +
        '&userId2=' +
        this.state.otherUserId +
        '&page=' +
        this.state.page +
        '&size=20';
    } else {
      tempurl =
        REACT_APP_userServiceURL +
        '/messages/getAllMessagesByGroupId?messageGroupId=' +
        this.state.otherUserId +
        '&userId=' +
        this.state.userId +
        '&page=' +
        this.state.page +
        '&size=20';
    }
    axios({
      method: 'get',
      url: tempurl,
      withCredentials: true,
    })
      .then((response) => {
        console.log(response.data.body);
        let gcFormat = [];

        for (let chatBody of response.data.body.messageList.content) {
          let temp = {
            _id: chatBody.id,
            text: aesUtil.decrypt(
              REACT_APP_messageEncryptKey,
              chatBody.message,
            ),
            createdAt: new Date(chatBody.createTime),
            index:
              response.data.body.messageList.content.indexOf(chatBody) +
              this.state.messages.length,
            user: {
              _id: chatBody.senderId,
              avatar: this.state.otherUserProfile
                ? this.state.otherUserProfile
                : chatBody.personalInfo && chatBody.personalInfo.profileImage
                ? chatBody.personalInfo.profileImage
                : defaultProfile,
            },
            sent:
              chatBody.senderId === this.state.userId &&
              chatBody.msgSentStatus === 1
                ? true
                : false,
            received:
              chatBody.senderId === this.state.userId &&
              chatBody.msgSentStatus === 2
                ? true
                : false,
            image:
              chatBody.attachment &&
              (chatBody.attachmentType === 'image/png' ||
                chatBody.attachmentType === 'image/jpeg' ||
                chatBody.attachmentType === 'image/jpg')
                ? chatBody.attachmentLink
                : '',
          };

          if (chatBody.star.length > 0) {
            temp.star = true;
          }

          if (
            chatBody.attachment &&
            chatBody.attachmentType.includes('video')
          ) {
            temp.video = chatBody.attachmentLink;
          } else if (
            chatBody.attachment &&
            chatBody.attachmentType.includes('audio')
          ) {
            temp.audio = chatBody.attachmentLink;
          } else if (
            chatBody.attachment &&
            chatBody.attachmentType.includes('video') === false &&
            chatBody.attachmentType.includes('audio') === false &&
            chatBody.attachmentType.includes('image') === false
          ) {
            temp.fileUri = chatBody.attachmentLink;
            temp.text = chatBody.attachmentLink.split('/').pop();
          }
          gcFormat.push(temp);
        }
        this.setState({
          loadEarlier: false,
          messages: this.state.messages.concat(gcFormat),
          isChatEmpty: response.data.body.messageList.empty,
        });
      })
      .catch((err) => console.log(err));
  };

  handleLoadMoreChat = () => {
    this.setState({page: this.state.page + 1, loadEarlier: true}, () =>
      this.getPrivateMessage(),
    );
  };

  lastActiveTime = (value) => {
    if (Date.now() - value >= 86400000 && Date.now() - value < 86400000 * 2) {
      return 'Yesterday';
    } else if (Date.now() - value < 86400000) {
      return moment.unix(value / 1000).format('hh:mm A');
    } else if (
      Date.now() - value >= 86400000 * 2 &&
      Date.now() - value < 86400000 * 7
    ) {
      return moment.unix(value / 1000).format('dddd');
    } else {
      return moment.unix(value / 1000).format('Do MMM, YYYY');
    }
  };

  onSend = (msg) => {
    msg[0]['sent'] = true;
    msg[0]['star'] = false;
    this.setState({
      messages: msg.concat(this.state.messages),
      isChatEmpty: false,
    });

    let postBody = {
      id: msg[0]._id,
      message: aesUtil.encrypt(REACT_APP_messageEncryptKey, msg[0].text),
      senderId: this.state.userId,
      messageGroupType: this.state.groupType,
      receiverId: this.state.otherUserId,
      createTime: new Date().getTime(),
      star: [],
      attachment: false,
      msgSentStatus: 1,
    };

    if (this.state.fileUri !== '') {
      postBody.attachment = true;
      postBody.attachmentLink = this.state.fileUri;
      postBody.attachmentType = this.state.attachments.type;
      postBody.attachmentFileName = this.state.attachments.name;
    }

    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/messaging/message_service/api/message/post',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({fileUri: '', attachments: {}});
      })
      .catch((err) => console.log(err));
  };

  liveCameraPicker = () => {
    const formData = new FormData();
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
      },
      withCredentials: true,
    };

    ImagePicker.openCamera({
      width: 600,
      height: 400,
      cropping: true,
      mediaType: 'any',
    })
      .then((image) => {
        formData.append('file', {
          uri: image.path,
          type: image.mime,
          name: image.path.split('-').pop(),
        });
        axios
          .post(
            REACT_APP_userServiceURL +
              '/messaging/message_service/api/mediaFile/upload',
            formData,
            config,
          )
          .then((response) => {
            if (response && response.data.body !== null) {
              this.setState({
                imageSendViewModalOpen: true,
                attachments: {
                  fileCopyUri: image.path,
                  type: image.mime,
                  name: image.path.split('-').pop(),
                },
                fileUri: response.data.body.fileDownloadUri,
              });
            }
          })
          .catch((err) => console.log(err));
      })
      .catch((err) => console.log('live camera error :', err));
  };

  videoPicker = () => {
    const formData = new FormData();
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
      },
      withCredentials: true,
    };

    ImagePicker.openPicker({
      mediaType: 'video',
    })
      .then((image) => {
        formData.append('file', {
          uri: image.path,
          type: image.mime,
          name: image.path.split('-').pop(),
        });
        axios
          .post(
            REACT_APP_userServiceURL +
              '/messaging/message_service/api/mediaFile/upload',
            formData,
            config,
          )
          .then((response) => {
            if (response && response.data.body !== null) {
              this.setState({
                imageSendViewModalOpen: true,
                attachments: {
                  fileCopyUri: response.data.body.fileDownloadUri,
                  type: image.mime,
                  name: image.sourceURL.split('/').pop(),
                  thumbnail: response.data.body.videoThumbnailUrl,
                },
                fileUri: response.data.body.fileDownloadUri,
              });
            }
          })
          .catch((err) => console.log(err));
      })
      .catch((err) => console.log('video picker error :', err));
  };

  documentPickerAudio = async () => {
    const formData = new FormData();
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
      },
      withCredentials: true,
    };

    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.audio],
      });
      formData.append('file', res);
      this.setState({attachments: res}, () => {
        axios
          .post(
            REACT_APP_userServiceURL +
              '/messaging/message_service/api/mediaFile/upload',
            formData,
            config,
          )
          .then((response) => {
            if (response && response.data.body !== null) {
              this.setState({
                imageSendViewModalOpen: true,
                fileUri: response.data.body.fileDownloadUri,
              });
            }
          })
          .catch((err) => console.log(err));
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  documentPickerSecondaryAttachment = async () => {
    const formData = new FormData();
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
      },
      withCredentials: true,
    };

    try {
      const res = await DocumentPicker.pick({
        type: [
          DocumentPicker.types.csv,
          DocumentPicker.types.xls,
          DocumentPicker.types.xlsx,
          DocumentPicker.types.doc,
          DocumentPicker.types.docx,
          DocumentPicker.types.ppt,
          DocumentPicker.types.pptx,
          DocumentPicker.types.plainText,
          DocumentPicker.types.pdf,
        ],
      });
      formData.append('file', res);
      this.setState({attachments: res}, () => {
        axios
          .post(
            REACT_APP_userServiceURL +
              '/messaging/message_service/api/mediaFile/upload',
            formData,
            config,
          )
          .then((response) => {
            if (response && response.data.body !== null) {
              this.setState(
                {
                  fileUri: response.data.body.fileDownloadUri,
                },
                () => this.sendAttachment(),
              );
            }
          })
          .catch((err) => console.log(err));
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  documentPickerImage = async () => {
    const formData = new FormData();
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
      },
      withCredentials: true,
    };

    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });
      formData.append('file', res);
      this.setState({attachments: res, attachmentModalOpen: false}, () => {
        axios
          .post(
            REACT_APP_userServiceURL +
              '/messaging/message_service/api/mediaFile/upload',
            formData,
            config,
          )
          .then((response) => {
            if (response && response.data.body !== null) {
              this.setState({
                imageSendViewModalOpen: true,
                fileUri: response.data.body.fileDownloadUri,
              });
            }
          })
          .catch((err) => console.log(err));
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  sendAttachment = () => {
    let tempImage = this.state.attachments.type.includes('image')
      ? this.state.fileUri
      : '';
    let chatBody = [
      {
        _id: new Date().getTime() + '' + Math.floor(Math.random() * 10),
        text: '',
        createdAt: new Date(Date.now()),
        user: {
          _id: this.state.userId,
          avatar: defaultProfile,
        },
        image: tempImage,
      },
    ];
    if (
      this.state.attachments.type &&
      this.state.attachments.type.includes('video')
    ) {
      chatBody[0].video = this.state.fileUri;
    } else if (
      this.state.attachments.type &&
      this.state.attachments.type.includes('audio')
    ) {
      chatBody[0].audio = this.state.fileUri;
    } else if (
      this.state.attachments.type &&
      this.state.attachments.type.includes('video') === false &&
      this.state.attachments.type.includes('audio') === false &&
      this.state.attachments.type.includes('image') === false
    ) {
      chatBody[0].fileUri = this.state.fileUri;
      chatBody[0].text = this.state.fileUri.split('/').pop();
    }
    this.setState({imageSendViewModalOpen: false});
    this.onSend(chatBody);
  };

  starMsg = () => {
    let postBody = {
      id: this.state.currentMsg._id,
      star: [this.state.userId],
    };
    axios({
      method: 'post',
      data: postBody,
      url:
        REACT_APP_userServiceURL +
        '/messaging/message_service/api/message/addUserToStarList',
      withCredentials: true,
    })
      .then((response) => {
        console.log(response.data.body);
        let tempmsg = cloneDeep(this.state.messages);
        tempmsg[this.state.currentMsg.index].star = !tempmsg[
          this.state.currentMsg.index
        ].star;
        this.setState({eachMsgViewModalOpen: false, messages: tempmsg});
      })
      .catch((err) => console.log(err));
  };

  unstar = () => {
    let postBody = {
      id: this.state.currentMsg._id,
      star: [this.state.userId],
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/messaging/message_service/api/message/removeUserToStarList',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        console.log(response.data.body);
        let tempmsg = cloneDeep(this.state.messages);
        tempmsg[this.state.currentMsg.index].star = !tempmsg[
          this.state.currentMsg.index
        ].star;
        this.setState({eachMsgViewModalOpen: false, messages: tempmsg});
      })
      .catch((err) => console.log('unstar error : ', err));
  };

  eachMsgViewModal = () => {
    return (
      <Modal
        visible={this.state.eachMsgViewModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {height: 700, bottom: 0},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={[defaultShape.CloseBtn, {marginBottom: 15}]}
            onPress={() => this.setState({eachMsgViewModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingVertical: 0,
                maxHeight: 360,
                minHeight: 160,
                alignItems: 'flex-start',
              },
            ]}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                marginLeft: 25,
                marginTop: 25,
                alignItems: 'center',
              }}>
              <Icon
                name="Reply"
                size={14}
                color={COLORS.dark_600}
                style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
              />
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 6},
                ]}>
                Reply
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                marginLeft: 25,
                marginTop: 15,
                alignItems: 'center',
              }}>
              <Icon
                name="Forward"
                size={14}
                color={COLORS.dark_600}
                style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
              />
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 6},
                ]}>
                Forward
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.state.currentMsg.star ? this.unstar : this.starMsg}
              style={{
                flexDirection: 'row',
                marginLeft: 25,
                marginTop: 15,
                alignItems: 'center',
              }}>
              <Icon
                name="Star"
                size={14}
                color={COLORS.dark_600}
                style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
              />
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 6},
                ]}>
                {this.state.currentMsg.star ? 'Unstar' : 'Star'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                marginLeft: 25,
                marginTop: 15,
                alignItems: 'center',
              }}>
              <Icon
                name="TrashBin"
                size={14}
                color={COLORS.dark_600}
                style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
              />
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 6},
                ]}>
                Delete
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  imageSendViewModal = () => {
    return (
      <Modal
        visible={this.state.imageSendViewModalOpen}
        supportedOrientations={['portrait', 'landscape']}>
        <SafeAreaView
          style={{
            flex: 1,
            backgroundColor: COLORS.black_300,
            justifyContent: 'space-between',
          }}>
          <View style={{marginLeft: 5}}>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  imageSendViewModalOpen: false,
                  attachments: {},
                  fileUri: '',
                })
              }
              style={defaultShape.Nav_Gylph_Btn}>
              <Icon name="Cross" color={COLORS.altgreen_300} size={16} />
            </TouchableOpacity>
          </View>

          {this.state.attachments.type &&
          this.state.attachments.type.includes('image') ? (
            <Image
              source={{uri: this.state.attachments.fileCopyUri}}
              style={{height: '34%', width: '100%', alignSelf: 'center'}}
            />
          ) : this.state.attachments.type &&
            this.state.attachments.type.includes('video') ? (
            <View style={{height: '34%', width: '100%', alignSelf: 'center'}}>
              <VideoPlayer
                source={{uri: this.state.attachments.fileCopyUri}}
                disableSeekbar={true}
                disableVolume={true}
                disableTimer={true}
                disableBack={true}
                paused={true}
                poster={this.state.attachments.thumbnail}
                style={{
                  width: '100%',
                  height: '100%',
                  alignSelf: 'center',
                }}
              />
            </View>
          ) : (
            <View style={{height: '34%', width: '100%', alignSelf: 'center'}}>
              <VideoPlayer
                source={{uri: this.state.attachments.fileCopyUri}}
                audioOnly={true}
                paused={true}
                disableSeekbar={true}
                disableVolume={true}
                disableTimer={false}
                disableBack={true}
                controls={false}
                poster="https://www.agbiz.co.za/Content/images/audio.jpg"
                style={{
                  width: '100%',
                  height: '100%',
                  alignSelf: 'center',
                }}
              />
            </View>
          )}

          <View style={{width: '100%', marginTop: 10}}>
            <TouchableOpacity
              onPress={this.sendAttachment}
              style={[defaultShape.Nav_Gylph_Btn, {alignSelf: 'flex-end'}]}>
              <Icon name="Send_fl" size={20} color={COLORS.altgreen_300} />
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </Modal>
    );
  };

  attachmentModal = () => {
    return (
      <Modal
        visible={this.state.attachmentModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({attachmentModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 130,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              },
            ]}>
            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({attachmentModalOpen: false}, () =>
                  setTimeout(() => {
                    this.liveCameraPicker();
                  }, 800),
                );
              }}>
              <Icon
                name="Camera"
                size={32}
                color={COLORS.green_600}
                style={{alignSelf: 'center'}}
              />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginTop: Platform.OS === 'ios' ? 6 : -20,
                  },
                ]}>
                Camera
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={this.documentPickerImage}>
              <Icon name="Img" size={32} color={COLORS.green_600} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginTop: Platform.OS === 'ios' ? 6 : -20,
                  },
                ]}>
                Photos
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({attachmentModalOpen: false}, () =>
                  setTimeout(() => {
                    this.videoPicker();
                  }, 800),
                );
              }}>
              <Icon name="Vid" size={32} color={COLORS.green_600} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginTop: Platform.OS === 'ios' ? 6 : -20,
                  },
                ]}>
                Video
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({attachmentModalOpen: false}, () =>
                  setTimeout(() => {
                    this.documentPickerAudio();
                  }, 500),
                );
              }}>
              <Icon name="Recorder" size={32} color={COLORS.green_600} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginTop: Platform.OS === 'ios' ? 6 : -20,
                  },
                ]}>
                Audio
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({attachmentModalOpen: false}, () =>
                  setTimeout(() => {
                    this.documentPickerSecondaryAttachment();
                  }, 500),
                );
              }}>
              <Icon name="Meatballs" size={32} color={COLORS.green_600} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginTop: Platform.OS === 'ios' ? 6 : -20,
                  },
                ]}>
                Others
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.imageSendViewModal()}
        {this.attachmentModal()}
        {this.eachMsgViewModal()}

        <View style={styles.headerView}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={defaultShape.Nav_Gylph_Btn}>
              <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
            </TouchableOpacity>
            <Image
              source={
                this.state.otherUserProfile
                  ? {uri: this.state.otherUserProfile}
                  : defaultProfile
              }
              style={{height: 36, width: 36, borderRadius: 18}}
            />
            <View>
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_600,
                    marginLeft: 10,
                    marginTop: Platform.OS === 'android' ? -8 : 0,
                  },
                ]}>
                {this.state.name}
              </Text>
              {this.state.lastActive && (
                <Text
                  style={[
                    typography.Note2,
                    {
                      color: COLORS.altgreen_400,
                      marginLeft: 10,
                    },
                  ]}>
                  last active at {this.lastActiveTime(this.state.lastActive)}
                </Text>
              )}
            </View>
          </View>

          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('MessageInfoStack', {
                screen: 'ChatInfo',
                params: {
                  userId: this.state.userId,
                  otherUserId: this.state.otherUserId,
                  grpType: this.state.groupType,
                  name: this.state.name,
                  otherUserProfile: this.state.otherUserProfile,
                  isChatEmpty: this.state.isChatEmpty,
                },
              })
            }
            style={[defaultShape.Nav_Gylph_Btn, {marginRight: 10}]}>
            <Icon name="Info" color={COLORS.dark_600} size={18} />
          </TouchableOpacity>
        </View>

        <GiftedChat
          placeholder="Start typing..."
          messages={this.state.messages}
          onSend={(messages) => this.onSend(messages)}
          scrollToBottom
          loadEarlier={this.state.messages.length >= 20 ? true : false}
          onLoadEarlier={() => this.handleLoadMoreChat()}
          isLoadingEarlier={this.state.loadEarlier}
          renderBubble={(props) => {
            return props.currentMessage.fileUri ? (
              <Bubble
                {...props}
                textStyle={{
                  right: {
                    color: '#1D525E',
                    fontFamily: 'Roboto-Regular',
                    fontSize: 12,
                  },
                  left: {
                    color: '#1D525E',
                    fontFamily: 'Roboto-Regular',
                    fontSize: 12,
                  },
                }}
                renderTicks={() => {
                  return props.currentMessage.received ? (
                    <Icon
                      name="Double_Tick"
                      color={COLORS.green_600}
                      size={8}
                      style={{marginRight: 5}}
                    />
                  ) : props.currentMessage.sent ? (
                    <Icon
                      name="Tick"
                      color={COLORS.green_600}
                      size={8}
                      style={{marginRight: 5}}
                    />
                  ) : (
                    <></>
                  );
                }}
                wrapperStyle={{
                  left: {
                    backgroundColor: '#FFFFFF',
                    paddingHorizontal: 5,
                    paddingTop: 2,
                  },
                  right: {
                    backgroundColor: '#CFE7C780',
                    paddingHorizontal: 5,
                    paddingTop: 2,
                  },
                }}
                renderMessageText={() => {
                  return (
                    <TouchableOpacity
                      onPress={() =>
                        this.readStorage(props.currentMessage.fileUri)
                      }
                      style={{
                        paddingHorizontal: 5,
                        paddingTop: 8,
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <Icon
                        name="Article"
                        color={COLORS.green_600}
                        size={18}
                        style={{marginRight: 5}}
                      />
                      <Text
                        style={{
                          color: '#1D525E',
                          fontFamily: 'Montserrat-SemiBold',
                          fontSize: 12,
                          paddingBottom: 5,
                        }}>
                        {props.currentMessage.text}
                      </Text>
                    </TouchableOpacity>
                  );
                }}
              />
            ) : (
              <Bubble
                {...props}
                onLongPress={() =>
                  this.setState({
                    eachMsgViewModalOpen: true,
                    currentMsg: props.currentMessage,
                  })
                }
                textStyle={{
                  right: {
                    color: '#1D525E',
                    fontFamily: 'Roboto-Regular',
                    fontSize: 12,
                  },
                  left: {
                    color: '#1D525E',
                    fontFamily: 'Roboto-Regular',
                    fontSize: 12,
                  },
                }}
                renderTicks={() => {
                  return props.currentMessage.received ? (
                    <Icon
                      name="Double_Tick"
                      color={COLORS.green_600}
                      size={8}
                      style={{marginRight: 5}}
                    />
                  ) : props.currentMessage.sent ? (
                    <Icon
                      name="Tick"
                      color={COLORS.green_600}
                      size={8}
                      style={{marginRight: 5}}
                    />
                  ) : (
                    <></>
                  );
                }}
                renderMessageVideo={() => {
                  return (
                    <View style={{height: 150, width: 150, paddingBottom: 5}}>
                      <VideoPlayer
                        source={{uri: props.currentMessage.video}}
                        disableSeekbar={true}
                        disableVolume={true}
                        disableTimer={true}
                        disableBack={true}
                        paused={true}
                        style={{
                          width: 150,
                          height: 150,
                        }}
                      />
                    </View>
                  );
                }}
                renderMessageAudio={() => {
                  return (
                    <View
                      style={{
                        height: 60,
                        width: 180,
                        paddingBottom: 5,
                        borderRadius: 8,
                      }}>
                      <VideoPlayer
                        source={{uri: props.currentMessage.audio}}
                        audioOnly={true}
                        paused={true}
                        disableSeekbar={true}
                        disableVolume={true}
                        disableTimer={false}
                        disableBack={true}
                        controls={false}
                        poster="https://www.agbiz.co.za/Content/images/audio.jpg"
                        style={{
                          width: 180,
                          height: 60,
                          borderRadius: 8,
                        }}
                      />
                    </View>
                  );
                }}
                wrapperStyle={{
                  left: {
                    backgroundColor: '#FFFFFF',
                  },
                  right: {
                    backgroundColor: '#CFE7C780',
                  },
                }}
              />
            );
          }}
          renderTime={(props) => {
            return (
              <Time
                {...props}
                timeTextStyle={{
                  right: {
                    color: '#698F8A',
                    fontFamily: 'Montserrat-Medium',
                    fontSize: 8,
                  },
                  left: {
                    color: '#AABCC3',
                    fontFamily: 'Montserrat-Medium',
                    fontSize: 8,
                  },
                }}
              />
            );
          }}
          renderSend={(props) => {
            return (
              <Send {...props} containerStyle={styles.sendContainer}>
                <View>
                  <Icon name="Send_fl" size={16} color={COLORS.green_500} />
                </View>
              </Send>
            );
          }}
          user={{
            _id: this.state.userId,
            avatar: defaultProfile,
          }}
          renderChatEmpty={() => {
            return (
              <View style={styles.encryptAlert}>
                <Text style={[typography.Caption, {color: COLORS.dark_800}]}>
                  <Icon name="PrivacyLock" color={COLORS.dark_800} size={14} />
                  {
                    ' Messages are end-to-end encrypted. No one outside of this chat can read them.'
                  }
                </Text>
              </View>
            );
          }}
          renderActions={() => {
            return (
              <TouchableOpacity
                onPress={() => this.setState({attachmentModalOpen: true})}
                style={styles.attachmentButton}>
                <Icon
                  name="Clip"
                  size={16}
                  color={COLORS.dark_800}
                  style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                />
              </TouchableOpacity>
            );
          }}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  sendContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginRight: 15,
  },
  headerView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 54,
    marginBottom: 5,
    backgroundColor: COLORS.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  encryptAlert: {
    backgroundColor: COLORS.green_300,
    borderRadius: 6,
    alignSelf: 'center',
    marginTop: 20,
    paddingVertical: 8,
    paddingHorizontal: 15,
    maxWidth: 300,
    transform: [{scaleY: -1}],
  },
  attachmentButton: {
    height: 35,
    width: 35,
    borderRadius: 35 / 2,
    backgroundColor: COLORS.altgreen_200,
    marginLeft: 8,
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
