import React, { Component } from 'react'
import { View, Text, SafeAreaView, ActivityIndicator, TouchableOpacity } from 'react-native'
import { WebView } from 'react-native-webview'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'

import { COLORS } from '../../Components/Shared/Colors'
import typography from '../../Components/Shared/Typography'
import defaultShape from '../../Components/Shared/Shape'
import icoMoonConfig from '../../../assets/Icons/selection.json'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)

export default class Webview extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderWebView = () => {
        return (
            <View style={{ height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' color={COLORS.primarydark} />
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 5 }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                        style={defaultShape.Nav_Gylph_Btn}>
                        <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
                    </TouchableOpacity>

                    <View>
                        <Text style={[typography.Button_Lead, { color: COLORS.dark_800, maxWidth: 200 }]} numberOfLines={1}> {this.props.route.params.news} </Text>
                        <Text style={[typography.Note, { color: COLORS.altgreen_300, maxWidth: 200 }]} numberOfLines={1}> {this.props.route.params.name} </Text>
                    </View>

                    <TouchableOpacity style={defaultShape.Nav_Gylph_Btn}>
                        <Icon name="Bookmark_OL" color={COLORS.altgreen_300} size={16} />
                    </TouchableOpacity>

                    <TouchableOpacity style={defaultShape.Nav_Gylph_Btn}>
                        <Icon name="Kebab" color={COLORS.altgreen_300} size={16} />
                    </TouchableOpacity>
                </View>

                <WebView
                    startInLoadingState={true}
                    source={{ uri: this.props.route.params.news_url }}
                    allowsBackForwardNavigationGestures={true}
                    renderLoading={this.renderWebView} />

            </SafeAreaView>
        );
    }
}
