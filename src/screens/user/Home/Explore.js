import React, {Component} from 'react';
import {
  Image,
  Modal,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  TouchableHighlight,
  SafeAreaView,
  Platform,
} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';

import {personalProfileRequest} from '../../../services/Redux/Actions/User/PersonalProfileActions';
import httpService from '../../../services/AxiosInterceptors';
import SearchBar from '../../../Components/User/SearchBar';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import DailyEdition from '../../../Components/User/Explore/DailyEdition';
import Curated from '../../../Components/User/Explore/Curated';
import Trending from '../../../Components/User/Explore/Trending';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import typography from '../../../Components/Shared/Typography';
import postIcon from '../../../../assets/Post_Icon.png';
import blogIcon from '../../../../assets/Blog_Icon.png';
import linkIcon from '../../../../assets/Link_Icon.png';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
httpService.setupInterceptors();

export default class Explore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 'Daily Edition',
      notification: false,
      redirectToProfile: false,
      postModalOpen: false,
    };
  }

  navigateNotification = (value) => {
    this.props.navigation.navigate(value);
  };

  selectedFeed = () => {
    if (this.state.selected === 'Daily Edition')
      return (
        <DailyEdition
          webviewNavigation={this.webviewNavigation}
          simpleNavigation={this.simpleNavigation}
          hashTagNavigation={this.hashTagNavigation}
        />
      );
    else if (this.state.selected === 'Curated')
      return (
        <Curated
          webviewNavigation={this.webviewNavigation}
          simpleNavigation={this.simpleNavigation}
        />
      );
    else if (this.state.selected === 'Trending')
      return (
        <Trending
          webviewNavigation={this.webviewNavigation}
          simpleNavigation={this.simpleNavigation}
          profileRedirectionNavigation={this.profileRedirectionNavigation}
          navigation={this.navigation}
          addListener={this.addListener}
        />
      );
  };
  

  addListener = (value,cb) =>{
    this.props.navigation.addListener(value,cb)
  }

  changeState = (value) => {
    this.setState(value);
  };

  navigation = (value, params) => {
    this.props.navigation.navigate(value, params);
  };

  webviewNavigation = (value, news, news_url, name) => {
    this.props.navigation.navigate(value, {
      news: news,
      news_url: news_url,
      name: name,
    });
  };

  simpleNavigation = (value, userId, categoryId, blogId, index, slug) => {
    this.props.navigation.navigate(value, {
      userId: userId,
      categoryId: categoryId,
      id: blogId,
      index: index,
      slug: slug,
    });
  };

  hashTagNavigation = (value,params) => {
    this.props.navigation.navigate(value,params);
  };

  profileRedirectionNavigation = (value, params) => {
    this.props.navigation.navigate(value, params);
  };

  headerComponent = () => {
    return (
      <View style={{backgroundColor: COLORS.primarydark}}>
        <View style={styles.header}>
          <TouchableOpacity
            style={styles.feed}
            onPress={() => {
              this.props.navigation.navigate('Feeds');
            }}>
            <Icon
              name="WN_Feeds_OL"
              size={20}
              color="#91B3A2"
              style={styles.feedIcon}
            />
            <Text style={styles.feedText}>Feeds</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.explore}>
            <Icon
              name="WN_Explore_OL"
              size={20}
              color="#fff"
              style={styles.exploreIcon}
            />
            <Text style={styles.exploreText}>Explore</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.feedDetails}>
          <View style={{marginBottom: 15}}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <TouchableOpacity
                onPress={() => this.setState({selected: 'Daily Edition'})}
                style={
                  this.state.selected === 'Daily Edition'
                    ? styles.selected
                    : styles.notSelected
                }>
                <Text
                  style={
                    this.state.selected === 'Daily Edition'
                      ? styles.selectedText
                      : styles.notSelectedText
                  }>
                  Daily Edition
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setState({selected: 'Curated'})}
                style={
                  this.state.selected === 'Curated'
                    ? styles.selected
                    : styles.notSelected
                }>
                <Text
                  style={
                    this.state.selected === 'Curated'
                      ? styles.selectedText
                      : styles.notSelectedText
                  }>
                  Curated
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setState({selected: 'Trending'})}
                style={
                  this.state.selected === 'Trending'
                    ? styles.selected
                    : styles.notSelected
                }>
                <Text
                  style={
                    this.state.selected === 'Trending'
                      ? styles.selectedText
                      : styles.notSelectedText
                  }>
                  Trending
                </Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
      </View>
    );
  };

  postModal = () => {
    return (
      <Modal
        visible={this.state.postModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({postModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              },
            ]}>
            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewFeedsPost', {link: false}),
                );
              }}>
              {/* <Icon name="Add_Post" size={44} color={COLORS.primarygreen} style={{ alignSelf: 'center' }} /> */}
              <Image style={{alignSelf: 'center'}} source={postIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    alignSelf: 'center',
                    position: 'absolute',
                    top: 50,
                  },
                ]}>
                Post
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewBlogPost'),
                );
              }}>
              {/* <Icon name='Blog' size={44} color={COLORS.primarygreen} /> */}
              <Image style={{alignSelf: 'center'}} source={blogIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    position: 'absolute',
                    top: 47,
                  },
                ]}>
                Blog
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewFeedsPost', {link: true}),
                );
              }}>
              {/* <Icon name='Link_Post' size={44} color={COLORS.primarygreen} /> */}
              <Image style={{alignSelf: 'center'}} source={linkIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    position: 'absolute',
                    top: 50,
                  },
                ]}>
                Link
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView
        style={Platform.OS === 'ios' ? {marginTop: 20, flex: 1} : {flex: 1}}>
        <View style={styles.searchBar}>
          <SearchBar
            changeState={this.changeState}
            navigateNotification={this.navigateNotification}
            navigation={this.navigation}
          />
        </View>

        <View style={styles.container}>
          <ScrollView alwaysBounceVertical={false}>
            {this.headerComponent()}
            {this.selectedFeed()}
            {this.postModal()}
          </ScrollView>

          <TouchableHighlight
            activeOpacity={0.2}
            underlayColor="#rgb(216 222 33 / 1%)"
            onPress={() => this.setState({postModalOpen: true})}
            style={styles.floatingIcon}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon
                name="AddPost"
                size={18}
                color={COLORS.primarydark}
                style={styles.editIcon}
              />
              <Text style={styles.floatingIconText}>POST</Text>
            </View>
          </TouchableHighlight>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: COLORS.primarydark,
    flex: 1,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  scrollViewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editIcon: {
    marginTop: Platform.OS === 'android' ? 12 : 0,
    marginRight: 8,
  },
  floatingIconText: {
    color: COLORS.primarydark,
    fontSize: 18,
    fontWeight: 'bold',
    // marginTop: 12,
  },
  floatingIcon: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 113,
    height: 44,
    borderRadius: 40,
    backgroundColor: '#D8DE21',
    position: 'absolute',
    bottom: 16,
    right: 12,
  },
  feedDetails: {
    marginTop: 20,
    marginLeft: 20,
  },
  selected: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#367681',
    marginRight: 15,
    height: 27,
    // width: 66,
    borderRadius: 16,
    textAlign: 'center',
  },
  notSelected: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    height: 27,
    // width: 66,
    borderWidth: 1,
    borderColor: '#698F8A',
    borderRadius: 16,
  },
  selectedText: {
    color: '#F7F7F5',
    fontSize: 12,
    paddingHorizontal: 14,
    fontFamily: 'Montserrat-Medium',
  },
  notSelectedText: {
    color: '#698F8A',
    fontSize: 12,
    paddingHorizontal: 14,
    fontFamily: 'Montserrat-Medium',
  },
  searchBar: {
    // marginTop: 8
  },
  explore: {
    flexDirection: 'row',
    paddingTop: 8,
    marginLeft: 15,
    borderBottomColor: '#BFC52E',
    borderBottomWidth: 4,
    width: 110,
    // borderBottomRightRadius: 4,
    // borderBottomLeftRadius: 4
  },
  feed: {
    flexDirection: 'row',
    paddingTop: 8,
    marginLeft: 20,
    width: 100,
  },
  header: {
    flexDirection: 'row',
    borderBottomColor: '#154A59',
    borderBottomWidth: 1,
    paddingTop: 10,
    height: 60,
  },
  feedText: {
    color: '#91B3A2',
    fontSize: 22,
    marginLeft: 5,
    fontFamily: 'Montserrat-Regular',
  },
  exploreText: {
    color: '#FFFFFF',
    fontSize: 22,
    marginLeft: 5,
    fontFamily: 'Montserrat-SemiBold',
  },
});
