import React, {Component} from 'react';
import {
  Share,
  Clipboard,
  Modal,
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions,
  Animated,
  PanResponder,
  ImageBackground,
} from 'react-native';
import axios from 'axios';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Snackbar from 'react-native-snackbar';
import Swiper from 'react-native-swiper';

import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import {COLORS} from '../../../Components/Shared/Colors';
import httpService from '../../../services/AxiosInterceptors';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultStyle from '../../../Components/Shared/Typography';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
httpService.setupInterceptors();
//Dimensions of device screen
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;
export default class SeeallRecommendedStories extends Component {
  constructor(props) {
    super(props);
    //position of the current article
    this.position = new Animated.ValueXY();
    //position of the swiped up article
    this.swipedCardPosition = new Animated.ValueXY({x: 0, y: -SCREEN_HEIGHT});
    this.state = {
      pageNumber: 1,
      limit: 100,
      recommended: [],
      skills: [],
      specialities: [],
      interests: [],
      persona: '',
      optionsModalOpen: false,
      shareModalOpen: false,
      customUrl: '',
      currentPressedId: '',
      currentIndex: 0,
    };
  }

  UNSAFE_componentWillMount() {
    this.PanResponder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gestureState) => true,
      onPanResponderMove: (evt, gestureState) => {
        if (gestureState.dy > 0 && this.state.currentIndex > 0) {
          this.swipedCardPosition.setValue({
            x: 0,
            y: -SCREEN_HEIGHT + gestureState.dy,
          });
        } else {
          this.position.setValue({
            x: 0,
            y: gestureState.dy,
          });
        }
      },
      onPanResponderRelease: (evt, gestureState) => {
        if (
          this.state.currentIndex > 0 &&
          gestureState.dy > 50 &&
          gestureState.vy > 0.7
        ) {
          Animated.timing(this.swipedCardPosition, {
            toValue: {x: 0, y: 0},
            duration: 400,
            useNativeDriver: false,
          }).start(() => {
            this.setState({currentIndex: this.state.currentIndex - 1});
            this.swipedCardPosition.setValue({x: 0, y: -SCREEN_HEIGHT});
          });
        } else if (
          -gestureState.dy > 20 &&
          -gestureState.vy > 0.7 &&
          this.state.currentIndex < this.state.recommended.length - 1
        ) {
          Animated.timing(this.position, {
            toValue: {x: 0, y: -SCREEN_HEIGHT},
            duration: 400,
            useNativeDriver: false,
          }).start(() => {
            this.setState({currentIndex: this.state.currentIndex + 1});
            this.position.setValue({x: 0, y: 0});
          });
        } else {
          Animated.parallel([
            Animated.spring(this.position, {
              toValue: {x: 0, y: 0},
              useNativeDriver: false,
            }),
            Animated.spring(this.swipedCardPosition, {
              toValue: {x: 0, y: -SCREEN_HEIGHT},
              useNativeDriver: false,
            }),
          ]).start();
        }
      },
    });
  }

  componentDidMount() {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get?id=' +
        this.props.route.params.userId +
        '&otherUserId=' +
        '',
      cache: true,
      withCredentials: true,
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        this.setState({
          skills: response.data.body.skills,
          specialities: response.data.body.specialities,
          interests: response.data.body.interests,
          persona: response.data.body.persona,
        });

        this.getRecommenedStory(
          response.data.body.skills,
          response.data.body.specialities,
          response.data.body.interests,
          response.data.body.persona,
          this.state.pageNumber,
          this.state.limit,
          this.props.route.params.userId,
        );
      })
      .catch((err) => console.log('Profile data error : ', err));
  }

  handleLoadMore = () => {
    this.setState({pageNumber: this.state.pageNumber + 1}, () =>
      this.getRecommenedStory(
        this.state.skills,
        this.state.specialities,
        this.state.interests,
        this.state.persona,
        this.state.pageNumber,
        this.state.limit,
        this.props.route.params.userId,
      ),
    );
  };

  getRecommenedStory = (
    skills,
    specialities,
    interests,
    persona,
    pageNumber,
    limit,
    userId,
  ) => {
    let input = {
      skill_set: skills,
      specialisation: specialities,
      interest: interests,
      persona: persona,
      page_no: 0,
      limit: 50,
      userId: userId,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/recommend/recommendstories/',
      headers: {'Content-Type': 'application/json'},
      data: input,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data !== undefined &&
          response.data.status === 200 &&
          response.data.data !== null &&
          response.data.data !== undefined &&
          response.data.count > 0
        ) {
          // console.log("recomended response from daily edition: ", response.data.data)
          // this.setState({ recommended: response.data.data })
          this.setState({
            recommended: this.state.recommended.concat(response.data.data),
          });
        }
      })
      .catch((err) => console.log('error recommendation: ', err));
  };

  hashTagTemplate = (item) => {
    return (
      <View
        style={{
          height: 30,
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 10,
        }}>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={item.hash_tags.slice(0, 3)}
          keyExtractor={(item) => item}
          renderItem={(item) => (
            <Text
              style={[
                typography.Subtitle_2,
                {color: COLORS.green_600, marginRight: 5},
              ]}>
              #{item.item}
            </Text>
          )}
        />
        <TouchableOpacity
          onPress={() =>
            this.setState({
              shareModalOpen: true,
              customUrl: item.url,
              currentPressedId: item._id,
            })
          }
          style={{marginLeft: 20, marginRight: 10, marginTop: -5}}>
          <Icon name="Share" size={16} color={COLORS.altgreen_300} />
        </TouchableOpacity>
      </View>
    );
  };

  //This function display the articles
  renderArticles() {
    return this.state.recommended
      .map((item, i) => {
        if (i == this.state.currentIndex - 1) {
          return (
            <Animated.View
              key={item.id}
              style={this.swipedCardPosition.getLayout()}
              {...this.PanResponder.panHandlers}>
              <View
                style={{
                  flex: 1,
                  position: 'absolute',
                  height: SCREEN_HEIGHT,
                  width: SCREEN_WIDTH,
                  backgroundColor: '#fff',
                }}>
                <View style={{flex: 2}}>
                  <ImageBackground
                    source={{uri: item.image_url}}
                    style={{
                      flex: 1,
                      height: null,
                      width: null,
                    }}>
                    <LinearGradient
                      colors={[
                        COLORS.white + '00',
                        COLORS.white + 'CC',
                        COLORS.white,
                      ]}
                      style={{
                        width: '100%',
                        height: 160,
                        position: 'absolute',
                        bottom: 0,
                        // alignItems: 'center',
                        justifyContent: 'flex-end',
                        paddingHorizontal: 15,
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('Webview', {
                            news: item.txt_data,
                            news_url: item.news_url,
                            name: item.name,
                          })
                        }>
                        <Text
                          numberOfLines={2}
                          style={[
                            typography.Title_1,
                            {color: COLORS.dark_800},
                          ]}>
                          {item.header}
                        </Text>
                      </TouchableOpacity>
                    </LinearGradient>
                  </ImageBackground>
                </View>
                <View style={{flex: 3, padding: 5, marginLeft: 10}}>
                  <Text style={[typography.Note, {color: COLORS.altgreen_400}]}>
                    {item.name}
                  </Text>
                  <Text
                    style={[
                      typography.Caption,
                      {color: COLORS.altgreen_300, marginTop: 5},
                    ]}>
                    {item.news}
                  </Text>
                  {this.hashTagTemplate(item)}
                </View>
              </View>
            </Animated.View>
          );
        } else if (i < this.state.currentIndex) {
          return null;
        }
        if (i == this.state.currentIndex) {
          return (
            <Animated.View
              key={item.id}
              style={this.position.getLayout()}
              {...this.PanResponder.panHandlers}>
              <View
                style={{
                  flex: 1,
                  position: 'absolute',
                  height: SCREEN_HEIGHT,
                  width: SCREEN_WIDTH,
                  backgroundColor: '#fff',
                }}>
                <View style={{flex: 2}}>
                  <View style={{flex: 2}}>
                    <ImageBackground
                      source={{uri: item.image_url}}
                      style={{
                        flex: 1,
                        height: null,
                        width: null,
                      }}>
                      <LinearGradient
                        colors={[
                          COLORS.white + '00',
                          COLORS.white + 'CC',
                          COLORS.white,
                        ]}
                        style={{
                          width: '100%',
                          height: 160,
                          position: 'absolute',
                          bottom: 0,
                          // alignItems: 'center',
                          justifyContent: 'flex-end',
                          paddingHorizontal: 15,
                        }}>
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate('Webview', {
                              news: item.txt_data,
                              news_url: item.news_url,
                              name: item.name,
                            })
                          }>
                          <Text
                            numberOfLines={2}
                            style={[
                              typography.Title_1,
                              {color: COLORS.dark_800},
                            ]}>
                            {item.header}
                          </Text>
                        </TouchableOpacity>
                      </LinearGradient>
                    </ImageBackground>
                  </View>
                </View>
                <View style={{flex: 3, padding: 5, marginLeft: 10}}>
                  <Text style={[typography.Note, {color: COLORS.altgreen_400}]}>
                    {item.name}
                  </Text>
                  <Text
                    style={[
                      typography.Caption,
                      {color: COLORS.altgreen_300, marginTop: 5},
                    ]}>
                    {item.news}
                  </Text>
                  {this.hashTagTemplate(item)}
                </View>
              </View>
            </Animated.View>
          );
        } else {
          return (
            <Animated.View key={item.id}>
              <View
                style={{
                  flex: 1,
                  position: 'absolute',
                  height: SCREEN_HEIGHT,
                  width: SCREEN_WIDTH,
                  backgroundColor: '#fff',
                }}>
                <View style={{flex: 2}}>
                  <View style={{flex: 2}}>
                    <ImageBackground
                      source={{uri: item.image_url}}
                      style={{
                        flex: 1,
                        height: null,
                        width: null,
                      }}>
                      <LinearGradient
                        colors={[
                          COLORS.white + '00',
                          COLORS.white + 'CC',
                          COLORS.white,
                        ]}
                        style={{
                          width: '100%',
                          height: 160,
                          position: 'absolute',
                          bottom: 0,
                          // alignItems: 'center',
                          justifyContent: 'flex-end',
                          paddingHorizontal: 15,
                        }}>
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate('Webview', {
                              news: item.txt_data,
                              news_url: item.news_url,
                              name: item.name,
                            })
                          }>
                          <Text
                            numberOfLines={2}
                            style={[
                              typography.Title_1,
                              {color: COLORS.dark_800},
                            ]}>
                            {item.header}
                          </Text>
                        </TouchableOpacity>
                      </LinearGradient>
                    </ImageBackground>
                  </View>
                </View>
                <View style={{flex: 3, padding: 5, marginLeft: 10}}>
                  <Text style={[typography.Note, {color: COLORS.altgreen_400}]}>
                    {item.name}
                  </Text>
                  <Text
                    style={[
                      typography.Caption,
                      {color: COLORS.altgreen_300, marginTop: 5},
                    ]}>
                    {item.news}
                  </Text>
                  {this.hashTagTemplate(item)}
                </View>
              </View>
            </Animated.View>
          );
        }
      })
      .reverse();
  }

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({optionsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Blog
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({optionsModalOpen: false, shareModalOpen: true});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Share
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({optionsModalOpen: false});
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Bookmark this blog
              </Text>
              <Icon
                name="Bookmark_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({optionsModalOpen: false});
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Don't show me blogs like this
              </Text>
              <Icon
                name="Hide"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}
              onPress={() =>
                this.setState({reasonForReportingModalOpen: true})
              }>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Report Content
              </Text>
              <Icon
                name="ReportComment_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation.navigate('RepostOnWenat', {
                    link: false,
                    pressedActivityId: this.state.currentPressedId,
                    entityType: 'STORY',
                    type: 'EXPLORE',
                  }),
                );
              }}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl + '/post/' + this.state.currentPressedId,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            {/* <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                    : [defaultShape.ActList_Cell_Gylph_Alt]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({shareModalOpen: false});
                }}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  Share through Mail
                </Text>
                <Icon
                  name="Mail_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity> */}

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Envelope"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message: this.state.customUrl,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.optionsModal()}
        {this.shareModal()}

        <View
          style={{
            flexDirection: 'row',
            paddingTop: 5,
            backgroundColor: COLORS.white,
          }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
          </TouchableOpacity>

          <View
            style={{
              marginLeft: 15,
              height: 38,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={[
                typography.Title_1,
                {color: COLORS.dark_800, maxWidth: 230},
              ]}
              numberOfLines={1}>
              {' '}
              Stories Just for You{' '}
            </Text>
          </View>
        </View>
        <Swiper loop={false} showsPagination={false}>
          <Swiper horizontal={false} loop={false} showsPagination={true}>
            <SafeAreaView style={{flex: 1}}>
              {this.renderArticles()}
            </SafeAreaView>
          </Swiper>
        </Swiper>

        {/* <FlatList
          data={this.state.recommended}
          keyExtractor={(item) => item.id}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={1}
          ListFooterComponent={
            <View
              style={{
                paddingVertical: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: COLORS.grey_400}}>Loading ...</Text>
            </View>
          }
          renderItem={(item) => (
            <View style={{marginHorizontal: 15, marginTop: 15}}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={{uri: item.item.image_url}}
                  style={{
                    height: 100,
                    width: 100,
                    borderRadius: 4,
                    marginRight: 10,
                  }}
                />

                <View>
                  <Text style={[typography.Note, {color: COLORS.altgreen_400}]}>
                    {item.item.name}
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('Webview', {
                        news: item.item.news,
                        news_url: item.item.news_url,
                        name: item.item.name,
                      })
                    }>
                    <Text
                      numberOfLines={2}
                      style={[
                        typography.Button_Lead,
                        {color: COLORS.dark_800, marginTop: 5, maxWidth: '82%'},
                      ]}>
                      {item.item.header}
                    </Text>
                  </TouchableOpacity>
                  <Text
                    numberOfLines={3}
                    style={[
                      typography.Note,
                      {
                        color: COLORS.altgreen_300,
                        fontFamily: 'Montserrat-Medium',
                        marginTop: 5,
                        maxWidth: '82%',
                      },
                    ]}>
                    {item.item.news}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  height: 30,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: 10,
                }}>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={item.item.hash_tags.slice(0, 3)}
                  keyExtractor={(item) => item}
                  renderItem={(item) => (
                    <Text
                      style={[
                        typography.Subtitle_2,
                        {color: COLORS.green_600, marginRight: 5},
                      ]}>
                      #{item.item}
                    </Text>
                  )}
                />

                <TouchableOpacity
                  onPress={() =>
                    this.setState(
                      {
                        shareModalOpen: true,
                        customUrl: item.item.news_url,
                        currentPressedId: item.item._id,
                      },
                      () => {
                        console.log(
                          'object press',
                          this.state.currentPressedId,
                        );
                      },
                    )
                  }
                  style={{
                    marginTop: 5,
                    height: 24,
                    width: 24,
                    borderRadius: 12,
                    backgroundColor: COLORS.grey_200,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Icon
                    name="Share"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                </TouchableOpacity>
              </View>
            </View>
          )}
        /> */}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
});
