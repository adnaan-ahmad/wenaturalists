import React, {Component} from 'react';
import {
  SafeAreaView,
  Modal,
  Text,
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  FlatList,
  ScrollView,
  TextInput,
  Platform,
  PermissionsAndroid,
  Share,
  ImageBackground,
  Clipboard,
  Touchable,
  Alert,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {connect} from 'react-redux';
import VideoPlayer from 'react-native-video-controls';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import * as Progress from 'react-native-progress';
import DocumentPicker from 'react-native-document-picker';
import Snackbar from 'react-native-snackbar';
import {v4 as uuidv4} from 'uuid';
import LinearGradient from 'react-native-linear-gradient';
import Autolink from 'react-native-autolink';
import RNUrlPreview from 'react-native-url-preview';
import TimeAgo from 'react-native-timeago';

import BlogDefault from '../../../../assets/BlogDefault.jpg';
import Comment from '../../../Components/User/Common/Comment';
import Report from '../../../Components/User/Common/Report';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import circleDefault from '../../../../assets/CirclesDefault.png';
import projectDefault from '../../../../assets/project-default.jpg';
import defaultStyle from '../../../Components/Shared/Typography';
import {
  feedsPhotosRequest,
  feedsVideosRequest,
} from '../../../services/Redux/Actions/User/FeedsActions';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import httpService from '../../../services/AxiosInterceptors';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import typography from '../../../Components/Shared/Typography';
import {log} from 'react-native-reanimated';
import SharedUserList from '../../../Components/User/Common/SharedUserList';
import LikedUserList from '../../../Components/User/Common/LikedUserList';
import {tagDescription} from '../../../Components/Shared/commonFunction';

httpService.setupInterceptors();

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
  'i',
); // fragment locator
class IndividualFeedsPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      data: {},
      commentData: [],
      commentAttachments: [],
      commentBody: '',
      replyExpandId: '',
      readMore: false,
      setreadmore: false,
      sendButton: false,
      replyExpand: false,
      reply: false,
      fullscreen: false,
      downloadStart: false,
      isDownloading: false,
      progressLimit: 0,
      optionsModalOpen: false,
      shareModalOpen: false,
      likeModalOpen: false,
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      optionsModal2Open: false,
      pressedUserId: '',
      pressedActivityId: '',
      isReported: false,
      edit: false,
      peopleShared: [],
      peopleSharedModalOpen: false,
      peopleLiked: [],
      causeItem: {},
      downloadStart: false,
      isDownloading: false,
      progressLimit: 0,
      attachmentModalopen: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        value &&
          this.setState({
            userId: value,
          });
        this.individualPostResponse(value);
        this.getCommentsData(value);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  individualPostResponse = (value) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/post/get?id=' +
        this.props.route.params.id +
        '&postRequestingUserId=' +
        value,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        // console.log("individual post response : ", response.data.body)
        this.setState({data: response.data.body});
      })
      .catch((err) => {
        console.log('error individual post : ', err);
      });
  };

  getCommentsData = (value) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/comment/getCommentsByActivityId/' +
        this.props.route.params.id +
        '?userId=' +
        value +
        '&page=' +
        0 +
        '&size=' +
        1000,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log("comments list: ", response.data.body.content)
          // console.log("comments list attachment: ", response.data.body.content[0].attachmentIds)
          this.setState({
            commentData: response.data.body.content,
            commentCount:
              response.data.body.content.length > 0
                ? response.data.body.content[0].commentsCount
                : 0,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getUsersWhoLiked = (id) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/like/getUsers/' +
        id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          this.setState({peopleLiked: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.props.route.params.id,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  peopleSharedModal = () => {
    return (
      <Modal
        visible={this.state.peopleSharedModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({peopleSharedModalOpen: false, peopleShared: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who Shared this post
              </Text>
            </View>

            <SharedUserList
              id={this.props.route.params.id}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  navigation = (value, params) => {
    this.setState({likeModalOpen: false, peopleSharedModalOpen: false}, () =>
      this.props.navigation.navigate(value, params),
    );
  };

  // Attachement start
  downloadProgressWrapper = () => {
    return (
      <Modal
        isVisible={this.state.isDownloading}
        backdropColor={'#ffffff'}
        animationIn={'slideInUp'}
        animationInTiming={300}
        animationOut={'slideOutDown'}
        animationOutTiming={300}
        avoidKeyboard={true}>
        <View>
          {this.state.progressLimit == 1 ? null : (
            <Progress.Pie
              style={styles.downloadInProgress}
              progress={this.state.progressLimit}
              size={50}
              color={'green'}
            />
          )}
          {this.state.progressLimit == 1 ? (
            <Text style={styles.downloadInComplete}>
              File Download Successfully
            </Text>
          ) : null}
        </View>
      </Modal>
    );
  };

  readStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.writeStorage(data);
      } else {
        // Permission Denied
        alert('Read Storage Permission Denied');
      }
    } else {
      this.getDownloadFile(data); //work on ios
    }
  };

  writeStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.getDownloadFile(data);
      } else {
        // Permission Denied
        alert('Write Storage Permission Denied');
      }
    }
  };

  getDownloadFile = (urlDownload) => {
    if (this.state.downloadStart == true) {
      return;
    } else {
      this.setState({downloadStart: true});
    }

    let directoryFile;
    let dirs;

    let fileName;
    try {
      fileName = urlDownload.split('-').pop();
    } catch (e) {
      fileName = uuidv4();
    }
    if (Platform.OS === 'android') {
      directoryFile = RNFetchBlob.fs.dirs;
      dirs = directoryFile.DownloadDir + '/Download/' + fileName;
    } else {
      directoryFile = RNFS.DocumentDirectoryPath;
      dirs = directoryFile + fileName;
    }

    try {
      RNFetchBlob.config({
        path: dirs,
      })
        .fetch('GET', urlDownload, {})
        .progress((received, total) => {
          let temp = parseFloat(received / total);

          if (temp > 0.9) {
            temp = 1;
            setTimeout(() => {
              this.setState({isDownloading: false});
            }, 1000);
          }
          this.setState(
            {
              isDownloading: true,
              progressLimit: temp,
            },
            () => this.downloadProgressWrapper(),
          );
        })
        .then((res) => {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'File downloaded successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });

          this.setState({
            ...this.state,
            downloadStart: false,
          });
          setTimeout(() => {
            this.setState({isDownloading: false});
          }, 1000);
        });
    } catch (error) {}
  };

  attachmentModal = () => {
    return (
      <Modal
        visible={this.state.attachmentModalopen}
        transparent
        animationType="slide"
        supportedOrientation={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({attachmentModalopen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>
          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
              },
            ]}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Attachments{' '}
              </Text>
            </View>
            <FlatList
              keyboardShouldPersistTaps="handled"
              showsVerticalScrollIndicator={false}
              // contentContainerStyle={{ justifyContent: 'flex-start' }}
              style={{height: '60%', width: '100%', marginTop: 6}}
              keyExtractor={(item) => item.id}
              data={this.state.causeItem.secondaryAttachmentIds}
              // initialNumToRender={10}
              renderItem={({item, index}) => (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginLeft: 20,
                  }}>
                  <Icon
                    name="Clip"
                    color={COLORS.green_500}
                    size={14}
                    style={{
                      marginTop: Platform.OS === 'android' ? 10 : 0,
                      marginRight: 6,
                    }}
                  />
                  <TouchableOpacity
                    onPress={() => {
                      this.readStorage(item.attachmentUrl);
                    }}>
                    <Text>
                      {item.attachmentUrl
                        .substring(item.attachmentUrl.lastIndexOf('/') + 1)
                        .substring(
                          item.attachmentUrl
                            .substring(item.attachmentUrl.lastIndexOf('/') + 1)
                            .indexOf('-') + 1,
                        )}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>
        </View>
      </Modal>
    );
  };
  // Attachement end

  likeModal = () => {
    return (
      <Modal
        visible={this.state.likeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({likeModalOpen: false, peopleLiked: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Who liked this post
              </Text>
            </View>

            <LikedUserList
              id={this.props.route.params.id}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  changeState = (value) => {
    this.setState(value);
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <Report
          changeState={this.changeState}
          entityId={this.props.route.params.id}
          entityType="POST"
        />
      </Modal>
    );
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedActivityId,
      // entityType: this.state.entityType,
      entityType: 'POST',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    setTimeout(() => {
      this.setState({reasonForReporting: '', description: ''});
    }, 1000);
  };

  deletePostAlert = () => {
    this.setState({optionsModalOpen: false});
    Alert.alert('', 'Are you sure you want to delete this post?', [
      {
        text: 'YES',
        onPress: () => this.handleDeleteSubmit(),
        style: 'cancel',
      },
      {
        text: 'NO',
        // onPress: () => console.log('user cancelled deletion'),
      },
    ]);
  };

  handleDeleteSubmit = () => {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL + '/backend/post/delete/' + this.state.data.id,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          // console.log(response.status)
        }
      })
      .catch((err) => {
        // console.log(err)
      });
    this.setState(
      {
        optionsModalOpen: false,
      },
      () => {
        this.props.navigation.goBack();
      },
    );
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({optionsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Post
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState(
                  {optionsModalOpen: false, likeModalOpen: true},
                  () => this.getUsersWhoLiked(this.props.route.params.id),
                );
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Like_FL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  See who liked it
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  optionsModalOpen: false,
                  peopleSharedModalOpen: true,
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  See who shared it
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({optionsModalOpen: false, shareModalOpen: true});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Share
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.handleHideModal();
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Hide
              </Text>
              <Icon
                name="Hide"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            {this.state.data.creatorId === this.state.userId && (
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [defaultShape.ActList_Cell_Gylph_Alt]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({optionsModalOpen: false}, () =>
                    this.props.navigation.navigate('EditPost', {
                      pressedActivityId: this.state.data.id,
                      entityType: this.state.data.postType,
                      description: this.state.data.description,
                      hashTags: this.state.data.hashTags,
                    }),
                  );
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Edit Post
                </Text>
                <Icon
                  name="EditBox"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            )}

            {this.state.data.canReport ? (
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    optionsModalOpen: false,
                    reasonForReportingModalOpen: !this.state.isReported
                      ? true
                      : false,
                  });
                  this.state.isReported
                    ? setTimeout(() => {
                        Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Your report request was already taken',
                          duration: Snackbar.LENGTH_LONG,
                        });
                      }, 1000)
                    : null;
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Report
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [defaultShape.ActList_Cell_Gylph_Alt]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.deletePostAlert();
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Delete Post
                </Text>
                <Icon
                  name="Hide"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </Modal>
    );
  };

  handleHideModal = () => {
    let data = {
      userId: this.state.userId,
      activityId: this.props.route.params.id,
      entityType: 'POST',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          console.log('response.status', response.status);
          this.props.navigation.goBack();
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          console.log('response.status', err.response.status);
          this.props.navigation.goBack();
        }
      });
    // this.setState({ optionsModalOpen: false, forumData: this.state.forumData.filter((item) => item.id !== id) })
  };

  documentPicker = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      for (const res of results) {
        // console.log(res);
        this.setState({
          commentAttachments: [...this.state.commentAttachments, res],
        });
      }
      this.setState({...this.state});
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  downloadProgressWrapper = () => {
    return (
      <View>
        <Modal
          isVisible={this.state.isDownloading}
          backdropColor={'#ffffff'}
          animationIn={'slideInUp'}
          animationInTiming={300}
          animationOut={'slideOutDown'}
          animationOutTiming={300}
          avoidKeyboard={true}>
          <View>
            {this.state.progressLimit == 1 ? null : (
              <Progress.Pie
                style={styles.downloadInProgress}
                progress={this.state.progressLimit}
                size={50}
                color={'green'}
              />
            )}
            {this.state.progressLimit == 1 ? (
              <Text style={styles.downloadInComplete}>
                File Download Successfully
              </Text>
            ) : null}
          </View>
        </Modal>
      </View>
    );
  };

  readStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.writeStorage(data);
      } else {
        // Permission Denied
        alert('Read Storage Permission Denied');
      }
    } else {
      this.getDownloadFile(data); //work on ios
    }
  };

  writeStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.getDownloadFile(data);
      } else {
        // Permission Denied
        alert('Write Storage Permission Denied');
      }
    }
  };

  getDownloadFile = (urlDownload) => {
    if (this.state.downloadStart == true) {
      return;
    } else {
      this.setState({downloadStart: true});
    }

    let directoryFile;
    let dirs;

    let fileName;
    try {
      fileName = urlDownload.split('-').pop();
    } catch (e) {
      fileName = uuidv4();
    }
    if (Platform.OS === 'android') {
      directoryFile = RNFetchBlob.fs.dirs;
      dirs = directoryFile.DownloadDir + '/Download' + fileName;
    } else {
      directoryFile = RNFS.DocumentDirectoryPath;
      dirs = directoryFile + fileName;
    }

    try {
      RNFetchBlob.config({
        path: dirs,
      })
        .fetch('GET', urlDownload, {})
        .progress((received, total) => {
          let temp = parseFloat(received / total);

          if (temp > 0.9) {
            temp = 1;
            setTimeout(() => {
              this.setState({isDownloading: false});
            }, 1000);
          }
          this.setState(
            {
              isDownloading: true,
              progressLimit: temp,
            },
            () => this.downloadProgressWrapper(),
          );
        })
        .then((res) => {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'File downloaded successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });

          this.setState({
            ...this.state,
            downloadStart: false,
          });
          setTimeout(() => {
            this.setState({isDownloading: false});
          }, 1000);
        });
    } catch (error) {}
  };

  getPost = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/post/get?id=' +
        this.props.route.params.id +
        '&postRequestingUserId=' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        this.setState({data: response.data.body});
      })
      .catch((err) => {
        console.log('error individual post : ', err);
      });
  };

  getAllComment = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/comment/getCommentsByActivityId/' +
        this.props.route.params.id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log("comments list: ", response.data.body.content)
          this.setState({commentData: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  unixTime = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    return day + ' ' + month + ' ' + year;
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + 1 + ' s';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + ' m';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + ' h';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + ' d';
    }

    if (difference >= 864000) {
      return day + ' ' + month + ' ' + year;
    }
  };

  deleteComment = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/comment/delete?commentId=' +
        this.state.pressedActivityId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          console.log(res.status);
          this.getCommentsData(this.state.userId);
          // this.state.commentData.filter(item => this.setState({ commentData: item.id !== this.state.pressedActivityId }))
        }
      })
      .catch((err) => {
        console.log(err);
      });
    this.setState({optionsModal2Open: false, commentBody: ''});
  };

  handleCommentEdit = () => {
    let formData = {
      commentId: this.state.pressedActivityId,
      description: this.state.commentBody.trim(),
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/comment/edit',
      data: formData,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          // console.log(res.status)
          this.getCommentsData(this.state.userId);
        }
      })
      .catch((err) => {
        console.log(err);
      });
    this.setState({edit: false, commentBody: '', reply: false});
  };

  optionsModal2 = () => {
    return (
      <Modal
        visible={this.state.optionsModal2Open}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({optionsModal2Open: false, commentBody: ''})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          {this.state.userId !== this.state.pressedUserId ? (
            <View style={defaultShape.Modal_Categories_Container}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    reasonForReportingModalOpen: !this.state.isReported
                      ? true
                      : false,
                    optionsModal2Open: false,
                    commentBody: '',
                  });
                  this.state.isReported
                    ? setTimeout(() => {
                        Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Your report request was already taken',
                          duration: Snackbar.LENGTH_LONG,
                        });
                      }, 500)
                    : null;
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Report
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={defaultShape.Modal_Categories_Container}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({optionsModal2Open: false, edit: true});
                  this.textInputField.focus();
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Edit
                </Text>
                <Icon
                  name="EditBox"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.deleteComment()}
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  Delete
                </Text>
                <Icon
                  name="TrashBin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  };

  renderReplyItem = (item) => {
    console.log('renderReplyItem', item);
    let str = item.item.userComment.replace(/&nbsp;/g, ' ');
    return (
      <View style={{backgroundColor: COLORS.grey_300}}>
        <View
          style={[
            styles.commentItemView,
            {
              backgroundColor: COLORS.bgfill,
              width: '95%',
              alignSelf: 'flex-end',
            },
          ]}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                item.item.userType === 'INDIVIDUAL'
                  ? this.state.userId === item.item.userId
                    ? this.props.navigation.navigate('ProfileStack', {
                        screen: 'ProfileScreen',
                        // params: { userId: item.item.userId },
                      })
                    : this.props.navigation.navigate('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.item.userId},
                      })
                  : this.props.navigation.navigate('ProfileStack', {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.item.userId},
                    })
              }
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                source={
                  item.item.profileImage
                    ? {uri: item.item.profileImage}
                    : item.item.userType === 'INDIVIDUAL'
                    ? defaultProfile
                    : {uri: 'https://cdn.dscovr.com/images/DefaultBusiness.png'}
                }
                style={defaultShape.Media_Round}
              />
              <Text
                numberOfLines={1}
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_800, marginLeft: 6, maxWidth: 160},
                ]}>
                {item.item.userName}
              </Text>
              <Text
                style={[
                  {
                    color:
                      ((item.item && item.item.connectDepth === -1) ||
                        (item.item && item.item.connectDepth === 0)) &&
                      !item.item.followed
                        ? '#97a600'
                        : '#888',
                    fontSize: 14,
                    marginLeft: 4,
                  },
                  typography.Note2,
                ]}>
                {item.item &&
                item.item.userConnectStatus &&
                item.item &&
                item.item.connectDepth === 1
                  ? '• 1st'
                  : item.item && item.item.connectDepth === 2
                  ? '• 2nd'
                  : ''}
              </Text>
            </TouchableOpacity>

            <View style={{flexDirection: 'row'}}>
              <Text
                style={[
                  typography.Note,
                  {
                    color: COLORS.altgreen_300,
                    fontFamily: 'Montserrat-Medium',
                    marginTop: 5,
                  },
                ]}>
                {this.unixTime2(item.item.time)}
              </Text>
              <TouchableOpacity
                style={defaultShape.Nav_Gylph_Btn}
                onPress={() => {
                  this.setState(
                    {
                      optionsModal2Open: true,
                      pressedUserId: item.item.userId,
                      pressedActivityId: item.item.id,
                    },
                    () => this.verifyReported(),
                  );
                  setTimeout(() => {
                    this.setState({
                      commentBody: str.replace(/<br\s*[\/]?>/gi, '\n'),
                    });
                  }, 500);
                }}>
                <Icon
                  name="Kebab"
                  color={COLORS.altgreen_400}
                  size={14}
                  style={{marginTop: -8}}
                />
              </TouchableOpacity>
            </View>
          </View>

          <Text style={[typography.Body_2, styles.commentBody]}>
            {str.replace(/<br\s*[\/]?>/gi, '\n')}
          </Text>

          {item.item.attachmentIds.length > 0 ? (
            <FlatList
              keyboardShouldPersistTaps="handled"
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{marginTop: 10, paddingLeft: 32, paddingRight: 30}}
              contentContainerStyle={{paddingRight: 40}}
              data={item.item.attachmentIds}
              keyExtractor={(attachmentItem) => attachmentItem.id}
              renderItem={(attachmentItem) => (
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {
                    this.readStorage(attachmentItem.item.attachmentUrl);
                  }}
                  style={[
                    defaultShape.ContextBtn_FL,
                    {paddingHorizontal: 5, marginRight: 10},
                  ]}>
                  {/* <Text style={[typography.Caption, { color: COLORS.dark_500 }]}>{attachmentItem.item.attachmentUrl.split("-").pop()}</Text> */}
                  <Image
                    source={{uri: attachmentItem.item.attachmentUrl}}
                    style={{width: 250, height: 160, borderRadius: 10}}
                  />
                </TouchableOpacity>
              )}
            />
          ) : (
            <></>
          )}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: 5,
            }}>
            <View></View>

            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({reply: true, replyExpand: true});
                  this.textInputField.focus();
                }}
                style={[
                  defaultShape.Nav_Gylph_Btn,
                  {flexDirection: 'row', marginRight: 5},
                ]}>
                <Icon name="Reply" color={COLORS.altgreen_300} size={14} />
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.altgreen_300,
                      marginLeft: 5,
                      marginTop: Platform.OS === 'ios' ? 0 : -8,
                    },
                  ]}>
                  Reply
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleLike(item.item.id, item.item.liked)}
                style={defaultShape.Nav_Gylph_Btn}>
                <Icon
                  name={item.item.liked ? 'Like_FL' : 'Like'}
                  color={COLORS.green_500}
                  size={14}
                  style={{marginTop: 3}}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: 20,
                  height: 20,
                  marginRight: 8,
                  marginLeft: -12,
                  marginTop: Platform.OS === 'ios' ? 4 : 8,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() =>
                  this.setState({likeModalOpen: true}, () =>
                    this.getUsersWhoLiked(item.item.id),
                  )
                }>
                <Text
                  style={[{color: COLORS.altgreen_300}, typography.Caption]}>
                  {item.item.likesCount}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  };

  renderCommentItem = (item) => {
    console.log('renderCommentItem', item);
    let str = item.item.userComment.replace(/&nbsp;/g, ' ');
    return (
      <>
        <View style={styles.commentItemView}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                item.item.userType === 'INDIVIDUAL'
                  ? this.state.userId === item.item.userId
                    ? this.props.navigation.navigate('ProfileStack', {
                        screen: 'ProfileScreen',
                        // params: { userId: item.item.userId },
                      })
                    : this.props.navigation.navigate('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.item.userId},
                      })
                  : this.props.navigation.navigate('ProfileStack', {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.item.userId},
                    })
              }
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                source={
                  item.item.profileImage
                    ? {uri: item.item.profileImage}
                    : item.item.userType === 'INDIVIDUAL'
                    ? defaultProfile
                    : {uri: 'https://cdn.dscovr.com/images/DefaultBusiness.png'}
                }
                style={defaultShape.Media_Round}
              />
              <Text
                numberOfLines={1}
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_800, marginLeft: 6, maxWidth: 160},
                ]}>
                {item.item.userName}
              </Text>
              <Text
                style={[
                  {
                    color:
                      ((item.item && item.item.connectDepth === -1) ||
                        (item.item && item.item.connectDepth === 0)) &&
                      !item.item.followed
                        ? '#97a600'
                        : '#888',
                    fontSize: 14,
                    marginLeft: 4,
                  },
                  typography.Note2,
                ]}>
                {item.item &&
                item.item.userConnectStatus &&
                item.item &&
                item.item.connectDepth === 1
                  ? '• 1st'
                  : item.item && item.item.connectDepth === 2
                  ? '• 2nd'
                  : ''}
              </Text>
            </TouchableOpacity>

            <View style={{flexDirection: 'row'}}>
              <Text
                style={[
                  typography.Note,
                  {
                    color: COLORS.altgreen_300,
                    fontFamily: 'Montserrat-Medium',
                    marginTop: 5,
                  },
                ]}>
                {this.unixTime2(item.item.time)}
              </Text>
              <TouchableOpacity
                style={defaultShape.Nav_Gylph_Btn}
                onPress={() => {
                  this.setState(
                    {
                      optionsModal2Open: true,
                      pressedUserId: item.item.userId,
                      pressedActivityId: item.item.id,
                    },
                    () => this.verifyReported(),
                  );
                  setTimeout(() => {
                    this.setState({
                      commentBody: str.replace(/<br\s*[\/]?>/gi, '\n'),
                    });
                  }, 500);
                }}>
                <Icon
                  name="Kebab"
                  color={COLORS.altgreen_400}
                  size={14}
                  style={{marginTop: -8}}
                />
              </TouchableOpacity>
            </View>
          </View>

          <Text style={[typography.Body_2, styles.commentBody]}>
            {str.replace(/<br\s*[\/]?>/gi, '\n')}
          </Text>

          {item.item.attachmentIds.length > 0 ? (
            <FlatList
              keyboardShouldPersistTaps="handled"
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{marginTop: 10, paddingLeft: 32, paddingRight: 30}}
              contentContainerStyle={{paddingRight: 40}}
              data={item.item.attachmentIds}
              keyExtractor={(attachmentItem) => attachmentItem.id}
              renderItem={(attachmentItem) => (
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {
                    this.readStorage(attachmentItem.item.attachmentUrl);
                  }}
                  style={[
                    defaultShape.ContextBtn_FL,
                    {paddingHorizontal: 5, marginRight: 10},
                  ]}>
                  {/* <Text style={[typography.Caption, { color: COLORS.dark_500 }]}>{attachmentItem.item.attachmentUrl.split("-").pop()}</Text> */}
                  <Image
                    source={{uri: attachmentItem.item.attachmentUrl}}
                    style={{width: 250, height: 160, borderRadius: 10}}
                  />
                </TouchableOpacity>
              )}
            />
          ) : (
            <></>
          )}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: 5,
            }}>
            {item.item.replies.length > 0 ? (
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    replyExpandId: item.item.id,
                    replyExpand: !this.state.replyExpand,
                  })
                }
                style={{flexDirection: 'row', marginLeft: 38}}>
                <View
                  style={{
                    backgroundColor: COLORS.altgreen_300,
                    height: 14,
                    width: 14,
                    borderRadius: 7,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginRight: 5,
                  }}>
                  <Icon
                    name="Arrow_Down"
                    color={COLORS.white}
                    size={10}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                </View>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-Medium',
                    },
                  ]}>
                  {item.item.replies.length}{' '}
                  {item.item.replies.length === 1 ? 'reply' : 'replies'}
                </Text>
              </TouchableOpacity>
            ) : (
              <View></View>
            )}

            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    reply: true,
                    replyExpand: true,
                    replyExpandId: item.item.id,
                  });
                  this.textInputField.focus();
                }}
                style={[
                  defaultShape.Nav_Gylph_Btn,
                  {flexDirection: 'row', marginLeft: -14},
                ]}>
                <Icon
                  name="Reply"
                  color={COLORS.altgreen_300}
                  size={14}
                  style={{marginRight: 5}}
                />
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.altgreen_300,
                      marginTop: Platform.OS === 'ios' ? 0 : -8,
                    },
                  ]}>
                  Reply
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleLike(item.item.id, item.item.liked)}
                style={defaultShape.Nav_Gylph_Btn}>
                <Icon
                  name={item.item.liked ? 'Like_FL' : 'Like'}
                  color={COLORS.green_500}
                  size={14}
                  style={{marginTop: 3}}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: 20,
                  height: 20,
                  marginRight: 8,
                  marginLeft: -12,
                  marginTop: Platform.OS === 'ios' ? 4 : 8,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() =>
                  this.setState({likeModalOpen: true}, () =>
                    this.getUsersWhoLiked(item.item.id),
                  )
                }>
                <Text
                  style={[{color: COLORS.altgreen_300}, typography.Caption]}>
                  {item.item.likesCount}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        {this.state.replyExpandId === item.item.id &&
        this.state.replyExpand === true ? (
          <View>
            <FlatList
              // style={{ marginTop: this.state.setreadmore ? 0 : 10 }}
              // horizontal
              // showsHorizontalScrollIndicator={false}
              keyboardShouldPersistTaps="handled"
              data={item.item.replies}
              keyExtractor={(rep) => rep.id}
              renderItem={(rep) => this.renderReplyItem(rep)}
            />
          </View>
        ) : (
          <></>
        )}
      </>
    );
  };

  handleLike = (activityId, liked) => {
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log('********* Hurray ************')
          // this.setState({ ...this.state })
          this.getPost();
          this.getAllComment();
        } else {
          console.log('response liked :', response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleCommentSubmit = () => {
    const formData = new FormData();

    this.setState({commentBody: '', commentAttachments: []});

    let params = {
      userId: this.state.userId,
      activityId: this.props.route.params.id,
      userComment: this.state.commentBody.trim('\n'),
    };

    let replyParams = {
      userId: this.state.userId,
      activityId: this.state.replyExpandId,
      userComment: this.state.commentBody.trim('\n'),
      commentType: 'REPLY',
    };

    if (
      this.state.commentAttachments &&
      this.state.commentAttachments.length > 0
    ) {
      this.state.commentAttachments.forEach((file) => {
        formData.append('files', file);
      });
    }

    if (this.state.reply) {
      formData.append('data', JSON.stringify(replyParams));
    } else {
      formData.append('data', JSON.stringify(params));
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/comment/create',
      data: formData,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          this.setState({
            data: {
              ...this.state.data,
              commentCount: this.state.data.commentCount + 1,
            },
            reply: false,
          });
          // if (typeof this.props.getAllComments == 'function') {
          //     this.props.getAllComments();
          // }
          // this.setState({
          //     'isCommentSubmitted': false,
          //     'userComment': '',
          //     'commentAttachments': []
          // });
          // console.log("posted comment: ", response.data)
          this.getAllComment();
        } else {
          console.log(response);
          // this.setState({'isCommentSubmitted': false});
        }
      })
      .catch((err) => {
        console.log(err.response);
        // this.setState({'isCommentSubmitted': false});
      });
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message: REACT_APP_domainUrl + '/post/' + this.props.route.params.id,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            {/* <TouchableOpacity style={Platform.OS === 'ios' ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingTop: 25, paddingBottom: 15 }] : defaultShape.ActList_Cell_Gylph_Alt} activeOpacity={0.6} onPress={() => { this.setState({ shareModalOpen: false }) }}>
                            <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>Repost on WeNaturalists</Text>
                            <Icon name='Forward' size={17} color={COLORS.altgreen_300} style={Platform.OS === 'android' ? { marginTop: 8 } : {}} />

                        </TouchableOpacity> */}

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation.navigate('RepostOnWenat', {
                    link: false,
                    pressedActivityId: this.props.route.params.id,
                    entityType: 'POST',
                  }),
                );
              }}>
              <Text
                style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl + '/post/' + this.props.route.params.id,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Envelope"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  trimDescription = (item) => {
    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  handleFollowUnfollow = (isFollowed, userId) => {
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          console.log(response.status);
          this.individualPostResponse(this.state.userId);
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderSharedPost = (item) => {
    return (
      <View style={styles.renderSharedPost}>
        {item.userId !== this.state.userId && this.renderSharedHeader(item)}

        {item.description ? (
          this.renderDescription(item)
        ) : (
          <View style={{backgroundColor: 'transparent', height: 10}}></View>
        )}

        {item.news_url ? (
          <RNUrlPreview
            text={item.news_url}
            titleNumberOfLines={1}
            titleStyle={[typography.Title_2, {color: COLORS.dark_800}]}
            descriptionStyle={[
              typography.Note,
              {fontFamily: 'Montserrat-Medium', color: COLORS.altgreen_300},
            ]}
            containerStyle={{
              width: '100%',
              flexDirection: 'column',
              backgroundColor: COLORS.altgreen_100,
            }}
            imageStyle={{width: '100%', height: 150, marginTop: -26}}
            imageProps={{width: '100%', height: 150}}
          />
        ) : (
          <></>
        )}

        <View style={{width: '103%'}}>
          {item.attachmentIds && item.attachmentIds.length ? (
            this.renderImage(item)
          ) : (
            <></>
          )}
          {item.title ? (
            <Text
              style={[typography.Button_Lead, {color: COLORS.dark_800}]}
              onPress={() =>
                this.props.navigation.navigate('ForumStack', {
                  screen: 'ForumDetails',
                  params: {slug: item.slug, userId: item.userId},
                })
              }>
              {item.title}
            </Text>
          ) : (
            <Text></Text>
          )}

          {item.title && this.renderHashTags(item)}

          {item.question ? (
            <Text
              style={[typography.Button_Lead, {color: COLORS.dark_800}]}
              onPress={() =>
                this.props.navigation.navigate('PollStack', {
                  screen: 'PollDetails',
                  params: {slug: item.slug, userId: item.userId},
                })
              }>
              {item.question}
            </Text>
          ) : (
            <Text></Text>
          )}
        </View>

        <View style={{marginLeft: 14}}>
          {item.secondaryAttachmentIds &&
          item.secondaryAttachmentIds.length > 0 ? (
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}
              onPress={() =>
                this.setState({attachmentModalopen: true, causeItem: item})
              }>
              <Icon
                name="Clip"
                color={COLORS.green_500}
                size={14}
                style={{
                  marginTop: Platform.OS === 'android' ? 10 : 0,
                  marginRight: 6,
                }}
              />
              <Text style={[{color: COLORS.altgreen_300}, defaultStyle.Note2]}>
                {item.secondaryAttachmentIds.length === 1
                  ? item.secondaryAttachmentIds.length + ' File '
                  : item.secondaryAttachmentIds.length + ' Files '}{' '}
                Attached
              </Text>
            </TouchableOpacity>
          ) : (
            <Text></Text>
          )}
        </View>
      </View>
    );
  };

  renderHashTags = (item) => {
    return (
      <ScrollView
        contentContainerStyle={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          marginLeft: 6,
          marginTop: 8,
        }}>
        {item.hashTags.map((hashTag, index) => (
          <View key={index}>
            <Text
              style={[
                typography.Subtitle_2,
                {color: '#bfc52e', marginRight: 8},
              ]}>
              #{hashTag}
            </Text>
          </View>
        ))}
      </ScrollView>
    );
  };

  renderDescription = (item) => {
    return (
      <Autolink
        text={this.trimDescription(tagDescription(item.description))}
        email
        hashtag="instagram"
        mention="twitter"
        phone="sms"
        numberOfLines={5}
        matchers={[
          {
            pattern: /@\[([^[]*)]\(([^(^)]*)\)/g,
            style: {color: COLORS.mention_color, fontWeight: 'bold'},
            getLinkText: (replacerArgs) => `${replacerArgs[2]}`,
            onPress: (match) => {
              this.getUserDetailsByCustomUrl(match.getReplacerArgs()[1]);
            },
          },
          {
            pattern: /#\[([^[]*)]\(([^(^)]*)\)/g,
            style: {color: COLORS.mention_color, fontWeight: 'bold'},
            getLinkText: (replacerArgs) => `#${replacerArgs[2]}`,
            onPress: (match) => {
              this.props.navigate('HashTagDetail', {
                slug: match.getReplacerArgs()[1],
              });
            },
          },
        ]}
        style={[
          typography.Body_1,
          {
            color:
              item.postType === 'ARTICLE' ? COLORS.dark_500 : COLORS.dark_700,
            marginLeft: 6,
            marginRight: 17,
            marginVertical: 8,
          },
        ]}
        url
      />
    );
  };
  renderImage = (item) => {
    if (item.attachmentIds) {
      if (item.attachmentIds.length > 5) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[3].attachmentUrl}}
              />

              <ImageBackground
                source={{uri: item.attachmentIds[4].attachmentUrl}}
                imageStyle={{}}
                style={{height: 120, width: '57.8%'}}>
                <View
                  style={{
                    height: 120,
                    width: '57.8%',
                    backgroundColor: COLORS.dark_900 + 'BF',
                    opacity: 0.95,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat-Bold',
                      fontSize: 28,
                      color: COLORS.white,
                    }}>
                    +{item.attachmentIds.length - 4}
                  </Text>
                </View>
              </ImageBackground>
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 5) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
              <Image
                style={{height: 182, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[3].attachmentUrl}}
              />
              <Image
                style={{height: 120, width: '33.33%'}}
                source={{uri: item.attachmentIds[4].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 4) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[3].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 3) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '100%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
              <Image
                style={{height: 151, width: '50%'}}
                source={{uri: item.attachmentIds[2].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 2) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '100%'}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
            </View>

            <View style={{flexDirection: 'row', marginLeft: -10}}>
              <Image
                style={{height: 151, width: '100%'}}
                source={{uri: item.attachmentIds[1].attachmentUrl}}
              />
            </View>
          </TouchableOpacity>
        );
      } else if (item.attachmentIds.length === 1) {
        return (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() =>
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: item.id,
              })
            }>
            {/* {this.state.selected === 'AUDIO' && item.attachmentIds[0].attachmentUrl || this.state.selected === 'VIDEOS' ? */}
            {item.attachmentIds[0].attachmentType === 'AUDIO' &&
            this.state.selected !== 'VIDEOS' ? (
              <VideoPlayer
                style={{width: '97%', height: 190}}
                // disableBack={true}
                tapAnywhereToPause={true}
                // onBack={() => this.setState({ hideControls: !this.state.hideControls })}
                // onError={() => this.setState({ showVideo: false })}
                disableFullscreen={true}
                // disablePlayPause={true}
                disableSeekbar={true}
                disableVolume={true}
                disableTimer={true}
                disableBack={true}
                paused={true}
                // source={{ uri: item.attachmentIds[0].attachmentUrl }}
                source={{
                  uri:
                    'https://dscovr-assets-dev.s3.amazonaws.com/posts/1603347470339-file_example_OGG_1920_13_3mg.ogg',
                }}
                navigator={this.props.navigator}
              />
            ) : item.attachmentIds[0].attachmentType === 'VIDEO' &&
              this.state.selected !== 'AUDIO' ? (
              <VideoPlayer
                style={{
                  width: this.state.videoWidth,
                  height: this.state.videoHeight,
                  // position: "absolute",
                  // top: 0,
                  // left: 0,
                  // bottom: 0,
                  // right: 0,
                }}
                poster={item.attachmentIds[0].thumbnails[0]}
                // disableBack={true}
                tapAnywhereToPause={true}
                // onBack={() => this.setState({ hideControls: !this.state.hideControls })}
                // onError={() => this.setState({ showVideo: false })}
                // disableFullscreen={true}
                // disablePlayPause={true}
                disableSeekbar={true}
                disableVolume={true}
                disableTimer={true}
                disableBack={true}
                // onEnterFullscreen={() => this.setState({ videoHeight: screenHeight, videoWidth: screenWidth })}
                // onExitFullscreen={() => this.setState({ videoHeight: 190, videoWidth: '97%' })}
                paused={true}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
                navigator={this.props.navigator}
                // fullscreen={true}
                // resizeMode="cover"
              />
            ) : (
              <Image
                style={{height: 345, marginLeft: -10}}
                source={{uri: item.attachmentIds[0].attachmentUrl}}
              />
            )}
          </TouchableOpacity>
        );
      }
      // }

      // else if(this.state.selected === 'VIDEOS' && this.props.userFeedsPhotos.body) {
      //   return (
      //     <Video style={{ width: 300, height: 200 }}
      //     paused={true}
      //     source={{ uri: item.attachmentIds[0].attachmentUrl }} />
      //   )
      // }
    } else return <></>;
  };
  renderSharedHeader = (item) => {
    return (
      <View style={styles.unreadNotiItem}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            item.userType === 'INDIVIDUAL' && item.userId === this.state.userId
              ? this.props.navigation.navigate('ProfileStack')
              : item.userType === 'INDIVIDUAL' &&
                item.userId !== this.state.userId
              ? this.props.navigation.navigate('ProfileStack', {
                  screen: 'OtherProfileScreen',
                  params: {userId: item.userId},
                })
              : item.userType === 'COMPANY'
              ? this.props.navigation.navigate('ProfileStack', {
                  screen: 'CompanyProfileScreen',
                  params: {userId: item.userId},
                })
              : this.props.navigation.navigate('CircleProfileStack', {
                  screen: 'CircleProfile',
                  params: {slug: item.params.circleSlug},
                });
            // console.log(item)
          }}
          style={styles.unreadNotiItem}>
          {item.profileImage ? (
            <Image
              style={[defaultShape.Media_Round, {}]}
              source={{uri: item.profileImage}}
            />
          ) : item.params && item.params.circleImage ? (
            <Image
              style={[defaultShape.Media_Round, {}]}
              source={{uri: item.params.circleImage}}
            />
          ) : item.userType === 'INDIVIDUAL' && !item.profileImage ? (
            <Image
              style={[defaultShape.Media_Round, {backgroundColor: 'orange'}]}
              source={defaultProfile}
            />
          ) : item.userType === 'COMPANY' && !item.profileImage ? (
            <Image
              style={[defaultShape.Media_Round, {backgroundColor: 'orange'}]}
              source={defaultBusiness}
            />
          ) : item.params &&
            item.params.circleSlug &&
            !item.params.circleImage ? (
            <Image
              style={[defaultShape.Media_Round, {}]}
              source={circleDefault}
            />
          ) : (
            <></>
          )}

          <View style={{}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                numberOfLines={1}
                style={[
                  defaultStyle.Title_2,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 8,
                  },
                ]}>
                {item.username
                  ? item.username
                  : item.params && item.params.circleTitle}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                marginLeft: 5,
              }}>
              {item.addressDetail && item.addressDetail.country && (
                <Icon
                  name="Location"
                  color={COLORS.altgreen_300}
                  size={10}
                  style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                />
              )}
              {item.addressDetail && item.addressDetail.country && (
                <Text
                  onPress={() =>
                    this.props.navigation.navigate('IndividualFeedsPost', {
                      id: item.id,
                    })
                  }
                  style={[
                    {
                      color: COLORS.altgreen_300,
                      marginLeft: 2,
                      fontSize: 10,
                      fontFamily: 'Montserrat-Medium',
                    },
                  ]}>
                  {item.addressDetail && item.addressDetail.country
                    ? item.addressDetail.country
                        .split(' ')
                        .splice(0, 2)
                        .join(' ')
                    : null}
                  {/* {+ new Date() - item.createTime} */}
                </Text>
              )}
              {item.addressDetail && item.addressDetail.country && (
                <Icon
                  name="Bullet_Fill"
                  color={COLORS.altgreen_300}
                  size={8}
                  style={{marginTop: Platform.OS === 'android' ? 0 : 0}}
                />
              )}

              <Text
                onPress={() =>
                  this.props.navigation.navigate('IndividualFeedsPost', {
                    id: item.id,
                  })
                }
                style={[
                  {
                    marginLeft: 2,
                    fontSize: 10,
                    fontFamily: 'Montserrat-Medium',
                  },
                ]}>
                <TimeAgo time={item.createTime} />
                {/* {+ new Date() - item.createTime} */}
              </Text>
            </View>
          </View>

          {/* <TouchableOpacity activeOpacity={0.5} style={[defaultShape.Nav_Gylph_Btn, {}]}
          onPress={() => this.setState({ optionsModalOpen: true, pressedActivityId: item.id })}>
          <Icon name="Kebab" color={COLORS.altgreen_300} size={14} style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }} />
        </TouchableOpacity> */}
        </TouchableOpacity>
      </View>
    );
  };

  navigateToProfile = (item) => {
    return item.userType === 'INDIVIDUAL'
      ? this.state.userId === item.userId
        ? this.props.navigation.navigate('ProfileStack', {
            screen: 'ProfileScreen',
            // params: { userId: item.userId },
          })
        : this.props.navigation.navigate('ProfileStack', {
            screen: 'OtherProfileScreen',
            params: {userId: item.userId},
          })
      : this.props.navigation.navigate('ProfileStack', {
          screen: 'CompanyProfileScreen',
          params: {userId: item.userId},
        });
  };

  navigate = (value, params) => {
    this.props.navigation.navigate(value, params);
  };

  getUserDetailsByCustomUrl = (customurl) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get_by_custom_url?customUrl=' +
        customurl +
        '&otherUserId=' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          response.data.body &&
          response.data.body.type === 'INDIVIDUAL' &&
          response.data.body.userId === this.state.userId
            ? this.props.navigation.navigate('ProfileStack')
            : response.data.body.type === 'INDIVIDUAL' &&
              response.data.body.userId !== this.state.userId
            ? this.props.navigation.navigate('ProfileStack', {
                screen: 'OtherProfileScreen',
                params: {userId: response.data.body.userId},
              })
            : response.data.body.type === 'COMPANY'
            ? this.props.navigation.navigate('ProfileStack', {
                screen: 'CompanyProfileScreen',
                params: {userId: response.data.body.userId},
              })
            : this.props.navigation.navigate('CircleProfileStack', {
                screen: 'CircleProfile',
                params: {slug: customurl},
              });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.optionsModal()}
        {this.optionsModal2()}
        {this.shareModal()}
        {this.likeModal()}
        {this.reasonForReportingModal()}
        {this.peopleSharedModal()}
        {this.attachmentModal()}

        {/****** Header start ******/}

        {this.state.fullscreen ? (
          <></>
        ) : (
          <View style={styles.header}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => this.props.navigation.goBack()}
                style={defaultShape.Nav_Gylph_Btn}>
                <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() =>
                  this.state.data.userType === 'INDIVIDUAL'
                    ? this.state.userId === this.state.data.userId
                      ? this.props.navigation.navigate('ProfileStack', {
                          screen: 'ProfileScreen',
                          // params: { userId: this.state.data.userId },
                        })
                      : this.props.navigation.navigate('ProfileStack', {
                          screen: 'OtherProfileScreen',
                          params: {userId: this.state.data.userId},
                        })
                    : this.props.navigation.navigate('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                        params: {userId: this.state.data.userId},
                      })
                }
                style={{flexDirection: 'row', alignItems: 'center'}}>
                {this.state.data.originalProfileImage ? (
                  <Image
                    style={[defaultShape.Media_Round, {}]}
                    source={{uri: this.state.data.originalProfileImage}}
                  />
                ) : this.state.data.params &&
                  this.state.data.params.circleImage ? (
                  <Image
                    style={[defaultShape.Media_Round, {}]}
                    source={{uri: this.state.data.params.circleImage}}
                  />
                ) : this.state.data.userType === 'INDIVIDUAL' &&
                  !this.state.data.originalProfileImage ? (
                  <Image
                    style={[
                      defaultShape.Media_Round,
                      {backgroundColor: 'orange'},
                    ]}
                    source={defaultProfile}
                  />
                ) : this.state.data.userType === 'COMPANY' &&
                  !this.state.data.originalProfileImage ? (
                  <Image
                    style={[
                      defaultShape.Media_Round,
                      {backgroundColor: 'orange'},
                    ]}
                    source={defaultBusiness}
                  />
                ) : this.state.data.params &&
                  this.state.data.params.circleSlug &&
                  !this.state.data.params.circleImage ? (
                  <Image
                    style={[defaultShape.Media_Round, {}]}
                    source={circleDefault}
                  />
                ) : (
                  <></>
                )}
                <View style={{marginLeft: 10}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Title_2,
                        {color: COLORS.dark_800, maxWidth: 160},
                      ]}>
                      {this.state.data.userName
                        ? this.state.data.userName
                        : this.state.data.params &&
                          this.state.data.params.circleTitle
                        ? this.state.data.params.circleTitle
                        : null}
                    </Text>

                    {!this.state.data.deactivated &&
                    this.state.data.userId !== this.state.userId &&
                    !this.state.isCompany ? (
                      <TouchableOpacity
                        onPress={() =>
                          this.handleFollowUnfollow(
                            this.state.data.followed,
                            this.state.data.userId,
                            // index,
                            this.state.data,
                          )
                        }
                        style={{
                          height: 30,
                          width: 30,
                          alignItems: 'center',
                          marginLeft: 4,
                        }}>
                        <Icon
                          name={this.state.data.followed ? 'TickRSS' : 'RSS'}
                          size={13}
                          color={COLORS.dark_600}
                          style={{marginTop: Platform.OS === 'android' ? 4 : 2}}
                        />
                      </TouchableOpacity>
                    ) : null}
                  </View>

                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'flex-start',
                    }}>
                    {this.state.data.country && (
                      <Icon
                        name="Location"
                        color={COLORS.altgreen_300}
                        size={10}
                        style={{marginTop: Platform.OS === 'android' ? -2 : 0}}
                      />
                    )}
                    {this.state.data.country && (
                      <Text
                        onPress={() =>
                          this.props.navigation.navigate(
                            'IndividualFeedsPost',
                            {id: this.state.data.id},
                          )
                        }
                        style={[
                          {
                            color: COLORS.altgreen_300,
                            marginRight: 4,
                            fontSize: 10,
                            fontFamily: 'Montserrat-Medium',
                          },
                        ]}>
                        {this.state.data.country
                          ? this.state.data.country
                              .split(' ')
                              .splice(0, 2)
                              .join(' ')
                          : null}
                        {/* {+ new Date() - item.createTime} */}
                      </Text>
                    )}
                    {this.state.data.country && (
                      <Icon
                        name="Bullet_Fill"
                        color={COLORS.altgreen_300}
                        size={8}
                        style={{marginTop: Platform.OS === 'android' ? 0 : 0}}
                      />
                    )}
                    <Text
                      style={[
                        typography.Note,
                        {
                          color: COLORS.altgreen_300,
                          fontFamily: 'Montserrat-Medium',
                        },
                      ]}>
                      <TimeAgo time={this.state.data.createTime} />
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              style={defaultShape.Nav_Gylph_Btn}
              onPress={() => this.setState({optionsModalOpen: true})}>
              <Icon name="Kebab" color={COLORS.altgreen_300} size={16} />
            </TouchableOpacity>
          </View>
        )}

        {/****** Header ends ******/}

        <ScrollView keyboardShouldPersistTaps="handled">
          {/****** Image Carousel start ******/}

          <View>
            {this.state.data.attachmentIds &&
            this.state.data.attachmentIds.length !== 0 ? (
              <FlatList
                keyboardShouldPersistTaps="handled"
                ref={(ref) => (this.flatlist = ref)}
                initialNumToRender={5}
                horizontal
                showsHorizontalScrollIndicator={false}
                snapToAlignment={'start'}
                snapToInterval={screenWidth}
                decelerationRate={'fast'}
                pagingEnabled
                data={this.state.data.attachmentIds}
                keyExtractor={(item) => item.id}
                renderItem={(item) =>
                  item.item.attachmentType === 'IMAGE' ||
                  item.item.attachmentType === 'COVER_IMAGE' ? (
                    <View>
                      <Image
                        source={{uri: item.item.attachmentUrl}}
                        style={{height: 264, width: screenWidth}}
                        resizeMode="cover"
                      />
                      {this.state.data.attachmentIds.length > 1 ? (
                        <View
                          style={{
                            position: 'absolute',
                            top: '40%',
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            zIndex: 1,
                          }}>
                          <TouchableOpacity
                            style={[
                              defaultShape.ContentSlideCtrl_Left,
                              {
                                backgroundColor:
                                  item.index > 0 ? '#00394DB3' : 'transparent',
                              },
                            ]}
                            onPress={() =>
                              item.index > 0
                                ? this.flatlist.scrollToIndex({
                                    index: item.index - 1,
                                  })
                                : null
                            }>
                            <Text style={{marginRight: 10}}>
                              <Icon
                                name="Arrow_Left"
                                color={
                                  item.index > 0
                                    ? COLORS.altgreen_50
                                    : 'transparent'
                                }
                                size={16}
                              />
                            </Text>
                          </TouchableOpacity>

                          <TouchableOpacity
                            style={[
                              defaultShape.ContentSlideCtrl_Right,
                              {
                                backgroundColor:
                                  item.index <
                                  this.state.data.attachmentIds.length - 1
                                    ? '#00394DB3'
                                    : 'transparent',
                              },
                            ]}
                            onPress={() =>
                              item.index ===
                              this.state.data.attachmentIds.length - 1
                                ? null
                                : this.flatlist.scrollToIndex({
                                    index: item.index + 1,
                                  })
                            }>
                            <Text style={{marginLeft: 10}}>
                              <Icon
                                name="Arrow_Right"
                                color={
                                  item.index <
                                  this.state.data.attachmentIds.length - 1
                                    ? COLORS.altgreen_50
                                    : 'transparent'
                                }
                                size={16}
                              />
                            </Text>
                          </TouchableOpacity>
                        </View>
                      ) : (
                        <></>
                      )}
                    </View>
                  ) : (
                    <VideoPlayer
                      style={{
                        width: screenWidth,
                        height: this.state.fullscreen ? screenHeight : 264,
                      }}
                      poster={
                        item.item.thumbnails && item.item.thumbnails[0]
                          ? item.item.thumbnails[0]
                          : 'https://www.agbiz.co.za/Content/images/audio.jpg'
                      }
                      tapAnywhereToPause={true}
                      onEnterFullscreen={() =>
                        this.setState({fullscreen: true})
                      }
                      onExitFullscreen={() =>
                        this.setState({fullscreen: false})
                      }
                      disableSeekbar={true}
                      disableVolume={true}
                      disableBack={true}
                      source={{uri: item.item.attachmentUrl}}
                      fullscreen={this.state.fullscreen}
                      fullscreenOrientation="landscape"
                      paused={true}
                    />
                  )
                }
              />
            ) : this.state.data.postType !== 'POST' &&
              this.state.data.postType !== 'LINK' ? (
              <Image
                source={BlogDefault}
                style={{width: '100%', height: 150}}
              />
            ) : (
              <></>
            )}
          </View>

          {/****** Image Carousel ends ******/}

          {/****** Caption start ******/}
          <TouchableOpacity>
            {this.state.data && this.state.data.title !== '' ? (
              <Text
                style={[
                  typography.Button_1,
                  {marginTop: 10, marginLeft: 10, color: COLORS.dark_800},
                ]}>
                {this.state.data.title}
              </Text>
            ) : (
              <></>
            )}
          </TouchableOpacity>
          {this.state.fullscreen ? (
            <></>
          ) : (
            <Autolink
              text={
                this.state.data && this.state.data.description
                  ? this.trimDescription(
                      tagDescription(this.state.data.description),
                    )
                  : ''
              }
              email
              hashtag="instagram"
              mention="twitter"
              phone="sms"
              numberOfLines={
                this.state.setreadmore === false
                  ? null
                  : this.state.readMore
                  ? 1000
                  : 5
              }
              onTextLayout={(e) =>
                e.nativeEvent.lines.length < 5
                  ? this.setState({setreadmore: false})
                  : this.setState({setreadmore: true})
              }
              matchers={[
                {
                  pattern: /@\[([^[]*)]\(([^(^)]*)\)/g,
                  style: {color: COLORS.mention_color, fontWeight: 'bold'},
                  getLinkText: (replacerArgs) => `${replacerArgs[2]}`,
                  onPress: (match) => {
                    this.getUserDetailsByCustomUrl(match.getReplacerArgs()[1]);
                  },
                },
                {
                  pattern: /#\[([^[]*)]\(([^(^)]*)\)/g,
                  style: {color: COLORS.mention_color, fontWeight: 'bold'},
                  getLinkText: (replacerArgs) => `#${replacerArgs[2]}`,
                  onPress: (match) => {
                    this.props.navigation.navigate([
                      'HashTagDetail',
                      {slug: match.getReplacerArgs()[1]},
                    ]);
                  },
                },
              ]}
              style={[
                typography.Body_1,
                {
                  color:
                    this.state.data.postType === 'ARTICLE'
                      ? COLORS.dark_500
                      : COLORS.dark_700,
                  marginLeft: 6,
                  marginRight: 17,
                  marginVertical: 8,
                },
              ]}
              url
            />
          )}

          {this.state.setreadmore && !this.state.fullscreen ? (
            <TouchableOpacity
              style={{height: 30, width: 80, marginLeft: 15}}
              onPress={() => this.setState({readMore: !this.state.readMore})}>
              <Text style={{color: COLORS.green_500, fontWeight: '700'}}>
                {this.state.readMore ? 'Read less' : 'Read more'}
              </Text>
            </TouchableOpacity>
          ) : (
            <></>
          )}

          {this.state.data && this.state.data.postType === 'LINK' ? (
            <RNUrlPreview
              text={this.state.data.postLinkTypeUrl}
              titleNumberOfLines={1}
              titleStyle={[typography.Title_2, {color: COLORS.dark_800}]}
              descriptionStyle={[
                typography.Note,
                {
                  fontFamily: 'Montserrat-Medium',
                  color: COLORS.altgreen_300,
                  marginTop: 8,
                },
              ]}
              containerStyle={{
                marginTop: 15,
                width: '100%',
                flexDirection: 'column',
                backgroundColor: COLORS.altgreen_100,
              }}
              imageStyle={{width: '100%', height: 150}}
              imageProps={{width: '100%', height: 150}}
            />
          ) : (
            <></>
          )}

          {/****** Caption ends ******/}

          {/* Share preview start */}
          {this.state.data && this.state.data.sharedEntityId ? (
            this.renderSharedPost(this.state.data.sharedEntityParams)
          ) : this.state.data.sharedEntityId &&
            this.state.data.attachmentIds &&
            this.state.data.attachmentIds.length ? (
            this.renderImage(this.state.data)
          ) : this.state.data.sharedEntityParams &&
            this.state.data.sharedEntityParams['content-not-found'] ? (
            <Text>Content does not exist or has been removed.</Text>
          ) : (
            <></>
          )}
          {/* Share Preview end */}

          {/* Attachement start */}
          <View style={{marginLeft: 14}}>
            {this.state.data &&
            this.state.data.secondaryAttachmentIds &&
            this.state.data.secondaryAttachmentIds.length > 0 ? (
              <TouchableOpacity
                style={{flexDirection: 'row', alignItems: 'center'}}
                onPress={() =>
                  this.setState({
                    attachmentModalopen: true,
                    causeItem: this.state.data,
                  })
                }>
                <Icon
                  name="Clip"
                  color={COLORS.green_500}
                  size={14}
                  style={{
                    marginTop: Platform.OS === 'android' ? 10 : 0,
                    marginRight: 6,
                  }}
                />
                <Text
                  style={[{color: COLORS.altgreen_300}, defaultStyle.Note2]}>
                  {this.state.data.secondaryAttachmentIds.length === 1
                    ? this.state.data.secondaryAttachmentIds.length + ' File '
                    : this.state.data.secondaryAttachmentIds.length +
                      ' Files '}{' '}
                  Attached
                </Text>
              </TouchableOpacity>
            ) : (
              <Text></Text>
            )}
          </View>
          {/* Attachement End */}

          {/****** Hashtags start ******/}

          {this.state.fullscreen ? (
            <></>
          ) : (
            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                style={{marginTop: this.state.setreadmore ? 0 : 10}}
                contentContainerStyle={{paddingRight: 16}}
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.data.hashTags}
                keyExtractor={(item) => item}
                renderItem={(item) => (
                  <Text
                    style={[
                      typography.Subtitle_2,
                      {color: COLORS.grey_350, marginLeft: 15},
                    ]}>
                    #{item.item}
                  </Text>
                )}
              />
            </View>
          )}

          {/****** Hashtags ends ******/}

          {/****** Like & Share start ******/}

          {this.state.fullscreen ? (
            <></>
          ) : (
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 15,
                justifyContent: 'space-between',
                alignItems: 'center',
                height: 44,
              }}>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() =>
                    this.handleLike(
                      this.props.route.params.id,
                      this.state.data.liked,
                    )
                  }>
                  <Icon
                    name={this.state.data.liked ? 'Like_FL' : 'Like'}
                    color={COLORS.green_500}
                    size={14}
                  />
                </TouchableOpacity>
                <Text
                  style={[
                    {
                      color: COLORS.altgreen_300,
                      marginLeft: 5,
                      marginTop: Platform.OS === 'ios' ? -2 : 2,
                    },
                    typography.Caption,
                  ]}>
                  {this.state.data.likesCount}
                </Text>
              </View>

              <TouchableOpacity
                activeOpacity={0.5}
                style={{flexDirection: 'row'}}
                onPress={() => {
                  this.setState({shareModalOpen: true});
                }}>
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.grey_350,
                      marginRight: 5,
                      marginTop: Platform.OS === 'ios' ? -2 : 2,
                    },
                  ]}>
                  {this.state.data.sharesCount} Share
                </Text>
                <Icon name="Share" color={COLORS.grey_350} size={14} />
              </TouchableOpacity>
            </View>
          )}

          {/****** Like & Share ends ******/}

          <TouchableOpacity
            activeOpacity={0.8}
            style={[
              defaultShape.ContextBtn_FL_Drk,
              {
                borderRadius: 6,
                alignSelf: 'center',
                marginTop: 10,
                marginBottom: 16,
              },
            ]}
            onPress={() => this.props.navigation.goBack()}>
            <Text
              style={[
                typography.Caption,
                {color: COLORS.altgreen_200, paddingVertical: 2},
              ]}>
              View Source
            </Text>
          </TouchableOpacity>

          <Comment
            id={this.props.route.params.id}
            commentData={this.state.commentData}
            commentCount={this.state.commentCount}
            navigateToProfile={this.navigateToProfile}
            navigate={this.navigate}
            key={this.state.commentCount}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: 44,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: COLORS.white,
    alignItems: 'center',
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  commentCountBar: {
    backgroundColor: COLORS.altgreen_200,
    paddingVertical: 6,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 6,
  },
  commentItemView: {
    paddingLeft: 15,
    backgroundColor: COLORS.white,
    paddingTop: 12,
    borderBottomColor: COLORS.altgreen_400,
    borderBottomWidth: 0.2,
  },
  commentBody: {
    color: COLORS.altgreen_400,
    marginTop: -10,
    marginLeft: 38,
    maxWidth: '80%',
  },
  commentBoxView: {
    position: 'absolute',
    bottom: 0,
    paddingVertical: 6,
    backgroundColor: COLORS.altgreen_200,
    width: '100%',
    zIndex: 1,
  },
  inputBox: {
    marginLeft: 10,
    width: '80%',
    minHeight: 30,
    maxHeight: 130,
    backgroundColor: COLORS.white,
    borderRadius: 6,
    paddingHorizontal: 10,
  },
  renderSharedPost: {
    width: '94%',
    marginTop: 5,
    marginLeft: 6,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: COLORS.grey_300,
    padding: 10,
  },
  unreadNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 8,
    height: 60,
  },
});

const mapStateToProps = (state) => {
  return {
    userFeedsPhotosProgress: state.feedsReducer.userFeedsPhotosProgress,
    userFeedsPhotos: state.feedsReducer.userFeedsPhotos,
    errorFeedsPhotos: state.feedsReducer.errorFeedsPhotos,

    userFeedsVideosProgress: state.feedsReducer.userFeedsVideosProgress,
    userFeedsVideos: state.feedsReducer.userFeedsVideos,
    errorFeedsVideos: state.feedsReducer.errorFeedsVideos,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    feedsPhotosRequest: (data) => dispatch(feedsPhotosRequest(data)),
    feedsVideosRequest: (data) => dispatch(feedsVideosRequest(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(IndividualFeedsPost);
