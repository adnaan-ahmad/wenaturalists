import React, {Component} from 'react';
import {
  Share,
  Clipboard,
  Modal,
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import axios from 'axios';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import LinearGradient from 'react-native-linear-gradient';
import Snackbar from 'react-native-snackbar';

import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import {COLORS} from '../../../Components/Shared/Colors';
import CommonFeeds from '../../../Components/User/Common/CommonFeeds';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class SeeallTrendingPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.route.params.userId,
      headerName:
        this.props.route.params.type === 'ARTICLE'
          ? 'BLOGS'
          : this.props.route.params.type,
      pageNumber: 0,
      postData: [],
    };
  }

  navigate = (value, params) => {
    return this.props.navigation.navigate(value, params)
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            paddingTop: 5,
            backgroundColor: COLORS.white,
          }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
          </TouchableOpacity>

          <View
            style={{
              marginLeft: 15,
              height: 38,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={[
                typography.Title_1,
                {color: COLORS.dark_800, maxWidth: 200},
              ]}
              numberOfLines={1}>
              {this.state.headerName === 'VIDEO' ? 'VIDEOS':this.state.headerName}
            </Text>
          </View>
        </View>

        <CommonFeeds
          navigate={this.navigate}
          type="TRENDING"
          newsFeedType={this.props.route.params.type === 'PHOTOS' ? "IMAGE": this.props.route.params.type}
        />
      </SafeAreaView>
    );
  }
}
