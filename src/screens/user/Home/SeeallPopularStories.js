import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  Dimensions,
  Animated,
  PanResponder,
  SafeAreaView,
  Modal,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  ImageBackground,
  Clipboard,
} from 'react-native';
import Swiper from 'react-native-swiper';
import {WebView} from 'react-native-webview';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import LinearGradient from 'react-native-linear-gradient';
import Snackbar from 'react-native-snackbar';
import axios from 'axios';

import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import {COLORS} from '../../../Components/Shared/Colors';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import httpService from '../../../services/AxiosInterceptors';
import defaultStyle from '../../../Components/Shared/Typography';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
httpService.setupInterceptors();

//Dimensions of device screen
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

class SeeallPopularStories extends Component {
  constructor(props) {
    super(props);
    //position of the current article
    this.position = new Animated.ValueXY();
    //position of the swiped up article
    this.swipedCardPosition = new Animated.ValueXY({x: 0, y: -SCREEN_HEIGHT});
    this.state = {
      currentIndex: 0,
      pageNumber: 0,
      limit: 10,
      popularStories: [],
      shareModalOpen: false,
      customUrl: '',
      pressedActivityId: '',
    };
  }

  UNSAFE_componentWillMount() {
    this.PanResponder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gestureState) => true,
      onPanResponderMove: (evt, gestureState) => {
        if (gestureState.dy > 0 && this.state.currentIndex > 0) {
          this.swipedCardPosition.setValue({
            x: 0,
            y: -SCREEN_HEIGHT + gestureState.dy,
          });
        } else {
          this.position.setValue({
            x: 0,
            y: gestureState.dy,
          });
        }
      },
      onPanResponderRelease: (evt, gestureState) => {
        if (
          this.state.currentIndex > 0 &&
          gestureState.dy > 50 &&
          gestureState.vy > 0.7
        ) {
          Animated.timing(this.swipedCardPosition, {
            toValue: {x: 0, y: 0},
            duration: 400,
            useNativeDriver: false,
          }).start(() => {
            this.setState({currentIndex: this.state.currentIndex - 1});
            this.swipedCardPosition.setValue({x: 0, y: -SCREEN_HEIGHT});
          });
        } else if (
          -gestureState.dy > 20 &&
          -gestureState.vy > 0.7 &&
          this.state.currentIndex < this.state.popularStories.length - 1
        ) {
          Animated.timing(this.position, {
            toValue: {x: 0, y: -SCREEN_HEIGHT},
            duration: 400,
            useNativeDriver: false,
          }).start(() => {
            this.setState({currentIndex: this.state.currentIndex + 1});
            this.position.setValue({x: 0, y: 0});
          });
        } else {
          Animated.parallel([
            Animated.spring(this.position, {
              toValue: {x: 0, y: 0},
              useNativeDriver: false,
            }),
            Animated.spring(this.swipedCardPosition, {
              toValue: {x: 0, y: -SCREEN_HEIGHT},
              useNativeDriver: false,
            }),
          ]).start();
        }
      },
    });
  }

  componentDidMount() {
    this.getPopularStory(0, 100);
  }

  handleLoadMore = () => {
    this.setState({pageNumber: this.state.pageNumber + 1}, () =>
      this.getPopularStory(this.state.pageNumber, this.state.limit),
    );
  };

  getPopularStory = (pageNumber, limit) => {
    let input = {
      page_no: 0,
      limit: 50,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/recommend/trendstories/',
      headers: {'Content-Type': 'application/json'},
      data: input,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data !== undefined &&
          response.data.status === 200 &&
          response.data.data !== null &&
          response.data.data !== undefined
        ) {
          // console.log("popular stories from daily edition: ", response.data.data)
          // this.state.popularStories.push(response.data.data)
          this.setState({
            popularStories: this.state.popularStories.concat(
              response.data.data,
            ),
          });
        }
      })
      .catch((err) => console.log('error in popular stories: ', err));
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation.navigate('RepostOnWenat', {
                    link: false,
                    pressedActivityId: this.state.currentPressedId,
                    entityType: 'STORY',
                    type: 'EXPLORE',
                  }),
                );
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(this.state.customUrl);
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  hashTagTemplate = (item) => {
    return (
      <View
        style={{
          height: 30,
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 10,
        }}>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={item.hash_tags.slice(0, 3)}
          keyExtractor={(item) => item}
          renderItem={(item) => (
            <Text
              style={[
                typography.Subtitle_2,
                {color: COLORS.green_600, marginRight: 5},
              ]}>
              #{item.item}
            </Text>
          )}
        />
        <TouchableOpacity
          onPress={() =>
            this.setState({
              shareModalOpen: true,
              customUrl: item.news_url,
              currentPressedId: item._id,
            })
          }
          style={{marginLeft: 20, marginRight: 10, marginTop: -5}}>
          <Icon name="Share" size={16} color={COLORS.altgreen_300} />
        </TouchableOpacity>
      </View>
    );
  };

  //This function display the articles
  renderArticles() {
    return this.state.popularStories
      .map((item, i) => {
        if (i == this.state.currentIndex - 1) {
          return (
            <Animated.View
              key={item.id}
              style={this.swipedCardPosition.getLayout()}
              {...this.PanResponder.panHandlers}>
              <View
                style={{
                  flex: 1,
                  position: 'absolute',
                  height: SCREEN_HEIGHT,
                  width: SCREEN_WIDTH,
                  backgroundColor: '#fff',
                }}>
                <View style={{flex: 2}}>
                  <ImageBackground
                    source={{uri: item.image_url}}
                    style={{
                      flex: 1,
                      height: null,
                      width: null,
                    }}>
                    <LinearGradient
                      colors={[
                        COLORS.white + '00',
                        COLORS.white + 'CC',
                        COLORS.white,
                      ]}
                      style={{
                        width: '100%',
                        height: 160,
                        position: 'absolute',
                        bottom: 0,
                        // alignItems: 'center',
                        justifyContent: 'flex-end',
                        paddingHorizontal: 15,
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('Webview', {
                            news: item.txt_data,
                            news_url: item.news_url,
                            name: item.name,
                          })
                        }>
                        <Text
                          numberOfLines={2}
                          style={[
                            typography.Title_1,
                            {color: COLORS.dark_800},
                          ]}>
                          {item.header}
                        </Text>
                      </TouchableOpacity>
                    </LinearGradient>
                  </ImageBackground>
                </View>
                <View style={{flex: 3, padding: 5, marginLeft: 10}}>
                  <Text style={[typography.Note, {color: COLORS.altgreen_400}]}>
                    {item.name}
                  </Text>
                  <Text
                    style={[
                      typography.Caption,
                      {color: COLORS.altgreen_300, marginTop: 5},
                    ]}>
                    {item.txt_data}
                  </Text>
                  {this.hashTagTemplate(item)}
                </View>
              </View>
            </Animated.View>
          );
        } else if (i < this.state.currentIndex) {
          return null;
        }
        if (i == this.state.currentIndex) {
          return (
            <Animated.View
              key={item.id}
              style={this.position.getLayout()}
              {...this.PanResponder.panHandlers}>
              <View
                style={{
                  flex: 1,
                  position: 'absolute',
                  height: SCREEN_HEIGHT,
                  width: SCREEN_WIDTH,
                  backgroundColor: '#fff',
                }}>
                <View style={{flex: 2}}>
                  <ImageBackground
                    source={{uri: item.image_url}}
                    style={{
                      flex: 1,
                      height: null,
                      width: null,
                    }}>
                    <LinearGradient
                      colors={[
                        COLORS.white + '00',
                        COLORS.white + 'CC',
                        COLORS.white,
                      ]}
                      style={{
                        width: '100%',
                        height: 160,
                        position: 'absolute',
                        bottom: 0,
                        // alignItems: 'center',
                        justifyContent: 'flex-end',
                        paddingHorizontal: 15,
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('Webview', {
                            news: item.txt_data,
                            news_url: item.news_url,
                            name: item.name,
                          })
                        }>
                        <Text
                          numberOfLines={2}
                          style={[
                            typography.Title_1,
                            {color: COLORS.dark_800},
                          ]}>
                          {item.header}
                        </Text>
                      </TouchableOpacity>
                    </LinearGradient>
                  </ImageBackground>
                </View>
                <View style={{flex: 3, padding: 5, marginLeft: 10}}>
                  <Text style={[typography.Note, {color: COLORS.altgreen_400}]}>
                    {item.name}
                  </Text>
                  <Text
                    style={[
                      typography.Caption,
                      {color: COLORS.altgreen_300, marginTop: 5},
                    ]}>
                    {item.txt_data}
                  </Text>
                  {this.hashTagTemplate(item)}
                </View>
              </View>
            </Animated.View>
          );
        } else {
          return (
            <Animated.View key={item.id}>
              <View
                style={{
                  flex: 1,
                  position: 'absolute',
                  height: SCREEN_HEIGHT,
                  width: SCREEN_WIDTH,
                  backgroundColor: '#fff',
                }}>
                <View style={{flex: 2}}>
                  <ImageBackground
                    source={{uri: item.image_url}}
                    style={{
                      flex: 1,
                      height: null,
                      width: null,
                    }}>
                    <LinearGradient
                      colors={[
                        COLORS.white + '00',
                        COLORS.white + 'CC',
                        COLORS.white,
                      ]}
                      style={{
                        width: '100%',
                        height: 160,
                        position: 'absolute',
                        bottom: 0,
                        // alignItems: 'center',
                        justifyContent: 'flex-end',
                        paddingHorizontal: 15,
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('Webview', {
                            news: item.txt_data,
                            news_url: item.news_url,
                            name: item.name,
                          })
                        }>
                        <Text
                          numberOfLines={2}
                          style={[
                            typography.Title_1,
                            {color: COLORS.dark_800},
                          ]}>
                          {item.header}
                        </Text>
                      </TouchableOpacity>
                    </LinearGradient>
                  </ImageBackground>
                </View>
                <View style={{flex: 3, padding: 5, marginLeft: 10}}>
                  <Text style={[typography.Note, {color: COLORS.altgreen_400}]}>
                    {item.name}
                  </Text>
                  <Text
                    style={[
                      typography.Caption,
                      {color: COLORS.altgreen_300, marginTop: 5},
                    ]}>
                    {item.txt_data}
                  </Text>
                  {this.hashTagTemplate(item)}
                </View>
              </View>
            </Animated.View>
          );
        }
      })
      .reverse();
  }

  onShare = async () => {
    try {
      const result = await Share.share({
        message: this.state.customUrl,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  render() {
    return (
      <>
        {this.shareModal()}
        <View
          style={{
            flexDirection: 'row',
            paddingTop: 5,
            backgroundColor: COLORS.white,
          }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
          </TouchableOpacity>
          <View
            style={{
              marginLeft: 15,
              height: 38,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={[
                typography.Title_1,
                {color: COLORS.dark_800, maxWidth: 200},
              ]}
              numberOfLines={1}>
              {' '}
              Popular Stories{' '}
            </Text>
          </View>
        </View>
        <Swiper loop={false} showsPagination={false}>
          <Swiper horizontal={false} loop={false} showsPagination={true}>
            <SafeAreaView style={{flex: 1}}>
              {this.renderArticles()}
            </SafeAreaView>
          </Swiper>
        </Swiper>
      </>
    );
  }
}
export default SeeallPopularStories;
const styles = StyleSheet.create({
  popularStoriesView: {
    // width: 200,
    marginHorizontal: 15,
    borderBottomColor: COLORS.grey_300,
    borderBottomWidth: 2,
    // height: 175,
    marginTop: 15,
    paddingBottom: 8,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
});
