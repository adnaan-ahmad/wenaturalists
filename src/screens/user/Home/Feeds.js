import React, {Component} from 'react';
import {
  TouchableHighlight,
  Linking,
  ActivityIndicator,
  Dimensions,
  Modal,
  ImageBackground,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Share,
  Clipboard,
  FlatList,
  Image,
  SafeAreaView,
  Platform,
} from 'react-native';
import {cloneDeep} from 'lodash';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {connect} from 'react-redux';
import VideoPlayer from 'react-native-video-controls';
import LinearGradient from 'react-native-linear-gradient';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Snackbar from 'react-native-snackbar';
import RNUrlPreview from 'react-native-url-preview';
import {TextInput} from 'react-native-paper';
import Autolink from 'react-native-autolink';

import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import {
  feedsPhotosRequest,
  feedsVideosRequest,
} from '../../../services/Redux/Actions/User/FeedsActions';
import {personalProfileRequest} from '../../../services/Redux/Actions/User/PersonalProfileActions';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import SearchBar from '../../../Components/User/SearchBar';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import httpService from '../../../services/AxiosInterceptors';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import typography from '../../../Components/Shared/Typography';
import postIcon from '../../../../assets/Post_Icon.png';
import blogIcon from '../../../../assets/Blog_Icon.png';
import linkIcon from '../../../../assets/Link_Icon.png';
import AudioThumb from '../../../../assets/AudioThumb.png';
import VideoThumb from '../../../../assets/VideoThumb.png';
import defaultStyle from '../../../Components/Shared/Typography';
import CommonFeeds from '../../../Components/User/Common/CommonFeeds';

httpService.setupInterceptors();
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const skeletonCount = [1, 2, 3, 4];
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
  'i',
); // fragment locator

class Feeds extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 'LATEST',
      userId: '',
      notification: false,
      redirectToProfile: false,
      pageNumber: 0,
      pageSize: 30,
      postModalOpen: false,
      optionsModalOpen: false,
      shareModalOpen: false,
      likeModalOpen: false,
      readMore: false,
      currentScrollPosition: 0,
      hideControls: false,
      pressedActivityId: '',
      peopleLiked: [],
      videoHeight: 190,
      videoWidth: '97%',
      entityType: '',
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      userType: '',
      selectedVideoId: '',
      isReported: false,
      feedsData: [],
      refresh: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      // console.log('refreshToken from Feeds.js : ', value)
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        // this.fetchFeedsData(value);
        // this.props.feedsPhotosRequest({
        //   userId: value,
        //   type: this.state.selected,
        //   pageNumber: this.state.pageNumber,
        //   size: this.state.pageSize,
        // });
        this.props.personalProfileRequest({userId: value, otherUserId: ''});
        this.setState({userId: value}, () => {
          this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.setState({refresh: true});
          });
        });

        axios({
          method: 'get',
          url:
            REACT_APP_userServiceURL +
            '/profile/get?id=' +
            value +
            '&otherUserId=' +
            '',
          cache: true,
          withCredentials: true,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => {
            this.setState({userType: response.data.body.type});
            // console.log('--- response.data.body ---', response.data.body)
          })
          .catch((err) => {
            // console.log("Profile data error : ", err)
          });
      })
      .catch((e) => {
        // console.log(e)
      });

    // this.props.userFeedsPhotos.body.content ?
    // this.setState({ likedPosts: this.props.userFeedsPhotos.body.content.filter((item) => item.liked) })
    // : null

    try {
      messaging().onNotificationOpenedApp((remoteMessage) => {
        let paramsObj = JSON.parse(remoteMessage.data.params);
        console.log(remoteMessage);
        if (remoteMessage) {
          if (remoteMessage.data.category === 'POST') {
            this.props.navigation.navigate('IndividualFeedsPost', {
              id: remoteMessage.data.url.split('/').pop(),
            });
          } else if (remoteMessage.data.category === 'FORUM') {
            this.props.navigation.navigate('ForumStack', {
              screen: 'ForumDetails',
              params: {
                slug: remoteMessage.data.url.split('/').pop(),
                userId: remoteMessage.data.userId,
              },
            });
          } else if (remoteMessage.data.category === 'POLL') {
            this.props.navigation.navigate('PollStack', {
              screen: 'PollDetails',
              params: {
                slug: remoteMessage.data.url.split('/').pop(),
                userId: remoteMessage.data.userId,
              },
            });
          } else if (remoteMessage.data.category === 'PROJECT') {
            remoteMessage.data.userEvent === 'MAXTHRESHOLDREPORTEDABUSE'
              ? null
              : paramsObj.entityType === 'POST'
              ? this.props.navigation.navigate('IndividualFeedsPost', {
                  id: paramsObj?.postId || paramsObj.entityId,
                })
              : this.props.navigation.navigate('ProjectDetailView', {
                  slug: paramsObj.slug.split('/').pop(),
                });
          } else if (remoteMessage.data.category === 'CIRCLE') {
            this.props.navigation.navigate('CircleProfileStack', {
              screen: 'CircleProfile',
              params: {slug: paramsObj.slug.split('/').pop()},
            });
          } else if (remoteMessage.data.category === 'NETWORK') {
            remoteMessage.data.userEvent === 'CONNECT_INVITE_ACCEPTED'
              ? this.props.navigation.navigate('ProfileStack', {
                  screen: 'OtherProfileScreen',
                  params: {userId: paramsObj.otherUserId},
                })
              : this.props.navigation.navigate('NetworkInvitationStack', {});
          } else {
            console.log('onNotificationOpenedApp', remoteMessage);
            this.props.navigation.navigate('Notification');
          }
        }
      });
    } catch (error) {
      console.log('onNotificationOpenedApp', error);
    }

    try {
      messaging()
        .getInitialNotification()
        .then((remoteMessage) => {
          let paramsObj = JSON.parse(remoteMessage.data.params);
          console.log(remoteMessage);
          if (remoteMessage) {
            if (remoteMessage.data.category === 'POST') {
              this.props.navigation.navigate('IndividualFeedsPost', {
                id: remoteMessage.data.url.split('/').pop(),
              });
            } else if (remoteMessage.data.category === 'FORUM') {
              this.props.navigation.navigate('ForumStack', {
                screen: 'ForumDetails',
                params: {
                  slug: remoteMessage.data.url.split('/').pop(),
                  userId: remoteMessage.data.userId,
                },
              });
            } else if (remoteMessage.data.category === 'POLL') {
              this.props.navigation.navigate('PollStack', {
                screen: 'PollDetails',
                params: {
                  slug: remoteMessage.data.url.split('/').pop(),
                  userId: remoteMessage.data.userId,
                },
              });
            } else if (remoteMessage.data.category === 'PROJECT') {
              remoteMessage.data.userEvent === 'MAXTHRESHOLDREPORTEDABUSE'
                ? null
                : paramsObj.entityType === 'POST'
                ? this.props.navigation.navigate('IndividualFeedsPost', {
                    id: paramsObj?.postId || paramsObj.entityId,
                  })
                : this.props.navigation.navigate('ProjectDetailView', {
                    slug: paramsObj.slug.split('/').pop(),
                  });
            } else if (remoteMessage.data.category === 'CIRCLE') {
              this.props.navigation.navigate('CircleProfileStack', {
                screen: 'CircleProfile',
                params: {slug: paramsObj.slug.split('/').pop()},
              });
            } else if (remoteMessage.data.category === 'NETWORK') {
              remoteMessage.data.userEvent === 'CONNECT_INVITE_ACCEPTED'
                ? this.props.navigation.navigate('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: {userId: paramsObj.otherUserId},
                  })
                : this.props.navigation.navigate('NetworkInvitationStack', {});
            } else {
              console.log('getInitialNotification', remoteMessage);
              this.props.navigation.navigate('Notification');
            }
          }
        });
    } catch (error) {
      console.log('getInitialNotification', error);
    }
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.user !== this.props.user) {
      return true;
    }
    if (nextProps.userFeedsPhotos !== this.props.userFeedsPhotos) {
      return true;
    }
    if (nextProps.userFeedsVideos !== this.props.userFeedsVideos) {
      return true;
    }
    if (
      nextProps.userFeedsPhotosProgress !== this.props.userFeedsPhotosProgress
    ) {
      return true;
    }
    if (
      nextProps.userFeedsVideosProgress !== this.props.userFeedsVideosProgress
    ) {
      return true;
    }

    if (nextState.feedsData !== this.state.feedsData) {
      return true;
    }
    if (nextState.selectedVideoId !== this.state.selectedVideoId) {
      return true;
    }
    if (nextState.userType !== this.state.userType) {
      return true;
    }
    if (nextState.description !== this.state.description) {
      return true;
    }
    if (nextState.reasonForReporting !== this.state.reasonForReporting) {
      return true;
    }
    if (
      nextState.reasonForReportingModalOpen !==
      this.state.reasonForReportingModalOpen
    ) {
      return true;
    }
    if (nextState.entityType !== this.state.entityType) {
      return true;
    }
    if (nextState.pageNumber !== this.state.pageNumber) {
      return true;
    }
    if (nextState.pageSize !== this.state.pageSize) {
      return true;
    }
    if (nextState.userId !== this.state.userId) {
      return true;
    }
    if (nextState.notification !== this.state.notification) {
      return true;
    }
    if (nextState.redirectToProfile !== this.state.redirectToProfile) {
      return true;
    }
    if (nextState.selected !== this.state.selected) {
      return true;
    }
    if (nextState.postModalOpen !== this.state.postModalOpen) {
      return true;
    }
    if (nextState.readMore !== this.state.readMore) {
      return true;
    }
    if (nextState.currentScrollPosition !== this.state.currentScrollPosition) {
      return true;
    }
    if (nextState.hideControls !== this.state.hideControls) {
      return true;
    }
    if (nextState.pressedActivityId !== this.state.pressedActivityId) {
      return true;
    }
    if (nextState.optionsModalOpen !== this.state.optionsModalOpen) {
      return true;
    }
    if (nextState.shareModalOpen !== this.state.shareModalOpen) {
      return true;
    }
    if (nextState.likeModalOpen !== this.state.likeModalOpen) {
      return true;
    }
    if (nextState.peopleLiked !== this.state.peopleLiked) {
      return true;
    }
    if (nextState.videoHeight !== this.state.videoHeight) {
      return true;
    }
    if (nextState.videoWidth !== this.state.videoWidth) {
      return true;
    }
    return false;
  }

  navigateNotification = (value) => {
    this.props.navigation.navigate(value);
  };

  changeState = (value) => {
    this.setState(value);
  };

  postModal = () => {
    return (
      <Modal
        visible={this.state.postModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({postModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              },
            ]}>
            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewFeedsPost', {link: false}),
                );
              }}>
              {/* <Icon name="Add_Post" size={44} color={COLORS.primarygreen} style={{ alignSelf: 'center' }} /> */}
              <Image style={{alignSelf: 'center'}} source={postIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    alignSelf: 'center',
                    position: 'absolute',
                    top: 50,
                  },
                ]}>
                Post
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewBlogPost'),
                );
              }}>
              {/* <Icon name='Blog' size={44} color={COLORS.primarygreen} /> */}
              <Image style={{alignSelf: 'center'}} source={blogIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    position: 'absolute',
                    top: 47,
                  },
                ]}>
                Blog
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewFeedsPost', {link: true}),
                );
              }}>
              {/* <Icon name='Link_Post' size={44} color={COLORS.primarygreen} /> */}
              <Image style={{alignSelf: 'center'}} source={linkIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    position: 'absolute',
                    top: 50,
                  },
                ]}>
                Link
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleScroll = (event) => {
    // console.log(event.nativeEvent.contentOffset.y)
    this.setState({currentScrollPosition: event.nativeEvent.contentOffset.y});
  };

  goToTop = () => {
    this.flatListRef.scrollToOffset({animated: true, offset: 0});
  };

  navigateProfile = () => {
    if (this.state.userType === 'INDIVIDUAL') {
      this.props.navigation.navigate('ProfileStack');
    } else if (this.state.userType === 'COMPANY') {
      this.props.navigation.navigate('ProfileStack', {
        screen: 'CompanyProfileScreen',
      });
    }
  };

  navigate = (value, params) => {
    return this.props.navigation.navigate(value, params);
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.postModal()}

        <View style={styles.searchBar}>
          <SearchBar
            changeState={this.changeState}
            navigateProfile={this.navigateProfile}
            navigateNotification={this.navigateNotification}
            navigation={this.navigate}
          />
        </View>

        <View style={styles.header}>
          <TouchableOpacity style={styles.feed}>
            <Icon
              name="WN_Feeds_OL"
              size={20}
              color="#fff"
              style={styles.feedIcon}
            />
            <Text style={styles.feedText}>Feeds</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('Explore');
            }}
            style={styles.explore}>
            <Icon
              name="WN_Explore_OL"
              size={20}
              color="#91B3A2"
              style={styles.exploreIcon}
            />
            <Text style={styles.exploreText}>Explore</Text>
          </TouchableOpacity>
        </View>

        <CommonFeeds navigate={this.navigate} type="OWNFEEDS" />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#00394D',
    flex: 1,
  },
  renderSharedPost: {
    width: '94%',
    marginTop: 5,
    marginLeft: 6,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: COLORS.grey_300,
    padding: 10,
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  renderItemStyle: {
    backgroundColor: COLORS.white,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginHorizontal: 10,
    marginVertical: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: COLORS.grey_300,
  },
  unreadNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderRadius: 8,
    // borderBottomColor: COLORS.grey_400,
    // borderBottomWidth: 0.3,
    // height: 44
  },
  scrollViewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editIcon: {
    marginTop: Platform.OS === 'android' ? 12 : 0,
    marginRight: 8,
  },
  editIcon2: {
    marginTop: Platform.OS === 'android' ? 8 : 0,
    // marginRight: 8
  },
  floatingIconText: {
    color: '#00394D',
    fontSize: 18,
    fontWeight: 'bold',
    // marginTop: 2,
  },
  floatingIcon: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 113,
    height: 44,
    borderRadius: 40,
    backgroundColor: '#D8DE21',
    position: 'absolute',
    bottom: 16,
    right: 12,
  },
  floatingIcon2: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 28,
    height: 42,
    borderRadius: 40,
    backgroundColor: '#1A4D5F80',
    position: 'absolute',
    bottom: 66,
    right: 12,
  },
  feedDetails: {
    marginTop: 20,
    marginLeft: 20,
    paddingBottom: 16,
  },
  selected: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#367681',
    marginRight: 15,
    height: 27,
    // width: 66,
    borderRadius: 16,
    textAlign: 'center',
  },
  notSelected: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    height: 27,
    // width: 66,
    borderWidth: 1,
    borderColor: '#698F8A',
    borderRadius: 16,
  },
  selectedText: {
    color: '#F7F7F5',
    fontSize: 12,
    paddingHorizontal: 14,
    fontFamily: 'Montserrat-Medium',
  },
  notSelectedText: {
    color: '#698F8A',
    fontSize: 12,
    paddingHorizontal: 14,
    fontFamily: 'Montserrat-Medium',
  },
  searchBar: {
    // marginTop: 8
  },
  explore: {
    flexDirection: 'row',
    paddingTop: 8,
    marginLeft: 15,
  },
  feed: {
    flexDirection: 'row',
    paddingTop: 8,
    marginLeft: 20,
    borderBottomColor: '#BFC52E',
    borderBottomWidth: 4,
    width: 100,
  },
  header: {
    flexDirection: 'row',
    borderBottomColor: '#154A59',
    borderBottomWidth: 1,
    paddingTop: 10,
    height: 60,
    backgroundColor: '#00394D',
  },
  feedText: {
    color: '#FFFFFF',
    fontSize: 22,
    marginLeft: 5,
    fontFamily: 'Montserrat-SemiBold',
  },
  exploreText: {
    color: '#91B3A2',
    fontSize: 22,
    marginLeft: 5,
    fontFamily: 'Montserrat-Regular',
  },
});

const mapStateToProps = (state) => {
  return {
    userFeedsPhotosProgress: state.feedsReducer.userFeedsPhotosProgress,
    userFeedsPhotos: state.feedsReducer.userFeedsPhotos,
    errorFeedsPhotos: state.feedsReducer.errorFeedsPhotos,

    userFeedsVideosProgress: state.feedsReducer.userFeedsVideosProgress,
    userFeedsVideos: state.feedsReducer.userFeedsVideos,
    errorFeedsVideos: state.feedsReducer.errorFeedsVideos,

    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    feedsPhotosRequest: (data) => dispatch(feedsPhotosRequest(data)),
    feedsVideosRequest: (data) => dispatch(feedsVideosRequest(data)),
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Feeds);
