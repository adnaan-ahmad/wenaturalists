import React, {Component} from 'react';
import {
  SafeAreaView,
  Modal,
  Text,
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  FlatList,
  ScrollView,
  TextInput,
  Platform,
  PermissionsAndroid,
  Share,
  Clipboard,
  ImageBackground,
  Alert
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {connect} from 'react-redux';
import VideoPlayer from 'react-native-video-controls';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import * as Progress from 'react-native-progress';
import DocumentPicker from 'react-native-document-picker';
import Snackbar from 'react-native-snackbar';
import {v4 as uuidv4} from 'uuid';
import LinearGradient from 'react-native-linear-gradient';
import HTMLView from 'react-native-htmlview';

import {
  trimDescription,
  unixTime2,
} from '../../../Components/Shared/commonFunction';
import defaultStyle from '../../../Components/Shared/Typography';
import {
  feedsPhotosRequest,
  feedsVideosRequest,
} from '../../../services/Redux/Actions/User/FeedsActions';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import httpService from '../../../services/AxiosInterceptors';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultShape from '../../../Components/Shared/Shape';
import defaultProfile from '../../../../assets/defaultProfile.png';
import BlogDefault from '../../../../assets/BlogDefault.jpg';
import welogo from '../../../../assets/welogo.png';
import typography from '../../../Components/Shared/Typography';
import LikedUserList from '../../../Components/User/Common/LikedUserList';
import SharedUserList from '../../../Components/User/Common/SharedUserList';

httpService.setupInterceptors();

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

class EditorsDesk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      data: {},
      commentData: [],
      commentAttachments: [],
      commentBody: '',
      replyExpandId: '',
      readMore: false,
      setreadmore: false,
      sendButton: false,
      replyExpand: false,
      reply: false,
      fullscreen: false,
      downloadStart: false,
      isDownloading: false,
      progressLimit: 0,
      optionsModalOpen: false,
      shareModalOpen: false,
      likeModalOpen: false,
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      optionsModal2Open: false,
      pressedUserId: '',
      pressedActivityId: '',
      peopleSharedModalOpen: false,
      isReported: false,
      edit: false,
      slug:
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.slug,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId')
      .then((value) => {
        this.setState({userId: value});
        // this.individualPostResponse(value);
        this.individualPostResponse(value);
        this.getCommentsData(value);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  individualPostResponse = (value) => {
    let url = '';
    if (
      this.props.route &&
      this.props.route.params &&
      this.props.route.params.slug
    ) {
      url =
        REACT_APP_userServiceURL +
        '/backend/explore_service/api/explore_blog/getBySlug?slug=' +
        this.props.route.params.slug +
        '&userId=' +
        value;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/post/get?id=' +
        this.props.route.params.id +
        '&postRequestingUserId=' +
        value;
    }
    axios({
      method: 'get',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        console.log('individual post response : ', response.data.body);
        this.setState({data: response.data.body});
      })
      .catch((err) => {
        console.log('error individual post : ', err);
      });
  };

  getCommentsData = (value) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/comment/getCommentsByActivityId/' +
        this.props.route.params.id +
        '?userId=' +
        value +
        '&page=' +
        0 +
        '&size=' +
        1000,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log("comments list: ", response.data.body.content)
          // console.log("comments list attachment: ", response.data.body.content[0].attachmentIds)
          this.setState({commentData: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getUsersWhoLiked = (id) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/like/getUsers/' +
        id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          this.setState({peopleLiked: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View
            style={[
              defaultShape.Linear_Gradient_View,
              {height: 700, bottom: 0},
            ]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={[defaultShape.CloseBtn, {marginBottom: 0}]}
            onPress={() =>
              this.setState({
                reasonForReportingModalOpen: false,
                reasonForReporting: '',
              })
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View
            style={{
              borderRadius: 20,
              backgroundColor: COLORS.altgreen_100,
              alignItems: 'center',
              paddingTop: 15,
              paddingBottom: 10,
              // paddingLeft: 20,
              width: '90%',
              alignSelf: 'center',
              marginBottom: 30,
              marginTop: 15,
            }}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Reason for reporting
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '94%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting:
                    'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting ===
                  'INAPPROPRIATE_ABUSIVE_OR_OFFENSIVE_CONTENT' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Inappropriate, abusive or offensive content
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting: 'CONTENT_PROMOTING_VIOLENCE_OR_TERRORISM',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting ===
                  'CONTENT_PROMOTING_VIOLENCE_OR_TERRORISM' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Content promoting violence or terrorism
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingVertical: 15, borderBottomWidth: 0},
                    ]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'FAKE_SPAM_OR_SCAM'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'FAKE_SPAM_OR_SCAM' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Fake, spam or scam
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'ACCOUNT_MAY_BE_HACKED'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'ACCOUNT_MAY_BE_HACKED' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Account may be hacked
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '90.5%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForReporting:
                    'DEFAMATION_TRADEMARK_OR_COPYRIGHT_VIOLATION',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting ===
                  'DEFAMATION_TRADEMARK_OR_COPYRIGHT_VIOLATION' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  numberOfLines={2}
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Defamation, trademark or copyright violation
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'HARASSMENT_OR_THREAT'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'HARASSMENT_OR_THREAT' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Harassment or threat
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt, {width: '95%'}]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({reasonForReporting: 'OTHERS'});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForReporting === 'OTHERS' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Others
                </Text>
              </View>
            </TouchableOpacity>

            {this.state.reasonForReporting === 'OTHERS' && (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <TextInput
                  theme={{
                    colors: {
                      text: COLORS.dark_700,
                      primary: COLORS.altgreen_300,
                      placeholder: COLORS.altgreen_300,
                    },
                  }}
                  placeholder="Write the details"
                  placeholderTextColor={COLORS.altgreen_300}
                  multiline
                  autoFocus
                  selectionColor="#C8DB6E"
                  style={[
                    defaultStyle.Subtitle_1,
                    {
                      width: '90%',
                      height: 56,
                      backgroundColor: COLORS.altgreen_100,
                      color: COLORS.dark_700,
                    },
                  ]}
                  onChangeText={(value) => this.setState({description: value})}
                  value={this.state.description}
                />
              </View>
            )}

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                this.state.reasonForReporting === ''
                  ? Snackbar.show({
                      backgroundColor: '#B22222',
                      text: 'Please select an option',
                      duration: Snackbar.LENGTH_LONG,
                    })
                  : this.state.reasonForReporting === 'OTHERS' &&
                    this.state.description.trim() === ''
                  ? Snackbar.show({
                      backgroundColor: '#B22222',
                      text: 'Please enter the detail',
                      duration: Snackbar.LENGTH_LONG,
                    })
                  : (this.handleReportAbuseSubmit(),
                    this.setState({reasonForReportingModalOpen: false}));
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 15,
                height: 27,
                marginVertical: 10,
                borderRadius: 16,
                textAlign: 'center',
                borderWidth: 1,
                borderColor: '#698F8A',
              }}>
              <Text
                style={{
                  color: '#698F8A',
                  fontSize: 14,
                  paddingHorizontal: 14,
                  paddingVertical: 20,
                  fontFamily: 'Montserrat-Medium',
                  fontWeight: 'bold',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedActivityId,
      // entityType: this.state.entityType,
      entityType: 'POST',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    setTimeout(() => {
      this.setState({reasonForReporting: '', description: ''});
    }, 1000);
  };

  deletePostAlert = () => {
    this.setState({optionsModalOpen: false});
    Alert.alert('', 'Are you sure you want to delete this post?', [
      {
        text: 'YES',
        onPress: () => this.handleDeleteSubmit(),
        style: 'cancel',
      },
      {
        text: 'NO',
        // onPress: () => console.log('user cancelled deletion'),
      },
    ]);
  };

  handleDeleteSubmit = () => {
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/backend/post/delete/' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          // console.log(response.status)
        }
      })
      .catch((err) => {
        // console.log(err)
      });
    this.setState({
      optionsModalOpen: false,
    },()=>{
      this.props.navigation.goBack();
    });
  };

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({optionsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Post
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState(
                  {optionsModalOpen: false, likeModalOpen: true},
                  () => this.getUsersWhoLiked(this.props.route.params.id),
                );
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Like_FL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  See who liked it
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  optionsModalOpen: false,
                  peopleSharedModalOpen: true,
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  See who shared it
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({optionsModalOpen: false, shareModalOpen: true});
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Share
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            {this.state.data.source !== "FROM EDITOR'S DESK" && <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.handleHideModal();
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Hide
              </Text>
              <Icon
                name="Hide"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>}

            {this.state.data.canReport ||   this.state.data.source !== "FROM EDITOR'S DESK"? (
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    optionsModalOpen: false,
                    reasonForReportingModalOpen: !this.state.isReported
                      ? true
                      : false,
                  });
                  this.state.isReported
                    ? setTimeout(() => {
                        Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Your report request was already taken',
                          duration: Snackbar.LENGTH_LONG,
                        });
                      }, 1000)
                    : null;
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Report
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            ) : (
              this.state.data.source !== "FROM EDITOR'S DESK" ? <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [defaultShape.ActList_Cell_Gylph_Alt]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.deletePostAlert();
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Delete Post
                </Text>
                <Icon
                  name="Hide"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity> :<></>
            )}
          </View>
        </View>
      </Modal>
    );
  };

  peopleSharedModal = () => {
    return (
      <Modal
        visible={this.state.peopleSharedModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({peopleSharedModalOpen: false, peopleShared: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who Shared this post
              </Text>
            </View>

            <SharedUserList
              id={this.state.pressedActivityId}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  navigation = (value, params) => {
    this.setState({likeModalOpen: false, peopleSharedModalOpen: false}, () =>
      this.props.navigation.navigate(value, params),
    );
  };

  likeModal = () => {
    console.log('object data show', this.state.pressedActivityId);
    return (
      <Modal
        visible={this.state.likeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({likeModalOpen: false, peopleLiked: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                Who liked this post
              </Text>
            </View>

            <LikedUserList
              id={this.state.pressedActivityId}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  documentPicker = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      for (const res of results) {
        // console.log(res);
        this.setState({
          commentAttachments: [...this.state.commentAttachments, res],
        });
      }
      this.setState({...this.state});
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  downloadProgressWrapper = () => {
    return (
      <View>
        <Modal
          isVisible={this.state.isDownloading}
          backdropColor={'#ffffff'}
          animationIn={'slideInUp'}
          animationInTiming={300}
          animationOut={'slideOutDown'}
          animationOutTiming={300}
          avoidKeyboard={true}>
          <View>
            {this.state.progressLimit == 1 ? null : (
              <Progress.Pie
                style={styles.downloadInProgress}
                progress={this.state.progressLimit}
                size={50}
                color={'green'}
              />
            )}
            {this.state.progressLimit == 1 ? (
              <Text style={styles.downloadInComplete}>
                File Download Successfully
              </Text>
            ) : null}
          </View>
        </Modal>
      </View>
    );
  };

  readStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.writeStorage(data);
      } else {
        // Permission Denied
        alert('Read Storage Permission Denied');
      }
    } else {
      this.getDownloadFile(data); //work on ios
    }
  };

  writeStorage = async (data) => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        this.getDownloadFile(data);
      } else {
        // Permission Denied
        alert('Write Storage Permission Denied');
      }
    }
  };

  getDownloadFile = (urlDownload) => {
    if (this.state.downloadStart == true) {
      return;
    } else {
      this.setState({downloadStart: true});
    }

    let directoryFile;
    let dirs;

    let fileName;
    try {
      fileName = urlDownload.split('-').pop();
    } catch (e) {
      fileName = uuidv4();
    }
    if (Platform.OS === 'android') {
      directoryFile = RNFetchBlob.fs.dirs;
      dirs = directoryFile.DownloadDir + '/Download' + fileName;
    } else {
      directoryFile = RNFS.DocumentDirectoryPath;
      dirs = directoryFile + fileName;
    }

    try {
      RNFetchBlob.config({
        path: dirs,
      })
        .fetch('GET', urlDownload, {})
        .progress((received, total) => {
          let temp = parseFloat(received / total);

          if (temp > 0.9) {
            temp = 1;
            setTimeout(() => {
              this.setState({isDownloading: false});
            }, 1000);
          }
          this.setState(
            {
              isDownloading: true,
              progressLimit: temp,
            },
            () => this.downloadProgressWrapper(),
          );
        })
        .then((res) => {
          Snackbar.show({
            backgroundColor: '#97A600',
            text: 'File downloaded successfully',
            textColor: '#00394D',
            duration: Snackbar.LENGTH_LONG,
          });

          this.setState({
            ...this.state,
            downloadStart: false,
          });
          setTimeout(() => {
            this.setState({isDownloading: false});
          }, 1000);
        });
    } catch (error) {}
  };

  getAllComment = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/comment/getCommentsByActivityId/' +
        this.props.route.params.id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log("comments list: ", response.data.body.content)
          this.setState({commentData: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  deleteComment = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/comment/delete?commentId=' +
        this.state.pressedActivityId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          console.log(res.status);
          this.getCommentsData(this.state.userId);
          // this.state.commentData.filter(item => this.setState({ commentData: item.id !== this.state.pressedActivityId }))
        }
      })
      .catch((err) => {
        console.log(err);
      });
    this.setState({optionsModal2Open: false, commentBody: ''});
  };

  handleCommentEdit = () => {
    let formData = {
      commentId: this.state.pressedActivityId,
      description: this.state.commentBody.trim(),
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/comment/edit',
      data: formData,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          // console.log(res.status)
          this.getCommentsData(this.state.userId);
        }
      })
      .catch((err) => {
        console.log(err);
      });
    this.setState({edit: false, commentBody: '', reply: false});
  };

  optionsModal2 = () => {
    return (
      <Modal
        visible={this.state.optionsModal2Open}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({optionsModal2Open: false, commentBody: ''})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          {this.state.userId !== this.state.pressedUserId ? (
            <View style={defaultShape.Modal_Categories_Container}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    reasonForReportingModalOpen: !this.state.isReported
                      ? true
                      : false,
                    optionsModal2Open: false,
                    commentBody: '',
                  });
                  this.state.isReported
                    ? setTimeout(() => {
                        Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Your report request was already taken',
                          duration: Snackbar.LENGTH_LONG,
                        });
                      }, 500)
                    : null;
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Report
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={defaultShape.Modal_Categories_Container}>
              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({optionsModal2Open: false, edit: true});
                  this.textInputField.focus();
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Edit
                </Text>
                <Icon
                  name="EditBox"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.deleteComment()}
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  Delete
                </Text>
                <Icon
                  name="TrashBin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  };

  renderReplyItem = (item) => {
    let str = item.item.userComment.replace(/&nbsp;/g, ' ');
    return (
      <View style={{backgroundColor: COLORS.grey_300}}>
        <View
          style={[
            styles.commentItemView,
            {
              backgroundColor: COLORS.bgfill,
              width: '95%',
              alignSelf: 'flex-end',
            },
          ]}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                item.item.userType === 'INDIVIDUAL'
                  ? this.state.userId === item.item.userId
                    ? this.props.navigation.navigate('ProfileStack', {
                        screen: 'ProfileScreen',
                        // params: { userId: item.item.userId },
                      })
                    : this.props.navigation.navigate('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.item.userId},
                      })
                  : this.props.navigation.navigate('ProfileStack', {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.item.userId},
                    })
              }
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                source={
                  item.item.profileImage
                    ? {uri: item.item.profileImage}
                    : item.item.userType === 'INDIVIDUAL'
                    ? defaultProfile
                    : {uri: 'https://cdn.dscovr.com/images/DefaultBusiness.png'}
                }
                style={defaultShape.Media_Round}
              />
              <Text
                numberOfLines={1}
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_800, marginLeft: 6, maxWidth: 160},
                ]}>
                {item.item.userName}
              </Text>
              <Text
                style={[
                  {
                    color:
                      ((item.item && item.item.connectDepth === -1) ||
                        (item.item && item.item.connectDepth === 0)) &&
                      !item.item.followed
                        ? '#97a600'
                        : '#888',
                    fontSize: 14,
                    marginLeft: 4,
                  },
                  typography.Note2,
                ]}>
                {item.item &&
                item.item.userConnectStatus &&
                item.item &&
                item.item.connectDepth === 1
                  ? '• 1st'
                  : item.item && item.item.connectDepth === 2
                  ? '• 2nd'
                  : ''}
              </Text>
            </TouchableOpacity>

            <View style={{flexDirection: 'row'}}>
              <Text
                style={[
                  typography.Note,
                  {
                    color: COLORS.altgreen_300,
                    fontFamily: 'Montserrat-Medium',
                    marginTop: 5,
                  },
                ]}>
                {unixTime2(item.item.time)}
              </Text>
              <TouchableOpacity
                style={defaultShape.Nav_Gylph_Btn}
                onPress={() => {
                  this.setState(
                    {
                      optionsModal2Open: true,
                      pressedUserId: item.item.userId,
                      pressedActivityId: item.item.id,
                    },
                    () => this.verifyReported(),
                  );
                  setTimeout(() => {
                    this.setState({
                      commentBody: str.replace(/<br\s*[\/]?>/gi, '\n'),
                    });
                  }, 500);
                }}>
                <Icon
                  name="Kebab"
                  color={COLORS.altgreen_400}
                  size={14}
                  style={{marginTop: -8}}
                />
              </TouchableOpacity>
            </View>
          </View>

          <Text style={[typography.Body_2, styles.commentBody]}>
            {str.replace(/<br\s*[\/]?>/gi, '\n')}
          </Text>

          {item.item.attachmentIds.length > 0 ? (
            <FlatList
              keyboardShouldPersistTaps="handled"
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{marginTop: 10, paddingLeft: 32, paddingRight: 30}}
              contentContainerStyle={{paddingRight: 40}}
              data={item.item.attachmentIds}
              keyExtractor={(attachmentItem) => attachmentItem.id}
              renderItem={(attachmentItem) => (
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {
                    this.readStorage(attachmentItem.item.attachmentUrl);
                  }}
                  style={[
                    defaultShape.ContextBtn_FL,
                    {paddingHorizontal: 5, marginRight: 10},
                  ]}>
                  {/* <Text style={[typography.Caption, { color: COLORS.dark_500 }]}>{attachmentItem.item.attachmentUrl.split("-").pop()}</Text> */}
                  <Image
                    source={{uri: attachmentItem.item.attachmentUrl}}
                    style={{width: 250, height: 160, borderRadius: 10}}
                  />
                </TouchableOpacity>
              )}
            />
          ) : (
            <></>
          )}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: 5,
            }}>
            <View></View>

            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({reply: true, replyExpand: true});
                  this.textInputField.focus();
                }}
                style={[
                  defaultShape.Nav_Gylph_Btn,
                  {flexDirection: 'row', marginRight: 5},
                ]}>
                <Icon name="Reply" color={COLORS.altgreen_300} size={14} />
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.altgreen_300,
                      marginLeft: 5,
                      marginTop: Platform.OS === 'ios' ? 0 : -8,
                    },
                  ]}>
                  Reply
                </Text>
              </TouchableOpacity>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TouchableOpacity
                  onPress={() => this.handleLike(item.item.id, item.item.liked)}
                  style={{marginTop: 3, marginRight: 5, alignItems: 'center'}}>
                  <Icon
                    name={item.item.liked ? 'Like_FL' : 'Like'}
                    size={16}
                    color={COLORS.green_500}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                </TouchableOpacity>
                <Text style={{fontSize: 14, color: COLORS.grey_350}}>
                  {item.item.likesCount}
                </Text>
              </View>
              <TouchableOpacity
                style={{
                  width: 20,
                  height: 20,
                  marginRight: 8,
                  marginLeft: -12,
                  marginTop: Platform.OS === 'ios' ? 4 : 8,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() =>
                  this.setState({likeModalOpen: true}, () =>
                    this.getUsersWhoLiked(item.item.id),
                  )
                }>
                <Text
                  style={[{color: COLORS.altgreen_300}, typography.Caption]}>
                  {item.item.likesCount}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  };

  renderCommentItem = (item) => {
    let str = item.item.userComment.replace(/&nbsp;/g, ' ');
    return (
      <>
        <View style={styles.commentItemView}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                item.item.userType === 'INDIVIDUAL'
                  ? this.state.userId === item.item.userId
                    ? this.props.navigation.navigate('ProfileStack', {
                        screen: 'ProfileScreen',
                        // params: { userId: item.item.userId },
                      })
                    : this.props.navigation.navigate('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.item.userId},
                      })
                  : this.props.navigation.navigate('ProfileStack', {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.item.userId},
                    })
              }
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                source={
                  item.item.profileImage
                    ? {uri: item.item.profileImage}
                    : item.item.userType === 'INDIVIDUAL'
                    ? defaultProfile
                    : {uri: 'https://cdn.dscovr.com/images/DefaultBusiness.png'}
                }
                style={defaultShape.Media_Round}
              />
              <Text
                numberOfLines={1}
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_800, marginLeft: 6, maxWidth: 160},
                ]}>
                {item.item.userName}
              </Text>
              <Text
                style={[
                  {
                    color:
                      ((item.item && item.item.connectDepth === -1) ||
                        (item.item && item.item.connectDepth === 0)) &&
                      !item.item.followed
                        ? '#97a600'
                        : '#888',
                    fontSize: 14,
                    marginLeft: 4,
                  },
                  typography.Note2,
                ]}>
                {item.item &&
                item.item.userConnectStatus &&
                item.item &&
                item.item.connectDepth === 1
                  ? '• 1st'
                  : item.item && item.item.connectDepth === 2
                  ? '• 2nd'
                  : ''}
              </Text>
            </TouchableOpacity>

            <View style={{flexDirection: 'row'}}>
              <Text
                style={[
                  typography.Note,
                  {
                    color: COLORS.altgreen_300,
                    fontFamily: 'Montserrat-Medium',
                    marginTop: 5,
                  },
                ]}>
                {unixTime2(item.item.time)}
              </Text>
              <TouchableOpacity
                style={defaultShape.Nav_Gylph_Btn}
                onPress={() => {
                  this.setState(
                    {
                      optionsModal2Open: true,
                      pressedUserId: item.item.userId,
                      pressedActivityId: item.item.id,
                    },
                    () => this.verifyReported(),
                  );
                  setTimeout(() => {
                    this.setState({
                      commentBody: str.replace(/<br\s*[\/]?>/gi, '\n'),
                    });
                  }, 500);
                }}>
                <Icon
                  name="Kebab"
                  color={COLORS.altgreen_400}
                  size={14}
                  style={{marginTop: -8}}
                />
              </TouchableOpacity>
            </View>
          </View>

          <Text style={[typography.Body_2, styles.commentBody]}>
            {str.replace(/<br\s*[\/]?>/gi, '\n')}
          </Text>

          {item.item.attachmentIds.length > 0 ? (
            <FlatList
              keyboardShouldPersistTaps="handled"
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{marginTop: 10, paddingLeft: 32, paddingRight: 30}}
              contentContainerStyle={{paddingRight: 40}}
              data={item.item.attachmentIds}
              keyExtractor={(attachmentItem) => attachmentItem.id}
              renderItem={(attachmentItem) => (
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {
                    this.readStorage(attachmentItem.item.attachmentUrl);
                  }}
                  style={[
                    defaultShape.ContextBtn_FL,
                    {paddingHorizontal: 5, marginRight: 10},
                  ]}>
                  {/* <Text style={[typography.Caption, { color: COLORS.dark_500 }]}>{attachmentItem.item.attachmentUrl.split("-").pop()}</Text> */}
                  <Image
                    source={{uri: attachmentItem.item.attachmentUrl}}
                    style={{width: 250, height: 160, borderRadius: 10}}
                  />
                </TouchableOpacity>
              )}
            />
          ) : (
            <></>
          )}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: 5,
              marginBottom: 6,
            }}>
            {item.item.replies.length > 0 ? (
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    replyExpandId: item.item.id,
                    replyExpand: !this.state.replyExpand,
                  })
                }
                style={{flexDirection: 'row', marginLeft: 38}}>
                <View
                  style={{
                    backgroundColor: COLORS.altgreen_300,
                    height: 14,
                    width: 14,
                    borderRadius: 7,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginRight: 5,
                  }}>
                  <Icon
                    name="Arrow_Down"
                    color={COLORS.white}
                    size={10}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                </View>
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-Medium',
                    },
                  ]}>
                  {item.item.replies.length}{' '}
                  {item.item.replies.length === 1 ? 'reply' : 'replies'}
                </Text>
              </TouchableOpacity>
            ) : (
              <View></View>
            )}

            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    reply: true,
                    replyExpand: true,
                    replyExpandId: item.item.id,
                  });
                  this.textInputField.focus();
                }}
                style={[
                  defaultShape.Nav_Gylph_Btn,
                  {flexDirection: 'row', marginLeft: -14},
                ]}>
                <Icon
                  name="Reply"
                  color={COLORS.altgreen_300}
                  size={14}
                  style={{marginRight: 5}}
                />
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.altgreen_300,
                      marginTop: Platform.OS === 'ios' ? 0 : -8,
                    },
                  ]}>
                  Reply
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleLike(item.item.id, item.item.liked)}
                style={defaultShape.Nav_Gylph_Btn}>
                <Icon
                  name={item.item.liked ? 'Like_FL' : 'Like'}
                  color={COLORS.green_500}
                  size={14}
                  style={{marginTop: 3}}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: 20,
                  height: 20,
                  marginRight: 8,
                  marginLeft: -12,
                  marginTop: Platform.OS === 'ios' ? 4 : 8,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() =>
                  this.setState({likeModalOpen: true}, () =>
                    this.getUsersWhoLiked(item.item.id),
                  )
                }>
                <Text
                  style={[{color: COLORS.altgreen_300}, typography.Caption]}>
                  {item.item.likesCount}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        {this.state.replyExpandId === item.item.id &&
        this.state.replyExpand === true ? (
          <View>
            <FlatList
              // style={{ marginTop: this.state.setreadmore ? 0 : 10 }}
              // horizontal
              // showsHorizontalScrollIndicator={false}
              keyboardShouldPersistTaps="handled"
              data={item.item.replies}
              keyExtractor={(rep) => rep.id}
              renderItem={(rep) => this.renderReplyItem(rep)}
            />
          </View>
        ) : (
          <></>
        )}
      </>
    );
  };

  handleLike = (activityId, liked) => {
    console.log('object like data', activityId);
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          // console.log('********* Hurray ************')
          // this.setState({ ...this.state })
          this.individualPostResponse(this.state.userId);
          this.getAllComment();
        } else {
          console.log('response liked :', response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleCommentSubmit = () => {
    const formData = new FormData();

    this.setState({commentBody: '', commentAttachments: []});

    let params = {
      userId: this.state.userId,
      activityId: this.props.route.params.id,
      userComment: this.state.commentBody.trim('\n'),
    };

    let replyParams = {
      userId: this.state.userId,
      activityId: this.state.replyExpandId,
      userComment: this.state.commentBody.trim('\n'),
      commentType: 'REPLY',
    };

    if (
      this.state.commentAttachments &&
      this.state.commentAttachments.length > 0
    ) {
      this.state.commentAttachments.forEach((file) => {
        formData.append('files', file);
      });
    }

    if (this.state.reply) {
      formData.append('data', JSON.stringify(replyParams));
    } else {
      formData.append('data', JSON.stringify(params));
    }

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/comment/create',
      data: formData,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          this.setState({
            data: {
              ...this.state.data,
              commentCount: this.state.data.commentCount + 1,
            },
            reply: false,
          });
          // if (typeof this.props.getAllComments == 'function') {
          //     this.props.getAllComments();
          // }
          // this.setState({
          //     'isCommentSubmitted': false,
          //     'userComment': '',
          //     'commentAttachments': []
          // });
          // console.log("posted comment: ", response.data)
          this.getAllComment();
        } else {
          console.log(response);
          // this.setState({'isCommentSubmitted': false});
        }
      })
      .catch((err) => {
        console.log(err.response);
        // this.setState({'isCommentSubmitted': false});
      });
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message: this.state.slug
          ? REACT_APP_domainUrl + '/explore-detail/blog/' + this.state.slug
          : REACT_APP_domainUrl + '/post/' + this.props.route.params.id,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation.navigate('RepostOnWenat', {
                    link: false,
                    pressedActivityId: this.props.route.params.id,
                    entityType: 'EXPLOREBLOG',
                    type: 'EXPLOREBLOG',
                  }),
                );
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  this.state.slug
                    ? REACT_APP_domainUrl +
                        '/explore-detail/blog/' +
                        this.state.slug
                    : REACT_APP_domainUrl +
                        '/post/' +
                        this.state.pressedActivityId,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleFollowUnfollow = (isFollowed, userId) => {
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          console.log(response.status);
          this.individualPostResponse(this.state.userId);
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleHideModal = () => {
    let data = {
      userId: this.state.userId,
      activityId: this.props.route.params.id,
      entityType: 'POST',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          console.log('response.status', response.status);
          this.props.navigation.goBack();
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          console.log('response.status', err.response.status);
          this.props.navigation.goBack();
        }
      });
    // this.setState({ optionsModalOpen: false, forumData: this.state.forumData.filter((item) => item.id !== id) })
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.optionsModal()}
        {this.optionsModal2()}
        {this.shareModal()}
        {this.peopleSharedModal()}
        {this.likeModal()}
        {this.reasonForReportingModal()}

        {/****** Header start ******/}

        {this.state.fullscreen ? (
          <></>
        ) : (
          <View style={styles.header}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TouchableOpacity
                onPress={() => this.props.navigation.goBack()}
                style={defaultShape.Nav_Gylph_Btn}>
                <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() =>
                  this.props.route &&
                  this.props.route.params &&
                  this.props.route.params.slug
                    ? null
                    : this.state.data.userType === 'INDIVIDUAL'
                    ? this.state.userId === this.state.data.userId
                      ? this.props.navigation.navigate('ProfileStack', {
                          screen: 'ProfileScreen',
                          // params: { userId: this.state.data.userId },
                        })
                      : this.props.navigation.navigate('ProfileStack', {
                          screen: 'OtherProfileScreen',
                          params: {userId: this.state.data.userId},
                        })
                    : this.props.navigation.navigate('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                        params: {userId: this.state.data.userId},
                      })
                }
                style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  source={
                    this.state.slug
                      ? welogo
                      : this.state.data.userProfileImage
                      ? {uri: this.state.data.userProfileImage}
                      : this.state.data.userType === 'INDIVIDUAL'
                      ? defaultProfile
                      : {
                          uri:
                            'https://cdn.dscovr.com/images/DefaultBusiness.png',
                        }
                  }
                  style={
                    this.state.slug
                      ? {height: 35, width: 25}
                      : defaultShape.Media_Round
                  }
                />
                <View style={{marginLeft: 10}}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Title_2,
                        {color: COLORS.dark_800, maxWidth: 180},
                      ]}>
                      {this.state.data.userName
                        ? this.state.data.userName
                        : "FROM EDITOR'S DESK"}
                    </Text>

                    {!this.state.data.deactivated &&
                    this.state.data.userId !== this.state.userId &&
                    !this.state.isCompany &&
                    this.props.route &&
                    this.props.route.params &&
                    !this.props.route.params.slug ? (
                      <TouchableOpacity
                        onPress={() =>
                          this.handleFollowUnfollow(
                            this.state.data.followed,
                            this.state.data.userId,
                            // index,
                            this.state.data,
                          )
                        }
                        style={{
                          height: 30,
                          width: 30,
                          alignItems: 'center',
                          marginLeft: 4,
                        }}>
                        <Icon
                          name={this.state.data.followed ? 'TickRSS' : 'RSS'}
                          size={13}
                          color={COLORS.dark_600}
                          style={{marginTop: Platform.OS === 'android' ? 4 : 2}}
                        />
                      </TouchableOpacity>
                    ) : null}
                  </View>
                  <Text
                    style={[
                      typography.Note,
                      {
                        color: COLORS.altgreen_300,
                        fontFamily: 'Montserrat-Medium',
                      },
                    ]}>
                    Published on {unixTime2(this.state.data.createTime)}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              style={defaultShape.Nav_Gylph_Btn}
              onPress={() =>
                this.setState({
                  optionsModalOpen: true,
                  pressedActivityId: this.props.route.params.id,
                })
              }>
              <Icon name="Kebab" color={COLORS.altgreen_300} size={16} />
            </TouchableOpacity>
          </View>
        )}

        {/****** Header ends ******/}

        <ScrollView keyboardShouldPersistTaps="handled">
          {/****** Image Carousel start ******/}

          <View>
            {this.state.slug ? (
              <ImageBackground
                source={
                  this.state.data.coverImageUrl
                    ? {uri: this.state.data.coverImageUrl}
                    : BlogDefault
                }
                style={{height: 300, width: '100%'}}>
                <LinearGradient
                  colors={[
                    COLORS.grey_100 + '00',
                    COLORS.grey_100 + 'CC',
                    COLORS.grey_100,
                  ]}
                  style={{
                    height: 100,
                    width: '100%',
                    justifyContent: 'flex-end',
                    position: 'absolute',
                    bottom: 0,
                  }}>
                  <Text
                    style={[
                      typography.Title_1,
                      {color: COLORS.dark_800, marginTop: 10, marginLeft: 20},
                    ]}>
                    {this.state.data.title}
                  </Text>
                </LinearGradient>
              </ImageBackground>
            ) : (
              <ImageBackground
                source={
                  this.state.data.attachmentIds &&
                  this.state.data.attachmentIds.length > 0 &&
                  this.state.data.attachmentIds[0].attachmentUrl
                    ? {uri: this.state.data.attachmentIds[0].attachmentUrl}
                    : BlogDefault
                }
                style={{height: 300, width: '100%'}}>
                <LinearGradient
                  colors={[
                    COLORS.grey_100 + '00',
                    COLORS.grey_100 + 'CC',
                    COLORS.grey_100,
                  ]}
                  style={{
                    height: 100,
                    width: '100%',
                    justifyContent: 'flex-end',
                    position: 'absolute',
                    bottom: 0,
                  }}>
                  <Text
                    style={[
                      typography.Title_1,
                      {color: COLORS.dark_800, marginTop: 10, marginLeft: 20},
                    ]}>
                    {this.state.data.title}
                  </Text>
                </LinearGradient>
              </ImageBackground>
            )}
          </View>

          {/****** Image Carousel ends ******/}
          {/* {!this.state.data.attachmentIds ||
            (this.state.data.attachmentIds.length === 0 && (
              <>
                <Image
                  source={{
                    uri:
                      'https://cdn.dscovr.com/images/banner-explore-blog-small.webp',
                  }}
                  style={{height: 264, width: '95%', alignSelf: 'center'}}
                />

                <Text
                  style={[
                    typography.Title_1,
                    {color: COLORS.dark_800, marginTop: 10, marginLeft: 20},
                  ]}>
                  {this.state.data.title}
                </Text>
              </>
            ))} */}

          {/****** Caption start ******/}

          {this.state.fullscreen ? (
            <></>
          ) : (
            <View style={{marginHorizontal: 20, marginTop: 15}}>
              {this.state.readMore ? (
                <HTMLView
                  value={
                    this.state.slug
                      ? this.state.data.content
                      : this.state.data &&
                        this.state.data.description &&
                        this.state.data.description
                  }
                  stylesheet={styles2}
                />
              ) : (
                <HTMLView
                  value={
                    this.state.slug
                      ? this.state.data.content
                      : this.state.data &&
                        this.state.data.description &&
                        this.state.data.description
                          .split(' ')
                          .slice(0, 60)
                          .join(' ')
                  }
                  stylesheet={styles2}
                />
              )}
            </View>
          )}

          {!this.state.fullscreen &&
          this.state.data &&
          this.state.data.description &&
          this.state.data.description.split(' ').length > 60 ? (
            <TouchableOpacity
              style={{height: 30, width: 80, marginLeft: 20}}
              onPress={() => this.setState({readMore: !this.state.readMore})}>
              <Text style={{color: '#007bff', fontWeight: '700'}}>
                {this.state.readMore ? 'Read less' : 'Read more'}
              </Text>
            </TouchableOpacity>
          ) : (
            <></>
          )}

          {/****** Caption ends ******/}

          {/****** Hashtags start ******/}

          {this.state.fullscreen ? (
            <></>
          ) : (
            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                style={{marginTop: this.state.setreadmore ? 0 : 10}}
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.data.hashTags}
                keyExtractor={(item) => item}
                renderItem={(item) => (
                  <Text
                    onPress={() =>
                      this.props.navigation.navigate('HashTagDetail', {
                        slug: item.item,
                      })
                    }
                    style={[
                      typography.Subtitle_2,
                      {color: '#007bff', marginLeft: 15},
                    ]}>
                    #{item.item}
                  </Text>
                )}
              />
            </View>
          )}

          {/****** Hashtags ends ******/}

          {/****** Like & Share start ******/}

          {this.state.fullscreen ? (
            <></>
          ) : (
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 15,
                justifyContent: 'space-between',
                alignItems: 'center',
                height: 44,
              }}>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() =>
                    this.handleLike(
                      this.props.route.params.id,
                      this.state.data.liked,
                    )
                  }>
                  <Icon
                    name={this.state.data.liked ? 'Like_FL' : 'Like'}
                    color={COLORS.green_500}
                    size={14}
                  />
                </TouchableOpacity>
                <Text
                  style={[
                    {
                      color: COLORS.altgreen_300,
                      marginLeft: 5,
                      marginTop: Platform.OS === 'ios' ? -2 : 2,
                    },
                    typography.Caption,
                  ]}>
                  {this.state.data.likesCount}
                </Text>
              </View>

              <TouchableOpacity
                activeOpacity={0.5}
                style={{flexDirection: 'row'}}
                onPress={() => {
                  this.setState({shareModalOpen: true});
                }}>
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.grey_350,
                      marginRight: 5,
                      marginTop: Platform.OS === 'ios' ? -2 : 2,
                    },
                  ]}>
                  {this.state.data.sharesCount} Share
                </Text>
                <Icon name="Share" color={COLORS.grey_350} size={14} />
              </TouchableOpacity>
            </View>
          )}

          {/****** Like & Share ends ******/}

          {!this.state.slug && (
            <TouchableOpacity
              activeOpacity={0.8}
              style={[
                defaultShape.ContextBtn_FL_Drk,
                {
                  borderRadius: 6,
                  alignSelf: 'center',
                  marginTop: 10,
                  marginBottom: 16,
                },
              ]}
              onPress={() => this.props.navigation.goBack()}>
              <Text
                style={[
                  typography.Caption,
                  {color: COLORS.altgreen_200, paddingVertical: 2},
                ]}>
                View Source
              </Text>
            </TouchableOpacity>
          )}

          {this.state.fullscreen ? (
            <></>
          ) : (
            <View style={styles.commentCountBar}>
              {this.state.commentData.length === 1 ? (
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-Medium',
                    },
                  ]}>
                  {this.state.commentData.length} Comment
                </Text>
              ) : (
                <Text
                  style={[
                    typography.Note,
                    {
                      color: COLORS.altgreen_300,
                      fontFamily: 'Montserrat-Medium',
                    },
                  ]}>
                  {this.state.commentData.length} Comments
                </Text>
              )}
            </View>
          )}

          {/****** Comments start ******/}

          {this.state.fullscreen ? (
            <></>
          ) : (
            <View>
              <FlatList
                keyboardShouldPersistTaps="handled"
                style={{paddingBottom: 50}}
                // horizontal
                // showsHorizontalScrollIndicator={false}
                data={this.state.commentData}
                keyExtractor={(item) => item.id}
                renderItem={(item) => this.renderCommentItem(item)}
              />
            </View>
          )}

          {/****** Comments ends ******/}
        </ScrollView>

        {/****** Comment Box start ******/}

        {this.state.fullscreen ? (
          <></>
        ) : (
          <View
            style={
              this.state.focus && Platform.OS === 'ios'
                ? [styles.commentBoxView, {bottom: 260}]
                : styles.commentBoxView
            }>
            {this.state.commentAttachments.length > 0 ? (
              <FlatList
                keyboardShouldPersistTaps="handled"
                data={this.state.commentAttachments}
                keyExtractor={(item) => item.name}
                renderItem={(item) => (
                  <Text
                    style={[
                      typography.Caption,
                      {
                        color: COLORS.dark_500,
                        marginLeft: 60,
                        marginBottom: 10,
                      },
                    ]}>
                    {item.item.name}
                  </Text>
                )}
              />
            ) : (
              <></>
            )}

            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 15,
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={this.documentPicker}
                style={defaultShape.AdjunctBtn_Small_Prim}>
                <Icon
                  name="Clip"
                  color={COLORS.dark_600}
                  size={14}
                  style={{marginTop: 5}}
                />
              </TouchableOpacity>

              <TextInput
                ref={(ref) => {
                  this.textInputField = ref;
                }}
                placeholder="Comment"
                placeholderTextColor={COLORS.grey_400}
                multiline
                style={styles.inputBox}
                onFocus={() => this.setState({sendButton: true, focus: true})}
                onBlur={() =>
                  this.setState({sendButton: false, focus: false, reply: false})
                }
                onChangeText={(data) => this.setState({commentBody: data})}
                value={this.state.commentBody}
              />

              <TouchableOpacity
                onPress={() =>
                  this.state.edit
                    ? this.handleCommentEdit()
                    : this.handleCommentSubmit()
                }
                style={{marginLeft: 10}}>
                {/* {(this.state.sendButton || this.state.commentAttachments.length > 0) && */}
                {this.state.commentBody.trim().length ||
                this.state.commentAttachments.length ? (
                  <Icon
                    name="Send_fl"
                    color={COLORS.altgreen_400}
                    size={14}
                    style={{marginTop: 5}}
                  />
                ) : (
                  <></>
                )}
              </TouchableOpacity>
            </View>
          </View>
        )}

        {/****** Comment Box ends ******/}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: 44,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: COLORS.white,
    alignItems: 'center',
  },
  crossIcon: {
    marginTop: Platform.OS === 'android' ? 5 : 0,
  },
  commentCountBar: {
    backgroundColor: COLORS.altgreen_200,
    paddingVertical: 6,
    // paddingTop: 12,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  commentItemView: {
    paddingLeft: 15,
    backgroundColor: COLORS.white,
    paddingTop: 12,
    borderBottomColor: COLORS.altgreen_400,
    borderBottomWidth: 0.2,
  },
  commentBody: {
    color: COLORS.altgreen_400,
    marginTop: -10,
    marginLeft: 38,
    maxWidth: '80%',
  },
  commentBoxView: {
    position: 'absolute',
    bottom: 0,
    paddingVertical: 6,
    backgroundColor: COLORS.altgreen_200,
    width: '100%',
    zIndex: 1,
  },
  inputBox: {
    marginLeft: 10,
    width: '80%',
    minHeight: 30,
    maxHeight: 130,
    backgroundColor: COLORS.white,
    borderRadius: 6,
    paddingHorizontal: 10,
  },
});

const styles2 = StyleSheet.create({
  p: {
    color: COLORS.dark_600,
    // fontSize: 12.5,
  },
  span: {
    color: COLORS.dark_600,
    // fontSize: 12.5,
  },
  div: {
    color: COLORS.dark_600,
    // fontSize: 12.5,
  },
  img: {
    width: '100%',
    height: '100%',
  },
});

const mapStateToProps = (state) => {
  return {
    userFeedsPhotosProgress: state.feedsReducer.userFeedsPhotosProgress,
    userFeedsPhotos: state.feedsReducer.userFeedsPhotos,
    errorFeedsPhotos: state.feedsReducer.errorFeedsPhotos,

    userFeedsVideosProgress: state.feedsReducer.userFeedsVideosProgress,
    userFeedsVideos: state.feedsReducer.userFeedsVideos,
    errorFeedsVideos: state.feedsReducer.errorFeedsVideos,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    feedsPhotosRequest: (data) => dispatch(feedsPhotosRequest(data)),
    feedsVideosRequest: (data) => dispatch(feedsVideosRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditorsDesk);
