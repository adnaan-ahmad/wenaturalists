import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
  ScrollView,
  Platform,
  Share,
  Modal,
  Linking,
  Alert,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import Clipboard from '@react-native-community/clipboard';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import {TextInput} from 'react-native-paper';
import Autolink from 'react-native-autolink';

import Report from '../../../Components/User/Common/Report';
import DefaultBusiness from '../../../../assets/DefaultBusiness.png';
import ViewVoters from '../../../Components/User/Poll/ViewVoters';
import LikedUserList from '../../../Components/User/Common/LikedUserList';
import SharedUserList from '../../../Components/User/Common/SharedUserList';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultShape from '../../../Components/Shared/Shape';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import defaultStyle from '../../../Components/Shared/Typography';
import {tagDescription} from '../../../Components/Shared/commonFunction';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');
const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
  'i',
); // fragment locator

export default class PollDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pollData: {},
      shareModalOpen: false,
      likeModalOpen: false,
      answerId: '',
      pressedId: '',
      likeModalOpen: false,
      peopleLiked: [],
      userId: '',
      isReported: false,
      optionsModalOpen: false,
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      pressedActivityId: '',
      viewVoterModalOpen: false,
      peopleVoted: [],
      currentVoterIndex: 1,
      peopleShared: [],
      peopleSharedModalOpen: false,
      isCompany: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      this.setState({userId: value});
    });
    AsyncStorage.getItem('userData').then((value) => {
      let objValue = JSON.parse(value);
      objValue.type === 'COMPANY' && this.setState({isCompany: true});
    });

    this.getPollDetails();
    // this.setState({
    //   peopleVoted:
    //     this.state.pollData && this.state.pollData.userVoteMap
    //       ? this.state.pollData.userVoteMap[1]
    //       : [],
    // });
  }

  peopleSharedModal = () => {
    return (
      <Modal
        transparent
        visible={this.state.peopleSharedModalOpen}
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 300,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({peopleSharedModalOpen: false, peopleShared: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who shared this poll
              </Text>
            </View>

            <SharedUserList
              id={this.state.pollData.id}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  getUsersWhoLiked = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/like/getUsers/' +
        this.state.pollData.id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        0 +
        '&size=' +
        10,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          // console.log('%%%%%%%%%%%%%%%% response.data.body %%%%%%%%%%%%%%%%%%%', response.data.body.content[0])
          this.setState({peopleLiked: response.data.body.content});
        }
      })
      .catch((err) => {
        // console.log(err)
      });
  };

  navigation = (value, params) => {
    this.setState(
      {
        likeModalOpen: false,
        peopleSharedModalOpen: false,
        viewVoterModalOpen: false,
      },
      () => this.props.navigation.navigate(value, params),
    );
  };

  likeModal = () => {
    return (
      <Modal
        visible={this.state.likeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({likeModalOpen: false, peopleLiked: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who liked this poll
              </Text>
            </View>

            <LikedUserList
              id={this.state.pollData.id}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  viewVoterModal = () => {
    return (
      <Modal
        visible={this.state.viewVoterModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 420,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({viewVoterModalOpen: false, peopleVoted: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                VOTERS
              </Text>
            </View>

            <ViewVoters
              votersList={
                this.state.pollData &&
                this.state.pollData.userVoteMap &&
                this.state.pollData.userVoteMap[this.state.currentVoterIndex]
                  ? this.state.pollData.userVoteMap[
                      this.state.currentVoterIndex
                    ]
                  : []
              }
              userType={this.state.pollData.userType}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  trimDescription = (item) => {
    var trimmed = item.split('^^__').join(' ').indexOf('@@@__');

    var str = item
      .split('^^__')
      .join(' ')
      .indexOf(' ', item.split('^^__').join(' ').indexOf('@@@__'));

    var sub = item.substring(trimmed, str);

    item = item.replace(' ' + sub, '');
    item = item.replace('@@@^^^', ' ');
    item = item.replace('@@@__', ' ');
    item = item.replace('  ', '');

    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  totalVoteCount = () => {
    let count = 0;
    for (let i of this.state.pollData.answerList) {
      count += i.totalVote;
    }
    return count;
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          REACT_APP_domainUrl + '/pollDetail/' + this.props.route.params.slug,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of result.activityType');
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation.navigate('SharePoll', {
                    pressedActivityId: this.state.pollData.id,
                  }),
                );
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl +
                    '/pollDetail/' +
                    this.props.route.params.slug,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to poll
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Envelope"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  getPollDetails = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/poll/getBySlug?slug=' +
        this.props.route.params.slug +
        '&userId=' +
        this.props.route.params.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '200 OK') {
          this.setState({
            pollData: res.body,
            peopleVoted:
              res.body.userVoteMap
                ? res.body.userVoteMap[1]
                : [],
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleLike = (activityId, liked) => {
    const data = {
      userId: this.props.route.params.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.getPollDetails();
        } else {
          console.log(response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleVote = (activityId, answerId) => {
    const data = {
      userId: this.props.route.params.userId,
      activityId: activityId,
      answerId: answerId,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/public/poll/vote',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          this.getPollDetails();
        } else {
          console.log(response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pollData.id,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          this.setState({isReported: response.data.body.reported});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleHideModal = (id) => {
    let data = {
      userId: this.state.userId,
      activityId: id,
      entityType: 'POLL',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          console.log('response.status', response.status);
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          console.log('response.status', err.response.status);
        }
      });
    this.setState({optionsModalOpen: false});
    this.props.navigation.goBack();
  };

  deletePoll = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/poll/delete?pollId=' +
        this.state.pressedActivityId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          console.log('response.status', res.status);
        }
      })
      .catch((err) => {
        console.log('response.status', err.response.data.message);
      });
    this.setState({optionsModalOpen: false});
    this.props.navigation.goBack();
  };

  changeState = (value) => {
    this.setState(value);
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <Report
          changeState={this.changeState}
          entityId={this.state.pressedActivityId}
          entityType="POLL"
        />
      </Modal>
    );
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedActivityId,
      // entityType: this.state.entityType,
      entityType: 'POLL',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          // console.log(response)
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          // console.log(err.response)
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          // console.log(err)
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    setTimeout(() => {
      this.setState({reasonForReporting: '', description: ''});
    }, 1000);
  };

  createTwoButtonAlert = () =>
    Alert.alert('', 'Are you sure you want to delete this Poll?', [
      {
        text: 'YES',
        onPress: () => this.deletePoll(),
        style: 'cancel',
      },
      {
        text: 'NO',
        onPress: () => this.setState({optionsModalOpen: false}),
      },
    ]);

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({optionsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          {this.state.userId !== this.state.pollData.userId ? (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
                ]}>
                <Text
                  style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                  Poll
                </Text>
              </View>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {optionsModalOpen: false, likeModalOpen: true},
                    () => this.getUsersWhoLiked(),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Like_FL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who liked it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    optionsModalOpen: false,
                    peopleSharedModalOpen: true,
                  });
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Share"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who shared it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              {!this.state.pollData.pinned && (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {paddingVertical: 15},
                        ]
                      : [
                          defaultShape.ActList_Cell_Gylph_Alt,
                          {borderBottomWidth: 0},
                        ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.handleHideModal(this.state.pressedActivityId);
                  }}>
                  <Text
                    style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                    Hide
                  </Text>
                  <Icon
                    name="Hide"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                  />
                </TouchableOpacity>
              )}

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    reasonForReportingModalOpen: !this.state.isReported
                      ? true
                      : false,
                    optionsModalOpen: false,
                  });
                  this.state.isReported
                    ? setTimeout(() => {
                        Snackbar.show({
                          backgroundColor: '#B22222',
                          text: 'Your report request was already taken',
                          duration: Snackbar.LENGTH_LONG,
                        });
                      }, 500)
                    : null;
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Report
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
                ]}>
                <Text
                  style={[defaultStyle.Caption, {color: COLORS.altgreen_400}]}>
                  Poll
                </Text>
              </View>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {optionsModalOpen: false, likeModalOpen: true},
                    () => this.getUsersWhoLiked(),
                  );
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Like_FL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who liked it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    optionsModalOpen: false,
                    peopleSharedModalOpen: true,
                  });
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Share"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      {color: COLORS.dark_600, marginLeft: 8},
                    ]}>
                    See who shared it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.handleHideModal(this.state.pressedActivityId);
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                  Hide
                </Text>
                <Icon
                  name="Hide"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.createTwoButtonAlert()}
                style={
                  Platform.OS === 'ios'
                    ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {paddingVertical: 15},
                      ]
                    : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        {borderBottomWidth: 0},
                      ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, {color: COLORS.dark_600}]}>
                  Delete
                </Text>
                <Icon
                  name="TrashBin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 8} : {}}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  };

  openWebsite = (website) => {
    if (website && website.includes('http')) return Linking.openURL(website);
    else if (website && !website.includes('http'))
      return Linking.openURL('https://' + website);
  };

  handleFollowUnfollow = (isFollowed, userId) => {
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        console.log(response.status);
        if (response && response.status === 202) {
          this.getPollDetails();
          // tempLikeList[index].followed = !item.followed;
          // this.setState({ peopleLiked: tempLikeList });
        } else {
          console.log(response);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getUserDetailsByCustomUrl = (customurl) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get_by_custom_url?customUrl=' +
        customurl +
        '&otherUserId=' +
        this.state.userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          response.data.body &&
          response.data.body.type === 'INDIVIDUAL' &&
          response.data.body.userId === this.state.userId
            ? this.props.navigation.navigate('ProfileStack')
            : response.data.body.type === 'INDIVIDUAL' &&
              response.data.body.userId !== this.state.userId
            ? this.props.navigation.navigate('ProfileStack', {
                screen: 'OtherProfileScreen',
                params: {userId: response.data.body.userId},
              })
            : response.data.body.type === 'COMPANY'
            ? this.props.navigation.navigate('ProfileStack', {
                screen: 'CompanyProfileScreen',
                params: {userId: response.data.body.userId},
              })
            : this.props.navigation.navigate('CircleProfileStack', {
                screen: 'CircleProfile',
                params: {slug: customurl},
              });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    let {pollData} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.shareModal()}
        {this.likeModal()}
        {this.optionsModal()}
        {this.reasonForReportingModal()}
        {this.viewVoterModal()}
        {this.peopleSharedModal()}

        {/*********** Header starts ***********/}
        <View
          style={
            Platform.OS === 'ios'
              ? [styles.header, {paddingVertical: 15}]
              : styles.header
          }>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              style={{
                width: 40,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="Arrow-Left"
                size={15}
                color="#91B3A2"
                style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                this.props.route.params.userId === pollData.userId &&
                pollData.userType === 'COMPANY'
                  ? this.props.navigation.navigate('ProfileStack', {
                      screen: 'CompanyProfileScreen',
                      // params: { userId: pollData.userId },
                    })
                  : pollData.userType === 'COMPANY'
                  ? this.props.navigation.navigate('ProfileStack', {
                      screen: 'CompanyProfileScreen',
                      params: {userId: pollData.userId},
                    })
                  : this.props.route.params.userId === pollData.userId
                  ? this.props.navigation.navigate('ProfileStack', {
                      // screen: 'ProfileScreen',
                      // params: { userId: pollData.userId },
                    })
                  : this.props.navigation.navigate('ProfileStack', {
                      screen: 'OtherProfileScreen',
                      params: {userId: pollData.userId},
                    })
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                source={
                  pollData.profileImage
                    ? {uri: pollData.profileImage}
                    : pollData.userType === 'COMPANY'
                    ? DefaultBusiness
                    : defaultProfile
                }
                style={styles.profileImage}
              />

              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                  }}>
                  <Text
                    numberOfLines={1}
                    style={[
                      typography.Subtitle_1,
                      {
                        color: COLORS.dark_500,
                        marginLeft: 5,
                        textTransform: 'capitalize',
                        maxWidth: 180,
                      },
                    ]}>
                    {pollData.userName && pollData.userName}
                  </Text>
                  {!pollData.deactivated && (
                    <Text
                      style={[
                        {color: '#888', fontSize: 14, marginLeft: 4},
                        typography.Note2,
                      ]}>
                      {this.state.pollData?.connectStatus ===
                      'PENDING_CONNECT'
                        ? 'Pending'
                        : this.state.pollData.connectDepth === 1
                        ? '• 1st'
                        : this.state.pollData.connectDepth === 2
                        ? '• 2nd'
                        : this.state.pollData.connectDepth === -1 ||
                          this.state.pollData.connectDepth === 0
                        ? ''
                        : ''}
                    </Text>
                  )}

                  {!pollData.deactivated &&
                  pollData.userId !== this.state.userId &&
                  !this.state.isCompany ? (
                    <TouchableOpacity
                      onPress={() =>
                        this.handleFollowUnfollow(
                          pollData.followed,
                          pollData.userId,
                        )
                      }
                      style={{
                        height: 30,
                        width: 30,
                        alignItems: 'center',
                        marginLeft: 0,
                      }}>
                      <Icon
                        name={pollData.followed ? 'TickRSS' : 'RSS'}
                        size={13}
                        color={COLORS.dark_600}
                        style={{marginTop: Platform.OS === 'android' ? 5 : 8}}
                      />
                    </TouchableOpacity>
                  ) : null}
                </View>

                <Text
                  style={[
                    typography.Note2,
                    {
                      color:
                        new Date(pollData.startDate).getTime() >
                        new Date().getTime()
                          ? 'rgb(220, 53, 69)'
                          : '#888',
                      marginLeft: 5,
                    },
                  ]}>
                  {new Date(pollData.startDate).getTime() < new Date().getTime()
                    ? 'Published on '
                    : 'Scheduled on '}
                  <Text
                    style={[{color: '#888', marginTop: 8}, typography.Note2]}>
                    {moment
                      .unix(pollData.startDate / 1000)
                      .format('Do MMM, YYYY')}
                  </Text>
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        {/*********** Header ends ***********/}

        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{paddingBottom: 20}}>
          <Text style={[typography.Title_2, styles.title, {fontSize: 15}]}>
            {pollData.question}
          </Text>

          {/* {pollData.description && regexp.test(pollData.description) ? (
            <Text
              numberOfLines={3}
              onPress={() => this.openWebsite(pollData.description)}
              style={[
                typography.Title_2,
                styles.title,
                {color: '#4068eb', marginTop: 2, fontSize: 13},
              ]}>
              {pollData.description}
            </Text>
          ) :  */}
          <View style={{marginLeft: 16, marginTop: 10, paddingRight: 12}}>
            {pollData && pollData.description ? (
              <Autolink
                text={tagDescription(pollData.description)}
                email
                hashtag="instagram"
                mention="twitter"
                phone="sms"
                numberOfLines={3}
                matchers={[
                  {
                    pattern: /@\[([^[]*)]\(([^(^)]*)\)/g,
                    style: {color: COLORS.mention_color, fontWeight: 'bold'},
                    getLinkText: (replacerArgs) => `${replacerArgs[2]}`,
                    onPress: (match) => {
                      this.getUserDetailsByCustomUrl(
                        match.getReplacerArgs()[1],
                      );
                    },
                  },
                  {
                    pattern: /#\[([^[]*)]\(([^(^)]*)\)/g,
                    style: {color: COLORS.mention_color, fontWeight: 'bold'},
                    getLinkText: (replacerArgs) => `#${replacerArgs[2]}`,
                    onPress: (match) => {
                      this.props.navigation.navigate('HashTagDetail', {
                        slug: match.getReplacerArgs()[1],
                      });
                    },
                  },
                ]}
                style={[
                  typography.Body_1,
                  {
                    color: COLORS.dark_700,
                    marginLeft: 6,
                    marginRight: 17,
                    marginVertical: 8,
                  },
                ]}
                url
              />
            ) : (
              <></>
            )}
          </View>
          {/* <Text
              style={[
                {color: '#00394d', marginTop: 2},
                styles.title,
                typography.Subtitle_2,
              ]}>
              {pollData.description}
            </Text> */}

          <View style={styles.forumItemView}>
            <TouchableOpacity
              style={{
                width: 26,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'flex-end',
              }}
              onPress={() =>
                this.setState(
                  {
                    optionsModalOpen: true,
                    pressedActivityId: pollData.id,
                    pressedUserId: pollData.userId,
                  },
                  () =>
                    pollData.userId !== this.state.userId &&
                    this.verifyReported(),
                )
              }>
              <Icon
                name="Kebab"
                size={14}
                color="#91B3A2"
                style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
              />
            </TouchableOpacity>

            {!pollData.votePoll &&
              pollData.endDate &&
              Date.now() < pollData.endDate &&
              new Date(pollData.startDate).getTime() < new Date().getTime() && (
                <Text
                  numberOfLines={1}
                  style={[
                    {color: '#698f8a', marginVertical: 10},
                    typography.Subtitle_1,
                  ]}>
                  Select an option:
                </Text>
              )}

            <View style={{marginTop: 10}}>
              {pollData.answerList &&
                pollData.answerList.map((option, index) =>
                  pollData.votePoll || Date.now() > pollData.endDate ? (
                    <View style={{marginBottom: 10}}>
                      <View
                        style={[
                          styles.votedPollItem,
                          {paddingLeft: 0, flexWrap: 'wrap'},
                        ]}>
                        <View
                          style={[
                            styles.progressView,
                            {
                              width: `${
                                (option.totalVote / this.totalVoteCount()) * 100
                              }%`,
                              backgroundColor: option.vote
                                ? COLORS.green_300
                                : COLORS.grey_300,
                            },
                          ]}></View>
                        <Text
                          style={{
                            color: '#426f73',
                            zIndex: 2,
                            alignSelf: 'center',
                            marginTop: 10,
                            marginLeft: 10,
                            marginRight: 6,
                            marginBottom: 10,
                            textAlign: 'center',
                          }}>
                          {option.name}
                        </Text>

                        <Text
                          style={{
                            color: '#426f73',
                            zIndex: 2,
                            textAlign: 'center',
                            marginRight: 10,
                            marginLeft: 20,
                            alignSelf: 'center',
                            // marginBottom: 10,
                            // marginTop: 10,
                          }}>
                          {option.totalVote}{' '}
                          {option.totalVote > 1 ? 'votes' : 'vote'}
                        </Text>
                      </View>
                      {/* <View style={{ flexDirection: 'row' }}></View> */}

                      {pollData.userVoteMap &&
                      pollData.userVoteMap[index + 1] &&
                      this.state.userId === this.state.pollData.userId ? (
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              currentVoterIndex: index + 1,
                              viewVoterModalOpen: true,
                              optionsModalOpen: false,
                            })
                          }
                          style={[
                            defaultShape.ActList_Cell_Gylph_Alt,
                            {
                              borderBottomWidth: 0,
                              marginLeft: 4,
                              marginTop: Platform.OS === 'ios' ? -10 : -15,
                              zIndex: 2,
                              marginBottom: Platform.OS === 'ios' ? 0 : 6,
                              // marginVertical: 60
                            },
                          ]}
                          activeOpacity={0.6}>
                          <Text
                            style={[
                              defaultStyle.Button_Lead,
                              {color: COLORS.dark_600, fontSize: 11},
                            ]}>
                            {option.totalVote > 1
                              ? 'View Voters'
                              : 'View Voter'}
                          </Text>
                        </TouchableOpacity>
                      ) : (
                        <></>
                      )}
                    </View>
                  ) : (
                    <View>
                      <TouchableOpacity
                        activeOpacity={
                          new Date(pollData.startDate).getTime() <
                          new Date().getTime()
                            ? 0.6
                            : 1
                        }
                        key={option.id}
                        style={
                          this.state.answerId === option.id
                            ? [
                                styles.pollItem,
                                {backgroundColor: COLORS.green_300},
                              ]
                            : styles.pollItem
                        }
                        onPress={() =>
                          new Date(pollData.startDate).getTime() <
                            new Date().getTime() &&
                          this.setState({answerId: option.id})
                        }>
                        <Text style={{color: '#426f73', textAlign: 'center'}}>
                          {option.name}
                        </Text>
                      </TouchableOpacity>

                      {pollData.userVoteMap &&
                      pollData.userVoteMap[index + 1] &&
                      this.state.userId === this.state.pollData.userId ? (
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              currentVoterIndex: index + 1,
                              viewVoterModalOpen: true,
                              optionsModalOpen: false,
                            })
                          }
                          style={[
                            defaultShape.ActList_Cell_Gylph_Alt,
                            {
                              borderBottomWidth: 0,
                              marginLeft: 4,
                              marginTop: -10,
                              zIndex: 2,
                              marginBottom: 6,
                              // marginVertical: 60
                            },
                          ]}
                          activeOpacity={0.6}>
                          <Text
                            style={[
                              defaultStyle.Button_Lead,
                              {color: COLORS.dark_600, fontSize: 11},
                            ]}>
                            {option.totalVote > 1
                              ? 'View Voters'
                              : 'View Voter'}
                          </Text>
                        </TouchableOpacity>
                      ) : (
                        <></>
                      )}
                    </View>
                  ),
                )}
            </View>

            {(new Date(pollData.startDate).getTime() < new Date().getTime() &&
              pollData.votePoll) ||
            Date.now() > pollData.endDate ? null : this.state.answerId !==
              '' ? (
              <TouchableOpacity
                style={styles.submitBtn}
                onPress={() =>
                  this.handleVote(pollData.id, this.state.answerId)
                }>
                <Text
                  style={[{color: COLORS.white}, typography.Subtitle_2]}
                  numberOfLines={1}>
                  SUBMIT
                </Text>
              </TouchableOpacity>
            ) : new Date(pollData.startDate).getTime() <
              new Date().getTime() ? (
              <View
                style={[styles.submitBtn, {backgroundColor: COLORS.dark_500}]}>
                <Text
                  style={[{color: COLORS.white}, typography.Subtitle_2]}
                  numberOfLines={1}>
                  SUBMIT
                </Text>
              </View>
            ) : (
              <></>
            )}

            <FlatList
              style={{marginTop: 10}}
              horizontal
              showsHorizontalScrollIndicator={false}
              data={pollData.hashTags}
              keyExtractor={(item) => item}
              renderItem={(item) => (
                <Text
                  style={[
                    typography.Subtitle_2,
                    {color: '#007bff', marginRight: 8},
                  ]}
                  onPress={() =>
                    this.props.navigation.navigate('HashTagDetail', {
                      slug: item.item,
                    })
                  }>
                  #{item.item}
                </Text>
              )}
            />

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
                marginBottom: 5,
                alignItems: 'center',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TouchableOpacity
                  onPress={() => this.handleLike(pollData.id, pollData.liked)}
                  activeOpacity={0.5}
                  style={{alignItems: 'center', flexDirection: 'row'}}>
                  <Icon
                    name={pollData.liked ? 'Like_FL' : 'Like'}
                    color={COLORS.green_500}
                    size={14}
                    style={{
                      marginTop: Platform.OS === 'android' ? 10 : 0,
                      marginHorizontal: 6,
                    }}
                  />
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={0.9}
                  onPress={() => this.handleLike(pollData.id, pollData.liked)}>
                  {pollData.likesCount > 1 ? (
                    <Text
                      //   onPress={() =>
                      //   this.handleLike(pollData.id, pollData.liked)
                      // }
                      style={[
                        {color: COLORS.altgreen_300},
                        typography.Caption,
                      ]}>
                      {pollData.likesCount} Likes
                    </Text>
                  ) : pollData.likesCount === 1 ? (
                    <Text
                      //   onPress={() =>
                      //   this.handleLike(pollData.id, pollData.liked)
                      // }
                      style={[
                        {color: COLORS.altgreen_300},
                        typography.Caption,
                      ]}>
                      1 Like
                    </Text>
                  ) : (
                    <Text
                      //   onPress={() =>
                      //   this.handleLike(pollData.id, pollData.liked)
                      // }
                      style={[
                        {color: COLORS.altgreen_300},
                        typography.Caption,
                      ]}>
                      0 Like
                    </Text>
                  )}
                </TouchableOpacity>
              </View>

              <TouchableOpacity
                activeOpacity={0.5}
                style={{flexDirection: 'row', marginTop: 10}}>
                <Text
                  onPress={() => {
                    this.setState({shareModalOpen: true});
                  }}
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.grey_350,
                      marginRight: 5,
                      marginTop: Platform.OS === 'ios' ? -2 : 2,
                    },
                  ]}>
                  {pollData.sharesCount ? pollData.sharesCount : null} Share
                </Text>
                <Icon
                  onPress={() => {
                    this.setState({shareModalOpen: true});
                  }}
                  name="Share"
                  color={COLORS.grey_350}
                  size={14}
                />
              </TouchableOpacity>
            </View>
          </View>

          {!this.props.route.params.hidden && (
            <TouchableOpacity
              activeOpacity={0.8}
              style={[
                defaultShape.ContextBtn_FL_Drk,
                {
                  borderRadius: 6,
                  width: 100,
                  alignSelf: 'center',
                  marginTop: 30,
                },
              ]}
              onPress={() => this.props.navigation.goBack('')}>
              <Text
                style={[
                  typography.Caption,
                  {color: COLORS.altgreen_200, paddingVertical: 2},
                ]}>
                View Source
              </Text>
            </TouchableOpacity>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  forumItemView: {
    width: '92%',
    marginTop: 15,
    backgroundColor: COLORS.white,
    borderRadius: 4,
    alignSelf: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
  profileImage: {
    height: 30,
    width: 30,
    borderRadius: 15,
    marginRight: 8,
  },
  title: {
    marginTop: 15,
    color: COLORS.dark_800,
    marginHorizontal: 15,
  },
  subtitle: {
    color: COLORS.dark_600,
    marginTop: 5,
  },
  body: {
    color: COLORS.altgreen_400,
    marginTop: 5,
  },
  commentCountBar: {
    backgroundColor: COLORS.altgreen_100,
    paddingVertical: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  commentItemView: {
    paddingLeft: 15,
    backgroundColor: COLORS.white,
    paddingTop: 12,
    borderBottomColor: COLORS.altgreen_400,
    borderBottomWidth: 0.2,
  },
  commentBody: {
    color: COLORS.altgreen_400,
    marginTop: -25,
    marginLeft: 38,
    maxWidth: '80%',
  },
  commentBoxView: {
    position: 'absolute',
    bottom: 0,
    paddingVertical: 6,
    backgroundColor: COLORS.altgreen_200,
    width: '100%',
    zIndex: 1,
  },
  inputBox: {
    marginLeft: 10,
    width: '80%',
    height: 40,
    backgroundColor: COLORS.white,
    borderRadius: 6,
    paddingHorizontal: 10,
  },
  submitBtn: {
    width: 90,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primarydark,
    borderRadius: 4,
    marginVertical: 10,
  },
  pollItem: {
    maxWidth: '100%',
    minHeight: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f2f3f4',
    borderRadius: 4,
    marginBottom: 10,
    padding: 8,
  },
  votedPollItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    maxWidth: '100%',
    minHeight: 30,
    alignItems: 'center',
    backgroundColor: '#f2f3f4',
    borderRadius: 4,
    marginBottom: 10,
    // paddingRight: 10,
  },
  progressView: {
    maxWidth: '100%',
    minHeight: 30,
    height: '100%',
    borderRadius: 4,
    marginBottom: 10,
    position: 'absolute',
    left: 0,
    zIndex: 1,
  },
});
