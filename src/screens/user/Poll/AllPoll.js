import React, { Component } from 'react';
import {
  Alert,
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
  Modal,
  ScrollView,
  Share,
  Linking,
  RefreshControl,
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { TextInput } from 'react-native-paper';
import DatePicker from 'react-native-datepicker';
import { connect } from 'react-redux';
import { cloneDeep } from 'lodash';
import Snackbar from 'react-native-snackbar';
import Clipboard from '@react-native-community/clipboard';
import LinearGradient from 'react-native-linear-gradient';
import Autolink from 'react-native-autolink';
import Report from '../../../Components/User/Common/Report';

import SuggestedHashTags from '../../../Components/User/Common/SuggestedHashTags';
import DefaultBusiness from '../../../../assets/DefaultBusiness.png';
import LikedUserList from '../../../Components/User/Common/LikedUserList';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultStyle from '../../../Components/Shared/Typography';
import { personalProfileRequest } from '../../../services/Redux/Actions/User/PersonalProfileActions';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import defaultCover from '../../../../assets/defaultCover.png';
import defaultShape from '../../../Components/Shared/Shape';
import { COLORS } from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import PollHeader from '../../../Components/User/Poll/PollHeader';
import SkeletonLoader from '../../../Components/Shared/SkeletonLoader';
import AddHashTags from '../../../Components/User/Common/AddHashTags';
import { tagDescription } from '../../../Components/Shared/commonFunction';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');
const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
  '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
  '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
  '(\\#[-a-z\\d_]*)?$',
  'i',
); // fragment locator

class AllPoll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      selectedTab: '',
      searchParam: '',
      page: 0,
      pollData: [],
      answerId: '',
      activityId: '',
      slug: '',
      selectedTrending: false,
      createPollModalOpen: false,
      shareModalOpen: false,
      startDate: 0,
      endDate: 0,
      hashTags: [],
      keyPressed: '',
      pollQuestion: '',
      writeSomething: '',
      publishButton: true,
      shareholders: [
        { name: '', id: '' },
        { name: '', id: '' },
      ],
      pollAnswersList: [],
      currentValue: '',
      pressedActivityId: '',
      pressedUserId: '',
      reasonForReportingModalOpen: false,
      reasonForReporting: '',
      description: '',
      optionsModalOpen: false,
      pressedId: '',
      likeModalOpen: false,
      peopleLiked: [],
      isReported: false,
      addHashTagsModalOpen: false,
      seconds: 59,
      peopleShared: [],
      pressedItem: {},
      refreshing: false,
      userData: {},
      suggestedHashTagsModalOpen: false
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      value &&
        this.setState({ userId: value }, () => {
          this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.onRefresh();
          });
        });
      value && this.getPollList(value, '', '');
    });

    AsyncStorage.getItem('userData').then((value) => {
      this.setState({ userData: JSON.parse(value) });
    });
  }

  // componentDidUpdate() {
  //   if (this.state.seconds === 0 && this.state.createPollModalOpen) {
  //     this.saveAsDraft();
  //   }
  // }

  componentWillUnmount() {
    clearInterval(this.interval);
    this._unsubscribe();
  }

  changeHashTagsState = (value) => {
    this.setState(value);
  };

  addHashTagsModal = () => {
    return (
      <Modal
        visible={this.state.addHashTagsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <AddHashTags
          changeHashTagsState={this.changeHashTagsState}
          hashTags={this.state.hashTags}
          currentOpen='POLL'
        />
      </Modal>
    )
  }

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.pressedActivityId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          console.log(response.data.body.reported);
          this.setState({ isReported: response.data.body.reported });
        } else {
          console.log(response);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  navigation = (value, params) => {
    this.setState({ likeModalOpen: false }, () =>
      this.props.navigation.navigate(value, params),
    );
  };

  likeModal = () => {
    return (
      <Modal
        visible={this.state.likeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, { bottom: 300 }]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({ likeModalOpen: false, peopleLiked: [] })
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                Who liked this poll
              </Text>
            </View>

            <LikedUserList
              id={this.state.pressedId}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  handleHideModal = (id) => {
    let data = {
      userId: this.state.userId,
      activityId: id,
      entityType: 'POLL',
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          console.log('response.status', response.status);
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          console.log('response.status', err.response.status);
        }
      });
    this.setState({
      optionsModalOpen: false,
      pollData: this.state.pollData.filter((item) => item.id !== id),
    });
  };

  deletePoll = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/public/poll/delete?pollId=' +
        this.state.pressedActivityId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.status === '202 ACCEPTED') {
          console.log('response.status', res.status);
        }
      })
      .catch((err) => {
        console.log('response.status', err.response.data.message);
      });
    this.setState({
      optionsModalOpen: false,
      pollData: this.state.pollData.filter(
        (item) => item.id !== this.state.pressedActivityId,
      ),
    });
  };

  changeState2 = (value) => {
    this.setState(value);
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <Report
          changeState={this.changeState2}
          entityId={this.state.pressedActivityId}
          entityType="POLL"
        />
      </Modal>
    );
  };

  handleReportAbuseSubmit = () => {
    let data = {
      reporterId: this.state.userId,
      entityId: this.state.pressedActivityId,
      // entityType: this.state.entityType,
      entityType: 'POLL',
      reason: this.state.reasonForReporting,
      description: this.state.description,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/reportabuse/add',
      data: data,
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 201) {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text:
              'Your request has been taken and appropriate action will be taken as per our report abuse policy',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
          console.log(response);
        } else {
          console.log(response);
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.status === 409) {
          console.log(err.response);
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: 'Your report request was already taken',
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        } else {
          console.log(err);
          Snackbar.show({
            backgroundColor: '#B22222',
            text: 'Please check your network or try again later',
            duration: Snackbar.LENGTH_LONG,
          });
        }
      });
    setTimeout(() => {
      this.setState({ reasonForReporting: '', description: '' });
    }, 1000);
  };

  createTwoButtonAlert = () =>
    Alert.alert('', 'Are you sure you want to delete this Poll?', [
      {
        text: 'YES',
        onPress: () => this.deletePoll(),
        style: 'cancel',
      },
      { text: 'NO' },
    ]);

  optionsModal = () => {
    return (
      <Modal
        visible={this.state.optionsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={[defaultShape.Linear_Gradient_View, { bottom: 50 }]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ optionsModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
            />
          </TouchableOpacity>

          {this.state.userId !== this.state.pressedUserId ? (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
                ]}>
                <Text
                  style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>
                  Poll
                </Text>
              </View>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { paddingVertical: 15 },
                    ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({ optionsModalOpen: false, likeModalOpen: true });
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Like_FL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? { marginTop: 9 } : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      { color: COLORS.dark_600, marginLeft: 8 },
                    ]}>
                    See who liked it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
              </TouchableOpacity>

              {this.state.selectedTab !== 'PINNED' ? (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        { paddingVertical: 15 },
                      ]
                      : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        { borderBottomWidth: 0 },
                      ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.handleHideModal(this.state.pressedActivityId);
                  }}>
                  <Text
                    style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                    Hide
                  </Text>
                  <Icon
                    name="Hide"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                  />
                </TouchableOpacity>
              ) : (
                <></>
              )}

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { paddingVertical: 15 },
                    ]
                    : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { borderBottomWidth: 0 },
                    ]
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({
                    reasonForReportingModalOpen: !this.state.isReported
                      ? true
                      : false,
                    optionsModalOpen: false,
                  });
                  this.state.isReported
                    ? setTimeout(() => {
                      Snackbar.show({
                        backgroundColor: '#B22222',
                        text: 'Your report request was already taken',
                        duration: Snackbar.LENGTH_LONG,
                      });
                    }, 1500)
                    : null;
                }}>
                <Text
                  style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                  Report
                </Text>
                <Icon
                  name="ReportComment_OL"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({ shareModalOpen: true, optionsModalOpen: false })
                }
                style={
                  Platform.OS === 'ios'
                    ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { paddingVertical: 15 },
                    ]
                    : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { borderBottomWidth: 0 },
                    ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                  Share
                </Text>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({ optionsModalOpen: false }, () =>
                    this.handlePinnedForumAndPoll(this.state.pressedItem),
                  )
                }
                style={
                  Platform.OS === 'ios'
                    ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { paddingVertical: 15 },
                    ]
                    : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { borderBottomWidth: 0 },
                    ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                  {this.state.pressedItem &&
                    this.state.pressedItem.item &&
                    this.state.pressedItem.item.pinned
                    ? 'Unpin'
                    : 'Pin'}{' '}
                </Text>
                <Icon
                  name="Pin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={defaultShape.Modal_Categories_Container}>
              <View
                style={[
                  defaultShape.ActList_Cell_Gylph_Alt,
                  { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
                ]}>
                <Text
                  style={[defaultStyle.Caption, { color: COLORS.altgreen_400 }]}>
                  Poll
                </Text>
              </View>

              <TouchableOpacity
                style={
                  Platform.OS === 'ios'
                    ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { paddingVertical: 15 },
                    ]
                    : defaultShape.ActList_Cell_Gylph_Alt
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState({ optionsModalOpen: false, likeModalOpen: true });
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Like_FL"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? { marginTop: 9 } : {}}
                  />
                  <Text
                    style={[
                      typography.Button_Lead,
                      { color: COLORS.dark_600, marginLeft: 8 },
                    ]}>
                    See who liked it
                  </Text>
                </View>
                <Icon
                  name="Arrow_Right"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
              </TouchableOpacity>

              {this.state.selectedTab !== 'PINNED' ? (
                <TouchableOpacity
                  style={
                    Platform.OS === 'ios'
                      ? [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        { paddingVertical: 15 },
                      ]
                      : [
                        defaultShape.ActList_Cell_Gylph_Alt,
                        { borderBottomWidth: 0 },
                      ]
                  }
                  activeOpacity={0.6}
                  onPress={() => {
                    this.handleHideModal(this.state.pressedActivityId);
                  }}>
                  <Text
                    style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                    Hide
                  </Text>
                  <Icon
                    name="Hide"
                    size={17}
                    color={COLORS.altgreen_300}
                    style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                  />
                </TouchableOpacity>
              ) : (
                <></>
              )}

              <TouchableOpacity
                onPress={() => this.createTwoButtonAlert()}
                style={
                  Platform.OS === 'ios'
                    ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { paddingVertical: 15 },
                    ]
                    : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { borderBottomWidth: 0 },
                    ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                  Delete
                </Text>
                <Icon
                  name="TrashBin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({ shareModalOpen: true, optionsModalOpen: false })
                }
                style={
                  Platform.OS === 'ios'
                    ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { paddingVertical: 15 },
                    ]
                    : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { borderBottomWidth: 0 },
                    ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                  Share
                </Text>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({ optionsModalOpen: false }, () =>
                    this.handlePinnedForumAndPoll(this.state.pressedItem),
                  )
                }
                style={
                  Platform.OS === 'ios'
                    ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { paddingVertical: 15 },
                    ]
                    : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      { borderBottomWidth: 0 },
                    ]
                }
                activeOpacity={0.6}>
                <Text
                  style={[defaultStyle.Button_Lead, { color: COLORS.dark_600 }]}>
                  {this.state.pressedItem &&
                    this.state.pressedItem.item &&
                    this.state.pressedItem.item.pinned
                    ? 'Unpin'
                    : 'Pin'}{' '}
                </Text>
                <Icon
                  name="Pin"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  };

  changeState = (item) => {
    this.setState(item, () =>
      this.getPollList(
        this.state.userId,
        this.state.selectedTab,
        this.state.searchParam,
      ),
    );
  };

  trimDescription = (item) => {
    var trimmed = item.split('^^__').join(' ').indexOf('@@@__');

    var str = item
      .split('^^__')
      .join(' ')
      .indexOf(' ', item.split('^^__').join(' ').indexOf('@@@__'));

    var sub = item.substring(trimmed, str);

    item = item.replace(' ' + sub, '');
    item = item.replace('@@@^^^', ' ');
    item = item.replace('@@@__', ' ');
    item = item.replace('  ', '');

    item = item.replace(/&nbsp;/g, ' ');
    item = item.replace(/<br\s*[\/]?>/gi, '\n');

    const regex = /(<([^>]+)>)/gi;
    item = item.replace(regex, '');

    return item.split('^^__').join(' ');
  };

  getPollList = (userId, showType, searchParam, shouldShowLoader) => {
    !shouldShowLoader && this.setState({ isLoading: true });
    axios({
      method: 'get',
      url: this.state.selectedTrending
        ? REACT_APP_userServiceURL +
        '/poll/trending?userId=' +
        userId +
        '&page=' +
        this.state.page +
        '&size=10'
        : REACT_APP_userServiceURL +
        '/poll/list?showType=' +
        showType +
        '&filterType=POLL&userId=' +
        userId +
        '&searchParam=' +
        searchParam +
        '&page=' +
        this.state.page +
        '&size=10',
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          console.log('pollList', res.body.content[0]);
          this.setState({
            pollData: this.state.pollData.concat(res.body.content),
            isLoading: false,
          });
        }
      })
      .catch((err) => {
        this.setState({ isLoading: false });
        console.log(err);
      });
  };

  handlePinnedForumAndPoll = (item) => {
    let tempPollData = cloneDeep(this.state.pollData);
    let pinnedBody = {
      userId: this.state.userId,
      entityId: item.item.id,
      entityType: 'POLL',
      pinned: !item.item.pinned,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/pinned/create',
      headers: { 'Content-Type': 'application/json' },
      data: pinnedBody,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          tempPollData[item.index].pinned = !item.item.pinned;
          this.setState({ pollData: tempPollData });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleLike = (activityId, liked, index) => {
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          let tempPollData = cloneDeep(this.state.pollData);
          tempPollData[index].liked = !liked;
          liked === false
            ? (tempPollData[index].likesCount += 1)
            : (tempPollData[index].likesCount -= 1);
          this.setState({ pollData: tempPollData });
        } else {
          console.log(response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleVote = (activityId, answerId, itemIndex) => {
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      answerId: answerId,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/public/poll/vote',
      data: data,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          let tempPollData = cloneDeep(this.state.pollData);
          tempPollData[itemIndex].votePoll = true;
          tempPollData[itemIndex].answerList[answerId - 1].vote = true;
          tempPollData[itemIndex].answerList[answerId - 1].totalVote += 1;
          this.setState({ pollData: tempPollData });
        } else {
          console.log(response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  totalVoteCount = (item) => {
    let count = 0;
    for (let i of item) {
      count += i.totalVote;
    }
    return count;
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message: REACT_APP_domainUrl + '/pollDetail/' + this.state.slug,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('shared with activity type of ', result.activityType);
        } else {
          console.log('shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismissed');
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{ marginTop: 'auto' }}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({ shareModalOpen: false })}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{ marginTop: Platform.OS === 'android' ? 5 : 0 }}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                { justifyContent: 'center', paddingBottom: 10, paddingTop: 0 },
              ]}>
              <Text style={[typography.Caption, { color: COLORS.altgreen_400 }]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { paddingTop: 25, paddingBottom: 15 },
                  ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ shareModalOpen: false }, () =>
                  this.props.navigation.navigate('SharePoll', {
                    pressedActivityId: this.state.pressedActivityId,
                  }),
                );
              }}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl + '/pollDetail/' + this.state.slug,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Copy link to poll
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity>

            {/* <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({ shareModalOpen: false });
              }}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Share through Mail
              </Text>
              <Icon
                name="Mail_OL"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? { marginTop: 8 } : {}}
              />
            </TouchableOpacity> */}

            <TouchableOpacity
              onPress={() => {
                this.setState({ shareModalOpen: false }, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, { paddingVertical: 15 }]
                  : [
                    defaultShape.ActList_Cell_Gylph_Alt,
                    { borderBottomWidth: 0 },
                  ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, { color: COLORS.dark_600 }]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                  <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Envelope"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{ marginTop: Platform.OS === 'android' ? 10 : 0 }}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleFollowUnfollow = (isFollowed, userId) => {
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          let tempPollData = cloneDeep(this.state.pollData);

          for (let i of tempPollData) {
            if (i.userId === userId) {
              i.followed = !isFollowed;
            }
          }
          this.setState({
            pollData: tempPollData,
          });
        } else {
          console.log(response);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  openWebsite = (website) => {
    if (website && website.includes('http')) return Linking.openURL(website);
    else if (website && !website.includes('http'))
      return Linking.openURL('https://' + website);
  };

  getUserDetailsByCustomUrl = (customurl) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get_by_custom_url?customUrl=' +
        customurl +
        '&otherUserId=' +
        this.state.userId,
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          response.data.body &&
            response.data.body.type === 'INDIVIDUAL' &&
            response.data.body.userId === this.state.userId
            ? this.props.navifation.navigate('ProfileStack')
            : response.data.body.type === 'INDIVIDUAL' &&
              response.data.body.userId !== this.state.userId
              ? this.props.navifation.navigate('ProfileStack', {
                screen: 'OtherProfileScreen',
                params: { userId: response.data.body.userId },
              })
              : response.data.body.type === 'COMPANY'
                ? this.props.navifation.navigate('ProfileStack', {
                  screen: 'CompanyProfileScreen',
                  params: { userId: response.data.body.userId },
                })
                : this.props.navifation.navigate('CircleProfileStack', {
                  screen: 'CircleProfile',
                  params: { slug: customurl },
                });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  renderPollItem = (item) => {
    return (
      <View style={styles.forumItemView}>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'flex-end',
            alignItems: 'flex-end',
            justifyContent: 'flex-end',
          }}>
          <TouchableOpacity
            style={{
              width: 26,
              height: 30,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() =>
              this.setState(
                {
                  pressedItem: item,
                  optionsModalOpen: true,
                  pressedActivityId: item.item.id,
                  pressedUserId: item.item.userId,
                  slug: item.item.slug,
                  pressedId: item.item.id,
                },
                () =>
                  this.state.pressedUserId !== this.state.userId &&
                  this.verifyReported(),
              )
            }>
            <Icon
              name="Kebab"
              size={14}
              color="#91B3A2"
              style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }}
            />
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('PollDetails', {
              slug: item.item.slug,
              userId: this.state.userId,
              pollData: this.state.pollData,
              connectStatus: item.item.connectStatus,
              connectDepth: item.item.connectDepth,
            })
          }>
          <Text
            numberOfLines={1}
            style={[{ color: '#00394d', fontWeight: 'bold' }, typography.H5]}>
            {item.item.question}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.9}
          style={{ marginLeft: 0, marginTop: 10 }}
          onPress={() =>
            this.props.navigation.navigate('PollDetails', {
              slug: item.item.slug,
              userId: this.state.userId,
              pollData: this.state.pollData,
              connectStatus: item.item.connectStatus,
              connectDepth: item.item.connectDepth,
            })
          }>
          <Autolink
            text={tagDescription(item.item.description)}
            email
            hashtag="instagram"
            mention="twitter"
            phone="sms"
            numberOfLines={3}
            matchers={[
              {
                pattern: /@\[([^[]*)]\(([^(^)]*)\)/g,
                style: { color: COLORS.mention_color, fontWeight: 'bold' },
                getLinkText: (replacerArgs) => `${replacerArgs[2]}`,
                onPress: (match) => {
                  this.getUserDetailsByCustomUrl(match.getReplacerArgs()[1]);
                },
              },
              {
                pattern: /#\[([^[]*)]\(([^(^)]*)\)/g,
                style: { color: COLORS.mention_color, fontWeight: 'bold' },
                getLinkText: (replacerArgs) => `#${replacerArgs[2]}`,
                onPress: (match) => {
                  this.props.navigation.navigate('HashTagDetail', {
                    slug: match.getReplacerArgs()[1],
                  });
                },
              },
            ]}
            style={[
              typography.Body_1,
              {
                color:
                  item.postType === 'ARTICLE'
                    ? COLORS.dark_500
                    : COLORS.dark_700,
                marginLeft: 6,
                marginRight: 17,
                marginVertical: 8,
              },
            ]}
            url
          />
        </TouchableOpacity>

        {item.item.description &&
          item.item.description.split(' ').length > 20 ? (
          <TouchableOpacity
            activeOpacity={0.6}
            style={{ height: 30, width: 80 }}
            onPress={() =>
              this.props.navigation.navigate('PollDetails', {
                slug: item.item.slug,
                userId: this.state.userId,
                pollData: this.state.pollData,
                connectStatus: item.item.connectStatus,
                connectDepth: item.item.connectDepth,
              })
            }>
            <Text style={{ color: '#4068eb', fontWeight: '700', fontSize: 13 }}>
              Read more
            </Text>
          </TouchableOpacity>
        ) : null}

        {!item.item.votePoll &&
          item.item.endDate &&
          Date.now() < item.item.endDate &&
          new Date(item.item.startDate).getTime() < new Date().getTime() &&
           (
            <Text
              numberOfLines={1}
              style={[
                { color: '#698f8a', marginVertical: 10 },
                typography.Subtitle_1,
              ]}>
              Select an option:
            </Text>
          )}

        <View style={{ marginTop: 20 }}>
          {item.item.answerList &&
            item.item.answerList.map((option) =>
              item.item.votePoll || Date.now() > item.item.endDate ? (
                <View
                  style={[
                    styles.votedPollItem,
                    { flexDirection: 'row', paddingRight: 10, flexWrap: 'wrap' },
                  ]}>
                  <View
                    style={[
                      styles.progressView,
                      {
                        width: `${(option.totalVote /
                            this.totalVoteCount(item.item.answerList)) *
                          100
                          }%`,
                        backgroundColor: option.vote
                          ? COLORS.green_300
                          : COLORS.grey_300,
                      },
                    ]}></View>
                  <Text
                    style={{
                      color: '#426f73',
                      zIndex: 2,
                      alignSelf: 'center',
                      marginTop: 10,
                      marginLeft: 10,
                      marginRight: 6,
                      textAlign: 'center'
                    }}>
                    {option.name}
                  </Text>
                  <Text
                    style={{
                      color: '#426f73',
                      zIndex: 2,
                      textAlign: 'center',
                      marginBottom: 10,
                      marginTop: 10,
                      marginLeft: 10,
                    }}>
                    {option.totalVote} {option.totalVote > 1 ? 'votes' : 'vote'}
                  </Text>
                </View>
              ) : (
                <TouchableOpacity
                  activeOpacity={
                    new Date(item.item.startDate).getTime() <
                      new Date().getTime()
                      ? 0.6
                      : 1
                  }
                  key={option.id}
                  style={
                    this.state.answerId === option.id &&
                      item.item.id === this.state.activityId
                      ? [styles.pollItem, { backgroundColor: COLORS.green_300 }]
                      : styles.pollItem
                  }
                  onPress={() =>
                    new Date(item.item.startDate).getTime() <
                    new Date().getTime() &&
                    this.setState({
                      answerId: option.id,
                      activityId: item.item.id,
                    })
                  }>
                  <Text style={{ color: '#426f73', textAlign: 'center' }}>{option.name}</Text>
                </TouchableOpacity>
              ),
            )}
        </View>

        {(new Date(item.item.startDate).getTime() < new Date().getTime() &&
          item.item.votePoll) ||
          Date.now() > item.item.endDate ? null : this.state.answerId !== '' &&
            item.item.id === this.state.activityId ? (
          <TouchableOpacity
            style={styles.submitBtn}
            onPress={() =>
              this.handleVote(item.item.id, this.state.answerId, item.index)
            }>
            <Text
              style={[{ color: COLORS.white }, typography.Subtitle_2]}
              numberOfLines={1}>
              SUBMIT
            </Text>
          </TouchableOpacity>
        ) : new Date(item.item.startDate).getTime() < new Date().getTime() ? (
          <View style={[styles.submitBtn, { backgroundColor: COLORS.dark_500 }]}>
            <Text
              style={[{ color: COLORS.white }, typography.Subtitle_2]}
              numberOfLines={1}>
              SUBMIT
            </Text>
          </View>
        ) : (
          <></>
        )}

        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() =>
            // console.log(item.item)
            this.state.userId === item.item.userId &&
              item.item.userType === 'COMPANY'
              ? this.props.navigation.navigate('ProfileStack', {
                screen: 'CompanyProfileScreen',
                // params: { userId: pollData.userId },
              })
              : item.item.userType === 'COMPANY'
                ? this.props.navigation.navigate('ProfileStack', {
                  screen: 'CompanyProfileScreen',
                  params: { userId: item.item.userId },
                })
                : this.state.userId === item.item.userId
                  ? this.props.navigation.navigate('ProfileStack')
                  : this.props.navigation.navigate('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: { userId: item.item.userId },
                  })
          }
          style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image
            source={
              item.item.originalProfileImage
                ? { uri: item.item.originalProfileImage }
                : item.item.userType === 'COMPANY'
                  ? DefaultBusiness
                  : defaultProfile
            }
            style={{ width: 30, height: 30, borderRadius: 15 }}
          />
          <Text
            numberOfLines={1}
            style={[
              {
                color: '#00394d',
                marginLeft: 6,
                textTransform: 'capitalize',
                maxWidth: '60%',
              },
              typography.OVERLINE,
            ]}>
            {item.item && item.item.userName && this.trimDescription(item.item.userName)}
          </Text>

          {!item.item.deactivated && (
            <Text
              style={[
                { color: '#888', fontSize: 14, marginLeft: 4 },
                typography.Note2,
              ]}
              onPress={() =>
                item.item.connectDepth === -1 ||
                  (item.item.connectDepth === 0 &&
                    item.item.userConnectStatus &&
                    item.item.userConnectStatus.connectStatus !==
                    'PENDING_CONNECT' &&
                    this.state.userData.type &&
                    this.state.userData.type !== 'COMPANY')
                  ? this.handleFollowUnfollow(
                    item.item.followed,
                    item.item.userId,
                  )
                  : null
              }>
              {item.item.connectStatus === 'PENDING_CONNECT'
                ? 'Pending'
                : item.item.connectDepth === 1
                  ? '• 1st'
                  : item.item.connectDepth === 2
                    ? '• 2nd'
                    : ''}
            </Text>
          )}

          {this.state.userId !== item.item.userId &&
            this.state.userData.type &&
            this.state.userData.type !== 'COMPANY' &&
            !item.item.deactivated ? (
            <TouchableOpacity
              style={{ height: 30, width: 30, alignItems: 'center' }}
              onPress={() =>
                this.handleFollowUnfollow(
                  item.item.followed,
                  item.item.userId,
                  item.index,
                )
              }>
              <Icon
                name={item.item.followed ? 'TickRSS' : 'RSS'}
                size={12}
                color={item.item.followed ? COLORS.dark_600 : COLORS.green_600}
                style={{
                  marginTop: Platform.OS === 'android' ? 6 : 10,
                  marginLeft: 4,
                }}
              />
            </TouchableOpacity>
          ) : (
            <></>
          )}

          {item.item.userConnectStatus &&
            (item.item.userConnectStatus.connectStatus === 'PENDING_CONNECT' ||
              item.item.userConnectStatus.connectStatus ===
              'REQUEST_RECEIVED') ? (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Icon
                onPress={() =>
                  this.props.navigation.navigate('NetworkInvitationStack')
                }
                name="FollowTick"
                size={12}
                color="#888"
                style={{
                  marginTop: Platform.OS === 'android' ? 12 : 3,
                  marginLeft: 4,
                }}
              />
              <Text
                onPress={() =>
                  this.props.navigation.navigate('NetworkInvitationStack')
                }
                style={[
                  {
                    color: '#888',
                    fontSize: 14,
                    marginLeft: 4,
                    marginTop: 4,
                  },
                  typography.Note2,
                ]}>
                {item.item.userConnectStatus &&
                  (item.item.userConnectStatus.connectStatus ===
                    'PENDING_CONNECT' ||
                    item.item.userConnectStatus.connectStatus ===
                    'REQUEST_RECEIVED')
                  ? 'Pending'
                  : ''}
              </Text>
            </View>
          ) : (
            <></>
          )}
        </TouchableOpacity>
        <Text
          style={[
            {
              color:
                new Date(item.item.startDate).getTime() > new Date().getTime()
                  ? 'rgb(220, 53, 69)'
                  : '#888',
              marginTop: 8,
            },
            typography.Note2,
          ]}>
          {new Date(item.item.startDate).getTime() < new Date().getTime()
            ? 'Published on '
            : 'Scheduled on '}
          <Text style={[{ color: '#888', marginTop: 8 }, typography.Note2]}>
            {moment.unix(item.item.startDate / 1000).format('Do MMM, YYYY')}
          </Text>
        </Text>

        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <TouchableOpacity
            onPress={() =>
              this.handleLike(item.item.id, item.item.liked, item.index)
            }
            activeOpacity={0.5}
            style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon
              name={item.item.liked ? 'Like_FL' : 'Like'}
              color={COLORS.green_500}
              size={14}
              style={{
                marginTop: Platform.OS === 'android' ? 10 : 0,
                marginHorizontal: 6,
              }}
            />
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.9}
            onPress={() =>
              this.handleLike(item.item.id, item.item.liked, item.index)
            }>
            {item.item.likesCount > 1 ? (
              <Text style={[{ color: COLORS.altgreen_300 }, typography.Caption]}>
                {item.item.likesCount} Likes
              </Text>
            ) : (
              <Text style={[{ color: COLORS.altgreen_300 }, typography.Caption]}>
                {item.item.likesCount} Like
              </Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  listHeaderComponent = () => {
    if (this.state.isLoading) {
      return <SkeletonLoader />;
    } else {
      return !this.state.pollData.length &&
        this.state.selectedTab === 'MYPOLL' ? (
        <View>
          <Text
            style={[
              typography.Title_2,
              {
                color: COLORS.dark_800,
                fontSize: 16,
                alignSelf: 'center',
                marginTop: 60,
                textAlign: 'center',
              },
            ]}>
            Polls are great way to understand what the members think of the
            questions you raise.
          </Text>

          <Text
            style={[
              typography.Subtitle_2,
              {
                color: '#698f8a',
                marginTop: 20,
                alignSelf: 'center',
                fontSize: 14,
              },
            ]}>
            Start by{' '}
            <Text
              onPress={() => this.setState({ createPollModalOpen: true })}
              style={[
                typography.Body_1_bold,
                {
                  textDecorationLine: 'underline',
                  color: '#97a600',
                  fontSize: 14,
                },
              ]}>
              creating
            </Text>{' '}
            or{' '}
            <Text
              onPress={() => this.changeState({ selectedTab: '' })}
              style={[
                typography.Body_1_bold,
                {
                  textDecorationLine: 'underline',
                  color: '#97a600',
                  fontSize: 14,
                },
              ]}>
              participating
            </Text>{' '}
            in a poll
          </Text>
        </View>
      ) : !this.state.pollData.length && this.state.selectedTab === 'PINNED' ? (
        <View>
          <Text
            style={[
              typography.Title_2,
              {
                color: COLORS.dark_800,
                fontSize: 16,
                alignSelf: 'center',
                marginTop: 60,
                textAlign: 'center',
              },
            ]}>
            Pining your favourite polls is a great way to revisit them later.
          </Text>

          <Text
            style={[
              typography.Subtitle_2,
              {
                color: '#698f8a',
                marginTop: 20,
                alignSelf: 'center',
                fontSize: 14,
              },
            ]}>
            Pin{' '}
            <Text
              onPress={() => this.changeState({ selectedTab: '' })}
              style={[
                typography.Body_1_bold,
                {
                  textDecorationLine: 'underline',
                  color: '#97a600',
                  fontSize: 14,
                },
              ]}>
              polls
            </Text>{' '}
            you like!
          </Text>
        </View>
      ) : (
        <></>
      );
    }
  };

  unixTimeDatePicker = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    return day + '-' + month + '-' + year;
  };

  currentDate = () => {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = dd + '-' + mm + '-' + yyyy;

    return today;
  };

  verifyMoreThanTwoOptions = () => {
    if (this.state.shareholders.length <= 2) return true;
    else if (this.state.shareholders.length > 2) {
      this.state.shareholders.map((data, index) => {
        if (data.id > 2 && data.name.trim() === '') {
          return false;
          // return false;
        }
        return true;
      });
    }
  };

  handleSubmit = () => {
    if (
      this.state.pollQuestion.trim() !== '' &&
      this.state.hashTags.length > 0 &&
      this.state.shareholders[0].name.trim() !== '' &&
      this.state.shareholders[1].name.trim() !== '' &&
      this.state.startDate !== 0 &&
      this.state.endDate !== 0 &&
      this.state.shareholders.every((item) => item.name.trim() !== '')
    ) {
      let postBody = {
        userId: this.state.userId,
        userName:
          this.state.userData && this.state.userData.firstName
            ? this.state.userData.firstName
            : this.state.userData.companyName,
        question: this.state.pollQuestion,
        hashTags: this.state.hashTags
          ? this.state.hashTags.map((item) => {
            return item.replace(/#/g, '');
          })
          : [],
        description: this.state.writeSomething,
        pollAnswersList: this.state.shareholders,
        status: 'ENABLE',
        startDate: this.state.startDate,
        endDate: this.state.endDate,
        userType: 'WENAT',
        createdBy:
          this.state.userData && this.state.userData.firstName
            ? this.state.userData.firstName
            : this.state.userData.companyName,
      };

      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/backend/public/poll/create',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          console.log(res.status);
          if (res.status === '201 CREATED') {
            Snackbar.show({
              backgroundColor: COLORS.dark_900,
              text: 'Poll created successfully',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
            // this.getPollList(
            //   this.state.userId,
            //   this.state.selectedTab,
            //   this.state.searchParam,
            // );
            setTimeout(() => {
              this.setState(
                {
                  createPollModalOpen: false,
                  pollQuestion: '',
                  writeSomething: '',
                  hashTags: [],
                  startDate: 0,
                  endDate: 0,
                  shareholders: [
                    { name: '', id: '' },
                    { name: '', id: '' },
                  ],
                  publishButton: true,
                  addHashTagsModalOpen: false,
                  pollData: [],
                  page: 0,
                },
                () =>
                  this.getPollList(
                    this.state.userId,
                    this.state.selectedTab,
                    this.state.searchParam,
                  ),
              );
            }, 1000);
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            if (err.response.data.message === 'For input string: ""')
              Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Please enter an option. You may also remove the option.',
                duration: Snackbar.LENGTH_LONG,
              });
            else
              Snackbar.show({
                backgroundColor: '#B22222',
                text: err.response.data.message,
                duration: Snackbar.LENGTH_LONG,
              });
          }
        });
    } else {
      if (this.state.pollQuestion.trim() === '')
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter a valid poll question',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (
        this.state.shareholders[0].name.trim() === '' ||
        this.state.shareholders[1].name.trim() === ''
      )
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter an option',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.startDate === 0)
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter the start date',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.endDate === 0)
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter the end date',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.hashTags.length === 0)
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Add at least 1 hashtag',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (
        this.state.shareholders.length > 2 &&
        this.state.shareholders.every((item) => item.name.trim() !== '') ===
        false
      )
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter an option. You may also remove the option',
          duration: Snackbar.LENGTH_LONG,
        });
    }
  };

  saveAsDraft = () => {
    if (this.state.pollQuestion.trim() !== '') {
      let parameterDetails = {};

      parameterDetails.userType = 'WENAT';
      parameterDetails.createdBy =
        this.state.userData && this.state.userData.firstName
          ? this.state.userData.firstName
          : this.state.userData.companyName;
      parameterDetails.pollQuestion = this.state.pollQuestion;
      if (this.state.writeSomething !== '')
        parameterDetails.writeSomething = this.state.writeSomething;
      parameterDetails.shareholders = this.state.shareholders;
      if (this.state.startDate)
        parameterDetails.startDate = this.state.startDate;
      if (this.state.endDate) parameterDetails.endDate = this.state.endDate;

      if (this.state.hashTags.length > 0)
        parameterDetails.hashTags = this.state.hashTags.map((item) => {
          return item.replace(/#/g, '');
        });

      let postBody = {
        userId: this.state.userId,
        parameterDetails: parameterDetails,
        type: 'POLL',
      };
      if (this.state.draftId) {
        postBody.draftId = this.state.draftId;
      }

      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/backend/drafts/create',
        headers: { 'Content-Type': 'application/json' },
        data: postBody,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          console.log(res.status);
          if (res.status === '201 CREATED') {
            Snackbar.show({
              backgroundColor: COLORS.dark_900,
              text: 'Poll Saved as draft',
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
            clearInterval(this.interval);
            setTimeout(() => {
              this.setState({
                createPollModalOpen: false,
                pollQuestion: '',
                writeSomething: '',
                hashTags: [],
                startDate: 0,
                endDate: 0,
                shareholders: [
                  { name: '', id: '' },
                  { name: '', id: '' },
                ],
                publishButton: true,
                addHashTagsModalOpen: false,
              });
              this.getPollList(
                this.state.userId,
                this.state.selectedTab,
                this.state.searchParam,
              );
            }, 1000);
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            console.log(err.response.data.message);
            Snackbar.show({
              backgroundColor: '#B22222',
              text: err.response.data.message,
              duration: Snackbar.LENGTH_LONG,
            });
          }
        });
    } else {
      Snackbar.show({
        backgroundColor: '#B22222',
        text: 'Please enter Poll Question',
        duration: Snackbar.LENGTH_LONG,
      });
    }
  };

  handleShareholderNameChange = (idx, value) => {
    const newShareholders = this.state.shareholders.map((shareholder, sidx) => {
      if (idx !== sidx) return shareholder;
      return { ...shareholder, name: value, id: idx + 1 };
    });

    this.setState({ shareholders: newShareholders, seconds: 59 });
  };

  handleRemoveShareholder = (idx) => {
    this.setState({
      shareholders: this.state.shareholders.filter((s, sidx) => idx !== sidx),
      seconds: 59,
    });
  };

  handleAddShareholder = () => {
    this.setState({
      shareholders: this.state.shareholders.concat([{ name: '', id: '' }]),
      seconds: 59,
    });
  };

  getMinEndDate = () => {
    var day = new Date(this.state.startDate);

    var nextDay = new Date(day);
    nextDay.setDate(day.getDate() + 1);
    return nextDay;
  }

  changeHashTagsState = (value) => {
    this.setState(value)
  }

  suggestedHashTagsModal = () => {
    return (
      <Modal
        visible={this.state.suggestedHashTagsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <SuggestedHashTags
          changeHashTagsState={this.changeHashTagsState}
          hashTags={this.state.hashTags}
          writeSomething={this.state.writeSomething}
        />
      </Modal>
    )
  }

  createPollModal = () => {
    return (
      <Modal
        onRequestClose={() => this.setState({ createPollModalOpen: false })}
        visible={this.state.createPollModalOpen}
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <SafeAreaView style={{ flex: 1 }}>
          <ScrollView
            style={{ marginTop: '-6%' }}
            keyboardShouldPersistTaps="handled">
            <View
              style={[
                defaultShape.Modal_Categories_Container,
                { backgroundColor: COLORS.white },
              ]}>
              <View
                style={
                  Platform.OS === 'ios'
                    ? [styles.header, { paddingVertical: 12 }]
                    : styles.header
                }>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                  }}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity
                      onPress={() =>
                        // this.state.pollQuestion.trim() === '' ? this.setState({ createPollModalOpen: false }) : this.saveAsDraft()
                        // this.saveAsDraft()
                        this.state.pollQuestion.trim() === '' ?
                        this.setState(
                          {
                            createPollModalOpen: false,
                            pollQuestion: '',
                            writeSomething: '',
                            hashTags: [],
                            startDate: 0,
                            endDate: 0,
                            shareholders: [
                              { name: '', id: '' },
                              { name: '', id: '' },
                            ],
                            publishButton: true,
                            addHashTagsModalOpen: false,
                          },
                          () => clearInterval(this.interval),
                        ) : this.saveAsDraft()
                      }>
                      <Icon
                        name="Cross"
                        size={14}
                        color={COLORS.dark_500}
                        style={{
                          paddingTop: Platform.OS === 'ios' ? 0 : 8,
                          marginRight: 8,
                        }}
                      />
                    </TouchableOpacity>

                    <Image
                      source={
                        this.state.userData && this.state.userData.profileImage
                          ? { uri: this.state.userData.profileImage }
                          : this.state.userData &&
                            this.state.userData.type === 'COMPANY'
                            ? DefaultBusiness
                            : defaultProfile
                      }
                      style={{ width: 30, height: 30, borderRadius: 15 }}
                    />
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Button_Lead,
                        { color: COLORS.dark_800, marginLeft: 8, maxWidth: 180 },
                      ]}>
                      Post as{' '}
                      <Text
                        style={[
                          typography.Button_1,
                          { color: COLORS.dark_800, marginLeft: 8, fontSize: 15 },
                        ]}>
                        {this.state.userData && this.state.userData.firstName
                          ? this.state.userData.firstName
                          : this.state.userData.companyName}
                      </Text>
                    </Text>
                  </View>

                  {this.state.publishButton ? (
                    <View style={{ flexDirection: 'row' }}>
                      <TouchableOpacity
                        onPress={() => this.handleSubmit()}
                        activeOpacity={0.9}
                        style={[
                          defaultShape.ContextBtn_FL_Drk,
                          {
                            paddingLeft: 10,
                            paddingVertical: 16,
                            alignSelf: 'flex-end',
                            marginRight: 0,
                            borderRadius: 0,
                            borderTopLeftRadius: 4,
                            borderBottomLeftRadius: 4,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                          },
                        ]}>
                        <Text
                          style={[
                            typography.Caption,
                            { color: COLORS.altgreen_300 },
                          ]}>
                          PUBLISH
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => this.setState({ publishButton: false })}
                        activeOpacity={0.9}
                        style={[
                          defaultShape.ContextBtn_FL_Drk,
                          {
                            paddingRight: 10,
                            paddingVertical: 16,
                            alignSelf: 'flex-end',
                            marginRight: 0,
                            borderRadius: 0,
                            borderTopRightRadius: 4,
                            borderBottomRightRadius: 4,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginLeft: -10,
                          },
                        ]}>
                        <Icon
                          name="Arrow2_Down"
                          color="#91b3a2"
                          size={16}
                          style={{
                            marginTop: Platform.OS === 'android' ? 9 : 0,
                            marginLeft: 6,
                          }}
                        />
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <View style={{ flexDirection: 'column' }}>
                      <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                          onPress={() => this.handleSubmit()}
                          activeOpacity={0.9}
                          style={[
                            defaultShape.ContextBtn_FL_Drk,
                            {
                              paddingLeft: 10,
                              paddingVertical: 16,
                              alignSelf: 'flex-end',
                              marginRight: 0,
                              borderRadius: 0,
                              borderTopLeftRadius: 4,
                              borderTopRightRadius: 4,
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                            },
                          ]}>
                          <Text
                            style={[
                              typography.Caption,
                              { color: COLORS.altgreen_300 },
                            ]}>
                            PUBLISH
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => this.setState({ publishButton: true })}
                          activeOpacity={0.9}
                          style={[
                            defaultShape.ContextBtn_FL_Drk,
                            {
                              paddingRight: 9.8,
                              paddingVertical: 16,
                              alignSelf: 'flex-end',
                              marginRight: 0,
                              borderRadius: 0,
                              borderTopRightRadius: 4,
                              borderTopRightRadius: 4,
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                              marginLeft: -10,
                            },
                          ]}>
                          <Icon
                            name="Arrow2_Down"
                            color="#91b3a2"
                            size={16}
                            style={{
                              marginTop: Platform.OS === 'android' ? 9 : 0,
                              marginLeft: 6,
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                      <TouchableOpacity
                        onPress={() => this.saveAsDraft()}
                        activeOpacity={0.9}
                        style={[
                          defaultShape.ContextBtn_FL_Drk,
                          {
                            width: 108.09,
                            paddingVertical: 16,
                            alignSelf: 'flex-end',
                            alignItems: 'flex-start',
                            marginRight: 0,
                            borderRadius: 0,
                            borderBottomLeftRadius: 4,
                            borderBottomRightRadius: 4,
                          },
                        ]}>
                        <Text
                          style={[
                            typography.Caption,
                            { color: COLORS.altgreen_300, marginLeft: 1.5 },
                          ]}>
                          DRAFT
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )}
                </View>
              </View>

              <View
                style={{
                  backgroundColor: COLORS.white,
                  alignItems: 'center',
                  width: '100%',
                  paddingBottom: 16,
                }}>
                <View
                  style={{
                    width: '100%',
                    height: 90,
                    backgroundColor: '#00394d',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={[typography.H3, { color: '#dadd21' }]}>
                    CREATE A POLL
                  </Text>
                </View>

                <View style={{}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      paddingLeft: 6,
                      paddingRight: 12,
                      marginTop: 8,
                    }}>
                    <TextInput
                      theme={{
                        colors: {
                          text: COLORS.dark_700,
                          primary: COLORS.altgreen_300,
                          placeholder: COLORS.altgreen_300,
                        },
                      }}
                      label="Add a Poll Question"
                      multiline
                      selectionColor="#C8DB6E"
                      style={[
                        typography.H3,
                        {
                          width: '90%',
                          backgroundColor: COLORS.white,
                          color: COLORS.dark_700,
                          borderRadius: 50,
                        },
                      ]}
                      onChangeText={(value) =>
                        this.setState({ pollQuestion: value, seconds: 59 })
                      }
                      value={this.state.pollQuestion}
                    />
                  </View>

                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      paddingLeft: 6,
                      paddingRight: 12,
                      marginTop: 8,
                    }}>
                    <TextInput
                      theme={{
                        colors: {
                          text: COLORS.dark_700,
                          primary: COLORS.altgreen_300,
                          placeholder: COLORS.altgreen_300,
                        },
                      }}
                      label="Write Poll Description"
                      multiline
                      selectionColor="#C8DB6E"
                      style={[
                        typography.H5,
                        {
                          // minHeight: 100,
                          width: '90%',
                          backgroundColor: COLORS.white,
                          color: COLORS.dark_700,
                        },
                      ]}
                      onChangeText={(value) =>
                        this.setState({
                          writeSomething: value,
                          seconds: 59,
                          suggestedHashTagsModalOpen: value[value.length - 1] === '#' ? true : false,
                          createPollModalOpen: value[value.length - 1] === '#' ? false : true
                        })
                      }
                      value={this.state.writeSomething}
                    />
                  </View>
                </View>

                <Text
                  style={[
                    typography.Title_1,
                    {
                      fontFamily: 'Montserrat-Medium',
                      color: '#698f8a',
                      alignSelf: 'flex-start',
                      marginLeft: 30,
                      fontSize: 17,
                      marginTop: 20,
                    },
                  ]}>
                  Add Options
                </Text>

                {this.state.shareholders.map((item, index) => {
                  return (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        // paddingLeft: 6,
                        paddingRight: 12,
                        marginTop: 6,
                        marginLeft: index <= 1 ? 0 : 30,
                      }}>
                      <TextInput
                        mode="outlined"
                        multiline
                        theme={{
                          colors: {
                            text: COLORS.dark_700,
                            primary: COLORS.altgreen_300,
                            placeholder: COLORS.altgreen_300,
                          },
                        }}
                        label={`${'Option #' + (index + 1)}`}
                        selectionColor="#C8DB6E"
                        style={[
                          typography.Subtitle_1,
                          {
                            width: index <= 1 ? 320 : 320,
                            maxHeight: 120,
                            color: COLORS.dark_700,
                            // paddingRight: 20
                          },
                        ]}
                        onChangeText={(value) =>
                          this.handleShareholderNameChange(index, value)
                        }
                        value={item.name}
                      />

                      {index > 1 ? (
                        <TouchableOpacity
                          onPress={() => this.handleRemoveShareholder(index)}
                          activeOpacity={0.5}
                          style={{
                            alignItems: 'center',
                            flexDirection: 'row',
                            marginLeft: 0,
                            zIndex: 2
                          }}>
                          <Icon
                            name="Delete"
                            color={COLORS.altgreen_300}
                            size={14}
                            style={{
                              marginTop: Platform.OS === 'android' ? 10 : 0,
                              marginHorizontal: 6,
                            }}
                          />
                        </TouchableOpacity>
                      ) : null}
                    </View>
                  );
                })}

                <TouchableOpacity
                  onPress={() => this.handleAddShareholder()}
                  activeOpacity={0.8}
                  style={{
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignSelf: 'flex-end',
                    marginRight: 32,
                  }}>
                  <Icon
                    name="AddList"
                    color={COLORS.altgreen_300}
                    size={14}
                    style={{
                      marginTop: Platform.OS === 'android' ? 12 : 0,
                      marginHorizontal: 6,
                    }}
                  />
                  <Text
                    style={[
                      typography.Title_1,
                      {
                        fontFamily: 'Montserrat-Medium',
                        color: '#698f8a',
                        // alignSelf: 'flex-start',
                        // marginLeft: 30,
                        fontSize: 14,
                        marginTop: 0,
                      },
                    ]}>
                    Add More
                  </Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  backgroundColor: 'white',
                  alignItems: 'center',
                  width: '100%',
                  marginTop: 0,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 10,
                    width: '90%',
                  }}>
                  <TouchableOpacity
                    activeOpacity={0.6}
                    style={{
                      height: 56,
                      width: '40%',
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginTop: 20,
                      paddingLeft: 6,
                      paddingRight: 12,
                    }}>
                    <View style={{ width: '100%' }}>
                      <Text
                        style={[
                          typography.Note2,
                          { color: COLORS.altgreen_300 },
                        ]}>
                        Start Date
                      </Text>
                      <DatePicker
                        style={{ width: '100%', borderWidth: 0 }}
                        date={
                          this.state.startDate === 0
                            ? ''
                            : this.unixTimeDatePicker(this.state.startDate)
                        }
                        mode="date"
                        placeholder="Start date"
                        format="DD-MM-YYYY"
                        minDate={this.currentDate()}
                        maxDate="01-01-2100"
                        // maxDate="16-07-2021"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        // hideText='true'
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: -10,
                            marginRight: 0,
                          },
                          dateInput: {
                            alignItems: 'flex-start',
                            borderWidth: 0,
                          },
                          dateText: {
                            color: COLORS.dark_700,
                            marginTop: -20,
                          },
                        }}
                        onDateChange={(date) => {
                          let tempdate = date.split('-').reverse().join('-');
                          let convertedUnixTime = new Date(tempdate).getTime();
                          this.setState({
                            startDate: convertedUnixTime,
                            seconds: 59,
                            endDate:
                              convertedUnixTime >=
                                new Date(this.state.endDate).getTime()
                                ? 0
                                : this.state.endDate,
                          });
                        }}
                      />
                    </View>
                  </TouchableOpacity>

                  {this.state.startDate ? (
                    <TouchableOpacity
                      activeOpacity={0.6}
                      style={{
                        height: 56,
                        width: '40%',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginTop: 20,
                        paddingLeft: 6,
                        paddingRight: 12,
                      }}>
                      <View style={{ width: '100%' }}>
                        <Text
                          style={[
                            typography.Note2,
                            { color: COLORS.altgreen_300 },
                          ]}>
                          End Date
                        </Text>
                        <DatePicker
                          style={{ width: '100%', borderWidth: 0 }}
                          date={
                            this.state.endDate === 0
                              ? ''
                              : this.unixTimeDatePicker(this.state.endDate)
                          }
                          mode="date"
                          placeholder="End date"
                          format="DD-MM-YYYY"
                          // minDate={this.state.startDate ? new Date(this.state.startDate).getDate() + 1 : this.currentDate()}
                          minDate={this.getMinEndDate()}
                          maxDate="01-01-2100"
                          // maxDate="16-07-2021"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          customStyles={{
                            dateIcon: {
                              position: 'absolute',
                              right: 0,
                              top: -10,
                              marginRight: 0,
                            },
                            dateInput: {
                              alignItems: 'flex-start',
                              borderWidth: 0,
                            },
                            dateText: {
                              color: COLORS.dark_700,
                              marginTop: -20,
                            },
                          }}
                          onDateChange={(date) => {
                            let tempdate = date.split('-').reverse().join('-');
                            let convertedUnixTime = new Date(
                              tempdate,
                            ).getTime();
                            this.setState({
                              endDate: convertedUnixTime,
                              seconds: 59,
                            });
                          }}
                        />
                      </View>
                    </TouchableOpacity>
                  ) : null}
                </View>

                <View
                  style={{
                    backgroundColor: COLORS.white,
                    alignItems: 'flex-start',
                    alignSelf: 'flex-start',
                    paddingVertical: 20,
                    paddingLeft: 30,
                    width: '100%',
                  }}>
                  <FlatList
                    keyboardShouldPersistTaps="handled"
                    scrollEventThrottle={0}
                    ref={(ref) => (this.scrollView = ref)}
                    onContentSizeChange={() => {
                      this.scrollView.scrollToEnd({ animated: false });
                      this.setState({ hashTagModalOpen: true });
                    }}
                    columnWrapperStyle={{
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                    }}
                    numColumns={3}
                    showsVerticalScrollIndicator={false}
                    data={[...this.state.hashTags, 'lastData']}
                    keyExtractor={(item) =>
                      item + Math.floor(Math.random() * 100) + 1
                    }
                    renderItem={({ item, index }) =>
                      index < this.state.hashTags.length ? (
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              hashTags: this.state.hashTags.filter(
                                (value, index2) =>
                                  value + index2 !== item + index,
                              ),
                              seconds: 59,
                            })
                          }
                          activeOpacity={0.6}
                          style={{
                            flexDirection: 'row',
                            backgroundColor: COLORS.altgreen_t50 + '80',
                            paddingHorizontal: 10,
                            marginVertical: 6,
                            height: 28,
                            borderRadius: 17,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginRight: 6,
                          }}>
                          <Text
                            style={[
                              typography.Caption,
                              {
                                color: COLORS.dark_500,
                                marginRight: 4,
                                marginTop: Platform.OS === 'ios' ? -2 : 0,
                              },
                            ]}>
                            {item}
                          </Text>
                          <Icon
                            name="Cross_Rounded"
                            color={COLORS.dark_500}
                            size={15}
                            style={{
                              marginTop: Platform.OS === 'android' ? 10 : 0,
                            }}
                          />
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              addHashTagsModalOpen: true,
                              createPollModalOpen: false,
                              seconds: 59,
                            })
                          }
                          activeOpacity={0.6}
                          style={{
                            flexDirection: 'row',
                            backgroundColor: COLORS.grey_200,
                            width: 137,
                            height: 28,
                            borderRadius: 17,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 7,
                          }}>
                          <Icon
                            name="Hashtag"
                            color={COLORS.dark_500}
                            size={14}
                            style={{
                              marginTop: Platform.OS === 'android' ? 7 : 0,
                            }}
                          />
                          <Text
                            style={[
                              typography.Caption,
                              {
                                color: COLORS.dark_500,
                                marginLeft: 5,
                                marginTop: Platform.OS === 'ios' ? -2 : 0,
                              },
                            ]}>
                            Add Hashtag
                          </Text>
                        </TouchableOpacity>
                      )
                    }
                  />

                  {/* <View style={{ width: '80%', backgroundColor: this.state.hashTags.length ? COLORS.green_300 : COLORS.grey_200, height: 2 }}></View> */}
                </View>
              </View>

              <View style={styles.border2}></View>
              <View
                style={[
                  styles.border,
                  { position: 'absolute', bottom: 80 },
                ]}></View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </Modal>
    );
  };

  handleLoadMore = () => {
    this.setState({ page: this.state.page + 1 }, () =>
      this.getPollList(
        this.state.userId,
        this.state.selectedTab,
        this.state.searchParam,
        true,
      ),
    );
  };

  onRefresh = () => {
    this.setState({ page: 0, pollData: [] }, () =>
      this.getPollList(
        this.state.userId,
        this.state.selectedTab,
        this.state.searchParam,
      ),
    );
  };

  render() {
    // console.log('shareholders', this.verifyMoreThanTwoOptions())
    return (
      <SafeAreaView style={{ flex: 1 }}>
        {/*********** Header starts ***********/}
        {this.createPollModal()}
        {this.shareModal()}
        {this.optionsModal()}
        {this.reasonForReportingModal()}
        {this.likeModal()}
        {this.addHashTagsModal()}
        {this.suggestedHashTagsModal()}

        <View
          style={
            Platform.OS === 'ios'
              ? [styles.header, { paddingVertical: 15 }]
              : styles.header
          }>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableOpacity
              style={{
                width: 40,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="Arrow-Left"
                size={15}
                color="#91B3A2"
                style={Platform.OS === 'ios' ? { marginTop: 0 } : { marginTop: 8 }}
              />
            </TouchableOpacity>
            <Text
              style={[
                typography.H3,
                {
                  color: COLORS.dark_800,
                  fontSize: 18,
                  marginLeft: 10,
                },
              ]}>
              POLLS
            </Text>
          </View>

          <TouchableOpacity
            style={[defaultShape.ContextBtn_FL_Drk]}
            onPress={() =>
              this.setState(
                { createPollModalOpen: true, seconds: 59 },
                // () =>
                // (this.interval = setInterval(
                //   () =>
                //     this.setState((prevState) => ({
                //       seconds: prevState.seconds - 1,
                //     })
                //     ),
                //   1000,
                // )),
              )
            }>
            <Text
              style={[
                typography.Caption,
                { color: COLORS.altgreen_200, paddingVertical: 2 },
              ]}>
              Add POLL
            </Text>
          </TouchableOpacity>
        </View>

        <PollHeader
          changeState={this.changeState}
          selectedTab={this.state.selectedTab}
        />

        {/*********** Header ends ***********/}

        {/*********** Forum ItemList starts ***********/}

        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 20 }}
          data={this.state.pollData}
          ListHeaderComponent={this.listHeaderComponent()}
          keyExtractor={(item) => item.id + Math.floor(Math.random() * 100) + 1}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={1}
          renderItem={(item) => this.renderPollItem(item)}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }
        />

        {/*********** Forum ItemList ends ***********/}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  forumItemView: {
    width: '92%',
    marginTop: 15,
    backgroundColor: COLORS.white,
    borderRadius: 4,
    alignSelf: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
  submitBtn: {
    width: 90,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primarydark,
    borderRadius: 4,
    marginVertical: 10,
  },
  pollItem: {
    maxWidth: '100%',
    minHeight: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f2f3f4',
    borderRadius: 4,
    marginBottom: 10,
    padding: 8,
  },
  votedPollItem: {
    // flexDirection: 'row',
    justifyContent: 'space-between',
    maxWidth: '100%',
    minHeight: 30,
    alignItems: 'center',
    backgroundColor: '#f2f3f4',
    borderRadius: 4,
    marginBottom: 10,
  },
  progressView: {
    maxWidth: '100%',
    minHeight: 30,
    height: '100%',
    borderRadius: 4,
    marginBottom: 10,
    position: 'absolute',
    left: 0,
    zIndex: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    userDataProgress: state.personalProfileReducer.userDataProgress,
    user: state.personalProfileReducer.user,
    error: state.personalProfileReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    personalProfileRequest: (data) => dispatch(personalProfileRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AllPoll);
