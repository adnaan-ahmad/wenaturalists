import React, {Component} from 'react';
import {
  Linking,
  Modal,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  Image,
  Platform,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {AirbnbRating} from 'react-native-ratings';
import {cloneDeep} from 'lodash';
import LinearGradient from 'react-native-linear-gradient';

import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import defaultShape from '../../../Components/Shared/Shape';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultStyle from '../../../Components/Shared/Typography';
import typography from '../../../Components/Shared/Typography';
import defaultProfile from '../../../../assets/defaultProfile.png';
import LikedUserList from '../../../Components/User/Common/LikedUserList';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');
const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
  'i',
); // fragment locator

export default class AllReviews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      uniqueIdOfWeNat: '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed',
      pageNumber: 0,
      pageSize: 10,
      feedbacks: [],
      pressedId: '',
      likeModalOpen: false,
      peopleLiked: [],
      isCompany: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    AsyncStorage.getItem('userData').then((value) => {
      let objValue = JSON.parse(value);
      objValue.type !== 'INDIVIDUAL' && this.setState({isCompany: true});
    });

    AsyncStorage.getItem('userId')
      .then((value) => {
        value && this.setState({userId: value});
        value && this.getAllFeedbacks(value);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  navigation = (value, params) => {
    this.setState({likeModalOpen: false}, () =>
      this.props.navigation.navigate(value, params),
    );
  };

  getAllFeedbacks(userId) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/feedback/getFeedbacksByActivityId/' +
        this.state.uniqueIdOfWeNat +
        '?userId=' +
        userId +
        '&page=' +
        this.state.pageNumber +
        '&size=' +
        this.state.pageSize,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          this.setState({
            feedbacks: this.state.feedbacks.concat(response.data.body.content),
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  handleLoadMore = () => {
    this.setState({pageNumber: this.state.pageNumber + 1}, () =>
      this.getAllFeedbacks(this.state.userId),
    );
  };

  handleLike = (activityId, liked, index) => {
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          let tempFeedbacks = cloneDeep(this.state.feedbacks);
          tempFeedbacks[index].liked = !liked;
          liked === false
            ? (tempFeedbacks[index].likesCount += 1)
            : (tempFeedbacks[index].likesCount -= 1);
          this.setState({feedbacks: tempFeedbacks});
        } else {
          console.log(response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  likeModal = () => {
    return (
      <Modal
        visible={this.state.likeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({likeModalOpen: false, peopleLiked: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who liked this review
              </Text>
            </View>

            <LikedUserList
              id={this.state.pressedId}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  handleFollowUnfollow = (isFollowed, userId) => {
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          // console.log(response.status)
          this.getAllFeedbacks(this.state.userId);
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        // console.log(err)
      });
  };

  renderFeedbackItem = (item) => {
    return (
      <View style={styles.feedbackItemView}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            // marginTop: 10,
          }}>
          <TouchableOpacity
            onPress={() =>
              item.item.userType === 'COMPANY'
                ? this.props.navigation.navigate('ProfileStack', {
                    screen: 'CompanyProfileScreen',
                    params: {userId: item.item.userId},
                  })
                : this.props.navigation.navigate('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: {userId: item.item.userId},
                  })
            }
            style={{flexDirection: 'row', alignItems: 'center'}}
            activeOpacity={0.8}>
            <Image
              source={
                item.item.originalProfileImage
                  ? {uri: item.item.originalProfileImage}
                  : item.item.userType === 'INDIVIDUAL'
                  ? defaultProfile
                  : item.item.userType === 'COMPANY'
                  ? defaultBusiness
                  : null
              }
              style={styles.profileImage}
            />
            <View style={{}}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  numberOfLines={1}
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.dark_600,
                      textTransform: 'capitalize',
                      maxWidth: 180,
                    },
                  ]}>
                  {item.item.userName}
                </Text>

                {(item.item.connectDepth === -1 ||
                  item.item.connectDepth === 0) &&
                item.item.followed &&
                this.state.userId !== item.item.userId &&
                item.item.userConnectStatus.connectStatus !==
                  'PENDING_CONNECT' ? (
                  <Icon
                    onPress={() =>
                      this.handleFollowUnfollow(
                        item.item.followed,
                        item.item.userId,
                      )
                    }
                    name="FollowTick"
                    size={12}
                    color="#888"
                    style={{
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                      marginLeft: 4,
                    }}
                  />
                ) : (
                  <></>
                )}

                <Text
                  onPress={() =>
                    item.item.connectDepth === -1 ||
                    item.item.connectDepth === 0
                      ? this.handleFollowUnfollow(
                          item.item.followed,
                          item.item.userId,
                        )
                      : null
                  }
                  style={[
                    {
                      color:
                        (item.item.connectDepth === -1 ||
                          item.item.connectDepth === 0) &&
                        !item.item.followed
                          ? '#97a600'
                          : '#888',
                      fontSize: 14,
                      marginLeft: 4,
                    },
                    typography.Note2,
                  ]}>
                  {item.item.connectDepth === 1
                    ? '• 1st'
                    : item.item.connectDepth === 2
                    ? '• 2nd'
                    : (item.item.connectDepth === -1 ||
                        item.item.connectDepth === 0) &&
                      item.item.userConnectStatus &&
                      item.item.userConnectStatus.connectStatus ===
                        'PENDING_CONNECT'
                    ? null
                    : (item.item.connectDepth === -1 ||
                        item.item.connectDepth === 0) &&
                      item.item.followed &&
                      this.state.userId !== item.item.userId &&
                      item.item.userConnectStatus.connectStatus !==
                        'PENDING_CONNECT'
                    ? 'Following'
                    : (item.item.connectDepth === -1 ||
                        item.item.connectDepth === 0) &&
                      !item.item.followed &&
                      this.state.userId !== item.item.userId &&
                      item.item.userConnectStatus.connectStatus !==
                        'PENDING_CONNECT'
                    ? 'Follow'
                    : null}
                </Text>

                {item.item.userConnectStatus &&
                item.item.userConnectStatus.connectStatus ===
                  'PENDING_CONNECT' ? (
                  <Icon
                    onPress={() =>
                      this.props.navigation.navigate('NetworkInvitationStack')
                    }
                    name="FollowTick"
                    size={12}
                    color="#888"
                    style={{
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                      marginLeft: 4,
                    }}
                  />
                ) : (
                  <></>
                )}

                <Text
                  onPress={() =>
                    this.props.navigation.navigate('NetworkInvitationStack')
                  }
                  style={[
                    {
                      color:
                        (item.item.connectDepth === -1 ||
                          item.item.connectDepth === 0) &&
                        !item.item.followed
                          ? '#97a600'
                          : '#888',
                      fontSize: 14,
                      marginLeft: 4,
                    },
                    typography.Note2,
                  ]}>
                  {item.item.userConnectStatus &&
                  item.item.userConnectStatus.connectStatus ===
                    'PENDING_CONNECT'
                    ? 'Pending'
                    : null}
                </Text>
              </View>
              <Text style={[typography.Note2, {color: COLORS.altgreen_400}]}>
                {moment
                  .unix(item.item.time / 1000)
                  .format('MMM D, YYYY, h:mm A')}
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={{alignSelf: 'flex-start', marginTop: 6}}>
          <AirbnbRating
            count={5}
            defaultRating={item.item.rating}
            size={20}
            showRating={false}
            selectedColor={COLORS.green_400}
            isDisabled={true}
          />
        </View>

        {regexp.test(item.item.title) ? (
          <Text
            numberOfLines={3}
            onPress={() => this.openWebsite(item.item.title)}
            style={[typography.Title_2, {color: '#4068eb', marginTop: 10}]}>
            {item.item.title}
          </Text>
        ) : (
          <Text
            numberOfLines={3}
            style={[
              typography.Title_2,
              {color: COLORS.dark_800, marginTop: 10},
            ]}>
            {item.item.title}
          </Text>
        )}

        {regexp.test(item.item.description) ? (
          <Text
            numberOfLines={this.state.pressedId === item.item.id ? 200 : 5}
            onPress={() => this.openWebsite(item.item.description)}
            style={[
              typography.Body_2_bold,
              {color: '#4068eb', marginTop: 5, fontSize: 11},
            ]}>
            {item.item.description}
          </Text>
        ) : (
          <Text
            numberOfLines={this.state.pressedId === item.item.id ? 200 : 5}
            style={[typography.Body_2, {color: COLORS.dark_500, marginTop: 5}]}>
            {item.item.description}
          </Text>
        )}

        {item.item.description.split(' ').length > 50 ? (
          <TouchableOpacity
            activeOpacity={0.6}
            style={{height: 30, width: 80}}
            onPress={() =>
              this.setState({
                pressedId:
                  this.state.pressedId === item.item.id ? '' : item.item.id,
              })
            }>
            <Text style={{color: '#4068eb', fontWeight: '700', fontSize: 13}}>
              {this.state.pressedId === item.item.id
                ? 'Read less'
                : 'Read more'}
            </Text>
          </TouchableOpacity>
        ) : null}

        <View
          style={{flexDirection: 'row', alignItems: 'center', marginLeft: -20}}>
          <TouchableOpacity
            onPress={() =>
              this.handleLike(item.item.id, item.item.liked, item.index)
            }
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon
              name={item.item.liked ? 'Like_FL' : 'Like'}
              size={15}
              color={COLORS.green_500}
              style={
                (Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8},
                {marginRight: -16})
              }
            />
          </TouchableOpacity>

          {item.item.likesCount > 1 ? (
            <Text
              onPress={() =>
                this.setState({likeModalOpen: true, pressedId: item.item.id})
              }
              style={[
                typography.Body_2,
                {
                  color: 'rgb(136, 136, 136)',
                  fontWeight: '500',
                  marginTop: Platform.OS === 'ios' ? 0 : -10,
                },
              ]}>
              {item.item.likesCount} Likes
            </Text>
          ) : (
            <Text
              onPress={() =>
                this.setState({likeModalOpen: true, pressedId: item.item.id})
              }
              style={[
                typography.Body_2,
                {
                  color: 'rgb(136, 136, 136)',
                  fontWeight: '500',
                  marginTop: Platform.OS === 'ios' ? 0 : -10,
                },
              ]}>
              {item.item.likesCount} Like
            </Text>
          )}
        </View>
      </View>
    );
  };

  openWebsite = (website) => {
    if (website && website.includes('http')) return Linking.openURL(website);
    else if (website && !website.includes('http'))
      return Linking.openURL('https://' + website);
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.likeModal()}

        <View
          style={
            Platform.OS === 'ios'
              ? [styles.header, {paddingVertical: 20}]
              : styles.header
          }>
          <TouchableOpacity
            style={{
              width: 40,
              height: 30,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => this.props.navigation.goBack()}>
            <Icon
              name="Arrow-Left"
              size={15}
              color="#91B3A2"
              style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
            />
          </TouchableOpacity>
          <Text
            style={[defaultStyle.H3, {color: COLORS.dark_800, fontSize: 22}]}>
            All Reviews
          </Text>
          <TouchableOpacity
            style={{
              width: 40,
              height: 30,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => this.props.navigation.goBack()}>
            <Icon
              name="Arrow-Left"
              size={15}
              color="#FFF"
              style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
            />
          </TouchableOpacity>
        </View>

        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingTop: 10, paddingBottom: 16}}
          data={this.state.feedbacks}
          keyExtractor={(item) => item.id}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={2}
          renderItem={(item) => this.renderFeedbackItem(item)}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  feedbackItemView: {
    width: '90%',
    marginTop: 15,
    backgroundColor: COLORS.white,
    borderRadius: 4,
    alignSelf: 'center',
    paddingHorizontal: 15,
    paddingTop: 10,
    // paddingBottom: 6
  },
  profileImage: {
    height: 30,
    width: 30,
    borderRadius: 15,
    marginRight: 8,
  },
});
