import React, {Component} from 'react';
import {
  Linking,
  Image,
  FlatList,
  Modal,
  ScrollView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  Platform,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {AirbnbRating} from 'react-native-ratings';
import LinearGradient from 'react-native-linear-gradient';
import {TextInput} from 'react-native-paper';
import Snackbar from 'react-native-snackbar';
import {cloneDeep} from 'lodash';

import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {COLORS} from '../../../Components/Shared/Colors';
import defaultStyle from '../../../Components/Shared/Typography';
import defaultProfile from '../../../../assets/defaultProfile.png';
import LikedUserList from '../../../Components/User/Common/LikedUserList';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const moment = require('moment');
const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
  'i',
); // fragment locator

export default class FeedBack extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uniqueIdOfWeNat: this.props.id
        ? this.props.id
        : '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed',
      feedbackGiven: false,
      averageRating: 0,
      averageRatingForLastThirtyDays: 0,
      shareYourFeedBackModalOpen: false,
      title: '',
      feedBack: '',
      myRating: 0,
      userId: '',
      pageNumber: 0,
      pageSize: 500,
      feedbacks: [],
      likeModalOpen: false,
      peopleLiked: [],
      pressedId: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });

    navigation = (value, params) => {
      this.setState({likeModalOpen: false}, () =>
        this.props.navigation.navigate(value, params),
      );
    };

    AsyncStorage.getItem('userId')
      .then((value) => {
        value && this.setState({userId: value});
        value && this.checkConfigFeedback(value);
        value && this.getAllFeedbacks(value);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  likeModal = () => {
    return (
      <Modal
        visible={this.state.likeModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({likeModalOpen: false, peopleLiked: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who liked this review
              </Text>
            </View>
            <LikedUserList
              id={this.state.pressedId}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  checkConfigFeedback(userId) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/feedback/getFeedbacksConfigByActivityId/' +
        this.state.uniqueIdOfWeNat +
        '?userId=' +
        userId,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        if (res.message === 'Success!') {
          this.setState({feedbackGiven: res.body.feedbackGiven});
          this.setState({averageRating: res.body.averageRating});
          this.setState({
            averageRatingForLastThirtyDays:
              res.body.averageRatingForLastThirtyDays,
          });
        }
      })
      .catch((err) => {
        if (err && err.response && err.response.data) {
          console.log('checkConfigFeedback', err.response.data.message);
        }
      });
  }

  ratingCompleted = (rating) => {
    this.setState({myRating: rating});
  };

  getAllFeedbacks(userId) {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/feedback/getFeedbacksByActivityId/' +
        this.state.uniqueIdOfWeNat +
        '?userId=' +
        userId +
        '&page=' +
        this.state.pageNumber +
        '&size=' +
        this.state.pageSize,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.body &&
          response.status === 200
        ) {
          this.setState({feedbacks: response.data.body.content});
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  handleSubmit = () => {
    if (this.state.title && this.state.feedBack && this.state.myRating) {
      let body = {
        userId: this.state.userId,
        entityId: this.state.uniqueIdOfWeNat,
        title: this.state.title,
        description: this.state.feedBack,
        rating: this.state.myRating,
      };
      axios({
        method: 'post',
        url: REACT_APP_userServiceURL + '/backend/feedback/create/',
        headers: {'Content-Type': 'application/json'},
        data: body,
        withCredentials: true,
      })
        .then((response) => {
          let res = response.data;
          if (res.status === '201 CREATED') {
            Snackbar.show({
              backgroundColor: COLORS.primarydark,
              text: 'Feedback submitted successfully',
              duration: Snackbar.LENGTH_LONG,
              textColor: COLORS.altgreen_100,
            });
            this.getAllFeedbacks(this.state.userId);
            this.checkConfigFeedback(this.state.userId);
            setTimeout(() => {
              this.setState({
                title: '',
                feedBack: '',
                myRating: 0,
                shareYourFeedBackModalOpen: false,
              });
            }, 2000);
          }
        })
        .catch((err) => {
          if (
            err &&
            err.response &&
            err.response.data &&
            err.response.data.message
          ) {
            Snackbar.show({
              backgroundColor: '#B22222',
              text: 'You have already provided a feedback',
              duration: Snackbar.LENGTH_LONG,
            });
            setTimeout(() => {
              this.setState({
                title: '',
                feedBack: '',
                myRating: 0,
                shareYourFeedBackModalOpen: false,
              });
            }, 2000);
          }
        });
    } else {
      if (this.state.myRating === 0)
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter a valid feedback',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.title === '')
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please enter a valid title',
          duration: Snackbar.LENGTH_LONG,
        });
      else if (this.state.feedBack === '')
        Snackbar.show({
          backgroundColor: '#B22222',
          text: 'Please provide a valid description',
          duration: Snackbar.LENGTH_LONG,
        });
    }
  };

  shareYourFeedBackModal = () => {
    return (
      <Modal
        visible={this.state.shareYourFeedBackModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={styles.linearGradientView}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={styles.linearGradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({
                title: '',
                feedBack: '',
                myRating: 0,
                shareYourFeedBackModalOpen: false,
              })
            }>
            <Icon
              name="Cross"
              size={13}
              color="#367681"
              style={{marginTop: Platform.OS === 'android' ? 6 : 0}}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {height: 400, backgroundColor: COLORS.bgfill},
            ]}>
            <View style={styles.modalHeaderView}>
              <Text
                style={[
                  defaultStyle.Button_2,
                  {
                    color: COLORS.dark_900,
                    textAlign: 'center',
                    textTransform: 'uppercase',
                  },
                ]}>
                Share Your Feedback
              </Text>
            </View>

            <View
              style={{marginTop: 60, alignSelf: 'flex-start', marginLeft: 26}}>
              <AirbnbRating
                count={5}
                defaultRating={0}
                size={24}
                showRating={false}
                selectedColor={COLORS.green_400}
                onFinishRating={this.ratingCompleted}
              />
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 16,
                paddingLeft: 6,
                paddingRight: 12,
              }}>
              <TextInput
                theme={{
                  colors: {
                    text: COLORS.dark_700,
                    primary: COLORS.altgreen_300,
                    placeholder: COLORS.altgreen_300,
                  },
                }}
                label="Title"
                // mode='outlined'
                selectionColor="#C8DB6E"
                style={[
                  defaultStyle.Subtitle_1,
                  {
                    width: '90%',
                    height: 56,
                    backgroundColor: COLORS.bgfill,
                    color: COLORS.dark_700,
                  },
                ]}
                onChangeText={(value) => this.setState({title: value})}
                value={this.state.title}
              />
            </View>

            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 16,
                paddingLeft: 6,
                paddingRight: 12,
              }}>
              <TextInput
                theme={{
                  colors: {
                    text: COLORS.dark_700,
                    primary: COLORS.altgreen_300,
                    placeholder: COLORS.altgreen_300,
                  },
                }}
                label="Feedback"
                // mode='outlined'
                multiline
                selectionColor="#C8DB6E"
                style={[
                  defaultStyle.Subtitle_1,
                  {
                    width: '93%',
                    backgroundColor: COLORS.bgfill,
                    color: COLORS.dark_700,
                    marginLeft: 11,
                    paddingBottom: 13,
                  },
                ]}
                onChangeText={(value) => this.setState({feedBack: value})}
                value={this.state.feedBack}
              />
            </ScrollView>

            {/* <View style={styles.border2}></View> */}

            <TouchableOpacity
              onPress={() => this.handleSubmit()}
              activeOpacity={0.7}
              style={[styles.updateButton, {marginTop: 60}]}>
              <Text
                style={[defaultStyle.Caption, {color: COLORS.altgreen_100}]}>
                SUBMIT
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleFollowUnfollow = (isFollowed, userId) => {
    let url;
    if (!isFollowed) {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/follows/' +
        userId;
    } else {
      url =
        REACT_APP_userServiceURL +
        '/graph/users/' +
        this.state.userId +
        '/unfollows/' +
        userId;
    }
    axios({
      method: 'post',
      url: url,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.status === 202) {
          // console.log(response.status)
          this.getAllFeedbacks(this.state.userId);
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        //   console.log(err)
      });
  };

  renderFeedbackItem = (item) => {
    return (
      <View style={styles.feedbackItemView}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            // marginTop: 10,
          }}>
          <TouchableOpacity
            onPress={() => {
              item.item.userType === 'INDIVIDUAL' &&
              item.item.userId === this.state.userId
                ? this.props.id
                  ? this.props.navigation('ProfileStack')
                  : this.props.navigation.navigate('ProfileStack')
                : item.item.userType === 'INDIVIDUAL' &&
                  item.item.userId !== this.state.userId
                ? this.props.id
                  ? this.props.navigation('ProfileStack', {
                      screen: 'OtherProfileScreen',
                      params: {userId: item.item.userId},
                    })
                  : this.props.navigation.navigate('ProfileStack', {
                      screen: 'OtherProfileScreen',
                      params: {userId: item.item.userId},
                    })
                : item.item.userType === 'COMPANY'
                ? this.props.id
                  ? this.props.navigation('ProfileStack', {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.item.userId},
                    })
                  : this.props.navigation.navigate('ProfileStack', {
                      screen: 'CompanyProfileScreen',
                      params: {userId: item.item.userId},
                    })
                : this.props.id
                ? this.props.navigation('CircleProfileStack', {
                    screen: 'CircleProfile',
                    params: {slug: item.item.customUrl},
                  })
                : this.props.navigation.navigate('CircleProfileStack', {
                    screen: 'CircleProfile',
                    params: {slug: item.item.customUrl},
                  });
            }}
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}
            activeOpacity={0.8}>
            <Image
              source={
                item.item.originalProfileImage
                  ? {uri: item.item.originalProfileImage}
                  : item.item.userType === 'INDIVIDUAL'
                  ? defaultProfile
                  : item.item.userType === 'COMPANY'
                  ? defaultBusiness
                  : null
              }
              style={styles.profileImage}
            />

            <View style={{}}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  numberOfLines={1}
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.dark_600,
                      textTransform: 'capitalize',
                      maxWidth: 100,
                    },
                  ]}>
                  {item.item.userName}
                  {/* This is a very big username which causes trouble */}
                </Text>

                {(item.item.connectDepth === -1 ||
                  item.item.connectDepth === 0) &&
                item.item.followed &&
                this.state.userId !== item.item.userId &&
                item.item.userConnectStatus.connectStatus !==
                  'PENDING_CONNECT' ? (
                  <Icon
                    onPress={() =>
                      this.handleFollowUnfollow(
                        item.item.followed,
                        item.item.userId,
                      )
                    }
                    name="FollowTick"
                    size={12}
                    color="#888"
                    style={{
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                      marginLeft: 4,
                    }}
                  />
                ) : (
                  <></>
                )}

                <Text
                  onPress={() =>
                    item.item.connectDepth === -1 ||
                    item.item.connectDepth === 0
                      ? this.handleFollowUnfollow(
                          item.item.followed,
                          item.item.userId,
                        )
                      : null
                  }
                  style={[
                    {
                      color:
                        (item.item.connectDepth === -1 ||
                          item.item.connectDepth === 0) &&
                        !item.item.followed
                          ? '#97a600'
                          : '#888',
                      fontSize: 14,
                      marginLeft: 4,
                    },
                    typography.Note2,
                  ]}>
                  {item.item.connectDepth === 1
                    ? '• 1st'
                    : item.item.connectDepth === 2
                    ? '• 2nd'
                    : (item.item.connectDepth === -1 ||
                        item.item.connectDepth === 0) &&
                      item.item.userConnectStatus &&
                      item.item.userConnectStatus.connectStatus ===
                        'PENDING_CONNECT'
                    ? null
                    : (item.item.connectDepth === -1 ||
                        item.item.connectDepth === 0) &&
                      item.item.followed &&
                      this.state.userId !== item.item.userId &&
                      item.item.userConnectStatus.connectStatus !==
                        'PENDING_CONNECT'
                    ? 'Following'
                    : (item.item.connectDepth === -1 ||
                        item.item.connectDepth === 0) &&
                      !item.item.followed &&
                      this.state.userId !== item.item.userId &&
                      item.item.userConnectStatus.connectStatus !==
                        'PENDING_CONNECT'
                    ? 'Follow'
                    : null}
                </Text>

                {item.item.userConnectStatus &&
                item.item.userConnectStatus.connectStatus ===
                  'PENDING_CONNECT' ? (
                  <Icon
                    onPress={() =>
                      this.props.navigation.navigate('NetworkInvitationStack')
                    }
                    name="FollowTick"
                    size={12}
                    color="#888"
                    style={{
                      marginTop: Platform.OS === 'android' ? 8 : 0,
                      marginLeft: 4,
                    }}
                  />
                ) : (
                  <></>
                )}

                <Text
                  onPress={() =>
                    this.props.navigation.navigate('NetworkInvitationStack')
                  }
                  style={[
                    {
                      color:
                        (item.item.connectDepth === -1 ||
                          item.item.connectDepth === 0) &&
                        !item.item.followed
                          ? '#97a600'
                          : '#888',
                      fontSize: 14,
                      marginLeft: 4,
                    },
                    typography.Note2,
                  ]}>
                  {item.item.userConnectStatus &&
                  item.item.userConnectStatus.connectStatus ===
                    'PENDING_CONNECT'
                    ? 'Pending'
                    : null}
                </Text>
              </View>
              <Text style={[typography.Note2, {color: COLORS.altgreen_400}]}>
                {moment
                  .unix(item.item.time / 1000)
                  .format('MMM D, YYYY, h:mm A')}
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={{alignSelf: 'flex-start', marginTop: 6}}>
          <AirbnbRating
            count={5}
            defaultRating={item.item.rating}
            size={20}
            showRating={false}
            selectedColor={COLORS.green_400}
            isDisabled={true}
          />
        </View>

        {regexp.test(item.item.title) ? (
          <Text
            numberOfLines={3}
            onPress={() => this.openWebsite(item.item.title)}
            style={[
              typography.Title_2,
              {color: '#4068eb', marginTop: 10, marginLeft: 8},
            ]}>
            {item.item.title}
          </Text>
        ) : (
          <Text
            numberOfLines={1}
            style={[
              typography.Title_2,
              {color: COLORS.dark_800, marginTop: 10, marginLeft: 8},
            ]}>
            {item.item.title}
          </Text>
        )}

        {regexp.test(item.item.description) ? (
          <Text
            numberOfLines={this.state.pressedId === item.item.id ? 200 : 5}
            onPress={() => this.openWebsite(item.item.description)}
            style={[
              typography.Body_2_bold,
              {color: '#4068eb', marginTop: 5, fontSize: 11, marginLeft: 8},
            ]}>
            {item.item.description}
          </Text>
        ) : (
          <Text
            numberOfLines={1}
            style={[
              typography.Body_2,
              {color: COLORS.dark_500, marginTop: 5, marginLeft: 8},
            ]}>
            {item.item.description}
          </Text>
        )}

        <TouchableOpacity
          onPress={() =>
            this.handleLike(item.item.id, item.item.liked, item.index)
          }
          style={{flexDirection: 'row', alignItems: 'center', marginLeft: -16}}>
          <View style={defaultShape.Nav_Gylph_Btn}>
            <Icon
              name={item.item.liked ? 'Like_FL' : 'Like'}
              size={15}
              color={COLORS.green_500}
              style={
                (Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8},
                {marginRight: -16})
              }
            />
          </View>

          {item.item.likesCount > 1 ? (
            <Text
              onPress={() =>
                this.setState({likeModalOpen: true, pressedId: item.item.id})
              }
              style={[
                typography.Body_2,
                {
                  color: 'rgb(136, 136, 136)',
                  fontWeight: '500',
                  marginTop: Platform.OS === 'ios' ? 0 : -10,
                },
              ]}>
              {item.item.likesCount} Likes
            </Text>
          ) : (
            <Text
              onPress={() =>
                this.setState({likeModalOpen: true, pressedId: item.item.id})
              }
              style={[
                typography.Body_2,
                {
                  color: 'rgb(136, 136, 136)',
                  fontWeight: '500',
                  marginTop: Platform.OS === 'ios' ? 0 : -10,
                },
              ]}>
              {item.item.likesCount} Like
            </Text>
          )}
        </TouchableOpacity>
      </View>
    );
  };

  listFooterComponent = () => {
    return this.state.feedbacks.length > 5 && !this.props.id ? (
      <TouchableOpacity
        onPress={() => this.props.navigation.navigate('AllReviews')}
        style={[
          defaultShape.ContextBtn_OL,
          {
            alignSelf: 'center',
            marginTop: 16,
            marginLeft: 10,
            paddingVertical: 3,
            paddingHorizontal: 10,
          },
        ]}>
        <Text style={[typography.Caption, {color: COLORS.dark_500}]}>
          See All
        </Text>
      </TouchableOpacity>
    ) : (
      <></>
    );
  };

  handleLike = (activityId, liked, index) => {
    const data = {
      userId: this.state.userId,
      activityId: activityId,
      liked: !liked,
    };

    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/like/create/',
      data: data,
      headers: {'Content-Type': 'application/json'},
      withCredentials: true,
    })
      .then((response) => {
        if (response && response.data && response.data.message === 'Success!') {
          let tempFeedbacks = cloneDeep(this.state.feedbacks);
          tempFeedbacks[index].liked = !liked;
          liked === false
            ? (tempFeedbacks[index].likesCount += 1)
            : (tempFeedbacks[index].likesCount -= 1);
          this.setState({feedbacks: tempFeedbacks});
        } else {
          console.log(response.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  openWebsite = (website) => {
    if (website && website.includes('http')) return Linking.openURL(website);
    else if (website && !website.includes('http'))
      return Linking.openURL('https://' + website);
  };

  render() {
    console.log(
      'peopleLiked',
      this.state.peopleLiked && this.state.peopleLiked.length >= 2
        ? this.state.peopleLiked[1]
        : null,
    );
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.shareYourFeedBackModal()}
        {this.likeModal()}

        {!this.props.id && (
          <View
            style={
              Platform.OS === 'ios'
                ? [styles.header, {paddingVertical: 20}]
                : styles.header
            }>
            <TouchableOpacity
              style={{
                width: 40,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="Arrow-Left"
                size={15}
                color="#91B3A2"
                style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
              />
            </TouchableOpacity>
            <Text
              style={[defaultStyle.H3, {color: COLORS.dark_800, fontSize: 22}]}>
              Feedback
            </Text>
            <TouchableOpacity
              style={{
                width: 40,
                height: 30,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="Arrow-Left"
                size={15}
                color="#FFF"
                style={Platform.OS === 'ios' ? {marginTop: 0} : {marginTop: 8}}
              />
            </TouchableOpacity>
          </View>
        )}

        {!this.state.feedbackGiven && (
          <TouchableOpacity
            onPress={() => this.setState({shareYourFeedBackModalOpen: true})}
            activeOpacity={0.5}
            style={[styles.mycircle, {backgroundColor: '#D9E1E4'}]}>
            <View
              style={{
                marginLeft: 15,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Icon
                name="Mail_OL"
                color={COLORS.dark_600}
                size={18}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 10}}
              />
              <Text
                style={[
                  defaultStyle.Button_Lead,
                  {color: COLORS.dark_600, marginLeft: 10},
                ]}>
                Share Your Feedback
              </Text>
            </View>
            <View
              style={{
                marginRight: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Icon
                name="Arrow_Right"
                color={COLORS.dark_700}
                size={14}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 7}}
              />
            </View>
          </TouchableOpacity>
        )}

        {this.state.feedbacks.length > 0 && (
          <View style={{marginTop: 26}}>
            <Text
              style={[
                defaultStyle.H3,
                {color: COLORS.dark_800, fontSize: 18, marginLeft: 20},
              ]}>
              Recent Reviews
            </Text>

            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                paddingTop: 0,
                paddingBottom: 16,
                paddingHorizontal: 10,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              data={this.state.feedbacks.slice(0, 5)}
              keyExtractor={(item) => item.id}
              renderItem={(item) => this.renderFeedbackItem(item)}
              ListFooterComponent={this.listFooterComponent()}
            />
          </View>
        )}

        <View style={{alignSelf: 'center', marginTop: 26}}>
          <AirbnbRating
            count={5}
            defaultRating={this.state.averageRating}
            size={36}
            showRating={false}
            isDisabled={true}
            selectedColor={COLORS.green_400}
          />
          <Text
            style={[
              defaultStyle.Button_Lead,
              {
                color: '#698f8a',
                textAlign: 'center',
                fontSize: 18,
                marginTop: 6,
                marginBottom: 16,
              },
            ]}>
            Average Rating
          </Text>

          <AirbnbRating
            count={5}
            defaultRating={this.state.averageRatingForLastThirtyDays}
            size={28}
            showRating={false}
            isDisabled={true}
            selectedColor={COLORS.green_400}
          />

          <Text
            style={[
              defaultStyle.Button_Lead,
              {color: '#698f8a', textAlign: 'center', fontSize: 14},
            ]}>
            Average Rating in previous 30 days
          </Text>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  mycircle: {
    width: '92%',
    height: 44,
    backgroundColor: COLORS.dark_700,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginTop: 20,
    alignItems: 'center',
    borderRadius: 8,
  },
  linearGradientView: {
    width: '100%',
    height: 700,
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  },
  modalHeaderView: {
    zIndex: 2,
    width: '100%',
    height: 58,
    backgroundColor: COLORS.altgreen_100,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  updateButton: {
    width: 86,
    height: 28,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: COLORS.dark_600,
  },
  border2: {
    width: '100%',
    alignSelf: 'center',
    backgroundColor: COLORS.bgfill,
    height: 10,
    zIndex: 2,
    marginTop: -6,
  },
  feedbackItemView: {
    width: 260,
    marginTop: 15,
    backgroundColor: COLORS.white,
    borderRadius: 4,
    alignSelf: 'center',
    paddingHorizontal: 15,
    // paddingTop: 3,
    // paddingBottom: 10,
    marginHorizontal: 8,
  },
  profileImage: {
    height: 30,
    width: 30,
    borderRadius: 15,
    marginRight: 8,
  },
});
