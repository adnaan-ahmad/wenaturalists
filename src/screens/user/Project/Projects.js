import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
  Modal,
  ScrollView,
  Linking,
  Share,
  Alert,
  TextInput,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import {cloneDeep} from 'lodash';
import Snackbar from 'react-native-snackbar';
import Clipboard from '@react-native-community/clipboard';

import SearchBar from '../../../Components/User/SearchBar';
import {COLORS} from '../../../Components/Shared/Colors';
import {nFormatter} from '../../../Components/Shared/commonFunction';
import Report from '../../../Components/User/Common/Report';
import SharedUserList from '../../../Components/User/Common/SharedUserList';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import projectDefault from '../../../../assets/project-default.jpg';
import defaultProfile from '../../../../assets/defaultProfile.png';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import ProjectView from '../../../Components/User/Common/ProjectView';

const moment = require('moment');
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      filter: 'Opportunities',
      page: 0,
      projects: [],
      projDetails: {},
      filterModalOpen: false,
      projectDetailsModalOpen: false,
      addProjModalOpen: false,
      userType: '',

      threeDotModalOpen: false,
      reasonForReportingModalOpen: false,
      shareModalOpen: false,
      peopleSharedModalOpen: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });
    AsyncStorage.getItem('userId').then((value) => {
      value && this.setState({userId: value});
      value && this.getProjectList(value, 'opportunity');
      value && this.getUserType(value);

      this._unsubscribe = this.props.navigation.addListener('focus', () => {
        this.setState({projects: [], page: 0}, () => {
          this.getProjectList(value, 'opportunity');
          this.getUserType(value);
        });
      });
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  changeState = (value) => {
    this.setState(value);
  };

  navigateNotification = () => {
    this.props.navigation.navigate('Notification');
  };

  navigateProfile = () => {
    if (this.state.userType === 'INDIVIDUAL') {
      this.props.navigation.navigate('ProfileStack');
    } else if (this.state.userType === 'COMPANY') {
      this.props.navigation.navigate('ProfileStack', {
        screen: 'CompanyProfileScreen',
      });
    }
  };

  navigation = (value, params) => {
    this.setState(
      {projectDetailsModalOpen: false, peopleSharedModalOpen: false},
      () => this.props.navigation.navigate(value, params),
    );
  };

  changeFun = () => {
    this.setState(
      {
        projectDetailsModalOpen: false,
        projects: [],
        page: 0,
      },
      () => {
        this.getProjectList(this.state.userId, 'opportunity');
      },
    );
  };

  getUserType = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/profile/get?id=' +
        userId +
        '&otherUserId=' +
        '',
      cache: true,
      withCredentials: true,
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        this.setState({userType: response.data.body.type});
        // console.log('--- response.data.body ---', response.data.body)
      })
      .catch((err) => {
        // console.log("Profile data error : ", err)
      });
  };

  getProjectList = (userId, filter) => {
    axios({
      method: 'get',
      url:
        filter === 'opportunity'
          ? REACT_APP_userServiceURL +
            '/project/filter/trending-' +
            filter +
            '/' +
            userId +
            '/INDIVIDUAL?page=' +
            this.state.page +
            '&size=10'
          : REACT_APP_userServiceURL +
            '/project/filter/trending-' +
            filter +
            '/' +
            userId +
            '?page=' +
            this.state.page +
            '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          projects: this.state.projects.concat(response.data.body.content),
        });
      })
      .catch((err) => console.log(err));
  };

  handleLoadMore = () => {
    let tempFilter = this.state.filter;
    if (tempFilter === 'Opportunities') {
      tempFilter = 'opportunity';
    } else if (tempFilter === 'Jobs') {
      tempFilter = 'jobs';
    } else if (tempFilter === 'Assignments') {
      tempFilter = 'assignments';
    } else if (tempFilter === 'Events') {
      tempFilter = 'events';
    } else if (tempFilter === 'Training') {
      tempFilter = 'trainings';
    } else if (tempFilter === 'Expedition') {
      tempFilter = 'expeditions';
    } else if (tempFilter === 'Fund Raise') {
      tempFilter = 'fundraise';
    }
    this.setState({page: this.state.page + 1}, () =>
      this.getProjectList(this.state.userId, tempFilter),
    );
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + 's';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + 'm ago';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + 'h ago';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + 'd ago';
    }

    if (difference >= 864000) {
      return 'on ' + day + ' ' + month + ' ' + year;
    }
  };

  getProjectDetails = (projectId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/find-project-description-details-by-userId-and-projectId/' +
        this.state.userId +
        '/' +
        projectId,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          projectDetailsModalOpen: true,
          projDetails: response.data.body,
        });
      })
      .catch((err) => console.log(err));
  };

  applyInProject = (projectId) => {
    let postBody = {
      participantUserId: this.state.userId,
      participantionPartyType: 'INDIVIDUAL_OR_RECRUITER',
      projectId: projectId,
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/backend/event/apply-in-project?isVolunteerProject=false',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let tempProjects = cloneDeep(this.state.projects);
        let index = tempProjects.findIndex((x) => x.projectId === projectId);
        tempProjects.splice(index, 1);
        this.setState({
          ...this.state,
          projDetails: {...this.state.projDetails, canApply: false},
          projects: tempProjects,
        });
      })
      .catch((err) => console.log(err));
  };

  flatlistHeader = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
          marginTop: 22,
          paddingBottom: 10,
          paddingHorizontal: 15,
          alignItems: 'center',
        }}>
        <Text
          style={[
            typography.Title_2,
            {color: COLORS.altgreen_400, marginRight: 40},
          ]}>
          Trending {this.state.filter}
        </Text>
        <TouchableOpacity
          onPress={() => this.setState({filterModalOpen: true})}
          style={[styles.addProj, {backgroundColor: COLORS.NoColor}]}>
          <Icon name="FilterSlide" color={COLORS.altgreen_400} size={18} />
        </TouchableOpacity>
      </View>
    );
  };

  renderProjectItem = (item) => {
    return (
      <View style={styles.projItem}>
        {item.item.project.type === 'ASSIGNMENT' ||
        item.item.project.type === 'JOB' ? (
          <View style={{marginHorizontal: 15, marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={
                  item.item.project.coverImage
                    ? {uri: item.item.project.coverImage}
                    : projectDefault
                }
                style={{height: 60, width: 60, borderRadius: 30}}
              />
              <View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_800, marginLeft: 10},
                  ]}>
                  {item.item.creatorName}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    marginLeft: 10,
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Location"
                    color={COLORS.altgreen_300}
                    size={12}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                  <Text
                    style={[
                      typography.Note2,
                      {color: COLORS.altgreen_300, marginLeft: 5},
                    ]}>
                    {item.item.project.location.country}
                  </Text>
                </View>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 10,
              }}>
              {item.item.project?.cost > 0 ? (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Wallet"
                    size={12}
                    color={COLORS.grey_400}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 6}}
                  />
                  <Text
                    style={[
                      typography.Note2,
                      {color: COLORS.dark_800, marginLeft: 6},
                    ]}>
                    {item.item?.project?.currency +
                      ' ' +
                      nFormatter(item.item?.project?.cost, 1)}
                  </Text>
                </View>
              ) : (
                <Text></Text>
              )}

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {item.item.participationCount > 0 && (
                  <Icon
                    name="AllGender"
                    color={COLORS.altgreen_300}
                    size={12}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                )}
                <Text
                  style={[
                    typography.Note2,
                    {color: COLORS.altgreen_300, marginLeft: 5},
                  ]}>
                  {item.item.participationCount === 0
                    ? ''
                    : item.item.participationCount}{' '}
                  {item.item.participationCount > 1
                    ? 'Applicants'
                    : item.item.participationCount === 0
                    ? ''
                    : 'Applicant'}
                </Text>
              </View>
            </View>
          </View>
        ) : (
          <TouchableOpacity
            onPress={() => this.getProjectDetails(item.item.projectId)}>
            <Image
              source={
                item.item.project.coverImage
                  ? {uri: item.item.project.coverImage}
                  : projectDefault
              }
              style={styles.projCoverImg}
            />

            {item.item.project?.cost > 0 && (
              <View
                style={{
                  flexDirection: 'row',
                  bottom: 6,
                  right: 6,
                  position: 'absolute',
                  backgroundColor: COLORS.white,
                  paddingHorizontal: 8,
                  paddingVertical: Platform.OS === 'ios' ? 5 : 0,
                  borderRadius: 4,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Icon
                  name="Wallet"
                  size={12}
                  color={COLORS.grey_400}
                  style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                />
                <Text
                  style={[
                    typography.Note2,
                    {color: COLORS.dark_800, marginLeft: 6},
                  ]}>
                  {item.item?.project?.currency +
                    ' ' +
                    nFormatter(item.item?.project?.cost, 1)}
                </Text>
              </View>
            )}
          </TouchableOpacity>
        )}
        <TouchableOpacity
          onPress={() => this.getProjectDetails(item.item.projectId)}>
          <Text
            style={[
              typography.Button_Lead,
              {color: COLORS.dark_800, marginTop: 10, marginLeft: 10},
            ]}>
            {item.item.project.title}
          </Text>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            marginHorizontal: 10,
            paddingBottom: 5,
            width: '85%',
          }}>
          {item.item.project.type === 'ASSIGNMENT' ||
          item.item.project.type === 'JOB' ? null : (
            <View
              style={{
                width: 40,
                alignItems: 'center',
                justifyContent: 'center',
                paddingRight: 5,
                borderRightColor: COLORS.altgreen_400,
                borderRightWidth: 2,
              }}>
              <Text
                style={[
                  typography.H6,
                  {color: COLORS.dark_600, marginLeft: 5},
                ]}>
                {moment
                  .unix(item.item.project.beginningTime / 1000)
                  .format('DD')}
              </Text>
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.altgreen_400, marginLeft: 5},
                ]}>
                {moment
                  .unix(item.item.project.beginningTime / 1000)
                  .format('MMM')
                  .toUpperCase()}
              </Text>
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.altgreen_400, marginLeft: 5},
                ]}>
                {moment
                  .unix(item.item.project.beginningTime / 1000)
                  .format('YYYY')}
              </Text>
            </View>
          )}
          <Text
            numberOfLines={3}
            style={[
              typography.Caption,
              {color: COLORS.dark_600, marginLeft: 5},
            ]}>
            {item.item.project.shortBrief}
          </Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 6,
            paddingRight: 15,
            paddingVertical: Platform.OS === 'ios' ? 8 : 0,
            borderTopColor: COLORS.grey_300,
            borderTopWidth: 0.6,
            backgroundColor:
              ((item.item.openings < 5 && item.item.openings > 0) ||
                (Math.floor(
                  Math.abs(
                    (item.item.project.lastTimeOfApplication - Date.now()) /
                      1000,
                  ) / 86400,
                ) < 5 &&
                  Math.floor(
                    Math.abs(
                      (item.item.project.lastTimeOfApplication - Date.now()) /
                        1000,
                    ) / 86400,
                  ) > 0)) &&
              (item.item.project.type === 'ASSIGNMENT' ||
                item.item.project.type === 'JOB')
                ? COLORS.dark_700
                : COLORS.white,
          }}>
          {item.item.project.type === 'ASSIGNMENT' ||
          item.item.project.type === 'JOB' ? (
            item.item.openings < 5 && item.item.openings > 0 ? (
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.green_500, marginLeft: 15},
                ]}>
                {item.item.openings +
                  ` ${item.item.openings > 1 ? 'OPENINGS' : 'OPENING'} LEFT`}
              </Text>
            ) : Math.floor(
                Math.abs(
                  (item.item.project.lastTimeOfApplication - Date.now()) / 1000,
                ) / 86400,
              ) < 5 &&
              Math.floor(
                Math.abs(
                  (item.item.project.lastTimeOfApplication - Date.now()) / 1000,
                ) / 86400,
              ) > 0 ? (
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.green_500, marginLeft: 15},
                ]}>
                {Math.floor(
                  Math.abs(
                    (item.item.project.lastTimeOfApplication - Date.now()) /
                      1000,
                  ) / 86400,
                ) +
                  ` ${
                    Math.floor(
                      Math.abs(
                        (item.item.project.lastTimeOfApplication - Date.now()) /
                          1000,
                      ) / 86400,
                    ) > 1
                      ? 'DAYS'
                      : 'DAY'
                  } LEFT TO APPLY`}
              </Text>
            ) : (
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.altgreen_300, marginLeft: 15},
                ]}>
                Posted {this.unixTime2(item.item.project.createTime)}
              </Text>
            )
          ) : (
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 15,
                alignItems: 'center',
              }}>
              <Icon
                name="Location"
                color={COLORS.altgreen_300}
                size={12}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.altgreen_300, marginLeft: 5},
                ]}>
                {item.item.project &&
                  item.item.project.location &&
                  item.item.project.location.country}
              </Text>
            </View>
          )}
          <TouchableOpacity
            onPress={() =>
              this.setState({
                projDetails: item.item,
                threeDotModalOpen: true,
              })
            }>
            <Icon
              name="Meatballs"
              color={COLORS.altgreen_300}
              size={12}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  addProjectModal = () => {
    return (
      <Modal
        visible={this.state.addProjModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({addProjModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 120,
                alignItems: 'flex-start',
                paddingTop: 15,
                paddingLeft: 20,
              },
            ]}>
            <TouchableOpacity
              onPress={() =>
                this.setState({addProjModalOpen: false}, () =>
                  this.props.navigation.navigate('ProjectsWebview', {
                    path: '/create-project',
                    type: 'create',
                  }),
                )
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 12,
              }}>
              <Icon name="AddProject" size={16} color={COLORS.dark_800} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 6,
                  },
                ]}>
                Add New
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.setState({addProjModalOpen: false}, () =>
                  this.props.navigation.navigate('ProjectsWebview', {
                    path: '/create-project/offline',
                    type: 'create',
                  }),
                )
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 15,
              }}>
              <Icon name="Jobs" size={16} color={COLORS.dark_800} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 6,
                  },
                ]}>
                Add Experience
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  peopleSharedModal = () => {
    return (
      <Modal
        visible={this.state.peopleSharedModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({peopleSharedModalOpen: false, peopleShared: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who Shared this post
              </Text>
            </View>

            <SharedUserList
              id={this.state.projDetails?.project?.id}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  filterModal = () => {
    return (
      <Modal
        visible={this.state.filterModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({filterModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <Text
            style={[
              typography.Caption,
              {
                fontSize: 12,
                color: COLORS.dark_500,
                textAlign: 'center',
                alignSelf: 'center',
                position: 'absolute',
                bottom: 110,
                zIndex: 2,
              },
            ]}>
            Sort By
          </Text>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
                flexDirection: 'row',
              },
            ]}>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <TouchableOpacity
                style={
                  this.state.filter === 'Opportunities'
                    ? styles.selectedText
                    : styles.unselectedText
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {
                      filterModalOpen: false,
                      filter: 'Opportunities',
                      projects: [],
                      page: 0,
                    },
                    () => this.getProjectList(this.state.userId, 'opportunity'),
                  );
                }}>
                <Text
                  style={[
                    typography.Caption,
                    {
                      fontSize: 11,
                      color:
                        this.state.filter === 'Opportunities'
                          ? COLORS.altgreen_200
                          : COLORS.dark_500,
                      textAlign: 'center',
                      alignSelf: 'center',
                    },
                  ]}>
                  All
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  this.state.filter === 'Jobs'
                    ? styles.selectedText
                    : styles.unselectedText
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {
                      filterModalOpen: false,
                      filter: 'Jobs',
                      projects: [],
                      page: 0,
                    },
                    () => this.getProjectList(this.state.userId, 'jobs'),
                  );
                }}>
                <Text
                  style={[
                    typography.Caption,
                    {
                      fontSize: 11,
                      color:
                        this.state.filter === 'Jobs'
                          ? COLORS.altgreen_200
                          : COLORS.dark_500,
                      textAlign: 'center',
                      alignSelf: 'center',
                    },
                  ]}>
                  Jobs
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  this.state.filter === 'Assignments'
                    ? styles.selectedText
                    : styles.unselectedText
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {
                      filterModalOpen: false,
                      filter: 'Assignments',
                      projects: [],
                      page: 0,
                    },
                    () => this.getProjectList(this.state.userId, 'assignments'),
                  );
                }}>
                <Text
                  style={[
                    typography.Caption,
                    {
                      fontSize: 11,
                      color:
                        this.state.filter === 'Assignments'
                          ? COLORS.altgreen_200
                          : COLORS.dark_500,
                      textAlign: 'center',
                      alignSelf: 'center',
                    },
                  ]}>
                  Assignments
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  this.state.filter === 'Events'
                    ? styles.selectedText
                    : styles.unselectedText
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {
                      filterModalOpen: false,
                      filter: 'Events',
                      projects: [],
                      page: 0,
                    },
                    () => this.getProjectList(this.state.userId, 'events'),
                  );
                }}>
                <Text
                  style={[
                    typography.Caption,
                    {
                      fontSize: 11,
                      color:
                        this.state.filter === 'Events'
                          ? COLORS.altgreen_200
                          : COLORS.dark_500,
                      textAlign: 'center',
                      alignSelf: 'center',
                    },
                  ]}>
                  Events
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  this.state.filter === 'Training'
                    ? styles.selectedText
                    : styles.unselectedText
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {
                      filterModalOpen: false,
                      filter: 'Training',
                      projects: [],
                      page: 0,
                    },
                    () => this.getProjectList(this.state.userId, 'trainings'),
                  );
                }}>
                <Text
                  style={[
                    typography.Caption,
                    {
                      fontSize: 11,
                      color:
                        this.state.filter === 'Training'
                          ? COLORS.altgreen_200
                          : COLORS.dark_500,
                      textAlign: 'center',
                      alignSelf: 'center',
                    },
                  ]}>
                  Training
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  this.state.filter === 'Expedition'
                    ? styles.selectedText
                    : styles.unselectedText
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {
                      filterModalOpen: false,
                      filter: 'Expedition',
                      projects: [],
                      page: 0,
                    },
                    () => this.getProjectList(this.state.userId, 'expeditions'),
                  );
                }}>
                <Text
                  style={[
                    typography.Caption,
                    {
                      fontSize: 11,
                      color:
                        this.state.filter === 'Expedition'
                          ? COLORS.altgreen_200
                          : COLORS.dark_500,
                      textAlign: 'center',
                      alignSelf: 'center',
                    },
                  ]}>
                  Expedition
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  this.state.filter === 'Fund Raise'
                    ? styles.selectedText
                    : styles.unselectedText
                }
                activeOpacity={0.6}
                onPress={() => {
                  this.setState(
                    {
                      filterModalOpen: false,
                      filter: 'Fund Raise',
                      projects: [],
                      page: 0,
                    },
                    () => this.getProjectList(this.state.userId, 'fundraise'),
                  );
                }}>
                <Text
                  style={[
                    typography.Caption,
                    {
                      fontSize: 11,
                      color:
                        this.state.filter === 'Fund Raise'
                          ? COLORS.altgreen_200
                          : COLORS.dark_500,
                      textAlign: 'center',
                      alignSelf: 'center',
                    },
                  ]}>
                  Fund Raise
                </Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  };

  projectDetailsModal = () => {
    let {projDetails} = this.state;
    return (
      <Modal
        visible={this.state.projectDetailsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({projectDetailsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <ProjectView
            projDetails={projDetails}
            slug={projDetails?.project?.slug}
            userId={this.state.userId}
            navigation={this.navigation}
            changeFun={this.changeFun}
            seeWhoShared={() =>
              this.setState({
                projectDetailsModalOpen: false,
                peopleSharedModalOpen: true,
              })
            }
          />
        </View>
      </Modal>
    );
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          REACT_APP_domainUrl +
          '/project/feeds/' +
          this.state.projDetails.project.slug,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // console.log('shared with activity type of result.activityType')
        } else {
          // console.log('shared')
        }
      } else if (result.action === Share.dismissedAction) {
        // console.log('dismissed')
      }
    } catch (error) {
      // console.log(error.message)
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation.navigate('RepostOnWenat', {
                    link: false,
                    pressedActivityId: this.state.projDetails?.project?.id,
                    entityType: this.state.projDetails?.project?.type,
                    type: 'PROJECT',
                  }),
                );
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl +
                    '/project/feeds/' +
                    this.state.projDetails.project.slug,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.projDetails?.project?.id,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          response.data.body.reported
            ? Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Your report request was already taken',
                duration: Snackbar.LENGTH_LONG,
              })
            : this.setState({
                threeDotModalOpen: false,
                reasonForReportingModalOpen: true,
              });
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <Report
          changeState={this.changeState}
          entityId={this.state.projDetails?.project?.id}
          entityType={this.state.projDetails?.project?.type}
        />
      </Modal>
    );
  };

  participantAvailableForHideAlert = () => {
    Alert.alert(
      '',
      'The ' +
        this.state.projDetails?.project?.type +
        ', ' +
        this.state.projDetails?.project?.title +
        ' you wish to hide will continue to be visible to other participants as before.',

      [
        {
          text: 'YES',
          onPress: () => this.hideProject(),
          style: 'cancel',
        },
        {
          text: 'NO',
          // onPress: () => console.log('user cancelled deletion'),
        },
      ],
    );
  };

  isParticipantAvailable = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/participation/isparticipant-available?projectId=' +
        this.state.projDetails.project.id,
      withCredentials: true,
    })
      .then((response) => {
        response.data.body === true
          ? this.participantAvailableForHideAlert()
          : this.hideProject();
      })
      .catch((err) => console.log(err));
  };

  hideProject = () => {
    let postBody = {
      userId: this.state.userId,
      activityId: this.state.projDetails.project.id,
      entityType: this.state.projDetails.project.type,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({threeDotModalOpen: false}, () => {
          let tempindex = this.state.projects.findIndex(
            (x) => x.project.id === this.state.projDetails.project.id,
          );
          this.state.projects.splice(tempindex, 1);
          this.setState({
            projects: this.state.projects,
          });
        });
      })
      .catch((err) => console.log(err.response.data));
  };

  threeDotModal = () => {
    let {projDetails} = this.state;
    return (
      <Modal
        visible={this.state.threeDotModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({threeDotModalOpen: false, projDetails: {}})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                minHeight: 120,
                maxHeight: 280,
                alignItems: 'flex-start',
                paddingTop: 15,
                paddingLeft: 20,
              },
            ]}>
            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  threeDotModalOpen: false,
                  peopleSharedModalOpen: true,
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  See who shared it
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.setState({threeDotModalOpen: false, shareModalOpen: true})
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 12,
              }}>
              <Icon name="Share" size={16} color={COLORS.dark_600} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_600,
                    marginLeft: 10,
                  },
                ]}>
                Share
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this.hideProject}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 15,
              }}>
              <Icon name="Eye_OL" size={16} color={COLORS.dark_600} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_600,
                    marginLeft: 10,
                  },
                ]}>
                Hide
              </Text>
            </TouchableOpacity>

            {projDetails.creatorUserId !== this.state.userId && (
              <TouchableOpacity
                onPress={this.verifyReported}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 15,
                }}>
                <Icon
                  name="ReportComment_OL"
                  size={16}
                  color={COLORS.dark_600}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {
                      color: COLORS.dark_600,
                      marginLeft: 10,
                    },
                  ]}>
                  Report
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.filterModal()}
        {this.projectDetailsModal()}
        {this.addProjectModal()}
        {this.threeDotModal()}
        {this.reasonForReportingModal()}
        {this.shareModal()}
        {this.peopleSharedModal()}

        <View>
          <SearchBar
            changeState={this.changeState}
            navigateNotification={this.navigateNotification}
            navigateProfile={this.navigateProfile}
            navigation={this.navigation}
          />
        </View>

        <View style={styles.header}>
          <TouchableOpacity style={styles.headerItemSelected}>
            <Text style={[typography.Body_1_bold, {color: COLORS.white}]}>
              OPPORTUNITY
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('MyProject')}
            style={styles.headerItemNotSelected}>
            <Text style={[typography.Subtitle_2, {color: COLORS.altgreen_200}]}>
              MY PROJECTS
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('PinnedProject')}
            style={styles.headerItemNotSelected}>
            <Text style={[typography.Subtitle_2, {color: COLORS.altgreen_200}]}>
              PINNED
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.setState({addProjModalOpen: true})}
            style={styles.addProj}>
            <Icon
              name="AddProject_Fl"
              color={COLORS.dark_800}
              size={18}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
            />
          </TouchableOpacity>
        </View>

        {this.state.projects.length ? (
          <FlatList
            data={this.state.projects}
            keyExtractor={(item) => item.projectId}
            ListHeaderComponent={this.flatlistHeader}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={2}
            renderItem={(item) => this.renderProjectItem(item)}
          />
        ) : (
          <>
            {this.flatlistHeader()}
            <Text
              style={[
                typography.Caption,
                {
                  color: COLORS.dark_600,
                  alignSelf: 'center',
                  marginTop: 20,
                  maxWidth: '90%',
                },
              ]}>
              There are no {this.state.filter} matching your preferences yet.
              Keep checking this space for new projects shared by the community.
            </Text>
          </>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    backgroundColor: COLORS.primarydark,
    height: 50,
    alignItems: 'flex-end',
  },
  headerItemSelected: {
    borderBottomColor: COLORS.green_500,
    borderBottomWidth: 6,
    paddingBottom: 8,
    marginLeft: 12,
  },
  headerItemNotSelected: {
    paddingBottom: 2,
    paddingBottom: 14,
    marginLeft: 12,
  },
  addProj: {
    backgroundColor: COLORS.green_500,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 50,
    position: 'absolute',
    right: 0,
  },
  projItem: {
    marginHorizontal: 15,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    marginVertical: 7.5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  projCoverImg: {
    width: '100%',
    height: 100,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  selectedTab: {
    borderBottomColor: COLORS.dark_800,
    borderBottomWidth: 4,
    paddingBottom: 4,
    marginLeft: 10,
  },
  unselectedTab: {
    marginLeft: 10,
  },
  selectedTabText: {
    color: COLORS.dark_800,
  },
  unselectedTabText: {
    color: COLORS.altgreen_300,
  },
  unselectedText: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLORS.altgreen_300,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginLeft: 10,
  },
  selectedText: {
    backgroundColor: COLORS.dark_700,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginLeft: 10,
  },
});
