import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
  Modal,
  ScrollView,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Snackbar from 'react-native-snackbar';

import SearchBar from '../../../Components/User/SearchBar';
import {nFormatter} from '../../../Components/Shared/commonFunction';
import Report from '../../../Components/User/Common/Report';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import projectDefault from '../../../../assets/project-default.jpg';
import {REACT_APP_userServiceURL} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const moment = require('moment');
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class PinnedProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      page: 0,
      projects: [],
      userType: '',
      addProjModalOpen: false,
      reasonForReportingModalOpen: false,
      projectId: '',
      projectType: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });
    AsyncStorage.getItem('userId').then((value) => {
      value && this.setState({userId: value});
      value && this.getProjectList(value, 'opportunity');

      axios({
        method: 'get',
        url:
          REACT_APP_userServiceURL +
          '/profile/get?id=' +
          value +
          '&otherUserId=' +
          '',
        cache: true,
        withCredentials: true,
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          this.setState({userType: response.data.body.type});
          // console.log('--- response.data.body ---', response.data.body)
        })
        .catch((err) => {
          // console.log("Profile data error : ", err)
        });
    });
  }

  navigation = (value, params) => {
    return this.props.navigation.navigate(value, params);
  };

  changeState = (value) => {
    this.setState(value);
  };

  navigateNotification = () => {
    this.props.navigation.navigate('Notification');
  };

  navigateProfile = () => {
    if (this.state.userType === 'INDIVIDUAL') {
      this.props.navigation.navigate('ProfileStack');
    } else if (this.state.userType === 'COMPANY') {
      this.props.navigation.navigate('ProfileStack', {
        screen: 'CompanyProfileScreen',
      });
    }
  };

  getProjectList = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/pinned-projects/' +
        userId +
        '/?page=' +
        this.state.page +
        '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          projects: this.state.projects.concat(response.data.body.content),
        });
      })
      .catch((err) => console.log(err));
  };

  handleLoadMore = () => {
    this.setState({page: this.state.page + 1}, () =>
      this.getProjectList(this.state.userId),
    );
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + 's';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + 'm ago';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + 'h ago';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + 'd ago';
    }

    if (difference >= 864000) {
      return 'on ' + day + ' ' + month + ' ' + year;
    }
  };

  renderProjectItem = (item) => {
    return (
      <View style={styles.projItem}>
        {item.item.project.type === 'ASSIGNMENT' ||
        item.item.project.type === 'JOB' ? (
          <View style={{height: 80, marginHorizontal: 15, marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={
                  item.item.creatorBannerImage
                    ? {uri: item.item.creatorBannerImage}
                    : projectDefault
                }
                style={{height: 60, width: 60, borderRadius: 30}}
              />
              <View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_800, marginLeft: 10},
                  ]}>
                  {item.item.creatorName}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    marginLeft: 10,
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Location"
                    color={COLORS.altgreen_300}
                    size={12}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                  <Text
                    style={[
                      typography.Note2,
                      {color: COLORS.altgreen_300, marginLeft: 5},
                    ]}>
                    {item.item.project.location.country}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        ) : (
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ProjectDetailView', {
                slug: item.item.project.slug,
              })
            }>
            <Image
              source={
                item.item.project.coverImage
                  ? {uri: item.item.project.coverImage}
                  : projectDefault
              }
              style={styles.projCoverImg}
            />

            {item.item.project?.cost > 0 && (
              <View
                style={{
                  flexDirection: 'row',
                  bottom: 6,
                  right: 6,
                  position: 'absolute',
                  backgroundColor: COLORS.white,
                  paddingHorizontal: 8,
                  paddingVertical: Platform.OS === 'ios' ? 5 : -2,
                  borderRadius: 4,
                  alignItems: 'center',
                }}>
                <Icon
                  name="Wallet"
                  size={12}
                  color={COLORS.grey_400}
                  style={{marginTop: Platform.OS === 'ios' ? 0 : 6}}
                />
                <Text
                  style={[
                    typography.Note2,
                    {color: COLORS.dark_800, marginLeft: 6},
                  ]}>
                  {item.item?.project?.currency +
                    ' ' +
                    nFormatter(item.item?.project?.cost, 1)}
                </Text>
              </View>
            )}
          </TouchableOpacity>
        )}
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('ProjectDetailView', {
              slug: item.item.project.slug,
            })
          }>
          <Text
            style={[
              typography.Button_Lead,
              {color: COLORS.dark_800, marginTop: 10, marginLeft: 10},
            ]}>
            {item.item.project.title}
          </Text>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            marginHorizontal: 10,
            paddingBottom: 5,
            width: '85%',
          }}>
          {item.item.project.type === 'ASSIGNMENT' ||
          item.item.project.type === 'JOB' ? null : (
            <View
              style={{
                width: 40,
                alignItems: 'center',
                justifyContent: 'center',
                paddingRight: 5,
                borderRightColor: COLORS.altgreen_400,
                borderRightWidth: 2,
              }}>
              <Text
                style={[
                  typography.H6,
                  {color: COLORS.dark_600, marginLeft: 5},
                ]}>
                {moment
                  .unix(item.item.project.beginningTime / 1000)
                  .format('DD')}
              </Text>
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.altgreen_400, marginLeft: 5},
                ]}>
                {moment
                  .unix(item.item.project.beginningTime / 1000)
                  .format('MMM')
                  .toUpperCase()}
              </Text>
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.altgreen_400, marginLeft: 5},
                ]}>
                {moment
                  .unix(item.item.project.beginningTime / 1000)
                  .format('YYYY')}
              </Text>
            </View>
          )}
          <Text
            numberOfLines={3}
            style={[
              typography.Caption,
              {color: COLORS.dark_600, marginLeft: 5},
            ]}>
            {item.item.project.shortBrief}
          </Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingRight: 15,
            paddingVertical: Platform.OS === 'ios' ? 8 : 0,
            borderTopColor: COLORS.grey_300,
            borderTopWidth: 0.6,
          }}>
          {item.item.project.type === 'ASSIGNMENT' ||
          item.item.project.type === 'JOB' ? (
            <Text
              style={[
                typography.Note2,
                {color: COLORS.altgreen_300, marginLeft: 15},
              ]}>
              Posted {this.unixTime2(item.item.project.createTime)}
            </Text>
          ) : (
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 15,
                alignItems: 'center',
              }}>
              <Icon
                name="Location"
                color={COLORS.altgreen_300}
                size={12}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.altgreen_300, marginLeft: 5},
                ]}>
                {item.item.project.location.country}
              </Text>
            </View>
          )}
          {item.item.project.creatorUserId !== this.state.userId && (
            <TouchableOpacity
              onPress={() =>
                this.verifyReported(
                  item.item.project.id,
                  item.item.project.type,
                )
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Icon
                name="ReportComment_OL"
                size={14}
                color={COLORS.altgreen_300}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };

  verifyReported = (projectId, projectType) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        projectId,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          response.data.body.reported
            ? Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Your report request was already taken',
                duration: Snackbar.LENGTH_LONG,
              })
            : this.setState({
                reasonForReportingModalOpen: true,
                projectId: projectId,
                projectType: projectType,
              });
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <Report
          changeState={this.changeState}
          entityId={this.state.projectId}
          entityType={this.state.projectType}
        />
      </Modal>
    );
  };

  addProjectModal = () => {
    return (
      <Modal
        visible={this.state.addProjModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({addProjModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 120,
                alignItems: 'flex-start',
                paddingTop: 15,
                paddingLeft: 20,
              },
            ]}>
            <TouchableOpacity
              onPress={() =>
                this.setState({addProjModalOpen: false}, () =>
                  this.props.navigation.navigate('ProjectsWebview', {
                    path: '/create-project',
                    type: 'create',
                  }),
                )
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 12,
              }}>
              <Icon name="AddProject" size={16} color={COLORS.dark_800} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 6,
                  },
                ]}>
                Add New
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.setState({addProjModalOpen: false}, () =>
                  this.props.navigation.navigate('ProjectsWebview', {
                    path: '/create-project/offline',
                    type: 'create',
                  }),
                )
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 15,
              }}>
              <Icon name="Jobs" size={16} color={COLORS.dark_800} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 6,
                  },
                ]}>
                Add Experience
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.addProjectModal()}
        {this.reasonForReportingModal()}
        <View>
          <SearchBar
            changeState={this.changeState}
            navigateNotification={this.navigateNotification}
            navigateProfile={this.navigateProfile}
            navigation={this.navigation}
          />
        </View>

        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ProjectScreen')}
            style={styles.headerItemNotSelected}>
            <Text style={[typography.Subtitle_2, {color: COLORS.altgreen_200}]}>
              OPPORTUNITY
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('MyProject')}
            style={styles.headerItemNotSelected}>
            <Text style={[typography.Subtitle_2, {color: COLORS.altgreen_200}]}>
              MY PROJECTS
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.headerItemSelected}>
            <Text style={[typography.Body_1_bold, {color: COLORS.white}]}>
              PINNED
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.addProj}
            onPress={() => this.setState({addProjModalOpen: true})}>
            <Icon
              name="AddProject_Fl"
              color={COLORS.dark_800}
              size={18}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
            />
          </TouchableOpacity>
        </View>

        <FlatList
          data={this.state.projects}
          keyExtractor={(item) => item.projectId}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={2}
          renderItem={(item) => this.renderProjectItem(item)}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    backgroundColor: COLORS.primarydark,
    height: 50,
    alignItems: 'flex-end',
  },
  headerItemSelected: {
    borderBottomColor: COLORS.green_500,
    borderBottomWidth: 6,
    paddingBottom: 8,
    marginLeft: 12,
  },
  headerItemNotSelected: {
    paddingBottom: 2,
    paddingBottom: 14,
    marginLeft: 12,
  },
  addProj: {
    backgroundColor: COLORS.green_500,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 50,
    position: 'absolute',
    right: 0,
  },
  projItem: {
    marginHorizontal: 15,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    marginVertical: 7.5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  projCoverImg: {
    width: '100%',
    height: 100,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  selectedTab: {
    borderBottomColor: COLORS.dark_800,
    borderBottomWidth: 4,
    paddingBottom: 4,
    marginLeft: 10,
  },
  unselectedTab: {
    marginLeft: 10,
  },
  selectedTabText: {
    color: COLORS.dark_800,
  },
  unselectedTabText: {
    color: COLORS.altgreen_300,
  },
  unselectedText: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLORS.altgreen_300,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginLeft: 10,
  },
  selectedText: {
    backgroundColor: COLORS.dark_700,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginLeft: 10,
  },
});
