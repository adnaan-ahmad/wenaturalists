import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
  Modal,
  ScrollView,
  Share,
  TextInput,
  KeyboardAvoidingView,
  Alert,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Snackbar from 'react-native-snackbar';
import Clipboard from '@react-native-community/clipboard';

import SearchBar from '../../../Components/User/SearchBar';
import Report from '../../../Components/User/Common/Report';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import projectDefault from '../../../../assets/project-default.jpg';
import SharedUserList from '../../../Components/User/Common/SharedUserList';
import {nFormatter} from '../../../Components/Shared/commonFunction';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';

const moment = require('moment');
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class MyProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      filter: 'timeline',
      runningProjects: [],
      upcomingProjects: [],
      closedProjects: [],
      cancelledProjects: [],
      addProjModalOpen: false,
      threeDotModalOpen: false,
      reasonForReportingModalOpen: false,
      shareModalOpen: false,
      deleteOrCancelModalOpen: false,
      peopleSharedModalOpen: false,
      userType: '',
      actionType: '',
      reasonForDeleteOrCancel: '',
      description: '',
      projectDetails: {},
      textInputHeight: 200,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('refreshToken').then((value) => {
      if (value === null) {
        this.props.navigation.replace('Login');
      }
    });
    AsyncStorage.getItem('userId').then((value) => {
      value && this.setState({userId: value});
      value && this.ongoingProject(value);
      value && this.upcomingProject(value);
      value && this.closedProject(value);
      value && this.cancelledProject(value);

      axios({
        method: 'get',
        url:
          REACT_APP_userServiceURL +
          '/profile/get?id=' +
          value +
          '&otherUserId=' +
          '',
        cache: true,
        withCredentials: true,
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          this.setState({userType: response.data.body.type});
          // console.log('--- response.data.body ---', response.data.body)
        })
        .catch((err) => {
          // console.log("Profile data error : ", err)
        });

      this._unsubscribe = this.props.navigation.addListener('focus', () => {
        value && this.ongoingProject(value);
        value && this.upcomingProject(value);
        value && this.closedProject(value);
        value && this.cancelledProject(value);
      });
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  changeState = (value) => {
    this.setState(value);
  };

  navigation = (value, params) => {
    this.setState(
      {projectDetailsModalOpen: false, peopleSharedModalOpen: false},
      () => this.props.navigation.navigate(value, params),
    );
  };

  navigateNotification = () => {
    this.props.navigation.navigate('Notification');
  };

  navigateProfile = () => {
    if (this.state.userType === 'INDIVIDUAL') {
      this.props.navigation.navigate('ProfileStack');
    } else if (this.state.userType === 'COMPANY') {
      this.props.navigation.navigate('ProfileStack', {
        screen: 'CompanyProfileScreen',
      });
    }
  };

  unixTime2 = (UNIX_timestamp) => {
    var date = new Date(UNIX_timestamp);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    var year = date.getFullYear();
    var month = months[date.getMonth()];
    var day = date.getDate();
    var time = date.getTime();

    var todaysDate = new Date();
    var time2 = todaysDate.getTime();

    var difference = Math.floor((time2 - time) / 1000);

    if (difference < 60) {
      return difference + 's';
    }

    if (difference > 59 && difference < 3600) {
      return Math.floor(difference / 60) + 'm ago';
    }

    if (difference >= 3600 && difference < 86400) {
      return Math.floor(difference / 3600) + 'h ago';
    }

    if (difference >= 86400 && difference < 864000) {
      return Math.floor(difference / 72000) + 'd ago';
    }

    if (difference >= 864000) {
      return 'on ' + day + ' ' + month + ' ' + year;
    }
  };

  ongoingProject = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/running-project/' +
        userId +
        '/' +
        userId +
        '/?filterType=' +
        this.state.filter +
        '&isPinned=false&userActivityType=ALL&projectFilterType=ALL&page=0&size=5',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          runningProjects: response.data.body.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  upcomingProject = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/upcoming-projects/' +
        userId +
        '/' +
        userId +
        '/?filterType=' +
        this.state.filter +
        '&isPinned=false&userActivityType=ALL&projectFilterType=ALL&page=0&size=5',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          upcomingProjects: response.data.body.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  closedProject = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/completed-projects/' +
        userId +
        '/' +
        userId +
        '/?filterType=' +
        this.state.filter +
        '&isPinned=false&userActivityType=ALL&projectFilterType=ALL&page=0&size=5',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          closedProjects: response.data.body.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  cancelledProject = (userId) => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/cancelled-projects/' +
        userId +
        '/?projectFilterType=ALL&page=0&size=5',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          cancelledProjects: response.data.body.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  profileVisibilityAlert = () => {
    Alert.alert(
      '',
      'Are you sure you want to display ' +
        this.state.projectDetails?.project?.title +
        ' in your profile?' +
        '\n\nNote: Ongoing or the Completed projects will reflect in your Profile under the experience section',

      [
        {
          text: 'YES',
          onPress: () => this.profileVisibility(),
          style: 'cancel',
        },
        {
          text: 'NO',
          // onPress: () => console.log('user cancelled deletion'),
        },
      ],
    );
  };

  profileVisibility = () => {
    let postBody = {
      userId: this.state.userId,
      projectId: this.state.projectDetails.project.id,
      visibleToProfile: true,
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/backend/participation/update/visible-to-profile',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({threeDotModalOpen: false}, () => {
          let tempindex = this.state.projects.findIndex(
            (x) => x.project.id === this.state.projectDetails.project.id,
          );
          this.state.projects[tempindex].visibleToProfile = true;
          this.setState({
            projects: this.state.projects,
          });
        });
      })
      .catch((err) => console.log(err.response.data));
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.projectDetails?.project?.id,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          response.data.body.reported
            ? Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Your report request was already taken',
                duration: Snackbar.LENGTH_LONG,
              })
            : this.setState({
                threeDotModalOpen: false,
                reasonForReportingModalOpen: true,
              });
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <Report
          changeState={this.changeState}
          entityId={this.state.projectDetails?.project?.id}
          entityType={this.state.projectDetails?.project?.type}
        />
      </Modal>
    );
  };

  deleteOrCancelModal = () => {
    return (
      <Modal
        visible={this.state.deleteOrCancelModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 0}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({
                deleteOrCancelModalOpen: false,
                reasonForDeleteOrCancel: '',
                description: '',
                actionType: '',
                textInputHeight: 200,
              })
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingHorizontal: 15,
                minHeight:
                  Platform.OS === 'ios' ? this.state.textInputHeight : 200,
              },
            ]}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                {this.state.actionType === 'delete'
                  ? 'Reason for deleting'
                  : 'Reason for cancelling'}
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0, paddingVertical: 10},
                    ]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {
                        borderBottomWidth: 0,
                        width: '100%',
                        paddingVertical: 10,
                      },
                    ]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForDeleteOrCancel:
                    Date.now() > this.state.projectDetails?.project?.endingTime
                      ? 'NO_LONGER_INTERESTED'
                      : 'POSTPONED_NOT_YET_FINALIZED',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForDeleteOrCancel ===
                    'POSTPONED_NOT_YET_FINALIZED' ||
                  this.state.reasonForDeleteOrCancel ===
                    'NO_LONGER_INTERESTED' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  {Date.now() > this.state.projectDetails?.project?.endingTime
                    ? 'I am no longer interested'
                    : 'Date postponed not yet finalized'}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0, paddingVertical: 10},
                    ]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0, width: '100%'},
                    ]
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  reasonForDeleteOrCancel: 'OTHERS',
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 17,
                    height: 17,
                    borderRadius: 8.5,
                    borderColor: COLORS.altgreen_300,
                    borderWidth: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLORS.altgreen_200,
                  }}>
                  {this.state.reasonForDeleteOrCancel === 'OTHERS' ? (
                    <Icon
                      name="Bullet_Fill"
                      size={17}
                      color={COLORS.dark_800}
                      style={{
                        marginLeft: -2,
                        marginTop: Platform.OS === 'android' ? 8 : 0,
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </View>
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  Others
                </Text>
              </View>
            </TouchableOpacity>

            {this.state.reasonForDeleteOrCancel === 'OTHERS' && (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingLeft: 6,
                  paddingRight: 12,
                }}>
                <TextInput
                  placeholder="Write the details"
                  multiline
                  style={[
                    typography.Subtitle_1,
                    {
                      width: '90%',
                      height: 56,
                      backgroundColor: COLORS.white,
                      color: COLORS.dark_700,
                      borderBottomColor: COLORS.grey_350,
                      borderBottomWidth: 1,
                    },
                  ]}
                  onFocus={() => this.setState({textInputHeight: 500})}
                  onBlur={() => this.setState({textInputHeight: 200})}
                  onChangeText={(value) => this.setState({description: value})}
                  value={this.state.description}
                />
              </View>
            )}

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                this.state.reasonForDeleteOrCancel === ''
                  ? Snackbar.show({
                      backgroundColor: '#B22222',
                      text: 'Please select an option',
                      duration: Snackbar.LENGTH_LONG,
                    })
                  : this.state.reasonForDeleteOrCancel === 'OTHERS' &&
                    this.state.description.trim() === ''
                  ? Snackbar.show({
                      backgroundColor: '#B22222',
                      text: 'Please enter the detail',
                      duration: Snackbar.LENGTH_LONG,
                    })
                  : (this.handleDeleteOrCancelSubmit(),
                    this.setState({
                      deleteOrCancelModalOpen: false,
                      reasonForDeleteOrCancel: '',
                      description: '',
                      actionType: '',
                    }));
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 15,
                height: 27,
                width: '80%',
                alignSelf: 'center',
                marginVertical: 10,
                borderRadius: 16,
                textAlign: 'center',
                borderWidth: 1,
                borderColor: '#698F8A',
              }}>
              <Text
                style={{
                  color: '#698F8A',
                  fontSize: 14,
                  paddingHorizontal: 14,
                  paddingVertical: 20,
                  fontFamily: 'Montserrat-Medium',
                  fontWeight: 'bold',
                  marginBottom: Platform.OS === 'ios' ? 15 : 0,
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  handleDeleteOrCancelSubmit = () => {
    let url = `${REACT_APP_userServiceURL}/backend/demolished/add`;

    let postBody = {
      activityId: this.state.projectDetails.project.id,
      creatorUserId: this.state.projectDetails.creatorUserId,
      description: this.state.description,
      entityType: this.state.projectDetails.project.type,
      eventType: this.state.actionType === 'delete' ? 'DELETED' : 'CANCELLED',
      reason: this.state.reasonForDeleteOrCancel,
      userId: this.state.userId,
    };

    axios({
      method: 'post',
      url: url,
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.ongoingProject(this.state.userId);
        this.upcomingProject(this.state.userId);
        this.closedProject(this.state.userId);
        this.cancelledProject(this.state.userId);
        this.setState({textInputHeight: 200});
      })
      .catch((err) => {
        console.log(err);
      });
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          REACT_APP_domainUrl +
          '/project/feeds/' +
          this.state.projectDetails.project.slug,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // console.log('shared with activity type of result.activityType')
        } else {
          // console.log('shared')
        }
      } else if (result.action === Share.dismissedAction) {
        // console.log('dismissed')
      }
    } catch (error) {
      // console.log(error.message)
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation.navigate('RepostOnWenat', {
                    link: false,
                    pressedActivityId: this.state.projectDetails?.project?.id,
                    entityType: this.state.projectDetails?.project?.type,
                    type: 'PROJECT',
                  }),
                );
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl +
                    '/project/feeds/' +
                    this.state.projectDetails.project.slug,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  participantAvailableForHideAlert = () => {
    Alert.alert(
      '',
      'The ' +
        this.state.projectDetails?.project?.type +
        ', ' +
        this.state.projectDetails?.project?.title +
        ' you wish to hide will continue to be visible to other participants as before.',

      [
        {
          text: 'YES',
          onPress: () => this.hideProject(),
          style: 'cancel',
        },
        {
          text: 'NO',
          // onPress: () => console.log('user cancelled deletion'),
        },
      ],
    );
  };

  isParticipantAvailable = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/participation/isparticipant-available?projectId=' +
        this.state.projectDetails.project.id,
      withCredentials: true,
    })
      .then((response) => {
        response.data.body === true
          ? this.participantAvailableForHideAlert()
          : this.hideProject();
      })
      .catch((err) => console.log(err));
  };

  hideProject = () => {
    let postBody = {
      userId: this.state.userId,
      activityId: this.state.projectDetails.project.id,
      entityType: this.state.projectDetails.project.type,
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/backend/hidden/hide',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        this.setState({threeDotModalOpen: false}, () => {
          this.ongoingProject(this.state.userId);
          this.upcomingProject(this.state.userId);
          this.closedProject(this.state.userId);
          this.cancelledProject(this.state.userId);
        });
      })
      .catch((err) => console.log(err.response.data));
  };

  renderProjectItem = (item) => {
    return (
      <View
        style={
          item.index === 4
            ? [styles.projItem, {marginRight: 15}]
            : styles.projItem
        }>
        {item.item.project.type === 'ASSIGNMENT' ||
        item.item.project.type === 'JOB' ? (
          <View style={{marginHorizontal: 15, marginTop: 10}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={
                  item.item.project.coverImage
                    ? {uri: item.item.project.coverImage}
                    : projectDefault
                }
                style={{height: 60, width: 60, borderRadius: 30}}
              />
              <View>
                <Text
                  numberOfLines={2}
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_800, marginLeft: 10, maxWidth: 120},
                  ]}>
                  {item.item.creatorName}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    marginLeft: 10,
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Location"
                    color={COLORS.altgreen_300}
                    size={12}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                  <Text
                    style={[
                      typography.Note2,
                      {color: COLORS.altgreen_300, marginLeft: 5},
                    ]}>
                    {item.item.project.location.country}
                  </Text>
                </View>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 5,
              }}>
              {item.item.project?.cost > 0 ? (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="Wallet"
                    size={12}
                    color={COLORS.grey_400}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 6}}
                  />
                  <Text
                    style={[
                      typography.Note2,
                      {color: COLORS.dark_800, marginLeft: 6},
                    ]}>
                    {item.item?.project?.currency +
                      ' ' +
                      nFormatter(item.item?.project?.cost, 1)}
                  </Text>
                </View>
              ) : (
                <Text></Text>
              )}

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {item.item.participationCount > 0 && (
                  <Icon
                    name="AllGender"
                    color={COLORS.altgreen_300}
                    size={12}
                    style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
                  />
                )}
                <Text
                  style={[
                    typography.Note2,
                    {color: COLORS.altgreen_300, marginLeft: 5},
                  ]}>
                  {item.item.participationCount === 0
                    ? ''
                    : item.item.participationCount}{' '}
                  {item.item.participationCount > 1
                    ? 'Applicants'
                    : item.item.participationCount === 0
                    ? ''
                    : 'Applicant'}
                </Text>
              </View>
            </View>
          </View>
        ) : (
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ProjectDetailView', {
                slug: item.item.project.slug,
              })
            }>
            <Image
              source={
                item.item.project.coverImage
                  ? {uri: item.item.project.coverImage}
                  : projectDefault
              }
              style={styles.projCoverImg}
            />

            {item.item.project?.cost > 0 && (
              <View
                style={{
                  flexDirection: 'row',
                  bottom: 6,
                  right: 6,
                  position: 'absolute',
                  backgroundColor: COLORS.white,
                  paddingHorizontal: 8,
                  paddingVertical: Platform.OS === 'ios' ? 5 : -2,
                  borderRadius: 4,
                  alignItems: 'center',
                }}>
                <Icon
                  name="Wallet"
                  size={12}
                  color={COLORS.grey_400}
                  style={{marginTop: Platform.OS === 'ios' ? 0 : 6}}
                />
                <Text
                  style={[
                    typography.Note2,
                    {color: COLORS.dark_800, marginLeft: 6},
                  ]}>
                  {item.item?.project?.currency +
                    ' ' +
                    nFormatter(item.item?.project?.cost, 1)}
                </Text>
              </View>
            )}
          </TouchableOpacity>
        )}
        <TouchableOpacity
          style={{marginTop: 5}}
          onPress={() =>
            this.props.navigation.navigate('ProjectDetailView', {
              slug: item.item.project.slug,
            })
          }>
          <Text
            numberOfLines={1}
            style={[
              typography.Button_Lead,
              {
                color: COLORS.dark_800,
                marginTop:
                  item.item.project.type === 'ASSIGNMENT' ||
                  item.item.project.type === 'JOB'
                    ? 0
                    : 10,
                marginLeft: 10,
              },
            ]}>
            {item.item.project.title}
          </Text>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            marginHorizontal: 10,
            paddingBottom: 5,
            width:
              item.item.project.type === 'ASSIGNMENT' ||
              item.item.project.type === 'JOB'
                ? 200
                : 160,
          }}>
          {item.item.project.type === 'ASSIGNMENT' ||
          item.item.project.type === 'JOB' ? null : (
            <View
              style={{
                width: 40,
                alignItems: 'center',
                justifyContent: 'center',
                paddingRight: 5,
                borderRightColor: COLORS.altgreen_400,
                borderRightWidth: 2,
              }}>
              <Text
                style={[
                  typography.H6,
                  {color: COLORS.dark_600, marginLeft: 5},
                ]}>
                {moment
                  .unix(item.item.project.beginningTime / 1000)
                  .format('DD')}
              </Text>
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.altgreen_400, marginLeft: 5},
                ]}>
                {moment
                  .unix(item.item.project.beginningTime / 1000)
                  .format('MMM')
                  .toUpperCase()}
              </Text>
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.altgreen_400, marginLeft: 5},
                ]}>
                {moment
                  .unix(item.item.project.beginningTime / 1000)
                  .format('YYYY')}
              </Text>
            </View>
          )}
          <Text
            numberOfLines={3}
            style={[
              typography.Caption,
              {color: COLORS.dark_600, marginLeft: 5},
            ]}>
            {item.item.project.shortBrief}
          </Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingRight: 15,
            paddingVertical: 8,
            borderTopColor: COLORS.grey_300,
            borderTopWidth: 0.6,
            backgroundColor:
              ((item.item.openings < 5 && item.item.openings > 0) ||
                (Math.floor(
                  Math.abs(
                    (item.item.project.lastTimeOfApplication - Date.now()) /
                      1000,
                  ) / 86400,
                ) < 5 &&
                  Math.floor(
                    Math.abs(
                      (item.item.project.lastTimeOfApplication - Date.now()) /
                        1000,
                    ) / 86400,
                  ) > 0)) &&
              (item.item.project.type === 'ASSIGNMENT' ||
                item.item.project.type === 'JOB')
                ? COLORS.dark_700
                : COLORS.white,
          }}>
          {item.item.project.type === 'ASSIGNMENT' ||
          item.item.project.type === 'JOB' ? (
            item.item.openings < 5 && item.item.openings > 0 ? (
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.green_500, marginLeft: 15},
                ]}>
                {item.item.openings +
                  ` ${item.item.openings > 1 ? 'OPENINGS' : 'OPENING'} LEFT`}
              </Text>
            ) : Math.floor(
                Math.abs(
                  (item.item.project.lastTimeOfApplication - Date.now()) / 1000,
                ) / 86400,
              ) < 5 &&
              Math.floor(
                Math.abs(
                  (item.item.project.lastTimeOfApplication - Date.now()) / 1000,
                ) / 86400,
              ) > 0 ? (
              <Text
                style={[
                  typography.Button_Lead,
                  {color: COLORS.green_500, marginLeft: 15},
                ]}>
                {Math.floor(
                  Math.abs(
                    (item.item.project.lastTimeOfApplication - Date.now()) /
                      1000,
                  ) / 86400,
                ) +
                  ` ${
                    Math.floor(
                      Math.abs(
                        (item.item.project.lastTimeOfApplication - Date.now()) /
                          1000,
                      ) / 86400,
                    ) > 1
                      ? 'DAYS'
                      : 'DAY'
                  } LEFT TO APPLY`}
              </Text>
            ) : (
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.altgreen_300, marginLeft: 15},
                ]}>
                Posted {this.unixTime2(item.item.project.createTime)}
              </Text>
            )
          ) : (
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 15,
                alignItems: 'center',
              }}>
              <Icon
                name="Location"
                color={COLORS.altgreen_300}
                size={12}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
              <Text
                style={[
                  typography.Note2,
                  {color: COLORS.altgreen_300, marginLeft: 5},
                ]}>
                {item.item.project.location.country}
              </Text>
            </View>
          )}
          {item.item.demolishedType !== 'CANCELLED' && (
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  projectDetails: item.item,
                  threeDotModalOpen: true,
                })
              }
              style={{
                height: 20,
                width: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon
                name="Meatballs"
                color={COLORS.altgreen_300}
                size={12}
                style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };

  addProjectModal = () => {
    return (
      <Modal
        visible={this.state.addProjModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({addProjModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 120,
                alignItems: 'flex-start',
                paddingTop: 15,
                paddingLeft: 20,
              },
            ]}>
            <TouchableOpacity
              onPress={() =>
                this.setState({addProjModalOpen: false}, () =>
                  this.props.navigation.navigate('ProjectsWebview', {
                    path: '/create-project',
                    type: 'create',
                  }),
                )
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 12,
              }}>
              <Icon name="AddProject" size={16} color={COLORS.dark_800} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 6,
                  },
                ]}>
                Add New
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.setState({addProjModalOpen: false}, () =>
                  this.props.navigation.navigate('ProjectsWebview', {
                    path: '/create-project/offline',
                    type: 'create',
                  }),
                )
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 15,
              }}>
              <Icon name="Jobs" size={16} color={COLORS.dark_800} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 6,
                  },
                ]}>
                Add Experience
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  peopleSharedModal = () => {
    return (
      <Modal
        visible={this.state.peopleSharedModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({peopleSharedModalOpen: false, peopleShared: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who Shared this post
              </Text>
            </View>

            <SharedUserList
              id={this.state.projectDetails?.project?.id}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  threeDotModal = () => {
    let {projectDetails} = this.state;
    return (
      <Modal
        visible={this.state.threeDotModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 50}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({threeDotModalOpen: false, projectDetails: {}})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                minHeight: 120,
                maxHeight: 380,
                alignItems: 'flex-start',
                paddingTop: 15,
                paddingLeft: 20,
              },
            ]}>
            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({
                  threeDotModalOpen: false,
                  peopleSharedModalOpen: true,
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="Share"
                  size={17}
                  color={COLORS.altgreen_300}
                  style={Platform.OS === 'android' ? {marginTop: 9} : {}}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {color: COLORS.dark_600, marginLeft: 8},
                  ]}>
                  See who shared it
                </Text>
              </View>
              <Icon
                name="Arrow_Right"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            {!projectDetails.visibleToProfile && (
              <TouchableOpacity
                onPress={this.profileVisibilityAlert}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 12,
                }}>
                <Icon name="Eye_F" size={16} color={COLORS.dark_600} />
                <Text
                  style={[
                    typography.Button_Lead,
                    {
                      color: COLORS.dark_600,
                      marginLeft: 10,
                    },
                  ]}>
                  Profile Visibility
                </Text>
              </TouchableOpacity>
            )}

            <TouchableOpacity
              onPress={() =>
                this.setState({threeDotModalOpen: false, shareModalOpen: true})
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 12,
              }}>
              <Icon name="Share" size={16} color={COLORS.dark_600} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_600,
                    marginLeft: 10,
                  },
                ]}>
                Share
              </Text>
            </TouchableOpacity>
            {projectDetails.canEdit && (
              <TouchableOpacity
                onPress={() =>
                  this.setState({threeDotModalOpen: false}, () =>
                    projectDetails.project && projectDetails.project.isOffLine
                      ? this.props.navigation.navigate('ProjectsWebview', {
                          path: `/edit-offline/${projectDetails.project.type}/${projectDetails.project.slug}`,
                          type: 'edit',
                        })
                      : this.props.navigation.navigate('ProjectsWebview', {
                          path: `/edit-project/${projectDetails.project.type}/${projectDetails.project.slug}`,
                          type: 'edit',
                        }),
                  )
                }
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 12,
                }}>
                <Icon name="EditBox" size={16} color={COLORS.dark_600} />
                <Text
                  style={[
                    typography.Button_Lead,
                    {
                      color: COLORS.dark_600,
                      marginLeft: 10,
                    },
                  ]}>
                  Edit
                </Text>
              </TouchableOpacity>
            )}
            <TouchableOpacity
              onPress={
                projectDetails.creatorUserId === this.state.userId
                  ? this.isParticipantAvailable
                  : this.hideProject
              }
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 15,
              }}>
              <Icon name="Eye_OL" size={16} color={COLORS.dark_600} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_600,
                    marginLeft: 10,
                  },
                ]}>
                Hide
              </Text>
            </TouchableOpacity>
            {projectDetails.canDelete && (
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    threeDotModalOpen: false,
                    deleteOrCancelModalOpen: true,
                    actionType: 'delete',
                  })
                }
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 15,
                }}>
                <Icon name="TrashBin" size={16} color={COLORS.dark_600} />
                <Text
                  style={[
                    typography.Button_Lead,
                    {
                      color: COLORS.dark_600,
                      marginLeft: 10,
                    },
                  ]}>
                  Delete
                </Text>
              </TouchableOpacity>
            )}
            {projectDetails.canCancel && (
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    threeDotModalOpen: false,
                    deleteOrCancelModalOpen: true,
                    actionType: 'cancel',
                  })
                }
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 15,
                }}>
                <Icon name="Cross" size={16} color={COLORS.dark_600} />
                <Text
                  style={[
                    typography.Button_Lead,
                    {
                      color: COLORS.dark_600,
                      marginLeft: 10,
                    },
                  ]}>
                  Cancel
                </Text>
              </TouchableOpacity>
            )}

            {projectDetails.creatorUserId !== this.state.userId && (
              <TouchableOpacity
                onPress={this.verifyReported}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 15,
                }}>
                <Icon
                  name="ReportComment_OL"
                  size={16}
                  color={COLORS.dark_600}
                />
                <Text
                  style={[
                    typography.Button_Lead,
                    {
                      color: COLORS.dark_600,
                      marginLeft: 10,
                    },
                  ]}>
                  Report
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.addProjectModal()}
        {this.threeDotModal()}
        {this.reasonForReportingModal()}
        {this.shareModal()}
        {this.deleteOrCancelModal()}
        {this.peopleSharedModal()}

        <View>
          <SearchBar
            changeState={this.changeState}
            navigateNotification={this.navigateNotification}
            navigateProfile={this.navigateProfile}
            navigation={this.navigation}
          />
        </View>

        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ProjectScreen')}
            style={styles.headerItemNotSelected}>
            <Text style={[typography.Subtitle_2, {color: COLORS.altgreen_200}]}>
              OPPORTUNITY
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.headerItemSelected}>
            <Text style={[typography.Body_1_bold, {color: COLORS.white}]}>
              MY PROJECTS
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('PinnedProject')}
            style={styles.headerItemNotSelected}>
            <Text style={[typography.Subtitle_2, {color: COLORS.altgreen_200}]}>
              PINNED
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.addProj}
            onPress={() => this.setState({addProjModalOpen: true})}>
            <Icon
              name="AddProject_Fl"
              color={COLORS.dark_800}
              size={18}
              style={{marginTop: Platform.OS === 'ios' ? 0 : 5}}
            />
          </TouchableOpacity>
        </View>

        <ScrollView>
          {this.state.runningProjects.length > 0 && (
            <View style={{paddingVertical: 15, backgroundColor: COLORS.white}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 15,
                  alignItems: 'center',
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_800}]}>
                  ONGOING PROJECTS
                </Text>
                {this.state.runningProjects.length >= 5 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('SeeAllProject', {
                        headerTitle: 'ONGOING PROJECTS',
                      })
                    }
                    style={[
                      styles.addProj,
                      {backgroundColor: COLORS.NoColor, width: 70},
                    ]}>
                    <Text
                      style={[
                        typography.Caption,
                        {color: COLORS.dark_500, marginRight: 15},
                      ]}>
                      View All
                    </Text>
                  </TouchableOpacity>
                )}
              </View>

              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.runningProjects}
                keyExtractor={(item) => item.projectId}
                ListFooterComponent={
                  this.state.runningProjects.length >= 5 ? (
                    <View
                      style={{
                        height: 240,
                        width: 150,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('SeeAllProject', {
                            headerTitle: 'ONGOING PROJECTS',
                          })
                        }
                        style={[defaultShape.ContextBtn_OL]}>
                        <Text
                          style={[
                            typography.Caption,
                            {color: COLORS.dark_500},
                          ]}>
                          See more
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <></>
                  )
                }
                renderItem={(item) => this.renderProjectItem(item)}
              />
            </View>
          )}

          {this.state.upcomingProjects.length > 0 && (
            <View style={{paddingVertical: 15}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 15,
                  alignItems: 'center',
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_800}]}>
                  UPCOMING PROJECTS
                </Text>
                {this.state.upcomingProjects.length >= 5 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('SeeAllProject', {
                        headerTitle: 'UPCOMING PROJECTS',
                      })
                    }
                    style={[
                      styles.addProj,
                      {backgroundColor: COLORS.NoColor, width: 70},
                    ]}>
                    <Text
                      style={[
                        typography.Caption,
                        {color: COLORS.dark_500, marginRight: 15},
                      ]}>
                      View All
                    </Text>
                  </TouchableOpacity>
                )}
              </View>

              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.upcomingProjects}
                ListFooterComponent={
                  this.state.upcomingProjects.length >= 5 ? (
                    <View
                      style={{
                        height: 240,
                        width: 150,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('SeeAllProject', {
                            headerTitle: 'UPCOMING PROJECTS',
                          })
                        }
                        style={[defaultShape.ContextBtn_OL]}>
                        <Text
                          style={[
                            typography.Caption,
                            {color: COLORS.dark_500},
                          ]}>
                          See more
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <></>
                  )
                }
                keyExtractor={(item) => item.projectId}
                renderItem={(item) => this.renderProjectItem(item)}
              />
            </View>
          )}

          {this.state.closedProjects.length > 0 && (
            <View style={{paddingVertical: 15, backgroundColor: COLORS.white}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 15,
                  alignItems: 'center',
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_800}]}>
                  COMPLETED PROJECTS
                </Text>
                {this.state.closedProjects.length >= 5 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('SeeAllProject', {
                        headerTitle: 'COMPLETED PROJECTS',
                      })
                    }
                    style={[
                      styles.addProj,
                      {backgroundColor: COLORS.NoColor, width: 70},
                    ]}>
                    <Text
                      style={[
                        typography.Caption,
                        {color: COLORS.dark_500, marginRight: 15},
                      ]}>
                      View All
                    </Text>
                  </TouchableOpacity>
                )}
              </View>

              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.closedProjects}
                ListFooterComponent={
                  this.state.closedProjects.length >= 5 ? (
                    <View
                      style={{
                        height: 240,
                        width: 150,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('SeeAllProject', {
                            headerTitle: 'COMPLETED PROJECTS',
                          })
                        }
                        style={[defaultShape.ContextBtn_OL]}>
                        <Text
                          style={[
                            typography.Caption,
                            {color: COLORS.dark_500},
                          ]}>
                          See more
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <></>
                  )
                }
                keyExtractor={(item) => item.projectId}
                renderItem={(item) => this.renderProjectItem(item)}
              />
            </View>
          )}

          {this.state.cancelledProjects.length > 0 && (
            <View style={{paddingVertical: 15}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 15,
                  alignItems: 'center',
                }}>
                <Text
                  style={[typography.Button_Lead, {color: COLORS.dark_800}]}>
                  CANCELLED PROJECTS
                </Text>
                {this.state.cancelledProjects.length >= 5 && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('SeeAllProject', {
                        headerTitle: 'CANCELLED PROJECTS',
                      })
                    }
                    style={[
                      styles.addProj,
                      {backgroundColor: COLORS.NoColor, width: 70},
                    ]}>
                    <Text
                      style={[
                        typography.Caption,
                        {color: COLORS.dark_500, marginRight: 15},
                      ]}>
                      View All
                    </Text>
                  </TouchableOpacity>
                )}
              </View>

              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.cancelledProjects}
                ListFooterComponent={
                  this.state.cancelledProjects.length >= 5 ? (
                    <View
                      style={{
                        height: 240,
                        width: 150,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('SeeAllProject', {
                            headerTitle: 'CANCELLED PROJECTS',
                          })
                        }
                        style={[defaultShape.ContextBtn_OL]}>
                        <Text
                          style={[
                            typography.Caption,
                            {color: COLORS.dark_500},
                          ]}>
                          See more
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <></>
                  )
                }
                keyExtractor={(item) => item.projectId}
                renderItem={(item) => this.renderProjectItem(item)}
              />
            </View>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    backgroundColor: COLORS.primarydark,
    height: 50,
    alignItems: 'flex-end',
  },
  headerItemSelected: {
    borderBottomColor: COLORS.green_500,
    borderBottomWidth: 6,
    paddingBottom: 8,
    marginLeft: 12,
  },
  headerItemNotSelected: {
    paddingBottom: 2,
    paddingBottom: 14,
    marginLeft: 12,
  },
  addProj: {
    backgroundColor: COLORS.green_500,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 50,
    position: 'absolute',
    right: 0,
  },
  projItem: {
    width: 220,
    marginLeft: 15,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    marginBottom: 8,
    marginTop: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  projCoverImg: {
    width: '100%',
    height: 100,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  selectedTab: {
    borderBottomColor: COLORS.dark_800,
    borderBottomWidth: 4,
    paddingBottom: 4,
    marginLeft: 10,
  },
  unselectedTab: {
    marginLeft: 10,
  },
  selectedTabText: {
    color: COLORS.dark_800,
  },
  unselectedTabText: {
    color: COLORS.altgreen_300,
  },
  unselectedText: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLORS.altgreen_300,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginLeft: 10,
  },
  selectedText: {
    backgroundColor: COLORS.dark_700,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginLeft: 10,
  },
});
