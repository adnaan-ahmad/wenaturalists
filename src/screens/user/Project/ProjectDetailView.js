import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
  Modal,
  ScrollView,
  ImageBackground,
  TextInput,
  KeyboardAvoidingView,
  Alert,
  Dimensions,
  Share,
} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import {cloneDeep} from 'lodash';
import Snackbar from 'react-native-snackbar';
import ImagePicker from 'react-native-image-crop-picker';
import Clipboard from '@react-native-community/clipboard';

import SearchBar from '../../../Components/User/SearchBar';
import {COLORS} from '../../../Components/Shared/Colors';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';
import projectDefault from '../../../../assets/project-default.jpg';
import defaultProfile from '../../../../assets/defaultProfile.png';
import defaultCover from '../../../../assets/defaultCover.png';
import defaultBusiness from '../../../../assets/DefaultBusiness.png';
import defaultCircle from '../../../../assets/CirclesDefault.png';
import postIcon from '../../../../assets/Post_Icon.png';
import blogIcon from '../../../../assets/Blog_Icon.png';
import linkIcon from '../../../../assets/Link_Icon.png';
import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import {validateEmail} from '../../../Components/Shared/commonFunction';
import ProjectView from '../../../Components/User/Common/ProjectView';
import CommonFeeds from '../../../Components/User/Common/CommonFeeds';
import SharedUserList from '../../../Components/User/Common/SharedUserList';
import InviteNew from '../../../Components/User/Project/InviteNew';
import GuestInvitee from '../../../Components/User/Project/GuestInvitee';
import Admin from '../../../Components/User/Project/Admin';
import Contribution from '../../../Components/User/Project/Contribution';
import Report from '../../../Components/User/Common/Report';
import FeedBack from '../FeedBack/FeedBack';

const moment = require('moment');
const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const windowWidth = Dimensions.get('window').width;
const regexp = new RegExp(
  '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$',
  'i',
);

export default class ProjectDetailView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slug: this.props.route.params.slug,
      userId: '',
      projDetails: {},
      projectDetailsModalOpen: false,
      peopleSharedModalOpen: false,
      postModalOpen: false,
      readMore: false,
      setreadmore: false,
      pageNoFeed: 0,
      shareModalOpen: false,
      reasonForReportingModalOpen: false,

      selectedTab: 'about',
      isCompany: false,
      coverImage: '',

      // participants state

      newApplicationExpand: false,
      pendingInvitationExpand: false,
      inviteOutsideWenatExpand: false,
      inviteNewParticipantModalOpen: false,
      pageNoParticipantsNewApplication: 0,
      pageNoParticipantsPendingInvitation: 0,
      pageNoParticipantsDetails: 0,
      newApplication: [],
      pendingInvitation: [],
      participantList: [],
      mailList: [],
      mailInput: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userId').then((value) => {
      value && this.setState({userId: value}, () => this.getProjectBySlug());
    });

    AsyncStorage.getItem('userData').then((value) => {
      let objValue = JSON.parse(value);
      objValue.type === 'COMPANY' && this.setState({isCompany: true});
    });
  }

  changeState = (value) => {
    this.setState(value);
  };

  changeFun = () => {
    this.setState(
      {
        pageNoParticipantsNewApplication: 0,
        pageNoParticipantsPendingInvitation: 0,
        pageNoParticipantsDetails: 0,
        newApplication: [],
        pendingInvitation: [],
        participantList: [],
      },
      () => {
        this.getProjectBySlug();
      },
    );
  };

  navigateToMyProject = () => {
    this.setState(
      {
        projectDetailsModalOpen: false,
      },
      () => {
        this.props.navigation.navigate('MyProject');
      },
    );
  };

  navigate = (value, params) => {
    return this.props.navigation.navigate(value, params);
  };

  navigation = (value, params) => {
    this.setState(
      {projectDetailsModalOpen: false, peopleSharedModalOpen: false},
      () => this.props.navigation.navigate(value, params),
    );
  };

  goToParticipants = () => {
    this.setState({
      projectDetailsModalOpen: false,
      selectedTab: 'participants',
    });
  };
  goToContributors = () => {
    this.setState({
      projectDetailsModalOpen: false,
      selectedTab: 'contributions',
    });
  };

  getProjectBySlug = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/find-project-description-details-by-userId-and-slug/' +
        this.state.userId +
        '/' +
        this.state.slug,
      withCredentials: true,
    })
      .then((response) => {
        this.setState(
          {
            projDetails: response.data.body,
            coverImage: response.data.body.project?.coverImage,
          },
          () => {
            this.getParticipantDetails();
            this.getParticipantNewApplication();
            this.getParticipantPendingInvitation();
          },
        );
      })
      .catch((err) => console.log(err));
  };

  // ********** update cover image api starts *************

  openPicker = () => {
    ImagePicker.openPicker({
      width: windowWidth * 0.9,
      height: 220,
      cropping: true,
      mediaType: 'photo',
    }).then((image) => {
      console.log(image);
      this.onCropComplete(image);
    });
  };

  onCropComplete = (crop) => {
    this.setState({coverImage: crop.path});

    let photo = {
      uri: crop.path,
      type: crop.mime,
      name: crop.path.split('-').pop(),
    };

    const formData = new FormData();

    formData.append('coverImage', photo);
    formData.append('projectId', this.state.projDetails.project.id);
    formData.append('type', this.state.projDetails.project.type);

    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL + '/backend/participation/update/cover/image',
      headers: {'Content-Type': 'multipart/form-data'},
      data: formData,
      withCredentials: true,
    })
      .then((response) => {
        let res = response.data;
        console.log('update cover: ', res);
        if (res.message === 'Success!') {
          Snackbar.show({
            backgroundColor: COLORS.primarydark,
            text: `Cover Image updated successfully`,
            textColor: COLORS.altgreen_100,
            duration: Snackbar.LENGTH_LONG,
          });
        }
      })
      .catch((err) => {
        console.log('error in update cover: ', err);
      });
  };

  // *********** update cover image api ends ***************

  getParticipantNewApplication = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/find-project-pending-participants-details-by-projectId/' +
        this.state.projDetails.project.id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        this.state.pageNoParticipantsNewApplication +
        '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          newApplication: this.state.newApplication.concat(
            response.data.body.content,
          ),
        });
      })
      .catch((err) => console.log(err));
  };

  getParticipantPendingInvitation = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/find-offline-project-pending-participants-details-by-projectId/' +
        this.state.projDetails.project.id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        this.state.pageNoParticipantsPendingInvitation +
        '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          pendingInvitation: this.state.pendingInvitation.concat(
            response.data.body.content,
          ),
        });
      })
      .catch((err) => console.log(err));
  };

  getParticipantDetails = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/participants/find-project-participants-details-by-projectId/' +
        this.state.projDetails.project.id +
        '?userId=' +
        this.state.userId +
        '&page=' +
        this.state.pageNoParticipantsDetails +
        '&size=10',
      withCredentials: true,
    })
      .then((response) => {
        this.setState({
          participantList: this.state.participantList.concat(
            response.data.body.content,
          ),
        });
      })
      .catch((err) => console.log(err));
  };

  verifyOutsideWenat = (value) => {
    validateEmail(value)
      ? axios({
          method: 'get',
          url:
            REACT_APP_userServiceURL +
            '/participants/verify/wenatmember/?emails=' +
            this.state.mailInput,
          withCredentials: true,
        })
          .then((response) => {
            if (response.data.body.length > 0) {
              this.setState({mailInput: ''});
              Snackbar.show({
                backgroundColor: '#B22222',
                text: `${value.trim()} is already a member at WeNaturalists`,
                duration: Snackbar.LENGTH_LONG,
              });
            } else {
              this.setState({
                mailList: [...this.state.mailList, value.trim()],
                mailInput: '',
              });
            }
          })
          .catch((err) => console.log(err))
      : Snackbar.show({
          backgroundColor: '#B22222',
          text: `Please enter a valid email`,
          duration: Snackbar.LENGTH_LONG,
        });
  };

  sendInviteOutsideWenat = () => {
    let postBody = {
      emails: this.state.mailList,
      invitationType: 'PARTICIPATION',
      approverId: this.state.userId,
      projectId: this.state.projDetails.project.id,
    };
    this.state.mailList.length > 0
      ? axios({
          method: 'post',
          url: REACT_APP_userServiceURL + '/backend/participation/outside/save',
          data: postBody,
          withCredentials: true,
        })
          .then((response) => {
            this.setState({
              mailList: [],
              mailInput: '',
            });
            Snackbar.show({
              backgroundColor: COLORS.primarydark,
              text: `Invite sent successfully`,
              textColor: COLORS.altgreen_100,
              duration: Snackbar.LENGTH_LONG,
            });
          })
          .catch((err) =>
            Snackbar.show({
              backgroundColor: '#B22222',
              text: err.response.data.message,
              duration: Snackbar.LENGTH_LONG,
            }),
          )
      : Snackbar.show({
          backgroundColor: '#B22222',
          text: `Please enter email addresses to send invite`,
          duration: Snackbar.LENGTH_LONG,
        });
  };

  cancelInvite = (id) => {
    let postBody = {
      projectId: this.state.projDetails.project.id,
      id: id,
    };
    axios({
      method: 'post',
      url:
        REACT_APP_userServiceURL +
        '/backend/participation/offline/delete/request',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let tempindex = this.state.pendingInvitation.findIndex(
          (x) => x.id === id,
        );
        this.state.pendingInvitation.splice(tempindex, 1);
        this.setState({
          pendingInvitation: this.state.pendingInvitation,
        });
      })
      .catch((err) => console.log(err));
  };

  cancelInviteAlert = (id) => {
    Alert.alert('', 'Are you sure you want to withdraw this invite?', [
      {
        text: 'YES',
        onPress: () => this.cancelInvite(id),
        style: 'cancel',
      },
      {
        text: 'NO',
        // onPress: () => console.log('user cancelled deletion'),
      },
    ]);
  };

  acceptOrIgnoreParticipantAlert = (id, otherParticipatingUserId, type) => {
    Alert.alert('', `Are you sure you want to ${type} this request?`, [
      {
        text: 'YES',
        onPress: () =>
          this.acceptIgnoreParticipant(id, otherParticipatingUserId, type),
        style: 'cancel',
      },
      {
        text: 'NO',
        // onPress: () => console.log('user cancelled deletion'),
      },
    ]);
  };

  acceptIgnoreParticipant = (id, otherParticipatingUserId, type) => {
    let postBody = {
      id: id,
      otherParticipatingUserId: otherParticipatingUserId,
      projectId: this.state.projDetails?.project?.id,
      approverId: this.state.userId,
      stage: type === 'accept' ? 'PUBLISHED' : 'DENIED',
    };
    axios({
      method: 'post',
      url: REACT_APP_userServiceURL + '/participants/update',
      data: postBody,
      withCredentials: true,
    })
      .then((response) => {
        let tempindex = this.state.newApplication.findIndex((x) => x.id === id);
        this.state.newApplication.splice(tempindex, 1);
        this.setState({
          newApplication: this.state.newApplication,
        });
      })
      .catch((err) => console.log(err));
  };

  inviteNewParticipantModal = () => {
    return (
      <Modal
        visible={this.state.inviteNewParticipantModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({inviteNewParticipantModalOpen: false})
            }>
            <Icon name="Cross" size={13} color={COLORS.dark_600} />
          </TouchableOpacity>

          <InviteNew
            type="Participants"
            userId={this.state.userId}
            projectId={this.state.projDetails?.project?.id}
            projectCreatorId={this.state.projDetails?.project?.creatorUserId}
            projectType={this.state.projDetails?.project?.type}
            partyType={this.state.projDetails?.userType}
            projectTitle={this.state.projDetails?.project?.title}
            changeFun={this.changeFun}
          />
        </View>
      </Modal>
    );
  };

  peopleSharedModal = () => {
    return (
      <Modal
        visible={this.state.peopleSharedModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View
          style={{
            marginTop: 'auto',
            height: '80%',
            position: 'absolute',
            top: 360,
            width: '100%',
          }}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 300}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() =>
              this.setState({peopleSharedModalOpen: false, peopleShared: []})
            }>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Who Shared this post
              </Text>
            </View>

            <SharedUserList
              id={this.state.projDetails?.project?.id}
              navigation={this.navigation}
            />
          </View>
        </View>
      </Modal>
    );
  };

  projectDetailsModal = () => {
    let {projDetails} = this.state;
    return (
      <Modal
        visible={this.state.projectDetailsModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({projectDetailsModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <ProjectView
            projDetails={projDetails}
            slug={projDetails?.project?.slug}
            userId={this.state.userId}
            isCompany={this.state.isCompany}
            goToParticipants={this.goToParticipants}
            goToContributors={this.goToContributors}
            navigation={this.navigation}
            changeFun={this.navigateToMyProject}
            seeWhoShared={() =>
              this.setState({
                projectDetailsModalOpen: false,
                peopleSharedModalOpen: true,
              })
            }
          />
        </View>
      </Modal>
    );
  };

  postModal = () => {
    return (
      <Modal
        visible={this.state.postModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={[defaultShape.Linear_Gradient_View, {bottom: 100}]}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({postModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View
            style={[
              defaultShape.Modal_Categories_Container,
              {
                paddingTop: 10,
                height: 140,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              },
            ]}>
            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewFeedsPost', {link: false}),
                );
              }}>
              {/* <Icon name="Add_Post" size={44} color={COLORS.primarygreen} style={{ alignSelf: 'center' }} /> */}
              <Image style={{alignSelf: 'center'}} source={postIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    alignSelf: 'center',
                    position: 'absolute',
                    top: 50,
                  },
                ]}>
                Post
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewBlogPost'),
                );
              }}>
              {/* <Icon name='Blog' size={44} color={COLORS.primarygreen} /> */}
              <Image style={{alignSelf: 'center'}} source={blogIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    position: 'absolute',
                    top: 47,
                  },
                ]}>
                Blog
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{justifyContent: 'center', alignItems: 'center'}}
              activeOpacity={0.6}
              onPress={() => {
                this.setState({postModalOpen: false}, () =>
                  this.props.navigation.navigate('NewFeedsPost', {link: true}),
                );
              }}>
              {/* <Icon name='Link_Post' size={44} color={COLORS.primarygreen} /> */}
              <Image style={{alignSelf: 'center'}} source={linkIcon} />
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    textAlign: 'center',
                    position: 'absolute',
                    top: 50,
                  },
                ]}>
                Link
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  verifyReported = () => {
    axios({
      method: 'get',
      url:
        REACT_APP_userServiceURL +
        '/backend/reportabuse/verifyAlreadyReported?reporterId=' +
        this.state.userId +
        '&entityId=' +
        this.state.projDetails?.project?.id,
      withCredentials: true,
    })
      .then((response) => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.body
        ) {
          // console.log(response.data.body.reported)
          response.data.body.reported
            ? Snackbar.show({
                backgroundColor: '#B22222',
                text: 'Your report request was already taken',
                duration: Snackbar.LENGTH_LONG,
              })
            : this.setState({reasonForReportingModalOpen: true});
        } else {
          // console.log(response)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  reasonForReportingModal = () => {
    return (
      <Modal
        visible={this.state.reasonForReportingModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <Report
          changeState={this.changeState}
          entityId={this.state.projDetails?.project?.id}
          entityType={this.state.projDetails?.project?.type}
        />
      </Modal>
    );
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          REACT_APP_domainUrl + '/project/feeds/' + this.state.projDetails.slug,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // console.log('shared with activity type of result.activityType')
        } else {
          // console.log('shared')
        }
      } else if (result.action === Share.dismissedAction) {
        // console.log('dismissed')
      }
    } catch (error) {
      // console.log(error.message)
    }
  };

  shareModal = () => {
    return (
      <Modal
        visible={this.state.shareModalOpen}
        transparent
        animationType="slide"
        supportedOrientations={['portrait', 'landscape']}>
        <View style={{marginTop: 'auto'}}>
          <View style={defaultShape.Linear_Gradient_View}>
            <LinearGradient
              colors={[
                COLORS.dark_800 + '00',
                COLORS.dark_800 + 'CC',
                COLORS.dark_800,
              ]}
              style={defaultShape.Linear_Gradient}></LinearGradient>
          </View>

          <TouchableOpacity
            activeOpacity={0.7}
            style={defaultShape.CloseBtn}
            onPress={() => this.setState({shareModalOpen: false})}>
            <Icon
              name="Cross"
              size={13}
              color={COLORS.dark_600}
              style={styles.crossIcon}
            />
          </TouchableOpacity>

          <View style={defaultShape.Modal_Categories_Container}>
            <View
              style={[
                defaultShape.ActList_Cell_Gylph_Alt,
                {justifyContent: 'center', paddingBottom: 10, paddingTop: 0},
              ]}>
              <Text style={[typography.Caption, {color: COLORS.altgreen_400}]}>
                Share
              </Text>
            </View>

            <TouchableOpacity
              style={
                Platform.OS === 'ios'
                  ? [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {paddingTop: 25, paddingBottom: 15},
                    ]
                  : defaultShape.ActList_Cell_Gylph_Alt
              }
              activeOpacity={0.6}
              onPress={() => {
                this.setState({shareModalOpen: false}, () =>
                  this.props.navigation.navigate('RepostOnWenat', {
                    link: false,
                    pressedActivityId: this.state.projDetails?.project?.id,
                    entityType: this.state.projDetails?.project?.type,
                    type: 'PROJECT',
                  }),
                );
              }}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Repost on WeNaturalists
              </Text>
              <Icon
                name="Forward"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                Clipboard.setString(
                  REACT_APP_domainUrl +
                    '/project/feeds/' +
                    this.state.projDetails.slug,
                );
                Snackbar.show({
                  backgroundColor: '#97A600',
                  text: 'Link Copied',
                  textColor: '#00394D',
                  duration: Snackbar.LENGTH_LONG,
                });
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [defaultShape.ActList_Cell_Gylph_Alt]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Copy link to post
              </Text>
              <Icon
                name="TxEdi_AddLink"
                size={17}
                color={COLORS.altgreen_300}
                style={Platform.OS === 'android' ? {marginTop: 8} : {}}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.setState({shareModalOpen: false}, () => this.onShare());
              }}
              style={
                Platform.OS === 'ios'
                  ? [defaultShape.ActList_Cell_Gylph_Alt, {paddingVertical: 15}]
                  : [
                      defaultShape.ActList_Cell_Gylph_Alt,
                      {borderBottomWidth: 0},
                    ]
              }
              activeOpacity={0.6}>
              <Text style={[typography.Button_Lead, {color: COLORS.dark_600}]}>
                Share via others
              </Text>

              <View
                style={[
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    width: 100,
                    marginRight: -6,
                  },
                ]}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_FB"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_Twitter"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Social_LinkedIn"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>

                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                    name="Meatballs"
                    size={14}
                    color={COLORS.dark_600}
                    style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  listHeaderComponent = () => {
    let {projDetails, selectedTab} = this.state;
    return (
      <>
        <ImageBackground
          style={styles.coverImage}
          source={
            this.state.coverImage && this.state.coverImage !== ''
              ? {uri: this.state.coverImage}
              : projectDefault
          }>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={[
              defaultShape.AdjunctBtn_Small_Sec,
              {position: 'absolute', top: 10, left: 10},
            ]}>
            <Icon
              name="Arrow-Left"
              color={COLORS.dark_600}
              size={14}
              style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
            />
          </TouchableOpacity>

          {projDetails?.project?.creatorUserId !== this.state.userId && (
            <TouchableOpacity
              onPress={this.verifyReported}
              style={[
                defaultShape.AdjunctBtn_Small_Sec,
                {position: 'absolute', top: 10, right: 75},
              ]}>
              <Icon
                name="ReportComment_OL"
                color={COLORS.dark_600}
                size={14}
                style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
              />
            </TouchableOpacity>
          )}

          {projDetails.stage !== 'CANCELLED' && (
            <TouchableOpacity
              onPress={() => this.setState({shareModalOpen: true})}
              style={[
                defaultShape.AdjunctBtn_Small_Sec,
                {position: 'absolute', top: 10, right: 115},
              ]}>
              <Icon
                name="Share"
                color={COLORS.dark_600}
                size={14}
                style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
              />
            </TouchableOpacity>
          )}

          {projDetails.canChangeImage && projDetails.stage !== 'CANCELLED' && (
            <TouchableOpacity
              onPress={this.openPicker}
              style={[
                defaultShape.AdjunctBtn_Small_Sec,
                {position: 'absolute', top: 10, right: 75},
              ]}>
              <Icon
                name="UploadPhoto"
                color={COLORS.dark_600}
                size={14}
                style={{marginTop: Platform.OS === 'android' ? 5 : 0}}
              />
            </TouchableOpacity>
          )}

          {projDetails.stage !== 'CANCELLED' && (
            <TouchableOpacity
              onPress={() => this.setState({projectDetailsModalOpen: true})}
              style={styles.viewProj}>
              <Text style={[typography.Caption, {color: COLORS.dark_800}]}>
                VIEW
              </Text>
            </TouchableOpacity>
          )}

          <View
            style={{
              width: '100%',
              height: 100,
              position: 'absolute',
              bottom: 0,
              alignSelf: 'center',
            }}>
            <LinearGradient
              colors={['#154A5900', '#154A59CC', '#154A59']}
              style={{
                flex: 1,
                paddingLeft: 15,
                paddingRight: 15,
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6,
                justifyContent: 'flex-end',
              }}>
              <Text style={[typography.H4, {color: COLORS.white}]}>
                {projDetails.project && projDetails.project.title}
              </Text>

              {projDetails.stage !== 'CANCELLED' ? (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 10,
                  }}>
                  <Text
                    style={[
                      typography.Note2,
                      {color: COLORS.altgreen_300, textTransform: 'capitalize'},
                    ]}>
                    Start Date
                  </Text>
                  <Text
                    style={[
                      typography.Note2,
                      {color: COLORS.white, marginLeft: 5},
                    ]}>
                    {projDetails.project &&
                      moment
                        .unix(projDetails.project.beginningTime / 1000)
                        .format('DD MMM YYYY')}
                  </Text>

                  <Text
                    style={[
                      typography.Note2,
                      {
                        color: COLORS.altgreen_300,
                        textTransform: 'capitalize',
                        marginLeft: 5,
                      },
                    ]}>
                    | End Date
                  </Text>
                  <Text
                    style={[
                      typography.Note2,
                      {color: COLORS.white, marginLeft: 5},
                    ]}>
                    {projDetails.project &&
                    projDetails?.project?.endingTime !== 0
                      ? moment
                          .unix(projDetails.project.endingTime / 1000)
                          .format('DD MMM YYYY')
                      : 'NA'}
                  </Text>

                  <Text
                    style={[
                      typography.Caption,
                      {
                        color: COLORS.green_500,
                        textTransform: 'capitalize',
                        marginLeft: 5,
                      },
                    ]}>
                    | {projDetails.stage && projDetails.stage}
                  </Text>
                </View>
              ) : (
                <Text
                  style={[
                    typography.Caption,
                    {
                      color: COLORS.green_500,
                      marginBottom: 10,
                    },
                  ]}>
                  Project was cancelled on{' '}
                  {moment
                    .unix(projDetails.demolishedDate / 1000)
                    .format('Do MMM YYYY')}
                </Text>
              )}
            </LinearGradient>
          </View>
        </ImageBackground>

        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{
            flexDirection: 'row',
            width: '100%',
            backgroundColor: COLORS.white,
          }}>
          <TouchableOpacity
            style={{width: 70}}
            onPress={() => this.setState({selectedTab: 'about'})}>
            <Text
              style={[
                selectedTab === 'about'
                  ? typography.Body_1_bold
                  : typography.Caption,
                {
                  color:
                    selectedTab === 'about'
                      ? COLORS.dark_800
                      : COLORS.altgreen_400,
                  textAlign: 'center',
                  marginBottom: 6,
                  paddingTop: 8,
                },
              ]}>
              ABOUT
            </Text>
            <View
              style={{
                width: 70,
                height: 5,
                backgroundColor:
                  selectedTab === 'about' ? COLORS.dark_800 : COLORS.NoColor,
                borderTopLeftRadius: 4,
                borderTopRightRadius: 4,
              }}></View>
          </TouchableOpacity>

          {projDetails.stage !== 'CANCELLED' && (
            <TouchableOpacity
              style={{paddingTop: 8, width: 70, marginLeft: 5}}
              onPress={() =>
                this.setState({
                  selectedTab: 'feeds',
                })
              }>
              <Text
                style={[
                  selectedTab === 'feeds'
                    ? typography.Body_1_bold
                    : typography.Caption,
                  {
                    color:
                      selectedTab === 'feeds'
                        ? COLORS.dark_800
                        : COLORS.altgreen_400,
                    marginBottom: 6,
                    textAlign: 'center',
                  },
                ]}>
                FEEDS
              </Text>
              <View
                style={{
                  width: 70,
                  height: 5,
                  backgroundColor:
                    selectedTab === 'feeds' ? COLORS.dark_800 : COLORS.NoColor,
                  borderTopLeftRadius: 4,
                  borderTopRightRadius: 4,
                }}></View>
            </TouchableOpacity>
          )}

          {projDetails.participantTabVisible &&
            projDetails.stage !== 'CANCELLED' &&
            projDetails?.project?.type !== 'FUNDRAISE' && (
              <TouchableOpacity
                style={{paddingTop: 8, width: 125, marginLeft: 5}}
                onPress={() => this.setState({selectedTab: 'participants'})}>
                <Text
                  style={[
                    selectedTab === 'participants'
                      ? typography.Body_1_bold
                      : typography.Caption,
                    {
                      color:
                        selectedTab === 'participants'
                          ? COLORS.dark_800
                          : COLORS.altgreen_400,
                      marginBottom: 6,
                      textAlign: 'center',
                    },
                  ]}>
                  PARTICIPANTS
                </Text>
                <View
                  style={{
                    width: 125,
                    height: 5,
                    backgroundColor:
                      selectedTab === 'participants'
                        ? COLORS.dark_800
                        : COLORS.NoColor,
                    borderTopLeftRadius: 4,
                    borderTopRightRadius: 4,
                  }}></View>
              </TouchableOpacity>
            )}

          {projDetails?.project?.type === 'FUNDRAISE' &&
            projDetails.stage !== 'CANCELLED' && (
              <TouchableOpacity
                style={{paddingTop: 8, width: 125, marginLeft: 5}}
                onPress={() => this.setState({selectedTab: 'contributions'})}>
                <Text
                  style={[
                    selectedTab === 'contributions'
                      ? typography.Body_1_bold
                      : typography.Caption,
                    {
                      color:
                        selectedTab === 'contributions'
                          ? COLORS.dark_800
                          : COLORS.altgreen_400,
                      marginBottom: 6,
                      textAlign: 'center',
                    },
                  ]}>
                  CONTRIBUTIONS
                </Text>
                <View
                  style={{
                    width: 125,
                    height: 5,
                    backgroundColor:
                      selectedTab === 'contributions'
                        ? COLORS.dark_800
                        : COLORS.NoColor,
                    borderTopLeftRadius: 4,
                    borderTopRightRadius: 4,
                  }}></View>
              </TouchableOpacity>
            )}

          {projDetails.guestFacultyTabVisible &&
            projDetails.stage !== 'CANCELLED' && (
              <TouchableOpacity
                style={{paddingTop: 8, width: 70, marginLeft: 5}}
                onPress={() => this.setState({selectedTab: 'invitee'})}>
                <Text
                  style={[
                    selectedTab === 'invitee'
                      ? typography.Body_1_bold
                      : typography.Caption,
                    {
                      color:
                        selectedTab === 'invitee'
                          ? COLORS.dark_800
                          : COLORS.altgreen_400,
                      marginBottom: 6,
                      textAlign: 'center',
                    },
                  ]}>
                  INVITEE
                </Text>
                <View
                  style={{
                    width: 70,
                    height: 5,
                    backgroundColor:
                      selectedTab === 'invitee'
                        ? COLORS.dark_800
                        : COLORS.NoColor,
                    borderTopLeftRadius: 4,
                    borderTopRightRadius: 4,
                  }}></View>
              </TouchableOpacity>
            )}

          {projDetails.adminTabShow && projDetails.stage !== 'CANCELLED' && (
            <TouchableOpacity
              style={{paddingTop: 8, width: 70, marginLeft: 5}}
              onPress={() => this.setState({selectedTab: 'admin'})}>
              <Text
                style={[
                  selectedTab === 'admin'
                    ? typography.Body_1_bold
                    : typography.Caption,
                  {
                    color:
                      selectedTab === 'admin'
                        ? COLORS.dark_800
                        : COLORS.altgreen_400,
                    marginBottom: 6,
                    textAlign: 'center',
                  },
                ]}>
                ADMIN
              </Text>
              <View
                style={{
                  width: 70,
                  height: 5,
                  backgroundColor:
                    selectedTab === 'admin' ? COLORS.dark_800 : COLORS.NoColor,
                  borderTopLeftRadius: 4,
                  borderTopRightRadius: 4,
                }}></View>
            </TouchableOpacity>
          )}

          {projDetails.stage === 'completed' && (
            <TouchableOpacity
              style={{paddingTop: 8, width: 70, marginLeft: 5, marginRight: 10}}
              onPress={() => this.setState({selectedTab: 'feedback'})}>
              <Text
                style={[
                  selectedTab === 'feedback'
                    ? typography.Body_1_bold
                    : typography.Caption,
                  {
                    color:
                      selectedTab === 'feedback'
                        ? COLORS.dark_800
                        : COLORS.altgreen_400,
                    marginBottom: 6,
                    textAlign: 'center',
                  },
                ]}>
                FEEDBACK
              </Text>
              <View
                style={{
                  width: 70,
                  height: 5,
                  backgroundColor:
                    selectedTab === 'feedback'
                      ? COLORS.dark_800
                      : COLORS.NoColor,
                  borderTopLeftRadius: 4,
                  borderTopRightRadius: 4,
                }}></View>
            </TouchableOpacity>
          )}
        </ScrollView>
      </>
    );
  };

  aboutSection = () => {
    let {projDetails} = this.state;
    return (
      <>
        <View
          style={{
            backgroundColor: COLORS.dark_800,
            paddingVertical: 10,
          }}>
          <View>
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.white,
                  marginVertical: 10,
                  lineHeight: 16,
                  alignSelf: 'center',
                },
              ]}>
              Brief Description
            </Text>
            <Text
              onPress={() => this.setState({readMore: !this.state.readMore})}
              numberOfLines={
                this.state.setreadmore === false
                  ? null
                  : this.state.readMore
                  ? 1000
                  : 5
              }
              onTextLayout={(e) =>
                e.nativeEvent.lines.length < 5
                  ? this.setState({setreadmore: false})
                  : this.setState({setreadmore: true})
              }
              style={[
                typography.Body_1,
                {
                  color: COLORS.altgreen_300,
                  marginHorizontal: 10,
                  lineHeight: 16,
                },
              ]}>
              {projDetails.project && projDetails.project.shortBrief}
            </Text>
            {this.state.setreadmore && (
              <TouchableOpacity
                style={{height: 30, width: 80, marginLeft: 10}}
                onPress={() => this.setState({readMore: !this.state.readMore})}>
                <Text style={{color: COLORS.green_500, fontWeight: '700'}}>
                  {this.state.readMore ? 'Read less' : 'Read more'}
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>

        {projDetails.noOfParticipants > 0 && (
          <View
            style={{
              backgroundColor: COLORS.dark_700,
              paddingVertical: 5,
              paddingLeft: 5,
              justifyContent: 'center',
            }}>
            <View
              style={{flexDirection: 'row', height: 30, alignItems: 'center'}}>
              <Icon
                name="Participants"
                color={COLORS.altgreen_300}
                size={14}
                style={{marginLeft: 10}}
              />
              <Text
                style={[
                  typography.Caption,
                  {
                    color: COLORS.altgreen_200,
                    marginLeft: 5,
                  },
                ]}>
                {projDetails.noOfParticipants}{' '}
                {projDetails.noOfParticipants > 1 ? 'Applicants' : 'Applicant'}
              </Text>
            </View>
          </View>
        )}

        <View style={styles.creatorCardView}>
          <View
            style={{
              width: '100%',
              height: 100,
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
            }}>
            <Image
              source={
                projDetails.creatorBannerImage
                  ? {uri: projDetails.creatorBannerImage}
                  : defaultCover
              }
              style={{
                width: '100%',
                height: '100%',
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
              }}
            />
          </View>
          <Text
            style={[
              typography.Caption,
              {color: COLORS.altgreen_400, marginTop: 15},
            ]}>
            Posted By
          </Text>
          <TouchableOpacity
            onPress={() => {
              projDetails.userType === 'INDIVIDUAL' &&
              projDetails.userId === this.state.userId
                ? this.props.navigation.navigate('ProfileStack')
                : projDetails.userType === 'INDIVIDUAL' &&
                  projDetails.userId !== this.state.userId
                ? this.props.navigation.navigate('ProfileStack', {
                    screen: 'OtherProfileScreen',
                    params: {userId: projDetails.userId},
                  })
                : projDetails.userType === 'COMPANY'
                ? this.props.navigation.navigate('ProfileStack', {
                    screen: 'CompanyProfileScreen',
                    params: {userId: projDetails.userId},
                  })
                : this.props.navigation.navigate('CircleProfileStack', {
                    screen: 'CircleProfile',
                    params: {slug: projDetails.customUrl},
                  });
            }}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
              marginBottom: 15,
            }}>
            <Image
              source={
                projDetails.imageUrl
                  ? {uri: projDetails.imageUrl}
                  : !this.state.isCompany &&
                    projDetails.userType === 'INDIVIDUAL'
                  ? defaultProfile
                  : this.state.isCompany || projDetails.userType === 'COMPANY'
                  ? defaultBusiness
                  : defaultCircle
              }
              style={{
                width: 40,
                height: 40,
                borderRadius: 20,
              }}
            />
            <View style={{justifyContent: 'center'}}>
              <Text
                numberOfLines={1}
                style={[
                  typography.Button_Lead,
                  {color: COLORS.dark_800, marginLeft: 10, maxWidth: 220},
                ]}>
                {projDetails.creatorName && projDetails.creatorName}
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Icon
                  name="Location"
                  color={COLORS.altgreen_400}
                  size={14}
                  style={{marginLeft: 10}}
                />
                <Text
                  style={[
                    typography.Note2,
                    {color: COLORS.grey_400, marginLeft: 2},
                  ]}>
                  {projDetails.project &&
                    projDetails.project.location &&
                    projDetails.project.location.city +
                      ', ' +
                      projDetails.project.location.state +
                      ', ' +
                      projDetails.project.location.country}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </>
    );
  };

  participantSection = () => {
    return (
      <View>
        {/* ******************** New Applications Starts ******************* */}

        {this.state.newApplication.length > 0 && (
          <TouchableOpacity
            onPress={() =>
              this.setState({
                newApplicationExpand: !this.state.newApplicationExpand,
                pendingInvitationExpand: false,
                inviteOutsideWenatExpand: false,
                // newApplication: [],
              })
            }
            style={{
              paddingHorizontal: 15,
              paddingVertical: 8,
              backgroundColor: COLORS.altgreen_200,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_800,
                  marginLeft: 6,
                  marginRight: 17,
                  marginVertical: 8,
                },
              ]}>
              New Applications
            </Text>
            <Icon
              name={this.state.newApplicationExpand ? 'Arrow_Up' : 'Arrow_Down'}
              size={12}
              color={COLORS.dark_800}
              style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
            />
          </TouchableOpacity>
        )}
        {this.state.newApplicationExpand && (
          <View
            style={{
              paddingHorizontal: 15,
              paddingBottom: 8,
              backgroundColor: COLORS.altgreen_200,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={this.state.newApplication}
              keyExtractor={(item) => item.id}
              renderItem={(item) => (
                <View style={styles.renderCard}>
                  <TouchableOpacity
                    onPress={() => {
                      item.item.userType === 'INDIVIDUAL' &&
                      item.item.userId === this.state.userId
                        ? this.props.navigation.navigate('ProfileStack')
                        : item.item.userType === 'INDIVIDUAL' &&
                          item.item.userId !== this.state.userId
                        ? this.props.navigation.navigate('ProfileStack', {
                            screen: 'OtherProfileScreen',
                            params: {userId: item.item.userId},
                          })
                        : item.item.userType === 'COMPANY'
                        ? this.props.navigation.navigate('ProfileStack', {
                            screen: 'CompanyProfileScreen',
                            params: {userId: item.item.userId},
                          })
                        : this.props.navigation.navigate('CircleProfileStack', {
                            screen: 'CircleProfile',
                            params: {slug: item.item.customUrl},
                          });
                    }}
                    style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      style={{height: 40, width: 40, borderRadius: 20}}
                      source={
                        item.item.profileImage
                          ? {uri: item.item.profileImage}
                          : item.item.userType === 'COMPANY'
                          ? defaultBusiness
                          : defaultProfile
                      }
                    />
                    <View style={{marginLeft: 6}}>
                      <Text
                        numberOfLines={1}
                        style={[
                          typography.Caption,
                          {
                            color: COLORS.dark_800,
                            maxWidth: 160,
                          },
                        ]}>
                        {item.item.userName}
                      </Text>
                      <Text
                        numberOfLines={1}
                        style={[
                          typography.Note2,
                          {
                            color: COLORS.altgreen_400,
                            maxWidth: 100,
                          },
                        ]}>
                        {item.item.country}
                      </Text>
                      <Text
                        style={[
                          typography.Note2,
                          {
                            color: COLORS.altgreen_400,
                          },
                        ]}>
                        {item.item.persona}
                      </Text>
                    </View>
                  </TouchableOpacity>

                  {this.state.projDetails.stage !== 'completed' && (
                    <View
                      style={{
                        marginLeft: 15,
                      }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.acceptOrIgnoreParticipantAlert(
                            item.item.id,
                            item.item.otherParticipantUserId,
                            'accept',
                          )
                        }
                        style={{
                          backgroundColor: COLORS.green_500,
                          borderRadius: 8,
                          height: 25,
                          width: 60,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={[
                            typography.Note2,
                            {
                              color: COLORS.white,
                            },
                          ]}>
                          ACCEPT
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() =>
                          this.acceptOrIgnoreParticipantAlert(
                            item.item.id,
                            item.item.otherParticipantUserId,
                            'ignore',
                          )
                        }
                        style={{
                          height: 25,
                          width: 60,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={[
                            typography.Note2,
                            {
                              color: COLORS.grey_400,
                            },
                          ]}>
                          IGNORE
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )}
                </View>
              )}
            />
          </View>
        )}

        {/* ******************** New Applications Ends ******************* */}

        {/* ******************** Pending Inviations Starts ******************* */}

        {this.state.pendingInvitation.length > 0 &&
          this.state.projDetails.pendingInvitationsVisible && (
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  newApplicationExpand: false,
                  pendingInvitationExpand: !this.state.pendingInvitationExpand,
                  inviteOutsideWenatExpand: false,
                  // pendingInvitation: [],
                })
              }
              style={{
                marginTop: 12,
                paddingHorizontal: 15,
                paddingBottom: 8,
                backgroundColor: COLORS.altgreen_200,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text
                style={[
                  typography.Button_Lead,
                  {
                    color: COLORS.dark_800,
                    marginLeft: 6,
                    marginRight: 17,
                    marginVertical: 8,
                  },
                ]}>
                Pending Invitations
              </Text>
              <Icon
                name={
                  this.state.pendingInvitationExpand ? 'Arrow_Up' : 'Arrow_Down'
                }
                size={12}
                color={COLORS.dark_800}
                style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
              />
            </TouchableOpacity>
          )}
        {this.state.pendingInvitationExpand &&
          this.state.projDetails.pendingInvitationsVisible && (
            <View
              style={{
                paddingHorizontal: 15,
                paddingBottom: 8,
                backgroundColor: COLORS.altgreen_200,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.pendingInvitation}
                keyExtractor={(item) => item.id}
                renderItem={(item) => (
                  <View style={styles.renderCard}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Image
                        style={{height: 40, width: 40, borderRadius: 20}}
                        source={
                          item.item.profileImage
                            ? {uri: item.item.profileImage}
                            : item.item.userType === 'COMPANY'
                            ? defaultBusiness
                            : defaultProfile
                        }
                      />
                      <View style={{marginLeft: 6}}>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Caption,
                            {
                              color: COLORS.dark_800,
                              maxWidth: 160,
                            },
                          ]}>
                          {item.item.userName}
                        </Text>
                        <Text
                          numberOfLines={1}
                          style={[
                            typography.Caption,
                            {
                              color: COLORS.altgreen_400,
                              maxWidth: 160,
                            },
                          ]}>
                          Invited By: {item.item.senderUserName}
                        </Text>
                        <Text
                          style={[
                            typography.Note2,
                            {
                              color: COLORS.dark_800,
                            },
                          ]}>
                          {item.item.country}
                        </Text>
                      </View>
                    </View>

                    {this.state.projDetails.stage !== 'completed' && (
                      <TouchableOpacity
                        onPress={() => this.cancelInviteAlert(item.item.id)}
                        style={{
                          width: 40,
                          alignItems: 'center',
                          justifyContent: 'center',
                          marginLeft: 15,
                        }}>
                        <Text
                          style={[
                            typography.Note2,
                            {
                              color: COLORS.grey_400,
                              textAlign: 'center',
                            },
                          ]}>
                          Cancel Invite
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                )}
              />
            </View>
          )}

        {/* ******************** Pending Invitation Ends ******************* */}

        {/* ******************** Invite friends outside WeNaturalists Starts ******************* */}

        <TouchableOpacity
          onPress={() =>
            this.setState({
              newApplicationExpand: false,
              pendingInvitationExpand: false,
              inviteOutsideWenatExpand: !this.state.inviteOutsideWenatExpand,
              mailList: [],
              mailInput: '',
              // pendingInvitation: [],
            })
          }
          style={{
            marginTop: 12,
            paddingHorizontal: 15,
            paddingVertical: 8,
            backgroundColor: COLORS.altgreen_250,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon
              name="Add_Group"
              size={14}
              color={COLORS.dark_800}
              style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
            />
            <Text
              style={[
                typography.Button_Lead,
                {
                  color: COLORS.dark_800,
                  marginLeft: 6,
                  marginRight: 17,
                  marginVertical: 8,
                },
              ]}>
              Invite friends outside WeNaturalists
            </Text>
          </View>
          <Icon
            name={
              this.state.inviteOutsideWenatExpand ? 'Arrow_Up' : 'Arrow_Down'
            }
            size={12}
            color={COLORS.dark_800}
            style={{marginTop: Platform.OS === 'android' ? 10 : 0}}
          />
        </TouchableOpacity>
        {this.state.inviteOutsideWenatExpand && (
          <View
            style={{
              paddingHorizontal: 15,
              paddingBottom: 8,
              backgroundColor: COLORS.altgreen_250,
              alignItems: 'center',
            }}>
            <View
              style={{
                minHeight: 100,
                width: '90%',
                alignSelf: 'flex-end',
                backgroundColor: COLORS.white,
                borderRadius: 8,
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                paddingLeft: 10,
              }}>
              <FlatList
                keyboardShouldPersistTaps="handled"
                scrollEventThrottle={0}
                ref={(ref) => (this.scrollView = ref)}
                onContentSizeChange={() => {
                  this.scrollView.scrollToEnd({animated: false});
                  this.setState({hashTagModalOpen: true});
                }}
                columnWrapperStyle={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                numColumns={10}
                showsVerticalScrollIndicator={false}
                data={this.state.mailList}
                keyExtractor={(item, index) => item + index}
                renderItem={({item, index}) => (
                  <View
                    style={{
                      marginLeft: 5,
                      marginTop: 5,
                      backgroundColor: COLORS.green_500,
                      borderRadius: 8,
                      paddingVertical: 2,
                      paddingHorizontal: 5,
                    }}>
                    <Text style={[typography.Note2, {color: COLORS.white}]}>
                      {item}
                    </Text>
                  </View>
                )}
              />
              <TextInput
                multiline
                placeholder="You can add upto 10 email addresses"
                style={{
                  height: 100,
                  width: '100%',
                  paddingLeft: 10,
                  paddingTop: 10,
                }}
                onChangeText={(value) => {
                  this.setState({mailInput: value.trim()});

                  value[value.length - 1] === ' ' && value.trim()
                    ? this.verifyOutsideWenat(value)
                    : null;
                }}
                value={this.state.mailInput}
              />
            </View>
            <TouchableOpacity
              onPress={this.sendInviteOutsideWenat}
              style={{
                backgroundColor: COLORS.dark_600,
                borderRadius: 4,
                paddingHorizontal: 10,
                paddingVertical: 6,
                alignSelf: 'flex-end',
                marginTop: 10,
              }}>
              <Text style={[typography.Caption, {color: COLORS.white}]}>
                SEND INVITE
              </Text>
            </TouchableOpacity>
          </View>
        )}

        {/* ******************** Invite friends outside WeNaturalists Ends ******************* */}

        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 15,
            justifyContent: 'space-between',
            marginTop: 15,
            alignItems: 'center',
          }}>
          <Text style={[typography.H6, {color: COLORS.dark_800}]}>
            PARTICIPANTS
          </Text>
          <TouchableOpacity
            onPress={() => this.setState({inviteNewParticipantModalOpen: true})}
            style={{
              backgroundColor: COLORS.altgreen_300,
              borderRadius: 8,
              paddingHorizontal: 10,
              paddingVertical: 6,
              alignSelf: 'flex-end',
              marginTop: 10,
            }}>
            <Text style={[typography.Caption, {color: COLORS.white}]}>
              Invite New Participants
            </Text>
          </TouchableOpacity>
        </View>

        <View style={{marginTop: 10, paddingHorizontal: 15}}>
          <FlatList
            data={this.state.participantList}
            keyExtractor={(item) => item.id}
            renderItem={(item) => (
              <TouchableOpacity
                onPress={() => {
                  item.item.userType === 'INDIVIDUAL' &&
                  item.item.userId === this.state.userId
                    ? this.props.navigation.navigate('ProfileStack')
                    : item.item.userType === 'INDIVIDUAL' &&
                      item.item.userId !== this.state.userId
                    ? this.props.navigation.navigate('ProfileStack', {
                        screen: 'OtherProfileScreen',
                        params: {userId: item.item.userId},
                      })
                    : item.item.userType === 'COMPANY'
                    ? this.props.navigation.navigate('ProfileStack', {
                        screen: 'CompanyProfileScreen',
                        params: {userId: item.item.userId},
                      })
                    : this.props.navigation.navigate('CircleProfileStack', {
                        screen: 'CircleProfile',
                        params: {slug: item.item.customUrl},
                      });
                }}
                style={[
                  styles.renderCard,
                  {
                    marginVertical: 5,
                    marginRight: 0,
                    shadowColor: '#000',
                    shadowOffset: {
                      width: 0,
                      height: 1,
                    },
                    shadowOpacity: 0.2,
                    shadowRadius: 1.41,

                    elevation: 2,
                  },
                ]}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image
                    style={{height: 40, width: 40, borderRadius: 20}}
                    source={
                      item.item.profileImage
                        ? {uri: item.item.profileImage}
                        : defaultProfile
                    }
                  />
                  <View style={{marginLeft: 6}}>
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Caption,
                        {
                          color: COLORS.dark_800,
                          maxWidth: 160,
                        },
                      ]}>
                      {item.item.userName}
                    </Text>
                    <Text
                      numberOfLines={1}
                      style={[
                        typography.Note2,
                        {
                          color: COLORS.altgreen_400,
                          maxWidth: 100,
                        },
                      ]}>
                      {item.item.country}
                    </Text>
                    <Text
                      style={[
                        typography.Note2,
                        {
                          color: COLORS.altgreen_400,
                        },
                      ]}>
                      {item.item.persona}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    );
  };

  render() {
    let {projDetails, selectedTab} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.projectDetailsModal()}
        {this.inviteNewParticipantModal()}
        {this.shareModal()}
        {this.reasonForReportingModal()}
        {this.peopleSharedModal()}
        {this.postModal()}

        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'position' : null}>
          <ScrollView>
            {this.listHeaderComponent()}
            {this.state.selectedTab === 'about' ? (
              this.aboutSection()
            ) : this.state.selectedTab === 'feeds' ? (
              <CommonFeeds
                navigate={this.navigate}
                type="PROJECTFEEDS"
                projectId={this.state.projDetails?.project?.id}
                canPost={this.state.projDetails?.participantTabVisible}
                postModalOpen={() => this.setState({postModalOpen: true})}
              />
            ) : this.state.selectedTab === 'participants' ? (
              this.participantSection()
            ) : this.state.selectedTab === 'contributions' ? (
              <Contribution
                userId={this.state.userId}
                projectId={this.state.projDetails?.project?.id}
                navigation={this.navigation}
              />
            ) : this.state.selectedTab === 'invitee' ? (
              <GuestInvitee
                userId={this.state.userId}
                projectId={this.state.projDetails?.project?.id}
                projectCreatorId={
                  this.state.projDetails?.project?.creatorUserId
                }
                projectType={this.state.projDetails?.project?.type}
                partyType={this.state.projDetails?.userType}
                projectTitle={this.state.projDetails?.project?.title}
              />
            ) : this.state.selectedTab === 'admin' ? (
              <Admin
                userId={this.state.userId}
                projectId={this.state.projDetails?.project?.id}
                projectCreatorId={
                  this.state.projDetails?.project?.creatorUserId
                }
                projectType={this.state.projDetails?.project?.type}
                partyType={this.state.projDetails?.userType}
                projectTitle={this.state.projDetails?.project?.title}
                navigation={this.navigation}
              />
            ) : this.state.selectedTab === 'feedback' ? (
              <FeedBack
                id={this.state.projDetails?.project?.id}
                navigation={this.navigation}
              />
            ) : (
              <></>
            )}
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  coverImage: {
    height: 220,
    width: '100%',
  },
  viewProj: {
    position: 'absolute',
    top: 10,
    right: 10,
    height: 28,
    width: 60,
    borderRadius: 20,
    backgroundColor: COLORS.green_500,
    alignItems: 'center',
    justifyContent: 'center',
  },
  creatorCardView: {
    width: '90%',
    borderRadius: 8,
    marginTop: 15,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.white,
    marginBottom: 40,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  renderSharedPost: {
    width: '94%',
    marginTop: 5,
    marginLeft: 6,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: COLORS.grey_300,
    padding: 10,
  },
  renderItemStyle: {
    backgroundColor: COLORS.white,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginHorizontal: 10,
    marginVertical: 6,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: COLORS.grey_300,
  },
  unreadNotiItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    borderRadius: 8,
  },
  renderCard: {
    paddingHorizontal: 15,
    paddingVertical: 6,
    borderRadius: 4,
    backgroundColor: COLORS.white,
    marginRight: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
