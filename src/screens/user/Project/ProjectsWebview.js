import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Platform,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {WebView} from 'react-native-webview';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
// import LottieView from 'lottie-react-native';

import {
  REACT_APP_userServiceURL,
  REACT_APP_domainUrl,
} from '../../../../env.json';
import {COLORS} from '../../../Components/Shared/Colors';
// import loaderanimation from '../../../lottie/loaderanimation.json';
import icoMoonConfig from '../../../../assets/Icons/selection.json';
import typography from '../../../Components/Shared/Typography';
import defaultShape from '../../../Components/Shared/Shape';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default class ProjectsWebview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: REACT_APP_domainUrl + this.props.route.params.path,
      type: this.props.route.params.type,
    };
  }

  webLoader = () => {
    // return <LottieView source={loaderanimation} autoPlay />;
    return (
      <View
        style={{
          height: '100%',
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size="large" color={COLORS.primarydark} />
      </View>
    );
  };

  //   onAndroidBackPress = () => {
  //     if (this.webView.canGoBack && this.webView.ref && Platform.OS === 'android') {
  //       this.webView.ref.goBack();
  //       return true;
  //     }
  //     return false;
  //   };

  // componentDidMount() {
  //   console.log(this.state.url)
  //   if (Platform.OS === 'android') {
  //     BackHandler.addEventListener(
  //       'hardwareBackPress',
  //       this.onAndroidBackPress,
  //     );
  //   }
  // }

  redirectToNativeScreen = (value) => {
    let screenDetails = JSON.parse(value);

    if (screenDetails.type === 'redirectToProject') {
      this.props.navigation.goBack();
    }
  };

  componentDidMount(){
    console.log("url",this.state.url)
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: COLORS.white,
            paddingTop: 5,
          }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={defaultShape.Nav_Gylph_Btn}>
            <Icon name="Arrow-Left" color={COLORS.altgreen_300} size={16} />
          </TouchableOpacity>

          <View>
            <Text
              style={[
                typography.Button_Lead,
                {color: COLORS.dark_800, maxWidth: 200, marginTop: 10},
              ]}
              numberOfLines={1}>
              {' '}
              {this.state.type && this.state.type === 'edit'
                ? 'Edit'
                : 'Create'}{' '}
              Project{' '}
            </Text>
            {/* <Text style={[typography.Note, { color: COLORS.altgreen_300, maxWidth: 200 }]} numberOfLines={1}> {this.props.route.params.name} </Text> */}
          </View>
        </View>

        <WebView
          source={{uri: this.state.url}}
          useWebKit={true}
          allowsBackForwardNavigationGestures={true}
          allowsInlineMediaPlayback={true}
          userAgent={Platform.OS === 'android' ? 'wenat_android' : 'wenat_ios'}
          //   ref={(webView) => {
          //     this.webView.ref = webView;
          //   }}
          startInLoadingState={true}
          originWhitelist={['*']}
          renderLoading={this.webLoader}
          allowUniversalAccessFromFileURLs={true}
          javaScriptEnabled={true}
          javaScriptEnabledAndroid={true}
          domStorageEnabled={true}
          bounces={false}
          mixedContentMode={'always'}
          onMessage={(event) => {
            const {data} = event.nativeEvent;
            this.redirectToNativeScreen(data);
          }}
          //   onNavigationStateChange={(navState) => {
          //     this.webView.canGoBack = navState.canGoBack;
          //   }}
          onError={(syntheticEvent) => {
            const {nativeEvent} = syntheticEvent;
            console.log('Error occured : ', nativeEvent);
            this.setState({errPage: true});
          }}
        />
      </SafeAreaView>
    );
  }
}
