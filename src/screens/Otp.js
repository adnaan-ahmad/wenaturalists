import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
    ScrollView,
    Dimensions,
    Platform
} from 'react-native';
import axios from "axios"
import OTPInputView from '@twotalltotems/react-native-otp-input'
import Snackbar from 'react-native-snackbar'
import { REACT_APP_userServiceURL } from '../../env.json'
import Header from '../Components/User/SignUp/Header'
import CountryCode from '../Json/CountryCode.json'
import styles from '../Components/GlobalCss/User/SignUpCss'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../assets/Icons/selection.json'

const Icon = createIconSetFromIcoMoon(icoMoonConfig)
// const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

export default class Otp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            otp: '',
            seconds: 59,
            minutes: 9,
            isLoading: false,
            animatingWidth: 20
        };
    }

    resendOtp = () => {
        let postBody = {
            "userId": this.props.userId,
            "otpType": 'SIGNUPRESEND'
        };
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/otp/send',
            headers: { 'Content-Type': 'application/json' },
            data: postBody,
            withCredentials: true
        }).then((response) => {
            let res = response.data;
            if (res.message === 'Success!') {
                console.log("response : ", res)
                this.setState({ minutes: 9, seconds: 59 })
            }
        }).catch((err) => {
            console.log(err)
        });
    }

    componentDidMount() {

        this.interval = setInterval(
            () => this.setState((prevState) => ({ seconds: prevState.seconds - 1, animatingWidth: this.state.animatingWidth + 250 / 600 })),
            1000
        )
    }

    componentDidUpdate() {

        if (this.state.minutes === 0 && this.state.seconds === 0) {
            clearInterval(this.interval);
        }
        if (this.state.seconds === -1) {
            this.setState({ minutes: this.state.minutes - 1, seconds: 59 })
        }
        if (this.state.seconds.toString().length === 1) {
            this.setState({ seconds: '0' + this.state.seconds })
        }

    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    // getCountryCode = () => {
    //     return CountryCode.filter((item) => {
    //         item.Name === 'select'
    //     })
    // }

    render() {
        // console.log(this.getCountryCode())
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={[styles.modalContent, { height: windowHeight, paddingTop: 10 }]}>


                        <Header text="Authentication" progress={2} />

                        <View style={{ height: 70, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                            <OTPInputView
                                placeholderCharacter=''
                                placeholderTextColor='#91B3A257'
                                style={{ width: 260, color: '#00394D', marginLeft: -12 }}
                                // style={{ width: 300, height: 120, textAlign: 'center', color: 'white', alignSelf: 'center' }}
                                pinCount={6}
                                code={this.state.otp}
                                onCodeChanged={code => this.setState({ otp: code })}
                                autoFocusOnLoad={false}
                                codeInputFieldStyle={styles.borderStyleBase}
                                codeInputHighlightStyle={styles.borderStyleHighLighted}
                                onCodeFilled={(code => {
                                    console.log(`Code is ${code}, you are good to go!`)
                                })}
                            />
                        </View>

                        <View style={styles.modalOtpContainer}>
                            <Text style={styles.modalOtpText}>Enter the 6 digit OTP sent on your</Text>
                            <Text style={styles.modalOtpText}>registered phone</Text>
                        </View>

                        <View style={{ width: 156, height: 31, backgroundColor: '#E7F3E3', borderRadius: 20, alignSelf: 'center', marginTop: 16, alignItems: 'center', justifyContent: 'center' }}>
                            {this.props.country === 'India' ?
                                <Text style={styles.phoneNumber}>+91 {this.props.mobileNumber}</Text>
                                :
                                <Text style={styles.phoneNumber}>{this.props.mobileNumber}</Text>}
                        </View>

                        <TouchableOpacity activeOpacity={0.5} style={{ marginTop: 30, height: 40, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30 }}>
                            {/* <Icon name='Exit' size={14} color='#367681' style={{}} /> */}

                            <TouchableOpacity activeOpacity={0.5} style={{ marginTop: 0, height: 40, flexDirection: 'row', justifyContent: 'center', paddingHorizontal: 30, alignSelf: 'center', alignItems: 'center' }}>
                                {/* <Icon name='Exit' size={14} color='#367681' style={{}} /> */}
                                <Icon name='EditBox' size={14} color='#367681' style={{ marginTop: Platform.OS === 'android' ? 8 : 0 }} />
                                <Text style={{ color: '#367681', fontSize: 14, marginLeft: 4, fontFamily: 'Montserrat-Medium' }}>Edit number</Text>
                            </TouchableOpacity>

                        </TouchableOpacity>


                        <TouchableOpacity
                            activeOpacity={.5}
                            style={styles.continueButton}
                            onPress={() => {
                                this.setState({ isLoading: true })

                                let otpBody = {
                                    "userId": this.props.userId,
                                    "transactionId": this.props.transactionId,
                                    "otp": this.state.otp
                                }

                                axios({
                                    method: 'post',
                                    url: REACT_APP_userServiceURL + '/otp/signup/verify',
                                    headers: { 'Content-Type': 'application/json' },
                                    data: otpBody,
                                    withCredentials: true
                                }).then((response) => {
                                    this.setState({ isLoading: false})
                                    let res = response.data;
                                    console.log(res)
                                    if (res.message === 'Success!') {
                                        console.log(res.message)
                                        this.props.changeState({ currentScreen: 3, userId: this.props.userId })
                                        Snackbar.show({
                                            backgroundColor: '#97A600',
                                            text: "OTP verified successfully",
                                            textColor: "#00394D",
                                            duration: Snackbar.LENGTH_LONG,
                                        })
                                    }
                                }).catch((err) => {
                                    this.setState({ isLoading: false, otp: '' })
                                    if (err && err.response && err.response.data) {
                                        this.setState({otp: ''})
                                        Snackbar.show({
                                            backgroundColor: '#B22222',
                                            text: err.response.data.message,
                                            duration: Snackbar.LENGTH_LONG,
                                        })
                                    }
                                });
                            }
                            }>
                            <Text style={styles.continue}>Submit</Text>
                        </TouchableOpacity>


                        {this.state.minutes === 0 && this.state.seconds === '0' + 0 ?
                            <TouchableOpacity onPress={this.resendOtp}
                                activeOpacity={.5}
                                style={{
                                    justifyContent: 'center', backgroundColor: '#D0E8C8', width: 194, height: 38, alignSelf: 'center', marginTop: '5%', borderRadius: 40
                                }}>

                                <Text style={styles.resend}>Resend OTP</Text>

                            </TouchableOpacity>
                            :
                            <View style={{
                                justifyContent: 'center', alignItems: 'center', backgroundColor: '#EDEFEF', width: 194, height: 38, alignSelf: 'center', marginTop: '5%', borderRadius: 40, flexDirection: 'row',
                            }}>
                                <View style={{ width: this.state.animatingWidth, alignSelf: 'flex-start', height: 38, backgroundColor: '#698F8A', borderTopLeftRadius: 26, borderBottomLeftRadius: 26, position: 'absolute', left: 0, bottom: 0, }}></View>
                                <Text style={styles.resend}>Resend OTP in {this.state.minutes}:{this.state.seconds}</Text>

                            </View>
                        }


                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}
