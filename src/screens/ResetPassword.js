import React, { Component } from 'react'
import {
    Text,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    SafeAreaView,
    ScrollView,
    Modal,
    TouchableHighlight,
    ActivityIndicator,
    FlatList,
    ImageBackground
} from 'react-native'
// import Modal from 'react-native-modal';
import { ProgressBar, TextInput } from 'react-native-paper'
import AsyncStorage from '@react-native-community/async-storage';
import CountryCode from '../Json/CountryCode.json'
import Snackbar from 'react-native-snackbar'
import axios from "axios"
import styles from '../Components/GlobalCss/User/ResetPasswordCss'
import EyeIcon from 'react-native-vector-icons/MaterialIcons'
import { REACT_APP_userServiceURL, REACT_APP_authUser, REACT_APP_authSecret } from '../../env.json'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../assets/Icons/selection.json'
import welogo from '../../assets/welogo.png'
import bg from '../../assets/BackgroundImg.png'
import UserAddress from './UserAddress'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import AddProfilePicture from '../Components/User/SignUp/AddProfilePicture'
import Header from '../Components/User/SignUp/Header'
// import MyStatusBar from '../Components/Shared/MyStatusBar'

const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const uppercaseRegex = /[A-Z]/
const Icon = createIconSetFromIcoMoon(icoMoonConfig)
const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height

class Signup extends Component {

    constructor(props) {
        super(props);

        let validation = {
            username: {},
        }
        let token
        this.state = {
            validation,
            isLoading: false,
            username: '',
            isDisabled: false,
            errors: {},

            error: {
                firstName: false,
                lastName: false,
                email: false,
                phone: false,
                phnerr: '',
                password: false,
                blank: false
            },
            focus: '',
            firstName: '',
            lastName: '',
            phoneNo: '',
            email: '',
            modalOpen: false,
            otp: '',
            userId: '',
            transactionId: '',
            password: '',
            newPassword: '',
            country: '',
            selectedCountryCode: 'Code',
            passwordIcon: 'visibility-off',
            newPasswordIcon: 'visibility-off',
            isIndianResident: true,
            agreeToTerms: true,
            emailAvailable: false,
            newToken: null,
            modalCountry: false,
            modalterm: false,
            query: '',
            countryCodes: [],
            currentScreen: 1,
            seconds: 59,
            minutes: 9,
            skip: false,
            animatingWidth: 20
        }
    }

    changeState = (value) => {
        this.setState(value)
    }

    componentDidMount() {

        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/api/authenticate?username=' + REACT_APP_authUser + '&password=' + REACT_APP_authSecret,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
            .then((response) => {
                this.token = response.headers.authorization;
                console.log(this.token)
            }).catch((e) => {
                console.log(e)
            })

        if (this.state.countryCodes.length === 0) CountryCode.sort((a, b) => a.Dial - b.Dial).map((item) => {
            if (item.Name !== 'select') {
                this.state.countryCodes.push({ label: item.Unicode + ' +' + item.Dial + ' ' + item.Name, value: { code: item.Dial, country: item.Name } })
            }
        })

        this.interval = setInterval(
            () => this.setState((prevState) => ({ seconds: prevState.seconds - 1, animatingWidth: this.state.animatingWidth + 174 / 600 })),
            1000
        )
    }

    componentDidUpdate() {

        if (this.state.skip === true) {
            this.setState({ skip: false })
            this.props.navigation.navigate('Login')
        }

        if (this.state.minutes === 0 && this.state.seconds === 0) {
            clearInterval(this.interval);
        }
        if (this.state.seconds === -1) {
            this.setState({ minutes: this.state.minutes - 1, seconds: 59 })
        }
        if (this.state.seconds.toString().length === 1) {
            this.setState({ seconds: '0' + this.state.seconds })
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    findCountry = (query) => {
        if (query === '') {
            return this.state.countryCodes
        }

        const regex = new RegExp(`${query.trim()}`, 'i')

        return this.state.countryCodes.filter(item => (item.value.code + item.value.country).search(regex) >= 0)
    }




    handleSubmit = () => {
        // console.log(this.state.email)
        // event.preventDefault();
        // if (this.validateForm()) {
        // this.setState({ isDisabled: true });
        let postBody = {
            "username": this.state.email
        };
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/user/reset/password',
            headers: { 'Content-Type': 'application/json', 'Authorization': this.token },
            data: postBody,
            withCredentials: false
        }).then((response) => {
            let res = response.data;

            if (res.status === '202 ACCEPTED') {

                this.setState({ currentScreen: 2, userId: res.body.userId, transactionId: res.body.transactionId })
                console.log(this.state.userId, this.state.transactionId)
            }
        }).catch((err) => {
            console.log(err)

        });

    }


    handleSubmitOtp = () => {
        console.log('token', this.token)
        let postBody = {
            "userId": this.state.userId,
            "transactionId": this.state.transactionId,
            "otp": this.state.otp
        };
        axios({
            method: 'post',
            url: REACT_APP_userServiceURL + '/otp/verify',
            headers: { 'Content-Type': 'application/json', 'Authorization': this.token },
            data: postBody,
            withCredentials: true
        }).then((response) => {
            let res = response.data;
            console.log(res)
            if (res.message === 'Success!') {
                
                this.setState({ currentScreen: 3 })
            }
        }).catch((err) => {
            // if (err && err.response && err.response.data) {
                console.log(err.response)
                Snackbar.show({
                    backgroundColor: 'red',
                    text: err.response.data.message,
                    textColor: "#FFF",
                    duration: Snackbar.LENGTH_LONG,
                })

            // }
            // this.clearOtp();
        })


    }



    handleSubmitPassword = () => {

        if (this.state.password === this.state.newPassword) {
            let postBody = {
                "transactionId": this.state.transactionId,
                "newPassword": this.state.password,
                "confirmPassword": this.state.newPassword
            };
            axios({
                method: 'post',
                url: REACT_APP_userServiceURL + '/user/change/password',
                headers: { 'Content-Type': 'application/json', 'Authorization': this.token },
                data: postBody,
                withCredentials: true
            }).then((response) => {
                let res = response.data;
                console.log(res)
                if (res.status === '202 ACCEPTED') {
                    Snackbar.show({
                        backgroundColor: '#97A600',
                        text: "Password has been successfully changed. You may login now",
                        textColor: "#00394D",
                        duration: Snackbar.LENGTH_LONG,
                    })
                    this.props.navigation.replace('Login')
                }
            }).catch((err) => {

                if (err && err.response && err.response.data) {
                    console.log(err.response)

                }
            });
        }
        else {
            Snackbar.show({
                backgroundColor: 'red',
                text: "New password and confirm password doesn't match",
                textColor: "#FFF",
                duration: Snackbar.LENGTH_LONG,
            })
        }

    }


    displayCurrentScreen = () => {

        // if (this.state.isLoading) {
        //   return <View style={{ flex: 1, justifyContent: 'center' }}>
        //     <ActivityIndicator color="#97A600" size="large" />
        //   </View>
        // }

        if (this.state.currentScreen === 1) {
            return (
                <SafeAreaView style={styles.container}>
                    <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled' style={{ backgroundColor: '#F7F7F5', paddingTop: '16%' }}>
                        {/* <MyStatusBar barStyle="light-content" /> */}
                        <View style={Platform.OS === 'ios' && this.state.focus !== '' ? [styles.mainSection, { height: windowHeight * 1.2 }] : styles.mainSection}>

                            <Header
                                text='Reset your Password' customStyle={{ backgroundColor: '#F7F7F5', marginBottom: 0, marginTop: '0%' }}
                                opacity={(this.state.modalCountry || this.state.modalterm) ? 0.3 : 1}
                                marginTop={'12%'}
                            />


                            {/* <View style={styles.termsConditions}> */}
                            {/* <View style={{ width: 34, height: 34, borderRadius: 17, backgroundColor: '#E2E7E9', justifyContent: 'center', alignItems: 'center' }}><Icon name='FAQ' size={18} color='#367681' style={{}} /></View> */}

                            <View style={{ marginTop: -20 }}>
                                <Text style={[styles.term, { marginLeft: 10 }]}>Enter your email or phone</Text>
                                {/* <TouchableOpacity activeOpacity={0.45}
                                        onPress={() => this.setState({ modalterm: true })}> */}

                                <Text style={[styles.term, { marginLeft: 10 }]}> number to receive OTP</Text>
                                {/* </TouchableOpacity> */}
                            </View>

                            {/* </View> */}

                            {/***** SignUp form *****/}

                            {
                                (this.state.modalterm || this.state.modalCountry) ?
                                    <View style={{ width: '100%', height: windowHeight, backgroundColor: '#00394D8C' }}></View>
                                    :
                                    <View style={styles.form}>



                                        <TextInput
                                            theme={this.state.error.email ? { colors: { text: '#367681', primary: 'red', placeholder: 'red' } } : { colors: { text: '#367681', primary: '#91B3A2', placeholder: '#91B3A2' } }}
                                            // mode='outlined'
                                            label={this.state.error.email ? "Please enter a valid email address" : "Email or Phone Number"}
                                            selectionColor='#C8DB6E'
                                            style={this.state.focus === 'email' ? [styles.inputBlur, { backgroundColor: '#FFFFFF', height: 50, borderBottomWidth: 1.5, borderTopWidth: 0.1, borderBottomColor: '#FFF', borderTopColor: '#FFF' }] : styles.inputBlur}
                                            onFocus={() => { this.changeState({ focus: 'email', error: { email: false } }) }}
                                            onBlur={() => { this.changeState({ focus: '' }) }}
                                            value={this.state.email}
                                            onChangeText={(value) => this.changeState({ email: value })}
                                        />
                                        <View style={this.state.focus === 'email' ? { backgroundColor: '#BFC52E', width: '100%', height: 1, marginTop: -7.5 }
                                            : { backgroundColor: '#F7F7F5', width: '100%', height: 10, marginTop: -7.5 }}></View>





                                        <View style={{ backgroundColor: '#F7F7F5' }}>


                                            {
                                                this.state.isLoading ?
                                                    <View style={[styles.agree, { alignItems: 'center' }]}>
                                                        <ActivityIndicator color="#fff" size="large" />
                                                    </View>
                                                    :
                                                    <TouchableHighlight underlayColor="#00394D"
                                                        style={styles.agree}
                                                        onPress={() => this.handleSubmit()}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            {/* <View style={{ width: '20%', height: 54, backgroundColor: '#00394D', borderTopLeftRadius: 26, borderBottomLeftRadius: 26 }}></View> */}
                                                            <Text style={styles.agreeTxt}>Continue</Text>
                                                        </View>
                                                    </TouchableHighlight>
                                            }
                                        </View>


                                        <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('Login')}>
                                            <Text style={{ color: '#154A59', fontSize: 18, textAlign: 'center', fontWeight: '500' }}>Back</Text>
                                        </TouchableOpacity>





                                    </View>
                            }

                            {/***** SignUp form *****/}

                        </View>
                        {/* <View style={styles.footer}>

            </View> */}
                    </ScrollView>
                </SafeAreaView>
            )
        }

        // {/***** OTP screen *****/}

        else if (this.state.currentScreen === 2) {
            return (
                <SafeAreaView style={styles.container}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {/* <MyStatusBar barStyle="light-content" /> */}
                        <View style={[styles.modalContent, { height: windowHeight }]}>


                            <Header text="Authentication" />


                            {/* <Text style={styles.phoneNumber}>+91 9876543210</Text> */}


                            <View style={{ height: 70, justifyContent: 'center', alignItems: 'center', marginTop: '0%', marginBottom: '-8%' }}>
                                <OTPInputView
                                    placeholderCharacter=''
                                    placeholderTextColor='#91B3A257'
                                    style={{ width: 260, color: '#00394D', marginLeft: -12 }}
                                    pinCount={6}
                                    code={this.state.otp}
                                    onCodeChanged={code => this.setState({ otp: code })}
                                    autoFocusOnLoad={false}
                                    codeInputFieldStyle={styles.borderStyleBase}
                                    codeInputHighlightStyle={styles.borderStyleHighLighted}
                                    onCodeFilled={(code => {
                                        console.log(`Code is ${code}, you are good to go!`)
                                    })}
                                />
                            </View>

                            <View style={styles.modalOtpContainer}>
                                <Text style={styles.modalOtpText}>Enter the 6 digit OTP sent on your</Text>
                                <Text style={styles.modalOtpText}>registered phone</Text>
                            </View>

                            <View style={{ width: 156, height: 31, backgroundColor: '#E7F3E3', borderRadius: 20, alignSelf: 'center', marginTop: 10, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={styles.phoneNumber}>+{this.state.selectedCountryCode} {this.state.phoneNo}</Text>
                            </View>


                            <TouchableOpacity activeOpacity={0.5} style={{ marginTop: 30, height: 40, flexDirection: 'row', justifyContent: 'center', paddingHorizontal: 30 }}>
                                {/* <Icon name='Exit' size={14} color='#367681' style={{}} /> */}
                                <Icon name='EditBox' size={14} color='#367681' style={{}} />
                                <Text style={{ color: '#367681', fontSize: 14, marginLeft: 4 }}>Edit number</Text>
                            </TouchableOpacity>

                            {
                                this.state.isLoading ?
                                    <View style={styles.continueButton}>
                                        <ActivityIndicator color="#fff" size="large" />
                                    </View>
                                    :
                                    <TouchableOpacity
                                        activeOpacity={.9}
                                        style={styles.continueButton}
                                        onPress={() => this.handleSubmitOtp()}
                                    // onPress={() => {
                                    //     this.setState({ isLoading: true })

                                    //     let otpBody = {
                                    //         "userId": this.state.userid,
                                    //         "transactionId": this.state.transactionid,
                                    //         "otp": this.state.otp
                                    //     }

                                    //     axios({
                                    //         method: 'post',
                                    //         url: REACT_APP_userServiceURL + '/otp/signup/verify',
                                    //         headers: { 'Content-Type': 'application/json' },
                                    //         data: otpBody,
                                    //         withCredentials: true
                                    //     }).then((response) => {
                                    //         this.setState({ isLoading: false, currentScreen: 3 })
                                    //         let res = response.data;
                                    //         if (res.message === 'Success!') {
                                    //             Snackbar.show({
                                    //                 backgroundColor: '#97A600',
                                    //                 text: "OTP verified successfully",
                                    //                 textColor: "#00394D",
                                    //                 duration: Snackbar.LENGTH_LONG,
                                    //             })
                                    //         }
                                    //     }).catch((err) => {
                                    //         this.setState({ isLoading: false })
                                    //         if (err && err.response && err.response.data) {
                                    //             Snackbar.show({
                                    //                 backgroundColor: '#B22222',
                                    //                 text: err.response.data.message,
                                    //                 duration: Snackbar.LENGTH_LONG,
                                    //             })
                                    //         }
                                    //     });
                                    // }
                                    // }
                                    >
                                        <Text style={styles.continue}>Submit</Text>
                                    </TouchableOpacity>
                            }

                            {this.state.minutes === 0 && this.state.seconds === '0' + 0 ?
                                <TouchableOpacity activeOpacity={.5} style={{
                                    justifyContent: 'center', backgroundColor: '#D0E8C8', width: 194, height: 38, alignSelf: 'center', marginTop: '5%', borderRadius: 40
                                }}>

                                    <Text style={styles.resend}>Resend OTP</Text>

                                </TouchableOpacity>
                                :
                                <View style={{
                                    justifyContent: 'center', alignItems: 'center', backgroundColor: '#EDEFEF', width: 194, height: 38, alignSelf: 'center', marginTop: '5%', borderRadius: 40, flexDirection: 'row',
                                }}>
                                    <View style={{ width: this.state.animatingWidth, alignSelf: 'flex-start', height: 38, backgroundColor: '#698F8A', borderTopLeftRadius: 26, borderBottomLeftRadius: 26, position: 'absolute', left: 0, bottom: 0, }}></View>
                                    <Text style={styles.resend}>Resend OTP in {this.state.minutes}:{this.state.seconds}</Text>

                                </View>
                            }


                            {/* <TouchableOpacity style={styles.backButton} onPress={() => this.changeState({ modalOpen: false })}>
              <Text style={styles.back}>BACK</Text>
            </TouchableOpacity> */}

                        </View>
                        {/* <View style={styles.footer}>

            </View> */}
                    </ScrollView>
                </SafeAreaView>
            )
        }

        // {/***** OTP screen *****/}

        else if (this.state.currentScreen === 3) {
            return <SafeAreaView style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='handled' style={{ backgroundColor: '#F7F7F5', paddingTop: '16%' }}>
                    {/* <MyStatusBar barStyle="light-content" /> */}
                    <View style={Platform.OS === 'ios' && this.state.focus !== '' ? [styles.mainSection, { height: windowHeight * 1.2 }] : styles.mainSection}>

                        <Header
                            text='Enter New Password' customStyle={{ backgroundColor: '#F7F7F5', marginBottom: 0, marginTop: '0%' }}
                            opacity={(this.state.modalCountry || this.state.modalterm) ? 0.3 : 1}
                            marginTop={'12%'}
                        />


                        {/* <View style={styles.termsConditions}> */}
                        {/* <View style={{ width: 34, height: 34, borderRadius: 17, backgroundColor: '#E2E7E9', justifyContent: 'center', alignItems: 'center' }}><Icon name='FAQ' size={18} color='#367681' style={{}} /></View> */}

                        <View style={{ marginTop: -20 }}>
                            <Text style={[styles.term, { marginLeft: 10 }]}>Your password must be at least 8</Text>
                            {/* <TouchableOpacity activeOpacity={0.45}
                                onPress={() => this.setState({ modalterm: true })}> */}

                            <Text style={[styles.term, { marginLeft: 10 }]}> characters long with 1 Uppercase</Text>
                            {/* </TouchableOpacity> */}
                        </View>

                        {/* </View> */}

                        {/* country picker */}

                        {/* country picker */}

                        {/* terms and condition picker */}

                        {/* terms and condition picker */}

                        {/***** SignUp form *****/}

                        {
                            (this.state.modalterm || this.state.modalCountry) ?
                                <View style={{ width: '100%', height: windowHeight, backgroundColor: '#00394D8C' }}></View>
                                :
                                <View style={styles.form}>



                                    <View style={styles.searchSection}>

                                        <TextInput
                                            theme={this.state.error.password ? { colors: { text: '#367681', primary: 'red', placeholder: 'red' } } : { colors: { text: '#367681', primary: '#91B3A2', placeholder: '#91B3A2' } }}
                                            // mode='outlined'
                                            // label="Password"
                                            label={(this.state.focus === 'password' && (this.state.password.length < 8 || uppercaseRegex.test(String(this.state.password)) === false)) ? "Password must be at least 8 characters long with 1 uppercase" : ((this.state.password !== '' && (this.state.password.length < 8 || uppercaseRegex.test(String(this.state.password)) === false)) ? "Password must be at least 8 characters long with 1 uppercase" : "Enter New Password")}
                                            selectionColor='#C8DB6E'
                                            style={this.state.focus === 'password' ? [styles.inputBlur, { backgroundColor: '#FFFFFF', height: 50, zIndex: 1, borderBottomWidth: 1.5, borderTopWidth: 0.1, borderBottomColor: '#FFF', borderTopColor: '#FFF' }] : [styles.inputBlur, { zIndex: 1 }]}
                                            onFocus={() => { this.changeState({ focus: 'password', error: { password: false } }) }}
                                            onBlur={() => { this.changeState({ focus: '' }) }}
                                            value={this.state.password}
                                            onChangeText={(value) => this.changeState({ password: value })}
                                            secureTextEntry={this.state.passwordIcon === 'visibility' ? false : true}
                                        />

                                        {this.state.focus === 'password' || this.state.password !== '' ? <EyeIcon style={[styles.icon, { marginRight: 19 }]}
                                            name={this.state.passwordIcon}
                                            size={17}
                                            color='#91B3A2'
                                            onPress={() => {
                                                this.state.passwordIcon === 'visibility' ? this.changeState({ passwordIcon: 'visibility-off' }) : this.changeState({ passwordIcon: 'visibility' })
                                            }}
                                        /> : <></>}

                                    </View>

                                    <View style={this.state.focus === 'password' ? { backgroundColor: '#BFC52E', width: '100%', height: 1, marginTop: -7.5 }
                                        : { backgroundColor: '#F7F7F5', width: '100%', height: 10, marginTop: -7.5 }}></View>




                                    <View style={styles.searchSection}>

                                        <TextInput
                                            theme={this.state.error.password ? { colors: { text: '#367681', primary: 'red', placeholder: 'red' } } : { colors: { text: '#367681', primary: '#91B3A2', placeholder: '#91B3A2' } }}
                                            // mode='outlined'
                                            // label="Password"
                                            label="Re-Enter Password"
                                            selectionColor='#C8DB6E'
                                            style={this.state.focus === 'newPassword' ? [styles.inputBlur, { backgroundColor: '#FFFFFF', height: 50, zIndex: 1, borderBottomWidth: 1.5, borderTopWidth: 0.1, borderBottomColor: '#FFF', borderTopColor: '#FFF' }] : [styles.inputBlur, { zIndex: 1 }]}
                                            onFocus={() => { this.changeState({ focus: 'newPassword', error: { newPassword: false } }) }}
                                            onBlur={() => { this.changeState({ focus: '' }) }}
                                            value={this.state.newPassword}
                                            onChangeText={(value) => this.changeState({ newPassword: value })}
                                            secureTextEntry={this.state.newPasswordIcon === 'visibility' ? false : true}
                                        />

                                        {this.state.focus === 'newPassword' || this.state.newPassword !== '' ? <EyeIcon style={[styles.icon, { marginRight: 19 }]}
                                            name={this.state.newPasswordIcon}
                                            size={17}
                                            color='#91B3A2'
                                            onPress={() => {
                                                this.state.newPasswordIcon === 'visibility' ? this.changeState({ newPasswordIcon: 'visibility-off' }) : this.changeState({ newPasswordIcon: 'visibility' })
                                            }}
                                        /> : <></>}

                                    </View>

                                    <View style={this.state.focus === 'newPassword' ? { backgroundColor: '#BFC52E', width: '100%', height: 1, marginTop: -7.5 }
                                        : { backgroundColor: '#F7F7F5', width: '100%', height: 10, marginTop: -7.5 }}></View>



                                    <View style={{ backgroundColor: '#F7F7F5' }}>


                                        {
                                            this.state.isLoading ?
                                                <View style={[styles.agree, { alignItems: 'center' }]}>
                                                    <ActivityIndicator color="#fff" size="large" />
                                                </View>
                                                :
                                                <TouchableHighlight underlayColor="#00394D"
                                                    style={styles.agree}
                                                    onPress={() => this.handleSubmitPassword()}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        {/* <View style={{ width: '20%', height: 54, backgroundColor: '#00394D', borderTopLeftRadius: 26, borderBottomLeftRadius: 26 }}></View> */}
                                                        <Text style={styles.agreeTxt}>Confirm</Text>
                                                    </View>
                                                </TouchableHighlight>
                                        }
                                    </View>


                                    <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('Login')}>
                                        <Text style={{ color: '#154A59', fontSize: 18, textAlign: 'center', fontWeight: '500' }}>Cancel</Text>
                                    </TouchableOpacity>





                                </View>
                        }

                        {/***** SignUp form *****/}

                    </View>
                    {/* <View style={styles.footer}>

    </View> */}
                </ScrollView>
            </SafeAreaView>
        }

        else if (this.state.currentScreen === 4) {
            this.props.navigation.replace("BottomTab")
            // return <AddProfilePicture
            //   changeState={this.changeState}
            // />
            return <></>
        }
    }

    render() {
        return this.displayCurrentScreen()
    }
}

export default Signup