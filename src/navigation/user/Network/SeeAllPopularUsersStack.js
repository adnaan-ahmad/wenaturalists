import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import SeeAllPopularUsers from '../../../screens/user/Network/SeeAllPopularUsers'

const Stack = createStackNavigator()

function SeeAllPopularUsersStack({hide, hideGroup, changeState}) {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>
        <Stack.Screen name='SeeAllPopularUsers' component={SeeAllPopularUsers} options={{ animationEnabled: false }}/>
      </Stack.Navigator>
  )
}

export default SeeAllPopularUsersStack
