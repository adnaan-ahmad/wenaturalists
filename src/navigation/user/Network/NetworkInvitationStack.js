import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import InvitationsNetwork from '../../../screens/user/Network/InvitationsNetwork'
import InvitationsCircle from '../../../screens/user/Network/InvitationsCircle'
import InvitationsProject from '../../../screens/user/Network/InvitationsProject'
import InvitationsAdmin from '../../../screens/user/Network/InvitationsAdmin'

const Stack = createStackNavigator()

function NetworkInvitationStack({hide, hideGroup, changeState}) {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>
        <Stack.Screen name='InvitationsNetwork' component={InvitationsNetwork} options={{ animationEnabled: false }}/>
        <Stack.Screen name='InvitationsCircle' component={InvitationsCircle} options={{ animationEnabled: false }}/>
        <Stack.Screen name='InvitationsProject' component={InvitationsProject} options={{ animationEnabled: false }}/>
        <Stack.Screen name='InvitationsAdmin' component={InvitationsAdmin} options={{ animationEnabled: false }}/>
      </Stack.Navigator>
  )
}

export default NetworkInvitationStack
