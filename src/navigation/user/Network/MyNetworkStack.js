import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import MyNetwork from '../../../screens/user/Network/MyNetwork'
import MyConnects from '../../../screens/user/Network/MyConnects'
import YourRequests from '../../../screens/user/Network/YourRequests'
import CircleNetwork from '../../../screens/user/Network/CircleNetwork'
import MyOrganization from '../../../screens/user/Network/MyOrganization'
import Followers from '../../../screens/user/Network/Followers'
import Following from '../../../screens/user/Network/Following'
import PublicChatGroup from '../../../screens/user/Network/PublicChatGroup'

const Stack = createStackNavigator()

function MyNetworkStack({hide, hideGroup, changeState}) {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>
        <Stack.Screen name='MyNetwork' component={MyNetwork} />
        <Stack.Screen name='MyConnects' component={MyConnects} />
        <Stack.Screen name='YourRequests' component={YourRequests} />
        <Stack.Screen name='Followers' component={Followers} options={{ animationEnabled: false }} />
        <Stack.Screen name='Following' component={Following} options={{ animationEnabled: false }} />
        <Stack.Screen name='MyOrganization' component={MyOrganization} />
        <Stack.Screen name='CircleNetwork' component={CircleNetwork} />
        <Stack.Screen name='PublicChatGroup' component={PublicChatGroup} />
        
      </Stack.Navigator>
  )
}

export default MyNetworkStack
