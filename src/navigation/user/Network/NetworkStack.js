import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import NetworkConnects from '../../../screens/user/Network/NetworkConnects'
import NetworkCompanies from '../../../screens/user/Network/NetworkCompanies'
import NetworkCircles from '../../../screens/user/Network/NetworkCircles'

const Stack = createStackNavigator()

function NetworkStack({hide, hideGroup, changeState}) {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>
        <Stack.Screen name='NetworkConnects' component={NetworkConnects} options={{ animationEnabled: false }}/>
        <Stack.Screen name='NetworkCompanies' component={NetworkCompanies} options={{ animationEnabled: false }}/>
        <Stack.Screen name='NetworkCircles' component={NetworkCircles} options={{ animationEnabled: false }}/>
      </Stack.Navigator>
  )
}

export default NetworkStack
