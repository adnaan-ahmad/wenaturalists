import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import Profile from '../../../screens/user/Profile/Profile'
import CompanyProfile from '../../../screens/user/Profile/CompanyProfile'
import Employees from '../../../screens/user/Profile/Employees'
import ActivitiesProfile from '../../../screens/user/Profile/ActivitiesProfile'
import CompanyActivities from '../../../screens/user/Profile/CompanyActivities'
import SeeallExperience from '../../../screens/user/Profile/SeeallExperience'
import SeeallEducation from '../../../screens/user/Profile/SeeallEducation'
import SeeallCircle from '../../../screens/user/Profile/SeeallCircle'
import SeeallEndorsement from '../../../screens/user/Profile/SeeallEndorsement'
import SeeallBusinessPage from '../../../screens/user/Profile/SeeallBusinessPage'
import SeeallCauses from '../../../screens/user/Profile/SeeallCauses'
import ReorderProfile from '../../../screens/user/Profile/ReorderProfile'
import ConnectionDetails from '../../../screens/user/Profile/ConnectionDetails'

const Stack = createStackNavigator()

function CompanyProfileStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>
        
        <Stack.Screen name='CompanyProfileScreen' component={CompanyProfile} options={{ animationEnabled: false }}/>
        <Stack.Screen name='EmployeesScreen' component={Employees} />
        
        <Stack.Screen name='CompanyActivitiesScreen' component={CompanyActivities} options={{ animationEnabled: false }}/>
        <Stack.Screen name='ConnectionDetailsScreen' component={ConnectionDetails} />
        <Stack.Screen name='SeeallExperienceScreen' component={SeeallExperience} />
        <Stack.Screen name='SeeallEducationScreen' component={SeeallEducation} />
        <Stack.Screen name='SeeallCircleScreen' component={SeeallCircle} />
        <Stack.Screen name='SeeallEndorsementScreen' component={SeeallEndorsement} />
        <Stack.Screen name='SeeallBusinessPageScreen' component={SeeallBusinessPage} />
        <Stack.Screen name='SeeallCausesScreen' component={SeeallCauses} />
        <Stack.Screen name='ReorderProfileScreen' component={ReorderProfile} />
      </Stack.Navigator>
  )
}

export default CompanyProfileStack
