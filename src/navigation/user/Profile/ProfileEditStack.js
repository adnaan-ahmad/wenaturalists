import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import ProfileEdit from '../../../screens/user/ProfileEdit/ProfileEdit'
import PersonalInfoEdit from '../../../screens/user/ProfileEdit/PersonalInfoEdit'
import AwardsEdit from '../../../screens/user/ProfileEdit/AwardsEdit'
import SkillsSpecializationEdit from '../../../screens/user/ProfileEdit/SkillsSpecializationEdit'
import ExperienceEdit from '../../../screens/user/ProfileEdit/ExperienceEdit'
import EducationEdit from '../../../screens/user/ProfileEdit/EducationEdit'
import OrganizationEdit from '../../../screens/user/ProfileEdit/OrganizationEdit'
import InterestsCausesEdit from '../../../screens/user/ProfileEdit/InterestsCausesEdit'
import CirclesEdit from '../../../screens/user/ProfileEdit/CirclesEdit'

const Stack = createStackNavigator()

function ProfileEditStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>

        <Stack.Screen name='ProfileEditScreen' component={ProfileEdit}/>
        <Stack.Screen name='PersonalInfoEdit' component={PersonalInfoEdit}/>
        <Stack.Screen name='AwardsEdit' component={AwardsEdit}/>
        <Stack.Screen name='EducationEdit' component={EducationEdit}/>
        <Stack.Screen name='ExperienceEdit' component={ExperienceEdit}/>
        <Stack.Screen name='OrganizationEdit' component={OrganizationEdit}/>
        <Stack.Screen name='SkillsSpecializationEdit' component={SkillsSpecializationEdit}/>
        <Stack.Screen name='InterestsCausesEdit' component={InterestsCausesEdit}/>
        <Stack.Screen name='CirclesEdit' component={CirclesEdit}/>

      </Stack.Navigator>
  )
}

export default ProfileEditStack
