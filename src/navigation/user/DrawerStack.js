import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import Drawer from '../../screens/user/Drawer'
// import Profile from '../../screens/user/Profile/Profile'
// import SeeallExperience from '../../screens/user/Profile/SeeallExperience'
// import SeeallEducation from '../../screens/user/Profile/SeeallEducation'
// import SeeallCircle from '../../screens/user/Profile/SeeallCircle'
// import SeeallEndorsement from '../../screens/user/Profile/SeeallEndorsement'
// import SeeallBusinessPage from '../../screens/user/Profile/SeeallBusinessPage'
// import SeeallCauses from '../../screens/user/Profile/SeeallCauses'
// import ReorderProfile from '../../screens/user/Profile/ReorderProfile'

const Stack = createStackNavigator()

function DrawerStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>
        <Stack.Screen name='DrawerScreen' component={Drawer} />
        {/* <Stack.Screen name='ProfileScreen' component={Profile} />
        <Stack.Screen name='SeeallExperienceScreen' component={SeeallExperience} />
        <Stack.Screen name='SeeallEducationScreen' component={SeeallEducation} />
        <Stack.Screen name='SeeallCircleScreen' component={SeeallCircle} />
        <Stack.Screen name='SeeallEndorsementScreen' component={SeeallEndorsement} />
        <Stack.Screen name='SeeallBusinessPageScreen' component={SeeallBusinessPage} />
        <Stack.Screen name='SeeallCausesScreen' component={SeeallCauses} />
        <Stack.Screen name='ReorderProfileScreen' component={ReorderProfile} /> */}
      </Stack.Navigator>
  )
}

export default DrawerStack
