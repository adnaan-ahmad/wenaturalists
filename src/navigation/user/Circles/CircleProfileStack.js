import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import CircleProfile from '../../../screens/user/Circles/CircleProfile'
import CircleActivities from '../../../screens/user/Circles/CircleActivities'
import OngoingProjects from '../../../screens/user/Circles/OngoingProjects'
import CompletedProjects from '../../../screens/user/Circles/CompletedProjects'
import CircleMembers from '../../../screens/user/Circles/CircleMembers'
import CircleProfileEdit from '../../../screens/user/Circles/CircleProfileEdit'
import CircleInfoEdit from '../../../screens/user/Circles/CircleInfoEdit'
import CircleSpecializationEdit from '../../../screens/user/Circles/CircleSpecializationEdit'
import CircleInterestsEdit from '../../../screens/user/Circles/CircleInterestsEdit'
import CirclePrivacyVisibility from '../../../screens/user/Circles/CirclePrivacyVisibility'
import CirclePrivacySharing from '../../../screens/user/Circles/CirclePrivacySharing'
import SentInvitations from '../../../screens/user/Circles/SentInvitations'
import ReceivedInvitations from '../../../screens/user/Circles/ReceivedInvitations'

const Stack = createStackNavigator()

function CircleStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>

        <Stack.Screen name='CircleProfile' component={CircleProfile} />
        <Stack.Screen name='CircleProfileEdit' component={CircleProfileEdit}/>
        <Stack.Screen name='CircleInfoEdit' component={CircleInfoEdit}/>
        <Stack.Screen name='CircleSpecializationEdit' component={CircleSpecializationEdit}/>
        <Stack.Screen name='CircleInterestsEdit' component={CircleInterestsEdit}/>
        <Stack.Screen name='CirclePrivacyVisibility' component={CirclePrivacyVisibility} options={{ animationEnabled: false }}/>
        <Stack.Screen name='CirclePrivacySharing' component={CirclePrivacySharing} options={{ animationEnabled: false }}/>
        <Stack.Screen name='CircleActivities' component={CircleActivities} options={{ animationEnabled: false }}/>
        <Stack.Screen name='OngoingProjects' component={OngoingProjects} options={{ animationEnabled: false }}/>
        <Stack.Screen name='CompletedProjects' component={CompletedProjects} options={{ animationEnabled: false }}/>
        <Stack.Screen name='CircleMembers' component={CircleMembers} options={{ animationEnabled: false }}/>
        <Stack.Screen name='SentInvitations' component={SentInvitations} options={{ animationEnabled: false }}/>
        <Stack.Screen name='ReceivedInvitations' component={ReceivedInvitations} options={{ animationEnabled: false }}/>

      </Stack.Navigator>
  )
}

export default CircleStack
