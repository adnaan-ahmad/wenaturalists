import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import Circles from '../../../screens/user/Circles/Circles'
import MyCircle from '../../../screens/user/Circles/MyCircle'

const Stack = createStackNavigator()

function CircleStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>

        <Stack.Screen name='Circles' component={Circles}/>
        <Stack.Screen name='MyCircle' component={MyCircle}/>

      </Stack.Navigator>
  )
}

export default CircleStack
