import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import PrivacySettings from '../../../screens/user/PrivacySettings/PrivacySettings'
import PrivacyVisibility from '../../../screens/user/PrivacySettings/PrivacyVisibility'
import PrivacySharing from '../../../screens/user/PrivacySettings/PrivacySharing'
import AccountSettings from '../../../screens/user/PrivacySettings/AccountSettings'
import NotificationSettings from '../../../screens/user/PrivacySettings/NotificationSettings'
import EmailNotification from '../../../screens/user/PrivacySettings/EmailNotification'
import ProfileDeactivationAdmin from '../../../screens/user/PrivacySettings/ProfileDeactivationAdmin'

const Stack = createStackNavigator()

function PrivacySettingStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>

        <Stack.Screen name='PrivacySettings' component={PrivacySettings}/>
        <Stack.Screen name='PrivacyVisibility' component={PrivacyVisibility}/>
        <Stack.Screen name='PrivacySharing' component={PrivacySharing} options={{animationEnabled: false}}/>
        <Stack.Screen name='AccountSettings' component={AccountSettings}/>
        <Stack.Screen name='NotificationSettings' component={NotificationSettings}/>
        <Stack.Screen name='EmailNotification' component={EmailNotification}/>
        <Stack.Screen name='ProfileDeactivationAdmin' component={ProfileDeactivationAdmin}/> 
      </Stack.Navigator>
  )
}

export default PrivacySettingStack
