import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';

import HomeStack from './HomeStack';
import MessagesStack from './Messages/MessagesStack';
import NetworkStack from './Network/NetworkStack';
import ProjectStack from './ProjectStack';
import DrawerStack from './DrawerStack';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../../../assets/Icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const BottomTab = createBottomTabNavigator();
export default class BottomTabNavigator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      drawer: false,
      isCompany: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('userData').then((value) => {
      let objValue = JSON.parse(value);
      objValue.type === 'COMPANY' && this.setState({isCompany: true});
    });
  }

  render() {
    return (
      <BottomTab.Navigator
        // barStyle={{ paddingTop : 20 }}
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'Home_F' : 'WN_Home_OL';
              // color = focused ? '#BFC52E' : '#367681'
            } else if (route.name === 'Messages') {
              iconName = focused ? 'Messeges_F' : 'WN_Messeges_OL';
              // color = focused ? '#BFC52E' : '#367681'
            } else if (route.name === 'Network') {
              iconName = focused ? 'Network_F' : 'WN_Network_OL';
              // color = focused ? '#BFC52E' : '#367681'
            } else if (route.name === 'Projects') {
              iconName = focused ? 'Projects_F1' : 'Projects_OL';
              // color = focused ? '#BFC52E' : '#367681'
            } else if (route.name === 'More') {
              iconName = focused ? 'Menu_FL' : 'Menu_OL';
              // color = focused ? '#BFC52E' : '#367681'
            }

            return <Icon name={iconName} size={19} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: '#367681',
          inactiveTintColor: '#367681',
          style: {paddingTop: 12},
        }}>
        <BottomTab.Screen name="Home" component={HomeStack} />
        <BottomTab.Screen name="Messages" component={MessagesStack} />
        {!this.state.isCompany && (
          <BottomTab.Screen name="Network" component={NetworkStack} />
        )}
        <BottomTab.Screen name="Projects" component={ProjectStack} />
        <BottomTab.Screen
          name="More"
          component={DrawerStack}
          listeners={({navigation, route}) => ({
            tabPress: (e) => {
              e.preventDefault();

              // Do something with the `navigation` object
              if (this.state.drawer) {
                this.setState({drawer: false}, () => navigation.goBack());
              } else {
                this.setState({drawer: true}, () =>
                  navigation.navigate('More'),
                );
              }
            },
          })}
        />
      </BottomTab.Navigator>
    );
  }
}
