import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import AllForum from '../../../screens/user/Forum/AllForum'
import ForumDetails from '../../../screens/user/Forum/ForumDetails'
import ShareForum from '../../../screens/user/Forum/ShareForum'

const Stack = createStackNavigator()

function ForumStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>

        <Stack.Screen name='AllForum' component={AllForum}/>
        <Stack.Screen name='ForumDetails' component={ForumDetails}/>
        <Stack.Screen name='ShareForum' component={ShareForum}/>

      </Stack.Navigator>
  )
}

export default ForumStack
