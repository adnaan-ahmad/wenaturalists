import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import ForumPoll from '../../../screens/user/Draft/ForumPoll'
import Projects from '../../../screens/user/Draft/Projects'
import Blogs from '../../../screens/user/Draft/Blogs'
import EditBlogDraft from '../../../screens/user/Draft/EditBlogDraft'

const Stack = createStackNavigator()

function ForumStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>      
        <Stack.Screen name='ForumPoll' component={ForumPoll} options={{ animationEnabled: false }}/>
        <Stack.Screen name='Projects' component={Projects} options={{ animationEnabled: false }}/>
        <Stack.Screen name='Blogs' component={Blogs} options={{ animationEnabled: false }}/>
        <Stack.Screen name='EditBlogDraft' component={EditBlogDraft} options={{ animationEnabled: false }}/>    
      </Stack.Navigator>
  )
}

export default ForumStack
