import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack'

import Feeds from '../../screens/user/Home/Feeds'
import Explore from '../../screens/user/Home/Explore'
// import IndividualFeedsPost from '../../screens/user/Home/IndividualFeedsPost'
import NewFeedsPost from '../../screens/user/Home/NewFeedsPost'

const Stack = createStackNavigator()

function HomeStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
      }}>
      <Stack.Screen name='Feeds' component={Feeds} options={{ animationEnabled: false }} />
      {/* <Stack.Screen name='IndividualFeedsPost' component={IndividualFeedsPost} /> */}
      {/* <Stack.Screen name='NewFeedsPost' component={NewFeedsPost} /> */}
      <Stack.Screen name='Explore' component={Explore} options={{ animationEnabled: false }} />
    </Stack.Navigator>
  )
}

export default HomeStack