import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import FeedBack from '../../../screens/user/FeedBack/FeedBack'
import AllReviews from '../../../screens/user/FeedBack/AllReviews'

const Stack = createStackNavigator()

function FeedBackStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>
        
        <Stack.Screen name='FeedBack' component={FeedBack}/>
        <Stack.Screen name='AllReviews' component={AllReviews}/>

      </Stack.Navigator>
  )
}

export default FeedBackStack
