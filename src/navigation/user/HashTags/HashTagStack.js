import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import GlobalHashtag from '../../../screens/user/HashTags/GlobalHashtag'

const Stack = createStackNavigator()

function HashTagStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>

        <Stack.Screen name='GlobalHashtag' component={GlobalHashtag}/>
      </Stack.Navigator>
  )
}

export default HashTagStack
