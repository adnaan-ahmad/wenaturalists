import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import HiddenContent from '../../../screens/user/HiddenContent/HiddenContent'
import HiddenFeeds from '../../../screens/user/HiddenContent/HiddenFeeds'
import HiddenProjects from '../../../screens/user/HiddenContent/HiddenProjects'
import HiddenForum from '../../../screens/user/HiddenContent/HiddenForum'
import HiddenPoll from '../../../screens/user/HiddenContent/HiddenPoll'

const Stack = createStackNavigator()

function HiddenContentStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>

        <Stack.Screen name='HiddenContent' component={HiddenContent}/>
        <Stack.Screen name='HiddenFeeds' component={HiddenFeeds}/>
        <Stack.Screen name='HiddenProjects' component={HiddenProjects}/>
        <Stack.Screen name='HiddenForum' component={HiddenForum}/>
        <Stack.Screen name='HiddenPoll' component={HiddenPoll}/>

      </Stack.Navigator>
  )
}

export default HiddenContentStack
