import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import AllPoll from '../../../screens/user/Poll/AllPoll'
import PollDetails from '../../../screens/user/Poll/PollDetails'
import SharePoll from '../../../screens/user/Poll/SharePoll'

const Stack = createStackNavigator()

function PollStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>

        <Stack.Screen name='AllPoll' component={AllPoll}/>
        <Stack.Screen name='PollDetails' component={PollDetails}/>
        <Stack.Screen name='SharePoll' component={SharePoll}/>

      </Stack.Navigator>
  )
}

export default PollStack
