import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import Projects from '../../screens/user/Project/Projects'
import MyProject from '../../screens/user/Project/MyProject'
import PinnedProject from '../../screens/user/Project/PinnedProject'
import SeeAllProject from '../../screens/user/Project/SeeAllProject'
import ProjectsWebview from '../../screens/user/Project/ProjectsWebview'

const Stack = createStackNavigator()

function ProjectStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>
        <Stack.Screen name='ProjectScreen' component={Projects} />
        <Stack.Screen name='MyProject' component={MyProject} options={{animationEnabled: false}}/>
        <Stack.Screen name='PinnedProject' component={PinnedProject} options={{animationEnabled: false}}/>
        <Stack.Screen name='SeeAllProject' component={SeeAllProject} />
        <Stack.Screen name='ProjectsWebview' component={ProjectsWebview} />
      </Stack.Navigator>
  )
}

export default ProjectStack