import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator,CardStyleInterpolators } from '@react-navigation/stack'

import FaqAndSupport from '../../../screens/user/FaqAndSupport/FaqAndSupport'
import FaqDetails from '../../../screens/user/FaqAndSupport/FaqDetails'

const Stack = createStackNavigator()

function ForumStack() {
  return (
      <Stack.Navigator
        screenOptions={{headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS}}>

        <Stack.Screen name='FaqAndSupport' component={FaqAndSupport}/>
        <Stack.Screen name='FaqDetails' component={FaqDetails}/>

      </Stack.Navigator>
  )
}

export default ForumStack
