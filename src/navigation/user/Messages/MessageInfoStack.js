import 'react-native-gesture-handler';
import * as React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

import ChatInfo from '../../../screens/user/Messages/ChatInfo';

const Stack = createStackNavigator();

function MessageInfoStack({hide, hideGroup, changeState}) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen name="ChatInfo" component={ChatInfo} />
    </Stack.Navigator>
  );
}

export default MessageInfoStack;
