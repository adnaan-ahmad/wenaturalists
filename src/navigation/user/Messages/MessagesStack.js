import 'react-native-gesture-handler';
import * as React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

import CentralPanelMessage from '../../../screens/user/Messages/CentralPanelMessage';

const Stack = createStackNavigator();

function MessagesStack({hide, hideGroup, changeState}) {
  // function MessagesStack () {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen
        name="CentralPanelMessage"
        component={CentralPanelMessage}
      />
    </Stack.Navigator>
  );
}

export default MessagesStack;
