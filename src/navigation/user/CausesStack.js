import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack'

import CausesDetail from '../../screens/user/Causes/CausesDetail'
import SupportedCauses from '../../screens/user/Causes/SupportedCauses'
import RecommendedCauses from '../../screens/user/Causes/RecommendedCauses'

const Stack = createStackNavigator()

function CausesStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false, gesturesEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
      }}>
      <Stack.Screen name='RecommendedCauses' component={RecommendedCauses} options={{ animationEnabled: false }}/>
      <Stack.Screen name='SupportedCauses' component={SupportedCauses} options={{ animationEnabled: false }}/>
      <Stack.Screen name='CausesDetail' component={CausesDetail} />
      
    </Stack.Navigator>
  )
}

export default CausesStack
