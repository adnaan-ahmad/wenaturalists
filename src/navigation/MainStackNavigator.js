import 'react-native-gesture-handler'
import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { Easing } from 'react-native-reanimated'
import { Component } from 'react'

import Signup from '../screens/Signup'
import TermsConditions from '../screens/TermsConditions'
import CookiePolicy from '../screens/CookiePolicy'
import PrivacyPolicy from '../screens/PrivacyPolicy'
import UserAgreement from '../screens/UserAgreement'
import ResetPassword from '../screens/ResetPassword'
import Login from '../screens/Login'
import LoginWebview from '../screens/LoginWebview'
import BottomTabNavigator from './user/BottomTabNavigator'
import ProfileStack from './user/Profile/ProfileStack'
import OtherProfileStack from './user/Profile/OtherProfileStack'
import CompanyProfileStack from './user/Profile/CompanyProfileStack'
import ProfileEditStack from './user/Profile/ProfileEditStack'
import CausesStack from './user/CausesStack'
import CircleStack from './user/Circles/CircleStack'
import CircleProfileStack from './user/Circles/CircleProfileStack'
import ForumStack from './user/Forum/ForumStack'
import PollStack from './user/Poll/PollStack'
import HiddenContentStack from './user/HiddenContent/HiddenContentStack'
import FaqAndSupportStack from './user/FaqAndSupport/FaqAndSupportStack'
import DraftStack from './user/Draft/DraftStack'
import FeedBackStack from './user/FeedBack/FeedBackStack'
import LandingPage from '../screens/LandingPage'
import IndividualFeedsPost from '../screens/user/Home/IndividualFeedsPost'
import NewFeedsPost from '../screens/user/Home/NewFeedsPost'
import NewBlogPost from '../screens/user/Home/NewBlogPost'
import Webview from '../screens/user/Webview'
import SeeallRecommendedStories from '../screens/user/Home/SeeallRecommendedStories'
import SeeallPopularStories from '../screens/user/Home/SeeallPopularStories'
import SeeallTrendingPost from '../screens/user/Home/SeeallTrendingPost'
import EditorsDesk from '../screens/user/Home/EditorsDesk'
import MyNetworkStack from './user/Network/MyNetworkStack'
import PrivacySettingStack from './user/PrivacySettings/PrivacySettingStack'
import NetworkInvitationStack from './user/Network/NetworkInvitationStack'
import SeeAllPopularUsersStack from './user/Network/SeeAllPopularUsersStack'
import Notification from '../screens/user/Notification'
import Chats from '../screens/user/Messages/Chats'
import MessageInfoStack from './user/Messages/MessageInfoStack'
import HashTagStack from './user/HashTags/HashTagStack'
import ProjectDetailView from '../screens/user/Project/ProjectDetailView'
import HashTagDetail from '../screens/user/HashTags/HashTagDetail'
import RepostOnWenat from '../Components/User/Common/RepostOnWenat'
import EditPost from '../Components/User/Common/EditPost'
import ViewAllActivity from '../Components/User/Common/ViewAllActivity'
import PeopleWithSimilar from '../Components/User/Profile/PeopleWithSimilar'
import AllForumHashtags from '../screens/user/HashTags/AllForumHashtags'
import AllPollHashtag from '../screens/user/HashTags/AllPollHashtag'

const Stack = createStackNavigator()

const forFade = ({ current }) => ({
  cardStyle: {
    opacity: current.progress,
  },
})

const configFade = {
  animation: 'timing',
  config: {
    duration: 700,
    easing: Easing.linear,
  },
};

const config = {
  animation: 'timing',
  config: {
    duration: 280,
    easing: Easing.linear,
  },
};

const MyTransition = {
  gestureDirection: 'horizontal',
  transitionSpec: {
      open: config,
      close: config,
  },
  cardStyleInterpolator: ({ current, next, layouts }) => {
    return {
      cardStyle: {
        transform: [
          {
            translateX: current.progress.interpolate({
              inputRange: [0, 1],
              outputRange: [layouts.screen.width, 0],
            }),
          }
        ],
      },
      overlayStyle: {
        opacity: current.progress.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 0],
        }),
      },
    };
  },
}

class MainStackNavigator extends Component {

  constructor(props) {
    super(props)
  }

render() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false, gesturesEnabled: true, animationEnabled: true, gestureDirection: 'horizontal',
          ...MyTransition
        }}>
        <Stack.Screen name='LandingPage' component={LandingPage} />
        <Stack.Screen name='Login' component={Login} options={{ cardStyleInterpolator: forFade, 
          transitionSpec:{open: configFade, close: configFade} }} />
        <Stack.Screen name='LoginWebview' component={LoginWebview} options={{ cardStyleInterpolator: forFade, 
          transitionSpec:{open: configFade, close: configFade} }} />
        <Stack.Screen name='ResetPassword' component={ResetPassword} />
        <Stack.Screen name='Signup' component={Signup} />
        <Stack.Screen name='TermsConditions' component={TermsConditions} />
        <Stack.Screen name='CookiePolicy' component={CookiePolicy} />
        <Stack.Screen name='PrivacyPolicy' component={PrivacyPolicy} />
        <Stack.Screen name='UserAgreement' component={UserAgreement} />
        <Stack.Screen name='BottomTab' component={BottomTabNavigator} />
        <Stack.Screen name='ProfileStack' component={ProfileStack} />
        <Stack.Screen name='OtherProfileStack' component={OtherProfileStack} />
        <Stack.Screen name='CompanyProfileStack' component={CompanyProfileStack} />
        <Stack.Screen name='ProfileEditStack' component={ProfileEditStack} />
        <Stack.Screen name='CausesStack' component={CausesStack} />
        <Stack.Screen name='CircleStack' component={CircleStack} />
        <Stack.Screen name='CircleProfileStack' component={CircleProfileStack} />
        <Stack.Screen name='PrivacySettingStack' component={PrivacySettingStack} />
        <Stack.Screen name='MyNetworkStack' component={MyNetworkStack} />
        <Stack.Screen name='NetworkInvitationStack' component={NetworkInvitationStack} />
        <Stack.Screen name='SeeAllPopularUsersStack' component={SeeAllPopularUsersStack} />
        <Stack.Screen name='ForumStack' component={ForumStack} />
        <Stack.Screen name='PollStack' component={PollStack} />
        <Stack.Screen name='HiddenContentStack' component={HiddenContentStack} />
        <Stack.Screen name='FaqAndSupportStack' component={FaqAndSupportStack} />
        <Stack.Screen name='DraftStack' component={DraftStack} />
        <Stack.Screen name='FeedBackStack' component={FeedBackStack} />
        <Stack.Screen name='IndividualFeedsPost' component={IndividualFeedsPost} />
        <Stack.Screen name='ProjectDetailView' component={ProjectDetailView} />
        <Stack.Screen name='NewFeedsPost' component={NewFeedsPost} />
        <Stack.Screen name='NewBlogPost' component={NewBlogPost} />
        <Stack.Screen name='SeeallRecommendedStories' component={SeeallRecommendedStories} />
        <Stack.Screen name='SeeallPopularStories' component={SeeallPopularStories} />
        <Stack.Screen name='SeeallTrendingPost' component={SeeallTrendingPost} />
        <Stack.Screen name='EditorsDesk' component={EditorsDesk} />
        <Stack.Screen name='Webview' component={Webview} />
        <Stack.Screen name='Notification' component={Notification} />
        <Stack.Screen name='Chats' component={Chats} />
        <Stack.Screen name='MessageInfoStack' component={MessageInfoStack} />
        <Stack.Screen name='HashTagStack' component={HashTagStack} />
        <Stack.Screen name='HashTagDetail' component={HashTagDetail} />
        <Stack.Screen name='AllForumHashtags' component={AllForumHashtags} />
        <Stack.Screen name='AllPollHashtag' component={AllPollHashtag} />
        <Stack.Screen name='RepostOnWenat' component={RepostOnWenat} />
        <Stack.Screen name='EditPost' component={EditPost} />
        <Stack.Screen name='ViewAllActivity' component={ViewAllActivity} />
        <Stack.Screen name='PeopleWithSimilar' component={PeopleWithSimilar} />
      </Stack.Navigator>
    </NavigationContainer>
  )
  }
}

export default MainStackNavigator
